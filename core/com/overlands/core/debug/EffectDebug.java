package com.overlands.core.debug;

import com.overlands.kitpvp.Main;
import de.slikey.effectlib.Effect;
import de.slikey.effectlib.EffectManager;
import de.slikey.effectlib.EffectType;
import de.slikey.effectlib.util.ParticleEffect;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

public class EffectDebug extends Effect  {

    protected int iteration = 0;
    protected boolean reverse = false;
    protected double size = 6;
    protected int radius = 5;
    protected float scale = 1.0f;

    protected Integer taskId;

    public EffectDebug(EffectManager effectManager) {
        super(effectManager);
        type = EffectType.REPEATING;
        period = 1;
        iterations = 200;
    }

    public void _start() {
        iteration = 0;
        run();
    }

    @Override
    public void onRun() {
        taskId = null;
        iterate();

        if (++iteration < iterations) {
            schedule();
        }
    }

    public void stop() {
        iteration = iterations;
        if (taskId != null) {
            Bukkit.getScheduler().cancelTask(taskId);
            taskId = null;
        }
    }

    protected void iterate() {
        Location origin = getLocation();
        if (origin == null) return;

        float currentRadius = scale(radius * scale);
        double startRadians = Math.random() * Math.PI * 2;

        size = size + 0.5 / Math.random();

        for (int i = 0; i < size; i++) {
            double radians = (double) i / size * Math.PI * 2 + startRadians;
            Vector direction = new Vector(Math.cos(radians) * currentRadius, 0, Math.sin(radians) * currentRadius);
            Location source = origin;
            Location target = getTarget();
            source = source.clone().add(0, 2.5, 0);
            source.add(direction);
            display(ParticleEffect.REDSTONE, source, 0, 10);
        }
    }

    protected void schedule() {
        taskId = Bukkit.getScheduler().scheduleSyncDelayedTask(Main.get(), this, period);
    }

    public float scale(float maxValue) {
        return (float)scale((double)maxValue);
    }

    public double scale(double maxValue) {
        int maxIteration = iterations - 1;
        if (maxIteration <= 0) return maxValue;
        double i = reverse ? (maxIteration - iteration) : iteration;
        return (maxValue * (i / maxIteration) * (i / maxIteration));
    }
}
