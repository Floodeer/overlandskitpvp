package com.overlands.core.damage.combat;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.google.common.collect.Maps;
import com.overlands.core.damage.OverlandsDamageEvent;
import com.overlands.core.damage.combat.event.ClearCombatEvent;
import com.overlands.core.damage.combat.event.OverlandsDeathEvent;
import com.overlands.core.damage.combat.event.DeathMessageType;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.calc.TimeUtils;
import com.overlands.core.utils.scheduler.SchedulerEvent;
import com.overlands.core.utils.scheduler.UpdateType;
import com.overlands.core.utils.search.EntitySearcher;
import com.overlands.core.utils.search.ItemSearcher;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;

import net.minecraft.server.v1_8_R3.ItemStack;

public class CombatManager implements Listener {

	public CombatManager() {
		Main.get().getServer().getPluginManager().registerEvents(this, Main.get());
	}
	
	public enum AttackReason {
		Attack, CustomWeaponName, DefaultWeaponName
	}

	private Map<Player, CombatLog> active = Maps.newHashMap();
	private Map<String, ClientCombat> combatClients = Maps.newHashMap();
	private HashSet<Player> removeList = new HashSet<Player>();

	protected long ExpireTime = 15000;

	protected AttackReason attackReason = AttackReason.CustomWeaponName;

	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		if (combatClients.containsKey(e.getPlayer().getName()))
			combatClients.remove(e.getPlayer().getName());
	}

	public ClientCombat get(String name) {
		if (!combatClients.containsKey(name))
			combatClients.put(name, new ClientCombat());

		return combatClients.get(name);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void addAttack(EntityDamageEvent event) {
		if (event.isCancelled())
			return;

		if (event.getEntity() == null || !(event.getEntity() instanceof Player))
			return;

		Player damagee = (Player) event.getEntity();

		LivingEntity damagerEnt = getDamagerEntity(event, true);

		// Attacked by Entity
		if (damagerEnt != null) {
			if (damagerEnt instanceof Player)
				get((Player) damagerEnt).setLastCombat(System.currentTimeMillis());

			get(damagee).Attacked(EntitySearcher.getName(damagerEnt), event.getDamage(), damagerEnt, event.getCause() + "",
					null);
		}
		// Damager is WORLD
		else {
			DamageCause cause = event.getCause();

			String source = "?";
			String reason = "-";

			if (cause == DamageCause.BLOCK_EXPLOSION) {
				source = "Explosion";
				reason = "-";
			} else if (cause == DamageCause.CONTACT) {
				source = "Cactus";
				reason = "-";
			} else if (cause == DamageCause.CUSTOM) {
				source = "Custom";
				reason = "-";
			} else if (cause == DamageCause.DROWNING) {
				source = "Water";
				reason = "-";
			} else if (cause == DamageCause.ENTITY_ATTACK) {
				source = "Entity";
				reason = "Attack";
			} else if (cause == DamageCause.ENTITY_EXPLOSION) {
				source = "Explosion";
				reason = "-";
			} else if (cause == DamageCause.FALL) {
				source = "Fall";
				reason = "-";
			} else if (cause == DamageCause.FALLING_BLOCK) {
				source = "Falling Block";
				reason = "-";
			} else if (cause == DamageCause.FIRE) {
				source = "Fire";
				reason = "-";
			} else if (cause == DamageCause.FIRE_TICK) {
				source = "Fire";
				reason = "-";
			} else if (cause == DamageCause.LAVA) {
				source = "Lava";
				reason = "-";
			} else if (cause == DamageCause.LIGHTNING) {
				source = "Lightning";
				reason = "-";
			} else if (cause == DamageCause.MAGIC) {
				source = "Magic";
				reason = "-";
			} else if (cause == DamageCause.MELTING) {
				source = "Melting";
				reason = "-";
			} else if (cause == DamageCause.POISON) {
				source = "Poison";
				reason = "-";
			} else if (cause == DamageCause.PROJECTILE) {
				source = "Projectile";
				reason = "-";
			} else if (cause == DamageCause.STARVATION) {
				source = "Starvation";
				reason = "-";
			} else if (cause == DamageCause.SUFFOCATION) {
				source = "Suffocation";
				reason = "-";
			} else if (cause == DamageCause.SUICIDE) {
				source = "Suicide";
				reason = "-";
			} else if (cause == DamageCause.VOID) {
				source = "Void";
				reason = "-";
			} else if (cause == DamageCause.WITHER) {
				source = "Wither";
				reason = "-";
			}

			get(damagee).Attacked(source, event.getDamage(), null, reason, null);
		}

	}

	public void addAttack(OverlandsDamageEvent event) {
		// Not Player > No Log
		if (event.getTargetPlayer() == null)
			return;

		// Damager is ENTITY
		if (event.getDamagerEntity(true) != null) {
			String reason = event.getReason();

			if (reason == null) {
				if (event.getDamagerPlayer(false) != null) {
					Player damager = event.getDamagerPlayer(false);

					reason = "Attack";

					if (attackReason == AttackReason.DefaultWeaponName) {
						reason = "Fists";

						if (damager.getItemInHand() != null) {
							byte data = 0;
							if (damager.getItemInHand().getData() != null)
								data = damager.getItemInHand().getData().getData();

							reason = ItemSearcher.getName(damager.getItemInHand().getType(), data, false);
						}
					} else if (attackReason == AttackReason.CustomWeaponName) {
						reason = "Fists";

						if (damager.getItemInHand() != null) {
							ItemStack itemStack = CraftItemStack.asNMSCopy(damager.getItemInHand());
							if (itemStack != null) {
								reason = CraftItemStack.asNMSCopy(damager.getItemInHand()).getName();
							}
						}
					}
				} else if (event.getProjectile() != null) {
					if (event.getProjectile() instanceof Arrow)
						reason = "Tiro com Arco";
					else if (event.getProjectile() instanceof Fireball)
						reason = "Fireball";
				}
			}

			if (event.getDamagerEntity(true) instanceof Player)
				get((Player) event.getDamagerEntity(true)).setLastCombat(System.currentTimeMillis());

			get(event.getTargetPlayer()).Attacked(EntitySearcher.getName(event.getDamagerEntity(true)),
					(int) event.getDamage(), event.getDamagerEntity(true), reason, event.getDamageMod());
		}
		// Damager is WORLD
		else {
			DamageCause cause = event.getCause();

			String source = "?";
			String reason = "-";

			if (cause == DamageCause.BLOCK_EXPLOSION) {
				source = "Explosion";
				reason = "-";
			} else if (cause == DamageCause.CONTACT) {
				source = "Cactus";
				reason = "-";
			} else if (cause == DamageCause.CUSTOM) {
				source = "Custom";
				reason = "-";
			} else if (cause == DamageCause.DROWNING) {
				source = "Water";
				reason = "-";
			} else if (cause == DamageCause.ENTITY_ATTACK) {
				source = "Entity";
				reason = "Attack";
			} else if (cause == DamageCause.ENTITY_EXPLOSION) {
				source = "Explosion";
				reason = "-";
			} else if (cause == DamageCause.FALL) {
				source = "Fall";
				reason = "-";
			} else if (cause == DamageCause.FALLING_BLOCK) {
				source = "Falling Block";
				reason = "-";
			} else if (cause == DamageCause.FIRE) {
				source = "Fire";
				reason = "-";
			} else if (cause == DamageCause.FIRE_TICK) {
				source = "Fire";
				reason = "-";
			} else if (cause == DamageCause.LAVA) {
				source = "Lava";
				reason = "-";
			} else if (cause == DamageCause.LIGHTNING) {
				source = "Lightning";
				reason = "-";
			} else if (cause == DamageCause.MAGIC) {
				source = "Magic";
				reason = "-";
			} else if (cause == DamageCause.MELTING) {
				source = "Melting";
				reason = "-";
			} else if (cause == DamageCause.POISON) {
				source = "Poison";
				reason = "-";
			} else if (cause == DamageCause.PROJECTILE) {
				source = "Projectile";
				reason = "-";
			} else if (cause == DamageCause.STARVATION) {
				source = "Starvation";
				reason = "-";
			} else if (cause == DamageCause.SUFFOCATION) {
				source = "Suffocation";
				reason = "-";
			} else if (cause == DamageCause.SUICIDE) {
				source = "Suicide";
				reason = "-";
			} else if (cause == DamageCause.VOID) {
				source = "Void";
				reason = "-";
			} else if (cause == DamageCause.WITHER) {
				source = "Wither";
				reason = "-";
			}

			if (event.getReason() != null)
				reason = event.getReason();

			get(event.getTargetPlayer()).Attacked(source, (int) event.getDamage(), null, reason, event.getDamageMod());
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void handleDeath(PlayerDeathEvent event) {
		event.setDeathMessage(null);

		if (!active.containsKey(event.getEntity()))
			return;

		CombatLog log = active.remove(event.getEntity());
		log.setDeathTime(System.currentTimeMillis());
	

		// Save Death
		get(event.getEntity().getName()).getDeaths().addFirst(log);

		// add Kill/Assist
		int assists = 0;
		for (int i = 0; i < log.getAttackers().size(); i++) {
			if (!log.getAttackers().get(i).isPlayer())
				continue;

			if (TimeUtils.elapsed(log.getAttackers().get(i).getLastDamage(), ExpireTime))
				continue;

			if (log.getKiller() == null) {
				log.setKiller(log.getAttackers().get(i));

				ClientCombat killerClient = get(log.getAttackers().get(i).getName());

				if (killerClient != null)
					killerClient.getKills().addFirst(log);
			}

			else {
				assists++;

				ClientCombat assistClient = get(log.getAttackers().get(i).getName());

				if (assistClient != null)
					assistClient.getAssists().addFirst(log);
			}
		}

		log.setAssists(assists);
		log.setLocation(event.getEntity().getLocation());
		
		OverlandsDeathEvent deathEvent = new OverlandsDeathEvent(event, get(event.getEntity().getName()), log);
		Main.get().getServer().getPluginManager().callEvent(deathEvent);

		if (deathEvent.getBroadcastType() == DeathMessageType.Detailed || deathEvent.getBroadcastType() == DeathMessageType.Absolute) {	
			
			//String killedColor = log.getKilledColor();
			//String deadPlayer = killedColor + event.getEntity().getName();

			if (log.getKiller() != null) {
				String killerColor = log.getKillerColor();

				String killPlayer = killerColor + log.getKiller().getName();

				if (log.getAssists() > 0)
					killPlayer += " + " + log.getAssists() + " player(s)";

				//String weapon = log.getKiller().getLastDamageSource();

				if(GamePlayer.get(event.getEntity().getUniqueId()).isWarnings()) {
					event.getEntity().sendMessage(MessageUtil.color("&7Você foi morto por &9" + killPlayer + "&7."));
				}
			}
		}
	}

	@EventHandler
	public void ExpireOld(SchedulerEvent event) {
		if (event.getType() != UpdateType.FAST)
			return;

		for (CombatLog log : active.values())
			log.expireOld();
	}

	public void add(Player player) {
		active.put(player, new CombatLog(player, 15000));
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void Clear(ClearCombatEvent event) {
		active.remove(event.getPlayer());
	}

	public CombatLog get(Player player) {
		if (!active.containsKey(player)) {
			add(player);
		}

		return active.get(player);
	}

	public long getExpireTime() {
		return ExpireTime;
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void clearInactives(SchedulerEvent event) {
		if (event.getType() == UpdateType.MIN_02) {
			Iterator<Player> removeIterator = removeList.iterator();
			while (removeIterator.hasNext()) {
				Player player = removeIterator.next();

				if (!player.isOnline())
					active.remove(player);

				removeIterator.remove();
			}

			for (Player player : active.keySet()) {
				if (!player.isOnline())
					removeList.add(player);
			}
		}
	}

	public void setUseWeaponName(AttackReason var) {
		attackReason = var;
	}

	public AttackReason getUseWeapoName() {
		return attackReason;
	}
	
	public static LivingEntity getDamagerEntity(EntityDamageEvent event, boolean ranged)
	{
		if (!(event instanceof EntityDamageByEntityEvent))
			return null;

		EntityDamageByEntityEvent eventEE = (EntityDamageByEntityEvent)event;

		//Get Damager
		if (eventEE.getDamager() instanceof LivingEntity)
			return (LivingEntity)eventEE.getDamager();

		if (!ranged)
			return null;

		if (!(eventEE.getDamager() instanceof Projectile))
			return null;

		Projectile projectile = (Projectile)eventEE.getDamager();

		if (projectile.getShooter() == null)
			return null;

		if (!(projectile.getShooter() instanceof LivingEntity))
			return null;

		return (LivingEntity)projectile.getShooter();
	}
}
