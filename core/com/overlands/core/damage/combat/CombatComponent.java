package com.overlands.core.damage.combat;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import com.overlands.core.damage.DamageChange;

public class CombatComponent {
	
	private boolean player = false;

	private LinkedList<CombatDamage> damage;

	protected LivingEntity ent;
	protected String entityName;
	protected long lastDamage = 0;

	public CombatComponent(String name, LivingEntity ent) {
		entityName = name;

		if (ent != null) {
			if (ent instanceof Player) {
				this.ent = ent;
				player = true;
			}
		}
	}
	
	public Player get() {
		return (Player)ent;
	}

	public void addDamage(String source, double dmg, List<DamageChange> mod) {
		if (source == null)
			source = "-";

		getDamage().addFirst(new CombatDamage(source, dmg, mod));
		lastDamage = System.currentTimeMillis();
	}

	public String getName() {
		if (entityName.equals("Null"))
			return "World";

		return entityName;
	}

	public LinkedList<CombatDamage> getDamage() {
		if (damage == null)
			damage = new LinkedList<CombatDamage>();

		return damage;
	}

	public String getReason() {
		if (damage.isEmpty())
			return null;

		return damage.get(0).getName();
	}

	public long getLastDamage() {
		return lastDamage;
	}

	public int getTotalDamage() {
		int total = 0;
		for (CombatDamage cur : getDamage())
			total += cur.getDamage();
		return total;
	}

	public String getBestWeapon() {
		HashMap<String, Integer> cumulative = new HashMap<String, Integer>();
		String weapon = null;
		int best = 0;
		for (CombatDamage cur : damage) {
			int dmg = 0;
			if (cumulative.containsKey(cur.getName()))
				dmg = cumulative.get(cur.getName());

			cumulative.put(cur.getName(), dmg);

			if (dmg >= best)
				weapon = cur.getName();
		}

		return weapon;
	}

	public boolean isPlayer() {
		return player;
	}

	public String getLastDamageSource() {
		return damage.getFirst().getName();
	}
}