package com.overlands.core.damage.combat;

import java.util.LinkedList;
import java.util.WeakHashMap;

import org.bukkit.entity.LivingEntity;

import com.google.common.collect.Lists;
import com.overlands.core.utils.calc.TimeUtils;

public class ClientCombat {
	
	private LinkedList<CombatLog> kills = Lists.newLinkedList();
	private LinkedList<CombatLog> assists = Lists.newLinkedList();
	private LinkedList<CombatLog> deaths = Lists.newLinkedList();

	private WeakHashMap<LivingEntity, Long> lastHurt = new WeakHashMap<>();
	private WeakHashMap<LivingEntity, Long> lastHurtBy = new WeakHashMap<>();
	private long lastHurtByWorld = 0;

	public LinkedList<CombatLog> getKills() {
		return kills;
	}

	public LinkedList<CombatLog> getAssists() {
		return assists;
	}

	public LinkedList<CombatLog> getDeaths() {
		return deaths;
	}

	public boolean canBeHurtBy(LivingEntity damager) {
		if (damager == null) {
			if (TimeUtils.elapsed(lastHurtByWorld, 250)) {
				lastHurtByWorld = System.currentTimeMillis();
				return true;
			} else {
				return false;
			}
		}

		if (!lastHurtBy.containsKey(damager)) {
			lastHurtBy.put(damager, System.currentTimeMillis());
			return true;
		}

		if (System.currentTimeMillis() - lastHurtBy.get(damager) > 400) {
			lastHurtBy.put(damager, System.currentTimeMillis());
			return true;
		}

		return false;
	}

	public boolean canHurt(LivingEntity damagee) {
		if (damagee == null)
			return true;

		if (!lastHurt.containsKey(damagee)) {
			lastHurt.put(damagee, System.currentTimeMillis());
			return true;
		}

		if (System.currentTimeMillis() - lastHurt.get(damagee) > 400) {
			lastHurt.put(damagee, System.currentTimeMillis());
			return true;
		}

		return false;
	}
}