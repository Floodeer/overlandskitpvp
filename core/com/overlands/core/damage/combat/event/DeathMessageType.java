package com.overlands.core.damage.combat.event;

public enum DeathMessageType 
{
	Absolute,
	Detailed,
	Simple,
	None
}