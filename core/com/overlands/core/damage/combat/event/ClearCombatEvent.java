package com.overlands.core.damage.combat.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class ClearCombatEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	private Player player;

	public ClearCombatEvent(Player player) {
		this.player = player;
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public Player getPlayer() {
		return player;
	}
}