package com.overlands.core.damage.combat.event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityDeathEvent;

import com.overlands.core.damage.combat.ClientCombat;
import com.overlands.core.damage.combat.CombatLog;

public class OverlandsDeathEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	private EntityDeathEvent event;
	private ClientCombat clientCombat;
	private CombatLog log;
	private DeathMessageType messageType = DeathMessageType.Detailed;

	public OverlandsDeathEvent(EntityDeathEvent event, ClientCombat clientCombat, CombatLog log) {
		this.event = event;
		this.clientCombat = clientCombat;
		this.log = log;
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public ClientCombat getClientCombat() {
		return this.clientCombat;
	}

	public CombatLog getLog() {
		return this.log;
	}

	public EntityDeathEvent getEvent() {
		return this.event;
	}

	public void setBroadcastType(DeathMessageType value) {
		this.messageType = value;
	}

	public DeathMessageType getBroadcastType() {
		return this.messageType;
	}
}