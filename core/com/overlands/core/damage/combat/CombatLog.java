package com.overlands.core.damage.combat;

import java.util.LinkedList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import com.overlands.core.damage.DamageChange;
import com.overlands.core.utils.calc.TimeUtils;

public class CombatLog {
	
	private LinkedList<CombatComponent> damager = new LinkedList<CombatComponent>();
	private CombatComponent player;
	private long expireTime;

	private long deathTime = 0;
	private CombatComponent killer;
	private int assistants;

	private String killedColor = ChatColor.YELLOW + "";
	private String killerColor = ChatColor.YELLOW + "";
	
	private Location loc;

	protected CombatComponent LastDamager;
	protected long lastDamaged;
	protected long lastCombat;

	public CombatLog(Player player, long expireTime) {
		this.expireTime = expireTime;
		this.player = new CombatComponent(player.getName(), player);
	}

	public LinkedList<CombatComponent> getAttackers() {
		return damager;
	}

	public CombatComponent getPlayer() {
		return player;
	}

	public void Attacked(String damagerName, double damage, LivingEntity damagerEnt, String attackName,List<DamageChange> mod) {
		CombatComponent comp = getEnemy(damagerName, damagerEnt);

		comp.addDamage(attackName, damage, mod);
		
		LastDamager = comp;
		lastDamaged = System.currentTimeMillis();
		lastCombat = System.currentTimeMillis();
	}

	public CombatComponent getEnemy(String name, LivingEntity ent) {
		expireOld();

		CombatComponent component = null;
		for (CombatComponent cur : damager) {
			if (cur.getName().equals(name))
				component = cur;
		}
		
		if (component != null) {
			damager.remove(component);
			damager.addFirst(component);
			return damager.getFirst();
		}

		damager.addFirst(new CombatComponent(name, ent));
		return damager.getFirst();
	}

	public void expireOld() {
		int expireFrom = -1;
		for (int i = 0; i < damager.size(); i++) {
			if (TimeUtils.elapsed(damager.get(i).getLastDamage(), expireTime)) {
				expireFrom = i;
				break;
			}
		}

		if (expireFrom != -1)
			while (damager.size() > expireFrom)
				damager.remove(expireFrom);
	}

	public CombatComponent getKiller() {
		return killer;
	}

	public void setKiller(CombatComponent killer) {
		this.killer = killer;
	}

	public int getAssists() {
		return assistants;
	}

	public void setAssists(int assistants) {
		this.assistants = assistants;
	}

	public CombatComponent getLastDamager() {
		return LastDamager;
	}

	public long getLastDamaged() {
		return lastDamaged;
	}

	public long getLastCombat() {
		return lastCombat;
	}

	public void setLastCombat(long time) {
		lastCombat = time;
	}

	public long getDeathTime() {
		return deathTime;
	}

	public void setDeathTime(long deathTime) {
		this.deathTime = deathTime;
	}

	public String getKilledColor() {
		return killedColor;
	}

	public void setKilledColor(String color) {
		killedColor = color;
	}

	public String getKillerColor() {
		return killerColor;
	}

	public void setKillerColor(String color) {
		killerColor = color;
	}

	public Location getLocation() {
		return loc;
	}

	public void setLocation(Location loc) {
		this.loc = loc;
	}
}