package com.overlands.core.damage.combat;

import java.util.List;

import com.google.common.collect.Lists;
import com.overlands.core.damage.DamageChange;

public class CombatDamage {

	private String name;
	private double dmg;
	private long time;
	private List<DamageChange> mod = Lists.newArrayList();

	public CombatDamage(String name, double dmg, List<DamageChange> mod) {
		this.name = name;
		this.dmg = dmg;
		this.time = System.currentTimeMillis();
		this.mod = mod;
	}

	public String getName() {
		return name;
	}

	public double getDamage() {
		return dmg;
	}

	public long getTime() {
		return time;
	}

	public List<DamageChange> getDamageMod() {
		return mod;
	}
}