package com.overlands.core.damage;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import org.bukkit.EntityEffect;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftLivingEntity;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Fish;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import com.overlands.core.damage.combat.CombatManager;
import com.overlands.core.utils.calc.VelocityUtils;
import com.overlands.core.utils.entity.EntityUtils;
import com.overlands.core.utils.reflection.AccessUtil;
import com.overlands.core.utils.world.WorldUtils;
import com.overlands.kitpvp.Main;

import net.minecraft.server.v1_8_R3.DamageSource;
import net.minecraft.server.v1_8_R3.EntityHuman;
import net.minecraft.server.v1_8_R3.EntityLiving;

public class DamageManager implements Listener {

	protected Field lastDamageByPlayerTime;
	protected Method k;
	private CombatManager combatManager;

	public DamageManager(CombatManager manager) {
		this.combatManager = manager;

		try {
			lastDamageByPlayerTime = EntityLiving.class.getDeclaredField("lastDamageByPlayerTime");
			AccessUtil.setAccessible(lastDamageByPlayerTime);
			k = EntityLiving.class.getDeclaredMethod("damageArmor", float.class);
			AccessUtil.setAccessible(k);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		Main.get().getServer().getPluginManager().registerEvents(this, Main.get());
	}

	public void newDamageEvent(LivingEntity damagee, LivingEntity damager, Projectile proj, DamageCause cause, double damage, boolean knockback, boolean ignoreRate, boolean ignoreArmor, String source, String reason) {
		newDamageEvent(damagee, damager, proj, cause, damage, knockback, ignoreRate, ignoreArmor, source, reason,false);
	}
	
	public CombatManager getCombatManager() {
		return combatManager;
	}

	public void newDamageEvent(LivingEntity damagee, LivingEntity damager, Projectile proj, DamageCause cause,double damage, boolean knockback, boolean ignoreRate, boolean ignoreArmor, String source, String reason,boolean cancelled) {
		Main.get().getServer().getPluginManager().callEvent(new OverlandsDamageEvent(damagee, damager, proj, cause, damage, knockback, ignoreRate, ignoreArmor, source, reason, cancelled));
	}

	private void damage(OverlandsDamageEvent event) {
		if (event.getTargetEntity() == null)
			return;

		if (event.getTargetEntity().getHealth() <= 0)
			return;

		if (event.getTargetPlayer() != null) {
			combatManager.addAttack(event);
		}

		if (event.getDamagerPlayer(true) != null && event.displayDamageToLevel()) {
			if (event.getCause() != DamageCause.THORNS)
				event.getDamagerPlayer(true).setLevel((int) event.getDamage());
		}

		try {
			double bruteBonus = 0;
			if (event.IsBrute() && (event.getCause() == DamageCause.ENTITY_ATTACK
					|| event.getCause() == DamageCause.PROJECTILE || event.getCause() == DamageCause.CUSTOM))
				bruteBonus = Math.min(8, event.getDamage() * 2);

			handleDamage
					(event.getTargetEntity(), event.getDamagerEntity(true),
							event.getCause(),(float) (event.getDamage() + bruteBonus), event.ignoreArmor());

			event.getTargetEntity().playEffect(EntityEffect.HURT);
			
			if (event.getCause() == DamageCause.PROJECTILE) {
				((CraftLivingEntity)event.getTargetEntity()).getHandle().o(((CraftLivingEntity)event.getTargetEntity()).getHandle().bv() + 1);
			}

			//Knockback handler
			if (event.isKnockback() && event.getDamagerEntity(true) != null) {
				double knockback = event.getDamage();
				if (knockback < 2)
					knockback = 2;
				knockback = Math.log10(knockback);

				for (double cur : event.getKnockback().values())
					knockback = knockback * cur;

				// Origin
				Location origin = event.getDamagerEntity(true).getLocation();
				if (event.getKnockbackOrigin() != null)
					origin = event.getKnockbackOrigin();

				// Vec
				Vector trajectory = VelocityUtils.getTrajectory2d(origin, event.getTargetEntity().getLocation());
				trajectory.multiply(0.6 * knockback);
				trajectory.setY(Math.abs(trajectory.getY()));

				// Apply
				double vel = 0.2 + trajectory.length() * 0.8;

				VelocityUtils.velocity(event.getTargetEntity(), trajectory, vel, false, 0, Math.abs(0.2 * knockback), 0.4 + (0.04 * knockback), true);
			}
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	private void handleDamage(LivingEntity damagee, LivingEntity damager, DamageCause cause, float damage, boolean ignoreArmor) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		EntityLiving entityDamagee = ((CraftLivingEntity) damagee).getHandle();
		EntityLiving entityDamager = null;

		if (damager != null)
			entityDamager = ((CraftLivingEntity) damager).getHandle();

		entityDamagee.aG = 1.5F;
		
		if(damagee.getNoDamageTicks() > 0)
			return;

		if ((float) entityDamagee.noDamageTicks > (float) entityDamagee.maxNoDamageTicks / 2.0F) {
			if (damage <= entityDamagee.lastDamage) {
				return;
			}

			applyDamage(entityDamagee, damage - entityDamagee.lastDamage, ignoreArmor);
			entityDamagee.lastDamage = damage;
		} else {
			entityDamagee.lastDamage = damage;
			entityDamagee.aw = entityDamagee.getHealth();
			applyDamage(entityDamagee, damage, ignoreArmor);
		}

		if (entityDamager != null)
			entityDamagee.b(entityDamager);

		if (entityDamager != null)
			if (entityDamager instanceof EntityHuman) {
				damagee.setNoDamageTicks(20);
				lastDamageByPlayerTime.setInt(entityDamagee, 100);
				entityDamagee.killer = (EntityHuman) entityDamager;
			}

		if (entityDamagee.getHealth() <= 0) {
			if (entityDamager != null) {
				if (entityDamager instanceof EntityHuman)
					entityDamagee.die(DamageSource.playerAttack((EntityHuman) entityDamager));
				else entityDamagee.die(DamageSource.mobAttack((EntityLiving) entityDamager));
			} else
				entityDamagee.die(DamageSource.GENERIC);
		}
	}

	private void applyDamage(EntityLiving entityLiving, float damage, boolean ignoreArmor)
			throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		if (!ignoreArmor) {
			int j = 25 - entityLiving.br();
			float k = damage * (float) j;

			this.k.invoke(entityLiving, damage);
			damage = k / 25.0f;
		}

		entityLiving.setHealth(entityLiving.getHealth() - damage);
	}



	@EventHandler(priority = EventPriority.HIGHEST)
	public void startDamageEvent(EntityDamageEvent event) {

		boolean preCancel = false;
		if (event.isCancelled())
			preCancel = true;

		if (!(event.getEntity() instanceof LivingEntity))
			return;

		if(event.getEntity() instanceof Player) {
			if(((Player)event.getEntity()).getNoDamageTicks() > 0)
				return;
		}

		
		LivingEntity damagee = getTargetEntity(event);
		LivingEntity damager = CombatManager.getDamagerEntity(event, true);
		Projectile projectile = getProjectile(event);

		if (projectile instanceof Fish)
			return;

		double damage = event.getDamage();

		if (projectile != null && projectile instanceof Arrow){
			damage = projectile.getVelocity().length() * 3;
		}

		newDamageEvent(damagee, damager, projectile, event.getCause(), damage, true, false, false, null, null, preCancel);
	
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void removeArrows(EntityDamageEvent event){
		if (event.isCancelled()) {
			Projectile projectile = getProjectile(event);
			if (projectile instanceof Arrow) {
				projectile.teleport(new Location(projectile.getWorld(), 0, 0, 0));
				projectile.remove();
			}
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void handleEnchants(OverlandsDamageEvent event) {
		if (event.isCancelled())
			return;

		Player damagee = event.getTargetPlayer();
		if (damagee != null) {
			for (ItemStack stack : damagee.getInventory().getArmorContents()) {
				if (stack == null)
					continue;

				Map<Enchantment, Integer> enchants = stack.getEnchantments();
				for (Enchantment e : enchants.keySet()) {
					if (e.equals(Enchantment.PROTECTION_ENVIRONMENTAL))
						event.addMod("Ench Prot", damagee.getName(), 0.5 * (double) enchants.get(e), false);

					else if (e.equals(Enchantment.PROTECTION_FIRE) && event.getCause() == DamageCause.FIRE
							&& event.getCause() == DamageCause.FIRE_TICK && event.getCause() == DamageCause.LAVA)
						event.addMod("Ench Prot", damagee.getName(), 0.5 * (double) enchants.get(e), false);

					else if (e.equals(Enchantment.PROTECTION_FALL) && event.getCause() == DamageCause.FALL)
						event.addMod("Ench Prot", damagee.getName(), 0.5 * (double) enchants.get(e), false);

					else if (e.equals(Enchantment.PROTECTION_EXPLOSIONS)
							&& event.getCause() == DamageCause.ENTITY_EXPLOSION)
						event.addMod("Ench Prot", damagee.getName(), 0.5 * (double) enchants.get(e), false);

					else if (e.equals(Enchantment.PROTECTION_PROJECTILE) && event.getCause() == DamageCause.PROJECTILE)
						event.addMod("Ench Prot", damagee.getName(), 0.5 * (double) enchants.get(e), false);
				}
			}
		}
	}
	
	@EventHandler
	public void DamageSound(OverlandsDamageEvent event) {
		if (event.isCancelled())
			return;
		if(WorldUtils.inSpawn(event.getTargetEntity().getLocation()))
			return;
		
		if (event.getCause() != DamageCause.ENTITY_ATTACK && event.getCause() != DamageCause.PROJECTILE)
			return;

		LivingEntity damagee = event.getTargetEntity();
		if (damagee == null)    return;

		Sound sound = Sound.HURT_FLESH;
		float vol = 1f;
		float pitch = 1f;

		if(damagee instanceof Player) {
			damagee.getWorld().playSound(damagee.getLocation(), sound, vol, pitch);
		}else {
			EntityUtils.playDamageSound(damagee);
		}	
	}
	
	

	@EventHandler(priority = EventPriority.MONITOR)
	public void onDamageEvent(OverlandsDamageEvent event) {
		if (!event.isCancelled() && event.getDamage() > 0) {
			damage(event);

	
			if (event.getProjectile() != null && event.getProjectile() instanceof Arrow) {
				Player player = event.getDamagerPlayer(true);
				if (player != null) {
					player.playSound(player.getLocation(), Sound.ORB_PICKUP, 0.5f, 0.5f);
				}
			}
		}
	}
	
	private LivingEntity getTargetEntity(EntityDamageEvent event){
		if (event.getEntity() instanceof LivingEntity)
			return (LivingEntity)event.getEntity();

		return null;
	}

	private Projectile getProjectile(EntityDamageEvent event){
		if (!(event instanceof EntityDamageByEntityEvent))
			return null;

		EntityDamageByEntityEvent eventEE = (EntityDamageByEntityEvent)event;

		if (eventEE.getDamager() instanceof Projectile)
			return (Projectile)eventEE.getDamager();

		return null;
	}

}
