package com.overlands.core.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.overlands.core.utils.reflection.ReflectionUtils;
import net.citizensnpcs.util.NMS;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftLivingEntity;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.json.simple.JSONObject;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers.ClientCommand;
import com.google.common.collect.Lists;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.core.utils.particles.UtilParticle;
import com.overlands.core.utils.particles.UtilParticle.ParticleType;
import com.overlands.core.utils.world.WorldUtils;
import com.overlands.kitpvp.game.GamePlayer;

import net.minecraft.server.v1_8_R3.DamageSource;

public class Util {

	public static String Scramble = "§k";
	public static String Bold = "§l";
	public static String Strike = "§m";
	public static String BoldStrike = "§l§m";
	public static String Line = "§n";
	public static String Italics = "§o";
	public static ChatColor cAqua = ChatColor.AQUA;
	public static ChatColor cBlack = ChatColor.BLACK;
	public static ChatColor cBlue = ChatColor.BLUE;
	public static ChatColor cDAqua = ChatColor.DARK_AQUA;
	public static ChatColor cDBlue = ChatColor.DARK_BLUE;
	public static ChatColor cDGray = ChatColor.DARK_GRAY;
	public static ChatColor cDGreen = ChatColor.DARK_GREEN;
	public static ChatColor cDPurple = ChatColor.DARK_PURPLE;
	public static ChatColor cDRed = ChatColor.DARK_RED;
	public static ChatColor cGold = ChatColor.GOLD;
	public static ChatColor cGray = ChatColor.GRAY;
	public static ChatColor cGreen = ChatColor.GREEN;
	public static ChatColor cPurple = ChatColor.LIGHT_PURPLE;
	public static ChatColor cRed = ChatColor.RED;
	public static ChatColor cWhite = ChatColor.WHITE;
	public static ChatColor cYellow = ChatColor.YELLOW;
	public static ChatColor mHead = ChatColor.BLUE;
	public static ChatColor mBody = ChatColor.GRAY;
	public static ChatColor mChat = ChatColor.WHITE;
	public static ChatColor mElem = ChatColor.YELLOW;
	public static ChatColor mCount = ChatColor.YELLOW;
	public static ChatColor mTime = ChatColor.GREEN;
	public static ChatColor mItem = ChatColor.YELLOW;
	public static ChatColor mSkill = ChatColor.GREEN;
	public static ChatColor mLink = ChatColor.GREEN;
	public static ChatColor mLoot = ChatColor.RED;
	public static ChatColor mGame = ChatColor.LIGHT_PURPLE;
	public static ChatColor wField = ChatColor.WHITE;
	public static ChatColor wFrame = ChatColor.DARK_GRAY;
	public static ChatColor descHead = ChatColor.DARK_GREEN;
	public static ChatColor descBody = ChatColor.WHITE;
	public static ChatColor chatPMHead = ChatColor.DARK_GREEN;
	public static ChatColor chatPMBody = ChatColor.GREEN;
	public static ChatColor chatClanHead = ChatColor.DARK_AQUA;
	public static ChatColor chatClanBody = ChatColor.AQUA;
	public static ChatColor chatAdminHead = ChatColor.DARK_PURPLE;
	public static ChatColor chatAdminBody = ChatColor.LIGHT_PURPLE;
	public static ChatColor listTitle = ChatColor.WHITE;
	public static ChatColor listValue = ChatColor.YELLOW;
	public static ChatColor listValueOn = ChatColor.GREEN;
	public static ChatColor listValueOff = ChatColor.RED;
	public static ChatColor consoleHead = ChatColor.RED;
	public static ChatColor consoleFill = ChatColor.WHITE;
	public static ChatColor consoleBody = ChatColor.YELLOW;
	public static ChatColor sysHead = ChatColor.DARK_GRAY;
	public static ChatColor sysBody = ChatColor.GRAY;
	public static ChatColor chat = ChatColor.WHITE;;
	public static ChatColor reset = ChatColor.RESET;

	public static void displayProgress(String prefix, double amount, String suffix, boolean progressDirectionSwap,
			Player... players) {
		if (progressDirectionSwap)
			amount = 1 - amount;

		int bars = 24;
		String progressBar = cGreen + "";
		boolean colorChange = false;
		for (int i = 0; i < bars; i++) {
			if (!colorChange && (float) i / (float) bars >= amount) {
				progressBar += cRed;
				colorChange = true;
			}

			progressBar += "▌";
		}
		sendActionBar((prefix == null ? "" : prefix + ChatColor.RESET + " ") + progressBar
				+ (suffix == null ? "" : ChatColor.RESET + " " + suffix), players);
	}

	public static List<Player> filterNearby(Player damager, Location loc, double area) {
		List<Player> players = Lists.newArrayList();

		LocationUtils.getNearbyPlayers(loc, area).stream()
				.filter((cur) -> GamePlayer.get(cur).isInGame() 
						&& GamePlayer.get(cur).getTeam() != GamePlayer.get(damager) 
						&& !cur.equals(damager))
				.forEach(players::add);
		return players;
	}
	
	public static boolean canBeDamaged(Player damager, Player target) {
		if(WorldUtils.inSpawn(target.getLocation()))
			return false;
		if(damager.equals(target))
			return false;
		if(!GamePlayer.get(target).isInGame())
			return false;
		if(GamePlayer.get(damager).getTeam() == GamePlayer.get(damager))
			return false;
		
		return true;
	}

	public static List<Player> filterInLineOfSight(Player damager, Location loc, double area) {
		return LocationUtils.getNearbyPlayers(loc, area)
				.stream()
				.filter(damager::hasLineOfSight)
				.map(player -> (GamePlayer) GamePlayer.get(player))
				.filter(gp -> gp.isInGame() && !gp.getTeam().equals(GamePlayer.get(damager).getTeam()) && !damager.equals(gp.getPlayer()))
				.map(gp -> (Player) gp.getPlayer())
				.collect(Collectors.toList());
	}

	public static void sendActionBar(String string, Player... players) {
		for (Player p : players)
			ActionBar.sendActionBar(p, string);
	}

	public static void sendActionBar(Player player, String str) {
		ActionBar.sendActionBar(player, str);
	}

	public static void sendSkyPacket(Player player, int number) {
		try {
			Class<?> packetClass = getNMSClass("PacketPlayOutGameStateChange");
			Constructor<?> packetConstructor = packetClass.getConstructor(int.class, float.class);
			Object packet = packetConstructor.newInstance(7, number);
			Method sendPacket = getNMSClass("PlayerConnection").getMethod("sendPacket", getNMSClass("Packet"));
			sendPacket.invoke(getConnection(player), packet);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void sendRespawnPacket(Player player) {
		PacketContainer packet = new PacketContainer(PacketType.Play.Client.CLIENT_COMMAND);
		packet.getClientCommands().write(0, ClientCommand.PERFORM_RESPAWN);
		try {
			ProtocolLibrary.getProtocolManager().recieveClientPacket(player, packet);
		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	public static void sendPacket(Player player, Object packet) {
		try {
			Object handle = player.getClass().getMethod("getHandle").invoke(player);
			Object playerConnection = handle.getClass().getField("playerConnection").get(handle);
			playerConnection.getClass().getMethod("sendPacket", getNMSClass("Packet")).invoke(playerConnection, packet);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public static Object getConnection(Player player) throws SecurityException, NoSuchMethodException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		Method getHandle = player.getClass().getMethod("getHandle");
		Object nmsPlayer = getHandle.invoke(player);
		Field conField = nmsPlayer.getClass().getField("playerConnection");
		Object con = conField.get(nmsPlayer);
		return con;
	}


	public static Class<?> getNMSClass(String name) {
		String version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
		try {
			return Class.forName("net.minecraft.server." + version + "." + name);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void sendTitle(Player player, Integer fadeIn, Integer stay, Integer fadeOut, String title,
			String subtitle) {
		try {
			Object e;
			Object chatTitle;
			Object chatSubtitle;
			Constructor<?> subtitleConstructor;
			Object titlePacket;
			Object subtitlePacket;

			if (title != null) {
				title = ChatColor.translateAlternateColorCodes('&', title);
				title = title.replaceAll("%player%", player.getDisplayName());
				e = getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("TIMES").get((Object) null);
				chatTitle = getNMSClass("IChatBaseComponent").getDeclaredClasses()[0]
						.getMethod("a", new Class[] { String.class })
						.invoke((Object) null, new Object[] { "{\"text\":\"" + title + "\"}" });
				subtitleConstructor = getNMSClass("PacketPlayOutTitle")
						.getConstructor(new Class[] { getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0],
								getNMSClass("IChatBaseComponent"), Integer.TYPE, Integer.TYPE, Integer.TYPE });
				titlePacket = subtitleConstructor.newInstance(new Object[] { e, chatTitle, fadeIn, stay, fadeOut });
				sendPacket(player, titlePacket);

				e = getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("TITLE").get((Object) null);
				chatTitle = getNMSClass("IChatBaseComponent").getDeclaredClasses()[0]
						.getMethod("a", new Class[] { String.class })
						.invoke((Object) null, new Object[] { "{\"text\":\"" + title + "\"}" });
				subtitleConstructor = getNMSClass("PacketPlayOutTitle").getConstructor(new Class[] {
						getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0], getNMSClass("IChatBaseComponent") });
				titlePacket = subtitleConstructor.newInstance(new Object[] { e, chatTitle });
				sendPacket(player, titlePacket);
			}

			if (subtitle != null) {
				subtitle = ChatColor.translateAlternateColorCodes('&', subtitle);
				subtitle = subtitle.replaceAll("%player%", player.getDisplayName());
				e = getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("TIMES").get((Object) null);
				chatSubtitle = getNMSClass("IChatBaseComponent").getDeclaredClasses()[0]
						.getMethod("a", new Class[] { String.class })
						.invoke((Object) null, new Object[] { "{\"text\":\"" + title + "\"}" });
				subtitleConstructor = getNMSClass("PacketPlayOutTitle")
						.getConstructor(new Class[] { getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0],
								getNMSClass("IChatBaseComponent"), Integer.TYPE, Integer.TYPE, Integer.TYPE });
				subtitlePacket = subtitleConstructor
						.newInstance(new Object[] { e, chatSubtitle, fadeIn, stay, fadeOut });
				sendPacket(player, subtitlePacket);

				e = getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("SUBTITLE").get((Object) null);
				chatSubtitle = getNMSClass("IChatBaseComponent").getDeclaredClasses()[0]
						.getMethod("a", new Class[] { String.class })
						.invoke((Object) null, new Object[] { "{\"text\":\"" + subtitle + "\"}" });
				subtitleConstructor = getNMSClass("PacketPlayOutTitle")
						.getConstructor(new Class[] { getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0],
								getNMSClass("IChatBaseComponent"), Integer.TYPE, Integer.TYPE, Integer.TYPE });
				subtitlePacket = subtitleConstructor
						.newInstance(new Object[] { e, chatSubtitle, fadeIn, stay, fadeOut });
				sendPacket(player, subtitlePacket);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void sendTabTitle(Player player, String header, String footer) {
		if (header == null)
			header = "";
		header = ChatColor.translateAlternateColorCodes('&', header);

		if (footer == null)
			footer = "";
		footer = ChatColor.translateAlternateColorCodes('&', footer);

		header = header.replaceAll("%player%", player.getDisplayName());
		footer = footer.replaceAll("%player%", player.getDisplayName());

		try {
			Object tabHeader = getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class)
					.invoke(null, "{\"text\":\"" + header + "\"}");
			Object tabFooter = getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class)
					.invoke(null, "{\"text\":\"" + footer + "\"}");
			Constructor<?> titleConstructor = getNMSClass("PacketPlayOutPlayerListHeaderFooter")
					.getConstructor(getNMSClass("IChatBaseComponent"));
			Object packet = titleConstructor.newInstance(tabHeader);
			Field field = packet.getClass().getDeclaredField("b");
			field.setAccessible(true);
			field.set(packet, tabFooter);
			sendPacket(player, packet);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void clearTitle(Player player) {
		sendTitle(player, 0, 0, 0, "", "");
	}

	public static void sendFormatted(Player p, String title, String sub) {
		sendTitle(p, 12, 38, 12, MessageUtil.color(title), MessageUtil.color(sub));
	}

	public static void sendFormatted2Long(Player p, String title, String sub) {
		sendTitle(p, 16, 60, 16, MessageUtil.color(title), MessageUtil.color(sub));
	}

	public static void healPlayer(Player p, double health) {
		Damageable damp = p;
		if (damp.getHealth() <= 20.0D) {
			p.setHealth(damp.getHealth() + health >= 20.0D ? 20.0D : damp.getHealth() + health);
		}
	}
	
	public static void heal(Player healer, Player target, double amount, boolean message, boolean critical, boolean particles) {
		GamePlayer healergp = GamePlayer.get(healer);
		GamePlayer targetgp = GamePlayer.get(target);
		if(critical) {
			amount = amount+Math.max(amount, amount+6);
		}
		if(target.getHealth() >= 20) 
			return;
		
		Damageable damp = target;
		if (damp.getHealth() < 20.0D) {
			target.setHealth(damp.getHealth() + amount >= 20.0D ? 20.0D : damp.getHealth() + amount);
		}else{
			return;
		}
		
		if(message) {
			if(!healer.equals(target) && targetgp.isWarnings()) {
				String heal = critical ? "&a&lcura crítica " + amount : "&a" + amount;
				target.sendMessage(MessageUtil.color("&a➡ &7" + healergp.getPlayer().getName() + " &7curou você em " + heal + " &7de vida."));
			}
			
			if(healergp.isWarnings()) {
				String heal = critical ? "&a&l(cura crítica) " + amount : "&a" + amount;
				if(healer.equals(target)) {
					healer.sendMessage(MessageUtil.color("&a➡ &7+" + heal));
				}else{
				    healer.sendMessage(MessageUtil.color("&a➡ &7Você curou " + targetgp.getPlayer().getName() + " em " + heal + " &7de vida."));
				}
			}
		}
	    if(particles) {
	    	ParticleEffect.VILLAGER_HAPPY.display(0.2f, 0.3f, 0.2f, 1.0f, 10, target.getLocation(), 96);
	    }
	    healergp.setHealingDone(healergp.getHealingDone()+1);
	    targetgp.setHealingReceived(targetgp.getHealingReceived()+1);
	}

	public static void areaDamage(Player damager, double amount, Location start, int area, boolean self,
			boolean player) {
		for (Entity e : LocationUtils.getNearbyEntities(start, area)) {
			if (!self) {
				if (e == damager)
					continue;
			}
			if (!player)
				if (e.getType() == EntityType.PLAYER)
					continue;

			damage(e, damager, (float) amount);
		}
	}

	public static void damage(Entity e, Player who, float f) {

		if (e instanceof LivingEntity) {
			((CraftLivingEntity) e).getHandle().damageEntity(DamageSource.playerAttack(((CraftPlayer) who).getHandle()),
					f);
		} else {
			((CraftEntity) e).getHandle().damageEntity(DamageSource.playerAttack(((CraftPlayer) who).getHandle()), f);
		}
	}

	public static void damagePlayer(Player p, Player who, double f, boolean trueDamage) {
		double damage = DamageUtils.calculateDamageRemoveArmor(p.getInventory().getArmorContents(),
				DamageCause.ENTITY_ATTACK, f, p.isBlocking());

		p.damage(trueDamage ? damage * 2 : f, who);
	}

	public static String onlyNumbers(String str) {
		return str.replaceAll("[^-?0-9]+", "");
	}

	@SuppressWarnings("unchecked")
	public static String saveLocation(Location location, boolean yawPitch) {
		final JSONObject jsonObject = new JSONObject();
		jsonObject.put("world", location.getWorld().getName());
		jsonObject.put("x", location.getX());
		jsonObject.put("y", location.getY());
		jsonObject.put("z", location.getZ());
		if (yawPitch) {
			jsonObject.put("yaw", location.getYaw());
			jsonObject.put("pitch", location.getPitch());
		}
		return jsonObject.toJSONString();
	}

	public static Location getLocation(JSONObject fromJson, boolean yawpitch) {
		double x = (double) fromJson.get("x");
		double y = (double) fromJson.get("y");
		double z = (double) fromJson.get("z");
		String world = (String) fromJson.get("world");
		if (yawpitch && fromJson.containsKey("pitch") && fromJson.containsKey("yaw")) {
			double pitch = (double) fromJson.get("pitch");
			double yaw = (double) fromJson.get("yaw");
			return new Location(Bukkit.getWorld(world), Math.floor(x), Math.floor(y), Math.floor(z), (float) yaw,
					(float) pitch);
		} else {
			return new Location(Bukkit.getWorld(world), Math.floor(x), Math.floor(y), Math.floor(z));
		}
	}
	
	public static void addCommand(Command cmd, Plugin plugin) throws ReflectiveOperationException {
		Method commandMap = plugin.getServer().getClass().getMethod("getCommandMap");
		Object cmdmap = commandMap.invoke(plugin.getServer());
		Method register = cmdmap.getClass().getMethod("register", String.class, Command.class);
		register.invoke(cmdmap, cmd.getName(), cmd);
	}

	public static void hunger(Player player, int mod) {
		if (player.isDead())
			return;

		int hunger = player.getFoodLevel() + mod;

		if (hunger < 0)
			hunger = 0;

		if (hunger > 20)
			hunger = 20;

		player.setFoodLevel(hunger);
	}

	public static void addEffect(Player toPlayer, PotionEffectType type, int time, int level) {
		toPlayer.addPotionEffect(new PotionEffect(type, time, level));
	}

	public static void reAddEffect(Player toPlayer, PotionEffectType type, int time, int level) {
		if (toPlayer.hasPotionEffect(type)) {
			toPlayer.removePotionEffect(type);
		}
		toPlayer.addPotionEffect(new PotionEffect(type, time, level));
	}

	public static String getDirection(Float f) {
		f = f / 90.0F;
		f = (float) Math.round(f);
		if ((f.floatValue() == -4.0F) || (f.floatValue() == 0.0F) || (f.floatValue() == 4.0F)) {
			return "SOUTH";
		}
		if ((f.floatValue() == -1.0F) || (f.floatValue() == 3.0F)) {
			return "EAST";
		}
		if ((f.floatValue() == -2.0F) || (f.floatValue() == 2.0F)) {
			return "NORTH";
		}
		if ((f.floatValue() == -3.0F) || (f.floatValue() == 1.0F)) {
			return "WEST";
		}
		return "";
	}

	public static boolean isChargingBow(Player player) {
		if (!ItemFactory.isMat(player.getItemInHand(), Material.BOW))
			return false;

		return (((CraftEntity) player).getHandle().getDataWatcher().getByte(0) & 1 << 4) != 0;
	}

	public static void clearWorld(World w) {
		w.getEntities().stream().filter(e -> e.getType() != EntityType.PLAYER && e.getType() != EntityType.ARMOR_STAND)
				.forEach(ent -> ent.remove());
	}

	public static Location theSpawn() {
		return new Location(Bukkit.getWorld("world"), -6.9, 63.5, 18, -179.4f, 2.3f);
	}

	public static void updatePlayer(Player p) {
		ProtocolLibrary.getProtocolManager().updateEntity(p, ProtocolLibrary.getProtocolManager().getEntityTrackers(p));

		Bukkit.getOnlinePlayers().stream().filter((players) -> players != p).forEach((player) -> {
			player.hidePlayer(p);
			player.showPlayer(p);
		});
	}

	public static String after(String str, String substr) {
		final int position = str.indexOf(substr);
		if (position >= 0) {
			return str.substring(position + substr.length());
		} else {
			return "Error";
		}
	}

	public static String before(String str, String substr) {
		final int position = str.indexOf(substr);
		if (position >= 0) {
			return str.substring(0, position);
		} else {
			return "Error";
		}
	}
	
	public static void display(ParticleEffect effect, Location location, int amount, float speed) {
		effect.display(0, 0, 0, speed, amount, location, 128);
	}

	public static void display(ParticleEffect effect, Location location, int amount) {
		effect.display(0, 0, 0, 0, amount, location, 128);
	}

	public static void display(ParticleEffect effect, double x, double y, double z, Location location, int amount) {
		effect.display((float) x, (float) y, (float) z, 0f, amount, location, 128);
	}

	public static void display(ParticleType type, float speed, int amount, Location loc) {
		new UtilParticle(ParticleType.EXPLOSION_HUGE, 1.0F, 18, 120.D).sendToLocation(loc);
	}

	public static void display(int red, int green, int blue, Location location) {
		ParticleEffect.REDSTONE.display(new ParticleEffect.OrdinaryColor(red, green, blue), location, 128);
	}

	public static void display(ParticleEffect effect, int red, int green, int blue, Location location, int amount) {
		for (int i = 0; i < amount; i++)
			effect.display(new ParticleEffect.OrdinaryColor(red, green, blue), location, 128);
	}
	
	public static void display(int id, int data, Location loc, float x, float y, float z, int count, float speed) {
		loc.getWorld().spigot().playEffect(loc, Effect.COLOURED_DUST, id, data, x, y, z, speed, count, 128);
	}
	
	public static void hidePlayer(Player player, Player target, boolean hide) {
		if (hide) {
			player.hidePlayer(target);
		}else{
			player.showPlayer(target);
		}
	}
	
	public static String removeLastCharOptional(String s) {
	    return Optional.ofNullable(s)
	      .filter(str -> str.length() != 0)
	      .map(str -> str.substring(0, str.length() - 1))
	      .orElse(s);
	  }
	
	public static String removeLastChar(String str) {
	    if (str != null && str.length() > 0) {
	        str = str.substring(0, str.length() - 1);
	    }
	    return str;
	}
}
