package com.overlands.core.utils.java;

public class CalculationAssertionException extends AssertionError {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CalculationAssertionException(String msg) {
		super(msg);
	}
	
	public CalculationAssertionException() {
		
	}
}
