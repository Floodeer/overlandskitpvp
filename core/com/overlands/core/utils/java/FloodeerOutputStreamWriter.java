package com.overlands.core.utils.java;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.zip.Deflater;

public class FloodeerOutputStreamWriter extends OutputStream {
	
	protected int originalPos = -1;

	public FloodeerOutputStreamWriter(File f) throws FileNotFoundException {

		out = new BufferedOutputStream(new FileOutputStream(f));

	}
	private Charset charset = StandardCharsets.ISO_8859_1;
	protected OutputStream out;
	protected int pos = 0;

	public Charset getCharset() {
		return charset;
	}

	public void setCharset(Charset charset) {
		this.charset = charset;
	}

	public void writeStr(String str) throws IOException {
		byte[] encoded = str.getBytes(charset);
		write4(encoded.length);
		write(encoded);
	}

	public void writeStr1(String str) throws IOException {
		byte[] encoded = str.getBytes(charset);
		int writeSize = Math.min(encoded.length, 255);
		write(writeSize);
		write(encoded, 0, writeSize);
	}

	public void writeBool(boolean val) throws IOException {
		write4(val ? 1 : 0);
	}

	public void compress(byte[] data) throws IOException {
		Deflater compresser = new Deflater();
		compresser.setInput(data);
		compresser.finish();
		byte[] buffer = new byte[131072];
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		while (!compresser.finished()) {
			int len = compresser.deflate(buffer);
			baos.write(buffer, 0, len);
		}
		write4(baos.size());
		write(baos.toByteArray());
	}

	public void write(byte[] b) throws IOException {
		out.write(b);
	}

	public void write(byte[] b, int off, int len) throws IOException {
		out.write(b, off, len);
		pos += len;
	}

	public void write(int b) throws IOException {
		out.write(b);
		pos++;
	}

	public void write2(int val) throws IOException {
		short i = (short) val;
		write(i & 255);
		write((i >>> 8) & 255);
	}

	public void write3(int val) throws IOException {
		write(val & 255);
		write((val >>> 8) & 255);
		write((val >>> 16) & 255);
	}

	public void write4(int val) throws IOException {
		write(val & 255);
		write((val >>> 8) & 255);
		write((val >>> 16) & 255);
		write((val >>> 24) & 255);
	}

	public void writeD(double val) throws IOException {
		long num = Double.doubleToLongBits(val);
		byte[] b = new byte[8];
		b[0] = (byte) (num & 0xFF);
		for (int i = 1; i < 8; i++)
			b[i] = (byte) ((num >>> (8 * i)) & 0xFF);
		write(b);
	}

	public void close() throws IOException {
		out.close();
	}

	public void fill(int count) throws IOException {
		for (int i = 0; i < count; i++) {
			write4(0);
		}
	}

	public void flush() throws IOException {
		out.flush();
	}

	protected static int[] makeEncodeTable(int seed) {
		int[] table = new int[256];
		int a = 6 + (seed % 250);
		int b = seed / 250;
		for (int i = 0; i < 256; i++)
			table[i] = i;
		for (int i = 1; i < 10001; i++) {
			int j = 1 + ((i * a + b) % 254);
			int t = table[j];
			table[j] = table[j + 1];
			table[j + 1] = t;
		}
		return table;
	}
}