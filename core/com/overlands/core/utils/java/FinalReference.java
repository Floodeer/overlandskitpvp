package com.overlands.core.utils.java;

public class FinalReference<T> {
	
	T t;
	T copyOfT;
	
	public FinalReference(T t) {
		this.t = t;
	}
	
	public void set(T t) {
		this.t = t;
	}
	
	public T get() {
		return t;
	}
	
	public T setAndGet(T t) {
		this.t = t;
		return t;
	}
	
	public void copy() {
		copyOfT = t;
	}
	
	public T getCopy() {
		return copyOfT == null ? t : copyOfT;
	}
}
