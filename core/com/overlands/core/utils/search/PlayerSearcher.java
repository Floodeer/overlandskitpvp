package com.overlands.core.utils.search;

import java.util.LinkedList;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Util;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.database.interfaces.Callback;

public class PlayerSearcher {

	public static LinkedList<Player> matchesPlayers(Player caller, String players, boolean inform) {
		LinkedList<Player> matchList = new LinkedList<Player>();

		String failList = "";

		for (String cur : players.split(",")) {
			Player match = searchOnline(caller, cur, inform);

			if (match != null)
				matchList.add(match);

			else
				failList += cur + " ";
		}

		if (inform && failList.length() > 0) {
			failList = failList.substring(0, failList.length() - 1);
			caller.sendMessage(MessageUtil.color("&cFalha:" + failList + "."));
		}

		return matchList;
	}

	public static Player searchExact(String name) {
		return Bukkit.getOnlinePlayers().stream().filter(player -> player.getName().equalsIgnoreCase(name)).findAny()
				.orElse(null);
	}

	public static Player searchOnline(CommandSender caller, String player, boolean inform) {
		LinkedList<Player> matchList = new LinkedList<Player>();

		for (Player cur : Bukkit.getOnlinePlayers()) {
			if (cur.getName().equalsIgnoreCase(player))
				return cur;

			if (cur.getName().toLowerCase().contains(player.toLowerCase()))
				matchList.add(cur);
		}

		if (matchList.size() != 1) {
			if (!inform)
				return null;

			if (caller != null)
				caller.sendMessage(
						MessageUtil.color("&e" + matchList.size() + " &7resultados para o player &b" + player));

			if (matchList.size() > 0) {
				String matchString = "";
				for (Player cur : matchList)
					matchString += Util.cDBlue + cur.getName() + Util.cDGray + ", ";
				if (matchString.length() > 1)
					matchString = matchString.substring(0, matchString.length() - 2);

				if (caller != null)
					caller.sendMessage(MessageUtil.color(matchString));
			}

			return null;
		}
		return matchList.get(0);
	}

	public static void searchOffline(String player, Callback<Boolean> callback) {
		Main.getDataManager().doesPlayerExists(player, callback);
	}
}
