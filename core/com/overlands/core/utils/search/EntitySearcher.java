package com.overlands.core.utils.search;

import java.util.HashMap;
import java.util.LinkedList;

import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Util;

public class EntitySearcher {

	private static HashMap<Entity, String> _nameMap = new HashMap<Entity, String>();
	private static HashMap<String, EntityType> creatureMap = new HashMap<String, EntityType>();

	@SuppressWarnings("deprecation")
	public static EntityType searchEntity(Player caller, String arg, boolean inform) {
		putAll();
		
		arg = arg.toLowerCase();
		LinkedList<EntityType> matchList = new LinkedList<EntityType>();
		for (String cur : creatureMap.keySet()) {
			if (cur.equalsIgnoreCase(arg))
				return creatureMap.get(cur);

			if (cur.toLowerCase().contains(arg))
				matchList.add(creatureMap.get(cur));
		}

		if (matchList.size() != 1) {
			if (!inform)
				return null;

			caller.sendMessage(MessageUtil.color("&e" + matchList.size() + " &7resultados para &b" + arg));

			if (matchList.size() > 0) {
				String matchString = "";
				for (EntityType cur : matchList)
					matchString += Util.cDBlue + cur.getName() + Util.cDGray + ", ";
				if (matchString.length() > 1)
					matchString = matchString.substring(0, matchString.length() - 2);

				caller.sendMessage(MessageUtil.color(matchString));
			}

			return null;
		}
		return matchList.get(0);
	}

	public static String getName(Entity ent) {
		if (ent == null)
			return "Null";

		if (ent.getType() == EntityType.PLAYER)
			return ((Player) ent).getName();

		if (_nameMap.containsKey(ent))
			return _nameMap.get(ent);

		if (ent instanceof LivingEntity) {
			LivingEntity le = (LivingEntity) ent;
			if (le.getCustomName() != null)
				return le.getCustomName();
		}

		return getName(ent.getType());
	}

	@SuppressWarnings("deprecation")
	public static String getName(EntityType type) {
		for (String cur : creatureMap.keySet())
			if (creatureMap.get(cur) == type)
				return cur;

		return type.getName();
	}

	public static String searchName(Player caller, String arg, boolean inform) {
		putAll();

		arg = arg.toLowerCase().replaceAll("_", " ");
		LinkedList<String> matchList = new LinkedList<String>();
		for (String cur : creatureMap.keySet()) {
			if (cur.equalsIgnoreCase(arg))
				return cur;

			if (cur.toLowerCase().contains(arg))
				matchList.add(cur);
		}
		if (matchList.size() != 1) {
			if (!inform)
				return null;

			caller.sendMessage(MessageUtil.color("&e" + matchList.size() + " &7resultados para &b" + arg));

			if (matchList.size() > 0) {
				String matchString = "";
				for (String cur : matchList)
					matchString += Util.cDBlue + cur + Util.cDGray + ", ";
				if (matchString.length() > 1)
					matchString = matchString.substring(0, matchString.length() - 2);

				matchString += Util.cDBlue + matchString + Util.cDGray + ", ";

				caller.sendMessage(MessageUtil.color(matchString));
			}
			return null;
		}
		return matchList.get(0);
	}

	public static void playDamageSound(LivingEntity damagee) 
	{
		Sound sound = Sound.HURT_FLESH;
		
		if (damagee.getType() == EntityType.BAT)				sound = Sound.BAT_HURT;
		else if (damagee.getType() == EntityType.BLAZE)			sound = Sound.BLAZE_HIT;
		else if (damagee.getType() == EntityType.CAVE_SPIDER)	sound = Sound.SPIDER_IDLE;
		else if (damagee.getType() == EntityType.CHICKEN)		sound = Sound.CHICKEN_HURT;
		else if (damagee.getType() == EntityType.COW)			sound = Sound.COW_HURT;
		else if (damagee.getType() == EntityType.CREEPER)		sound = Sound.CREEPER_HISS;
		else if (damagee.getType() == EntityType.ENDER_DRAGON)	sound = Sound.ENDERDRAGON_GROWL;
		else if (damagee.getType() == EntityType.ENDERMAN)		sound = Sound.ENDERMAN_HIT;
		else if (damagee.getType() == EntityType.GHAST)			sound = Sound.GHAST_SCREAM;
		else if (damagee.getType() == EntityType.GIANT)			sound = Sound.ZOMBIE_HURT;
		//else if (damagee.getType() == EntityType.HORSE)		sound = Sound.
		else if (damagee.getType() == EntityType.IRON_GOLEM)	sound = Sound.IRONGOLEM_HIT;
		else if (damagee.getType() == EntityType.MAGMA_CUBE)	sound = Sound.MAGMACUBE_JUMP;
		else if (damagee.getType() == EntityType.MUSHROOM_COW)	sound = Sound.COW_HURT;
		else if (damagee.getType() == EntityType.OCELOT)		sound = Sound.CAT_MEOW;
		else if (damagee.getType() == EntityType.PIG)			sound = Sound.PIG_IDLE;
		else if (damagee.getType() == EntityType.PIG_ZOMBIE)	sound = Sound.ZOMBIE_HURT;
		else if (damagee.getType() == EntityType.SHEEP)			sound = Sound.SHEEP_IDLE;
		else if (damagee.getType() == EntityType.SILVERFISH)	sound = Sound.SILVERFISH_HIT;
		else if (damagee.getType() == EntityType.SKELETON)		sound = Sound.SKELETON_HURT;
		else if (damagee.getType() == EntityType.SLIME)			sound = Sound.SLIME_ATTACK;
		else if (damagee.getType() == EntityType.SNOWMAN)		sound = Sound.STEP_SNOW;
		else if (damagee.getType() == EntityType.SPIDER)		sound = Sound.SPIDER_IDLE;
		//else if (damagee.getType() == EntityType.SQUID)		sound = Sound;
		//else if (damagee.getType() == EntityType.VILLAGER)	sound = Sound;
		//else if (damagee.getType() == EntityType.WITCH)		sound = Sound.;
		else if (damagee.getType() == EntityType.WITHER)		sound = Sound.WITHER_HURT;
		else if (damagee.getType() == EntityType.WOLF)			sound = Sound.WOLF_HURT;
		else if (damagee.getType() == EntityType.ZOMBIE)		sound = Sound.ZOMBIE_HURT;	

		damagee.getWorld().playSound(damagee.getLocation(), sound, 1.5f + (float)(0.5f * Math.random()), 0.8f + (float)(0.4f * Math.random()));
	}
	
	static {
		putAll();
	}
	
	private static void putAll() {
		if (creatureMap.isEmpty()) {
			creatureMap.put("Bat", EntityType.BAT);
			creatureMap.put("Blaze", EntityType.BLAZE);
			creatureMap.put("Arrow", EntityType.ARROW);
			creatureMap.put("Cave Spider", EntityType.CAVE_SPIDER);
			creatureMap.put("Chicken", EntityType.CHICKEN);
			creatureMap.put("Cow", EntityType.COW);
			creatureMap.put("Creeper", EntityType.CREEPER);
			creatureMap.put("Ender Dragon", EntityType.ENDER_DRAGON);
			creatureMap.put("Enderman", EntityType.ENDERMAN);
			creatureMap.put("Ghast", EntityType.GHAST);
			creatureMap.put("Giant", EntityType.GIANT);
			creatureMap.put("Horse", EntityType.HORSE);
			creatureMap.put("Iron Golem", EntityType.IRON_GOLEM);
			creatureMap.put("Item", EntityType.DROPPED_ITEM);
			creatureMap.put("Magma Cube", EntityType.MAGMA_CUBE);
			creatureMap.put("Mooshroom", EntityType.MUSHROOM_COW);
			creatureMap.put("Ocelot", EntityType.OCELOT);
			creatureMap.put("Pig", EntityType.PIG);
			creatureMap.put("Pig Zombie", EntityType.PIG_ZOMBIE);
			creatureMap.put("Sheep", EntityType.SHEEP);
			creatureMap.put("Silverfish", EntityType.SILVERFISH);
			creatureMap.put("Skeleton", EntityType.SKELETON);
			creatureMap.put("Slime", EntityType.SLIME);
			creatureMap.put("Snowman", EntityType.SNOWMAN);
			creatureMap.put("Spider", EntityType.SPIDER);
			creatureMap.put("Squid", EntityType.SQUID);
			creatureMap.put("Villager", EntityType.VILLAGER);
			creatureMap.put("Witch", EntityType.WITCH);
			creatureMap.put("Wither", EntityType.WITHER);
			creatureMap.put("WitherSkull", EntityType.WITHER_SKULL);
			creatureMap.put("Wolf", EntityType.WOLF);
			creatureMap.put("Zombie", EntityType.ZOMBIE);
			creatureMap.put("Armor Stand", EntityType.ARMOR_STAND);
		}
	}
}
