package com.overlands.core.utils.search;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Util;

public class ItemSearcher {

	private static HashMap<Integer, HashMap<Byte, Entry<String, Boolean>>> _names;


	public static LinkedList<Entry<Material, Byte>> matchItem(Player caller, String items, boolean inform) {
		LinkedList<Entry<Material, Byte>> matchList = new LinkedList<Entry<Material, Byte>>();

		StringBuilder failList = new StringBuilder();
		for (String cur : items.split(",")) {
			Entry<Material, Byte> match = searchItem(caller, cur, inform);

			if (match != null)
				matchList.add(match);

			else
				failList.append(cur).append(" ");
		}

		if (inform && (failList.length() > 0)) {
			failList = new StringBuilder(failList.substring(0, failList.length() - 1));
			caller.sendMessage(MessageUtil.color("&cFalha na busca de item: " + failList));
		}

		return matchList;
	}

	@SuppressWarnings("deprecation")
	public static Entry<Material, Byte> searchItem(Player caller, String args, boolean inform) {
		LinkedList<Entry<Material, Byte>> matchList = new LinkedList<Entry<Material, Byte>>();

		for (Material cur : Material.values()) {
			if (cur.toString().equalsIgnoreCase(args))
				return new AbstractMap.SimpleEntry<Material, Byte>(cur, (byte) 0);

			if (cur.toString().toLowerCase().contains(args.toLowerCase()))
				matchList.add(new AbstractMap.SimpleEntry<Material, Byte>(cur, (byte) 0));

			String[] arg = args.split(":");

			int id = 0;
			try {
				if (arg.length > 0)
					id = Integer.parseInt(arg[0]);
			} catch (Exception e) {
				continue;
			}

			if (id != cur.getId())
				continue;

			byte data = 0;
			try {
				if (arg.length > 1)
					data = Byte.parseByte(arg[1]);
			} catch (Exception e) {
				continue;
			}

			return new AbstractMap.SimpleEntry<Material, Byte>(cur, data);
		}
		if (matchList.size() != 1) {
			if (!inform)
				return null;

			return null;
		}
		return matchList.get(0);
	}

	private static String clean(String string) {
		String out = "";
		String[] words = string.split("_");

		for (String word : words) {
			if (word.length() < 1)
				return "Inválido";

			out += word.charAt(0) + word.substring(1).toLowerCase() + " ";
		}

		return out.substring(0, out.length() - 1);
	}

	static {
		_names = new HashMap<>();

		for (int id = 0; id < 10000; id++) {
			@SuppressWarnings("deprecation")
			Material mat = Material.getMaterial(id);

			if (mat == null)
				continue;
			HashMap<Byte, Entry<String, Boolean>> variants = new HashMap<Byte, Entry<String, Boolean>>();
			_names.put(id, variants);

			for (byte data = 0; data < 50; data++) {
				try {
					String name = "";

					@SuppressWarnings("deprecation")
					ItemStack stack = new ItemStack(id, 1, data);
					if (CraftItemStack.asNMSCopy(stack) != null && CraftItemStack.asNMSCopy(stack).getName() != null)
						name = CraftItemStack.asNMSCopy(stack).getName();

					if (id == 140)
						name = "Flower Pot";

					if (name.isEmpty())
						name = clean(mat.toString());
					boolean duplicate = false;
					for (Entry<String, Boolean> cur : variants.values())
						if (cur.getKey().equalsIgnoreCase(name)) {
							duplicate = true;
							break;
						}

					if (duplicate)
						continue;

					variants.put(data,
							new AbstractMap.SimpleEntry<String, Boolean>(name, (mat.getMaxStackSize() == 1)));
				} catch (Exception e) {

				}
			}
		}
	}

	@SuppressWarnings("deprecation")
	public static String getName(ItemStack stack, boolean formatted) {
		if (stack == null)
			return "Unarmed";

		if (stack.getData() != null)
			return getName(stack.getTypeId(), stack.getData().getData(), formatted);
		else
			return getName(stack.getTypeId(), (byte) 0, formatted);
	}

	@SuppressWarnings("deprecation")
	public static String getName(Block block, boolean formatted) {
		return getName(block.getTypeId(), block.getData(), formatted);
	}

	@SuppressWarnings("deprecation")
	public static String getName(Material mat, byte data, boolean formatted) {
		return getName(mat.getId(), data, formatted);
	}

	@SuppressWarnings("deprecation")
	public static String itemToStr(ItemStack item) {
		String data = "0";
		if (item.getData() != null)
			data = item.getData().getData() + "";

		return item.getType() + ":" + item.getAmount() + ":" + item.getDurability() + ":" + data;
	}

	public static String getName(int id, byte data, boolean formatted) {
		String out = "";
		if (formatted)
			out = "" + Util.mItem;

		if (!_names.containsKey(id))
			return out + "Inválido";

		if (!_names.get(id).containsKey(data)) {
			if (_names.get(id).containsKey(0))
				return out + _names.get(id).get(0).getKey();

			for (Entry<String, Boolean> cur : _names.get(id).values())
				return cur.getKey();

			return out + "Inválido";
		}

		return out + _names.get(id).get(data).getKey();
	}
}
