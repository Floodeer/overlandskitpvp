package com.overlands.core.utils.particles;

import org.bukkit.Location;
import org.bukkit.util.Vector;

import com.overlands.core.utils.reflection.RefClass;
import com.overlands.core.utils.reflection.ReflectionUtils;
import com.overlands.core.utils.reflection.ReflectionUtils.ConstructorInvoker;
import com.overlands.core.utils.reflection.ReflectionUtils.MethodInvoker;

public class ColoredParticle {

	private static Class<?> enumParticleClass = ReflectionUtils.getMinecraftClass("EnumParticle");
    private static ConstructorInvoker particleConstructor;
    private static MethodInvoker getParticleMethod;
    private ColoredParticle.ParticleType type;
    private Location location;
    private double offsetX;
    private double offsetY;
    private double offsetZ;

    static {
        particleConstructor = ReflectionUtils.getConstructorZ(ReflectionUtils.getMinecraftClass("PacketPlayOutWorldParticles"), new Class[]{enumParticleClass, Boolean.TYPE, Float.TYPE, Float.TYPE, Float.TYPE, Float.TYPE, Float.TYPE, Float.TYPE, Float.TYPE, Integer.TYPE, int[].class});
        getParticleMethod = ReflectionUtils.getMethodZ(enumParticleClass, "a", String.class);
    }

    public ColoredParticle(ColoredParticle.ParticleType particle, Location loc, Vector vec) {
        this.type = particle;
        this.location = loc;
        this.offsetX = vec.getX();
        this.offsetY = vec.getY();
        this.offsetZ = vec.getZ();
    }

    public void send() {
        Object construtor = particleConstructor.invoke(new Object[]{getParticleMethod.invoke(enumParticleClass, this.type.getName()), true, Float.valueOf((float)this.location.getX()), Float.valueOf((float)this.location.getY()), Float.valueOf((float)this.location.getZ()), Float.valueOf((float)this.color(this.offsetX)), Float.valueOf((float)this.color(this.offsetY)), Float.valueOf((float)this.color(this.offsetZ)), Float.valueOf(1.0F), Integer.valueOf(0), new int[0]});
        RefClass.sendPacketNearby(this.location, construtor);
    }

    private double color(double d) {
        d = d <= 0.0D ? -1.0D:d;
        return d / 255.0D;
    }

    public static enum ParticleType {
        SPELL_MOB("mobSpell"),
        NOTE("note"),
        REDSTONE("reddust");

        private String name;

        private ParticleType(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }
    }
}
