package com.overlands.core.utils.command;

import java.util.Collection;
import java.util.List;

import org.bukkit.command.CommandSender;

import com.overlands.kitpvp.ranks.Rank;

public interface ICommand {

	void run(CommandSender sender, String[] args);

	Collection<String> getAliases();

	void setAliasUsed(String name);

	List<String> onTabComplete(CommandSender sender, String commandLabel, String[] args);

	Rank getRequiredRank();

	Rank[] getSpecificRanks();
}
