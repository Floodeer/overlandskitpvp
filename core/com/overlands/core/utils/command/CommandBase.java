package com.overlands.core.utils.command;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.ranks.Rank;

public abstract class CommandBase implements ICommand {

	private List<String> aliases;
	private Rank requiredRank;
	private Rank[] specificRank;
	private boolean consoleSender;
	private boolean isClient;
	protected String aliasUsed;

	public CommandBase(Rank requiredRank, String... aliases) {
		this.requiredRank = requiredRank;
		this.aliases = Arrays.asList(aliases);
		this.consoleSender = false;
	}

	public CommandBase(Rank requiredRank, Rank[] ranks, String... aliases) {
		this.requiredRank = requiredRank;
		this.aliases = Arrays.asList(aliases);
		this.specificRank = ranks;
		this.consoleSender = false;
	}

	public CommandBase(Rank requiredRank, boolean console, String... aliases) {
		this.requiredRank = requiredRank;
		this.aliases = Arrays.asList(aliases);
		this.consoleSender = console;
	}

	public CommandBase(Rank requiredRank, boolean console, Rank[] ranks, String... aliases) {
		this.requiredRank = requiredRank;
		this.aliases = Arrays.asList(aliases);
		this.specificRank = ranks;
		this.consoleSender = console;
	}
	
	@Override
	public Collection<String> getAliases() {
		return aliases;
	}

	@Override
	public void setAliasUsed(String alias) {
		aliasUsed = alias;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, String commandLabel, String[] args) {
		return null;
	}
	
	@Override
	public Rank getRequiredRank() {
		return requiredRank;
	}

	@Override
	public Rank[] getSpecificRanks() {
		return specificRank;
	}
	
	public boolean isConsoleSender() {
		return consoleSender;
	}
	
	protected List<String> getMatches(String start, List<String> possibleMatches) {
		List<String> matches = new ArrayList<String>();
		possibleMatches.forEach((possibleMatch) -> {
			if (possibleMatch.toLowerCase().startsWith(start.toLowerCase()))
				matches.add(possibleMatch);
		});
		return matches;
	}

	@SuppressWarnings("rawtypes")
	protected List<String> getMatches(String start, Enum[] numerators) {
		List<String> matches = new ArrayList<String>();

		for (Enum e : numerators) {
			String s = e.toString();
			if (s.toLowerCase().startsWith(start.toLowerCase()))
				matches.add(s);
		}

		return matches;
	}

	protected List<String> getPlayerMatches(Player sender, String start) {
		List<String> matches = new ArrayList<String>();

		Bukkit.getOnlinePlayers().forEach((player) -> {
			if (sender.canSee(player) && player.getName().toLowerCase().startsWith(start.toLowerCase())) {
				matches.add(player.getName());
			}
		});
		return matches;
	}
	
	protected void resetCommandCharge(Player caller) {
		Main.getEnergyManager().recharge(caller, "Command");
	}

	public boolean isClient() {
		return isClient;
	}

	public void setClient(boolean isClient) {
		this.isClient = isClient;
	}
}
