package com.overlands.core.utils.command;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.google.common.collect.Maps;
import com.overlands.kitpvp.ranks.Rank;

public abstract class MultiCommandBase extends CommandBase {

	protected Map<String, ICommand> commands;
	
	public MultiCommandBase(Rank requiredRank, String... aliases) {
		super(requiredRank, aliases);

		this.commands  = Maps.newHashMap();
	}
	
	public void addSimpleCommandBase(ICommand command) {
		for (String commandRoot : command.getAliases()) {
			commands.put(commandRoot, command);
		}
	}

	@Override
	public void run(CommandSender caller, String[] args) {
		String commandName = null;
		String[] newArgs = null;

		if (args != null && args.length > 0) {
			commandName = args[0];

			if (args.length > 1) {
				newArgs = new String[args.length - 1];

				for (int i = 0; i < newArgs.length; i++) {
					newArgs[i] = args[i + 1];
				}
			}
		}

		ICommand command = commands.get(commandName);

		if (command != null && Rank.hasRank((Player)caller, getRequiredRank()) || (getSpecificRanks() != null && Rank.hasRank((Player)caller, getSpecificRanks()))) {
			command.setAliasUsed(commandName);

			command.run(caller, newArgs);
		} else {
			help(caller, args);
		}
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, String commandLabel, String[] args) {
		if (args.length == 1) {
			List<String> possibleMatches = new ArrayList<String>();

			for (ICommand command : commands.values()) {
				possibleMatches.addAll(command.getAliases());
			}

			return getMatches(args[0], possibleMatches);
		} else if (args.length > 1) {
			String commandName = args[0];

			ICommand command = commands.get(commandName);

			if (command != null) {
				return command.onTabComplete(sender, commandLabel, args);
			}
		}
		return null;
	}
	
	protected abstract void help(CommandSender caller, String[] args);

}
