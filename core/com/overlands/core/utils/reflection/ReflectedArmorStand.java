package com.overlands.core.utils.reflection;

import java.util.Map;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.EulerAngle;

import com.google.common.collect.Maps;
import com.overlands.core.utils.calc.MathUtils;

import net.minecraft.server.v1_8_R3.PacketPlayOutEntityEquipment;

public class ReflectedArmorStand {
	


	 //1.8
     static Class<?> craftArmorStandClass = ReflectionUtils.getClass("{obc}.entity.CraftArmorStand");
     static Class<?> nmsItemStack = ReflectionUtils.getClass("{nms}.ItemStack");
     
     private static PacketPlayOutEntityEquipment equip;
     
     //>1.8
	 static Class<?> craftItemStackClass = ReflectionUtils.getClass("{obc}.inventory.CraftItemStack");
     static Class<?> packetTeleportClass = ReflectionUtils.getMinecraftClass("PacketPlayOutEntityTeleport");
     static Class<?> packetRotationClass = ReflectionUtils.getMinecraftClass("PacketPlayOutEntity$PacketPlayOutEntityLook");
     static Class<?> nmsWorldField = ReflectionUtils.getClass("{nms}.World");
     static Class<?> packetSpawnEntityLivingClass = ReflectionUtils.getClass("{nms}.PacketPlayOutSpawnEntityLiving");
     static Class<?> entityArmorStandClass = ReflectionUtils.getClass("{nms}.EntityArmorStand");

     
     static Class<?> entityClass = ReflectionUtils.getClass("{nms}.Entity");
     static Class<?> entityLivingClass = ReflectionUtils.getClass("{nms}.EntityLiving");
     static Class<?> itemStackClass = ReflectionUtils.getMinecraftClass("ItemStack");
     static Class<?> vector3fClass = ReflectionUtils.getMinecraftClass("Vector3f");
     static ReflectionUtils.MethodInvoker getIdMethod;
     static ReflectionUtils.MethodInvoker asNMSCopyMethod;
     static ReflectionUtils.MethodInvoker getHandleWorldMethod;
     static ReflectionUtils.MethodInvoker setPositionMethod;
     static ReflectionUtils.MethodInvoker setCustomeNameMethod;
     static ReflectionUtils.MethodInvoker setCustomeNameVisibleMethod;
     static ReflectionUtils.MethodInvoker setInvisibleMethod;
     static ReflectionUtils.MethodInvoker setSmallMethod;
     static ReflectionUtils.MethodInvoker setHeadPoseMethod;
     static ReflectionUtils.MethodInvoker setArmsMethod;
     static ReflectionUtils.MethodInvoker setMarkerMethod;
     static ReflectionUtils.MethodInvoker setEquipMethod;
     static ReflectionUtils.ConstructorInvoker armorStandConstructor;
     static ReflectionUtils.ConstructorInvoker packetSpawnConstructor;
     static ReflectionUtils.ConstructorInvoker packetRotationContrustuctor;
     static ReflectionUtils.ConstructorInvoker packetTeleportContrustuctor;
     static ReflectionUtils.ConstructorInvoker vector3fConstructor;
     
     static ReflectionUtils.FieldAccessor<?> IDPacketEquipField;
     static ReflectionUtils.FieldAccessor<?> itemStackPacketEquipField;
     
     Object packetSpawn;
     Object packetRotation;
     int ID;
     Location location;
     String displayName = "";
     boolean visible = true;
     boolean small = false;
     boolean arms = false;
     boolean marker = false;
     float yaw = 0.0F;
     float pitch = 0.0F;
     Object armorStand;
     
     private Map<Integer, String> lines;

    public ReflectedArmorStand(Location location) {
  
    	 getIdMethod = ReflectionUtils.getMethodZ(entityClass, "getId", new Class[0]);
         asNMSCopyMethod = ReflectionUtils.getMethodZ(craftItemStackClass, "asNMSCopy", ItemStack.class);
         getHandleWorldMethod = ReflectionUtils.getMethodZ("{obc}.CraftWorld", "getHandle", new Class[0]);
         setPositionMethod = ReflectionUtils.getMethodZ(entityClass, "setLocation", double.class, double.class, double.class, float.class, float.class);
         setCustomeNameMethod = ReflectionUtils.getMethodZ(entityArmorStandClass, "setCustomName", String.class);
         setCustomeNameVisibleMethod = ReflectionUtils.getMethodZ(entityArmorStandClass, "setCustomNameVisible", boolean.class);
         setInvisibleMethod = ReflectionUtils.getMethodZ(entityArmorStandClass, "setInvisible", boolean.class);
         setSmallMethod = ReflectionUtils.getMethodZ(entityArmorStandClass, "setSmall", boolean.class);
         setHeadPoseMethod = ReflectionUtils.getMethodZ(entityArmorStandClass, "setHeadPose", vector3fClass);
         setArmsMethod = ReflectionUtils.getMethodZ(entityArmorStandClass, "setArms", boolean.class);
         setMarkerMethod = ReflectionUtils.getMethodZ(craftArmorStandClass, "setMarker", boolean.class);
         
         armorStandConstructor = ReflectionUtils.getConstructorZ(entityArmorStandClass, nmsWorldField);
         packetSpawnConstructor = ReflectionUtils.getConstructorZ(packetSpawnEntityLivingClass, entityLivingClass);
         packetRotationContrustuctor = ReflectionUtils.getConstructorZ(packetRotationClass, int.class, byte.class, byte.class, boolean.class);
         packetTeleportContrustuctor = ReflectionUtils.getConstructorZ(packetTeleportClass, entityClass);
         vector3fConstructor = ReflectionUtils.getConstructorZ(vector3fClass, float.class, float.class, float.class);
         
        this.location = location;
        this.armorStand = armorStandConstructor.invoke(new Object[]{getHandleWorldMethod.invoke(location.getWorld(), new Object[0])});
        setPositionMethod.invoke(this.armorStand, new Object[]{location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch()});
        this.packetSpawn = packetSpawnConstructor.invoke(new Object[]{this.armorStand});
        this.ID = ((Integer)getIdMethod.invoke(this.armorStand, new Object[0])).intValue();
        
        lines = Maps.newHashMap();
    }

    public ReflectedArmorStand setVisible(boolean visible) {
        setInvisibleMethod.invoke(this.armorStand, !visible);
        this.visible = visible;
        return this;
    }

    public ReflectedArmorStand setSmall(boolean small) {
        setSmallMethod.invoke(this.armorStand, small);
        this.small = small;
        return this;
    }

    public ReflectedArmorStand setHeadPose(EulerAngle angle) {
        EulerAngle ea = this.toNMS(angle);
        setHeadPoseMethod.invoke(this.armorStand, new Object[]{vector3fConstructor.invoke(new Object[]{Float.valueOf((float)ea.getX()), Float.valueOf((float)ea.getY()), Float.valueOf((float)ea.getZ())})});
        return this;
    }

    public ReflectedArmorStand setRotation(float yaw, float pitch) {
        this.packetRotation = packetRotationContrustuctor.invoke(new Object[]{Integer.valueOf(this.ID), Byte.valueOf(MathUtils.toPackedByte(yaw)), Byte.valueOf(MathUtils.toPackedByte(pitch)), Boolean.valueOf(false)});
        this.yaw = yaw;
        this.pitch = pitch;
        return this;
    }

    
    public ReflectedArmorStand setEquipment(int slot, ItemStack item) {
         equip = new PacketPlayOutEntityEquipment(this.ID, slot, CraftItemStack.asNMSCopy(item));
         return this;
    }

    public ReflectedArmorStand setDisplayName(String displayName) {
        setCustomeNameMethod.invoke(this.armorStand, new Object[]{displayName});
        setCustomeNameVisibleMethod.invoke(this.armorStand, new Object[]{Boolean.valueOf(true)});
        this.displayName = displayName;
        return this;
    }

    public ReflectedArmorStand setMarker(boolean marker) {
        setMarkerMethod.invoke(this.armorStand, new Object[]{Boolean.valueOf(marker)});
        this.marker = marker;
        return this;
    }

    public ReflectedArmorStand setArms(boolean arms) {
        setArmsMethod.invoke(this.armorStand, new Object[]{Boolean.valueOf(arms)});
        this.arms = arms;
        return this;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public int getID() {
        return this.ID;
    }

    public boolean isVisible() {
        return this.visible;
    }

    public boolean isSmall() {
        return this.small;
    }

    public void teleport(Player player, Location newLocation) {
        setPositionMethod.invoke(this.armorStand, new Object[]{Double.valueOf(newLocation.getX()), Double.valueOf(newLocation.getY()), Double.valueOf(newLocation.getZ()), Float.valueOf(this.yaw), Float.valueOf(this.pitch)});
        Object packetTeleport = packetTeleportContrustuctor.invoke(new Object[]{this.armorStand});
        RefClass.sendPacket(player, new Object[]{packetTeleport});
        this.location = newLocation;
    }
    
    public void teleport(Location newLocation) {
        setPositionMethod.invoke(this.armorStand, newLocation.getX(), newLocation.getY(), newLocation.getZ(), this.yaw, this.pitch);
        Object packetTeleport = packetTeleportContrustuctor.invoke(this.armorStand);
        RefClass.sendPacket(packetTeleport);
        this.location = newLocation;
    }

    public void remove() {
        RefClass.destroyEntity(new int[]{this.ID});
    }

    public ReflectedArmorStand spawnArmorStand() {
        RefClass.sendPacketNearby(this.location, new Object[]{this.packetSpawn, this.packetRotation});
        if(equip != null)
        	  RefClass.sendPacketNearby(this.location, equip);
        return this;
    }


    public ReflectedArmorStand spawnArmorStand(Player player) {
        RefClass.sendPacket(player, new Object[]{this.packetSpawn, this.packetRotation});
        return this;
    }
    
    public void setLine(int l, String str) {
    	this.lines.put(l, str);
    }
    
    public void putAll(Map<Integer, String> l) {
    	this.lines = l;
    }

     EulerAngle toNMS(EulerAngle old) {
        return new EulerAngle((double)((float)Math.toDegrees(old.getX())), (double)((float)Math.toDegrees(old.getY())), (double)((float)Math.toDegrees(old.getZ())));
    } 
}
