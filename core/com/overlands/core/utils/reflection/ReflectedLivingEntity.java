package com.overlands.core.utils.reflection;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.reflection.ReflectionUtils.ConstructorInvoker;
import com.overlands.core.utils.reflection.ReflectionUtils.MethodInvoker;

public class ReflectedLivingEntity {

    private static Class<?> packetRotationPitchClass = ReflectionUtils.getMinecraftClass("PacketPlayOutEntity$PacketPlayOutEntityLook");
    private static Class<?> packetSpawnEntityLivingClass = ReflectionUtils.getClass("{nms}.PacketPlayOutSpawnEntityLiving");
    private static Class<?> packetRotationYawClass = ReflectionUtils.getMinecraftClass("PacketPlayOutEntityHeadRotation");
    private static Class<?> nmsWorldClass = ReflectionUtils.getClass("{nms}.World");
    private static Class<?> entityClass = ReflectionUtils.getClass("{nms}.Entity");
    private static Class<?> entityLivingClass = ReflectionUtils.getClass("{nms}.EntityLiving");
    private static Class<?> packetTeleportClass = ReflectionUtils.getMinecraftClass("PacketPlayOutEntityTeleport");
    private static Class<?> entityAgeableClass = ReflectionUtils.getClass("{nms}.EntityAgeable");
    private Class<?> entityTypeClass;
    private static ConstructorInvoker packetSpawnConstructor;
    private static ConstructorInvoker packetRotationPitchContrustuctor;
    private static ConstructorInvoker packetRotationYawContrustuctor;
    private static ConstructorInvoker packetTeleportContrustuctor;
    private static MethodInvoker getHandleWorldMethod;
    private static MethodInvoker setPositionMethod;
    private static MethodInvoker getIdMethod;
    private static MethodInvoker getBukkitEntityMethod;
    private static MethodInvoker setInvisibleMethod;
    private static MethodInvoker setAgeMethod;
    private List<Object> packets;
    private Location location;
    private Object livingEntity;
    private Entity entityBukkit;
    private EntityType entityType;
    private boolean isSpawned;
    private int ID;
    private float yaw = 0.0F;
    private float pitch = 0.0F;
    private Object packetSpawn;
    private Object packetYawRotation;
    private Object packetPitchRotation;

    static {
        packetSpawnConstructor = ReflectionUtils.getConstructorZ(packetSpawnEntityLivingClass, new Class[]{entityLivingClass});
        packetRotationPitchContrustuctor = ReflectionUtils.getConstructorZ(packetRotationPitchClass, new Class[]{Integer.TYPE, Byte.TYPE, Byte.TYPE, Boolean.TYPE});
        packetRotationYawContrustuctor = ReflectionUtils.getConstructorZ(packetRotationYawClass, new Class[]{entityClass, Byte.TYPE});
        packetTeleportContrustuctor = ReflectionUtils.getConstructorZ(packetTeleportClass, new Class[]{entityClass});
        getHandleWorldMethod = ReflectionUtils.getMethodZ("{obc}.CraftWorld", "getHandle", new Class[0]);
        setPositionMethod = ReflectionUtils.getMethodZ(entityClass, "setLocation", new Class[]{Double.TYPE, Double.TYPE, Double.TYPE, Float.TYPE, Float.TYPE});
        getIdMethod = ReflectionUtils.getMethodZ(entityClass, "getId", new Class[0]);
        getBukkitEntityMethod = ReflectionUtils.getMethodZ(entityClass, "getBukkitEntity", new Class[0]);
        setInvisibleMethod = ReflectionUtils.getMethodZ(entityClass, "setInvisible", new Class[]{Boolean.TYPE});
        setAgeMethod = ReflectionUtils.getMethodZ(entityAgeableClass, "setAge", new Class[]{Integer.TYPE});
    }

    public ReflectedLivingEntity(Location var1, EntityType var2) {
        this.location = var1;
        this.entityType = var2;
        this.entityTypeClass = ReflectionUtils.getMinecraftClass("Entity" + var2.getEntityClass().getSimpleName());
        ReflectionUtils.ConstructorInvoker var3 = ReflectionUtils.getConstructorZ(ReflectionUtils.getMinecraftClass("Entity" + var2.getEntityClass().getSimpleName()), new Class[]{nmsWorldClass});
        this.livingEntity = var3.invoke(new Object[]{getHandleWorldMethod.invoke(var1.getWorld(), new Object[0])});
        setPositionMethod.invoke(this.livingEntity, new Object[]{var1.getX(), var1.getY(), var1.getZ(), var1.getYaw(), var1.getPitch()});
        this.ID = (Integer)getIdMethod.invoke(this.livingEntity, new Object[0]);
        this.packetSpawn = packetSpawnConstructor.invoke(new Object[]{this.livingEntity});
        this.entityBukkit = (Entity)getBukkitEntityMethod.invoke(this.livingEntity, new Object[0]);
    }

    public void setSize(int var1) {
        if (this.entityType == EntityType.SLIME || this.entityType == EntityType.MAGMA_CUBE) {
            String var2 = this.getPackageName();
            ReflectionUtils.MethodInvoker var3;
            if (!var2.equalsIgnoreCase("v1_11_R1") && !var2.equalsIgnoreCase("v1_11_R1") && !var2.equalsIgnoreCase("v1_12_R1") && !var2.equalsIgnoreCase("v1_12_R2")) {
                var3 = ReflectionUtils.getMethodZ(this.entityTypeClass, "setSize", new Class[]{Integer.TYPE});
                var3.invoke(this.livingEntity, new Object[]{var1});
            } else {
                var3 = ReflectionUtils.getMethodZ(this.entityTypeClass, "setSize", new Class[]{Integer.TYPE, Boolean.TYPE});
                var3.invoke(this.livingEntity, new Object[]{var1, false});
            }
        }

    }

    public void setBaby() {
        if (entityAgeableClass.isAssignableFrom(this.entityTypeClass)) {
            setAgeMethod.invoke(this.livingEntity, new Object[]{-1});
        }

    }

    public void setVisible(boolean var1) {
        setInvisibleMethod.invoke(this.livingEntity, new Object[]{!var1});
    }

    public ReflectedLivingEntity teleport(Location var1, float var2, float var3) {
        setPositionMethod.invoke(this.livingEntity, new Object[]{var1.getX(), var1.getY(), var1.getZ(), var2, var3});
        Object var4 = packetTeleportContrustuctor.invoke(new Object[]{this.livingEntity});
        RefClass.sendPacketNearby(var1, new Object[]{var4});
        this.location = var1;
        return this;
    }

    public ReflectedLivingEntity setYawRotation(float var1) {
        this.packetYawRotation = packetRotationYawContrustuctor.invoke(new Object[]{this.livingEntity,  MathUtils.toPackedByte(var1)});
        if (this.isSpawned) {
            RefClass.sendPacketNearby(this.location, new Object[]{this.packetYawRotation});
        }

        this.yaw = var1;
        return this;
    }

    public ReflectedLivingEntity setPitchRotation(float var1) {
        this.packetPitchRotation = packetRotationPitchContrustuctor.invoke(new Object[]{this.ID, MathUtils.toPackedByte(this.yaw), MathUtils.toPackedByte(var1), false});
        if (this.isSpawned) {
            RefClass.sendPacketNearby(this.location, new Object[]{this.packetPitchRotation});
        }

        this.pitch = var1;
        return this;
    }

    public ReflectedLivingEntity spawnLivingEntity() {
        this.packets = RefClass.sendConstantPacket(new Object[]{this.packetSpawn, this.packetYawRotation, this.packetPitchRotation});
        this.isSpawned = true;
        return this;
    }

    public ReflectedLivingEntity spawnLivingEntity(Player var1) {
        RefClass.sendPacket(var1, new Object[]{this.packetSpawn, this.packetYawRotation, this.packetPitchRotation});
        this.isSpawned = true;
        return this;
    }

    private String getPackageName() {
        String var1 = Bukkit.getServer().getClass().getPackage().getName();
        return var1.substring(var1.lastIndexOf(46) + 1);
    }

    public float getYaw() {
        return this.yaw;
    }

    public float getPitch() {
        return this.pitch;
    }

    public void removeLivingEntity() {
        RefClass.destroyEntity(new int[]{this.ID});
        if (this.packets != null) {
            RefClass.getPacketsToSend().removeAll(this.packets);
        }
    }

    public Entity getBukkitEntity() {
        return this.entityBukkit;
    }

    public Location getLocation() {
        return this.location;
    }

    public Object getLivingEntity() {
        return this.livingEntity;
    }

    public int getID() {
        return this.ID;
    }
}
