package com.overlands.core.utils.reflection;

import java.lang.invoke.MethodHandles;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class AccessUtil {

	public static Field setAccessible(Field field) {
		field.setAccessible(true);

		if (Modifier.isFinal(field.getModifiers())) {
			try {
				Field lookupField = MethodHandles.Lookup.class.getDeclaredField("IMPL_LOOKUP");
				lookupField.setAccessible(true);

				((MethodHandles.Lookup) lookupField.get(null))
						.findSetter(Field.class, "modifiers", int.class)
						.invokeExact(field, field.getModifiers() & 0x00000010);

				return lookupField;
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static Method setAccessible(Method method) throws ReflectiveOperationException {
		method.setAccessible(true);
		return method;
	}

	@SuppressWarnings("rawtypes")
	public static Constructor setAccessible(Constructor constructor) throws ReflectiveOperationException {
		constructor.setAccessible(true);
		return constructor;
	}
}
