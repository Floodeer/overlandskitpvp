package com.overlands.core.utils.reflection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.IronGolem;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import com.mojang.authlib.GameProfile;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.reflection.ReflectionUtils.ConstructorInvoker;
import com.overlands.core.utils.reflection.ReflectionUtils.FieldAccessor;
import com.overlands.core.utils.reflection.ReflectionUtils.MethodInvoker;

public class RefClass {
	
	static private List<Object> packetsToSend = new ArrayList<>();
	static private Map<UUID, List<Object>> packetsToSendPlayer = new HashMap<>();
	static private Map<UUID, List<Object>> packetsToSendToOthers = new HashMap<>();
	 
    static private Class<?> entityClass = ReflectionUtils.getClass("{nms}.Entity");
    static private Class<?> minecraftServerClass = ReflectionUtils.getClass("{nms}.MinecraftServer");
    static private Class<?> packetDestroyClass = ReflectionUtils.getMinecraftClass("PacketPlayOutEntityDestroy");
	
	static private FieldAccessor<?> playerConnectionField = ReflectionUtils.getField("{nms}.EntityPlayer", "playerConnection", Object.class);
    static private MethodInvoker getHandleWorldMethod = ReflectionUtils.getMethodZ("{obc}.CraftWorld", "getHandle", new Class[0]);
    
    static private Class<?> packetRotationPitchClass;
    static private Class<?> packetRotationYawClass;
    
    static private MethodInvoker getHandleMethod;
    static private MethodInvoker getServerMethod;
    static private MethodInvoker setInvisibleMethod;
    static private MethodInvoker setSilentMethod;
    static private MethodInvoker setPositionMethod;
    static private MethodInvoker sendPacket;
    static private MethodInvoker broadcastentityeffectMethod;
    static private MethodInvoker getIdMethod;
    
    static private ConstructorInvoker packetRotationPitchContrustuctor;
    static private ConstructorInvoker packetRotationYawContrustuctor;
    static private ConstructorInvoker packetSpawnContrustuctor;
    static private ConstructorInvoker packetStatusContrustuctor;
    static private ConstructorInvoker dragonContrustuctor;
	static private ConstructorInvoker packetDestroyConstructor;
	
	static private FieldAccessor<?> worldField;
    static private FieldAccessor<?> noClipField;
    static private FieldAccessor<?> recentTps;
    static private FieldAccessor<?> lastX;
    static private FieldAccessor<?> lastY;
    static private FieldAccessor<?> lastZ;
	static private FieldAccessor<?> packetDestroyIDField;
	
    static {
	    packetDestroyIDField = ReflectionUtils.getField(packetDestroyClass, Object.class, 0);
	    sendPacket = ReflectionUtils.getMethodZ("{nms}.PlayerConnection", "sendPacket", ReflectionUtils.getMinecraftClass("Packet"));
	    packetDestroyConstructor = ReflectionUtils.getConstructorZ(packetDestroyClass);
        noClipField = ReflectionUtils.getField(ReflectionUtils.getMinecraftClass("Entity"), "noclip", boolean.class);
        getHandleMethod = ReflectionUtils.getMethodZ("{obc}.entity.CraftEntity", "getHandle");
        getServerMethod = ReflectionUtils.getMethodZ(minecraftServerClass, "getServer");
        setInvisibleMethod = ReflectionUtils.getMethodZ(entityClass, "setInvisible", boolean.class);
        setPositionMethod = ReflectionUtils.getMethodZ(entityClass, "setLocation", double.class, double.class, double.class, float.class, float.class);
        worldField = ReflectionUtils.getField(entityClass, ReflectionUtils.getMinecraftClass("World"), 0);
        broadcastentityeffectMethod = ReflectionUtils.getMethodZ(ReflectionUtils.getMinecraftClass("World"), "broadcastEntityEffect", entityClass, byte.class);
        packetRotationPitchClass = ReflectionUtils.getMinecraftClass("PacketPlayOutEntity$PacketPlayOutEntityLook");
        packetRotationPitchContrustuctor = ReflectionUtils.getConstructorZ(packetRotationPitchClass, int.class, byte.class, byte.class, boolean.class);
        packetRotationYawClass = ReflectionUtils.getMinecraftClass("PacketPlayOutEntityHeadRotation");
        packetRotationYawContrustuctor = ReflectionUtils.getConstructorZ(packetRotationYawClass, entityClass, byte.class);
        packetSpawnContrustuctor = ReflectionUtils.getConstructorZ(ReflectionUtils.getMinecraftClass("PacketPlayOutSpawnEntityLiving"), ReflectionUtils.getMinecraftClass("EntityLiving"));
        packetStatusContrustuctor = ReflectionUtils.getConstructorZ(ReflectionUtils.getMinecraftClass("PacketPlayOutEntityStatus"), entityClass, byte.class);
        dragonContrustuctor = ReflectionUtils.getConstructorZ(ReflectionUtils.getMinecraftClass("EntityEnderDragon"), ReflectionUtils.getMinecraftClass("World"));
        getIdMethod = ReflectionUtils.getMethodZ(entityClass, "getId");
        recentTps = ReflectionUtils.getField(minecraftServerClass, "recentTps", double[].class);
        lastX = ReflectionUtils.getField(entityClass, "lastX", double.class);
        lastY = ReflectionUtils.getField(entityClass, "lastY", double.class);
        lastZ = ReflectionUtils.getField(entityClass, "lastZ", double.class);

        try {
            setSilentMethod = ReflectionUtils.getMethodZ(ReflectionUtils.getMinecraftClass("Entity"), "setSilent", boolean.class);
        } catch (IllegalStateException var0) {
            setSilentMethod = null;
        }
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<Object> sendConstantPacketExclude(Collection<? extends Player> var0, Player var1, Object... var2) {
        packetsToSendToOthers.putIfAbsent(var1.getUniqueId(), new ArrayList<>());
        Object[] var6 = var2;
        int var5 = var2.length;

        for(int var4 = 0; var4 < var5; ++var4) {
            Object var3 = var6[var4];
            if (var3 != null) {
                var0.stream().filter((var1x) -> {
                    return !var1x.equals(var1);
                }).forEach((var2x) -> {
                    sendPacket.invoke(playerConnectionField.get(getHandleMethod.invoke(var2x, new Object[0])), new Object[]{var3});
                    ((List)packetsToSendToOthers.get(var1.getUniqueId())).add(var3);
                });
            }
        }
        return (List)packetsToSendToOthers.get(var1);
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static List<Object> sendConstantPacket(Object... var0) {
        sendPacket(var0);
        addPacket(var0);
        return new ArrayList(Arrays.asList(var0));
    }

    public static void setNoClip(Entity var0, boolean var1) {
        noClipField.set(getHandleMethod.invoke(var0, new Object[0]), Boolean.valueOf(var1));
    }

    public static void broadcastEntityEffect(Entity var0, byte var1) {
        Object var2 = worldField.get(getHandleMethod.invoke(var0, new Object[0]));
        broadcastentityeffectMethod.invoke(var2, new Object[]{getHandleMethod.invoke(var0, new Object[0]), Byte.valueOf(var1)});
    }

    public static void ironGolemAttack(IronGolem var0) {
        Object var1 = worldField.get(getHandleMethod.invoke(var0, new Object[0]));
        broadcastentityeffectMethod.invoke(var1, new Object[]{getHandleMethod.invoke(var0, new Object[0]), (byte)4});
    }

    public static void setPitchHeadRotation(Entity var0, float var1) {
        Object var2 = packetRotationPitchContrustuctor.invoke(new Object[]{getIdMethod.invoke(getHandleMethod.invoke(var0, new Object[0]), new Object[0]), MathUtils.toPackedByte(var0.getLocation().getYaw()), MathUtils.toPackedByte(var1), (byte)4});
        RefClass.sendPacketNearby(var0.getLocation(), new Object[]{var2});
    }

    public static void setYawRotation(Entity var0, float var1) {
        Object var2 = packetRotationYawContrustuctor.invoke(new Object[]{getHandleMethod.invoke(var0, new Object[0]), MathUtils.toPackedByte(var1)});
        RefClass.sendPacketNearby(var0.getLocation(), new Object[]{var2});
    }

    public static Vector getLastLocation(Entity var0) {
        Object var1 = getHandleMethod.invoke(var0, new Object[0]);
        return new Vector(((Double)lastX.get(var1)).doubleValue(), ((Double)lastY.get(var1)).doubleValue(), ((Double)lastZ.get(var1)).doubleValue());
    }

    public static int spawnDragonEffect(Location location) {
    	    Object dragon = dragonContrustuctor.invoke(getHandleWorldMethod.invoke(location.getWorld()));
    	    setPositionMethod.invoke(dragon, location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());
    	    Object packetSpawn = packetSpawnContrustuctor.invoke(dragon);
    	    Object packetStatus = packetStatusContrustuctor.invoke(dragon, (byte)3);
    	    sendPacket(packetSpawn, packetStatus);

    	    return  (int)getIdMethod.invoke(dragon);
    }

    public static void setInvisible(Entity var0, boolean var1) {
        setInvisibleMethod.invoke(getHandleMethod.invoke(var0, new Object[0]), new Object[]{Boolean.valueOf(var1)});
    }

    public static double getTPS() {
        Object var0 = getServerMethod.invoke(minecraftServerClass, new Object[0]);
        double[] var1 = (double[])recentTps.get(var0);
        return var1[0];
    }

    public static void setSilent(Entity var0) {
        if(setSilentMethod != null) {
            setSilentMethod.invoke(getHandleMethod.invoke(var0, new Object[0]), new Object[]{true});
        }
    }
    
    public static GameProfile extractGameProfile(Player p) {
		Object handle = getHandleMethod.invoke(p, new Object[0]);
		MethodInvoker m = ReflectionUtils.getMethodZ("{nms}.EntityHuman", "getProfile", new Class[0]);
		return (GameProfile) m.invoke(handle, new Object[0]);
	}

	public static void sendPacket(Player player, Object... packets) {
		for (Object packet : packets) {
			sendPacket.invoke(playerConnectionField.get(getHandleMethod.invoke(player, new Object[0])), packet);
		}
	}

	public static void sendPacketNearby(Location from, Object... packets) {
		Bukkit.getOnlinePlayers().forEach((player) -> {
			RefClass.sendPacket(player, packets);
		});
	}
	
	public static void sendPacketNearby(Location from, net.minecraft.server.v1_8_R3.Packet<?> packets) {
		Bukkit.getOnlinePlayers().forEach((player) -> {
			((CraftPlayer)player).getHandle().playerConnection.sendPacket(packets);
		});
	}

	public static void sendPacket(Object... packets) {
		for(Object packet : packets) {
			Bukkit.getOnlinePlayers().forEach((player) -> {
				sendPacket.invoke(playerConnectionField.get(getHandleMethod.invoke(player, new Object[0])), packet);
			});
		}
	}

	public static void destroyEntity(int... id) {
		Object packet = packetDestroyConstructor.invoke(new Object[0]);
		packetDestroyIDField.set(packet, id);
		sendPacket(new Object[] { packet });
	}

	public static void destroyEntity(Player player, int... id) {
		Object packet = packetDestroyConstructor.invoke(new Object[0]);
		packetDestroyIDField.set(packet, id);
		sendPacket(player, new Object[] { packet });
	}

	  public static void addPacket(Object... o) {
	        packetsToSend.addAll(Arrays.asList(o));
	    }

	public static void removeKeyPacket(Object key) {
		packetsToSend.remove(key);
	}

    public static Map<UUID, List<Object>> getPacketsToSendToOthers() {
        return packetsToSendToOthers;
    }

    public static List<Object> getPacketsToSend() {
        return packetsToSend;
    }

    public static Map<UUID, List<Object>> getPacketsToSendPlayer() {
        return packetsToSendPlayer;
    }
}