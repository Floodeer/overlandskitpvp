package com.overlands.core.utils;

import java.util.List;
import java.util.Random;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftFirework;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.scheduler.BukkitRunnable;

import com.overlands.kitpvp.Main;

public class FireworkUtils {

	private static final int RGB_MAX = 255;

	private static Random r = new Random();

	public static void spawnRandomFirework(Location location) {
		FireworkEffect fe = getRandomFireworkEffect();

		org.bukkit.entity.Firework fw = (org.bukkit.entity.Firework) location.getWorld().spawnEntity(location,
				EntityType.FIREWORK);
		FireworkMeta fm = fw.getFireworkMeta();

		fm.addEffect(fe);

		int rp = r.nextInt(2) + 1;
		fm.setPower(rp);

		fw.setFireworkMeta(fm);

	}

	public static FireworkEffect getRandomFireworkEffect() {
		Color color = getRandomBukkitColor();
		Color fade = getRandomBukkitColor();

		int rt = r.nextInt(4) + 1;
		FireworkEffect.Type type;
		switch (rt) {
		case 2:
			type = FireworkEffect.Type.BALL_LARGE;
			break;
		case 3:
			type = FireworkEffect.Type.BURST;
			break;
		case 4:
			type = FireworkEffect.Type.CREEPER;
			break;
		case 5:
			type = FireworkEffect.Type.STAR;
			break;
		default:
			type = FireworkEffect.Type.BALL;
		}
		if (rt == 1)
			type = FireworkEffect.Type.BALL;
		if (rt == 2)
			type = FireworkEffect.Type.BALL_LARGE;
		if (rt == 3)
			type = FireworkEffect.Type.BURST;
		if (rt == 4)
			type = FireworkEffect.Type.CREEPER;

		return FireworkEffect.builder().flicker(r.nextBoolean()).withColor(color).withFade(fade).with(type)
				.trail(r.nextBoolean()).build();
	}

	public static Color getRandomColor() {
		return Color.fromRGB(r.nextInt(RGB_MAX), r.nextInt(RGB_MAX), r.nextInt(RGB_MAX));
	}

	public static Color getRandomBukkitColor() {
		int rt = r.nextInt(16);
		switch (rt) {
		case 0:
			return Color.WHITE;
		case 1:
			return Color.SILVER;
		case 2:
			return Color.GRAY;
		case 3:
			return Color.BLACK;
		case 4:
			return Color.RED;
		case 5:
			return Color.MAROON;
		case 6:
			return Color.YELLOW;
		case 7:
			return Color.OLIVE;
		case 8:
			return Color.LIME;
		case 9:
			return Color.GREEN;
		case 10:
			return Color.AQUA;
		case 11:
			return Color.TEAL;
		case 12:
			return Color.BLUE;
		case 13:
			return Color.NAVY;
		case 14:
			return Color.FUCHSIA;
		case 15:
			return Color.PURPLE;
		case 16:
			return Color.ORANGE;
		default:
			return Color.AQUA;
		}
	}

	public static void playInstantFirework(Location paramLocation, FireworkEffect.Type paramType, boolean paramBoolean1, boolean paramBoolean2, List<Color> paramList1, List<Color> paramList2) {
	    Firework localFirework = (Firework)paramLocation.getWorld().spawn(paramLocation, Firework.class);
	    FireworkMeta localFireworkMeta = localFirework.getFireworkMeta();
	    localFireworkMeta.addEffect(FireworkEffect.builder().with(paramType).flicker(paramBoolean1).trail(paramBoolean2).withColor(paramList1).withFade(paramList2).build());
	    localFirework.setFireworkMeta(localFireworkMeta);
	    new BukkitRunnable() {
	      @Override
	      public void run() {
	        localFirework.detonate();
	      }
	    }.runTaskLater(Main.get(), 2L);
	  }
	  
	
	public static void playInstantFirework(Location paramLocation, FireworkEffect paramFireworkEffect) {

		Entity localEntity = paramLocation.getWorld().spawnEntity(paramLocation, EntityType.FIREWORK);
		Firework localFirework = (Firework) localEntity;
		FireworkMeta localFireworkMeta = localFirework.getFireworkMeta();
		localFireworkMeta.addEffect(paramFireworkEffect);
		localFireworkMeta.setPower(1);
		localFirework.setFireworkMeta(localFireworkMeta);
		((CraftFirework) localFirework).getHandle().expectedLifespan = 1;
	}
	
	public static void playInstantFirework(Location paramLocation) {

		Entity localEntity = paramLocation.getWorld().spawnEntity(paramLocation, EntityType.FIREWORK);
		Firework localFirework = (Firework) localEntity;
		FireworkMeta localFireworkMeta = localFirework.getFireworkMeta();
		localFireworkMeta.addEffect(getRandomFireworkEffect());
		localFireworkMeta.setPower(1);
		localFirework.setFireworkMeta(localFireworkMeta);
		((CraftFirework) localFirework).getHandle().expectedLifespan = 1;
	}

	public static void playFirework(Location loc, Type type, Color color, boolean flicker, boolean trail) {
		playInstantFirework(loc,FireworkEffect.builder().flicker(flicker).withColor(color).with(type).trail(trail).build());
	}
	public static void playFirework(Location loc, Type type, Color[] color, boolean flicker, boolean trail) {
		playInstantFirework(loc,FireworkEffect.builder().flicker(flicker).withColor(color).with(type).trail(trail).build());
	}
}
