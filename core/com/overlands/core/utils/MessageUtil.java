package com.overlands.core.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.google.common.collect.Lists;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.ranks.Rank;

public class MessageUtil {


	public static void sendCentredMessage(Player player, String message) {
		if(message == null || message.equals("")) {
			player.sendMessage("");
			return;
		}
		message = ChatColor.translateAlternateColorCodes('&', message);

		int messagePxSize = 0;
		boolean previousCode = false;
		boolean isBold = false;

		for(char c : message.toCharArray()){
			if(c == '§'){
				previousCode = true;
			}else if(previousCode){
				previousCode = false;
				isBold = c == 'l' || c == 'L';
			}else{
				DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);
				messagePxSize += isBold ? dFI.getBoldLength() : dFI.getLength();
				messagePxSize++;
			}
		}
		int CENTER_PX = 154;
		int halvedMessageSize = messagePxSize / 2;
		int toCompensate = CENTER_PX - halvedMessageSize;
		int spaceLength = DefaultFontInfo.SPACE.getLength() + 1;
		int compensated = 0;
		StringBuilder sb = new StringBuilder();
		while(compensated < toCompensate){
			sb.append(" ");
			compensated += spaceLength;
		}
		player.sendMessage(sb.toString() + message);
	}

	public static void sendPrivateMessage(Player p1, Player p2, String msg) {
		
	    GamePlayer sender = Main.getPM().getPlayer(p1.getUniqueId());
		GamePlayer target = Main.getPM().getPlayer(p2.getUniqueId());
		
		String targetName = target.getPlayer().getName();
		String senderName = sender.getPlayer().getName();
		if(Main.get().getChatCore().isInSilence(p1)) {
			return;
		}
		 if(Rank.isYoutuber(p1) || Rank.isAdmin(p1) && !Rank.isStaff(p2)) {
				p1.sendMessage(MessageUtil.color("&9Esse player não recebe mensagens privadas."));
		 }else {
				p2.sendMessage(MessageUtil.color(Rank.fromName(sender.getRank()).getColoredTag() + " " + senderName + " &7para você &d> " + ChatColor.RESET + msg));
				p1.sendMessage(MessageUtil.color("&7Você para " + Rank.fromName(target.getRank()).getColoredTag() + " " + targetName + "&d> " + ChatColor.RESET + msg));
				p2.playSound(p2.getLocation(), Sound.NOTE_PIANO, 2f, 2f);
				target.setLastMessage(sender);
		 }
	}
	
	public static void broadcast(String msg) {
		Bukkit.broadcastMessage(MessageUtil.color("&9&lOVERLANDS > " + msg));
	}
	
	public static void sendStaffMessage(String msg) {
		Bukkit.getOnlinePlayers().stream().filter(p -> Rank.isStaff(p)).forEach(p -> p.sendMessage(MessageUtil.color("&9[STAFF] &r" + msg)));
	}
	
	public static void sendRawStaffMessage(String msg) {
		Bukkit.getOnlinePlayers().stream().filter(p -> Rank.isStaff(p)).forEach(p -> p.sendMessage(MessageUtil.color(msg)));
	}
	
	public static void sendHunterMessage(String msg) {
		Bukkit.getOnlinePlayers().stream().filter(p -> Rank.isStaff(p)).forEach(p -> p.sendMessage(MessageUtil.color("&4[HUNTER] &r" + msg)));
	}

	public static String combine(String[] args, int start, String color, boolean comma) {
		if (args.length == 0)
			return "";

		String out = "";

		for (int i = start; i < args.length; i++) {
			if (color != null) {
				String preColor = ChatColor.getLastColors(args[i]);
				out += color + args[i] + preColor;
			} else
				out += args[i];

			if (comma)
				out += ", ";
			else
				out += " ";
		}

		if (out.length() > 0)
			if (comma)
				out = out.substring(0, out.length() - 2);
			else
				out = out.substring(0, out.length() - 1);

		return out;
	}

	public static String color(String str) {
		return ChatColor.translateAlternateColorCodes('&', str);
	}

	public static void staffMsg(Player p, String str) {
		p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[STAFF] " + str));
	}

	public static String modulate(String message) {
		return ChatColor.DARK_AQUA + color(message);
	}

	public static String encapsulate(String cap) {
		return ChatColor.GREEN + ChatColor.BOLD.toString() + cap + ChatColor.YELLOW;
	}

	public static List<String> color(List<String> list) {
		List<String> newList = Lists.newArrayList();
		list.forEach((str) -> {
			color(str);
			newList.add(str);
		});
		return newList;
	}

	public static String[] color(String... strings) {
		for (String s : strings) {
			color(s);
		}
		return strings;
	}

	public static List<String> replaceAll(List<String> list, String oldChar, String newChar) {
		ArrayList<String> strings = new ArrayList<String>();
		Iterator<String> i = list.iterator();
		while (i.hasNext()) {
			String str = (String) i.next();
			strings.add(str.replace(oldChar, newChar));
		}
		return strings;
	}

	public static String getBefore(String str, String toChar) {
		return StringUtils.substringBefore(str, toChar);
	}

	public static String getAfter(String str, String toChar) {
		return StringUtils.substringAfter(str, toChar);
	}

	public static String onlyNumbers(String str) {
		return str.replaceAll("[^-?0-9]+", "");
	}

	public static String createSpacer() {
		String build = "";
		for (int i = 0; i < 15; i++) {
			build = add(build);
		}
		return build;
	}

	private static String add(String build) {
		Random random = new Random();
		int r = random.nextInt(7) + 1;
		build = build + ChatColor.values()[r];
		return build;
	}
	
	public static String permission = color("&cVocê não tem permissão.");
}
