package com.overlands.core.utils.song;

import org.bukkit.Sound;

import com.overlands.core.utils.Sounds;

public class Instrument {
	public static Sound getInstrument(byte instrument) {
		switch (instrument) {
		case 0:
			return Sounds.NOTE_PIANO.bukkitSound();
		case 1:
			return Sounds.NOTE_BASS_GUITAR.bukkitSound();
		case 2:
			return Sounds.NOTE_BASS_DRUM.bukkitSound();
		case 3:
			return Sounds.NOTE_SNARE_DRUM.bukkitSound();
		case 4:
			return Sounds.NOTE_STICKS.bukkitSound();
		}
		return Sounds.NOTE_PIANO.bukkitSound();
	}

	public static org.bukkit.Instrument getBukkitInstrument(byte instrument) {
		switch (instrument) {
		case 0:
			return org.bukkit.Instrument.PIANO;
		case 1:
			return org.bukkit.Instrument.BASS_GUITAR;
		case 2:
			return org.bukkit.Instrument.BASS_DRUM;
		case 3:
			return org.bukkit.Instrument.SNARE_DRUM;
		case 4:
			return org.bukkit.Instrument.STICKS;
		}
		return org.bukkit.Instrument.PIANO;
	}
}
