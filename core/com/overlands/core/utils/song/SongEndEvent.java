package com.overlands.core.utils.song;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class SongEndEvent extends Event {

	private static final HandlerList handlers = new HandlerList();
	private SongPlayer song;
	private SongEndCause endCause;

	public SongEndEvent(SongPlayer song, SongEndCause endCause) {
		this.song = song;
		this.setEndCause(endCause);
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public SongPlayer getSongPlayer() {
		return song;
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public SongEndCause getEndCause() {
		return endCause;
	}

	public void setEndCause(SongEndCause endCause) {
		this.endCause = endCause;
	}
}