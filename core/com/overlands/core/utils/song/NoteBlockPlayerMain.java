package com.overlands.core.utils.song;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.entity.Player;

public class NoteBlockPlayerMain {

	public static HashMap<UUID, ArrayList<SongPlayer>> playingSongs = new HashMap<UUID, ArrayList<SongPlayer>>();
	public static HashMap<UUID, Byte> playerVolume = new HashMap<UUID, Byte>();

	public static boolean isReceivingSong(Player p) {
		return ((playingSongs.get(p.getUniqueId()) != null) && (!playingSongs.get(p.getUniqueId()).isEmpty()));
	}

	public static void stopPlaying(Player p) {
		if (playingSongs.get(p.getUniqueId()) == null) {
			return;
		}
		for (SongPlayer s : playingSongs.get(p.getUniqueId())) {
			s.removePlayer(p);
		}
	}

	public static void setPlayerVolume(Player p, byte volume) {
		playerVolume.put(p.getUniqueId(), volume);
	}

	public static byte getPlayerVolume(Player p) {
		Byte b = playerVolume.get(p.getUniqueId());
		if (b == null) {
			b = 100;
			playerVolume.put(p.getUniqueId(), b);
		}
		return b;
	}
}
