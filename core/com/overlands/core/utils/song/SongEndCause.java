package com.overlands.core.utils.song;

public enum SongEndCause {

	PLAYER_REMOVED,
	SONG_END;
}
