package com.overlands.core.utils.calc;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeUtils {
	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";
	public static final String DATE_FORMAT_DAY = "yyyy-MM-dd";

	public static String now() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		return sdf.format(cal.getTime());
	}
	
	public static String formatHours(int time) {
		Date date = new Date(time* 1000L);
        return new SimpleDateFormat("hh:mm:ss").format(date);
	}
	
	public static String formatMinutes(int time) {
		Date date = new Date(time* 1000L);
        return new SimpleDateFormat("mm:ss").format(date);
	}
	
	public static String formatSeconds(int time) {
		Date date = new Date(time* 1000L);
        return new SimpleDateFormat("ss").format(date);
	}

	public static String when(long time) {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		return sdf.format(time);
	}

	public static String today() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_DAY);
		return sdf.format(cal.getTime());
	}

	public enum TimeUnit {
		FIT, DAYS, HOURS, MINUTES, SECONDS, MILLISECONDS
	}

	public static String sinceExact(long epoch) {
		return convertString(System.currentTimeMillis() - epoch, 1, TimeUnit.FIT);
	}
	
	public static String since(long epoch) {
		return "levou " + convertString(System.currentTimeMillis() - epoch, 1, TimeUnit.FIT) + ".";
	}
	
	public static String getSince(long epoch) {
		return convertPrString(System.currentTimeMillis() - epoch, 1, TimeUnit.FIT);
	}
	
	public static double convert(long time, int trim, TimeUnit type) {
		if (type == TimeUnit.FIT) {
			if (time < 60000)
				type = TimeUnit.SECONDS;
			else if (time < 3600000)
				type = TimeUnit.MINUTES;
			else if (time < 86400000)
				type = TimeUnit.HOURS;
			else
				type = TimeUnit.DAYS;
		}

		if (type == TimeUnit.DAYS)
			return MathUtils.trim(trim, (time) / 86400000d);
		if (type == TimeUnit.HOURS)
			return MathUtils.trim(trim, (time) / 3600000d);
		if (type == TimeUnit.MINUTES)
			return MathUtils.trim(trim, (time) / 60000d);
		if (type == TimeUnit.SECONDS)
			return MathUtils.trim(trim, (time) / 1000d);
		else
			return MathUtils.trim(trim, time);
	}

	public static String MakeStr(long time) {
		return convertString(time, 1, TimeUnit.FIT);
	}

	public static String MakeStr(long time, int trim) {
		return convertString(Math.max(0, time), trim, TimeUnit.FIT);
	}
	
	public static String makePrStr(long time, int trim) {
		return convertPrString(Math.max(0, time), trim, TimeUnit.FIT);
	}

	public static String convertString(long time, int trim, TimeUnit type) {
		if (time == -1)
			return "Permanentemente";

		if (type == TimeUnit.FIT) {
			if (time < 60000)
				type = TimeUnit.SECONDS;
			else if (time < 3600000)
				type = TimeUnit.MINUTES;
			else if (time < 86400000)
				type = TimeUnit.HOURS;
			else
				type = TimeUnit.DAYS;
		}

		String text;
		double num;
		if (trim == 0) {
			if (type == TimeUnit.DAYS)
				text = (num = MathUtils.trim(trim, time / 86400000d)) + " Dia";
			else if (type == TimeUnit.HOURS)
				text = (num = MathUtils.trim(trim, time / 3600000d)) + " Hora";
			else if (type == TimeUnit.MINUTES)
				text = (num = MathUtils.trim(trim, time / 60000d)) + " Minuto";
			else if (type == TimeUnit.SECONDS)
				text = (int) (num = (int) MathUtils.trim(trim, time / 1000d)) + " Segundo";
			else
				text = (int) (num = (int) MathUtils.trim(trim, time)) + " Millisegundo";
		} else {
			if (type == TimeUnit.DAYS)
				text = (num = MathUtils.trim(trim, time / 86400000d)) + " Dia";
			else if (type == TimeUnit.HOURS)
				text = (num = MathUtils.trim(trim, time / 3600000d)) + " Hora";
			else if (type == TimeUnit.MINUTES)
				text = (num = MathUtils.trim(trim, time / 60000d)) + " Minuto";
			else if (type == TimeUnit.SECONDS)
				text = (num = MathUtils.trim(trim, time / 1000d)) + " Segundo";
			else
				text = (int) (num = (int) MathUtils.trim(0, time)) + " Millisegundo";
		}

		if (num != 1)
			text += "s";

		return text;
	}
	
	public static String convertPrString(long time, int trim, TimeUnit type) {
		if (time == -1)
			return "permanentemente";

		if (type == TimeUnit.FIT) {
			if (time < 60000)
				type = TimeUnit.SECONDS;
			else if (time < 3600000)
				type = TimeUnit.MINUTES;
			else if (time < 86400000)
				type = TimeUnit.HOURS;
			else
				type = TimeUnit.DAYS;
		}

		String text;
		double num;
		if (trim == 0) {
			if (type == TimeUnit.DAYS)
				text = (num = MathUtils.trim(trim, time / 86400000d)) + " dia";
			else if (type == TimeUnit.HOURS)
				text = (num = MathUtils.trim(trim, time / 3600000d)) + " hora";
			else if (type == TimeUnit.MINUTES)
				text = (num = MathUtils.trim(trim, time / 60000d)) + " minuto";
			else if (type == TimeUnit.SECONDS)
				text = (int) (num = (int) MathUtils.trim(trim, time / 1000d)) + " segundo";
			else
				text = (int) (num = (int) MathUtils.trim(trim, time)) + " millisegundo";
		} else {
			if (type == TimeUnit.DAYS)
				text = (num = MathUtils.trim(trim, time / 86400000d)) + " dia";
			else if (type == TimeUnit.HOURS)
				text = (num = MathUtils.trim(trim, time / 3600000d)) + " hora";
			else if (type == TimeUnit.MINUTES)
				text = (num = MathUtils.trim(trim, time / 60000d)) + " minuto";
			else if (type == TimeUnit.SECONDS)
				text = (num = MathUtils.trim(trim, time / 1000d)) + " segundo";
			else
				text = (int) (num = (int) MathUtils.trim(0, time)) + " millisegundo";
		}

		if (num != 1)
			text += "s";

		return text;
	}


	public static boolean elapsed(long from, long required) {
		return System.currentTimeMillis() - from > required;
	}

	public static boolean reach(long from, long required) {
		return System.currentTimeMillis() - from < required;
	}

}