package com.overlands.core.utils.calc.particle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.overlands.core.utils.scheduler.SchedulerEvent;
import com.overlands.core.utils.scheduler.UpdateType;

public class ParticleListen implements Listener {

	private static final List<String> Moving = new ArrayList<>();
	private final HashMap<UUID, Location> lastBlockLocation = new HashMap<>();

	@EventHandler
	public void onUpdate(SchedulerEvent e) {
		if (e.getType() == UpdateType.FASTEST) {
			for (Player p : Bukkit.getOnlinePlayers()) {
				Location loc1 = p.getLocation();
				Location loc2 = this.lastBlockLocation.get(p.getUniqueId());
				if (this.lastBlockLocation.get(p.getUniqueId()) == null) {
					this.lastBlockLocation.put(p.getUniqueId(), loc1);
					loc2 = this.lastBlockLocation.get(p.getUniqueId());
				}
				this.lastBlockLocation.put(p.getUniqueId(), p.getLocation());
				if ((loc2.getX() != loc1.getX()) || (loc2.getY() != loc1.getY())
						|| (loc2.getZ() != loc1.getZ())) {
					if (!Moving.contains(p.getName())) {
						Moving.add(p.getName());
					}
				} else if (Moving.contains(p.getName())) {
					Moving.remove(p.getName());
				}
			}
		}
	}

	public static boolean isMoving(Player paramPlayer) {
		if (Moving.contains(paramPlayer.getName())) {
			return true;
		}
		return false;
	}
}