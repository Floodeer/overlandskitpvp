package com.overlands.core.utils.calc.particle;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.calc.VelocityUtils;

public class Wings implements Runnable {

	private Location location;
	private double xPos;
	private double yPos;
	private int time = 0;
	private boolean loop = false;
	private final boolean[][] shape;
	private final boolean[][] shape2;
	private Player player;

	public Wings(Player player) {
		this.player = player;
		boolean x = true;
		boolean o = false;
		shape = new boolean[][] { {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
				{o, o, o, o, o, o, o, o, o, o, o, o, x, x, x, x, o, o},
				{o, o, o, o, o, o, o, o, o, o, x, x, x, x, x, o, o, o},
				{o, o, o, o, o, o, o, o, o, x, x, x, x, x, o, o, o, o},
				{o, o, o, o, o, o, o, o, o, x, x, x, x, o, o, o, o, o},
				{o, o, o, o, o, o, o, o, o, x, x, x, x, o, o, o, o, o},
				{o, o, o, o, o, o, o, o, o, x, x, x, o, o, o, o, o, o},
				{o, o, o, o, o, o, o, o, o, o, x, x, o, o, o, o, o, o},
				{o, o, o, o, o, o, o, o, o, o, o, x, x, o, o, o, o, o} };

		shape2 = new boolean[][] { {o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o},
				{o, x, x, x, x, o, o, o, o, o, o, o, o, o, o, o, o, o},
				{o, o, x, x, x, x, x, o, o, o, o, o, o, o, o, o, o, o},
				{o, o, o, x, x, x, x, x, o, o, o, o, o, o, o, o, o, o},
				{o, o, o, o, x, x, x, x, o, o, o, o, o, o, o, o, o, o},
				{o, o, o, o, x, x, x, x, o, o, o, o, o, o, o, o, o, o},
				{o, o, o, o, o, x, x, x, o, o, o, o, o, o, o, o, o, o},
				{o, o, o, o, o, x, x, o, o, o, o, o, o, o, o, o, o, o},
				{o, o, o, o, x, x, o, o, o, o, o, o, o, o, o, o, o, o} };
				
				player.sendMessage(MessageUtil.color("&aAtivado efeito &c&lDemon Wings&a."));
	}

	private Player getPlayer() {
		return player;
	}
	
	@Override
	public void run() {
		location = getPlayer().getLocation();
		location.setPitch(0.0F);
		Vector direction = location.getDirection().normalize();
		direction.multiply(-0.3D);
		location.add(direction);
		double space = 0.2D;
		double xBase = location.getX() - space * (double) shape[0].length / 2.0D + space;
		yPos = location.clone().getY() + 2.0D;

        for (boolean[] a1 : shape) {
            for (boolean a2 : a1) {
                if (a2) {
                    Location i = location.clone();
                    i.setX(xPos);
                    i.setY(yPos);
                    Vector v = i.toVector().subtract(location.toVector());
                    VelocityUtils.rotateAroundAxisY(v, Math.toRadians(-getPlayer().getLocation().getYaw()) - (double) time * MathUtils.PI / 62.0D);
                    location.add(v);
                    int target = (int) (MathUtils.randomRange(100, 255));
                    getPlayer().getWorld().spigot().playEffect(location, Effect.COLOURED_DUST, 1, 1, (float) target, 0.0F, 0.0F, 1.0F, 0, 64);
                    location.subtract(v);
                }
                xPos += space;
            }
            yPos -= space;
            xPos = xBase;
        }

		double shapes11 = location.getX() - space * (double) shape2[0].length / 2.0D + space;
		yPos = location.clone().getY() + 2.0D;

        for (boolean[] a1 : shape2) {
            for (boolean a2 : a1) {
                if (a2) {
                    Location shapes14 = location.clone();
                    shapes14.setX(xPos);
                    shapes14.setY(yPos);
                    Vector v = shapes14.toVector().subtract(location.toVector());
                    VelocityUtils.rotateAroundAxisY(v,
                            Math.toRadians((double) (-getPlayer().getLocation().getYaw())) + (double) time * MathUtils.PI / 62.0D);
                    location.add(v);
                    int step = (int) (MathUtils.randomRange(100, 255));
                    getPlayer().getWorld().spigot().playEffect(location, Effect.COLOURED_DUST, 1, 1, (float) step, 0.0F, 0.0F, 1.0F, 0, 64);
                    location.subtract(v);
                }

                xPos += space;
            }

            yPos -= space;
            xPos = shapes11;
        }

		if (loop) {
			++time;
			if (time >= 0) {
				loop = false;
			}
		} else {
			--time;
			if (time <= -14) {
				loop = true;
			}
		}
	}
}
