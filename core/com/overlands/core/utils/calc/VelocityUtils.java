package com.overlands.core.utils.calc;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import com.overlands.core.utils.block.BlockUtils;

public class VelocityUtils {
	
	public static Vector getTrajectory(Entity paramEntity1, Entity paramEntity2) {
		return getTrajectory(paramEntity1.getLocation().toVector(), paramEntity2.getLocation().toVector());
	}

	public static Vector getTrajectory(Entity paramEntity, Player paramPlayer) {
		return getTrajectory(paramEntity.getLocation().toVector(), paramPlayer.getLocation().toVector());
	}

	public static Vector getTrajectory(Location paramLocation, Player paramPlayer) {
		return getTrajectory(paramLocation.toVector(), paramPlayer.getLocation().toVector());
	}

	public static Vector getTrajectory(Location paramLocation1, Location paramLocation2) {
		return getTrajectory(paramLocation1.toVector(), paramLocation2.toVector());
	}

	public static Vector getTrajectory(Vector vector1, Vector vector2) {
		return vector2.subtract(vector1).normalize();
	}

	public static Vector getTrajectory2d(Entity paramEntity1, Entity paramEntity2) {
		return getTrajectory2d(paramEntity1.getLocation().toVector(), paramEntity2.getLocation().toVector());
	}

	public static Vector getTrajectory2d(Location paramLocation1, Location paramLocation2) {
		return getTrajectory2d(paramLocation1.toVector(), paramLocation2.toVector());
	}

	public static Vector getTrajectory2d(Vector vector1, Vector vector2) {
		return vector2.subtract(vector1).setY(0).normalize();
	}

	public static boolean hasSight(Location paramLocation, Player paramPlayer) {
		return (hasSight(paramLocation, paramPlayer.getLocation()))
				|| (hasSight(paramLocation, paramPlayer.getEyeLocation()));
	}

	public static boolean hasSight(Location paramLocation1, Location paramLocation2) {
		Location localLocation = new Location(paramLocation1.getWorld(), paramLocation1.getX(), paramLocation1.getY(),
				paramLocation1.getZ());
		double d = 0.1D;
		Vector localVector = getTrajectory(paramLocation1, paramLocation2).multiply(0.1D);
		while (MathUtils.offset(localLocation, paramLocation2) > d) {
			localLocation.add(localVector);
			if (!BlockUtils.airFoliage(localLocation.getBlock())) {
				return false;
			}
		}
		return true;
	}

	public static float getPitch(Vector vec) {
		double x = vec.getX();
		double y = vec.getY();
		double z = vec.getZ();
		double xz = Math.sqrt(x * x + z * z);

		double pitch = Math.toDegrees(Math.atan(xz / y));
		if (y <= 0.0D)
			pitch += 90.0D;
		else {
			pitch -= 90.0D;
		}
		return (float) pitch;
	}

	public static float getYaw(Vector vec) {
		double x = vec.getX();
		double z = vec.getZ();

		double yaw = Math.toDegrees(Math.atan(-x / z));
		if (z < 0.0D) {
			yaw += 180.0D;
		}
		return (float) yaw;
	}

	public static Vector normalize(Vector vector) {
		if (vector.length() > 0.0D) {
			vector.normalize();
		}
		return vector;
	}

	public static Vector clone(Vector vector) {
		return new Vector(vector.getX(), vector.getY(), vector.getZ());
	}

	public static Vector getBumpVector(Entity paramEntity, Location paramLocation, double paramDouble) {
		Vector localVector = paramEntity.getLocation().toVector().subtract(paramLocation.toVector()).normalize();
		localVector.multiply(paramDouble);
		return localVector;
	}

	public static void knockback(Entity p, Location loc, double multiply, double y, boolean zero) {
		Location l = p.getLocation();
		l.setPitch(0.0F);
		loc.setPitch(0.0F);
		Vector v = l.toVector().subtract(loc.toVector()).normalize();
		if (zero) {
			v = loc.toVector().subtract(l.toVector()).normalize();
		}

		v.setY(y);
		p.setVelocity(v.multiply(multiply));
		return;
	}
	
	public static void knockbackDirection(Entity p, Location loc, double multiply, double y, boolean zero) {
		Location l = p.getLocation();
		l.setPitch(0.0F);
		loc.setPitch(0.0F);
		Vector v = l.toVector().subtract(loc.getDirection()).normalize();
		if (zero) {
			v = loc.getDirection().subtract(l.toVector()).normalize();
		}

		v.setY(y);
		p.setVelocity(v.multiply(multiply));
		return;
	}
	
	public static void knockback(Entity p, Vector vec, double multiply, double y) {
		p.setVelocity(vec.multiply(6.4D).setY(-0.7D));
		return;
	}

	public static Vector getRandomVectorLine() {
		int min = -5;
		int max = 5;
		int rz = (int) (Math.random() * (max - min) + min);
		int rx = (int) (Math.random() * (max - min) + min);

		double miny = -5.0D;
		double maxy = -1.0D;
		double ry = Math.random() * (maxy - miny) + miny;

		return new Vector(rx, ry, rz).normalize();
	}

	public static Vector getPullVector(Entity paramEntity, Location paramLocation, double paramDouble) {
		Vector localVector = paramLocation.toVector().subtract(paramEntity.getLocation().toVector()).normalize();
		localVector.multiply(paramDouble);
		return localVector;
	}

	public static void bumpEntity(Entity paramEntity, Location paramLocation, double paramDouble) {
		paramEntity.setVelocity(getBumpVector(paramEntity, paramLocation, paramDouble));
	}

	public static void bumpEntity(Entity paramEntity, Location paramLocation, double paramDouble1,
			double paramDouble2) {
		Vector localVector = getBumpVector(paramEntity, paramLocation, paramDouble1);
		localVector.setY(paramDouble2);
		paramEntity.setVelocity(localVector);
	}

	public static void pullEntity(Entity paramEntity, Location paramLocation, double paramDouble) {
		paramEntity.setVelocity(getPullVector(paramEntity, paramLocation, paramDouble));
	}

	public static void pullEntity(Entity paramEntity, Location paramLocation, double paramDouble1,
			double paramDouble2) {
		Vector localVector = getPullVector(paramEntity, paramLocation, paramDouble1);
		localVector.setY(paramDouble2);
		paramEntity.setVelocity(localVector);
	}

	public static void velocity(Entity paramEntity, double multiply, double paramDouble2, double paramDouble3) {
		velocity(paramEntity, paramEntity.getLocation().getDirection(), multiply, false, 0.0D, paramDouble2,
				paramDouble3);
	}

	public static void velocity(Entity paramEntity, Vector vector, double multiply, boolean paramBoolean,
			double y, double y2, double y3) {
		if ((Double.isNaN(vector.getX())) || (Double.isNaN(vector.getY())) || (Double.isNaN(vector.getZ()))
				|| (vector.length() == 0.0D)) {
			return;
		}
		if (paramBoolean) {
			vector.setY(y);
		}
		vector.normalize();
		vector.multiply(multiply);

		vector.setY(vector.getY() + y2);
		if (vector.getY() > y3) {
			vector.setY(y3);
		}
		paramEntity.setFallDistance(0.0F);
		paramEntity.setVelocity(vector);
	}

	public static final Vector rotateAroundAxisX(Vector vector, double paramDouble) {
		double d3 = Math.cos(paramDouble);
		double d4 = Math.sin(paramDouble);
		double d1 = vector.getY() * d3 - vector.getZ() * d4;
		double d2 = vector.getY() * d4 + vector.getZ() * d3;
		return vector.setY(d1).setZ(d2);
	}

	public static final Vector rotateAroundAxisY(Vector vector, double paramDouble) {
		double d3 = Math.cos(paramDouble);
		double d4 = Math.sin(paramDouble);
		double d1 = vector.getX() * d3 + vector.getZ() * d4;
		double d2 = vector.getX() * -d4 + vector.getZ() * d3;
		return vector.setX(d1).setZ(d2);
	}

	public static final Vector rotateAroundAxisZ(Vector vector, double paramDouble) {
		double d3 = Math.cos(paramDouble);
		double d4 = Math.sin(paramDouble);
		double d1 = vector.getX() * d3 - vector.getY() * d4;
		double d2 = vector.getX() * d4 + vector.getY() * d3;
		return vector.setX(d1).setY(d2);
	}

	public static final Vector rotateVector(Vector vector, double paramDouble1, double paramDouble2, double paramDouble3) {
		rotateAroundAxisX(vector, paramDouble1);
		rotateAroundAxisY(vector, paramDouble2);
		rotateAroundAxisZ(vector, paramDouble3);
		return vector;
	}

	 public static Vector rotateAroundFixedLoc(Vector v, Location loc) {
	    double yaw = loc.getYaw() / 180.0F * 3.141592653589793D;
	    double pitch = loc.getPitch() / 180.0F * 3.141592653589793D;
	    v = rotateAroundAxisX(v, pitch);
	    v = rotateAroundAxisY(v, -yaw);
	    return v;
	 }
	 
	public static final double angleToXAxis(Vector vector) {
		return Math.atan2(vector.getX(), vector.getY());
	}

	public static void velocity(Entity paramEntity, double paramDouble1, double paramDouble2, double paramDouble3,
			boolean paramBoolean) {
		velocity(paramEntity, paramEntity.getLocation().getDirection(), paramDouble1, false, 0.0D, paramDouble2,
				paramDouble3, paramBoolean);
	}

	public static void velocity(Entity paramEntity, Vector vector, double paramDouble1, boolean paramBoolean1,
			double paramDouble2, double paramDouble3, double paramDouble4, boolean paramBoolean2) {
		if ((Double.isNaN(vector.getX())) || (Double.isNaN(vector.getY())) || (Double.isNaN(vector.getZ()))
				|| (vector.length() == 0.0D)) {
			return;
		}
		if (paramBoolean1) {
			vector.setY(paramDouble2);
		}
		vector.normalize();
		vector.multiply(paramDouble1);

		vector.setY(vector.getY() + paramDouble3);
		if (vector.getY() > paramDouble4) {
			vector.setY(paramDouble4);
		}
		if (paramBoolean2) {
			vector.setY(vector.getY() + 0.2D);
		}
		paramEntity.setFallDistance(0.0F);
		paramEntity.setVelocity(vector);
	}

	public static Vector getBackVector(Location loc) {
		final float newZ = (float) (loc.getZ() + (1 * Math.sin(Math.toRadians(loc.getYaw() + 90 * 1))));
		final float newX = (float) (loc.getX() + (1 * Math.cos(Math.toRadians(loc.getYaw() + 90 * 1))));
		return new Vector(newX - loc.getX(), 0, newZ - loc.getZ());
	}

	public static Vector rotateX(Vector v, double a) {
		double y = Math.cos(a) * v.getY() - Math.sin(a) * v.getZ();
		double z = Math.sin(a) * v.getY() + Math.cos(a) * v.getZ();
		return v.setY(y).setZ(z);
	}

	public static Vector rotateY(Vector v, double b) {
		double x = Math.cos(b) * v.getX() + Math.sin(b) * v.getZ();
		double z = -Math.sin(b) * v.getX() + Math.cos(b) * v.getZ();
		return v.setX(x).setY(z);
	}

	public static final Vector rotateZ(Vector v, double c) {
		double x = Math.cos(c) * v.getX() - Math.sin(c) * v.getY();
		double y = Math.sin(c) * v.getX() + Math.cos(c) * v.getY();
		return v.setX(x).setY(y);
	}

	public static class Vector3D {

		private final double x;
		private final double y;
		private final double z;

		public Vector3D(double x, double y, double z) {
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public Vector3D(Location location) {
			this(location.toVector());
		}

		public Vector3D(Vector vector) {
			if (vector == null)
				throw new IllegalArgumentException("Vector cannot be NULL.");
			this.x = vector.getX();
			this.y = vector.getY();
			this.z = vector.getZ();
		}

		public Vector3D abs() {
			return new Vector3D(Math.abs(x), Math.abs(y), Math.abs(z));
		}

		public Vector3D add(double x, double y, double z) {
			return new Vector3D(this.x + x, this.y + y, this.z + z);
		}

		public Vector3D add(Vector3D other) {
			if (other == null)
				throw new IllegalArgumentException("other cannot be NULL");

			return new Vector3D(x + other.x, y + other.y, z + other.z);
		}

		public Vector3D multiply(double factor) {
			return new Vector3D(x * factor, y * factor, z * factor);
		}

		public Vector3D multiply(int factor) {
			return new Vector3D(x * factor, y * factor, z * factor);
		}

		public Vector3D subtract(Vector3D other) {
			if (other == null)
				throw new IllegalArgumentException("other cannot be NULL");
			return new Vector3D(x - other.x, y - other.y, z - other.z);
		}
	}

	public static boolean hasIntersection(Vector3D p1, Vector3D p2, Vector3D min, Vector3D max) {
		final double epsilon = 0.0001f;

		Vector3D d = p2.subtract(p1).multiply(0.5);
		Vector3D e = max.subtract(min).multiply(0.5);
		Vector3D c = p1.add(d).subtract(min.add(max).multiply(0.5));
		Vector3D ad = d.abs();

		if (Math.abs(c.x) > e.x + ad.x)
			return false;
		if (Math.abs(c.y) > e.y + ad.y)
			return false;
		if (Math.abs(c.z) > e.z + ad.z)
			return false;

		if (Math.abs(d.y * c.z - d.z * c.y) > e.y * ad.z + e.z * ad.y + epsilon)
			return false;
		if (Math.abs(d.z * c.x - d.x * c.z) > e.z * ad.x + e.x * ad.z + epsilon)
			return false;
		if (Math.abs(d.x * c.y - d.y * c.x) > e.x * ad.y + e.y * ad.x + epsilon)
			return false;

		return true;
	}
}