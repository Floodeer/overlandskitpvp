package com.overlands.core.utils.calc;

import java.util.NavigableMap;
import java.util.Random;
import java.util.TreeMap;

public class RandomSet<E> {
	
    private final NavigableMap<Double, E> map = new TreeMap<Double, E>();
    private final Random random;
    private double total = 0;

    public RandomSet() {
        this(new Random());
    }

    public RandomSet(Random random) {
        this.random = random;
    }

    public void add(double weight, E result) {
        if (weight <= 0) return;
        total += weight;
        map.put(total, result);
    }

    public E result() {
        double value = random.nextDouble() * total;
        return map.ceilingEntry(value).getValue();
    }
}