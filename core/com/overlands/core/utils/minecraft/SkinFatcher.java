package com.overlands.core.utils.minecraft;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class SkinFatcher {

	public static String[] getValue(String name) {
		try {
			URL api = new URL("https://api.mojang.com/users/profiles/minecraft/" + name);
			InputStreamReader reader = new InputStreamReader(api.openStream());
			String uuid = new JsonParser().parse(reader).getAsJsonObject().get("id").getAsString();

			URL profile = new URL(
					"https://sessionserver.mojang.com/session/minecraft/profile/" + uuid + "?unsigned=false");
			
			InputStreamReader profileReader = new InputStreamReader(profile.openStream());
			JsonObject textureProperty = new JsonParser().parse(profileReader).getAsJsonObject().get("properties")
					.getAsJsonArray().get(0).getAsJsonObject();
		
			String texture = textureProperty.get("value").getAsString();
			String signature = textureProperty.get("signature").getAsString();

			return new String[] { texture, signature };
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
 }