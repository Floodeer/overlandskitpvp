package com.overlands.core.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.bukkit.entity.Player;

import com.overlands.kitpvp.Main;

public class ActionBar {
	
	public static void sendActionBar(Player p, String msg) {
		send(p, msg);
	}
	
	private static void send(Player player, String message) {
		try {
			Class<?> c1 = Class.forName("org.bukkit.craftbukkit." + Main.nmsver + ".entity.CraftPlayer");
			Object p = c1.cast(player);
			Object ppoc = null;
			Class<?> c4 = Class.forName("net.minecraft.server." + Main.nmsver + ".PacketPlayOutChat");
			Class<?> c5 = Class.forName("net.minecraft.server." + Main.nmsver + ".Packet");
			Class<?> c2 = Class.forName("net.minecraft.server." + Main.nmsver + ".ChatComponentText");
			Class<?> c3 = Class.forName("net.minecraft.server." + Main.nmsver + ".IChatBaseComponent");
			Object o = c2.getConstructor(new Class<?>[] { String.class }).newInstance(message);
			ppoc = c4.getConstructor(new Class<?>[] { c3, byte.class }).newInstance(o, (byte) 2);
			Method m1 = c1.getDeclaredMethod("getHandle");
			Object h = m1.invoke(p);
			Field f1 = h.getClass().getDeclaredField("playerConnection");
			Object pc = f1.get(h);
			Method m5 = pc.getClass().getDeclaredMethod("sendPacket", c5);
			m5.invoke(pc, ppoc);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
