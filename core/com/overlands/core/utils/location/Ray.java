package com.overlands.core.utils.location;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;
 
public class Ray {
   
    private Vector origin, direction;
   
    public Ray(Vector origin, Vector direction) {
        this.origin = origin;
        this.direction = direction;
    }
   
    public static Ray from(Player player) {
        return new Ray(player.getEyeLocation().toVector(), player.getLocation().getDirection());
    }

    public static Vector right(Vector vector) {
        Vector n = vector.clone();
        n = n.setY(0).normalize();
        double x = n.getX();
        n.setX(n.getZ());
        n.setZ(-x);
        return n;
    }

    public Ray level() {
        return new Ray(origin, direction.setY(0).normalize());
    }
   
    public Vector getOrigin() {
        return origin;
    }
   
    public Vector getDirection() {
        return direction;
    }
   
    public double origin(int i) {
        switch (i) {
            case 0:
                return origin.getX();
            case 1:
                return origin.getY();
            case 2:
                return origin.getZ();
            default:
                return 0;
        }
    }
   
    public double direction(int i) {
        switch (i) {
            case 0:
                return direction.getX();
            case 1:
                return direction.getY();
            case 2:
                return direction.getZ();
            default:
                return 0;
        }
    }
 
    public Vector getPoint(double distance) {
        return direction.clone().normalize().multiply(distance).add(origin);
    }

    public static Location getPoint(Player player, double distance) {
        Vector point = Ray.from(player).getPoint(distance);
        return new Location(player.getWorld(), point.getX(), point.getY(), point.getZ());
    }
}