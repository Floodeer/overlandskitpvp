package com.overlands.core.utils.item;

import org.bukkit.Location;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;

import com.overlands.core.utils.Runner;
import com.overlands.kitpvp.Main;

public class EntityItemFactory {
	
	public static void dropTemporary(Location loc, ItemStack itemS, int ticks) {
		Item item = loc.getWorld().dropItem(loc, itemS);
		item.setPickupDelay(Integer.MAX_VALUE);
		Runner.make(Main.get()).delay(ticks).run(item::remove);
	}

}
