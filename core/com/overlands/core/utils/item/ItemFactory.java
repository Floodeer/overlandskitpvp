package com.overlands.core.utils.item;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import com.overlands.core.utils.DyeColorUtil;
import com.overlands.core.utils.GlowUtils;
import com.overlands.core.utils.reflection.RefClass;

public class ItemFactory {

	private static HashSet<Material> _axeSet = new HashSet<>();
	private static HashSet<Material> _swordSet = new HashSet<>();
	private static HashSet<Material> _maulSet = new HashSet<>();
	private static HashSet<Material> pickSet = new HashSet<>();
	private static HashSet<Material> diamondSet = new HashSet<>();
	private static HashSet<Material> goldSet = new HashSet<>();

	public static boolean isAxe(ItemStack paramItemStack) {
		if (paramItemStack == null) {
			return false;
		}
		if (_axeSet.isEmpty()) {
			_axeSet.add(Material.WOOD_AXE);
			_axeSet.add(Material.STONE_AXE);
			_axeSet.add(Material.IRON_AXE);
			_axeSet.add(Material.GOLD_AXE);
			_axeSet.add(Material.DIAMOND_AXE);
		}
		return _axeSet.contains(paramItemStack.getType());
	}

	public static boolean isSword(ItemStack paramItemStack) {
		if (paramItemStack == null) {
			return false;
		}
		if (_swordSet.isEmpty()) {
			_swordSet.add(Material.WOOD_SWORD);
			_swordSet.add(Material.STONE_SWORD);
			_swordSet.add(Material.IRON_SWORD);
			_swordSet.add(Material.GOLD_SWORD);
			_swordSet.add(Material.DIAMOND_SWORD);
		}
		return _swordSet.contains(paramItemStack.getType());
	}

	public static boolean isShovel(ItemStack paramItemStack) {
		if (paramItemStack == null) {
			return false;
		}
		if (_maulSet.isEmpty()) {
			_maulSet.add(Material.WOOD_SPADE);
			_maulSet.add(Material.STONE_SPADE);
			_maulSet.add(Material.IRON_SPADE);
			_maulSet.add(Material.GOLD_SPADE);
			_maulSet.add(Material.DIAMOND_SPADE);
		}
		return _maulSet.contains(paramItemStack.getType());
	}

	public static HashSet<Material> scytheSet = new HashSet<>();

	public static boolean isHoe(ItemStack paramItemStack) {
		if (paramItemStack == null) {
			return false;
		}
		if (scytheSet.isEmpty()) {
			scytheSet.add(Material.WOOD_HOE);
			scytheSet.add(Material.STONE_HOE);
			scytheSet.add(Material.IRON_HOE);
			scytheSet.add(Material.GOLD_HOE);
			scytheSet.add(Material.DIAMOND_HOE);
		}
		return scytheSet.contains(paramItemStack.getType());
	}

	public static boolean isPickaxe(ItemStack paramItemStack) {
		if (paramItemStack == null) {
			return false;
		}
		if (pickSet.isEmpty()) {
			pickSet.add(Material.WOOD_PICKAXE);
			pickSet.add(Material.STONE_PICKAXE);
			pickSet.add(Material.IRON_PICKAXE);
			pickSet.add(Material.GOLD_PICKAXE);
			pickSet.add(Material.DIAMOND_PICKAXE);
		}
		return pickSet.contains(paramItemStack.getType());
	}

	public static boolean isDiamond(ItemStack paramItemStack) {
		if (paramItemStack == null) {
			return false;
		}
		if (diamondSet.isEmpty()) {
			diamondSet.add(Material.DIAMOND_SWORD);
			diamondSet.add(Material.DIAMOND_AXE);
			diamondSet.add(Material.DIAMOND_SPADE);
			diamondSet.add(Material.DIAMOND_HOE);
		}
		return diamondSet.contains(paramItemStack.getType());
	}

	public static boolean isGold(ItemStack paramItemStack) {
		if (paramItemStack == null) {
			return false;
		}
		if (goldSet.isEmpty()) {
			goldSet.add(Material.GOLD_SWORD);
			goldSet.add(Material.GOLD_AXE);
		}
		return goldSet.contains(paramItemStack.getType());
	}

	public static boolean isBow(ItemStack paramItemStack) {
		if (paramItemStack == null) {
			return false;
		}
		return paramItemStack.getType() == Material.BOW;
	}

	public static boolean isWeapon(ItemStack paramItemStack) {
		return (isAxe(paramItemStack)) || (isSword(paramItemStack));
	}

	public static boolean isMat(ItemStack paramItemStack, Material paramMaterial) {
		if (paramItemStack == null) {
			return false;
		}
		return paramItemStack.getType() == paramMaterial;
	}

	public static boolean isRepairable(ItemStack paramItemStack) {
		return paramItemStack.getType().getMaxDurability() > 0;
	}
	
	public static ItemStack clearClientAttributes(ItemStack item) {
	   item.getItemMeta().addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
	   item.getItemMeta().addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
	   return item;
	}

	public static ItemStack unbreakable(ItemStack stack) {
		applyEnchant(stack, 10, Enchantment.DURABILITY, true);
		if (stack.getItemMeta() != null)
			stack.getItemMeta().spigot().setUnbreakable(true);
		return stack;
	}
	
	public static ItemStack anUnbreakable(ItemStack stack) {
		applyEnchant(stack, 10, Enchantment.DURABILITY, true);
		if (stack.getItemMeta() != null)
			stack.getItemMeta().spigot().setUnbreakable(true);
		return stack;
	}
	
	public static ItemStack createUnbreakable(Material stack, boolean enchant) {
		ItemStack item = create(stack);
		if(enchant)
			unbreakable(item);
		else
			anUnbreakable(item);
		
		return item;
		
	}
	
	public static ItemStack createUnbreakable(Material stack) {
		ItemStack item = create(stack);
		unbreakable(item);
		return item;
		
	}
	
	public static ItemStack createUnbreakable(Material stack, Enchantment enchant, int level, boolean show) {
		ItemStack item = create(stack, enchant, level);
		if(show)
			unbreakable(item);
		else
			anUnbreakable(item);
		return item;
		
	}
	
	public static ItemStack createUnbreakable(Material stack, Enchantment enchant, int level) {
		ItemStack item = create(stack, enchant, level);
		anUnbreakable(item);
		return item;
		
	}
	
	public static boolean hasName(ItemStack is, String name) {
		try {
			if (!is.hasItemMeta()) {
				return false;
			}
			if (!is.getItemMeta().hasDisplayName()) {
				return false;
			}
			if (is.getItemMeta().getDisplayName().equalsIgnoreCase(name)) {
				return true;
			}
			return false;
		} catch (Exception localException) {
		}
		return false;
	}

	public static ItemStack color(Color color, ItemStack... item) {
		ItemStack it = null;
		for (int i = 0; i < item.length; i++) {
			LeatherArmorMeta l = (LeatherArmorMeta) item[i].getItemMeta();
			l.setColor(color);
			item[i].setItemMeta(l);
			it = item[i];
		}
		return it;
	}

	public static List<String> colorList(List<String> list) {
		ArrayList<String> strings = new ArrayList<String>();
		Iterator<String> i = list.iterator();
		while (i.hasNext()) {
			String str = (String) i.next();
			strings.add(ChatColor.translateAlternateColorCodes('&', str));
		}
		return strings;
	}

	public static ItemStack createArmor(Material paramMaterial, Color paramColor) {
		ItemStack localItemStack = new ItemStack(paramMaterial, 1);
		LeatherArmorMeta localLeatherArmorMeta = (LeatherArmorMeta) localItemStack.getItemMeta();
		localLeatherArmorMeta.setColor(paramColor);
		localItemStack.setItemMeta(localLeatherArmorMeta);
		return unbreakable(localItemStack);
	}
	
	public static ItemStack createArmor(Material paramMaterial, String name, Color paramColor) {
		ItemStack localItemStack = new ItemStack(paramMaterial, 1);
		LeatherArmorMeta localLeatherArmorMeta = (LeatherArmorMeta) localItemStack.getItemMeta();
		localLeatherArmorMeta.setColor(paramColor);
		localLeatherArmorMeta.setDisplayName(name);
        localLeatherArmorMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS);
		localItemStack.setItemMeta(localLeatherArmorMeta);
		return unbreakable(localItemStack);
	}
	
	public static ItemStack createNormalArmor(Material paramMaterial, Color paramColor) {
		ItemStack localItemStack = new ItemStack(paramMaterial, 1);
		LeatherArmorMeta localLeatherArmorMeta = (LeatherArmorMeta) localItemStack.getItemMeta();
		localLeatherArmorMeta.setColor(paramColor);
		localItemStack.setItemMeta(localLeatherArmorMeta);
		return localItemStack;
	}

	public static ItemStack create(Material tipo, String nome, String lore, int quantidade, byte data,
			Enchantment enchant, int enchantLevel) {
		ItemStack createer = new ItemStack(tipo, quantidade, data);
		ItemMeta meta = createer.getItemMeta();
		meta.setDisplayName(nome);
		if (lore != null)
			meta.setLore(Arrays.asList(lore));
		meta.addEnchant(enchant, enchantLevel, true);
		createer.setItemMeta(meta);
		return createer;
	}

	public static ItemStack createSkull(String paramString) {
		@SuppressWarnings("deprecation")
		ItemStack localItemStack = new ItemStack(397, 1, (short) 3);
		SkullMeta localSkullMeta = (SkullMeta) localItemStack.getItemMeta();

		localSkullMeta.setOwner(paramString);
		localItemStack.setItemMeta(localSkullMeta);

		return localItemStack;
	}

	public static ItemStack createGlowed(Material tipo, String nome, String lore, int quantidade, byte data) {
		ItemStack builder = new ItemStack(tipo, quantidade, data);
		ItemMeta meta = builder.getItemMeta();
		if (lore != null)
			meta.setLore(Arrays.asList(lore));
		meta.setDisplayName(nome);
		builder.setItemMeta(meta);
		GlowUtils.addGlow(builder);
		return builder;
	}

	public static ItemStack createGlowed(Material tipo, String nome, List<String> lore, int quantidade, byte data) {
		ItemStack builder = new ItemStack(tipo, quantidade, data);
		ItemMeta meta = builder.getItemMeta();
		if (lore != null)
			meta.setLore(lore);
		meta.setDisplayName(nome);
		builder.setItemMeta(meta);
		GlowUtils.addGlow(builder);
		return builder;
	}

	public static ItemStack createGlowed(Material tipo, String nome, String lore, int quantidade) {
		ItemStack builder = new ItemStack(tipo, quantidade, (byte) 0);
		ItemMeta meta = builder.getItemMeta();
		if (lore != null)
			meta.setLore(Arrays.asList(lore));
		meta.setDisplayName(nome);
		builder.setItemMeta(meta);
		GlowUtils.addGlow(builder);
		return builder;

	}

	public static ItemStack createGlowed(Material tipo, String nome, String lore) {
		ItemStack builder = new ItemStack(tipo, 1, (byte) 0);
		ItemMeta meta = builder.getItemMeta();
		if (lore != null)
			meta.setLore(Arrays.asList(lore));
		meta.setDisplayName(nome);
		builder.setItemMeta(meta);
		GlowUtils.addGlow(builder);
		return builder;
	}

	public static ItemStack createGlowed(Material tipo, String nome, List<String> lore) {
		ItemStack builder = new ItemStack(tipo, 1, (byte) 0);
		ItemMeta meta = builder.getItemMeta();
		if (lore != null)
			meta.setLore(lore);
		meta.setDisplayName(nome);
		builder.setItemMeta(meta);
		GlowUtils.addGlow(builder);
		return builder;
	}

	public static ItemStack createGlowed(Material tipo, String nome) {
		ItemStack builder = new ItemStack(tipo, 1, (byte) 0);
		ItemMeta meta = builder.getItemMeta();
		meta.setDisplayName(nome);
		builder.setItemMeta(meta);
		GlowUtils.addGlow(builder);
		return builder;
	}

	public static ItemStack applyGlow(ItemStack s) {
		GlowUtils.addGlow(s);
		return s;
	}

	public static ItemStack createGlowed(Material tipo) {
		ItemStack builder = new ItemStack(tipo, 1, (byte) 0);
		ItemMeta meta = builder.getItemMeta();
		builder.setItemMeta(meta);
		GlowUtils.addGlow(builder);
		return builder;
	}

	public static ItemStack create(Material tipo, String nome, String lore, int quantidade, byte data,
			boolean enchantPass, int enchantLevel, Enchantment enchant) {
		ItemStack builder = new ItemStack(tipo, quantidade, data);
		ItemMeta meta = builder.getItemMeta();
		if (lore != null)
			meta.setLore(Arrays.asList(lore));
		meta.setDisplayName(nome);
		meta.addEnchant(enchant, enchantLevel, enchantPass);
		builder.setItemMeta(meta);
		return builder;
	}

	public static ItemStack create(Material tipo, String nome, List<String> lore, int quantidade, byte data,
			boolean enchantPass, int enchantLevel, Enchantment enchant) {
		ItemStack builder = new ItemStack(tipo, quantidade, data);
		ItemMeta meta = builder.getItemMeta();
		meta.setDisplayName(nome);
		if (lore != null)
			meta.setLore(lore);
		meta.addEnchant(enchant, enchantLevel, enchantPass);
		builder.setItemMeta(meta);
		return builder;
	}

	public static ItemStack create(Material tipo, String nome, List<String> lore, int level, Enchantment e) {
		ItemStack builder = new ItemStack(tipo, 1, (byte) 0);
		ItemMeta meta = builder.getItemMeta();
		if (nome != null)
			meta.setDisplayName(nome);
		if (lore != null)
			meta.setLore(lore);
		meta.addEnchant(e, level, true);
		builder.setItemMeta(meta);
		return builder;
	}

	public static ItemStack create(Material tipo, String nome, int level, Enchantment e) {
		ItemStack builder = new ItemStack(tipo, 1, (byte) 0);
		ItemMeta meta = builder.getItemMeta();
		meta.setDisplayName(nome);
		meta.addEnchant(e, level, true);
		builder.setItemMeta(meta);
		return builder;
	}

	public static ItemStack create(Material tipo, String nome, String lore, int level, Enchantment e) {
		ItemStack builder = new ItemStack(tipo, 1, (byte) 0);
		ItemMeta meta = builder.getItemMeta();
		meta.setDisplayName(nome);
		if (lore != null)
			meta.setLore(Arrays.asList(lore));
		meta.addEnchant(e, level, true);
		builder.setItemMeta(meta);
		return builder;
	}

	public static ItemStack create(Material tipo, String nome, String lore, int quantidade, byte data) {
		ItemStack builder = new ItemStack(tipo, quantidade, data);
		ItemMeta meta = builder.getItemMeta();
		if (lore != null)
			meta.setLore(Arrays.asList(lore));
		meta.setDisplayName(nome);
		builder.setItemMeta(meta);
		return builder;
	}

	public static ItemStack create(Material tipo, String nome, List<String> lore, int quantidade, byte data) {
		ItemStack builder = new ItemStack(tipo, quantidade, data);
		ItemMeta meta = builder.getItemMeta();
		meta.setDisplayName(nome);
		if (lore != null)
			meta.setLore(lore);
		builder.setItemMeta(meta);
		return builder;
	}

	public static ItemStack create(Material tipo, String nome, int quantidade, byte data) {
		ItemStack builder = new ItemStack(tipo, quantidade, data);
		ItemMeta meta = builder.getItemMeta();
		meta.setDisplayName(nome);
		builder.setItemMeta(meta);
		return builder;
	}

	public static ItemStack create(Material tipo, String nome, List<String> lore) {
		ItemStack builder = new ItemStack(tipo, 1);
		ItemMeta meta = builder.getItemMeta();
		meta.setDisplayName(nome);
		if (lore != null)
			meta.setLore(lore);
		builder.setItemMeta(meta);
		return builder;
	}

	public static ItemStack create(Material tipo, int amount, byte b) {
		ItemStack builder = new ItemStack(tipo, amount, b);
		ItemMeta meta = builder.getItemMeta();
		builder.setItemMeta(meta);
		return builder;
	}
	
	public static ItemStack createBanner(DyeColor color) {
		ItemStack builder = new ItemStack(Material.BANNER);
		BannerMeta meta = (BannerMeta) builder.getItemMeta();
		meta.setBaseColor(color);
		builder.setItemMeta(meta);
		return builder;
	}

	public static ItemStack create(Material tipo, int i) {
		ItemStack builder = new ItemStack(tipo, i, (byte) 0);
		ItemMeta meta = builder.getItemMeta();
		builder.setItemMeta(meta);
		return builder;
	}

	public static ItemStack create(Material tipo, String nome, String lore) {
		ItemStack builder = new ItemStack(tipo, 1, (byte) 0);
		ItemMeta meta = builder.getItemMeta();
		if (lore != null)
			meta.setLore(Arrays.asList(lore));
		meta.setDisplayName(nome);
		builder.setItemMeta(meta);
		return builder;
	}

	public static ItemStack create(Material tipo, String nome) {
		ItemStack builder = new ItemStack(tipo, 1, (byte) 0);
		ItemMeta meta = builder.getItemMeta();
		meta.setDisplayName(nome);
		builder.setItemMeta(meta);
		return builder;
	}

	public static ItemStack create(Material tipo, byte bytes) {
		ItemStack builder = new ItemStack(tipo, 1, bytes);
		return builder;
	}

	public static ItemStack create(Material tipo, String nome, byte bytes) {
		ItemStack builder = new ItemStack(tipo, 1, bytes);
		ItemMeta meta = builder.getItemMeta();
		meta.setDisplayName(nome);
		builder.setItemMeta(meta);
		return builder;
	}

	public static void applyName(ItemStack item, String name) {
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(name);
		item.setItemMeta(meta);
	}

	public static void applyLoreArrays(ItemStack item, List<String> lores) {
		ItemMeta meta = item.getItemMeta();
		meta.setLore(lores);
		item.setItemMeta(meta);
	}

	public static void applyLore(ItemStack item, String lore) {
		ItemMeta meta = item.getItemMeta();
		if (lore != null)
			meta.setLore(Arrays.asList(lore));
		item.setItemMeta(meta);
	}

	public static ItemStack applyEnchant(ItemStack item, int level, Enchantment enchant, boolean ignore) {
		ItemMeta meta = item.getItemMeta();
		meta.addEnchant(enchant, level, ignore);
		item.setItemMeta(meta);
		return item;
	}

	public static ItemStack applyEnchantment(ItemStack item, Enchantment enchant, int i) {
		ItemMeta meta = item.getItemMeta();
		meta.addEnchant(enchant, i, true);
		item.setItemMeta(meta);
		return item;

	}

	public static ItemStack create(Material mat, String name, String lore, byte data) {
		ItemStack builder = new ItemStack(mat, 1, data);
		ItemMeta meta = builder.getItemMeta();
		meta.setDisplayName(name);
		if (lore != null)
			meta.setLore(Arrays.asList(lore));
		builder.setItemMeta(meta);
		return builder;
	}

	public static ItemStack create(Material item, String name, List<String> lore, byte data) {
		ItemStack builder = new ItemStack(item, 1, data);
		ItemMeta meta = builder.getItemMeta();
		meta.setDisplayName(name);
		if (lore != null)
			meta.setLore(lore);
		builder.setItemMeta(meta);
		return builder;
	}

	public static ItemStack create(Material mat) {
		return new ItemStack(mat, 1);
	}

	public static ItemStack create(Material mat, Enchantment enchant, int level) {
		return create(mat, null, level, enchant);
	}

	public static ItemStack createSkull(Player p, String name, List<String> lore) {
		ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal());
		SkullMeta headMeta = (SkullMeta) head.getItemMeta();
		if (lore != null)
			headMeta.setLore(lore);
		if (name != null)
			headMeta.setDisplayName(name);

		GameProfile profile = RefClass.extractGameProfile(p);

		Field profileField;
		try {
			profileField = headMeta.getClass().getDeclaredField("profile");
			profileField.setAccessible(true);
			profileField.set(headMeta, profile);
		} catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e1) {
			e1.printStackTrace();
		}
		head.setItemMeta(headMeta);
		return head;
	}
	
	public static ItemStack createSkull(Player p, String name) {
		ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal());
		SkullMeta headMeta = (SkullMeta) head.getItemMeta();
		if (name != null)
			headMeta.setDisplayName(name);

		GameProfile profile = RefClass.extractGameProfile(p);

		Field profileField;
		try {
			profileField = headMeta.getClass().getDeclaredField("profile");
			profileField.setAccessible(true);
			profileField.set(headMeta, profile);
		} catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e1) {
			e1.printStackTrace();
		}
		head.setItemMeta(headMeta);
		return head;
	}
	
	public static ItemStack createSkull(GameProfile profile, String name) {
		ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal());
		SkullMeta headMeta = (SkullMeta) head.getItemMeta();
		if (name != null)
			headMeta.setDisplayName(name);

		Field profileField;
		try {
			profileField = headMeta.getClass().getDeclaredField("profile");
			profileField.setAccessible(true);
			profileField.set(headMeta, profile);
		} catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e1) {
			e1.printStackTrace();
		}
		head.setItemMeta(headMeta);
		return head;
	}

	public static ItemStack createSkull(String urlToFormat, int quantidade, String name, List<String> lore) {
		ItemStack head = new ItemStack(Material.SKULL_ITEM, quantidade, (short) SkullType.PLAYER.ordinal());

		SkullMeta headMeta = (SkullMeta) head.getItemMeta();
		headMeta.setLore(lore);
		headMeta.setDisplayName(name);
		GameProfile profile = new GameProfile(UUID.randomUUID(), null);
		profile.getProperties().put("textures", new Property("textures", urlToFormat));
		Field profileField;
		try {
			profileField = headMeta.getClass().getDeclaredField("profile");
			profileField.setAccessible(true);
			profileField.set(headMeta, profile);
		} catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e1) {
			e1.printStackTrace();
		}
		head.setItemMeta(headMeta);
		return head;
	}
	
	public static ItemStack createSkullWithTexture(String urlToFormat) {
		ItemStack head = new ItemStack(Material.SKULL_ITEM, (short) SkullType.PLAYER.ordinal());
		SkullMeta headMeta = (SkullMeta) head.getItemMeta();
		GameProfile profile = new GameProfile(UUID.randomUUID(), null);
		profile.getProperties().put("textures", new Property("textures", urlToFormat));
		Field profileField;
		try {
			profileField = headMeta.getClass().getDeclaredField("profile");
			profileField.setAccessible(true);
			profileField.set(headMeta, profile);
		} catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e1) {
			e1.printStackTrace();
		}
		head.setItemMeta(headMeta);
		return head;
	}
	
	public static ItemStack createSkull(GameProfile skin, int quantidade, String name, List<String> lore) {
		ItemStack head = new ItemStack(Material.SKULL_ITEM, quantidade, (short) SkullType.PLAYER.ordinal());

		SkullMeta headMeta = (SkullMeta) head.getItemMeta();
		headMeta.setLore(lore);
		headMeta.setDisplayName(name);
		GameProfile profile = new GameProfile(UUID.randomUUID(), null);
		profile.getProperties().putAll("textures", skin.getProperties().get("textures"));
		Field profileField;
		try {
			profileField = headMeta.getClass().getDeclaredField("profile");
			profileField.setAccessible(true);
			profileField.set(headMeta, profile);
		} catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e1) {
			e1.printStackTrace();
		}
		head.setItemMeta(headMeta);
		return head;
	}

	public static ItemStack createSkull(String textures, String name) {
		ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal());

		SkullMeta headMeta = (SkullMeta) head.getItemMeta();
		headMeta.setDisplayName(name);
		GameProfile profile = new GameProfile(UUID.randomUUID(), null);
		profile.getProperties().put("textures", new Property("textures", textures));
		Field profileField;
		try {
			profileField = headMeta.getClass().getDeclaredField("profile");
			profileField.setAccessible(true);
			profileField.set(headMeta, profile);
		} catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e1) {
			e1.printStackTrace();
		}
		head.setItemMeta(headMeta);
		return head;
	}

	public static ItemStack createSkull(String urlToFormat, int q) {
		ItemStack head = new ItemStack(Material.SKULL_ITEM, q, (short) SkullType.PLAYER.ordinal());

		SkullMeta headMeta = (SkullMeta) head.getItemMeta();
		GameProfile profile = new GameProfile(UUID.randomUUID(), null);
		profile.getProperties().put("textures", new Property("textures", urlToFormat));
		Field profileField;
		try {
			profileField = headMeta.getClass().getDeclaredField("profile");
			profileField.setAccessible(true);
			profileField.set(headMeta, profile);
		} catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e1) {
			e1.printStackTrace();
		}
		head.setItemMeta(headMeta);
		return head;
	}

	public static ItemStack separate(String config) {
		String item = StringUtils.substringBefore(config, ":");
		String id = StringUtils.substringAfter(config, ":");
		return create(Material.valueOf(item), Byte.parseByte(id));
	}

	public static Material separateMaterial(String config) {
		String item = StringUtils.substringBefore(config, ":");
		return Material.valueOf(item);
	}

	public static byte separateByte(String config) {
		String id = StringUtils.substringAfter(config, ":");
		if (id != "") {
			if (Integer.parseInt(id) == 0) {
				return (byte) 0;
			} else {
				return Byte.parseByte(id);
			}
		}
		return 0;
	}

	public static void createPotion(Player p, PotionType potionType, PotionEffectType effectType, String nome,
			int tempo, int nivel, int slot, int quantidade) {

		ItemStack stack = new Potion(potionType).toItemStack(quantidade);
		PotionMeta meta = (PotionMeta) stack.getItemMeta();

		meta.setDisplayName(nome);
		meta.addCustomEffect(new PotionEffect(effectType, tempo, nivel), true);
		stack.setItemMeta(meta);

		p.getInventory().setItem(slot, stack);

	}

	public static void createPotion(Player p, PotionType potionType, boolean splash, PotionEffectType effectType,
			String nome, int tempo, int nivel, int slot, int quantidade) {

		@SuppressWarnings("deprecation")
		ItemStack stack = new Potion(potionType, 1, splash).toItemStack(quantidade);
		PotionMeta meta = (PotionMeta) stack.getItemMeta();

		meta.setDisplayName(nome);
		meta.addCustomEffect(new PotionEffect(effectType, tempo, nivel), true);
		stack.setItemMeta(meta);

		p.getInventory().setItem(slot, stack);

	}

	public static ItemStack createPotion(PotionType potionType, boolean splash, PotionEffectType effectType,
			String nome, String lore, int tempo, int nivel, int quantidade) {
		@SuppressWarnings("deprecation")
		ItemStack stack = new Potion(potionType, 1, splash).toItemStack(quantidade);
		PotionMeta meta = (PotionMeta) stack.getItemMeta();

		if (nome != null)
			meta.setDisplayName(nome);
		if (lore != null)
			meta.setLore(Arrays.asList(lore));
		meta.addCustomEffect(new PotionEffect(effectType, tempo, nivel), true);
		stack.setItemMeta(meta);
		return stack;
	}

	public static ItemStack createPotion(PotionType potionType, boolean splash, PotionEffectType effectType,
			String nome, int tempo, int nivel, int quantidade) {
		@SuppressWarnings("deprecation")
		ItemStack stack = new Potion(potionType, 1, splash).toItemStack(quantidade);
		PotionMeta meta = (PotionMeta) stack.getItemMeta();

		meta.setDisplayName(nome);
		meta.addCustomEffect(new PotionEffect(effectType, tempo, nivel), true);
		stack.setItemMeta(meta);
		return stack;
	}

	public static ItemStack applyName(ItemStack itemStack, String name, String... lores) {
		ItemMeta itemMeta = itemStack.getItemMeta();

		if (!name.isEmpty()) {
			itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
		}

		if (lores.length > 0) {
			List<String> loreList = new ArrayList<String>(lores.length);

			for (String lore : lores) {
				loreList.add(ChatColor.translateAlternateColorCodes('&', lore));
			}

			itemMeta.setLore(loreList);
		}

		itemStack.setItemMeta(itemMeta);
		return itemStack;
	}

	public static class EasyFactory {

		public static ItemStack getLeftArrow() {// <<
			return ItemFactory.createSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMzdhZWU5YTc1YmYwZGY3ODk3MTgzMDE1Y2NhMGIyYTdkNzU1YzYzMzg4ZmYwMTc1MmQ1ZjQ0MTlmYzY0NSJ9fX0=", 1, "§7Voltar", null);
		}

		public static ItemStack getRightArrow() { //>>
			return ItemFactory.createSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNjgyYWQxYjljYjRkZDIxMjU5YzBkNzVhYTMxNWZmMzg5YzNjZWY3NTJiZTM5NDkzMzgxNjRiYWM4NGE5NmUifX19", 1, "§7Próxima", null);
		}
		
		public static ItemStack getXArrow() { //x
			return ItemFactory.createSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTZjNjBkYTQxNGJmMDM3MTU5YzhiZThkMDlhOGVjYjkxOWJmODlhMWEyMTUwMWI1YjJlYTc1OTYzOTE4YjdiIn19fQ==", 1, "§7Fechar", null);
		}
		
		public static ItemStack getQuestionMark() {//TODO ?
			return ItemFactory.createSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNDZiYTYzMzQ0ZjQ5ZGQxYzRmNTQ4OGU5MjZiZjNkOWUyYjI5OTE2YTZjNTBkNjEwYmI0MGE1MjczZGM4YzgyIn19fQ==", 1, "§7???", null);
		}
		
		public static ItemStack getQuestionMark(String name, List<String> lore) {//TODO ?
			return ItemFactory.createSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNDZiYTYzMzQ0ZjQ5ZGQxYzRmNTQ4OGU5MjZiZjNkOWUyYjI5OTE2YTZjNTBkNjEwYmI0MGE1MjczZGM4YzgyIn19fQ==", 1, name, lore);
		}
		
		public static ItemStack reset(String name, List<String> lore) {
			return ItemFactory.create(Material.STAINED_GLASS, name, lore, 1, (byte) 14);
		}

		public static ItemStack resetGlowed(String name, List<String> lore) {
			return ItemFactory.createGlowed(Material.STAINED_GLASS, name, lore, 1, (byte) 14);
		}
	}
	
	public static class AlphabetBanner {

	    private final String alphabet;
	    public DyeColor baseColor;
	    public DyeColor dyeColor;
	    public boolean bordered;


	    public AlphabetBanner(String alphabet, DyeColor baseColor, DyeColor dyeColor, boolean bordered) {
	        this.alphabet = ChatColor.stripColor(alphabet.toUpperCase()).substring(0, 1);
	        this.baseColor = baseColor;
	        this.dyeColor = dyeColor;
	        this.bordered = bordered;
	    }

	    public static ItemStack get(String name, String alphabet) {
	        return get(name, alphabet, DyeColor.WHITE, DyeColor.BLACK, true);
	    }

	    public static ItemStack get(String name, String alphabet, DyeColor baseColor, DyeColor dyeColor, boolean bordered) {
	        AlphabetBanner alphabetBanner = new AlphabetBanner(alphabet, baseColor, dyeColor, bordered);
	        return alphabetBanner.toItemStack(name);
	    }

	    public ItemStack toItemStack(String name) {
	        ItemStack banner = new ItemStack(Material.BANNER, 1);
	        BannerMeta bannerMeta = (BannerMeta) banner.getItemMeta();
	        bannerMeta.setBaseColor(dyeColor);
	        bannerMeta.setDisplayName(name);
	        bannerMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
	        if (!bordered) {

	            switch (alphabet) {
	                case "A":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_MIDDLE));
	                    break;
	                case "B":
	                case "8":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_MIDDLE));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    break;
	                case "C":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    break;
	                case "D":
	                    banner = new ItemStack(Material.BANNER, 1, DyeColorUtil.toShort(dyeColor));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.RHOMBUS_MIDDLE));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    break;
	                case "E":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_MIDDLE));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    break;
	                case "F":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_MIDDLE));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    break;
	                case "G":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.HALF_HORIZONTAL));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    break;
	                case "H":
	                    banner = new ItemStack(Material.BANNER, 1, DyeColorUtil.toShort(dyeColor));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    break;
	                case "I":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_CENTER));
	                    break;
	                case "J":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.HALF_HORIZONTAL));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    break;
	                case "K":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_MIDDLE));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_DOWNLEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_DOWNRIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    break;
	                case "L":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    break;
	                case "M":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.TRIANGLE_TOP));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.TRIANGLES_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    break;
	                case "N":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.TRIANGLE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_DOWNRIGHT));
	                    break;
	                case "O":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    break;
	                case "P":
	                    banner = new ItemStack(Material.BANNER, 1, DyeColorUtil.toShort(dyeColor));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.HALF_HORIZONTAL));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    break;
	                case "Q":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_DOWNRIGHT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.HALF_HORIZONTAL));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    break;
	                case "R":
	                    banner = new ItemStack(Material.BANNER, 1, DyeColorUtil.toShort(dyeColor));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.HALF_HORIZONTAL_MIRROR));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_DOWNRIGHT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.HALF_VERTICAL));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_MIDDLE));
	                    break;
	                case "S":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.TRIANGLE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.TRIANGLE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.SQUARE_TOP_RIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.SQUARE_BOTTOM_LEFT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.RHOMBUS_MIDDLE));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_DOWNRIGHT));
	                    break;
	                case "T":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_CENTER));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    break;
	                case "U":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    break;
	                case "V":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.TRIANGLES_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_DOWNLEFT));
	                    break;
	                case "W":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.TRIANGLE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.TRIANGLES_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    break;
	                case "X":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_DOWNLEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_DOWNRIGHT));
	                    break;
	                case "Y":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_DOWNRIGHT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.HALF_HORIZONTAL_MIRROR));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_DOWNLEFT));
	                    break;
	                case "Z":
	                case "2":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.TRIANGLE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.TRIANGLE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.SQUARE_TOP_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.SQUARE_BOTTOM_RIGHT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.RHOMBUS_MIDDLE));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_DOWNLEFT));
	                    break;
	                case "1":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.SQUARE_TOP_LEFT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_CENTER));
	                    break;
	                case "3":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_MIDDLE));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    break;
	                case "4":
	                    banner = new ItemStack(Material.BANNER, 1, DyeColorUtil.toShort(dyeColor));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.HALF_HORIZONTAL));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_MIDDLE));
	                    break;
	                case "5":
	                    banner = new ItemStack(Material.BANNER, 1, DyeColorUtil.toShort(dyeColor));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.HALF_VERTICAL_MIRROR));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.HALF_HORIZONTAL_MIRROR));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.DIAGONAL_RIGHT_MIRROR));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_DOWNRIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    break;
	                case "6":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.HALF_HORIZONTAL));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_MIDDLE));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    break;
	                case "7":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.DIAGONAL_RIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_DOWNLEFT));
	                    break;
	                case "9":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.HALF_HORIZONTAL_MIRROR));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_MIDDLE));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    break;
	                case "0":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_DOWNLEFT));
	                    break;
	                case "?":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.HALF_HORIZONTAL_MIRROR));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_MIDDLE));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.SQUARE_BOTTOM_LEFT));
	                    break;
	                case "!":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.HALF_HORIZONTAL));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_MIDDLE));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.SQUARE_BOTTOM_LEFT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.HALF_VERTICAL_MIRROR));
	                    break;
	                case ".":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.SQUARE_BOTTOM_LEFT));
	                    break;
	            }

	        } else {
	            //有框
	            switch (alphabet) {
	                case "A":
	                    banner = new ItemStack(Material.BANNER, 1, DyeColorUtil.toShort(dyeColor));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.HALF_HORIZONTAL));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "B":
	                case "8":
	                    banner = new ItemStack(Material.BANNER, 1, DyeColorUtil.toShort(dyeColor));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.STRIPE_CENTER));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_MIDDLE));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "C":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "D":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.CURLY_BORDER));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "E":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_MIDDLE));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "F":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_MIDDLE));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "G":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.HALF_HORIZONTAL));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "H":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_MIDDLE));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "I":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_CENTER));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "J":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.HALF_HORIZONTAL));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "K":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_MIDDLE));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.HALF_VERTICAL_MIRROR));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.CROSS));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "L":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "M":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.TRIANGLE_TOP));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.TRIANGLES_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "N":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.DIAGONAL_RIGHT_MIRROR));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_DOWNRIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "O":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "P":
	                    banner = new ItemStack(Material.BANNER, 1, DyeColorUtil.toShort(dyeColor));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.HALF_HORIZONTAL));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "Q":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.SQUARE_BOTTOM_RIGHT));
	                    break;
	                case "R":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.HALF_HORIZONTAL_MIRROR));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_DOWNRIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "S":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.RHOMBUS_MIDDLE));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_DOWNRIGHT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.CURLY_BORDER));
	                    break;
	                case "T":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_CENTER));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "U":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "V":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.TRIANGLES_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_DOWNLEFT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "W":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.TRIANGLE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.TRIANGLES_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "X":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.STRIPE_CENTER));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.CROSS));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "Y":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.CROSS));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.HALF_VERTICAL_MIRROR));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_DOWNLEFT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "Z":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_DOWNLEFT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "1":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.SQUARE_TOP_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_CENTER));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "2":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.RHOMBUS_MIDDLE));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_DOWNLEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "3":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_MIDDLE));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "4":
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.HALF_HORIZONTAL));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_MIDDLE));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "5":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_DOWNRIGHT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.CURLY_BORDER));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.SQUARE_BOTTOM_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "6":
	                    banner = new ItemStack(Material.BANNER, 1, DyeColorUtil.toShort(dyeColor));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.HALF_HORIZONTAL_MIRROR));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "7":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.DIAGONAL_RIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_DOWNLEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.SQUARE_BOTTOM_LEFT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "9":
	                    banner = new ItemStack(Material.BANNER, 1, DyeColorUtil.toShort(dyeColor));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.HALF_HORIZONTAL));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "0":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_BOTTOM));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_LEFT));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_DOWNLEFT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "?":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_RIGHT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.HALF_HORIZONTAL_MIRROR));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_TOP));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_MIDDLE));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.SQUARE_BOTTOM_LEFT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case "!":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.HALF_HORIZONTAL));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.STRIPE_MIDDLE));
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.SQUARE_BOTTOM_LEFT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.HALF_VERTICAL_MIRROR));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	                case ".":
	                    bannerMeta.addPattern(new Pattern(dyeColor, PatternType.SQUARE_BOTTOM_LEFT));
	                    bannerMeta.addPattern(new Pattern(baseColor, PatternType.BORDER));
	                    break;
	            }
	        }
	        banner.setItemMeta(bannerMeta);
	        return banner;
	    }
	}
}
