package com.overlands.core.utils;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;

public class Imobilizer implements Listener {

	protected static ArrayList<Player> imob = new ArrayList<>();

	public static void ImobilizePlayer(final Player toImobilize, int ticks) {
		toImobilize.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 9));
		toImobilize.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, -4));
		imob.add(toImobilize);
		if(GamePlayer.get(toImobilize).hasTitles() && GamePlayer.get(toImobilize).isWarnings()) {
			Util.sendTitle(toImobilize, 8, ticks * 20, 9, "", "&c&lATORDOADO");
		}
		new BukkitRunnable() {

			@Override
			public void run() {
				toImobilize.removePotionEffect(PotionEffectType.SLOW);
				toImobilize.removePotionEffect(PotionEffectType.JUMP);
				imob.remove(toImobilize);
				if(GamePlayer.get(toImobilize).hasTitles() && GamePlayer.get(toImobilize).isWarnings()) {
					Util.clearTitle(toImobilize);
				}
			}
		}.runTaskLater(Main.get(), ticks);
	}
	
	public static void ImobilizePlayerClear(final Player toImobilize, int ticks) {
		toImobilize.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 9));
		toImobilize.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, -4));
		imob.add(toImobilize);
		new BukkitRunnable() {

			@Override
			public void run() {
				toImobilize.removePotionEffect(PotionEffectType.SLOW);
				toImobilize.removePotionEffect(PotionEffectType.JUMP);
				imob.remove(toImobilize);
			}
		}.runTaskLater(Main.get(), ticks);
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onMove(PlayerMoveEvent e) {
		Player p = e.getPlayer();
		if (imob.contains(p)) {
			Location from = e.getFrom();
			if (from.getZ() != e.getTo().getZ() && from.getX() != e.getTo().getX()) {
				ParticleEffect.SPELL_WITCH.display(0, 0, 0, 3, 5, p.getLocation(), 12);
				p.teleport(from);

			}
		}
	}
}
