package com.overlands.core.utils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.scheduler.SchedulerEvent;
import com.overlands.core.utils.scheduler.UpdateType;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;

public class Launcher implements Listener {
	
	private Player p;
	private boolean cancelled  = false;
	
	
	public Launcher(Player player) {
		this.p = player;
		Bukkit.getPluginManager().registerEvents(this, Main.get());
		GamePlayer.get(player).setLaunched(true);
		launch();
	}
	
	@EventHandler
	public void onSchedule(SchedulerEvent e) {
		if(e.getType() == UpdateType.FASTEST) {
			if(!cancelled)
			 ActionBar.sendActionBar(p, MessageUtil.color("&9&lPressione &c&lSHIFT &9&lpara descer."));
		}
	}
	
	public void launch() {
		p.getWorld().playSound(p.getLocation(), Sounds.EXPLODE.bukkitSound(), 3.0F, 0.0F);
		p.setVelocity(p.getEyeLocation().getDirection().multiply(MathUtils.randomRange(1, 3.4)));
		p.setVelocity(new Vector(p.getVelocity().getX(), 1.3D, p.getVelocity().getZ()));
		new BukkitRunnable() {
			@Override
			public void run() {
				p.setVelocity(new Vector(0, 0, 0));
				GamePlayer.get(p).setLaunched(false);
				new BukkitRunnable() {
					
					@Override
					public void run() {
						if(cancelled) {
							p.setFallDistance(0F);
							un();
							cancel();
							return;
						}
						p.setVelocity(p.getVelocity().add(new Vector(0, 0.2, 0)));
					}
				}.runTaskTimer(Main.get(), 0, 1);
			}
			
		}.runTaskLater(Main.get(), 40);
	}
	
	@EventHandler
	public void shift(PlayerToggleSneakEvent e) {
		if(!cancelled)
			cancelled = true;
	}
	
	public void un() {
		HandlerList.unregisterAll(this);
	}
}
