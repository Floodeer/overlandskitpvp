package com.overlands.core.utils.block;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.overlands.core.utils.calc.MathUtils;
import com.overlands.kitpvp.Main;

public class BlockUtils {

	public static Map<Location, String> blocksToRestore = new HashMap<>();
	public static ArrayList<Block> blockstorestore = new ArrayList<>();
	public static HashSet<FallingBlock> blockToExplode = new HashSet<>();
	
	public static HashSet<Byte> blockUseSet = new HashSet<Byte>();
	public static HashSet<Byte> fullSolid = new HashSet<Byte>();
	public static HashSet<Byte> blockPassSet = new HashSet<Byte>();
	public static HashSet<Byte> blockAirFoliageSet = new HashSet<Byte>();

	static {
		blockAirFoliageSet.add((byte) 0);
		blockAirFoliageSet.add((byte) 6);
		blockAirFoliageSet.add((byte) 31);
		blockAirFoliageSet.add((byte) 32);
		blockAirFoliageSet.add((byte) 37);
		blockAirFoliageSet.add((byte) 38);
		blockAirFoliageSet.add((byte) 39);
		blockAirFoliageSet.add((byte) 40);
		blockAirFoliageSet.add((byte) 51);
		blockAirFoliageSet.add((byte) 59);
		blockAirFoliageSet.add((byte) 104);
		blockAirFoliageSet.add((byte) 105);
		blockAirFoliageSet.add((byte) 115);
		blockAirFoliageSet.add((byte) 141);
		blockAirFoliageSet.add((byte) 142);

		blockPassSet.add((byte) 0);
		blockPassSet.add((byte) 6);
		blockPassSet.add((byte) 8);
		blockPassSet.add((byte) 9);
		blockPassSet.add((byte) 10);
		blockPassSet.add((byte) 11);
		blockPassSet.add((byte) 26);
		blockPassSet.add((byte) 27);
		blockPassSet.add((byte) 28);
		blockPassSet.add((byte) 30);
		blockPassSet.add((byte) 31);
		blockPassSet.add((byte) 32);
		blockPassSet.add((byte) 37);
		blockPassSet.add((byte) 38);
		blockPassSet.add((byte) 39);
		blockPassSet.add((byte) 40);
		blockPassSet.add((byte) 50);
		blockPassSet.add((byte) 51);
		blockPassSet.add((byte) 55);
		blockPassSet.add((byte) 59);
		blockPassSet.add((byte) 63);
		blockPassSet.add((byte) 64);
		blockPassSet.add((byte) 65);
		blockPassSet.add((byte) 66);
		blockPassSet.add((byte) 68);
		blockPassSet.add((byte) 69);
		blockPassSet.add((byte) 70);
		blockPassSet.add((byte) 71);
		blockPassSet.add((byte) 72);
		blockPassSet.add((byte) 75);
		blockPassSet.add((byte) 76);
		blockPassSet.add((byte) 77);
		blockPassSet.add((byte) 78);
		blockPassSet.add((byte) 83);
		blockPassSet.add((byte) 90);
		blockPassSet.add((byte) 92);
		blockPassSet.add((byte) 93);
		blockPassSet.add((byte) 94);
		blockPassSet.add((byte) 96);
		blockPassSet.add((byte) 101);
		blockPassSet.add((byte) 102);
		blockPassSet.add((byte) 104);
		blockPassSet.add((byte) 105);
		blockPassSet.add((byte) 106);
		blockPassSet.add((byte) 107);
		blockPassSet.add((byte) 111);
		blockPassSet.add((byte) 115);
		blockPassSet.add((byte) 116);
		blockPassSet.add((byte) 117);
		blockPassSet.add((byte) 118);
		blockPassSet.add((byte) 119);
		blockPassSet.add((byte) 120);
		blockPassSet.add((byte) 171);

		fullSolid.add((byte) 1); //
		fullSolid.add((byte) 2); //
		fullSolid.add((byte) 3); //
		fullSolid.add((byte) 4); //
		fullSolid.add((byte) 5); //
		fullSolid.add((byte) 7); //
		fullSolid.add((byte) 12); //
		fullSolid.add((byte) 13); //
		fullSolid.add((byte) 14); //
		fullSolid.add((byte) 15); //
		fullSolid.add((byte) 16); //
		fullSolid.add((byte) 17); //
		fullSolid.add((byte) 19); //
		fullSolid.add((byte) 20); //
		fullSolid.add((byte) 21); //
		fullSolid.add((byte) 22); //
		fullSolid.add((byte) 23); //
		fullSolid.add((byte) 24); //
		fullSolid.add((byte) 25); //
		fullSolid.add((byte) 29); //
		fullSolid.add((byte) 33); //
		fullSolid.add((byte) 35); //
		fullSolid.add((byte) 41); //
		fullSolid.add((byte) 42); //
		fullSolid.add((byte) 43); //
		fullSolid.add((byte) 44); //
		fullSolid.add((byte) 45); //
		fullSolid.add((byte) 46); //
		fullSolid.add((byte) 47); //
		fullSolid.add((byte) 48); //
		fullSolid.add((byte) 49); //
		fullSolid.add((byte) 56); //
		fullSolid.add((byte) 57); //
		fullSolid.add((byte) 58); //
		fullSolid.add((byte) 60); //
		fullSolid.add((byte) 61); //
		fullSolid.add((byte) 62); //
		fullSolid.add((byte) 73); //
		fullSolid.add((byte) 74); //
		fullSolid.add((byte) 79); //
		fullSolid.add((byte) 80); //
		fullSolid.add((byte) 82); //
		fullSolid.add((byte) 84); //
		fullSolid.add((byte) 86); //
		fullSolid.add((byte) 87); //
		fullSolid.add((byte) 88); //
		fullSolid.add((byte) 89); //
		fullSolid.add((byte) 91); //
		fullSolid.add((byte) 95); //
		fullSolid.add((byte) 97); //
		fullSolid.add((byte) 98); //
		fullSolid.add((byte) 99); //
		fullSolid.add((byte) 100); //
		fullSolid.add((byte) 103); //
		fullSolid.add((byte) 110); //
		fullSolid.add((byte) 112); //
		fullSolid.add((byte) 121); //
		fullSolid.add((byte) 123); //
		fullSolid.add((byte) 124); //
		fullSolid.add((byte) 125); //
		fullSolid.add((byte) 126); //
		fullSolid.add((byte) 129); //
		fullSolid.add((byte) 133); //
		fullSolid.add((byte) 137); //
		fullSolid.add((byte) 138); //
		fullSolid.add((byte) 152); //
		fullSolid.add((byte) 153); //
		fullSolid.add((byte) 155); //
		fullSolid.add((byte) 158); //

		blockUseSet.add((byte) 23); // Dispenser
		blockUseSet.add((byte) 26); // Bed
		blockUseSet.add((byte) 33); // Piston
		blockUseSet.add((byte) 47); // Bookcase
		blockUseSet.add((byte) 54); // Chest
		blockUseSet.add((byte) 58); // Workbench
		blockUseSet.add((byte) 61); // Furnace
		blockUseSet.add((byte) 62); // Furnace
		blockUseSet.add((byte) 64); // Wood Door
		blockUseSet.add((byte) 69); // Lever
		blockUseSet.add((byte) 71); // Iron Door
		blockUseSet.add((byte) 77); // Button
		blockUseSet.add((byte) 85); // Fence (stupid minecraft)
		blockUseSet.add((byte) 93); // Repeater
		blockUseSet.add((byte) 94); // Repeater
		blockUseSet.add((byte) 96); // Trapdoor
		blockUseSet.add((byte) 107); // Fence Gate
		blockUseSet.add((byte) 113); // Nether Fence (stupid minecraft)
		blockUseSet.add((byte) 116); // Enchantment Table
		blockUseSet.add((byte) 117); // Brewing Stand
		blockUseSet.add((byte) 130); // Ender Chest
		blockUseSet.add((byte) 145); // Anvil
		blockUseSet.add((byte) 146); // Trapped Chest
		blockUseSet.add((byte) 154); // Hopper
		blockUseSet.add((byte) 158); // Dropper

		blockUseSet.add((byte) 184); // Fences/Gates
		blockUseSet.add((byte) 185); // Fences/Gates
		blockUseSet.add((byte) 186); // Fences/Gates
		blockUseSet.add((byte) 187); // Fences/Gates
		blockUseSet.add((byte) 188); // Fences/Gates
		blockUseSet.add((byte) 189); // Fences/Gates
		blockUseSet.add((byte) 190); // Fences/Gates
		blockUseSet.add((byte) 191); // Fences/Gates
		blockUseSet.add((byte) 192); // Fences/Gates

		blockUseSet.add((byte) 193); // Wood Doors
		blockUseSet.add((byte) 194); // Wood Doors
		blockUseSet.add((byte) 195); // Wood Doors
		blockUseSet.add((byte) 196); // Wood Doors
		blockUseSet.add((byte) 197); // Wood Doors
	}

	public static boolean solid(Block block) {
		if (block == null)
			return false;
		return solid(block.getTypeId());
	}

	public static boolean solid(int block) {
		return solid((byte) block);
	}

	public static boolean solid(byte block) {
		return !blockPassSet.contains(block);
	}

	public static boolean airFoliage(Block block) {
		if (block == null)
			return false;
		return airFoliage(block.getTypeId());
	}

	public static boolean airFoliage(int block) {
		return airFoliage((byte) block);
	}

	public static boolean airFoliage(byte block) {
		return blockAirFoliageSet.contains(block);
	}

	public static boolean fullSolid(Block block) {
		if (block == null)
			return false;

		return fullSolid(block.getTypeId());
	}

	public static boolean fullSolid(int block) {
		return fullSolid((byte) block);
	}

	public static boolean fullSolid(byte block) {
		return fullSolid.contains(block);
	}

	public static boolean usable(Block block) {
		if (block == null)
			return false;

		return usable(block.getTypeId());
	}

	public static boolean usable(int block) {
		return usable((byte) block);
	}

	public static boolean usable(byte block) {
		return blockUseSet.contains(block);
	}

	public static List<Block> getBlocksInRadius(Location location, int radius, boolean hollow) {
		List<Block> blocks = new ArrayList<>();

		int bX = location.getBlockX();
		int bY = location.getBlockY();
		int bZ = location.getBlockZ();

		for (int x = bX - radius; x <= bX + radius; x++) {
			for (int y = bY - radius; y <= bY + radius; y++) {
				for (int z = bZ - radius; z <= bZ + radius; z++) {

					double distance = ((bX - x) * (bX - x) + (bY - y) * (bY - y) + (bZ - z) * (bZ - z));

					if (distance < radius * radius && !(hollow && distance < ((radius - 1) * (radius - 1)))) {
						Location l = new Location(location.getWorld(), x, y, z);
						if (l.getBlock().getType() != Material.BARRIER)
							blocks.add(l.getBlock());
					}
				}

			}
		}

		return blocks;
	}

	public static boolean isOnGround(Entity entity) {
		Block block = entity.getLocation().getBlock().getRelative(BlockFace.DOWN);
		if (block.getType().isSolid())
			return true;
		return false;
	}

	public static double getDistance(int x1, int z1, int x2, int z2) {
		int dx = x1 - x2;
		int dz = z1 - z2;
		return Math.sqrt((dx * dx + dz * dz));
	}

	@SuppressWarnings("deprecation")
	public static void forceRestore() {
		for (Location loc : blocksToRestore.keySet()) {
			Block b = loc.getBlock();
			String s = blocksToRestore.get(loc);
			Material m = Material.valueOf(s.split(",")[0]);
			byte d = Byte.valueOf(s.split(",")[1]);
			b.setType(m);
			b.setData(d);
		}
	}

	public static void restoreBlockAt(final Location LOCATION) {
		Bukkit.getScheduler().runTaskAsynchronously(Main.get(), new Runnable() {
			@SuppressWarnings("deprecation")
			@Override
			public void run() {
				if (!blocksToRestore.containsKey(LOCATION))
					return;
				Block b = LOCATION.getBlock();
				String s = blocksToRestore.get(LOCATION);
				Material m = Material.valueOf(s.split(",")[0]);
				byte d = Byte.valueOf(s.split(",")[1]);
				for (Player player : b.getLocation().getWorld().getPlayers())
					player.sendBlockChange(LOCATION, m, d);
				blocksToRestore.remove(LOCATION);
			}
		});
	}

	public static void sendBlockChange(final Block BLOCK, final Material NEW_TYPE, final byte NEW_DATA,
			final int TICK_DELAY) {
		Bukkit.getScheduler().runTaskAsynchronously(Main.get(), new Runnable() {
			@Override
			public void run() {
				if (blocksToRestore.containsKey(BLOCK.getLocation()))
					return;
				if (!blocksToRestore.containsKey(BLOCK.getLocation())) {
					blocksToRestore.put(BLOCK.getLocation(), BLOCK.getType().toString() + "," + BLOCK.getData());
					for (Player player : BLOCK.getLocation().getWorld().getPlayers())
						player.sendBlockChange(BLOCK.getLocation(), NEW_TYPE, NEW_DATA);
					Bukkit.getScheduler().runTaskLater(Main.get(), new Runnable() {
						@Override
						public void run() {
							restoreBlockAt(BLOCK.getLocation());
						}
					}, TICK_DELAY);
				}
			}
		});
	}


	public static void setToRestore(final Block BLOCK, final Material NEW_TYPE, final byte NEW_DATA, final int TICK_DELAY) {
		if (blocksToRestore.containsKey(BLOCK.getLocation()))
			return;
		if (BLOCK.getType() != Material.AIR && BLOCK.getType() != Material.SIGN_POST
				&& BLOCK.getType() != Material.CHEST && BLOCK.getType() != Material.STONE_PLATE
				&& BLOCK.getType() != Material.WOOD_PLATE && BLOCK.getType() != Material.WALL_SIGN
				&& BLOCK.getType() != Material.WALL_BANNER && BLOCK.getType() != Material.STANDING_BANNER
				&& BLOCK.getType() != Material.CROPS && BLOCK.getType() != Material.LONG_GRASS
				&& BLOCK.getType() != Material.SAPLING && BLOCK.getType() != Material.DEAD_BUSH
				&& BLOCK.getType() != Material.RED_ROSE && BLOCK.getType() != Material.RED_MUSHROOM
				&& BLOCK.getType() != Material.BROWN_MUSHROOM && BLOCK.getType() != Material.TORCH
				&& BLOCK.getType() != Material.LADDER && BLOCK.getType() != Material.VINE
				&& BLOCK.getType() != Material.DOUBLE_PLANT && BLOCK.getType() != Material.PORTAL
				&& BLOCK.getType() != Material.CACTUS && BLOCK.getType() != Material.WATER
				&& BLOCK.getType() != Material.STATIONARY_WATER && BLOCK.getType() != Material.LAVA
				&& BLOCK.getType() != Material.STATIONARY_LAVA && BLOCK.getType() != Material.PORTAL
				&& BLOCK.getType() != Material.ENDER_PORTAL && BLOCK.getType() != Material.SOIL
				&& BLOCK.getType() != Material.BARRIER && BLOCK.getType() != Material.COMMAND
				&& BLOCK.getType() != Material.DROPPER && BLOCK.getType() != Material.DISPENSER
				&& !BLOCK.getType().toString().toLowerCase().contains("door") && BLOCK.getType() != Material.BED
				&& BLOCK.getType() != Material.BED_BLOCK && !isPortalBlock(BLOCK) && BLOCK.getType().getId() != 43
				&& BLOCK.getType().getId() != 44) {
			if (!blocksToRestore.containsKey(BLOCK.getLocation())) {
				blocksToRestore.put(BLOCK.getLocation(), BLOCK.getType().toString() + "," + BLOCK.getData());
				for (Player player : BLOCK.getLocation().getWorld().getPlayers())
					player.sendBlockChange(BLOCK.getLocation(), NEW_TYPE, NEW_DATA);
				Bukkit.getScheduler().runTaskLater(Main.get(), new Runnable() {
					@Override
					public void run() {
						restoreBlockAt(BLOCK.getLocation());
					}
				}, TICK_DELAY);
			}
		}
	}

	public static boolean isPortalBlock(Block b) {
		for (BlockFace face : BlockFace.values())
			if (b.getRelative(face).getType() == Material.PORTAL)
				return true;
		return false;
	}

	public static HashMap<Block, Double> getInRadius(Location paramLocation, double paramDouble) {
		return getInRadius(paramLocation, paramDouble, 999.0D);
	}

	public static HashMap<Block, Double> getInRadius(Location paramLocation, double paramDouble1, double paramDouble2) {
		HashMap<Block, Double> localHashMap = new HashMap<Block, Double>();
		int i = (int) paramDouble1 + 1;
		for (int j = -i; j <= i; j++) {
			for (int k = -i; k <= i; k++) {
				for (int m = -i; m <= i; m++) {
					if (Math.abs(m) <= paramDouble2) {
						Block localBlock = paramLocation.getWorld().getBlockAt((int) (paramLocation.getX() + j),
								(int) (paramLocation.getY() + m), (int) (paramLocation.getZ() + k));

						double d = MathUtils.offset(paramLocation, localBlock.getLocation().add(0.5D, 0.5D, 0.5D));
						if (d <= paramDouble1) {
							localHashMap.put(localBlock, Double.valueOf(1.0D - d / paramDouble1));
						}
					}
				}
			}
		}
		return localHashMap;
	}

	public static HashMap<Block, Double> getInRadius(Block paramBlock, double paramDouble) {
		HashMap<Block, Double> localHashMap = new HashMap<Block, Double>();
		int i = (int) paramDouble + 1;
		for (int j = -i; j <= i; j++) {
			for (int k = -i; k <= i; k++) {
				for (int m = -i; m <= i; m++) {
					Block localBlock = paramBlock.getRelative(j, m, k);

					double d = MathUtils.offset(paramBlock.getLocation(), localBlock.getLocation());
					if (d <= paramDouble) {
						localHashMap.put(localBlock, Double.valueOf(1.0D - d / paramDouble));
					}
				}
			}
		}
		return localHashMap;
	}

	@SuppressWarnings("deprecation")
	public static boolean isBlock(ItemStack paramItemStack) {
		if (paramItemStack == null) {
			return false;
		}
		return (paramItemStack.getTypeId() > 0) && (paramItemStack.getTypeId() < 256);
	}

	public static boolean isSign(Material material) {
		if (material == Material.WALL_SIGN || material == Material.SIGN_POST || material == Material.SIGN)
			return true;
		return false;
	}

	public static Block getHighest(World paramWorld, int paramInt1, int paramInt2) {
		return getHighest(paramWorld, paramInt1, paramInt2, null);
	}

	public static Block getHighest(World paramWorld, int paramInt1, int paramInt2, HashSet<Material> paramHashSet) {
		Block localBlock = paramWorld.getHighestBlockAt(paramInt1, paramInt2);
		while ((airFoliage(localBlock)) || (localBlock.getType() == Material.LEAVES)
				|| ((paramHashSet != null) && (paramHashSet.contains(localBlock.getType())))) {
			localBlock = localBlock.getRelative(BlockFace.DOWN);
		}
		return localBlock.getRelative(BlockFace.UP);
	}

	public static ArrayList<Block> getSurrounding(Block paramBlock, boolean paramBoolean) {
		ArrayList<Block> localArrayList = new ArrayList<>();
		if (paramBoolean) {
			for (int i = -1; i <= 1; i++) {
				for (int j = -1; j <= 1; j++) {
					for (int k = -1; k <= 1; k++) {
						if ((i != 0) || (j != 0) || (k != 0)) {
							localArrayList.add(paramBlock.getRelative(i, j, k));
						}
					}
				}
			}
		} else {
			localArrayList.add(paramBlock.getRelative(BlockFace.UP));
			localArrayList.add(paramBlock.getRelative(BlockFace.DOWN));
			localArrayList.add(paramBlock.getRelative(BlockFace.NORTH));
			localArrayList.add(paramBlock.getRelative(BlockFace.SOUTH));
			localArrayList.add(paramBlock.getRelative(BlockFace.EAST));
			localArrayList.add(paramBlock.getRelative(BlockFace.WEST));
		}
		return localArrayList;
	}

	public boolean isVisible(Block paramBlock) {
		for (Block localBlock : getSurrounding(paramBlock, false)) {
			if (!localBlock.getType().isOccluding()) {
				return true;
			}
		}
		return false;
	}

	public static ArrayList<Block> getInBoundingBox(Location a, Location b) {
		ArrayList<Block> blocks = new ArrayList<Block>();

		for (int x = Math.min(a.getBlockX(), b.getBlockX()); x <= Math.max(a.getBlockX(), b.getBlockX()); x++)
			for (int y = Math.min(a.getBlockY(), b.getBlockY()); y <= Math.max(a.getBlockY(), b.getBlockY()); y++)
				for (int z = Math.min(a.getBlockZ(), b.getBlockZ()); z <= Math.max(a.getBlockZ(), b.getBlockZ()); z++) {
					Block block = a.getWorld().getBlockAt(x, y, z);

					if (block.getType() != Material.AIR)
						blocks.add(block);
				}

		return blocks;
	}
}