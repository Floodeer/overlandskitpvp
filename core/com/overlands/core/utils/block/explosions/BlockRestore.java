package com.overlands.core.utils.block.explosions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.scheduler.BukkitRunnable;

import com.overlands.core.utils.block.BlockUtils;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.scheduler.SchedulerEvent;
import com.overlands.core.utils.scheduler.TaskCallback;
import com.overlands.core.utils.scheduler.UpdateType;
import com.overlands.kitpvp.Main;

public class BlockRestore implements Listener {

	private HashMap<Block, BlockRestoreData> blocks = new HashMap<Block, BlockRestoreData>();
	
	public BlockRestore() {
		Main.get().getServer().getPluginManager().registerEvents(this, Main.get());
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onBlockBreak(BlockBreakEvent event) {
		if (contains(event.getBlock()))
			event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onBlockPlace(BlockPlaceEvent event) {
		if (contains(event.getBlockPlaced()))
			event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onPistonEvent(BlockPistonExtendEvent event) {
		if (event.isCancelled())
			return;

		Block push = event.getBlock();
		for (int i = 0; i < 13; i++) {
			push = push.getRelative(event.getDirection());

			if (push.getType() == Material.AIR)
				return;

			if (contains(push)) {
				push.getWorld().playEffect(push.getLocation(), Effect.STEP_SOUND, push.getTypeId());
				event.setCancelled(true);
				return;
			}
		}
	}

	@EventHandler
	public void onUpdate(SchedulerEvent event) {
		if (event.getType() != UpdateType.FAST)
			return;

		ArrayList<Block> toRemove = new ArrayList<Block>();

		Iterator<BlockRestoreData> data = blocks.values().iterator();
		for(int i = 0; i < 15; i++) {
			if(data.hasNext()) {
				BlockRestoreData next = data.next();
				if(next.expire())
					toRemove.add(next.block);
			}else {
				break;
			}
		}
		for (Block cur : toRemove)
			blocks.remove(cur);
	}

	public void restore(Block block) {
		if (!contains(block))
			return;

		blocks.remove(block).restore();
		
		if(blocks.isEmpty()) {
			Bukkit.getWorlds().get(0).getEntitiesByClass(Item.class).forEach(Entity::remove);
		}
	}

	public void restoreAll() {
		for (BlockRestoreData data : blocks.values())
			data.restore();

		blocks.clear();
	}
	
	public void restoreFakeSync(Collection<Block> blocks) {
		new BukkitRunnable() {
			Iterator<Block> data = blocks.iterator();
			@Override
			public void run() {
				for(int i = 0; i < 10; i++) {
					if(data.hasNext()) {
						data.next().getState().update();
					}else {
						blocks.clear();
						cancel();
						break;
					}
				}
			}
		}.runTaskTimer(Main.get(), 5, 5);
	}
	
	public void restoreAllSync() {
		new BukkitRunnable() {
			Iterator<BlockRestoreData> data = blocks.values().iterator();
			@Override
			public void run() {
				for(int i = 0; i < 10; i++) {
					if(data.hasNext()) {
						data.next().restore();
					}else {
						blocks.clear();
						cancel();
						break;
					}
				}
			}
		}.runTaskTimer(Main.get(), 5, 5);
	}

	
	public void restoreAllSync(TaskCallback callback) {
		new BukkitRunnable() {
			Iterator<BlockRestoreData> data = blocks.values().iterator();
			@Override
			public void run() {
				for(int i = 0; i < 10; i++) {
					if(data.hasNext()) {
						data.next().restore();
					}else {
						blocks.clear();
						cancel();
						callback.onCall();
						break;
					}
				}
			}
		}.runTaskTimer(Main.get(), 5, 5);
	}

	public HashSet<Location> restoreBlockAround(Material type, Location location, int radius) {
		HashSet<Location> restored = new HashSet<Location>();

		Iterator<Block> blockIterator = blocks.keySet().iterator();

		while (blockIterator.hasNext()) {
			Block block = blockIterator.next();

			if (block.getType() != type)
				continue;

			if (MathUtils.offset(block.getLocation().add(0.5, 0.5, 0.5), location) > radius)
				continue;

			restored.add(block.getLocation().add(0.5, 0.5, 0.5));

			blocks.get(block).restore();

			blockIterator.remove();
		}

		return restored;
	}

	public void add(Block block, int toID, byte toData, long expireTime) {
		add(block, toID, toData, block.getTypeId(), block.getData(), expireTime);
	}

	public void add(Block block, int toID, byte toData, int fromID, byte fromData, long expireTime) {
		if (!contains(block))
			getBlocks().put(block, new BlockRestoreData(block, toID, toData, fromID, fromData, expireTime, 0));
		else
			getData(block).update(toID, toData, expireTime);
	}

	public void snow(Block block, byte heightAdd, byte heightMax, long expireTime, long meltDelay, int heightJumps) {

		if (((block.getTypeId() == 78 && block.getData() >= (byte) 7) || block.getTypeId() == 80)
				&& getData(block) != null) {
			getData(block).update(78, heightAdd, expireTime, meltDelay);

			if (heightJumps > 0)
				snow(block.getRelative(BlockFace.UP), heightAdd, heightMax, expireTime, meltDelay, heightJumps - 1);
			if (heightJumps == -1)
				snow(block.getRelative(BlockFace.UP), heightAdd, heightMax, expireTime, meltDelay, -1);

			return;
		}

		if (!BlockUtils.solid(block.getRelative(BlockFace.DOWN)) && block.getRelative(BlockFace.DOWN).getTypeId() != 78)
			return;

		if (block.getRelative(BlockFace.DOWN).getTypeId() == 78
				&& block.getRelative(BlockFace.DOWN).getData() < (byte) 7)
			return;

		if (block.getRelative(BlockFace.DOWN).getTypeId() == 79 || block.getRelative(BlockFace.DOWN).getTypeId() == 174)
			return;

		if (block.getRelative(BlockFace.DOWN).getTypeId() == 44 || block.getRelative(BlockFace.DOWN).getTypeId() == 126)
			return;

		if (block.getRelative(BlockFace.DOWN).getType().toString().contains("STAIRS"))
			return;

		if (block.getRelative(BlockFace.DOWN).getType().name().toLowerCase().contains("fence")
				|| block.getRelative(BlockFace.DOWN).getType().name().toLowerCase().contains("wall"))
			return;

		if (!BlockUtils.airFoliage(block) && block.getTypeId() != 78 && block.getType() != Material.CARPET)
			return;

		if (block.getTypeId() == 78)
			if (block.getData() >= (byte) (heightMax - 1))
				heightAdd = 0;

		if (!contains(block))
			getBlocks().put(block, new BlockRestoreData(block, 78, (byte) Math.max(0, heightAdd - 1), block.getTypeId(),
					block.getData(), expireTime, meltDelay));
		else
			getData(block).update(78, heightAdd, expireTime, meltDelay);
	}

	public boolean contains(Block block) {
		if (getBlocks().containsKey(block))
			return true;
		return false;
	}

	public BlockRestoreData getData(Block block) {
		if (blocks.containsKey(block))
			return blocks.get(block);
		return null;
	}

	public HashMap<Block, BlockRestoreData> getBlocks() {
		return blocks;
	}
}
