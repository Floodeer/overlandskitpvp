package com.overlands.core.utils.block.explosions;


import java.util.AbstractMap;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ExplosionPrimeEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.overlands.core.utils.block.BlockUtils;
import com.overlands.core.utils.calc.VelocityUtils;
import com.overlands.core.utils.scheduler.SchedulerEvent;
import com.overlands.core.utils.scheduler.UpdateType;
import com.overlands.core.utils.world.WorldUtils;
import com.overlands.kitpvp.Main;


public class Explosions implements Listener {
	
	private boolean regenerateGround = false;
	private boolean temporaryDebris = true;
	private boolean enableDebris = false;
	private boolean tntSpread = true;
	private boolean liquidDamage = true;
	private boolean explosionsEnabled = true;
	private Set<FallingBlock> explosionBlocks = Sets.newConcurrentHashSet();

	private BlockRestore blockRestore;

	public Explosions(BlockRestore blockRestore) {
		this.blockRestore = blockRestore;	
		
		Main.get().getServer().getPluginManager().registerEvents(this, Main.get());
		setRegenerate(true);
		setExplosions(true);
	}

	public BlockRestore getBlockRestore() {
		return blockRestore;
	}
	
	@EventHandler
	public void explosionPrime(ExplosionPrimeEvent event) {
		if (event.getRadius() >= 5)
			return;
		if(event.getEntity().hasMetadata("skills"))
			return;
		if(!explosionsEnabled) {
			event.setCancelled(true);
			return;
		}
		if(WorldUtils.inSpawn(event.getEntity().getLocation())) {
			event.setCancelled(true);
			return;
		}

		if (liquidDamage)
			for (Block block : BlockUtils.getInRadius(event.getEntity().getLocation(), (double) event.getRadius())
					.keySet())
				if (block.isLiquid())
					block.setTypeId(0);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void explosionEntity(EntityExplodeEvent event) {
		if (event.isCancelled()) {
			return;
		}

		if(event.getEntity().hasMetadata("skills")) {
			return;
		}
		
		if(!explosionsEnabled) {
			event.setCancelled(true);
			return;
		}
		
		if(WorldUtils.inSpawn(event.getEntity().getLocation())) {
			event.setCancelled(true);
			return;
		}
		
		try {
			if (event.getEntityType() == EntityType.CREEPER)
				event.blockList().clear();

			if (event.getEntityType() == EntityType.WITHER_SKULL)
				event.blockList().clear();
			
			event.blockList().forEach(block -> {
				if(BlockUtils.isSign(block.getType()) || 
						BlockUtils.isSign(block.getRelative(BlockFace.SOUTH).getType()) || 
						BlockUtils.isSign(block.getRelative(BlockFace.NORTH).getType()) ||
						BlockUtils.isSign(block.getRelative(BlockFace.EAST).getType())  ||
						BlockUtils.isSign(block.getRelative(BlockFace.WEST).getType())) {
					event.blockList().remove(block);
				}
			});
		} catch (Exception e) {
		}
		

		if (event.blockList().isEmpty())
			return;

		event.setYield(0f);

		final HashMap<Block, Entry<Integer, Byte>> blocks = new HashMap<Block, Entry<Integer, Byte>>();

		for (Block cur : event.blockList()) {
			if (cur.getTypeId() == 0 || cur.isLiquid())
				continue;

			if (cur.getType() == Material.CHEST || cur.getType() == Material.IRON_ORE
					|| cur.getType() == Material.COAL_ORE || cur.getType() == Material.GOLD_ORE
					|| cur.getType() == Material.DIAMOND_ORE) {
				cur.breakNaturally();
				continue;
			}

			blocks.put(cur, new AbstractMap.SimpleEntry<Integer, Byte>(cur.getTypeId(), cur.getData()));

			if (!regenerateGround) {
				if (cur.getTypeId() != 98 || (cur.getData() != 0 && cur.getData() != 3))
					cur.setTypeId(0);
			}else {
				int heightDiff = cur.getLocation().getBlockY() - event.getEntity().getLocation().getBlockY();
				blockRestore.add(cur, 0, (byte) 0, (long) (9000 + (heightDiff * 2600) + Math.random() * 2100));
			}
		}

		event.blockList().clear();

		final Entity fEnt = event.getEntity();
		final Location fLoc = event.getLocation();
		new BukkitRunnable() {

			@Override
			public void run() {
				for (Block cur : blocks.keySet()) {
					if (blocks.get(cur).getKey() == 98)
						if (blocks.get(cur).getValue() == 0 || blocks.get(cur).getValue() == 3)
							continue;

					if (tntSpread && blocks.get(cur).getKey() == 46) {
						TNTPrimed ent = cur.getWorld().spawn(cur.getLocation().add(0.5, 0.5, 0.5), TNTPrimed.class);
						Vector vec = VelocityUtils.getTrajectory(fEnt, ent);
						if (vec.getY() < 0)
							vec.setY(vec.getY() * -1);

						VelocityUtils.velocity(ent, vec, 1, false, 0, 0.6, 10, false);

						ent.setFuseTicks(10);
					} else {

						double chance = 0.85 + (double) explosionBlocks.size() / (double) 500;
						if (Math.random() > Math.min(0.975, chance)) {
							FallingBlock fall = cur.getWorld().spawnFallingBlock(cur.getLocation().add(0.5, 0.5, 0.5),
									blocks.get(cur).getKey(), blocks.get(cur).getValue());

							Vector vec = VelocityUtils.getTrajectory(fEnt, fall);
							if (vec.getY() < 0)
								vec.setY(vec.getY() * -1);

							VelocityUtils.velocity(fall, vec, 0.5 + 0.25 * Math.random(), false, 0,
									0.4 + 0.20 * Math.random(), 10, false);

							explosionBlocks.add(fall);
						}
					}
				}
				for (Block cur : BlockUtils.getInRadius(fLoc, 4d).keySet())
					if (cur.getTypeId() == 98)
						if (cur.getData() == 0 || cur.getData() == 3)
							cur.setTypeIdAndData(98, (byte) 2, true);
			}
		}.runTaskLater(Main.get(), 1);

	}

	@EventHandler
	public void explosionBlockUpdate(SchedulerEvent event) {
		if (event.getType() != UpdateType.TICK)
			return;

		Iterator<FallingBlock> fallingIterator = explosionBlocks.iterator();

		while (fallingIterator.hasNext()) {
			FallingBlock cur = fallingIterator.next();

			if (cur.isDead() || !cur.isValid() || cur.getTicksLived() > 400 || !cur.getWorld()
					.isChunkLoaded(cur.getLocation().getBlockX() >> 4, cur.getLocation().getBlockZ() >> 4)) {
				fallingIterator.remove();
				if (cur.getTicksLived() > 400 || !cur.getWorld().isChunkLoaded(cur.getLocation().getBlockX() >> 4,
						cur.getLocation().getBlockZ() >> 4)) {
					cur.remove();
					return;
				}

				Block block = cur.getLocation().getBlock();
				block.setTypeIdAndData(0, (byte) 0, true);

				if (enableDebris) {
					if (temporaryDebris) {
						blockRestore.add(block, cur.getBlockId(), cur.getBlockData(), 10000);
					} else {
						block.setTypeIdAndData(cur.getBlockId(), cur.getBlockData(), true);
					}
				} else {
					cur.getWorld().playEffect(block.getLocation(), Effect.STEP_SOUND, cur.getBlockId());
				}

				cur.remove();
			}
		}
	}

	@EventHandler
	public void explosionItemSpawn(ItemSpawnEvent event) {
		/**
		 * for (FallingBlock block : explosionBlocks)
			if (MathUtils.offset(event.getEntity().getLocation(), block.getLocation()) < 1)
			
			For now we don't need to spawn items, otherwise use this.
		 */
				event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOW)
	public void explosionBlocks(EntityExplodeEvent event) {
		if(event.getEntity().hasMetadata("skills"))
			return;
		
		if(!explosionsEnabled) {
			event.setCancelled(true);
			return;
		}
		
		if (event.getEntity() == null)
			event.blockList().clear();
	}

	public void setRegenerate(boolean regenerate) {
		regenerateGround = regenerate;
	}

	public void setDebris(boolean value) {
		enableDebris = value;
	}

	public void setLiquidDamage(boolean value) {
		liquidDamage = value;
	}

	public void setTNTSpread(boolean value) {
		tntSpread = value;
	}

	public void setTemporaryDebris(boolean value) {
		temporaryDebris = value;
	}

	public Set<FallingBlock> getExplosionBlocks() {
		return explosionBlocks;
	}

	public void blockExplosion(Collection<Block> blockSet, Location mid, boolean onlyAbove) {
		blockExplosion(blockSet, mid, onlyAbove, true);
	}

	public void blockExplosion(Collection<Block> blockSet, Location mid, boolean onlyAbove, boolean removeBlock) {
		if (blockSet.isEmpty())
			return;

		final HashMap<Block, Entry<Integer, Byte>> blocks = new HashMap<Block, Entry<Integer, Byte>>();
		final List<Block> blackListed = Lists.newArrayList();
		
		for (Block cur : blockSet) {
			if (cur.getTypeId() == 0)
				continue;
			if(cur.getTypeId() == Material.BARRIER.getId())
				 blackListed.add(cur);
			else if(cur.getTypeId() == Material.SIGN.getId())
				 blackListed.add(cur);
			else if(cur.getTypeId() == Material.REDSTONE_TORCH_OFF.getId())
				 blackListed.add(cur);
			else if(cur.getTypeId() == Material.REDSTONE_TORCH_ON.getId())
				 blackListed.add(cur);
			else if(cur.getTypeId() == Material.REDSTONE_COMPARATOR.getId())
				 blackListed.add(cur);
			else if(cur.getTypeId() == Material.REDSTONE_COMPARATOR_ON.getId())
				 blackListed.add(cur);
			else if(cur.getTypeId() == Material.REDSTONE_COMPARATOR_OFF.getId())
				 blackListed.add(cur);
			else if(cur.getTypeId() == Material.REDSTONE.getId())
				 blackListed.add(cur);
			else if(cur.getTypeId() == Material.REDSTONE_BLOCK.getId())
				 blackListed.add(cur);
			else if(cur.getTypeId() == Material.REDSTONE_WIRE.getId())
				 blackListed.add(cur);
			else if(cur.getState() instanceof Sign)
				 blackListed.add(cur);
			else if(cur.getState() instanceof ItemFrame)
				 blackListed.add(cur);
			else if(cur.getType() == Material.IRON_PLATE) 
				 blackListed.add(cur);
			else if(cur.getType() == Material.CARPET) 
				 blackListed.add(cur);
			for(BlockFace face : BlockFace.values()) {
				if(cur.getRelative(face).getType() == Material.LADDER || cur.getRelative(face).getType() == Material.ITEM_FRAME || BlockUtils.isSign(cur.getRelative(face).getType())) {
					 blackListed.add(cur);
				}else if(cur.getRelative(face).getType() == Material.REDSTONE_TORCH_OFF || cur.getRelative(face).getType() == Material.REDSTONE_TORCH_ON) {
					 blackListed.add(cur);
				}else if(cur.getRelative(face).getType() == Material.REDSTONE || cur.getRelative(face).getType() == Material.REDSTONE_WIRE) {
					 blackListed.add(cur);
				}else if(cur.getType().toString().startsWith("REDSTONE")) {
					blackListed.add(cur);
				}else if(cur.getType().toString().endsWith("PLATE")) {
					blackListed.add(cur);
				}
			}
			if(blackListed.contains(cur)) {
				continue;
			}
			


			if (onlyAbove && cur.getY() < mid.getY())
				continue;
			
			  blocks.put(cur, new AbstractMap.SimpleEntry<Integer, Byte>(cur.getTypeId(), cur.getData()));

			if (removeBlock) {
				if(cur.getType() == Material.PISTON_EXTENSION ||
						cur.getType() == Material.PISTON_MOVING_PIECE ||
						cur.getType() == Material.PISTON_STICKY_BASE ||
						cur.getType() == Material.PISTON_BASE) {
					
					getBlockRestore().add(cur, Material.PISTON_STICKY_BASE.getId(), (byte)0, 5000);
					cur.setType(Material.AIR);
				}else {
					getBlockRestore().add(cur, cur.getTypeId(), cur.getData(), 5000);
					cur.setType(Material.AIR);
				}
			}else {
				BlockUtils.setToRestore(cur, Material.AIR, (byte)0, 45);
			}
		}

		final Location fLoc = mid;
		new BukkitRunnable() {

			@Override
			public void run() {
				for (Block cur : blocks.keySet()) {
					if (blocks.get(cur).getKey() == 98)
						if (blocks.get(cur).getValue() == 0 || blocks.get(cur).getValue() == 3)
							continue;

					double chance = 0.2 + (double) explosionBlocks.size() / (double) 80;
					if (Math.random() > Math.min(0.98, chance)) {
						FallingBlock fall = cur.getWorld().spawnFallingBlock(cur.getLocation().add(0.5, 0.5, 0.5),blocks.get(cur).getKey(), blocks.get(cur).getValue());

						Vector vec = VelocityUtils.getTrajectory(fLoc, fall.getLocation());
						if (vec.getY() < 0)
							vec.setY(vec.getY() * -1);

						VelocityUtils.velocity(fall, vec, 0.5 + 0.25 * Math.random(), false, 0,0.4 + 0.20 * Math.random(), 10, false);

						explosionBlocks.add(fall);
					}
				}
			}
		}.runTaskLater(Main.get(), 1);
	}

	public boolean hasExplosions() {
		return explosionsEnabled;
	}

	public void setExplosions(boolean explosionsEnabled) {
		this.explosionsEnabled = explosionsEnabled;
	}
}