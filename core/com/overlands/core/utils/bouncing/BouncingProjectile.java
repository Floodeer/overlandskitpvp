package com.overlands.core.utils.bouncing;

import java.lang.reflect.Field;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.material.TrapDoor;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.projectiles.BlockProjectileSource;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.Main;

public class BouncingProjectile implements Listener {

	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	public void onEntityShootBowEvent(EntityShootBowEvent event) {
		LivingEntity entity = event.getEntity();
		Entity projectile = event.getProjectile();

		if (((entity instanceof Player)) && (projectile.getType() == EntityType.ARROW)) {
			if (projectile.hasMetadata("bouncing")) {
				Projectile projectileP = (Projectile) projectile;
				LivingEntity target = findTarget(projectileP);
				if (target != null) {
					playEffect(projectile, ParticleEffect.FLAME);
					aimAtTarget(projectileP, target, projectileP.getVelocity().length());
				}
			}
		}
	}

	private LivingEntity findTarget(Projectile projectile) {
		ProjectileSource source = projectile.getShooter();
		LivingEntity shooterEntity = null;
		Block shooterBlock = null;

		if (source instanceof LivingEntity) {
			shooterEntity = (LivingEntity) source;
		} else if (source instanceof BlockProjectileSource) {
			shooterBlock = ((BlockProjectileSource) source).getBlock();
		}

		double radius = 150.0D;
		Location projectileLocation = projectile.getLocation();
		Vector projectileDirection = projectile.getVelocity().normalize();
		Vector projectileVector = projectileLocation.toVector();

		LivingEntity target = null;
		double minDotProduct = Double.MIN_VALUE;
		for (Entity entity : projectile.getNearbyEntities(radius, radius, radius)) {
			if (entity instanceof LivingEntity && !entity.equals(shooterEntity)) {
				LivingEntity living = (LivingEntity) entity;
				Location newTargetLocation = living.getEyeLocation();

				Vector toTarget = newTargetLocation.toVector().subtract(projectileVector).normalize();
				double dotProduct = toTarget.dot(projectileDirection);
				if (dotProduct > 0.97D
						&& (shooterEntity != null ? shooterEntity.hasLineOfSight(living)
								: this.canSeeBlock(living, shooterBlock, (int) radius))
						&& (target == null || dotProduct > minDotProduct)) {
					target = living;
					minDotProduct = dotProduct;
				}
			}
		}

		return target;
	}

	private boolean canSeeBlock(LivingEntity entity, Block block, int maxDistance) {
		Location blockLocation = block.getLocation();
		Vector blockVector = blockLocation.toVector();
		Location eyeLocation = entity.getEyeLocation();
		Vector dir = (eyeLocation.toVector().subtract(blockVector)).normalize();
		BlockIterator iterator = new BlockIterator(blockLocation.getWorld(), blockVector, dir, 0, maxDistance);

		while (iterator.hasNext()) {
			Block b = iterator.next();
			if (b.getType() != Material.AIR && !b.equals(block))
				return false;
		}
		return true;
	}

	private void aimAtTarget(final Projectile projectile, final LivingEntity target, final double speed) {
		Location projectileLocation = projectile.getLocation();
		Location targetLocation = target.getEyeLocation();
		if (target.isDead() || !target.isValid()
				|| !targetLocation.getWorld().getName().equals(projectileLocation.getWorld().getName())
				|| targetLocation.distanceSquared(projectileLocation) > 25000) {
			return;
		}

		Vector oldVelocity = projectile.getVelocity();

		Vector direction = targetLocation.toVector().subtract(projectileLocation.toVector()).normalize()
				.multiply(targetLocation.getY() > projectileLocation.getY() ? speed : speed / 3);
		projectile.setVelocity(oldVelocity.add(direction).normalize().multiply(speed));

		Bukkit.getServer().getScheduler().runTaskLater(Main.get(), new Runnable() {

			@Override
			public void run() {
				if (!projectile.isDead() && projectile.isValid() && !projectile.isOnGround()
						&& projectile.getTicksLived() < 600)
					aimAtTarget(projectile, target, speed);
			}
		}, 1L);
	}

	public static void playEffect(final Entity entity, ParticleEffect type) {
		type.display(0.0F, 0.0F, 0.0F, 0.0F, 1, entity.getLocation(), 256);
		new BukkitRunnable() {
			
			@Override
			public void run() {
				if (entity == null || entity.isDead() || !entity.isValid() || entity.isOnGround()) {
					cancel();
					return;
				}
				type.display(0.0F, 0.0F, 0.0F, 0.0F, 1, entity.getLocation(), 256);
			}
		}.runTaskTimer(Main.get(), 0, 10);
	}

	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	public void onProjectileHitEvent(ProjectileHitEvent event) {
		Projectile projectile = event.getEntity();
		ProjectileSource source = projectile.getShooter();
		EntityType projectileType = projectile.getType();

		if (projectile.hasMetadata("bouncing")) {
			Vector arrowVelocity = projectile.getVelocity();
			double speed = arrowVelocity.length();

			if (speed < 0.3D || (projectileType == EntityType.ARROW && speed < 0.5D)) {
				BouncyProjectileHitGroundEvent e = new BouncyProjectileHitGroundEvent(projectile, projectile.getLocation(), true);
				Bukkit.getPluginManager().callEvent(e);
				return;
			}

			Location arrowLocation = projectile.getLocation();
			Block hitBlock = arrowLocation.getBlock();

			BlockFace blockFace = BlockFace.UP;
			if (isWoodenTrigger(hitBlock.getType()))
				return;
			if (!isHollowUpDownType(hitBlock)) {
				BlockIterator blockIterator = new BlockIterator(arrowLocation.getWorld(), arrowLocation.toVector(),
						arrowVelocity, 0.0D, 3);

				Block previousBlock = hitBlock;
				Block nextBlock = blockIterator.next();
				if (isWoodenTrigger(nextBlock.getType()))
					return;
				while (blockIterator.hasNext() && (nextBlock.getType() == Material.AIR || nextBlock.isLiquid()
						|| nextBlock.equals(hitBlock))) {
					previousBlock = nextBlock;
					nextBlock = blockIterator.next();
					if (isWoodenTrigger(nextBlock.getType()))
						return;
				}
				blockFace = nextBlock.getFace(previousBlock);

			}

			if (blockFace != null) {
				if (blockFace == BlockFace.SELF) {
					blockFace = BlockFace.UP;
				}

				if (isWoodenTrigger(hitBlock.getRelative(blockFace).getType()))
					return;

				Vector mirrorDirection = new Vector(blockFace.getModX(), blockFace.getModY(), blockFace.getModZ());
				double dotProduct = arrowVelocity.dot(mirrorDirection);
				mirrorDirection = mirrorDirection.multiply(dotProduct).multiply(2.0D);
				speed *= 0.6D;

				Projectile newProjectile;
				if (projectileType == EntityType.ARROW) {
					newProjectile = projectile.getWorld().spawnArrow(arrowLocation,
							arrowVelocity.subtract(mirrorDirection), (float) speed, 4.0F);

					if (source instanceof Player) {
						Field field;
						try {
							Object entityArrow = newProjectile.getClass().getMethod("getHandle").invoke(newProjectile);
							field = entityArrow.getClass().getDeclaredField("fromPlayer");
							field.setAccessible(true);
							field.set(entityArrow, 1);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				} else {
					newProjectile = (Projectile) projectile.getWorld().spawnEntity(arrowLocation, projectile.getType());
					newProjectile.setVelocity(arrowVelocity.subtract(mirrorDirection).normalize().multiply(speed));
				}

				BouncyProjectileHitGroundEvent e = new BouncyProjectileHitGroundEvent(projectile, projectile.getLocation(), false);
				Bukkit.getPluginManager().callEvent(e);
				if(e.isCancelled()) {
					return;
				}
				newProjectile.setShooter(source);
				newProjectile.setFireTicks(projectile.getFireTicks());
				newProjectile.setMetadata("bouncing", new FixedMetadataValue(Main.get(), true));
				projectile.remove();
			}
		}
	}

	private boolean isHollowUpDownType(Block block) {
		Material type = block.getType();
		return isStep(type) || type == Material.CARPET || type == Material.SNOW || type == Material.DIODE_BLOCK_OFF
				|| type == Material.DIODE_BLOCK_ON || type == Material.REDSTONE_COMPARATOR_OFF
				|| type == Material.REDSTONE_COMPARATOR_ON || type == Material.CAULDRON || type == Material.BED_BLOCK
				|| type == Material.DAYLIGHT_DETECTOR || type == Material.RAILS || type == Material.DETECTOR_RAIL
				|| type == Material.POWERED_RAIL || type == Material.ACTIVATOR_RAIL || type == Material.GOLD_PLATE
				|| type == Material.IRON_PLATE || type == Material.STONE_PLATE
				|| (type == Material.TRAP_DOOR && !(new TrapDoor(type, block.getData()).isOpen()));
	}

	private boolean isStep(Material type) {
		return type == Material.STEP || type == Material.WOOD_STEP;
	}

	@SuppressWarnings("unused")
	private boolean isStair(Material type) {
		return type == Material.WOOD_STAIRS || type == Material.SANDSTONE_STAIRS || type == Material.COBBLESTONE_STAIRS
				|| type == Material.BRICK_STAIRS || type == Material.QUARTZ_STAIRS || type == Material.BIRCH_WOOD_STAIRS
				|| type == Material.NETHER_BRICK_STAIRS || type == Material.SMOOTH_STAIRS
				|| type == Material.SPRUCE_WOOD_STAIRS;
	}

	private boolean isWoodenTrigger(Material type) {
		return type == Material.WOOD_BUTTON || type == Material.WOOD_PLATE;
	}
	
	public static class BouncyProjectileHitGroundEvent extends Event implements Cancellable {
		private static final HandlerList handlers = new HandlerList();
		private Projectile fb;
		private Location hit;
		private boolean cancelled = false;
		private boolean last;
		
		public BouncyProjectileHitGroundEvent(Projectile fb, Location hit, boolean last) {
			this.hit = hit;
			this.fb = fb;
			this.last = last;
		}

		public Projectile getProjectile() {
			return fb;
		}

		public Location getHit() {
			return hit;
		}

		public boolean isLast() {
			return last;
		}

		public HandlerList getHandlers() {
			return handlers;
		}

		public static HandlerList getHandlerList() {
			return handlers;
		}

		public void setCancelled(boolean cancelled) {
			this.cancelled = cancelled;
		}

		public boolean isCancelled() {
			return cancelled;
		}
	}

}
