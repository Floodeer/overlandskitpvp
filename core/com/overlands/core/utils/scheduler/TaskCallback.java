package com.overlands.core.utils.scheduler;

public interface TaskCallback {

	public void onCall();
}
