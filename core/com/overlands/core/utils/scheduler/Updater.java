package com.overlands.core.utils.scheduler;

import org.bukkit.plugin.java.JavaPlugin;

public class Updater implements Runnable {
	
	private final JavaPlugin plugin;

	public Updater(JavaPlugin plugin) {
		this.plugin = plugin;
		this.plugin.getServer().getScheduler().runTaskTimer(this.plugin, this, 0, 1);
	}

	@Override
	public void run() {
		for(UpdateType type : UpdateType.class.getEnumConstants()) {
			if(type.Elapsed()) {
				this.plugin.getServer().getPluginManager().callEvent(new SchedulerEvent(type));
			}
		}
	}
}
