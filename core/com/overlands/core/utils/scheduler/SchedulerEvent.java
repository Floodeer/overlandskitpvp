package com.overlands.core.utils.scheduler;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class SchedulerEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	private UpdateType type;

	public SchedulerEvent(UpdateType paramUpdateType) {
		this.type = paramUpdateType;
	}

	public UpdateType getType() {
		return this.type;
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}