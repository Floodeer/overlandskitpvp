package com.overlands.core.utils.entity;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import com.overlands.core.utils.reflection.Refs;
import com.overlands.core.utils.reflection.Refs.RefClass;
import com.overlands.core.utils.reflection.Refs.RefConstructor;
import com.overlands.core.utils.reflection.Refs.RefField;
import com.overlands.core.utils.reflection.Refs.RefMethod;

public class EntityDisguiser {

	private DisguiseType disguise;
	private Entity disguised;
	private RefClass entity;
	private Class<?> entityObject;
	private Object thisObject;

	public EntityDisguiser(DisguiseType d, Entity p) {
		disguise = d;
		this.disguised = p;
		Location location = disguised.getLocation();
		switch (disguise) {
		case ZOMBIE:
			entity = getEntity("EntityZombie", p);
			break;
		case WITHER_SKELETON:
			entity = getEntity("EntitySkeleton", p);

			RefMethod methodSkeleton = entity.findMethodByName("setSkeletonType");

			methodSkeleton.of(thisObject).call(1);
			break;
		case SKELETON:
			entity = getEntity("EntitySkeleton", p);
			break;
		case ZOMBIEPIG:
			entity = getEntity("EntityPigZombie", p);
			break;
		case BLAZE:
			entity = getEntity("EntityBlaze", p);
			break;
		case ENDERMAN:
			entity = getEntity("EntityEnderman", p);
			break;
		case CREEPER:
			entity = getEntity("EntityCreeper", p);
			break;
		case SPIDER:
			entity = getEntity("EntitySpider", p);
			break;
		case WITCH:
			entity = getEntity("EntityWitch", p);
			break;
		case WITHER_BOSS:
			entity = getEntity("EntityWither", p);
			break;
		case GHAST:
			entity = getEntity("EntityGhast", p);
			break;
		case GIANT:
			entity = getEntity("EntityGiant", p);
			break;
			
		case TNT_PRIMED:
			entity = getEntity("EntityTNTPrimed", p);
			break;
		}
		if (d != null) {

			RefMethod m = entity.getMethod("setPosition", double.class, double.class, double.class);
			RefMethod mm = entity.getMethod("d", int.class);
			RefMethod mmm = entity.getMethod("setCustomName", String.class);
			RefMethod mmmm = entity.getMethod("setCustomNameVisible", boolean.class);

			m.of(thisObject).call(location.getX(), location.getY(), location.getZ());
			mm.of(thisObject).call(disguised.getEntityId());
			mmm.of(thisObject).call(ChatColor.YELLOW + disguised.getName());
			mmmm.of(thisObject).call(true);

			RefField rf = entity.getField("locX");

			rf.of(thisObject).set(location.getX());

			RefField rf1 = entity.getField("locY");

			rf1.of(thisObject).set(location.getY());

			RefField rf2 = entity.getField("locZ");

			rf2.of(thisObject).set(location.getZ());

			RefField rf3 = entity.getField("yaw");

			rf3.of(thisObject).set(location.getYaw());

			RefField rf4 = entity.getField("pitch");

			rf4.of(thisObject).set(location.getPitch());

		}
	}

	@SuppressWarnings("deprecation")
	public Entity getPlayer() {
		return disguised;
	}

	@SuppressWarnings("deprecation")
	public void removeDisguise() {
		this.disguise = null;

		RefClass p29 = Refs.getRefClass("{nms}.PacketPlayOutEntityDestroy");

		RefClass p20 = Refs.getRefClass("{nms}.PacketPlayOutNamedEntitySpawn");

		RefConstructor pp20 = p20.getConstructor(Refs.getRefClass("{nms}.EntityHuman"));

		RefConstructor pp29 = p29.getConstructor(int[].class);

		int[] entityId;

		entityId = new int[1];

		entityId[0] = disguised.getEntityId();

		Object packetEntityDestroy = pp29.create(entityId);

		Object packetNamedEntitySpawn = pp20
				.create((Refs.getRefClass("{cb}.entity.CraftPlayer")).getMethod("getHandle").of(getPlayer()).call());

		RefClass classCraftPlayer = Refs.getRefClass("{cb}.entity.CraftPlayer");
		RefMethod methodGetHandle = classCraftPlayer.getMethod("getHandle");
		RefClass classEntityPlayer = Refs.getRefClass("{nms}.EntityPlayer");
		RefField fieldPlayerConnection = classEntityPlayer.getField("playerConnection");
		RefClass classPlayerConnection = Refs.getRefClass("{nms}.PlayerConnection");
		RefMethod methodSendPacket = classPlayerConnection.findMethodByName("sendPacket");

		for (Player player : Bukkit.getOnlinePlayers()) {

			if (player != getPlayer()) {
				Object handle = methodGetHandle.of(player).call();
				Object connection = fieldPlayerConnection.of(handle).get();

				methodSendPacket.of(connection).call(packetEntityDestroy);
				methodSendPacket.of(connection).call(packetNamedEntitySpawn);
			}
		}

	}

	public void changeDisguise(DisguiseType d) {
		removeDisguise();
		this.disguise = d;
		EntityDisguiser dis = new EntityDisguiser(d, disguised);
		dis.disguiseToAll();
	}

	@SuppressWarnings("deprecation")
	public void disguiseToAll() {

		RefClass p29 = Refs.getRefClass("{nms}.PacketPlayOutEntityDestroy");

		RefClass p20 = Refs.getRefClass("{nms}.PacketPlayOutSpawnEntityLiving");

		RefConstructor pp20 = p20.getConstructor(Refs.getRefClass("{nms}.EntityLiving"));

		RefConstructor pp29 = p29.getConstructor(int[].class);

		int[] entityId;

		entityId = new int[1];

		entityId[0] = disguised.getEntityId();

		Object packetEntityDestroy = pp29.create(entityId);

		Object packetNamedEntitySpawn = pp20.create(thisObject);

		RefClass classCraftPlayer = Refs.getRefClass("{cb}.entity.CraftPlayer");
		RefMethod methodGetHandle = classCraftPlayer.getMethod("getHandle");
		RefClass classEntityPlayer = Refs.getRefClass("{nms}.EntityPlayer");
		RefField fieldPlayerConnection = classEntityPlayer.getField("playerConnection");
		RefClass classPlayerConnection = Refs.getRefClass("{nms}.PlayerConnection");
		RefMethod methodSendPacket = classPlayerConnection.findMethodByName("sendPacket");

		for (Player all : Bukkit.getOnlinePlayers()) {
			Object handle = methodGetHandle.of(all).call();
			Object connection = fieldPlayerConnection.of(handle).get();

			methodSendPacket.of(connection).call(packetEntityDestroy);
			methodSendPacket.of(connection).call(packetNamedEntitySpawn);
		}
	}

	public static enum DisguiseType {
		ZOMBIE(Type.BIPED), WITHER_SKELETON(Type.BIPED), SKELETON(Type.BIPED), ZOMBIEPIG(Type.BIPED), BLAZE(
				Type.MOB), ENDERMAN(Type.MOB), CREEPER(Type.MOB), SPIDER(
						Type.MOB), WITCH(Type.MOB), WITHER_BOSS(Type.MOB), GHAST(Type.MOB), GIANT(Type.MOB),
		TNT_PRIMED(Type.MOB);

		private Type type;

		DisguiseType(Type type) {
			this.type = type;
		}

		public Type getType() {
			return type;
		}

		public boolean isBiped() {
			if (type == Type.BIPED) {
				return true;
			}
			return false;
		}

		public static enum Type {
			BIPED, MOB;
		}
	}

	@SuppressWarnings("deprecation")
	private RefClass getEntity(String entity, Entity p) {
		RefClass ent = Refs.getRefClass("{nms}." + entity);

		RefConstructor entConstructor = ent.getConstructor(Refs.getRefClass("{nms}.World"));

		RefClass classCraftWorld = Refs.getRefClass("{cb}.CraftWorld");
		RefMethod methodGetHandle = classCraftWorld.getMethod("getHandle");

		Object handle = methodGetHandle.of(p.getWorld()).call();

		Object fin = entConstructor.create(handle);

		this.thisObject = fin;
		this.entityObject = fin.getClass();

		return Refs.getRefClass(entityObject);
	}

}
