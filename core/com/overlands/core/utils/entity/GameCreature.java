package com.overlands.core.utils.entity;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Creature;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityTargetEvent;

import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.scheduler.SchedulerEvent;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.Game;


public abstract class GameCreature<T extends LivingEntity> implements Listener {

	private Game game;

	private String name;
	private T entity;

	private Location target;
	private long targetTime;

	public GameCreature(Game game, String name, Class<T> entityZZ, Location loc) {
		this.game = game;
		this.name = name;

		entity = loc.getWorld().spawn(loc, entityZZ);
		if (name != null) {
			entity.setCustomName(name);
			entity.setCustomNameVisible(true);
		}

		onSpawn(entity);
		Bukkit.getServer().getPluginManager().registerEvents(this, Main.get());
	 }
	
	@EventHandler
	public void onDeath(EntityDeathEvent e) {
		if(getEntity().equals(e.getEntity()))
			onDeath();
	}

	public String getName() {
		return name;
	}

	public T getEntity() {
		return entity;
	}

	public Location getTarget() {
		return target;
	}

	public void setTarget(Location loc) {
		target = loc;
		targetTime = System.currentTimeMillis();
	}

	public long getTargetTime() {
		return targetTime;
	}

	public Location getPlayerTarget() {
		if (game.getPlayers().size() == 0) {
			return Main.mapConfig.center;
		} else {
			Player target = game.getPlayers().get(MathUtils.random(game.getPlayers().size()-1)).getPlayer();
			return target.getLocation();
		}
	}

	public Location getRoamTarget() {
		if (Math.random() > 0.75)
			return getPlayerTarget();

		Location loc = null;

		while (loc == null || MathUtils.offset(loc, getEntity().getLocation()) < 16) {
			loc = new Location(Bukkit.getWorld("KitPvP"), MathUtils.random(80) - 40, 5, MathUtils.random(80) - 40);
		}

		return loc;
	}

	public void move(Creature creature) {

		setTarget(getRoamTarget());

		if (creature.getTarget() != null) {
			if (MathUtils.offset2d(creature, creature.getTarget()) > 12) {
				creature.setTarget(null);
			}
		}
		else {
			EntityUtils.move(creature, getTarget(), 1f);
			//game.moves++;
		}
	}

	public boolean updater(SchedulerEvent event) {
		if (entity == null || !entity.isValid())
			return true;

		onUpdate(event);

		return false;
	}

	public void onDeath() {
		HandlerList.unregisterAll(this);
	}
	
	public abstract void onSpawn(T ent);

	public abstract void onUpdate(SchedulerEvent event);

	public abstract void onDamage(EntityDamageEvent event);

	public abstract void target(EntityTargetEvent event);
}
