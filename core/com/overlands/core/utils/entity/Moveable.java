package com.overlands.core.utils.entity;

public interface Moveable {
	
	public void move();
}
