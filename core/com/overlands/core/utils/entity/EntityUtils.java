package com.overlands.core.utils.entity;

import java.lang.reflect.Field;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.BlockFace;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftCreature;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEnderDragon;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftLivingEntity;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Giant;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.util.Vector;

import com.overlands.core.utils.block.BlockUtils;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.calc.VelocityUtils;

import net.minecraft.server.v1_8_R3.EntityCreature;
import net.minecraft.server.v1_8_R3.EntityEnderDragon;
import net.minecraft.server.v1_8_R3.EntityHuman;
import net.minecraft.server.v1_8_R3.EntityInsentient;
import net.minecraft.server.v1_8_R3.EntityLiving;
import net.minecraft.server.v1_8_R3.GenericAttributes;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NavigationAbstract;
import net.minecraft.server.v1_8_R3.PathfinderGoal;
import net.minecraft.server.v1_8_R3.PathfinderGoalLookAtPlayer;
import net.minecraft.server.v1_8_R3.PathfinderGoalMoveTowardsRestriction;
import net.minecraft.server.v1_8_R3.PathfinderGoalRandomLookaround;
import net.minecraft.server.v1_8_R3.PathfinderGoalSelector;

public class EntityUtils {

	static private Field goalSelector;
	static private Field targetSelector;
	static private Field bsRestrictionGoal;
	static private Field pathfinderBList;
	static private Field pathfinderCList;

	public static void silentEntity(Entity e) {
		NBTTagCompound tag = new NBTTagCompound();
		((CraftEntity) e).getHandle().e(tag);
		tag.setInt("Silent", 1);
		((CraftEntity) e).getHandle().f(tag);
	}

	public static void modifySpeed(LivingEntity e, double to) {
		((CraftLivingEntity) e).getHandle().getAttributeInstance(GenericAttributes.MOVEMENT_SPEED).setValue(to);
	}

	public static void setHealth(EntityLiving e, float f) {
		e.setHealth(f);
	}
	
	public static void nav(Creature e, Location loc, float speed) {
		EntityCreature ec = ((CraftCreature) e).getHandle();
		NavigationAbstract nav = ec.getNavigation();
		nav.a(loc.getX(), loc.getY(), loc.getZ(), speed);
	}

	public static boolean isGrounded(Entity ent) {
		if (ent instanceof CraftEntity)
			return ((CraftEntity) ent).getHandle().onGround;

		return BlockUtils.solid(ent.getLocation().getBlock().getRelative(BlockFace.DOWN));
	}

	@SuppressWarnings("deprecation")
	public static boolean inWater(LivingEntity ent) {
		return ent.getLocation().getBlock().getTypeId() == 8 || ent.getLocation().getBlock().getTypeId() == 9;
	}
	
	public static boolean onBlock(Player player) {
		double xMod = player.getLocation().getX() % 1;
		if (player.getLocation().getX() < 0)
			xMod += 1;

		double zMod = player.getLocation().getZ() % 1;
		if (player.getLocation().getZ() < 0)
			zMod += 1;

		int xMin = 0;
		int xMax = 0;
		int zMin = 0;
		int zMax = 0;

		if (xMod < 0.3)
			xMin = -1;
		if (xMod > 0.7)
			xMax = 1;

		if (zMod < 0.3)
			zMin = -1;
		if (zMod > 0.7)
			zMax = 1;

		for (int x = xMin; x <= xMax; x++) {
			for (int z = zMin; z <= zMax; z++) {
				if (player.getLocation().add(x, -0.5, z).getBlock().getType() != Material.AIR
						&& !player.getLocation().add(x, -0.5, z).getBlock().isLiquid())
					return true;

				if (player.getLocation().add(x, 0, z).getBlock().getType() == Material.WATER_LILY)
					return true;

				Material beneath = player.getLocation().add(x, -1.5, z).getBlock().getType();
				if (player.getLocation().getY() % 0.5 == 0
						&& (beneath == Material.FENCE || beneath == Material.FENCE_GATE
								|| beneath == Material.NETHER_FENCE || beneath == Material.COBBLE_WALL))
					return true;
			}
		}
		return false;
	}

	public static boolean hitBox(Location loc, LivingEntity ent, double mult, EntityType disguise) {
		if (disguise != null) {
			if (disguise == EntityType.SQUID) {
				if (MathUtils.offset(loc, ent.getLocation().add(0, 0.4, 0)) < 0.6 * mult)
					return true;

				return false;
			}
		}

		if (ent instanceof Player) {
			Player player = (Player) ent;

			if (MathUtils.offset(loc, player.getEyeLocation()) < 0.4 * mult) {
				return true;
			} else if (MathUtils.offset2d(loc, player.getLocation()) < 0.6 * mult) {
				if (loc.getY() > player.getLocation().getY() && loc.getY() < player.getEyeLocation().getY()) {
					return true;
				}
			}
		} else {
			if (ent instanceof Giant) {
				if (loc.getY() > ent.getLocation().getY() && loc.getY() < ent.getLocation().getY() + 12)
					if (MathUtils.offset2d(loc, ent.getLocation()) < 4)
						return true;
			} else {
				if (loc.getY() > ent.getLocation().getY() && loc.getY() < ent.getLocation().getY() + 2)
					if (MathUtils.offset2d(loc, ent.getLocation()) < 0.5 * mult)
						return true;
			}
		}
		return false;
	}

	public static void move(Entity paramEntity, Location paramLocation, float paramFloat) {
		if (!(paramEntity instanceof Creature)) {
			return;
		}
		if (MathUtils.offset(paramEntity.getLocation(), paramLocation) < 0.1D) {
			return;
		}
		EntityCreature localEntityCreature = ((CraftCreature) paramEntity).getHandle();
		NavigationAbstract localNavigationAbstract = localEntityCreature.getNavigation();
		if (MathUtils.offset(paramEntity.getLocation(), paramLocation) > 24.0D) {
			Location localLocation = paramEntity.getLocation();

			localLocation.add(VelocityUtils.getTrajectory(paramEntity.getLocation(), paramLocation).multiply(24));

			localNavigationAbstract.a(localLocation.getX(), localLocation.getY(), localLocation.getZ(), paramFloat);
		} else {
			localNavigationAbstract.a(paramLocation.getX(), paramLocation.getY(), paramLocation.getZ(), paramFloat);
		}
	}

	public static boolean moveFast(Entity paramEntity, Location paramLocation, float paramFloat) {
		if (!(paramEntity instanceof Creature)) {
			return false;
		}
		if (MathUtils.offset(paramEntity.getLocation(), paramLocation) < 0.1D) {
			return false;
		}
		if (MathUtils.offset(paramEntity.getLocation(), paramLocation) < 2.0D) {
			paramFloat = Math.min(paramFloat, 1.0F);
		}
		EntityCreature localEntityCreature = ((CraftCreature) paramEntity).getHandle();
		localEntityCreature.getControllerMove().a(paramLocation.getX(), paramLocation.getY(), paramLocation.getZ(),
				paramFloat);

		return true;
	}

	public static void moveEnderDragon(Entity paramEntityToMove, Vector paramVector, Player paramEye) {
		EntityEnderDragon ec = ((CraftEnderDragon) paramEntityToMove).getHandle();

		ec.hurtTicks = -1;

		ec.getBukkitEntity().setVelocity(paramVector);

		ec.pitch = paramEye.getLocation().getPitch();
		ec.yaw = paramEye.getLocation().getYaw() - 180;
	}

	public static void addAI(Entity entity, int value, PathfinderGoal ai) {
		if (((CraftEntity) entity).getHandle() instanceof EntityInsentient) {
			EntityInsentient ei = ((EntityInsentient) ((CraftEntity) entity).getHandle());

			if (goalSelector == null) {
				try {
					goalSelector = EntityInsentient.class.getDeclaredField("goalSelector");
				} catch (NoSuchFieldException e) {
					e.printStackTrace();
					return;
				}
				goalSelector.setAccessible(true);
			}

			try {
				((PathfinderGoalSelector) goalSelector.get(ei)).a(value, ai);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}

	}

	public static void addLookAtPlayerAI(Entity entity, float dist) {
		if (((CraftEntity) entity).getHandle() instanceof EntityInsentient) {
			addAI(entity, 7, new PathfinderGoalLookAtPlayer(((EntityInsentient) ((CraftEntity) entity).getHandle()),
					EntityHuman.class, dist));
			addAI(entity, 8,
					new PathfinderGoalRandomLookaround(((EntityInsentient) ((CraftEntity) entity).getHandle())));
		}
	}

	public static void clearSelectorsAI(Entity entity) {
		try {
			if (goalSelector == null) {
				goalSelector = EntityInsentient.class.getDeclaredField("goalSelector");
				goalSelector.setAccessible(true);
			}

			if (targetSelector == null) {
				targetSelector = EntityInsentient.class.getDeclaredField("targetSelector");
				targetSelector.setAccessible(true);
			}

			if (pathfinderBList == null) {
				pathfinderBList = PathfinderGoalSelector.class.getDeclaredField("b");
				pathfinderBList.setAccessible(true);
			}

			if (pathfinderCList == null) {
				pathfinderCList = PathfinderGoalSelector.class.getDeclaredField("c");
				pathfinderCList.setAccessible(true);
			}

			if (entity instanceof CraftCreature) {
				EntityCreature creature = ((CraftCreature) entity).getHandle();

				if (bsRestrictionGoal == null) {
					bsRestrictionGoal = EntityCreature.class.getDeclaredField("c");
					bsRestrictionGoal.setAccessible(true);
				}

				bsRestrictionGoal.set(creature, new PathfinderGoalMoveTowardsRestriction(creature, 0D));
			}

			if (((CraftEntity) entity).getHandle() instanceof EntityInsentient) {
				EntityInsentient creature = (EntityInsentient) ((CraftEntity) entity).getHandle();

				((List<?>) pathfinderBList.get(((PathfinderGoalSelector) goalSelector.get(creature)))).clear();
				((List<?>) pathfinderCList.get(((PathfinderGoalSelector) goalSelector.get(creature)))).clear();

				((List<?>) pathfinderBList.get(((PathfinderGoalSelector) targetSelector.get(creature)))).clear();
				((List<?>) pathfinderCList.get(((PathfinderGoalSelector) targetSelector.get(creature)))).clear();
			}

		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
	}

	public static void removeGoalSelectors(Entity entity) {
		try {
			if (goalSelector == null) {
				goalSelector = EntityInsentient.class.getDeclaredField("goalSelector");
				goalSelector.setAccessible(true);
			}

			if (((CraftEntity) entity).getHandle() instanceof EntityInsentient) {
				EntityInsentient creature = (EntityInsentient) ((CraftEntity) entity).getHandle();

				PathfinderGoalSelector gs = new PathfinderGoalSelector(
						((CraftWorld) entity.getWorld()).getHandle().methodProfiler);

				goalSelector.set(creature, gs);
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
	}

	public static Entity addCreature(Entity e, Location l) {
		Entity ent = ((CraftWorld) l.getWorld()).addEntity(((CraftEntity) e).getHandle(), SpawnReason.CUSTOM);
		return ent;
	}
	
	public static void playDamageSound(LivingEntity damagee) 
	{
		Sound sound = Sound.HURT_FLESH;
		
		if (damagee.getType() == EntityType.BAT)				sound = Sound.BAT_HURT;
		else if (damagee.getType() == EntityType.BLAZE)			sound = Sound.BLAZE_HIT;
		else if (damagee.getType() == EntityType.CAVE_SPIDER)	sound = Sound.SPIDER_IDLE;
		else if (damagee.getType() == EntityType.CHICKEN)		sound = Sound.CHICKEN_HURT;
		else if (damagee.getType() == EntityType.COW)			sound = Sound.COW_HURT;
		else if (damagee.getType() == EntityType.CREEPER)		sound = Sound.CREEPER_HISS;
		else if (damagee.getType() == EntityType.ENDER_DRAGON)	sound = Sound.ENDERDRAGON_GROWL;
		else if (damagee.getType() == EntityType.ENDERMAN)		sound = Sound.ENDERMAN_HIT;
		else if (damagee.getType() == EntityType.GHAST)			sound = Sound.GHAST_SCREAM;
		else if (damagee.getType() == EntityType.GIANT)			sound = Sound.ZOMBIE_HURT;
		//else if (damagee.getType() == EntityType.HORSE)		sound = Sound.
		else if (damagee.getType() == EntityType.IRON_GOLEM)	sound = Sound.IRONGOLEM_HIT;
		else if (damagee.getType() == EntityType.MAGMA_CUBE)	sound = Sound.MAGMACUBE_JUMP;
		else if (damagee.getType() == EntityType.MUSHROOM_COW)	sound = Sound.COW_HURT;
		else if (damagee.getType() == EntityType.OCELOT)		sound = Sound.CAT_MEOW;
		else if (damagee.getType() == EntityType.PIG)			sound = Sound.PIG_IDLE;
		else if (damagee.getType() == EntityType.PIG_ZOMBIE)	sound = Sound.ZOMBIE_HURT;
		else if (damagee.getType() == EntityType.SHEEP)			sound = Sound.SHEEP_IDLE;
		else if (damagee.getType() == EntityType.SILVERFISH)	sound = Sound.SILVERFISH_HIT;
		else if (damagee.getType() == EntityType.SKELETON)		sound = Sound.SKELETON_HURT;
		else if (damagee.getType() == EntityType.SLIME)			sound = Sound.SLIME_ATTACK;
		else if (damagee.getType() == EntityType.SNOWMAN)		sound = Sound.STEP_SNOW;
		else if (damagee.getType() == EntityType.SPIDER)		sound = Sound.SPIDER_IDLE;
		//else if (damagee.getType() == EntityType.SQUID)		sound = Sound;
		//else if (damagee.getType() == EntityType.VILLAGER)	sound = Sound;
		//else if (damagee.getType() == EntityType.WITCH)		sound = Sound.;
		else if (damagee.getType() == EntityType.WITHER)		sound = Sound.WITHER_HURT;
		else if (damagee.getType() == EntityType.WOLF)			sound = Sound.WOLF_HURT;
		else if (damagee.getType() == EntityType.ZOMBIE)		sound = Sound.ZOMBIE_HURT;	

		damagee.getWorld().playSound(damagee.getLocation(), sound, 1.5f + (float)(0.5f * Math.random()), 0.8f + (float)(0.4f * Math.random()));
	}
}
