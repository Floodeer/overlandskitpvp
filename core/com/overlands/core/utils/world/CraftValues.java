package com.overlands.core.utils.world;

import java.lang.reflect.Field;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftArrow;
import org.bukkit.entity.Arrow;

import net.minecraft.server.v1_8_R3.EntityArrow;

public class CraftValues {
	
	public static Location getArrowCollisionLoc(Arrow arrow) {
		EntityArrow entityArrow = ((CraftArrow)arrow).getHandle();
		try {
			Field fieldX = EntityArrow.class.getDeclaredField("d");
			Field fieldY = EntityArrow.class.getDeclaredField("e");
			Field fieldZ = EntityArrow.class.getDeclaredField("f");

			fieldX.setAccessible(true);
			fieldY.setAccessible(true); 
			fieldZ.setAccessible(true);

			int x = fieldX.getInt(entityArrow);
			int y = fieldY.getInt(entityArrow);
			int z = fieldZ.getInt(entityArrow);
			return arrow.getWorld().getBlockAt(x, y, z).getLocation();
		}catch(SecurityException | NoSuchFieldException | IllegalAccessException ex ) {
			ex.printStackTrace();
		}
		return null;
	}


}
