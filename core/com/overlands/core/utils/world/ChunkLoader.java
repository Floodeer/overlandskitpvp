package com.overlands.core.utils.world;

import java.util.Iterator;
import java.util.Map;

import org.bukkit.Bukkit;

import com.google.common.collect.Maps;

public class ChunkLoader implements Runnable {

	private static ChunkLoader worldChunkLoader = null;

	private Map<WorldInfo, Runnable> worldRunnableMap = Maps.newHashMap();

	private long loadPassStart;
	private long maxPassTime = 25;

	private ChunkLoader() {
		Bukkit.getScheduler().scheduleSyncRepeatingTask(Bukkit.getPluginManager().getPlugins()[0], this, 0, 1L);
	}

	public static void addWorld(WorldInfo worldInfo, Runnable runnable) {
		if (worldChunkLoader == null) {
			worldChunkLoader = new ChunkLoader();
		}

		worldChunkLoader.worldRunnableMap.put(worldInfo, runnable);
	}

	@Override
	public void run() {
		loadPassStart = System.currentTimeMillis();

		Iterator<WorldInfo> worldInfoIterator = worldRunnableMap.keySet().iterator();

		while (worldInfoIterator.hasNext()) {
			WorldInfo worldInfo = worldInfoIterator.next();

			while (worldInfo.currentChunkX <= worldInfo.getMaxChunkX()) {
				while (worldInfo.currentChunkZ <= worldInfo.getMaxChunkZ()) {
					if (System.currentTimeMillis() - loadPassStart >= maxPassTime)
						return;

					worldInfo.getWorld().loadChunk(worldInfo.currentChunkX, worldInfo.currentChunkZ);
					worldInfo.currentChunkZ++;
				}

				worldInfo.currentChunkZ = worldInfo.getMinChunkZ();
				worldInfo.currentChunkX++;
			}

			worldRunnableMap.get(worldInfo).run();
			worldInfoIterator.remove();
		}
	}
}