package com.overlands.core.utils.world;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_8_R3.CraftChunk;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;

import net.minecraft.server.v1_8_R3.Block;
import net.minecraft.server.v1_8_R3.BlockPosition;

public class WorldBlockHandler {

	public static void change(World world, int x, int y, int z, int id, int data) {
		Chunk chunk = world.getChunkAt(x >> 4, z >> 4);
		net.minecraft.server.v1_8_R3.Chunk c = ((CraftChunk) chunk).getHandle();

		c.a(new BlockPosition(x & 0xF, y, z & 0xF), Block.getById(id).getBlockData());
		((CraftWorld) world).getHandle().notify(new BlockPosition(x, y, z));
	}

	public static void change(Location loc, Material to) {
		change(loc.getWorld(), loc.getBlockX(), loc.getBlockY(), loc.getBlockZ(), to);
	}

	public static void change(Location loc, int id, byte data) {
		change(loc.getWorld(), loc.getBlockX(), loc.getBlockY(), loc.getBlockZ(), id, data);
	}

	public static void change(World world, int x, int y, int z, Material to) {
		change(world, x, y, z, to, 0);
	}

	public static void change(World world, int x, int y, int z, Material to, int data) {
		change(world, x, y, z, to.getId(), data);
	}
	
	public static void chunkBlockSet(World world, int x, int y, int z, int id, byte data, boolean notifyPlayers)
	{
		world.getBlockAt(x, y, z).setTypeIdAndData(id, data, notifyPlayers);
	}
}
