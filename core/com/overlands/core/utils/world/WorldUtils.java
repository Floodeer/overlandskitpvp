package com.overlands.core.utils.world;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import com.overlands.core.utils.MessageUtil;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Difficulty;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.WorldCreator;
import org.bukkit.block.Block;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.util.Vector;

import com.google.common.collect.Lists;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.kitpvp.Main;

public class WorldUtils {

	public static List<Location> transformBlock = Lists.newArrayList();

	public static void putAll() {
		Location startLoc = Main.mapConfig.lowBorder;
		Location endLoc = Main.mapConfig.highBorder;

		final Location highPoints;
		final Location lowPoints;

		final int lowx = Math.min(startLoc.getBlockX(), endLoc.getBlockX());
		final int lowy = Math.min(startLoc.getBlockY(), endLoc.getBlockY());
		final int lowz = Math.min(startLoc.getBlockZ(), endLoc.getBlockZ());

		final int highx = Math.max(startLoc.getBlockX(), endLoc.getBlockX());
		final int highy = Math.max(startLoc.getBlockY(), endLoc.getBlockY());
		final int highz = Math.max(startLoc.getBlockZ(), endLoc.getBlockZ());

		highPoints = new Location(startLoc.getWorld(), highx, highy, highz);
		lowPoints = new Location(startLoc.getWorld(), lowx, lowy, lowz);

		Chunk cMin = lowPoints.getChunk();
		Chunk cMax = highPoints.getChunk();

		for (int cx = cMin.getX(); cx < cMax.getX(); cx++) {
			for (int cz = cMin.getZ(); cz < cMax.getZ(); cz++) {
				Chunk chunk = startLoc.getWorld().getChunkAt(cx, cz);
				chunk.load(true);
				int x = chunk.getX() << 4;
				int z = chunk.getZ() << 4;

				World world = chunk.getWorld();

				for (int xx = x; xx < x + 16; xx++) {
					for (int zz = z; zz < z + 16; zz++) {
						for (int yy = 0; yy < 256; yy++) {
							Block block = world.getBlockAt(xx, yy, zz);
							if(block.getType() == Material.SPONGE) {
								transformBlock.add(block.getLocation());
							}
						}
					}
				}
			}
		}
	}
	
	public static boolean inSpawn(Location loc) {
		Location startLoc = Main.mapConfig.lowBorderSpawn;
		Location endLoc = Main.mapConfig.highBorderSpawn;

		final Location highPoints;
		final Location lowPoints;

		final int lowx = Math.min(startLoc.getBlockX(), endLoc.getBlockX());
		final int lowy = Math.min(startLoc.getBlockY(), endLoc.getBlockY());
		final int lowz = Math.min(startLoc.getBlockZ(), endLoc.getBlockZ());

		final int highx = Math.max(startLoc.getBlockX(), endLoc.getBlockX());
		final int highy = Math.max(startLoc.getBlockY(), endLoc.getBlockY());
		final int highz = Math.max(startLoc.getBlockZ(), endLoc.getBlockZ());

		highPoints = new Location(startLoc.getWorld(), highx, highy, highz);
		lowPoints = new Location(startLoc.getWorld(), lowx, lowy, lowz);
		
		if (highPoints == null || lowPoints == null) {
			return false;
		}
		if (loc == null || !loc.getWorld().equals(highPoints.getWorld())) {
			return false;
		}

		return lowPoints.getBlockX() <= loc.getBlockX() && highPoints.getBlockX() >= loc.getBlockX() && lowPoints.getBlockZ() <= loc.getBlockZ() && highPoints.getBlockZ() >= loc.getBlockZ() && lowPoints.getBlockY() <= loc.getBlockY() && highPoints.getBlockY() >= loc.getBlockY();
	}
	
	public static World getWorld(String world) {
		return Bukkit.getServer().getWorld(world);
	}

	public static List<Block> getLoadedChunkBlocks(World w) {
		List<Block> b = Lists.newArrayList();
		for (Chunk c : w.getLoadedChunks()) {
			int cx = c.getX() << 4;
			int cz = c.getZ() << 4;
			for (int x = cx; x < cx + 16; x++) {
				for (int z = cz; z < cz + 16; z++) {
					for (int y = 0; y < 128; y++) {
						b.add(w.getBlockAt(x, y, z));
					}
				}
			}
		}
		return b;
	}

	public static String chunkToStr(Chunk chunk) {
		if (chunk == null)
			return "";

		return chunk.getWorld().getName() + "," + chunk.getX() + "," + chunk.getZ();
	}

	public static String chunkToStrClean(Chunk chunk) {
		if (chunk == null)
			return "";

		return "(" + chunk.getX() + "," + chunk.getZ() + ")";
	}

	public static Chunk strToChunk(String string) {
		try {
			String[] tokens = string.split(",");

			return getWorld(tokens[0]).getChunkAt(Integer.parseInt(tokens[1]), Integer.parseInt(tokens[2]));
		} catch (Exception e) {
			return null;
		}
	}

	public static String locToStr(Location loc) {
		if (loc == null)
			return "";

		return loc.getWorld().getName() + "," + MathUtils.trim(1, loc.getX()) + "," + MathUtils.trim(1, loc.getY())
				+ "," + MathUtils.trim(1, loc.getZ());
	}

	public static String locToStrClean(Location loc) {
		if (loc == null)
			return "Null";

		return "(" + loc.getBlockX() + ", " + loc.getBlockY() + ", " + loc.getBlockZ() + ")";
	}

	public static Location strToLoc(String string) {
		if (string.length() == 0)
			return null;

		String[] tokens = string.split(",");

		try {
			for (World cur : Bukkit.getServer().getWorlds()) {
				if (cur.getName().equalsIgnoreCase(tokens[0])) {
					return new Location(cur, Double.parseDouble(tokens[1]), Double.parseDouble(tokens[2]),
							Double.parseDouble(tokens[3]));
				}
			}
		} catch (Exception e) {
			return null;
		}

		return null;
	}

	public static Location locMerge(Location a, Location b) {
		a.setX(b.getX());
		a.setY(b.getY());
		a.setZ(b.getZ());
		return a;
	}

	public static String envToStr(Environment env) {
		if (env == Environment.NORMAL)
			return "Overworld";
		if (env == Environment.NETHER)
			return "Nether";
		if (env == Environment.THE_END)
			return "The End";
		return "Unknown";
	}

	public static World getWorldType(Environment env) {
		for (World cur : Bukkit.getServer().getWorlds())
			if (cur.getEnvironment() == env)
				return cur;

		return null;
	}

	public static Location averageLocation(Collection<Location> locs) {
		if (locs.isEmpty())
			return null;

		Vector vec = new Vector(0, 0, 0);
		double count = 0;

		World world = null;

		for (Location spawn : locs) {
			count++;
			vec.add(spawn.toVector());

			world = spawn.getWorld();
		}

		vec.multiply(1d / count);

		return vec.toLocation(world);
	}
	
	public static boolean loadWorld(String worldName) {
		WorldCreator worldCreator = new WorldCreator(worldName);
		worldCreator.generateStructures(false);
		worldCreator.generator(new ChunkGenerator() {
			@Override
			public List<BlockPopulator> getDefaultPopulators(World world) {
				return Arrays.asList(new BlockPopulator[0]);
			}

			@Override
			public boolean canSpawn(World world, int x, int z) {
				return true;
			}

			public byte[] generate(World world, Random random, int x, int z) {
				return new byte[32768];
			}
		
			public ChunkData generateChunkData(World world, Random random, int x, int z, BiomeGrid biome) {
				return createChunkData((org.bukkit.World) world);
				
			}

			@Override
			public Location getFixedSpawnLocation(World world, Random random) {
				return new Location(world, 0.0D, 64.0D, 0.0D);
			}
		});
		World world = worldCreator.createWorld();
		world.setDifficulty(Difficulty.NORMAL);
		world.setSpawnFlags(true, true);
		world.setStorm(false);
		world.setThundering(false);
		world.setPVP(true);
		world.setWeatherDuration(Integer.MAX_VALUE);
		world.setAutoSave(false);
		world.setKeepSpawnInMemory(false);
		world.setTicksPerAnimalSpawns(1);
		world.setTicksPerMonsterSpawns(1);

		world.setGameRuleValue("doMobSpawning", "false");
		world.setGameRuleValue("doFireTick", "false");
		world.setGameRuleValue("showDeathMessages", "false");

		boolean loaded = false;
		for (World w : Main.get().getServer().getWorlds()) {
			if (w.getName().equals(world.getName())) {
				loaded = true;
				break;
			}
		}
		return loaded;
	}

	public static void unloadWorld(String w) {
		World world = Main.get().getServer().getWorld(w);
		if (world != null) {
			Main.get().getServer().unloadWorld(world, true);
		}
	}

	public static void copyWorld(File source, File target) {
		try {
			FileUtils.copyDirectory(source, target);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void deleteWorld(String name) {
		unloadWorld(name);
		File target = new File(Main.get().getServer().getWorldContainer().getAbsolutePath(), name);
		deleteWorld(target);
	}

	public static boolean deleteWorld(File path) {
		if(path.exists()) {
			File[] files = path.listFiles();
			if (files != null) {
				for (File file: files) {
					if(file.isDirectory()) {
						deleteWorld(file);
					} else {
						file.delete();
					}
				}
			}
		}
		return(path.delete());
	}

	public static void closeSpawn(boolean br) {
		if(br) {
			Bukkit.broadcastMessage(MessageUtil.color("&8As portas do spawn foram fechadas temporariamente para o evento! Digite &c/espectador &8para espectar."));
		}
	}
}
