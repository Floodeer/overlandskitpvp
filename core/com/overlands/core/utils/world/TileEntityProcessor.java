package com.overlands.core.utils.world;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.block.BlockState;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitScheduler;

import com.google.common.base.Preconditions;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

public abstract class TileEntityProcessor {

    private Queue<Chunk> chunks;
    private Queue<BlockState> tileEntities;
    private Plugin plugin;
    private final long tickProcessingTimeNano;
    private BukkitRunnable task;

    public TileEntityProcessor(Queue<Chunk> chunks, long tickProcessingTime, TimeUnit tickProcessingUnit) {
        Preconditions.checkNotNull(chunks, "Chunks cannot be NULL.");
        Preconditions.checkNotNull(tickProcessingUnit, "Tick processing unit cannot be NULL.");

        this.chunks = chunks;
        this.tickProcessingTimeNano = tickProcessingUnit.toNanos(tickProcessingTime);
    }

    protected abstract void processTileEntity(BlockState state);

    public final boolean isRunning() {
        return task != null;
    }

    public final void start(Plugin plugin) {
        start(Bukkit.getScheduler(), plugin);
    }

    public final void start(BukkitScheduler scheduler, final Plugin plugin) {
        Preconditions.checkState(!isRunning(), "Processor is already running.");
        Preconditions.checkNotNull(scheduler, "Scheduler cannot be NULL.");
        Preconditions.checkNotNull(plugin, "Plugin cannot be NULL.");

        this.plugin = plugin;
       
        task = new BukkitRunnable() {
            @Override
            public void run() {
                long start = System.nanoTime();

                while (true) {
                    if (increment()) {
                        if (!processTileEntities(start)) {
                            break;
                        }
                    } else {
                        stop();
                        break;
                    }
                }
            }
        };
        task.runTask(plugin);
    }

    public final void stop() {
        Preconditions.checkState(isRunning(), "Cannot stop a stopped processor.");
        task.cancel();
        task = null;
    }

    private boolean processTileEntities(long start) {
        while (!tileEntities.isEmpty()) {
            if (System.nanoTime() > start + tickProcessingTimeNano) {
                return false;
            }

            BlockState state = tileEntities.poll();
            processTileEntity(state);
        }
        return true;
    }

    private boolean increment() {
        if (isEmpty(tileEntities)) {
            if (isEmpty(chunks)) {
                return false;
            }
            BlockState[] blockStates = chunks.poll().getTileEntities();
            tileEntities = new ArrayDeque<>(Arrays.asList(blockStates)
            );
            return true;
        }
        return false;
    }

    private boolean isEmpty(Queue<?> queue) {
        return queue == null || queue.isEmpty();
    }
}
 