package com.floodeer.scoreboards.global;

import java.util.ArrayList;

public abstract class ScoreboardElement {
	
	public abstract ArrayList<String> getLines();
}