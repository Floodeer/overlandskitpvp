package com.floodeer.scoreboards.global;

import java.util.ArrayList;
import java.util.HashMap;

public class ScoreboardElementScores extends ScoreboardElement {

	private String key;
	private HashMap<String, Integer> scores;
	private boolean higherIsBetter;

	public ScoreboardElementScores(String key, String line, int value, boolean higherIsBetter) {
		scores = new HashMap<String, Integer>();
		this.key = key;
		addScore(line, value);
		this.higherIsBetter = higherIsBetter;
	}

	public ArrayList<String> getLines() {
		ArrayList<String> orderedScores = new ArrayList<>();
		while (orderedScores.size() < scores.size()) {
			String bestKey = null;
			int bestScore = 0;
			for (String key : scores.keySet()) {
				if (!orderedScores.contains(key)) {
					if (bestKey == null || higherIsBetter && scores.get(key) >= bestScore
							|| !higherIsBetter && scores.get(key) <= bestScore) {
						bestKey = key;
						bestScore = scores.get(key);
					}
				}
			}
			orderedScores.add(bestKey);
		}
		return orderedScores;
	}

	public boolean isKey(String key) {
		return key.equals(key);
	}

	public void addScore(String line, int value) {
		scores.put(line, Integer.valueOf(value));
	}

	public String getKey() {
		return key;
	}

}