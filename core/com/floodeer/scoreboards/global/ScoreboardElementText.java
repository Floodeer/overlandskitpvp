package com.floodeer.scoreboards.global;

import java.util.ArrayList;

public class ScoreboardElementText extends ScoreboardElement {

	private String line;

	public ScoreboardElementText(String line) {
		this.line = line;
	}

	public ArrayList<String> getLines() {
		ArrayList<String> orderedScores = new ArrayList<>();
		orderedScores.add(this.line);
		return orderedScores;
	}
}