package com.floodeer.scoreboards.global;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.calc.MathUtils;

/**
 * This scoreboard class is used for global scoreboard.
 *
 */
public class GlobalScoreboard {

	private Scoreboard scoreboard;
	private Objective sideObjective;
	private ArrayList<ScoreboardElement> elements = new ArrayList<>();
	private String[] current = new String[15];
	private String title;
	private int shineIndex;
	private boolean shineDirection = true;

	public GlobalScoreboard(String name, boolean side) {
		this.title = "  " + MessageUtil.color(name) + "  ";
		this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
		if(side) {
			this.sideObjective = this.scoreboard.registerNewObjective("Obj" + MathUtils.random(), "dummy");
			this.sideObjective.setDisplaySlot(DisplaySlot.SIDEBAR);
			this.sideObjective.setDisplayName(Util.Bold + this.title);
		}
	}

	public Scoreboard getScoreboard() {
		return this.scoreboard;
	}

	public Objective getObjectiveSide() {
		return this.sideObjective;
	}

	public void updateTitle() {
		String out;
		if (this.shineDirection) {
			out = Util.cGold + Util.Bold;
		} else {
			out = Util.cWhite + Util.Bold;
		}
		for (int i = 0; i < this.title.length(); i++) {
			char c = this.title.charAt(i);
			if (this.shineDirection) {
				if (i == this.shineIndex) {
					out = out + Util.cYellow + Util.Bold;
				}
				if (i == this.shineIndex + 1) {
					out = out + Util.cWhite + Util.Bold;
				}
			} else {
				if (i == this.shineIndex) {
					out = out + Util.cYellow + Util.Bold;
				}
				if (i == this.shineIndex + 1) {
					out = out + Util.cGold + Util.Bold;
				}
			}
			out = out + c;
		}
		this.sideObjective.setDisplayName(out);

		this.shineIndex += 1;
		if (this.shineIndex == this.title.length() * 2) {
			this.shineIndex = 0;
			this.shineDirection = (!this.shineDirection);
		}
	}

	public void resetScore(String line) {
		this.scoreboard.resetScores(line);
	}

	public String clean(String line) {
		if (ChatColor.stripColor(line).length() > 18) {
			line = line.substring(0, 18);
		}
		return line;
	}

	public void write(String line) {
		line = clean(MessageUtil.color(line));

		this.elements.add(new ScoreboardElementText(line));
	}

	public void writeOrdered(String key, String line, int value, boolean prependScore) {
		if (prependScore) {
			line = value + " " + line;
		}
		line = clean(line);
		for (ScoreboardElement elem : this.elements) {
			if ((elem instanceof ScoreboardElementScores)) {
				ScoreboardElementScores scores = (ScoreboardElementScores) elem;
				if (scores.isKey(key)) {
					scores.addScore(line, value);
					return;
				}
			}
		}
		this.elements.add(new ScoreboardElementScores(key, line, value, true));
	}

	public void writeBlank() {
		 this.elements.add(new ScoreboardElementText(" "));
	}

	public void draw() {
		ArrayList<String> newLines = new ArrayList<String>();

		for (ScoreboardElement elem : elements) {
			for (String line : elem.getLines()) {
				while (true) {
					boolean matched = false;

					for (String otherLine : newLines) {
						if (line.equals(otherLine)) {
							line += ChatColor.RESET;
							matched = true;
						}
					}
					if (!matched)
						break;
				}
				newLines.add(line);
			}
		}

		HashSet<Integer> toAdd = new HashSet<Integer>();
		HashSet<Integer> toDelete = new HashSet<>();
		for (int i = 0; i < 15; i++) {
			if (i >= newLines.size()) {
				if (this.current[i] != null) {
					toDelete.add(i);
				}
			} else if ((this.current[i] == null) || (!this.current[i].equals(newLines.get(i)))) {
				toDelete.add(i);
				toAdd.add(Integer.valueOf(i));
			}
		}
		for (Iterator<Integer> localIterator2 = toDelete.iterator(); localIterator2.hasNext();) {
			int i = localIterator2.next();
			if (this.current[i] != null) {
				resetScore(this.current[i]);
				this.current[i] = null;
			}
		}
		for (Iterator<Integer> localIterator2 = toAdd.iterator(); localIterator2.hasNext();) {
			int i = localIterator2.next();
			String newLine = (String) newLines.get(i);
			getObjectiveSide().getScore(newLine).setScore(15 - i);
			this.current[i] = newLine;
		}
	}

	public void reset() {
		this.elements.clear();
	}
}