package com.overlands.kitpvp.contracts;

import java.util.List;

import com.google.common.collect.Table;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.google.common.collect.Lists;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.scheduler.SchedulerEvent;
import com.overlands.core.utils.scheduler.UpdateType;

public class ContractManager implements Listener {
	
	private final List<Contract> contracts = Lists.newArrayList();
	private final List<Contract> expired = Lists.newArrayList();

	public static Table contractTable;
	
	@EventHandler
	public void onScheduler(SchedulerEvent event) {
		if(event.getType() != UpdateType.TICK)
			return;
		if(contracts.isEmpty())
			return;
		
		contracts.forEach(contract -> {
			if(!contract.isActivated())
				return;
			if(contract.expired()) {
				expired.add(contract);
			}
		});
		
		expired.forEach(contract -> {
			Player owner = contract.getOwner();
			owner.sendMessage(MessageUtil.color("&9Seu contrato &c" + contract.getName() + " &9expirou!"));
		});
		
		expired.clear();
	}
}
