package com.overlands.kitpvp.contracts;

import java.util.List;

import org.bukkit.entity.Player;

public interface Contract {
	
	Player getOwner();
	
	int getId();

	long getActivationTime();
	
	String getName();

	List<String> getDescription();
	
	int getReward();

	int getExpReward();

	long getTimeLimit();

	long getCooldown();
	
	boolean expired();
	
	boolean isActivated();
}
