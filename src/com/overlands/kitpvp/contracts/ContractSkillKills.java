package com.overlands.kitpvp.contracts;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.bukkit.entity.Player;

import com.overlands.core.utils.MessageUtil;

public class ContractSkillKills implements Contract {

	private final Player owner;
	private final boolean activated;
	private long activation;
	
	public ContractSkillKills(Player owner) {
		this.owner = owner;
		this.activated = true;
	}
	
	@Override
	public String getName() {
		return "Skill Killer";
	}

	@Override
	public long getActivationTime() {
		return activation;
	}

	@Override
	public boolean expired() {
		return System.currentTimeMillis() - activation > getTimeLimit();
	}
	
	@Override
	public List<String> getDescription() {
		return MessageUtil.color(Arrays.asList(
				"&8Contrato de grande momento",
				" ",
				"&7Tarefa:",
				"&eMate 20 players usando sua",
				"&ehabilidade.",
				" ",
				"&7Tempo limite: &a5 minutos",
				"&7Recompensa: &63,500 coins&7, &610 exp!"));
	}
	
	@Override
	public int getId() {
		return 0;
	}
	
	@Override
	public long getTimeLimit() {
		return 300000;
	}

	@Override
	public long getCooldown() {
		return TimeUnit.DAYS.toMillis(1);
	}

	@Override
	public int getReward() {
		return 3500;
	}

	@Override
	public int getExpReward() {
		return 10;
	}

	@Override
	public boolean isActivated() {
		return activated;
	}
	
	public void start() {
		activation = System.currentTimeMillis();
	}
	
	public Player getOwner() {
		return owner;
	}
}
