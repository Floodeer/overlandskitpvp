package com.overlands.kitpvp.contracts;

import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.database.interfaces.IDatabase;
import com.overlands.kitpvp.database.util.SQLOperation;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.CompletableFuture;

public class ContractDatabase {

    private IDatabase database;

    public ContractDatabase(IDatabase database) {
        this.database = database;
    }

    public void getContract(int id, CompletableFuture<Contract> promise) {
        new SQLOperation() {
            final String query = "SELECT * FROM Contract WHERE id = ?";
            PreparedStatement statement;
            ResultSet resultSet;
            @Override
            public void onExecute() throws SQLException {
                statement = database.getConnection().get().prepareStatement(query);
                statement.setInt(1, id);
                resultSet = statement.executeQuery();

              
            }
        }.writeOperation(database.getExecutor(), Main.get().getLogger(), "SQL getContract operation failure.");
    }
}

