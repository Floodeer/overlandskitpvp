package com.overlands.kitpvp.mysterybox;

import java.util.Arrays;
import java.util.Map;

import org.apache.commons.lang.WordUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.google.common.collect.Maps;
import com.overlands.core.utils.MenuUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Runner;
import com.overlands.core.utils.Sounds;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.calc.RandomSet;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.effects.ClassEffectType;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.perks.PerkType;

public class MysteryBox implements Listener {
	
	private Map<Player, Block> using = Maps.newHashMap();
	private Map<Player, Location> playerUsing = Maps.newHashMap();
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if(e.getClickedBlock().getType() == Material.ENDER_CHEST) {
				e.setCancelled(true);
				Block b =  e.getClickedBlock();
				Player p = e.getPlayer();
				GamePlayer gp = GamePlayer.get(p);
				if(using.containsValue(b)) {
					p.sendMessage(MessageUtil.color("&cMysteryBox em uso, tente usar outra."));
					return;
				}
				if(gp.isOpeningMysteryBox()) {
					p.sendMessage(MessageUtil.color("&cVocê já está abrindo uma MysteryBox!"));
					return;
				}
				if(gp.getMysteryBoxes() <= 0) {
					p.sendMessage(MessageUtil.color("&cVocê não tem MysteryBoxes para abrir."));
					return;
				}
				using.put(p, b);
				Location location = b.getLocation().clone();
				location.add(0.5D, 4.5D, 0.5D);
				playerUsing.put(p, location);
				openMenu(p);
			}
		}
	}
	
	private void openMenu(Player p) {
		MenuUtils m = new MenuUtils(Main.get(), p, MessageUtil.color("&7MysteryBoxes"), 6);
		for(int i = 0; i < GamePlayer.get(p).getMysteryBoxes(); i++) {
			if(i >= m.getMenu().getSize()-9) {
				continue;
			}
			m.addItem(ItemFactory.create(Material.ENDER_CHEST, MessageUtil.color("&7MysteryBoxes #" + (i + 1))));
		}
		
		m.setItem(49, ItemFactory.EasyFactory.getXArrow());
		m.build();
		m.showMenu(p);
	}
	
	@EventHandler
	public void onClick(InventoryClickEvent e) {
		if(e.getInventory().getName().equalsIgnoreCase(MessageUtil.color("&7MysteryBoxes"))) {
			e.setCancelled(true);
			if(ItemFactory.isMat(e.getCurrentItem(), Material.ENDER_CHEST)) {
				Player p = (Player)e.getWhoClicked();
				open(p, using.get(p), playerUsing.get(p));
				e.getWhoClicked().closeInventory();
			}
			if(e.getRawSlot() == 49) {
				e.getWhoClicked().closeInventory();
			}
		}
	}
	
	@EventHandler
	public void onInvClose(InventoryCloseEvent e) {
		if(e.getInventory().getName().equalsIgnoreCase(MessageUtil.color("&7MysteryBoxes"))) {
			Player p = (Player) e.getPlayer();
			
			if(using.containsKey(p))
				using.remove(p);
			if(playerUsing.containsKey(p))
				playerUsing.remove(p);
		}
	}
	
	public void open(Player p, Block box, Location location) {
		GamePlayer.get(p).setMysteryBoxes(GamePlayer.get(p).getMysteryBoxes()-1);
		Main.getPM().getPlayer(p.getUniqueId()).setOpeningMysteryBox(true);
		playMusic(location);
		final ArmorStand mb = (ArmorStand) p.getWorld().spawn(location.clone(), ArmorStand.class);
		mb.setMetadata("mb", new FixedMetadataValue(Main.get(), null));
		mb.setGravity(false);
		mb.setVisible(false);
		mb.setCustomName(MessageUtil.color("&b&lMysteryBox de &a" + p.getName()));
		mb.setCustomNameVisible(true);
		mb.setHelmet(ItemFactory.create(Material.ENDER_CHEST));
		new BukkitRunnable() {
			int step = 0;
			ArmorStand lootAS;

			@Override
			public void run() {
				step += 1;
				if ((step == 240) || (!p.isOnline())) {
					Main.getPM().getPlayer(p.getUniqueId()).setOpeningMysteryBox(false);
					lootAS.remove();
					cancel();
					if(using.containsKey(p))
						using.remove(p);
					if(playerUsing.containsKey(p))
						playerUsing.remove(p);
				} else if (step <= 60) {
					mb.getWorld().playSound(mb.getEyeLocation(), Sounds.NOTE_PIANO.bukkitSound(),1.0F, 2.0F);
					mb.teleport(mb.getLocation().subtract(0.0D, 0.07D, 0.0D));
					ParticleEffect.FIREWORKS_SPARK.display(0, 0, 0, 0, 1, mb.getEyeLocation(), 120);
					ParticleEffect.REDSTONE.display(0, 0, 0, 0, 10, mb.getEyeLocation(), 120);
				} else if (step == 80) {
					ParticleEffect.ENCHANTMENT_TABLE.display(0, 0, 0, 1, 60, mb.getEyeLocation().add(0.0D, 1.0D, 0.0D), 120);
				} else if (step == 120) {
					mb.getWorld().playSound(mb.getLocation(), Sounds.EXPLODE.bukkitSound(), 1.0F,1.0F);
					ParticleEffect.LAVA.display(0, 0, 0, 1, 15, mb.getEyeLocation().add(0.0D, 1.0D, 0.0D), 120);
					new Runner(Main.get()).delay(3).run(() -> {
						ParticleEffect.SMOKE_NORMAL.display(0, 0, 0, 1, 40, mb.getEyeLocation(), 120);
						ParticleEffect.FLAME.display(0, 0, 0, 1, 40, mb.getEyeLocation(), 120);
					});
				} else if (step >= 160) {
					ParticleEffect.EXPLOSION_NORMAL.display(1, 0.5f, 1, 0, 1, mb.getLocation(), 120);
					ParticleEffect.CRIT_MAGIC.display(0.3f, 0.3f, 0.3f, 0, 1, mb.getLocation(), 120);
				}
				if (step == 160) {
					mb.remove();
					lootAS = mb.getWorld().spawn(mb.getLocation(), ArmorStand.class);
					lootAS.setMetadata("mb", new FixedMetadataValue(Main.get(), null));
					lootAS.setVisible(false);
					lootAS.setGravity(false);
					lootAS.setHelmet(ItemFactory.create(Material.ENDER_CHEST));
					displayEffects(lootAS);
					loot(GamePlayer.get(p), lootAS.getLocation());
				}
			}
		}.runTaskTimer(Main.get(), 0, 1);

	}

	private void playMusic(Location location) {
		new BukkitRunnable() {
			int step = 0;
			int step2 = 0;
			float increase = 0.0F;

			public void run() {
				step += 1;
				step2 += 1;
				if(location == null) {
					cancel();
					return;
				}
				if (step <= 60) {
					if (step2 == 1) {
						location.getWorld().playSound(location, Sounds.NOTE_BASS_GUITAR.bukkitSound(), 1.0F,
								0.5F + increase);
					} else if (step2 == 2) {
						location.getWorld().playSound(location, Sounds.NOTE_BASS_GUITAR.bukkitSound(), 1.0F,
								0.4F + increase);
					} else if (step2 == 3) {
						location.getWorld().playSound(location, Sounds.NOTE_BASS_GUITAR.bukkitSound(), 1.0F,
								0.55F + increase);
					} else if (step2 == 4) {
						location.getWorld().playSound(location, Sounds.NOTE_BASS_GUITAR.bukkitSound(), 1.0F,0.6F + increase);
						step2 = 0;
						increase += 0.08F;
					}
				} else {
					cancel();
				}
			}
		}.runTaskTimer(Main.get(), 0L, 2L);
	}

	private void displayEffects(ArmorStand a) {
		new BukkitRunnable() {
			int step = 0;
			double radius = 0.0D;
			ParticleEffect particleType = ParticleEffect.FIREWORKS_SPARK;

			public void run() {
				this.step += 1;
				if (this.step == 40) {
					cancel();
					return;
				}
				this.radius += 0.1D;
				for (Location location : LocationUtils.getCircle(a.getEyeLocation(), this.radius, 20)) {
					this.particleType.display(0.0F, 0.0F, 0.0F, 0, 1, location, 120);
				}
			}
		}.runTaskTimer(Main.get(), 0L, 1L);
	}

	public void loot(GamePlayer gp, Location location) {
		int coins = MathUtils.random(1500);
		int exp = MathUtils.random(60);
		double roll = MathUtils.random.nextDouble();
		if(roll < 0.25) {
			RandomSet<PerkType> perks = new RandomSet<>();
			Arrays.stream(PerkType.values()).forEach(perk -> perks.add(perk.getRarity(), perk));
			PerkType perk = perks.result();
			String name = WordUtils.capitalize(perk.getByName().replaceAll("_", " "));
			if(gp.getPerks().contains(perk.toString().toUpperCase())) {
				gp.getPlayer().sendMessage(MessageUtil.color("&9Você encontrou um perk repetido. " + "&a+ &6&l" + coins + " &9coins."));
				setTempHologram(location, perk.getDisplay(), MessageUtil.color("&9Perk &6" + name + " &9(REPETIDO)"));
			}else {
				setTempHologram(location, perk.getDisplay(), MessageUtil.color("&9Perk &6"  + name));
				gp.getPerks().add(perk.toString().toUpperCase());
				found(gp.getPlayer(), "&bPERK &6" + name);
			}
		}else if(roll < 0.50) {
			RandomSet<ClassEffectType> effects = new RandomSet<>();
			Arrays.stream(ClassEffectType.values()).forEach(effect -> effects.add(effect.getRarity(), effect));
			ClassEffectType effect = effects.result();
			String name = WordUtils.capitalize(effect.toString().replaceAll("_", " "));
			if(gp.getEffects().contains(effect.toString().toUpperCase())) {
				gp.getPlayer().sendMessage(MessageUtil.color("&9Você encontrou um efeito repetido. " + "&a+ &6&l" + coins + " &9coins."));
				setTempHologram(location, effect.getDisplay(), MessageUtil.color("Efeito " + name + " &9(REPETIDO)"));
			}else {
				setTempHologram(location, effect.getDisplay(), MessageUtil.color("&9Efeito &6"  + name));
				gp.getEffects().add(effect.toString().toUpperCase());
				found(gp.getPlayer(), "&bEFEITO &6" + name);
			}
			
		}else {
			if(MathUtils.randomBoolean()) {
				setTempHologram(location, ItemFactory.create(Material.GOLD_INGOT), "&a+ &6&l" + coins + " &9coins");
				found(gp.getPlayer(), coins + " coins");
				gp.setCoins(gp.getCoins()+coins);
			}else{
				setTempHologram(location, ItemFactory.create(Material.EXP_BOTTLE), "&a+ &6&l" + exp + " &9exp");
				found(gp.getPlayer(), exp + " exp");
				gp.setExp(gp.getExp()+exp);
			}
		}		
	}
	
	
	private void setTempHologram(Location l, ItemStack item, String name) {
		Hologram winHolo = HologramsAPI.createHologram(Main.get(), l.clone().add(0, 2.8, 0));
		winHolo.appendTextLine(MessageUtil.color(name));
		winHolo.appendItemLine(item);
		Runner.make(Main.get()).delay(80).run(winHolo::delete);
	}
	
	private void found(Player p, String item) {
		p.sendMessage(MessageUtil.color("&9Você encontrou &b" + item + " &9na MysteryBox."));
	}
}
