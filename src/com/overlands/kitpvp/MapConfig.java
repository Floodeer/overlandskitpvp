package com.overlands.kitpvp;

import java.io.File;
import java.util.Map;

import net.citizensnpcs.api.npc.NPC;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import com.google.common.collect.Maps;
import com.overlands.core.utils.ConfigDeserializer;

public class MapConfig extends ConfigDeserializer {

	protected MapConfig(File configFile) {
		super(configFile);
		// TODO Auto-generated constructor stub
	} 
	
	@ConfigOptions(name = "Spawn")
	public Location spawn = Bukkit.getWorlds().get(0) == null ? Bukkit.getWorlds().get(1).getSpawnLocation() : Bukkit.getWorlds().get(0).getSpawnLocation();
	
	@ConfigOptions(name = "Respawn")
	public Location respawn = Bukkit.getWorlds().get(0) == null ? Bukkit.getWorlds().get(1).getSpawnLocation() : Bukkit.getWorlds().get(0).getSpawnLocation();
	
	@ConfigOptions(name = "Center")
	public Location center = Bukkit.getWorlds().get(0) == null ? Bukkit.getWorlds().get(1).getSpawnLocation() : Bukkit.getWorlds().get(0).getSpawnLocation();
	
	@ConfigOptions(name = "Profile")
	public Location profile = Bukkit.getWorlds().get(0) == null ? Bukkit.getWorlds().get(1).getSpawnLocation() : Bukkit.getWorlds().get(0).getSpawnLocation();
	
	@ConfigOptions(name = "Borders.map.low")
	public Location lowBorder = Bukkit.getWorlds().get(0) == null ? Bukkit.getWorlds().get(1).getSpawnLocation() : Bukkit.getWorlds().get(0).getSpawnLocation();
	
	@ConfigOptions(name = "Borders.map.high")
	public Location highBorder = Bukkit.getWorlds().get(0) == null ? Bukkit.getWorlds().get(1).getSpawnLocation() : Bukkit.getWorlds().get(0).getSpawnLocation();
	
	@ConfigOptions(name = "Borders.spawn.low")
	public Location lowBorderSpawn = Bukkit.getWorlds().get(0) == null ? Bukkit.getWorlds().get(1).getSpawnLocation() : Bukkit.getWorlds().get(0).getSpawnLocation();
	
	@ConfigOptions(name = "Borders.spawn.high")
	public Location highBorderSpawn = Bukkit.getWorlds().get(0) == null ? Bukkit.getWorlds().get(1).getSpawnLocation() : Bukkit.getWorlds().get(0).getSpawnLocation();
	
	@ConfigOptions(name = "NPCs.commands")
	public Map<String, String> npcsCommands = Maps.newHashMap();

}
