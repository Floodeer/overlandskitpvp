package com.overlands.kitpvp.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.ranks.Rank;

public class LeaveCommand extends CommandBase {

	public LeaveCommand() {
		super(Rank.MOD, "sair", "leave", "quit");
	}

	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player p = (Player)commandSender;
		GamePlayer gp = GamePlayer.get(p);
		if(gp.isInGame()) {
			gp.getGame().removePlayer(gp);
		}
	}
}
