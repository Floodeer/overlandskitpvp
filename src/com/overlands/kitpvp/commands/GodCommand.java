package com.overlands.kitpvp.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.ranks.Rank;

public class GodCommand extends CommandBase {

	public GodCommand() {
		super(Rank.ADMIN, "godmode", "nocooldown");
	}

	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player p = (Player)commandSender;
		if(Main.getGame().isGodMode()) {
			Bukkit.broadcastMessage(MessageUtil.color("&9God Mode desativado."));
			Main.getGame().setGodMode(false);
			Main.getGame().getPlayers().forEach(g -> g.setEnergy(0));
		}else{
			Bukkit.broadcastMessage(MessageUtil.color("&9God Mode ativado por &4" + p.getName() + "&9."));
			Main.getGame().setGodMode(true);
			Main.getGame().getPlayers().forEach(g -> g.setEnergy(100));
		}
	}
}
