package com.overlands.kitpvp.commands;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Runner;
import com.overlands.core.utils.calc.TimeUtils;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.core.utils.search.PlayerSearcher;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.database.interfaces.Callback;
import com.overlands.kitpvp.ranks.Rank;

public class MySQLCommand extends CommandBase {

	public MySQLCommand() {
		super(Rank.ADMIN, true, "resetardata", "defaultdata", "cleardata");
	}

	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player p = (Player)commandSender;
		if(args != null) {
			if(args.length == 0) {
				p.sendMessage(MessageUtil.color("&6/mysql resetar <player> <data>"));
			}
			
			if(args[0].equalsIgnoreCase("resetar")) {
				if(args.length <= 3) {
					return;
				}
				Player player = null;
				String target = args[1];
				if (PlayerSearcher.searchExact(target) == null) {
					p.sendMessage(MessageUtil.color("&cPlayer não online, iniciando busca..."));
					PlayerSearcher.searchOffline(target, new Callback<Boolean>() {

						@Override
						public void onCall(Boolean result) {
							if (!result) {
								p.sendMessage(MessageUtil.color("&cNenhum player foi encontrado."));
								return;
							} else {
								p.sendMessage(MessageUtil.color("&aPlayer offline encontrado, resetando..."));
								resetPlayer(p, target, args[2]);
								return;
							}
						}
					});
				}else{
					player = PlayerSearcher.searchExact(target);
					player.kickPlayer("&cUma ação interna impediu você de continuar online, logue novamente.");
					Runner.make(Main.get()).delay(60).run(() -> {
						resetPlayer(p, PlayerSearcher.searchExact(target).getName(), args[2]);
					});
				}
			}
 		}
	}
	
	public void resetPlayer(Player sender, String player, String what) {
		long t = System.currentTimeMillis();
		sender.sendMessage(MessageUtil.color("&7Resetando &9" + what + " &7de &b=" + player));
		
		Main.get().getDB().getExecutor().execute(new Runnable() {
			PreparedStatement preparedStatement = null;

			@Override
			public void run() {
				try {
					String query = "UPDATE `player_data` SET `" + what + "` = DEFAULT WHERE `playername` = ?;";
					preparedStatement = Main.get().getDB().getConnection().get().prepareStatement(query.toString());
					preparedStatement.setString(1, player);
					preparedStatement.executeUpdate();
					sender.sendMessage(MessageUtil.color("&7Ação concluída, em &9" + TimeUtils.getSince(t) + "&7."));
				} catch (SQLException ex) {
					ex.printStackTrace();
					sender.sendMessage(MessageUtil.color("&cOcorreu um erro ao executar operação; " + ex.getLocalizedMessage()));
				} finally {
					if (preparedStatement != null)
						try {
							preparedStatement.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
				}
			}
		});	
	}
}
