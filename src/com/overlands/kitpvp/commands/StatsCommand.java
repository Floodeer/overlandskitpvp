package com.overlands.kitpvp.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.core.utils.search.PlayerSearcher;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.ranks.Rank;

public class StatsCommand extends CommandBase {

	public StatsCommand() {
		super(Rank.DEFAULT, false, "s", "stats", "perfil", "profile");
	}

	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player p = (Player)commandSender;
		
		if(args == null || args.length == 0) {
			Main.getProfile().open(p);
		}else {
			Player target = PlayerSearcher.searchExact(args[0]);
			if(target == null) {
				p.sendMessage(MessageUtil.color("&cPlayer offline ou inválido."));
				return;
			}
			Main.getProfile().open(p, target);
		}
	}
}
