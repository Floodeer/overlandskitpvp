package com.overlands.kitpvp.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Runner;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.GamePlayer.PlayerState.State;
import com.overlands.kitpvp.ranks.Rank;

public class RespawnCommand extends CommandBase {

	public RespawnCommand() {
		super(Rank.DEFAULT, "respawn", "lobby", "spawn");
	}

	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player p = (Player)commandSender;
		GamePlayer gp = GamePlayer.get(p);
		if(gp.isInGame()) {
			if(gp.getState().getState() == State.SAFE) {
				p.sendMessage(MessageUtil.color("&9Teleportando em 3 segundos..."));
				Runner.make(Main.get()).delay(60).run(() -> p.teleport(gp.isFastRespawn() ? Main.mapConfig.respawn : Main.mapConfig.spawn));
			}else{
				p.sendMessage(MessageUtil.color("&cAguarde &b" + gp.getState().getTime() + " &csegundos para sair de batalha."));
			}
		}else{
			Main.getGame().addPlayer(gp);
		}
	}
}
