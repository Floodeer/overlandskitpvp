package com.overlands.kitpvp.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.JsonBuilder;
import com.overlands.core.utils.JsonBuilder.ClickAction;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.ranks.Rank;

public class HelpCommand extends CommandBase {

	public HelpCommand() {
		super(Rank.DEFAULT, "ajuda", "help", "?", "*");
	}
	
	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player sender = (Player)commandSender;
		if(args == null || args.length == 0) {
			if(!Rank.isStaff(sender)) {
				playerHelp(sender);
			}else{
				staffHelp(sender);
			}
		}else{
			if(args[0].equalsIgnoreCase("2")) {
				if(Rank.isStaff(sender)) {
					staffHelp2(sender);
				}
			}else if(args[0].equalsIgnoreCase("3")) {
				if(Rank.isStaff(sender)) {
					staffHelp3(sender);
				}
			}else if(args[0].equalsIgnoreCase("hunter")) {
				if(Rank.isStaff(sender)) {
					staffHelpHunter(sender);
				}
			}
		}
	}
	
	private void playerHelp(Player caller) {
		caller.sendMessage(MessageUtil.color("&a() = opcional"));
		caller.sendMessage(MessageUtil.color("&c<> = obrigatório"));
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&6/spawn &7- Voltar ao spawn."));
		caller.sendMessage(MessageUtil.color("&6/prestige &7- Subir nível prestige."));
		caller.sendMessage(MessageUtil.color("&6/msg <player> <mensagem> &7- Mensagem privada."));
		caller.sendMessage(MessageUtil.color("&6/reportar <player> (motivo) &7- Reportar um player."));
		caller.sendMessage(MessageUtil.color("&6/dupla <player> &7- Fazer dupla com um player."));
		caller.sendMessage(MessageUtil.color("&6/dupla aceitar <player> &7- Aceitar dupla."));
		caller.sendMessage(MessageUtil.color("&6/dupla ignorar <player> &7- Ignorar dupla."));
		caller.sendMessage(MessageUtil.color("&6/dupla desfazer &7- Desfazer ou sair de uma dupla."));
		caller.sendMessage(MessageUtil.color(" "));
	}
	
	private void staffHelp(Player caller) {
		caller.sendMessage(MessageUtil.color("&a() = opcional"));
		caller.sendMessage(MessageUtil.color("&c<> = obrigatório"));
		caller.sendMessage(MessageUtil.color("&7use &atrue &7ou &cfalse&7."));
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&6/expulsar <player> (motivo)"));
		caller.sendMessage(MessageUtil.color("&6/banir <player> (motivo)"));
		caller.sendMessage(MessageUtil.color("&6/desbanir <player>"));
		caller.sendMessage(MessageUtil.color("&6/chat &7- Use para ver os argumentos."));
		caller.sendMessage(MessageUtil.color("&6/admin &7- Vanish e modo de monitoramento."));
		caller.sendMessage(MessageUtil.color("&6/staffchat (mensagem) &7- Chat da staff."));
		caller.sendMessage(MessageUtil.color("&6/check <player> &7Informações do player."));
		caller.sendMessage(MessageUtil.color("&6/sair &7- Sair da partida."));
		caller.sendMessage(MessageUtil.color(" "));
		new JsonBuilder("Clique para ver a próxima página").withColor(ChatColor.YELLOW).withClickEvent(ClickAction.RUN_COMMAND, "/help 2").sendJson(caller);
	}
	
	private void staffHelp2(Player caller) {
		for(int i = 0; i < 20; i++) 
			caller.sendMessage(MessageUtil.color(" "));
		
		caller.sendMessage(MessageUtil.color("&6/data &7- Use para ver os argumentos."));
		caller.sendMessage(MessageUtil.color("&6/update &7- Atualiza a DB e Leaderboards."));
		caller.sendMessage(MessageUtil.color("&6/voar (speed) &7- Ativar/Desativar o fly."));
		caller.sendMessage(MessageUtil.color("&6/corrigir &7- Corrigir entidades inválidas."));
		caller.sendMessage(MessageUtil.color("&6/gm (gamemode) &7- Mudar modo de jogo."));
		caller.sendMessage(MessageUtil.color("&6/sbroadcast &7- Broadcast do servidor."));
		caller.sendMessage(MessageUtil.color("&6/broadcast &7- Mensagem global para o servidor."));
		caller.sendMessage(MessageUtil.color("&6/tp &7- Use para ver os argumentos."));
		caller.sendMessage(MessageUtil.color(" "));
		new JsonBuilder("Clique para ver a próxima página").withColor(ChatColor.YELLOW).withClickEvent(ClickAction.RUN_COMMAND, "/help 3").sendJson(caller);
	}
	
	private void staffHelp3(Player caller) {
		for(int i = 0; i < 20; i++) 
			caller.sendMessage(MessageUtil.color(" "));
		
		caller.sendMessage(MessageUtil.color("&6/tocar <nome> &7- Use para ver a lista de músicas."));
		caller.sendMessage(MessageUtil.color("&6/give &7- Use para ver os argumentos."));
		caller.sendMessage(MessageUtil.color("&6/booster &7- Use para ver os argumentos."));
		caller.sendMessage(MessageUtil.color("&6/godmode &7- Ativa o ilimitado de skills."));
		caller.sendMessage(MessageUtil.color("&6/lag &7- Inicia debug do servidor."));
		caller.sendMessage(MessageUtil.color("&6/top &7- Teleporta pro bloco mais alto."));
		caller.sendMessage(MessageUtil.color("&6/kill (player) &7- Matar um player."));
		caller.sendMessage(" ");
		new JsonBuilder("Clique para ver os comandos do Hunter").withColor(ChatColor.YELLOW).withClickEvent(ClickAction.RUN_COMMAND, "/help hunter").sendJson(caller);
	}
	
	private void staffHelpHunter(Player caller) {
		for(int i = 0; i < 20; i++) 
			caller.sendMessage(MessageUtil.color(" "));
		
		caller.sendMessage(MessageUtil.color("&6/hunter test <player> <tempo> &7- Verificar um player."));
		caller.sendMessage(MessageUtil.color("&4/hunter treinar <categoria> &7- Iniciar treinamento da rede neural."));
		caller.sendMessage(MessageUtil.color("&6/hunter reconstruir &7- Reconstruir rede neural."));
		caller.sendMessage(MessageUtil.color("&6/hunter info &7- Informações da rede neural."));
		caller.sendMessage(" ");
		caller.sendMessage(MessageUtil.color("&4O comando de treinamento deve ser executado apenas para coleta de data!"));
	}
}
