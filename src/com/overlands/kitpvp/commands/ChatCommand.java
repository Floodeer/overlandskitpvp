package com.overlands.kitpvp.commands;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.ranks.Rank;

public class ChatCommand extends CommandBase {

	public ChatCommand() {
		super(Rank.MOD, "chat", "togglechat");
	}
	
	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player sender = (Player)commandSender;
		if(args == null || args.length == 0) {
			help(sender);
			return;
		}
		if(args[0].equalsIgnoreCase("desligar") || args[0].equalsIgnoreCase("desativar") || args[0].equalsIgnoreCase("off")) {
			if(args.length > 2) {
				Main.get().getChatCore().silence(Integer.parseInt(args[1])* 3600000L, Boolean.parseBoolean(args[2]));
			}else if(args.length > 1) {
				if(MathUtils.isInteger(args[1])) {
					Main.get().getChatCore().silence(Integer.parseInt(args[1])* 3600000L, true);
				}else if(MathUtils.isBoolean(args[1])){
					Main.get().getChatCore().silence(-1, Boolean.parseBoolean(args[1]));
				}else{
					Main.get().getChatCore().silence(-1, false);
				}
			}
		}else if(args[0].equalsIgnoreCase("ligar") || args[0].equalsIgnoreCase("reativar") || args[0].equalsIgnoreCase("ativar") || args[0].equalsIgnoreCase("on")) {
			if(args.length > 1 && MathUtils.isBoolean(args[1]))
				Main.get().getChatCore().silence(0, Boolean.parseBoolean(args[1]));
			else
				Main.get().getChatCore().silence(0, true);
		}else if(args[0].equalsIgnoreCase("slow") || args[0].equalsIgnoreCase("slowmode")) {
			if(args.length > 2) {
				Main.get().getChatCore().setChatSlow(Integer.parseInt(args[1]), Boolean.parseBoolean(args[2]));
			}else if(args.length > 1) {
				if(MathUtils.isInteger(args[1])) {
					Main.get().getChatCore().silence(Integer.parseInt(args[1]), true);
				}else if(MathUtils.isBoolean(args[1])){
					Main.get().getChatCore().setChatSlow(-1, Boolean.parseBoolean(args[1]));
				}else{
					Main.get().getChatCore().setChatSlow(-1, false);
				}
			}
		}else if(args[0].equalsIgnoreCase("resetar") || args[0].equalsIgnoreCase("removeslow") || args[0].equalsIgnoreCase("slowoff")) {
			if(args.length > 1 && MathUtils.isBoolean(args[1]))
				Main.get().getChatCore().setChatSlow(0, Boolean.parseBoolean(args[1]));
			else
				Main.get().getChatCore().setChatSlow(0, true);
			
			Main.get().getChatCore().silence(0, false);
		}else if(args[0].equalsIgnoreCase("limpar") || args[0].equalsIgnoreCase("clear")) {
			for(int i = 0; i < 50; i++) {
				Bukkit.getOnlinePlayers().stream().forEach(p -> p.sendMessage(" "));
			}
		}
	}
	
	private void help(Player caller) {
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&a() = opcional"));
		caller.sendMessage(MessageUtil.color("&c<> = obrigatório"));
		caller.sendMessage(MessageUtil.color("&7Para &e(avisar) &7use &atrue &7ou &cfalse&7."));
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&6/chat desligar (tempo) (avisar)"));
		caller.sendMessage(MessageUtil.color("&6/chat ativar (avisar)"));
		caller.sendMessage(MessageUtil.color("&6/chat slow (tempo) (avisar)"));
		caller.sendMessage(MessageUtil.color("&6/chat resetar (avisar)"));
		caller.sendMessage(MessageUtil.color("&6/chat limpar"));
		caller.sendMessage(MessageUtil.color(" "));
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, String commandLabel, String[] args) {
		if(args.length == 1) {
			return Arrays.asList("desligar", "ativar", "slow", "resetar", "limpar");
		}
		return null;
	}
}
