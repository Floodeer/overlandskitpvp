package com.overlands.kitpvp.commands;

import java.io.File;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.google.common.collect.Lists;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.ranks.Rank;

public class MusicCommand extends CommandBase {

	public MusicCommand() {
		super(Rank.ADMIN, "music", "musica", "tocar", "play");
	}

	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player sender = (Player)commandSender;
		if(args == null || args.length == 0) {
			help(sender);
			return;
		}
		if(args[0].equalsIgnoreCase("pausar") || args[0].equalsIgnoreCase("parar")) {
			Main.get().getMusicCore().stop();
			sender.sendMessage(MessageUtil.color("&9Músicas pausadas."));
			return;
		}
		if(!Main.get().getMusicCore().play(args[0], Main.getPM().getPlayers())) {
			help(sender);
		}else{
			sender.sendMessage(MessageUtil.color("&9Tocando: &b" + args[0]));
		}
	}
	
	private void help(Player sender) {
		StringBuilder b = new StringBuilder();
		String separe = "";
		for(File perk : Main.get().getMusicCore().getMusicFile().listFiles()) {
			b.append(separe);
			b.append(ChatColor.AQUA + perk.getName());
			separe = ChatColor.GRAY + "\n";
		}
		
		sender.sendMessage(MessageUtil.color("&cMúsica inváida. Use: " + b.toString()));
	}
	

	@Override
	public List<String> onTabComplete(CommandSender sender, String commandLabel, String[] args) {
		if(args != null) {
			if(args.length == 1) {
				return getMatches(args[0], musics());		
			}
		}
		return null;
	}
	
	private List<String> musics() {
		List<String> files = Lists.newArrayList();
		for(File perk : Main.get().getMusicCore().getMusicFile().listFiles()) {
			files.add(perk.getName().replaceAll(".nbs", "").replaceAll("\\s", "_"));
		}
		return files;
	}
}
