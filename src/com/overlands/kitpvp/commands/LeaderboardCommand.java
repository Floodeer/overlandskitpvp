package com.overlands.kitpvp.commands;

import org.bukkit.command.CommandSender;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Runner;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.ranks.Rank;

public class LeaderboardCommand extends CommandBase {

	public LeaderboardCommand() {
		super(Rank.ADMIN, true, "leaderboard", "update");
	}

	@Override
	public void run(CommandSender p, String[] args) {
		p.sendMessage(MessageUtil.color("&7Pedido de atualização enviado. Pode levar uns segundos para atualizar."));
		Main.getLB().requestUpdate();
	}
}
