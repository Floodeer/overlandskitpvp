package com.overlands.kitpvp.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.comphenix.protocol.ProtocolLibrary;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.core.utils.search.PlayerSearcher;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.ranks.Rank;

public class CheckPlayerCommand extends CommandBase {
	
	public CheckPlayerCommand() {
		super(Rank.ADMIN, "checkPlayer", "getPlayer", "playerversion", "check");
	}
	
	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player sender = (Player)commandSender;
		if(args == null || args.length == 0) {
			sender.sendMessage(MessageUtil.color("&9Especifique um player."));
			return;
		}
		Player player = PlayerSearcher.searchOnline(sender, args[0], false);
		if(!player.isOnline() || player == null) {
			sender.sendMessage(MessageUtil.color("Player offline ou não existe."));
			return;
		}
		GamePlayer gp = Main.getPM().getPlayer(player.getUniqueId());

		if(gp.getProtocolVersion() == 47) {
		    int i = ProtocolLibrary.getProtocolManager().getProtocolVersion(player);
			gp.setProtocolVersion(i);
		}
		
		String protocolVersion = "???";
		if(gp.getProtocolVersion() == 47) {
			protocolVersion = "1.8.";
		}else if(gp.getProtocolVersion() == 107) {
			protocolVersion = "1.9";
		}else if(gp.getProtocolVersion() == 108) {
			protocolVersion = "1.9.1";
		}else if(gp.getProtocolVersion() == 109) {
			protocolVersion = "1.9.2";
		}else if(gp.getProtocolVersion() == 110) {
			protocolVersion = "1.9.4";
		}else if(gp.getProtocolVersion() == 210) {
			protocolVersion = "1.10";
		}else if(gp.getProtocolVersion() == 315) {
			protocolVersion = "1.11";
		}else if(gp.getProtocolVersion() == 316) {
			protocolVersion = "1.11.2";
		}else if(gp.getProtocolVersion() == 335) {
			protocolVersion = "1.12";
		}else if(gp.getProtocolVersion() == 338) {
			protocolVersion = "1.12.1";
		}else if(gp.getProtocolVersion() == 340) {
			protocolVersion = "1.12.2";
		}else if(gp.getProtocolVersion() == 393) {
			protocolVersion = "1.13";
		}else if(gp.getProtocolVersion() == 401) {
			protocolVersion = "1.13.1";
		}else if(gp.getProtocolVersion() == 404) {
			protocolVersion = "1.13.2";
		}else{
			protocolVersion = "desconhecido";
		}
		
		sender.sendMessage(MessageUtil.color("&7Informações coletadas de " + gp.getPlayer().getName()));
		sender.sendMessage(" ");
		sender.sendMessage(MessageUtil.color("&e--- Minecraft ---"));
		sender.sendMessage(MessageUtil.color("&fIP (raw): &b"+ (Rank.isStaff(player) || player.getName().equalsIgnoreCase("Touyama") ? "&cEscondido" : player.spigot().getRawAddress())));
		sender.sendMessage(MessageUtil.color("&fProtocolo: &b" + gp.getProtocolVersion() + " (MC " + protocolVersion + ")"));
		sender.sendMessage(MessageUtil.color("&fUUID: &b" + gp.getUUID().toString()));
		sender.sendMessage(MessageUtil.color("&fNome: &b" + gp.getPlayer().getName()));
		sender.sendMessage(" ");
		sender.sendMessage(MessageUtil.color("&e--- Game Info ---"));
		sender.sendMessage(MessageUtil.color("&fBalance: &b" + gp.getCoins()));
		sender.sendMessage(MessageUtil.color("&fExp: &b" + gp.getExp()));
		sender.sendMessage(MessageUtil.color("&fLevel: &b" + gp.getLevel()));
		sender.sendMessage(" ");
	}
}
