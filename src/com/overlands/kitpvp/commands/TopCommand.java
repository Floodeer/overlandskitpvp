package com.overlands.kitpvp.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.GamePlayer.PlayerState;
import com.overlands.kitpvp.ranks.Rank;

public class TopCommand extends CommandBase {

	public TopCommand() {
		super(Rank.OWNER, new Rank[] { Rank.ADMIN, Rank.LEAD_BUILDER, Rank.DEVELOPER }, "top", "topo", "subir");
	}

	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player sender = (Player) commandSender;
		if (GamePlayer.get(sender).getState().getState() == PlayerState.State.FIGHTING) {
			sender.sendMessage(MessageUtil.color("&9Você não pode usar este comando em combate."));
			return;
		}
		sender.teleport(sender.getWorld().getHighestBlockAt(sender.getLocation().clone().add(0.5, 0, 0.5)).getLocation());
		sender.sendMessage(MessageUtil.color("&aTeleportado!"));
	}
}
