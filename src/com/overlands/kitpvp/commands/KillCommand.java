package com.overlands.kitpvp.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.core.utils.search.PlayerSearcher;
import com.overlands.kitpvp.ranks.Rank;

public class KillCommand extends CommandBase {

	public KillCommand() {
		super(Rank.DEFAULT, "kill", "suicidar", "matar");
	}

	@Override
	public void run(CommandSender sender, String[] args) {
		Player player = (Player)sender;
		if(args == null || args.length == 0) {
			if(!Rank.isAdmin(player)) {
				player.sendMessage(MessageUtil.color("&9Se matar não é uma opção."));
			}else {
				player.setHealth(0);
			    player.sendMessage(MessageUtil.color("&6Você se matou."));
			}
		}else {
			if (Rank.isAdmin(player)) {
				if (PlayerSearcher.searchOnline(sender, args[0], true) == null)
					return;
				Player p = PlayerSearcher.searchOnline(sender, args[0], false);
				p.setHealth(0);
				if (args.length > 2 && MathUtils.isBoolean(args[1])) {
					if (Boolean.parseBoolean(args[1]))
						p.sendMessage(MessageUtil.color(Rank.color(player) + sender.getName() + " &6matou você."));
					else if (!Boolean.parseBoolean(args[1]))
						p.sendMessage(MessageUtil.color("&cVocê foi morto."));
				} else {
					p.sendMessage(MessageUtil.color("&cVocê foi morto."));
				}
			}
		}
	}
}
