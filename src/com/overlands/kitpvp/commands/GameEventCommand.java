package com.overlands.kitpvp.commands;

import java.util.List;

import com.overlands.kitpvp.game.events.*;
import com.overlands.kitpvp.game.events.Package;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.google.common.collect.Lists;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.ranks.Rank;

public class GameEventCommand extends CommandBase {

	public GameEventCommand() {
		super(Rank.ADMIN, "evento");
	}

	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player p = (Player)commandSender;
		if(args == null || args.length == 0) {
			p.sendMessage(MessageUtil.color("&9Argumentos insuficientes."));
			return;
		}

		if(Main.getGame().getEvent() != null) {
			Main.getGame().getEvent().onEnd();
		}

		if(args[0].equalsIgnoreCase("package")) {
			new Package();
		}else if(args[0].equalsIgnoreCase("star")) {
			new BleedingStar();
		}else if(args[0].equalsIgnoreCase("meteor")) {
			new MeteorEvent();
		}else if(args[0].equalsIgnoreCase("party")) {
			new PartyEvent();
		}else if(args[0].equalsIgnoreCase("boss")) {
			new DemonBossEvent();
		}else if(args[0].equalsIgnoreCase("raffle")) {
			new RaffleEvent();
		}else if(args[0].equalsIgnoreCase("mystery")) {
			new MysteryClasses();
		}else if(args[0].equalsIgnoreCase("inferno")) {
			new InfernoEvent();
		}else if(args[0].equalsIgnoreCase("battleroyale")) {
			new BattleRoyaleEvent();
		}else if(args[0].equalsIgnoreCase("bounty")) {
			new BountyEvent();
		}else if(args[0].equalsIgnoreCase("overclocked")) {
			new Overclocked();
		}else if(args[0].equalsIgnoreCase("cancelar")) {
			if (Main.getGame().getEvent() != null) {
				Main.getGame().getEvent().onEnd();
				Bukkit.broadcastMessage(MessageUtil.color("&cEvento cancelado por um Administrador."));
			}
		}
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, String commandLabel, String[] args) {
		if(args != null) {
			if(args.length == 1) {
				return getMatches(args[0], values());
			}
		}
		return null;
	}
	
	private List<String> values() {
		return Lists.newArrayList("package", "star", "meteor", "party", "boss", "raffle", "mystery", "battleroyale", "bounty", "overclocked", "cancelar");
	}
}
