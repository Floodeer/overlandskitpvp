package com.overlands.kitpvp.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.GamePlayer.PlayerState;
import com.overlands.kitpvp.ranks.Rank;

public class FlyCommand extends CommandBase {

	public FlyCommand() {
		super(Rank.ADMIN, "fly", "voar", "togglefly");

	}

	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player sender = (Player)commandSender;
		if(GamePlayer.get(sender).getState().getState() == PlayerState.State.FIGHTING) {
			sender.sendMessage(MessageUtil.color("&9Você não pode usar este comando em combate."));
			return;
		}
		if(args != null && args.length > 0)  {
			if(MathUtils.isFloat(args[0])) {
				sender.setFlySpeed(Float.parseFloat(args[0])/10);
				sender.sendMessage(MessageUtil.color("&cVelocidade do fly modificada para " + args[0]));
				return;
			}
		}
		if (!sender.isFlying() && !sender.getAllowFlight()) {
			sender.setAllowFlight(true);
			sender.setFlying(true);
			sender.sendMessage(MessageUtil.color("&aFly ativado."));
		} else {
			sender.setAllowFlight(false);
			sender.setFlying(false);
			sender.sendMessage(MessageUtil.color("&cFly desativado."));
		}
	}
}
