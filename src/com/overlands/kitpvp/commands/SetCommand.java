package com.overlands.kitpvp.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.entity.Player;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.ranks.Rank;
import com.sk89q.worldedit.bukkit.selections.Selection;

public class SetCommand extends CommandBase {

	public SetCommand() {
		super(Rank.LEAD_BUILDER, "setLocation", "setLoc", "addLoc");

	}

	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player p = (Player)commandSender;
		if(args == null || args.length == 0) {
			p.sendMessage(MessageUtil.color("&9Argumentos insuficientes."));
			return;
		}
		
		if(args[0].equalsIgnoreCase("spawn")) {
			Main.mapConfig.spawn = p.getLocation();
			try {
				Main.mapConfig.save();
				p.sendMessage(MessageUtil.color("&aSucesso."));
			} catch (InvalidConfigurationException e) {
				e.printStackTrace();
			}
		}else if(args[0].equalsIgnoreCase("respawn")) {
			Main.mapConfig.respawn = p.getLocation();
			try {
				Main.mapConfig.save();
				p.sendMessage(MessageUtil.color("&aSucesso."));
			} catch (InvalidConfigurationException e) {
				e.printStackTrace();
			}
		}else if(args[0].equalsIgnoreCase("profile")) {
			Main.mapConfig.profile = p.getLocation();
			try {
				Main.mapConfig.save();
				p.sendMessage(MessageUtil.color("&aSucesso."));
			} catch (InvalidConfigurationException e) {
				e.printStackTrace();
			}
		}else if(args[0].equalsIgnoreCase("center")) {
			Main.mapConfig.center = p.getLocation();
			try {
				Main.mapConfig.save();
				p.sendMessage(MessageUtil.color("&aSucesso."));
			} catch (InvalidConfigurationException e) {
				e.printStackTrace();
			}
		}else if(args[0].equalsIgnoreCase("bounds")) {
			Selection s = Main.get().getWE().getSelection(p);
			Main.mapConfig.lowBorder = s.getMinimumPoint();
			Main.mapConfig.highBorder = s.getMaximumPoint();
			try {
				Main.mapConfig.save();
				p.sendMessage(MessageUtil.color("&aSucesso."));
			} catch (InvalidConfigurationException e) {
				e.printStackTrace();
			}
		}else if(args[0].equalsIgnoreCase("spawnBounds")) {
			Selection s = Main.get().getWE().getSelection(p);
			Main.mapConfig.lowBorderSpawn = s.getMinimumPoint();
			Main.mapConfig.highBorderSpawn = s.getMaximumPoint();
			try {
				Main.mapConfig.save();
				p.sendMessage(MessageUtil.color("&aSucesso."));
			} catch (InvalidConfigurationException e) {
				e.printStackTrace();
			}
		}
	}
}
