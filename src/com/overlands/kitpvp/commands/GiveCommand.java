package com.overlands.kitpvp.commands;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.search.ItemSearcher;
import com.overlands.core.utils.search.PlayerSearcher;
import com.overlands.kitpvp.ranks.Rank;

public class GiveCommand extends CommandBase {

	public GiveCommand() {
		super(Rank.BUILDER, "dar", "give", "g", "item", "i");
	}

	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player sender = (Player)commandSender;
		if(!Rank.isAdmin(sender)) {
			sender.sendMessage(MessageUtil.color("&cVocê não pode usar este comando em jogo."));
			return;
		}
		
		if(args == null || args.length == 0) {
			sender.sendMessage(MessageUtil.color(" "));
			sender.sendMessage(MessageUtil.color("&a() = opcional"));
			sender.sendMessage(MessageUtil.color("&c<> = obrigatório"));
			sender.sendMessage(MessageUtil.color(" "));
			sender.sendMessage(MessageUtil.color("&6/give (player) <item> (quantidade) (encantamento:nivel)"));
			sender.sendMessage(MessageUtil.color(" "));
			return;
		}
		parse(sender, args);
	}

	public void parse(Player player, String[] args) {
		if (args.length == 1)
			give(player, player.getName(), args[0], "1", "");

		else if (args.length == 2)
			give(player, args[0], args[1], "1", "");

		else if (args.length == 3)
			give(player, args[0], args[1], args[2], "");

		else
			give(player, args[0], args[1], args[2], args[3]);
	}

	public void help(Player caller) {
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&a() = opcional"));
		caller.sendMessage(MessageUtil.color("&c<> = obrigatório"));
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&6/give (player) (item) (quantidade) (encantamento:nivel)"));
		caller.sendMessage(MessageUtil.color(" "));
	}

	public void give(Player player, String target, String itemNames, String amount, String enchants) {
		LinkedList<Entry<Material, Byte>> itemList = new LinkedList<Entry<Material, Byte>>();
		itemList = ItemSearcher.matchItem(player, itemNames, true);
		if (itemList.isEmpty())
			return;

		LinkedList<Player> giveList = new LinkedList<Player>();
		if (target.equalsIgnoreCase("all")) {
			for(Player cur : Bukkit.getOnlinePlayers())
				giveList.add(cur);
		} else {
			giveList = PlayerSearcher.matchesPlayers(player, target, true);
			if (giveList.isEmpty())
				return;
		}
		
		int count = 1;
		try {
			count = Integer.parseInt(amount);

			if (count < 1) {
				player.sendMessage(MessageUtil.color("&cQuantidade do item inválida, modificando para 1."));
				count = 1;
			}
		} catch (Exception e) {
			player.sendMessage(MessageUtil.color("&cQuantidade do item inválida, modificando para 1."));
		}


		HashMap<Enchantment, Integer> enchs = new HashMap<Enchantment, Integer>();
		if (enchants.length() > 0) {
			for (String cur : enchants.split(",")) {
				try {
					String[] tokens = cur.split(":");
					enchs.put(Enchantment.getByName(tokens[0]), Integer.parseInt(tokens[1]));
				} catch (Exception e) {
					player.sendMessage(MessageUtil.color("&cEncantamento inválido."));
				}
			}
		}
		
		String givenList = "";
		for (Player cur : giveList)
			givenList += cur.getName() + " ";
		if (givenList.length() > 0)
			givenList = givenList.substring(0, givenList.length() - 1);

		for (Entry<Material, Byte> curItem : itemList) {
			for (Player cur : giveList) {
				ItemStack stack = ItemFactory.create(curItem.getKey(), count, curItem.getValue());
				stack.addUnsafeEnchantments(enchs);
				cur.getInventory().addItem(stack);
				if (!cur.equals(player))
					cur.sendMessage(MessageUtil.color("&9Você recebeu &b" + count + " " + ItemSearcher.getName(curItem.getKey(), curItem.getValue(), false) + " &9de " + Rank.color(player) + player.getName() + "&9."));
			}

			if (target.equalsIgnoreCase("all"))
				player.sendMessage(MessageUtil.color(Rank.color(player) + player.getName() + " &9deu a todos &b" + count + " " + ItemSearcher.getName(curItem.getKey(),curItem.getValue(), false) + "&9."));

			else if (giveList.size() > 1)
				player.sendMessage(MessageUtil.color("&9Você deu &b" + count + " " + ItemSearcher.getName(curItem.getKey(), curItem.getValue(), false) + " &9para &b" + givenList + "&9."));
			else
				player.sendMessage(MessageUtil.color("&9Você deu &b" + count + " " + ItemSearcher.getName(curItem.getKey(), curItem.getValue(), false) + " &9para &b" + Rank.color(giveList.getFirst()) + giveList.getFirst().getName() + "&9."));
		}
	}
}
