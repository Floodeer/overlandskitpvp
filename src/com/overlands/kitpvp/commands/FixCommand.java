package com.overlands.kitpvp.commands;

import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import com.overlands.core.utils.Util;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.ranks.Rank;

public class FixCommand extends CommandBase {

	public FixCommand() {
		super(Rank.BUILDER, true, "corrigir", "fix");
	}

	@Override
	public void run(CommandSender p, String[] args) {
		Main.mapConfig.center.getWorld().getEntities().stream()
		        .filter(entity -> entity instanceof ArmorStand)
				.map(entity -> (ArmorStand) entity)
				.filter(entity -> !entity.isVisible())
				.forEach(Entity::remove);

		Main.mapConfig.center.getWorld().getLivingEntities().stream()
		       .filter(entity -> entity instanceof Player)
			   .map(entity -> (Player) entity)
			   .filter(OfflinePlayer::isOnline)
			   .forEach(entity -> Util.updatePlayer(entity));

		
	}
}
