package com.overlands.kitpvp.commands.staff;

import java.io.File;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.core.utils.scheduler.TaskCallback;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.MapBackupHandler;
import com.overlands.kitpvp.ranks.Rank;

public class MapBackupCommand extends CommandBase {

	public MapBackupCommand() {
		super(Rank.LEAD_BUILDER, "mapbackup");
	}
	
	private boolean used = false;
	
	@Override
	public void run(CommandSender sender, String[] args) {
		if(args == null || args.length == 0) {
			sender.sendMessage(MessageUtil.color("&cEspecifique um nome para o backup, não use espaços."));
			return;
		}
		String name = args[0];
		if (name.indexOf(' ') >= 0) {
			sender.sendMessage(MessageUtil.color("&cNão use espaço."));
			return;
		}
		
		File file = new File(Main.get().getServer().getWorldContainer(), "KitPvP" + name + ".zip");
		if(file.exists()) {
			if(used) {
				file.delete();
				used = false;
			}else {
				sender.sendMessage(MessageUtil.color("&cEsse backup já existe, use o comando novamente para deletar o backup atual e executar outro."));
				used = true;
				return;
			}
		}
		MessageUtil.sendStaffMessage("&7&l&o>>> " + (sender instanceof Player ? sender.getName() : "O servidor ") + " iniciou uma tarefa de backup.");
		   
		MessageUtil.sendRawStaffMessage("&eVerificando blocos antes de executar backup...");
		Main.get().getExplosionManager().getBlockRestore().restoreAllSync(new TaskCallback() {
			
			@Override
			public void onCall() {
				MessageUtil.sendRawStaffMessage("&eTodos os blocos do mapa foram resetados, desabilitado explosões...");
				Main.get().getExplosionManager().setExplosions(false);
				MessageUtil.sendRawStaffMessage(MessageUtil.color("&7&l&o>> Salvando mapa..."));
				MapBackupHandler.executeBackup(name);
			}
		});
	}
}
