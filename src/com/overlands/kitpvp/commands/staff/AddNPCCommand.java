package com.overlands.kitpvp.commands.staff;

import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.npc.CitizensNPC;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.ranks.Rank;

public class AddNPCCommand extends CommandBase {

	public AddNPCCommand() {
		super(Rank.ADMIN, false, "npccommand", "addnpccommand", "setcommand");
	}
	
	@Override
	public void run(CommandSender sender, String[] args) {
		Player player = (Player)sender;
		if(args == null || args.length == 0) {
			player.sendMessage(MessageUtil.color("&cEspecifique o comando para ser adicionado."));
			return;
		}
		
		LivingEntity entity = LocationUtils.getTarget(player);
		if(entity != null && entity.hasMetadata("NPC")) {
			Main.mapConfig.npcsCommands.put(Integer.toString(CitizensAPI.getNPCRegistry().getNPC(entity).getId()), args[0]);
			try {
				Main.mapConfig.save();
			} catch (InvalidConfigurationException e) {
				e.printStackTrace();
			}
			player.sendMessage(MessageUtil.color("&9Comando adicionado."));
        }else {
			player.sendMessage(MessageUtil.color("&cOlhe para o NPC que deseja adicionar um comando."));
        }
    }
}
