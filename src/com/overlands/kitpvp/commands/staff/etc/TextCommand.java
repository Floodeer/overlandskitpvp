package com.overlands.kitpvp.commands.staff.etc;

import org.bukkit.block.BlockFace;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.block.BlockText;
import com.overlands.core.utils.block.BlockText.TextAlign;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.ranks.Rank;

public class TextCommand extends CommandBase {

	public TextCommand() {
		super(Rank.LEAD_BUILDER, false, "escrever", "printar", "digitar", "texto");
	}
	
	@Override
	public void run(CommandSender sender, String[] args) {
		Player player = (Player)sender;
		if(args == null || args.length == 0) {
			help(player);
			return;
		}
		
		if(args.length > 4) {
			if(!MathUtils.isInteger(args[2])) {
				help(player);
				return;
			}else if(!MathUtils.isInteger(args[3])) {
				help(player);
				return;
			}
			BlockText.makeText(args[0], player.getLocation(), BlockFace.valueOf(args[1]), Integer.parseInt(args[2]), Byte.parseByte(args[3]), TextAlign.valueOf(args[4]));
		}
	}

	private void help(Player caller) {
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&a() = opcional"));
		caller.sendMessage(MessageUtil.color("&c<> = obrigatório"));
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&6/texto <palavra> <BlockFace> <id> <data> <alinhamento>"));
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&6BlockFaces: NORTH, SOUTH, WEST, EAST"));
		caller.sendMessage(MessageUtil.color("&6Alinhamentos: LEFT, RIGHT, CENTER"));
		caller.sendMessage(MessageUtil.color(" "));
	}
}
