package com.overlands.kitpvp.commands.staff.etc;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.entity.WitherSkull;

import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.ranks.Rank;

public class WitherSkullCommand extends CommandBase {

	public WitherSkullCommand() {
		super(Rank.ADMIN, false, "shoot");
	}
	
	@Override
	public void run(CommandSender sender, String[] args) {
		Player player = (Player)sender;
		if(args == null || args.length == 0) {
			WitherSkull skull = player.launchProjectile(WitherSkull.class);
			skull.setVelocity(player.getLocation().getDirection().multiply(1.5));
			skull.setShooter(player);
			skull.setIsIncendiary(false);
			skull.setCharged(false);
		}else {
			if(args[0].equalsIgnoreCase("charged")) {
				WitherSkull skull = player.launchProjectile(WitherSkull.class);
				skull.setVelocity(player.getLocation().getDirection().multiply(1.5));
				skull.setShooter(player);
				skull.setIsIncendiary(false);
				skull.setCharged(true);
			}else if(args[0].equalsIgnoreCase("tnt")) {
				TNTPrimed tnt = player.getWorld().spawn(player.getLocation(), TNTPrimed.class);
				tnt.setIsIncendiary(false);
			}
		}
	}
}
