package com.overlands.kitpvp.commands.staff;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Runner;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.GamePlayer.PlayerState;
import com.overlands.kitpvp.ranks.Rank;

public class AdminCommand extends CommandBase {

	public AdminCommand() {
		super(Rank.MOD, false, "admin", "vanish");
	}
	
	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player sender = (Player)commandSender;
		GamePlayer gp = GamePlayer.get(sender);
		if(gp.isAdminMode()) {
			gp.setAdminMode(false);
			gp.getPlayer().setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
			Runner.make(Main.get()).delay(5).run(() -> Main.getGame().addPlayer(gp));
			Bukkit.getOnlinePlayers().stream().filter(all -> !all.canSee(gp.getPlayer())).forEach(all -> {
				Util.hidePlayer(all, gp.getPlayer(), false);
			});
			gp.getPlayer().setFlying(false);
			gp.getPlayer().setAllowFlight(false);
			gp.getPlayer().sendMessage(MessageUtil.color("&9Removido do modo admin."));
		}else {
			if(gp.getState().getState() == PlayerState.State.FIGHTING) {
				gp.getPlayer().sendMessage(MessageUtil.color("&eVocê não pode usar isso agora."));
				return;
			}
			if(gp.isInGame()) {
				gp.getGame().removePlayer(gp);
				sender.teleport(Main.mapConfig.spawn);
			}
			gp.setAdminMode(true);
			gp.getPlayer().setAllowFlight(true);
			gp.getPlayer().setFlying(true);
			gp.getPlayer().sendMessage(MessageUtil.color("&9Apenas membros da staff podem ter ver agora. Alguns valores do Scoreboard como CPU podem não ser preciso devido ao número de tarefas que a CPU executa em um segundo."));
		}
	}
}
