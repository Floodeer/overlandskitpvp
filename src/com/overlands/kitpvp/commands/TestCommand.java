package com.overlands.kitpvp.commands;

import java.io.File;
import java.io.IOException;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Runner;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.calc.MathUtils;
import de.slikey.effectlib.effect.ColoredImageEffect;
import de.slikey.effectlib.util.ParticleEffect;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.ranks.Rank;

import de.slikey.effectlib.effect.ImageEffect;
import de.slikey.effectlib.util.DynamicLocation;

public class TestCommand extends CommandBase {

	public TestCommand() {
		super(Rank.TESTER, false, "teste");
	}

	@Override
	public void run(CommandSender p, String[] args) {
		Player player = (Player)p;
        if(args.length == 0)
            return;

      //  Util.sendSkyPacket(player, Integer.parseInt(args[0]));
		if(args == null || args.length <= 1 ) {
			player.sendMessage(MessageUtil.color("&c/teste <path> <colored/particle>"));
			return;
		}
		File file = new File(Main.get().getDataFolder() + File.separator + args[0]);
		if(!file.exists()) {
			player.sendMessage("Arquivo não encontrado!");
			return;

		}
		if (args[1].equalsIgnoreCase("colored")){
			ColoredImageEffect effect = new ColoredImageEffect(Main.get().effectManager);
			effect.setDynamicOrigin(new DynamicLocation((player.getLocation())));
			//effect.particle = args.length == 0 || args[1] == null ? ParticleEffect.CLOUD : ParticleEffect.valueOf(args[1]);
			effect.loadFile(file);
			effect.enableRotation = false;
			effect.visibleRange = 250;
			effect.start();
			Runner.make(Main.get()).delay(10*20).run(() -> effect.cancel());
		}else{
			ImageEffect effect = new ImageEffect(Main.get().effectManager);
			effect.setDynamicOrigin(new DynamicLocation((player.getLocation())));
			effect.particle = ParticleEffect.valueOf(args[1]) == null ? ParticleEffect.CLOUD : ParticleEffect.valueOf(args[1]);
			effect.loadFile(file);
			effect.infinite();
			effect.visibleRange = 250;
			effect.start();
			Runner.make(Main.get()).delay(10*20).run(() -> effect.cancel());
		}
	}
}
