package com.overlands.kitpvp.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.core.utils.search.PlayerSearcher;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.ranks.Rank;

public class TeleportCommand extends CommandBase {

	public TeleportCommand() {
		super(Rank.MOD, false, "tp", "teleport", "teleportar");
	}
	
	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player sender = (Player)commandSender;
		if(args == null || args.length == 0) {
			help(sender, args);
			return;
		}
		Player p = null;
		if(args[0].equalsIgnoreCase("puxar")) {
			if(args.length > 2) {
				if(PlayerSearcher.searchOnline(sender, args[1], true) != null) {
					p = PlayerSearcher.searchOnline(sender, args[0], true);
					p.teleport(sender);
					p.sendMessage(MessageUtil.color("&9Você foi teleportado."));
				}
			}else{
				help(sender, args);
				return;
			}
		}else if(args[0].equalsIgnoreCase("all")) {
			if(Rank.isAdmin(sender)) {
				Main.getPM().getPlayers().stream().forEach(g -> g.getPlayer().teleport(sender.getLocation()));
			}else {
				sender.sendMessage(MessageUtil.permission);
			}
		}else{
			if(PlayerSearcher.searchOnline(sender, args[0], true) != null) {
				p = PlayerSearcher.searchOnline(sender, args[0], true);
				sender.teleport(p);
				sender.sendMessage(MessageUtil.color("&9Teleportado!"));
			}
		}
	}

	
	protected void help(Player caller, String[] args) {
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&a() = opcional"));
		caller.sendMessage(MessageUtil.color("&c<> = obrigatório"));
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&6/tp <player>"));
		caller.sendMessage(MessageUtil.color("&6/tp puxar <player>"));
		caller.sendMessage(MessageUtil.color("&6/tp all"));
		caller.sendMessage(MessageUtil.color(" "));
	}
}
