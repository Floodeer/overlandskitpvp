package com.overlands.kitpvp.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.ranks.Rank;

public class PrefsCommand extends CommandBase {

	public PrefsCommand() {
		super(Rank.DEFAULT, "prefs", "settings", "preferencias", "p");
	}

	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player p = (Player)commandSender;
		Main.getPreferences().show(GamePlayer.get(p));
	}
}
