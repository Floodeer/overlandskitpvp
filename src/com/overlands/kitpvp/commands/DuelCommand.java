package com.overlands.kitpvp.commands;

import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.google.common.collect.Lists;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.core.utils.search.PlayerSearcher;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.duels.DuelController;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.ranks.Rank;

public class DuelCommand extends CommandBase {

	public DuelCommand() {
		super(Rank.DEFAULT, false, "desafio", "duelo", "1v1");
	}
	
	protected void help(Player caller, String[] args) {
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&a() = opcional"));
		caller.sendMessage(MessageUtil.color("&c<> = obrigatório"));
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&6/duelo desafiar <player>"));
		caller.sendMessage(MessageUtil.color("&6/duelo aceitar <player>"));
		caller.sendMessage(MessageUtil.color("&6/duelo negar <player>"));
		caller.sendMessage(MessageUtil.color(" "));
	}

	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player sender = (Player)commandSender;
		
		if(args == null || args.length == 0) {
			 help(sender, args);
			 return;
		}
		
		if(args[0].equalsIgnoreCase("desafiar")) {
			if(GamePlayer.get(sender).isInDuel()) {
				sender.sendMessage(MessageUtil.color("&cVocê já está em um duelo."));
				return;
			}
			if(args.length == 1) {
				sender.sendMessage(MessageUtil.color("&cEspecifique um player."));
				return;
			}
			if(PlayerSearcher.searchOnline(sender, args[1], true) != null) {
				Player target = PlayerSearcher.searchOnline(sender, args[1], false);
				if(target == null) {
					sender.sendMessage(MessageUtil.color("&cEspecifique um player."));
					return;
				}
				if(target == sender) {
					sender.sendMessage(MessageUtil.color("&cEspecifique um player."));
					return;
				}
				GamePlayer targetgp = GamePlayer.get(target);
				if(targetgp.isInDuel()) {
					sender.sendMessage(MessageUtil.color("&cEste player já está em um duelo."));
					return;
				}
				Main.get().getDuels().showPart1(sender, target);
				sender.sendMessage(MessageUtil.color("&9Configure seu duelo e clique depois confirme o convite."));
			}
		}else if(args[0].equalsIgnoreCase("aceitar")) {
			if(args.length == 1) {
				sender.sendMessage(MessageUtil.color("&cEspecifique o player que mandou o convite."));
				return;
			}	
			if(PlayerSearcher.searchOnline(sender, args[1], true) != null) {
				GamePlayer gp = GamePlayer.get(sender);
				Player target = PlayerSearcher.searchOnline(sender, args[1], false);
				if(target == null) {
					sender.sendMessage(MessageUtil.color("&cEspecifique um player."));
					return;
				}
				if(target == sender) {
					sender.sendMessage(MessageUtil.color("&cEspecifique um player."));
					return;
				}
				GamePlayer targetgp = GamePlayer.get(target);
				if(gp.hasDuelRequest()) {
					DuelController.accept(targetgp, gp);
				}
			}
		}else if(args[0].equalsIgnoreCase("negar")) {
			if(args.length == 1) {
				sender.sendMessage(MessageUtil.color("&cEspecifique o player que mandou o desafio."));
				return;
			}
			if(PlayerSearcher.searchOnline(sender, args[1], true) != null) {
				GamePlayer gp = GamePlayer.get(sender);
				Player target = PlayerSearcher.searchOnline(sender, args[1], false);
				if(target == null) {
					sender.sendMessage(MessageUtil.color("&cEspecifique um player."));
					return;
				}
				if(target == sender) {
					sender.sendMessage(MessageUtil.color("&cEspecifique um player."));
					return;
				}

				GamePlayer targetgp = GamePlayer.get(target);
				if(gp.hasDuelRequest()) {
					DuelController.denyInvite(gp, targetgp);
				}
			}
		}
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, String commandLabel, String[] args) {
		if(args != null) {
			if(args.length == 0) {
				return getMatches(args[0],  Lists.newArrayList("desafiar", "aceitar", "negar"));
			}else if(args.length == 2) {
				return getPlayerMatches((Player)sender, args[1]);
			}
		}
		return null;
	}
}
