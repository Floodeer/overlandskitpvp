package com.overlands.kitpvp.commands.team;

import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.google.common.collect.Lists;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.core.utils.search.PlayerSearcher;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.team.TeamManager;
import com.overlands.kitpvp.ranks.Rank;

public class TeamCommand extends CommandBase {

	public TeamCommand() {
		super(Rank.DEFAULT, false, "party", "team", "time", "aliado", "dupla");
	}
	
	protected void help(Player caller, String[] args) {
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&a() = opcional"));
		caller.sendMessage(MessageUtil.color("&c<> = obrigatório"));
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&6/dupla convidar <player>"));
		caller.sendMessage(MessageUtil.color("&6/dupla aceitar <player>"));
		caller.sendMessage(MessageUtil.color("&6/dupla ignorar <player>"));
		caller.sendMessage(MessageUtil.color("&6/dupla desfazer"));
		caller.sendMessage(MessageUtil.color(" "));
	}

	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player sender = (Player)commandSender;
		
		if(args == null || args.length == 0) {
			 help(sender, args);
			 return;
		}
		
		if(args[0].equalsIgnoreCase("convidar")) {
			if(GamePlayer.get(sender).getTeam() != null) {
				sender.sendMessage(MessageUtil.color("&cVocê já possui uma dupla, para formar uma nova use &e/dupla desfazer&c."));
				return;
			}
			if(args.length == 1) {
				sender.sendMessage(MessageUtil.color("&cEspecifique um player."));
				return;
			}
			if(PlayerSearcher.searchOnline(sender, args[1], true) != null) {
				Player target = PlayerSearcher.searchOnline(sender, args[1], false);
				GamePlayer targetgp = GamePlayer.get(target);
				if(targetgp.getTeam() != null) {
					sender.sendMessage(MessageUtil.color("&cEste player já possui uma dupla."));
					return;
				}
				TeamManager.createInvite(GamePlayer.get(sender), targetgp);
				sender.sendMessage(MessageUtil.color("&9Convite de equipe enviado."));
			}
		}else if(args[0].equalsIgnoreCase("aceitar")) {
			if(args.length == 1) {
				sender.sendMessage(MessageUtil.color("&cEspecifique o player que mandou o convite."));
				return;
			}
			if(PlayerSearcher.searchOnline(sender, args[1], true) != null) {
				GamePlayer gp = GamePlayer.get(sender);
				Player target = PlayerSearcher.searchOnline(sender, args[1], false);
				GamePlayer targetgp = GamePlayer.get(target);
				if(gp.hasTeamRequest()) {
					TeamManager.accept(targetgp, gp);
					gp.getGame().updateTeams(gp.getPlayer());
					gp.getGame().updateTeams(targetgp.getPlayer());
				}
			}
		}else if(args[0].equalsIgnoreCase("desfazer")) {
			if(GamePlayer.get(sender).getTeam() != null) {
				TeamManager.disband(GamePlayer.get(sender), true);
				return;
			}
		}else if(args[0].equalsIgnoreCase("ignorar")) {
			if(args.length == 1) {
				sender.sendMessage(MessageUtil.color("&cEspecifique o player que mandou o convite."));
				return;
			}
			if(PlayerSearcher.searchOnline(sender, args[1], true) != null) {
				GamePlayer gp = GamePlayer.get(sender);
				Player target = PlayerSearcher.searchOnline(sender, args[1], false);
				GamePlayer targetgp = GamePlayer.get(target);
				if(gp.hasTeamRequest()) {
					TeamManager.denyInvite(gp, targetgp);
				}
			}
		}
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, String commandLabel, String[] args) {
		if(args != null) {
			if(args.length == 0) {
				return getMatches(args[0],  Lists.newArrayList("convidar", "desfazer", "aceitar"));
			}else if(args.length == 2) {
				return getPlayerMatches((Player)sender, args[1]);
			}
		}
		return null;
	}
}
