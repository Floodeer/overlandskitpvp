package com.overlands.kitpvp.commands;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Runner;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.core.utils.minecraft.ProfileLoader;
import com.overlands.core.utils.minecraft.UUIDFetcher;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.ranks.Rank;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FakeCommand extends CommandBase {

    public FakeCommand() {
        super(Rank.YOUTUBER, "fake", "skin");
    }

    @Override
    public void run(CommandSender commandSender, String[] args) {
        Player player = (Player)commandSender;
        if(args == null || args.length == 0) {
            player.sendMessage(MessageUtil.color("&cEspecifique um nome."));
            return;
        }
        if(args[0].equalsIgnoreCase("reset") || args[0].equalsIgnoreCase("resetar") || args[0].equalsIgnoreCase("remover")) {
            if(Main.getProfileManager().hasCustomProfile(player)) {
                Main.getProfileManager().restoreProfile(player);
                player.sendMessage(MessageUtil.color("&ePerfil removido."));
            }
            return;
        }

        if(Main.getProfileManager().hasCustomProfile(player)) {
            Main.getProfileManager().restoreProfile(player);
        }

        if(aliasUsed.equalsIgnoreCase("fake")) {
           if (Main.getProfileManager().isInvalid(args[0])) {
                player.sendMessage(MessageUtil.color("&cPerfil já sendo ultilizado ou desabilitado, tente outro!"));
                return;
            }

            player.sendMessage(MessageUtil.color("&6Carregando novo perfil..."));
            ProfileLoader loader = new ProfileLoader(UUIDFetcher.getUUIDOf(args[0]).toString(), args[0]);
            Main.getProfileManager().applyName(player, args[0], false);

            Runner.make(Main.get()).delay(20).run(() -> {
                Main.getProfileManager().applySkin(player, loader.loadProfileWith(player.getUniqueId()), true);
                player.sendMessage(MessageUtil.color("&aPerfil aplicado com sucesso!"));
            });
        }else{
            Main.getProfileManager().applyName(player, args[0], false);
            player.sendMessage(MessageUtil.color("&aNome aplicado com sucesso!"));
        }
    }
}
