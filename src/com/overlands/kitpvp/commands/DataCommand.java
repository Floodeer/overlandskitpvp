package com.overlands.kitpvp.commands;

import java.util.List;

import com.overlands.kitpvp.database.util.QueryUtils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.google.common.collect.Lists;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.core.utils.search.PlayerSearcher;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.database.interfaces.Callback;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.ranks.Rank;

public class DataCommand extends CommandBase {

	public DataCommand() {
		super(Rank.ADMIN, true, "mysql", "sql", "db", "database", "operation", "execute", "run");
	}

	@Override
	public void run(CommandSender p, String[] args) {
		if (args == null || args.length <= 3) {
			p.sendMessage(MessageUtil.color("&9Argumentos insuficientes. &6/run <player> <operation> <query> <value>"));
			p.sendMessage(MessageUtil.color("&9Dica: Pressione tab para auto-completar."));
			return;
		}
		String target = args[0];
		String operation = args[1];
		String query = args[2];

		Player player = null;

		if (PlayerSearcher.searchExact(target) == null) {
			p.sendMessage(MessageUtil.color("&cPlayer não online, iniciando busca..."));
			PlayerSearcher.searchOffline(target, new Callback<Boolean>() {

				@Override
				public void onCall(Boolean result) {
					if (result) {
						p.sendMessage(MessageUtil.color("&cNenhum player foi encontrado."));
						return;
					} else {
						p.sendMessage(MessageUtil.color("&aPlayer offline encontrado, modificando valores..."));
						if(query.equalsIgnoreCase("mysteryBoxes")) {
							QueryUtils.modifyOfflinePlayer(p, target, "mysteryBoxes", args[3]);
						}else if(query.equalsIgnoreCase("coins")) {
							QueryUtils.modifyOfflinePlayer(p, target, "balance", args[3]);
						}
						QueryUtils.modifyOfflinePlayer(p, target, query.toLowerCase(), args[3]);
						return;
					}
				}
			});
		}else{
			player = PlayerSearcher.searchExact(target);
		}
		if (player == null)
			return;

		GamePlayer gp = GamePlayer.get(player);
		if (operation.equalsIgnoreCase("set") || operation.equalsIgnoreCase("setar")) {
			if (query.equalsIgnoreCase("coins")) {
				if (MathUtils.isDouble(args[3])) {
					gp.setCoins(Integer.parseInt(args[3]));
					p.sendMessage(MessageUtil.color("&aSucesso."));
				} else {
					p.sendMessage(MessageUtil.color("&9Erro ao executar operação > Valor inválido."));
				}
			} else if (query.equalsIgnoreCase("level")) {
				if (MathUtils.isInteger(args[3]) && Integer.parseInt(args[3]) <= 120) {
					gp.setLevel(Integer.parseInt(args[3]));
					p.sendMessage(MessageUtil.color("&aSucesso."));
					Main.getGame().updateTeams(gp.getPlayer());
					Main.getGame().update();
				} else {
					p.sendMessage(MessageUtil.color("&9Erro ao executar operação > Valor inválido ou muito alto."));
				}
			} else if (query.equalsIgnoreCase("score")) {
				if (MathUtils.isInteger(args[3])) {
					gp.setScore(Integer.parseInt(args[3]));
					p.sendMessage(MessageUtil.color("&aSucesso."));
				} else {
					p.sendMessage(MessageUtil.color("&9Erro ao executar operação > Valor inválido."));
				}
			} else if (query.equalsIgnoreCase("kills")) {
				if (MathUtils.isInteger(args[3])) {
					gp.setKills(Integer.parseInt(args[3]));
					p.sendMessage(MessageUtil.color("&aSucesso."));
				} else {
					p.sendMessage(MessageUtil.color("&9Erro ao executar operação > Valor inválido."));
				}
			} else if (query.equalsIgnoreCase("mysteryBoxes")) {
				if (MathUtils.isInteger(args[3])) {
					gp.setMysteryBoxes(Integer.parseInt(args[3]));
					p.sendMessage(MessageUtil.color("&aSucesso."));
				} else {
					p.sendMessage(MessageUtil.color("&9Erro ao executar operação > Valor inválido."));
				}
			} else if (query.equalsIgnoreCase("prestigeLevel")) {
				if (MathUtils.isInteger(args[3])) {
					gp.setPrestigeLevel(Integer.parseInt(args[3]));
					p.sendMessage(MessageUtil.color("&aSucesso."));
					Main.getGame().updateTeams(gp.getPlayer());
					Main.getGame().update();
				} else {
					p.sendMessage(MessageUtil.color("&9Erro ao executar operação > Valor inválido."));
				}
			} else if (query.equalsIgnoreCase("general_level")) {
				if (MathUtils.isInteger(args[3])) {
					gp.setGeneralLevel(Integer.parseInt(args[3]));
					p.sendMessage(MessageUtil.color("&aSucesso."));
					Main.getGame().updateTeams(gp.getPlayer());
					Main.getGame().update();
				} else {
					p.sendMessage(MessageUtil.color("&9Erro ao executar operação > Valor inválido."));
				}
			}
			
		} else if (operation.equalsIgnoreCase("add") || operation.equalsIgnoreCase("adicionar")) {
			if (query.equalsIgnoreCase("coins")) {
				if (MathUtils.isDouble(args[3])) {
					gp.setCoins(gp.getCoins() + Integer.parseInt(args[3]));
					p.sendMessage(MessageUtil.color("&aSucesso."));
				} else {
					p.sendMessage(MessageUtil.color("&9Erro ao executar operação > Valor inválido."));
				}
			} else if (query.equalsIgnoreCase("level")) {
				if (MathUtils.isInteger(args[3]) && Integer.parseInt(args[2]) <= 120 && gp.getLevel() + Integer.parseInt(args[3]) <= 120) {
					gp.setLevel(gp.getLevel() + Integer.parseInt(args[3]));
					p.sendMessage(MessageUtil.color("&aSucesso."));
					Main.getGame().updateTeams(gp.getPlayer());
					Main.getGame().update();
				} else {
					p.sendMessage(MessageUtil.color("&9Erro ao executar operação > Valor inválido ou muito alto."));
				}
				
				Main.getGame().update(gp);
				Main.getGame().updateTeams(gp.getPlayer());
			} else if (query.equalsIgnoreCase("score")) {
				if (MathUtils.isInteger(args[3])) {
					gp.setScore(gp.getScore() + Integer.parseInt(args[3]));
					p.sendMessage(MessageUtil.color("&aSucesso."));
				} else {
					p.sendMessage(MessageUtil.color("&9Erro ao executar operação > Valor inválido."));
				}
			} else if (query.equalsIgnoreCase("kills")) {
				if (MathUtils.isInteger(args[3])) {
					gp.setKills(gp.getKills() + Integer.parseInt(args[3]));
					p.sendMessage(MessageUtil.color("&aSucesso."));
				} else {
					p.sendMessage(MessageUtil.color("&9Erro ao executar operação > Valor inválido."));
				}
			} else if (query.equalsIgnoreCase("mysteryBoxes")) {
				if (MathUtils.isInteger(args[3])) {
					gp.setMysteryBoxes(gp.getMysteryBoxes() + Integer.parseInt(args[3]));
					p.sendMessage(MessageUtil.color("&aSucesso."));
				} else {
					p.sendMessage(MessageUtil.color("&9Erro ao executar operação > Valor inválido."));
				}
			}
		}
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, String commandLabel, String[] args) {
		if(args != null) {
			if(args.length == 1) {
				return getPlayerMatches((Player)sender, args[0]);
			}else if(args.length == 2) {
				return getMatches(args[1],  Lists.newArrayList("set", "add"));
			}else if(args.length == 3) {
				return getMatches(args[2], Lists.newArrayList("coins", "level", "mysteryBoxes", "score", "general_level", "prestigeLevel"));
			}
		}
		return null;
	}
}
