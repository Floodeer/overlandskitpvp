package com.overlands.kitpvp.commands;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.ranks.Rank;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.entity.Player;

public class ClassConfigUpdateCommand extends CommandBase {

    public ClassConfigUpdateCommand() {
        super(Rank.ADMIN, "reloadclasses", "rc", "updateclasses");
    }

    @Override
    public void run(CommandSender sender, String[] args) {
        Player p = (Player)sender;
        try {
            Main.classConfig.load();
            p.sendMessage(MessageUtil.color("&aClasses atualizadas com sucesso."));

            if(args.length > 0 && args[0].equalsIgnoreCase("true")) {
                Main.getGame().getPlayers().forEach(gp -> {
                    p.teleport(gp.isFastRespawn() ? Main.mapConfig.respawn : Main.mapConfig.spawn);
                });
                Bukkit.broadcastMessage(MessageUtil.color("&eOcorreu uma atualização nas classes e você foi mandado para o spawn."));
            }
        }catch(InvalidConfigurationException ex) {
            p.sendMessage(MessageUtil.color("&cErro ao atualizar classes! &4" + ex.getMessage()));
        }
    }
}
