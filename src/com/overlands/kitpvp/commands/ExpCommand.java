package com.overlands.kitpvp.commands;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.core.utils.search.PlayerSearcher;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.event.OverlandsPlayerLevelUpEvent;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.levels.LevelFormatter;
import com.overlands.kitpvp.ranks.Rank;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;


public class ExpCommand extends CommandBase {

	public ExpCommand() {
		super(Rank.ADMIN, false, "exp", "experiencia", "xp");
	}

	@Override
	public void run(CommandSender p, String[] args) {
		Player player = (Player)p;
		if(args == null || args.length <= 1) {
		    player.sendMessage(MessageUtil.color("&c/exp <player> <quantidade>"));
		    return;
        }
		String target = args[0];
		if(PlayerSearcher.searchExact(target) == null) {
            player.sendMessage(MessageUtil.color("&cPlayer inválido ou offline."));
		    return;
        }

		if(!MathUtils.isInteger(args[1]))
		    return;

		int xp = Integer.parseInt(args[1]);

		GamePlayer gp = GamePlayer.get(Main.get().getServer().getPlayer(target));
        int until = LevelFormatter.expUntilNextLevel(gp);

        gp.setExp(gp.getExp()+xp);

        if(gp.getLevel() < 120 && gp.getExp() + xp >= until) {
            gp.setExp(0);
            gp.setLevel(gp.getLevel()+1);
            gp.setGeneralLevel(gp.getGeneralLevel()+1);
            Main.get().getServerManager().call(new OverlandsPlayerLevelUpEvent(gp.getPlayer(), gp.getLevel()-1, gp.getLevel()));
        }
        if(gp.isInGame()) {
            gp.getGame().updateScoreboard(gp, 8, MessageUtil.color("&fPróximo: &b" + LevelFormatter.expUntilNextLevel(gp)));
        }

        player.sendMessage(MessageUtil.color("&eVocê deu &6" + xp + " &exp para &b" + target + "&e."));
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, String commandLabel, String[] args) {
		if(args != null) {
			if(args.length == 1) {
				return getPlayerMatches((Player)sender, args[0]);
			}
		}
		return null;
	}
}
