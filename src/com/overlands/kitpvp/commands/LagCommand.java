package com.overlands.kitpvp.commands;

import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.EntityEffect;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Runner;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.ranks.Rank;

public class LagCommand extends CommandBase {

	public LagCommand() {
		super(Rank.MOD, "lag", "testlag", "checklag", "tps");
	}

	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player sender = (Player)commandSender;
		int processors = Runtime.getRuntime().availableProcessors();

		if (!Main.get().getServerManager().lagCheck) {
			sender.sendMessage(MessageUtil.color("&c[Info] &9Núcleos disponíveis: &b" + processors));
			sender.sendMessage(MessageUtil.color("&cIniciando debug mode..."));
			Runner.make(Main.get()).delay(60).run(() -> {
				Main.get().getServerManager().lagCheck = true;
				Main.get().getServerManager().checkListeners.addAll(Bukkit.getOnlinePlayers().stream().filter(Rank::isStaff).collect(Collectors.toList()));
				if(args != null && args.length > 0 && args[0].equalsIgnoreCase("true")) {
					Runner.make(Main.get()).interval(5).limit(80).run(() -> Main.getPM().getPlayers().stream().filter(g -> !g.isInGame()).forEach((g) -> g.getPlayer().playEffect(EntityEffect.HURT)));
				}
			});

		} else {
			Main.get().getServerManager().lagCheck = false;
			Main.get().getServerManager().checkListeners.clear();
			sender.sendMessage(ChatColor.GREEN + "Debug desativado.");
		}
	}
}
