package com.overlands.kitpvp.commands;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.core.utils.search.PlayerSearcher;
import com.overlands.core.utils.world.WorldUtils;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.ranks.Rank;

public class ClassCommand extends CommandBase {

	public ClassCommand() {
		super(Rank.ADMIN, "classes", "classe", "kit");
	}

	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player p = (Player)commandSender;
		if(args == null || args.length <= 1) {
			p.sendMessage(MessageUtil.color("&9/classes <player/all> <classe>"));
			return;
		}
		if(args[0].equalsIgnoreCase("all")) {
			if(ClassType.fromName(args[1]) == null) {
				p.sendMessage(MessageUtil.color("&9Classe inválida."));
				return;
			}
			Main.getGame().getPlayers().stream().filter(gp -> !WorldUtils.inSpawn(gp.getPlayer().getLocation())).forEach(gp -> {
				gp.setClass(gp.getGame().gameClasses.get(ClassType.fromName(args[1])));
				gp.setClassType(gp.getGame().gameClasses.get(ClassType.fromName(args[1])).getType().toString());
				gp.getSelectedClass().giveItems(gp.getPlayer());
				gp.getPlayer().sendMessage(MessageUtil.color("&9Sua classe foi alterada por &4" + p.getName() + " &9."));
			});
		}else {
			Player target = null;
			if (PlayerSearcher.searchExact(args[0]) == null) {
				p.sendMessage(MessageUtil.color("&9Player inválido. Use &bALL &9 para modificar a classe de todos os players."));
				return;
			}
			target = PlayerSearcher.searchExact(args[0]);
			GamePlayer gp = GamePlayer.get(target);
			if(ClassType.fromName(args[1]) == null) {
				p.sendMessage(MessageUtil.color("&9Classe inválida."));
				return;
			}
			gp.setClass(gp.getGame().gameClasses.get(ClassType.fromName(args[1])));
			gp.setClassType(gp.getGame().gameClasses.get(ClassType.fromName(args[1])).getType().toString());
			gp.getSelectedClass().giveItems(gp.getPlayer());
			gp.getPlayer().sendMessage(MessageUtil.color("&9Sua classe foi alterada por &4" + p.getName() + " &9."));
		}
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, String commandLabel, String[] args) {
		if(args.length == 1) {
			return getPlayerMatches((Player)sender, args[0]);
		}else if(args.length == 2) {
			if(args[1].isEmpty()) {
				return Arrays.stream(ClassType.values())
						.filter(f -> f.getSkillName() != null && !f.getSkillName().isEmpty())
						.map(ClassType::toString)
						.collect(Collectors.toList());
			}else{
				return getMatches(args[1], Arrays.stream(ClassType.values())
						.filter(f -> f.getSkillName() != null && !f.getSkillName().isEmpty())
						.map(ClassType::toString)
						.collect(Collectors.toList()));
			}
 		}
		return null;
	}
}
