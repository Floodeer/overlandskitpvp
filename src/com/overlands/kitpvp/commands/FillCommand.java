package com.overlands.kitpvp.commands;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.search.PlayerSearcher;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.ranks.Rank;

public class FillCommand extends CommandBase {

	public FillCommand() {
		super(Rank.ADMIN, "fill");

	}

	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player p = (Player)commandSender;
		if(args == null || args.length == 0) {
			GamePlayer.get(p).setEnergy(100);
			while(!InventoryUtils.isFull(InventoryUtils.toCraftInventory(p.getInventory()))) {
				p.getInventory().setItem(p.getInventory().firstEmpty(), ItemFactory.create(Material.MUSHROOM_SOUP));
			}	
			p.sendMessage(MessageUtil.color("&9Alterações no seu inventário e sua energia."));
			return;
		}
		if(PlayerSearcher.searchOnline(p, args[0], true) != null) {
			Player target = PlayerSearcher.searchOnline(p, args[0], false);
			GamePlayer.get(target).setEnergy(100);
			while(!InventoryUtils.isFull(InventoryUtils.toCraftInventory(target.getInventory()))) {
				target.getInventory().setItem(target.getInventory().firstEmpty(), ItemFactory.create(Material.MUSHROOM_SOUP));
			}	
			p.sendMessage(MessageUtil.color("&9Alterações no inventário e energia de &b" + target.getName() + "&9."));
		}
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, String commandLabel, String[] args) {
		return getPlayerMatches((Player)sender, args[0]);
	}
}
