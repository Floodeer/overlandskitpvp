package com.overlands.kitpvp.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.ranks.Rank;

public class BukkitCommand extends CommandBase {

	public BukkitCommand() {
		super(Rank.DEFAULT, "bukkit", "pl", "plugins", "spigot", "bungee", "bukkit:plugins", "version");
	}

	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player sender = (Player)commandSender;
		sender.sendMessage("This server is running CraftBukkit version git-Spigot-21fe707-e1ebe52 (MC: 1.8.8) "
				+ "(Implementing API version 1.8.8-R0.1-OVERLANDS-VERSION)");
		return;
	}
}
