package com.overlands.kitpvp.commands;

import org.bukkit.GameMode;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.ranks.Rank;

public class GameModeCommand extends CommandBase {

	public GameModeCommand() {
		super(Rank.LEAD_BUILDER, "gm", "gamemode", "gmc");
	}
	
	
	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player sender = (Player)commandSender;
		if(args == null || args.length == 0) {
			if(sender.getGameMode() == GameMode.SURVIVAL || sender.getGameMode() == GameMode.ADVENTURE) {
				sender.setGameMode(GameMode.CREATIVE);
				sender.sendMessage(MessageUtil.color("&9Modo de jogo modificado para criativo."));
			}else{
				sender.setGameMode(GameMode.SURVIVAL);
				sender.sendMessage(MessageUtil.color("&9Modo de jogo modificado para survival."));
			}
		}else{
			for(String str : gmAliases) {
				if(args[0].equalsIgnoreCase(str)) {
					if(sender.getGameMode() != GameMode.CREATIVE) {
						sender.setGameMode(GameMode.CREATIVE);
						sender.sendMessage(MessageUtil.color("&9Modo de jogo modificado para criativo."));
					}
				}
			}
			for(String str : svAliases) {
				if(args[0].equalsIgnoreCase(str)) {
					if(sender.getGameMode() != GameMode.SURVIVAL) {
						sender.setGameMode(GameMode.SURVIVAL);
						sender.sendMessage(MessageUtil.color("&9Modo de jogo modificado para survival."));
					}
				}
			}	
			for(String str : specAliases) {
				if(args[0].equalsIgnoreCase(str)) {
					if(sender.getGameMode() != GameMode.SPECTATOR) {
						sender.setGameMode(GameMode.SPECTATOR);
						sender.sendMessage(MessageUtil.color("&9Modo de jogo modificado para espectador."));
					}
				}
			}
		}
	}
	
	String[] gmAliases = new String[]{"c", "creative", "1", "criativo"};
	String[] svAliases = new String[]{"0", "survival", "s", "sobrevivencia"};
	String[] specAliases = new String[]{"3", "spectator", "spect", "espectador"};
}
