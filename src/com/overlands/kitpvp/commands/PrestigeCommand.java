package com.overlands.kitpvp.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.levels.Prestige;
import com.overlands.kitpvp.ranks.Rank;

public class PrestigeCommand extends CommandBase {

	public PrestigeCommand() {
		super(Rank.DEFAULT, "prestige", "upar", "levelup");
	}

	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player p = (Player)commandSender;
		Prestige.open(GamePlayer.get(p));

	}
}
