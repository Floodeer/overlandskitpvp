package com.overlands.kitpvp.commands;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.effects.ClassEffectType;
import com.overlands.kitpvp.effects.EffectSelector;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.perks.PerkType;
import com.overlands.kitpvp.ranks.Rank;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.libs.joptsimple.internal.Classes;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class BetaTestCommand extends CommandBase {

    public BetaTestCommand() {
        super(Rank.DEFAULT, "beta", "unlockall");
    }

    @Override
    public void run(CommandSender sender, String[] args) {
        Player p = (Player)sender;
        GamePlayer gp = GamePlayer.get(p);
        if(!Rank.hasPermission(p, Rank.TESTER)) {
            gp.setRank("Tester");
            p.sendMessage(MessageUtil.color("&6Adicionado a lista de Tester, todos os kits, perks e efeitos foram desbloqueados!"));
            Arrays.stream(ClassType.values()).filter(cur -> cur.getClazz() != null).forEach(cur -> gp.getKits().add(cur.toString()));
            Arrays.stream(PerkType.values()).forEach(cur -> gp.getPerks().add(cur.getByName()));
            Arrays.stream(ClassEffectType.values()).forEach(cur -> gp.getEffects().add(cur.toString()));
        }
    }
}
