package com.overlands.kitpvp.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.effects.EffectSelector;
import com.overlands.kitpvp.ranks.Rank;

public class EffectsCommand extends CommandBase {

	public EffectsCommand() {
		super(Rank.DEFAULT, "effects", "efeitos");
	}

	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player sender = (Player)commandSender;
		EffectSelector.show(sender);
	}
}
