package com.overlands.kitpvp.commands;

import java.util.Arrays;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.ranks.Rank;

public class NetworkBoosterCommand extends CommandBase {

	public NetworkBoosterCommand() {
		super(Rank.ADMIN, "booster");
	}
	
	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player sender = (Player)commandSender;
		if(args == null || args.length <= 0) {
			help(sender);
			return;
		}
		if(args[0].equalsIgnoreCase("ativar")) {
			if(Main.RUNNING_BOOSTER) {
				sender.sendMessage(MessageUtil.color("&cO network booster já está ativado."));
				return;
			}
			if(args.length > 1) {
				Main.get().getServerManager().activateBooster(args[1]);
			}else{
				Main.get().getServerManager().activateBooster(sender.getName());
			}
		}else if(args[0].equalsIgnoreCase("desativar")) {
			if(!Main.get().getServerManager().networkBooster) {
				sender.sendMessage(MessageUtil.color("&cNão há nenhum Network Booster ativado."));
				return;
			}
			Main.get().getServerManager().desactiveBooster();
		}
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, String commandLabel, String[] args) {
		String lastArg = args[args.length - 1];
		return getMatches(lastArg, Arrays.asList("ativar", "desativar"));
	}
	

	private void help(Player caller) {
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&a() = opcional"));
		caller.sendMessage(MessageUtil.color("&c<> = obrigatório"));
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&6/booster ativar (player)"));
		caller.sendMessage(MessageUtil.color("&6/chat desativar"));
		caller.sendMessage(MessageUtil.color(" "));
	}
}
