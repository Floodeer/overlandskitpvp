package com.overlands.kitpvp.commands.message;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.core.utils.search.PlayerSearcher;
import com.overlands.kitpvp.ranks.Rank;

public class PrivateMessageCommand extends CommandBase {

	public PrivateMessageCommand() {
		super(Rank.DEFAULT, false, "m", "msg", "message", "tell", "t", "w", "whisper", "me");
	}

	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player sender = (Player)commandSender;
		if (args == null || args.length <= 1) {
			help(sender);
			return;
		}

		Player player = null;
		if (PlayerSearcher.searchOnline(sender, args[0], false) != null) {
			player = PlayerSearcher.searchOnline(sender, args[0], false);
		} else {
			sender.sendMessage(MessageUtil.color("&cPlayer offline."));
			return;
		}
		if(player.hasMetadata("hidden") || player.hasMetadata("god")) {
			sender.sendMessage(MessageUtil.color("&cPlayer offline."));
			return;
		}
		
		String msg = "";
		if (args.length > 1) {
			msg = MessageUtil.combine(args, 1, null, false);
		}
		
		MessageUtil.sendPrivateMessage(sender, player, msg);
	}

	private void help(Player caller) {
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&a() = opcional"));
		caller.sendMessage(MessageUtil.color("&c<> = obrigatório"));
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&6/msg <player> <mensagem>"));
		caller.sendMessage(MessageUtil.color(" "));
	}
}
