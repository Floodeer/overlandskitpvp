package com.overlands.kitpvp.commands.message;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.ranks.Rank;

public class ServerBroadcastCommand extends CommandBase {

	public ServerBroadcastCommand() {
		super(Rank.ADMIN, true, "server", "sbroadcast", "serverbroadcast", "serverb", "serverwarn");
	}
	
	@Override
	public void run(CommandSender sender, String[] args) {
		if(args == null || args.length == 0) {
			help(sender);
			return;
		}
		
		String msg = "";
        msg = MessageUtil.combine(args, 0, null, false);

        MessageUtil.broadcast(ChatColor.RED +  msg);
	}
	
	private void help(CommandSender caller) {
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&a() = opcional"));
		caller.sendMessage(MessageUtil.color("&c<> = obrigatório"));
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&6/broadcast <mensagem>"));
		caller.sendMessage(MessageUtil.color(" "));
	}
}
