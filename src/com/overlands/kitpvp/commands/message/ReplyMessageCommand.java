package com.overlands.kitpvp.commands.message;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.ranks.Rank;


public class ReplyMessageCommand extends CommandBase {

	public ReplyMessageCommand() {
		super(Rank.DEFAULT, "r", "reply", "responder");
	}
	
	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player sender = (Player)commandSender;
		if(args == null || args.length == 0) {
			help(sender);
			return;
		}
		
		if(GamePlayer.get(sender).getLastMessage() == null) {
			sender.sendMessage(MessageUtil.color("&cVocê não tem nenhum player para reenviar uma mensagem."));
			return;
		}
		
		GamePlayer lastMessage = GamePlayer.get(sender).getLastMessage();
		
		String msg = "";
		if (args.length > 0) {
			msg = MessageUtil.combine(args, 0, null, false);
		}
		
		MessageUtil.sendPrivateMessage(sender, lastMessage.getPlayer(), msg);
	}
	
	private void help(Player caller) {
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&a() = opcional"));
		caller.sendMessage(MessageUtil.color("&c<> = obrigatório"));
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&6/r <mensagem>"));
		caller.sendMessage(MessageUtil.color(" "));
	}
}
