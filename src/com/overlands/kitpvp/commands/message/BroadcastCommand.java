package com.overlands.kitpvp.commands.message;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.ranks.Rank;	

public class BroadcastCommand extends CommandBase {

	public BroadcastCommand() { 
		super(Rank.ADMIN, true, "broadcast", "aviso", "global");
	}
	
	@Override
	public void run(CommandSender commandSender, String[] args) {
		Player sender = (Player)commandSender;
		if(args == null || args.length == 0) {
			help(sender);
			return;
		}
		
		String msg = "";
        msg = MessageUtil.combine(args, 0, null, false);

        String format = "%tag%%player%: %message%";
		Rank rank = Rank.fromName(GamePlayer.get(sender).getRank());
		String message = format.replaceAll("%player%", sender.getName()).replaceAll("%tag%", rank.getChatTag()).replaceAll("%message%", (rank.getPriority() > 8 ? MessageUtil.color(ChatColor.RESET + msg) : ChatColor.RESET + msg));
        MessageUtil.broadcast(message);
    }
	
	private void help(Player caller) {
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&a() = opcional"));
		caller.sendMessage(MessageUtil.color("&c<> = obrigatório"));
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&6/broadcast <mensagem>"));
		caller.sendMessage(MessageUtil.color(" "));
	}
}
