package com.overlands.kitpvp.commands;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.google.common.collect.Lists;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.calc.TimeUtils;
import com.overlands.core.utils.command.CommandBase;
import com.overlands.core.utils.search.PlayerSearcher;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.database.interfaces.Callback;
import com.overlands.kitpvp.ranks.Rank;


public class RankCommand extends CommandBase {

	public RankCommand() {
		super(Rank.ADMIN, true, "rank", "rankCore", "grupo");
	}

	private void modifyOfflinePlayer(CommandSender sender, String player, String rank) {
		long t = System.currentTimeMillis();
		sender.sendMessage(MessageUtil.color("&9Verificando data de &b" + player + "&9."));
		Main.getDataManager().doesPlayerExists(player, new Callback<Boolean>() {
			
			@Override
			public void onCall(Boolean result) {
				if(result) {
					Main.get().getDB().getExecutor().execute(new Runnable() {
						PreparedStatement preparedStatement = null;

						@Override
						public void run() {
							try {
								String query = "UPDATE `overlands_kitpvp` SET `rank` = ? WHERE `playername` = ?;";
								preparedStatement = Main.get().getDB().getConnection().get().prepareStatement(query.toString());
								preparedStatement.setString(1, rank);
								preparedStatement.setString(2, player);
								preparedStatement.executeUpdate();
								sender.sendMessage(MessageUtil.color("&9Ação concluída, levou " + TimeUtils.getSince(t) + " para modificar o rank de &a" + player + "&9 para " + Rank.fromName(rank).getColoredTag() + "&9."));
							} catch (SQLException ex) {
								ex.printStackTrace();
								sender.sendMessage(MessageUtil.color("&cOcorreu um erro ao executar operação; " + ex.getLocalizedMessage()));
							} finally {
								if (preparedStatement != null)
									try {
										preparedStatement.close();
									} catch (SQLException e) {
										e.printStackTrace();
									}
							  }
						 }
					});
				}else {
					sender.sendMessage(MessageUtil.color("&cPlayer não encontrado."));
				}
			}
		});
	}

	@Override
	public void run(CommandSender sender, String[] args) {
		if (args == null || args.length <= 0) {
			help(sender);
			return;
		}
		if (args[0].equalsIgnoreCase("set") || args[0].equalsIgnoreCase("setar")) {
			if (args.length == 1) {
				sender.sendMessage(MessageUtil.color("&cEspecifique um player."));
				return;
			}
			if (args.length == 2) {
				sender.sendMessage(MessageUtil.color("&cEspecifique um rank."));
				return;
			}
			if (Rank.fromName(args[2]) == null) {
				sender.sendMessage(MessageUtil.color("&cRank desconhecido."));
				return;
			}
			Player player = PlayerSearcher.searchExact(args[1]);
			if(player == null) {
				modifyOfflinePlayer(sender, args[1], args[2]);
			} else {
				
				Main.getPM().getPlayer(player.getUniqueId()).setRank(args[2]);
				sender.sendMessage(MessageUtil.color("&aRank de " + player.getName() + " modificado para " + Rank.fromName(args[2]).getColoredTag() + "&a."));
			}
		}
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, String commandLabel, String[] args) {
		if(args.length == 1) {
			return Collections.singletonList("setar");
		}
		if(args.length == 2) {
			return getPlayerMatches((Player)sender, args[1]);
		}else if(args.length == 3) {
			List<String> ranks = Lists.newArrayList();
			Arrays.stream(Rank.values()).map(Rank::toString).forEach(ranks::add);
			return getMatches(args[2], ranks);
 		}
		return null;
	}

	private void help(CommandSender caller) {
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&a() = opcional"));
		caller.sendMessage(MessageUtil.color("&c<> = obrigatório"));
		caller.sendMessage(MessageUtil.color(" "));
		caller.sendMessage(MessageUtil.color("&6/rank setar <player> <rank>"));
		caller.sendMessage(MessageUtil.color(" "));
	}
}
