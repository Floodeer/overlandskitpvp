package com.overlands.kitpvp;

import com.overlands.core.utils.ConfigDeserializer;

import java.io.File;
import java.util.Arrays;

public class ClassConfig extends ConfigDeserializer {

    public ClassConfig(File configFile) {
        super(configFile, Arrays.asList("##### Importante #####",
                "EPH = Energia por hit, quanto de energia o player recebe ao acertar um hit",
                "EPS = Energia por segundo, quanto de energia por segundo o player recebe",
                " ",
                "Se a classe for desabilitada é necessário adicionar um argumento extra no comando de reloadclasses para remover os players que estão com a classe selecionada /reloadclasses true"));
    }

    @ConfigOptions(name = "Classes.Herobrine.Enabled")
    public boolean herobrineEnabled = true;

    @ConfigOptions(name = "Classes.Herobrine.Damage")
    public double herobrineDamage = 3.0;

    @ConfigOptions(name = "Classes.Herobrine.Price")
    public int herobrinePrice = 0;

    @ConfigOptions(name = "Classes.Herobrine.EPH")
    public int herobrineEPH = 12;

    @ConfigOptions(name = "Classes.Herobrine.EPS")
    public int herobrineEPS = 0;

    @ConfigOptions(name = "Classes.Enderman.Enabled")
    public boolean endermanEnabled = true;

    @ConfigOptions(name = "Classes.Enderman.Damage")
    public double endermanDamage = 0;

    @ConfigOptions(name = "Classes.Enderman.Price")
    public int endermanPrice = 5500;

    @ConfigOptions(name = "Classes.Enderman.EPH")
    public int endermanEPH = 17;

    @ConfigOptions(name = "Classes.Enderman.EPS")
    public int endermanEPS = 0;

    @ConfigOptions(name = "Classes.Creeper.Enabled")
    public boolean creeperEnabled = true;

    @ConfigOptions(name = "Classes.Creeper.Damage")
    public double creeperDamage = 4.0;

    @ConfigOptions(name = "Classes.Creeper.Price")
    public int creeperPrice = 0;

    @ConfigOptions(name = "Classes.Creeper.EPH")
    public int creeperEPH = 18;

    @ConfigOptions(name = "Classes.Creeper.EPS")
    public int creeperEPS = 0;

    @ConfigOptions(name = "Classes.Dreadlord.Enabled")
    public boolean dreadlordEnabled = true;

    @ConfigOptions(name = "Classes.Dreadlord.Damage")
    public double dreadlordDamage = 5.0;

    @ConfigOptions(name = "Classes.Dreadlord.Price")
    public int dreadlordPrice = 15000;

    @ConfigOptions(name = "Classes.Dreadlord.EPH")
    public int dreadlordEPH = 12;

    @ConfigOptions(name = "Classes.Dreadlord.EPS")
    public int dreadlordEPS = 0;

    @ConfigOptions(name = "Classes.Cryomancer.Enabled")
    public boolean cryomancerEnabled = true;

    @ConfigOptions(name = "Classes.Cryomancer.Damage")
    public double cryomancerDamage = 4.0;

    @ConfigOptions(name = "Classes.Cryomancer.Price")
    public int cryomancerPrice = 5600;

    @ConfigOptions(name = "Classes.Cryomancer.EPH")
    public int cryomancerEPH = 10;

    @ConfigOptions(name = "Classes.Cryomancer.EPS")
    public int cryomancerEPS = 0;

    @ConfigOptions(name = "Classes.Pigman.Enabled")
    public boolean pigmanEnabled = true;

    @ConfigOptions(name = "Classes.Pigman.Damage")
    public double pigmanDamage = 4.5;

    @ConfigOptions(name = "Classes.Pigman.Price")
    public int pigmanPrice = 13800;

    @ConfigOptions(name = "Classes.Pigman.EPH")
    public int pigmanEPH = 8;

    @ConfigOptions(name = "Classes.Pigman.EPS")
    public int pigmanEPS = 0;

    @ConfigOptions(name = "Classes.Shaman.Enabled")
    public boolean shamanEnabled = true;

    @ConfigOptions(name = "Classes.Shaman.Damage")
    public double shamanDamage = 1.75;

    @ConfigOptions(name = "Classes.Shaman.Price")
    public int shamanPrice = 14500;

    @ConfigOptions(name = "Classes.Shaman.EPH")
    public int shamanEPH = 7;

    @ConfigOptions(name = "Classes.Shaman.EPS")
    public int shamanEPS = 0;

    @ConfigOptions(name = "Classes.Spider.Enabled")
    public boolean spiderEnabled = true;

    @ConfigOptions(name = "Classes.Spider.Damage")
    public double spiderDamage = 7.5;

    @ConfigOptions(name = "Classes.Spider.Price")
    public int spiderPrice = 8000;

    @ConfigOptions(name = "Classes.Spider.EPH")
    public int spiderEPH = 0;

    @ConfigOptions(name = "Classes.Spider.EPS")
    public int spiderEPS = 12;

    @ConfigOptions(name = "Classes.Arcanist.Enabled")
    public boolean arcanistEnabled = true;

    @ConfigOptions(name = "Classes.Arcanist.Damage")
    public double arcanistDamage = 1.25;

    @ConfigOptions(name = "Classes.Arcanist.Price")
    public int arcanistPrice = 0;

    @ConfigOptions(name = "Classes.Arcanist.EPH")
    public int arcanistEPH = 36;

    @ConfigOptions(name = "Classes.Arcanist.EPS")
    public int arcanistEPS = 0;

    @ConfigOptions(name = "Classes.Infected.Enabled")
    public boolean infectedEnabled = true;

    @ConfigOptions(name = "Classes.Infected.Damage")
    public double infectedDamage = 1;

    @ConfigOptions(name = "Classes.Infected.Price")
    public int infectedPrice = 9000;

    @ConfigOptions(name = "Classes.Infected.EPH")
    public int infectedEPH = 5;

    @ConfigOptions(name = "Classes.Infected.EPS")
    public int infectedEPS = 1;

    @ConfigOptions(name = "Classes.Golem.Enabled")
    public boolean golemEnabled = true;

    @ConfigOptions(name = "Classes.Golem.Damage")
    public double golemDamage = 5.0;

    @ConfigOptions(name = "Classes.Golem.Price")
    public int golemPrice = 12000;

    @ConfigOptions(name = "Classes.Golem.EPH")
    public int golemEPH = 10;

    @ConfigOptions(name = "Classes.Golem.EPS")
    public int golemEPS = 0;

    @ConfigOptions(name = "Classes.Paladin.Enabled")
    public boolean paladinEnabled = true;

    @ConfigOptions(name = "Classes.Paladin.Damage")
    public double paladinDamage = 3.75;

    @ConfigOptions(name = "Classes.Paladin.Price")
    public int paladinPrice = 13200;

    @ConfigOptions(name = "Classes.Paladin.EPH")
    public int paladinEPH = 9;

    @ConfigOptions(name = "Classes.Paladin.EPS")
    public int paladinEPS = 0;

    @ConfigOptions(name = "Classes.Pyromancer.Enabled")
    public boolean pyromancerEnabled = true;

    @ConfigOptions(name = "Classes.Pyromancer.Damage")
    public double pyromancerDamage = 3.0;

    @ConfigOptions(name = "Classes.Pyromancer.Price")
    public int pyromancerPrice = 6200;

    @ConfigOptions(name = "Classes.Pyromancer.EPH")
    public int pyromancerEPH = 12;

    @ConfigOptions(name = "Classes.Pyromancer.EPS")
    public int pyromancerEPS = 0;

    @ConfigOptions(name = "Classes.Pirata.Enabled")
    public boolean pirataEnabled = true;

    @ConfigOptions(name = "Classes.Pirata.Damage")
    public double pirataDamage = 3.5;

    @ConfigOptions(name = "Classes.Pirata.Price")
    public int pirataPrice = 12400;

    @ConfigOptions(name = "Classes.Pirata.EPH")
    public int pirataEPH = 10;

    @ConfigOptions(name = "Classes.Pirata.EPS")
    public int pirataEPS = 0;

    @ConfigOptions(name = "Classes.Falcon.Enabled")
    public boolean falconEnabled = true;

    @ConfigOptions(name = "Classes.Falcon.Damage")
    public double falconDamage = 8.5;

    @ConfigOptions(name = "Classes.Falcon.Price")
    public int falconPrice = 6500;

    @ConfigOptions(name = "Classes.Falcon.EPH")
    public int falconEPH = 18;

    @ConfigOptions(name = "Classes.Falcon.EPS")
    public int falconEPS = 0;

    @ConfigOptions(name = "Classes.Magma.Enabled")
    public boolean magmaEnabled = true;

    @ConfigOptions(name = "Classes.Magma.Damage")
    public double magmaDamage = 3.5;

    @ConfigOptions(name = "Classes.Magma.Price")
    public int magmaPrice = 14200;

    @ConfigOptions(name = "Classes.Magma.EPH")
    public int magmaEPH = 5;

    @ConfigOptions(name = "Classes.Magma.EPS")
    public int magmaEPS = 8;

    @ConfigOptions(name = "Classes.Electro.Enabled")
    public boolean electroEnabled = true;

    @ConfigOptions(name = "Classes.Electro.Damage")
    public double electroDamage = 2.0;

    @ConfigOptions(name = "Classes.Electro.Price")
    public int electroPrice = 0;

    @ConfigOptions(name = "Classes.Electro.EPH")
    public int electroEPH = 8;

    @ConfigOptions(name = "Classes.Electro.EPS")
    public int electroEPS = 0;

    @ConfigOptions(name = "Classes.Epsilon.Enabled")
    public boolean epsilonEnabled = true;

    @ConfigOptions(name = "Classes.Epsilon.Damage")
    public double epsilonDamage = 4.25;

    @ConfigOptions(name = "Classes.Epsilon.Price")
    public int epsilonPrice = 7000;

    @ConfigOptions(name = "Classes.Epsilon.EPH")
    public int epsilonEPH = 8;

    @ConfigOptions(name = "Classes.Epsilon.EPS")
    public int epsilonEPS = 1;

    @ConfigOptions(name = "Classes.Skeleton.Enabled")
    public boolean skeletonEnabled = true;

    @ConfigOptions(name = "Classes.Skeleton.Damage")
    public double skeletonDamage = 4.5;

    @ConfigOptions(name = "Classes.Skeleton.Price")
    public int skeletonPrice = 7000;

    @ConfigOptions(name = "Classes.Skeleton.EPH")
    public int skeletonEPH = 5;

    @ConfigOptions(name = "Classes.Skeleton.EPS")
    public int skeletonEPS = 7;

    @ConfigOptions(name = "Classes.Blaze.Enabled")
    public boolean blazeEnabled = true;

    @ConfigOptions(name = "Classes.Blaze.Damage")
    public double blazeDamage = 4.75;

    @ConfigOptions(name = "Classes.Blaze.Price")
    public int blazePrice = 0;

    @ConfigOptions(name = "Classes.Blaze.EPH")
    public int blazeEPH = 2;

    @ConfigOptions(name = "Classes.Blaze.EPS")
    public int blazeEPS = 7;

    @ConfigOptions(name = "Classes.Magnatition.Enabled")
    public boolean magnatitionEnabled = true;

    @ConfigOptions(name = "Classes.Magnatition.Damage")
    public double magnatitionDamage = 4.75;

    @ConfigOptions(name = "Classes.Magnatition.Price")
    public int magnatitionPrice =14000;

    @ConfigOptions(name = "Classes.Magnatition.EPH")
    public int magnatitionEPH = 6;

    @ConfigOptions(name = "Classes.Magnatition.EPS")
    public int magnatitionEPS = 1;

    @ConfigOptions(name = "Classes.Rumbler.Enabled")
    public boolean rumblerEnabled = true;

    @ConfigOptions(name = "Classes.Rumbler.Damage")
    public double rumblerDamage = 4.25;

    @ConfigOptions(name = "Classes.Rumbler.Price")
    public int rumblerPrice = 1000;

    @ConfigOptions(name = "Classes.Rumbler.EPH")
    public int rumblerEPH = 5;

    @ConfigOptions(name = "Classes.Rumbler.EPS")
    public int rumblerEPS = 5;

    @ConfigOptions(name = "Classes.Wizard.Enabled")
    public boolean wizardEnabled = true;

    @ConfigOptions(name = "Classes.Wizard.Damage")
    public double wizardDamage = 3.75;

    @ConfigOptions(name = "Classes.Wizard.Price")
    public int wizardPrice = 8600;

    @ConfigOptions(name = "Classes.Wizard.EPH")
    public int wizardEPH = 0;

    @ConfigOptions(name = "Classes.Wizard.EPS")
    public int wizardEPS = 12;

    @ConfigOptions(name = "Classes.Lana.Enabled")
    public boolean lanaEnabled = true;

    @ConfigOptions(name = "Classes.Lana.Damage")
    public double lanaDamage = 1.5;

    @ConfigOptions(name = "Classes.Lana.Price")
    public int lanaPrice = 13600;

    @ConfigOptions(name = "Classes.Lana.EPH")
    public int lanaEPH = 5;

    @ConfigOptions(name = "Classes.Lana.EPS")
    public int lanaEPS = 7;

    @ConfigOptions(name = "Classes.Crusader.Enabled")
    public boolean crusaderEnabled = true;

    @ConfigOptions(name = "Classes.Crusader.Damage")
    public double crusaderDamage = 3.0;

    @ConfigOptions(name = "Classes.Crusader.Price")
    public int crusaderPrice = 19000;

    @ConfigOptions(name = "Classes.Crusader.EPH")
    public int crusaderEPH = 7;

    @ConfigOptions(name = "Classes.Crusader.EPS")
    public int crusaderEPS = 1;

    @ConfigOptions(name = "Classes.Ancient.Enabled")
    public boolean ancientEnabled = true;

    @ConfigOptions(name = "Classes.Ancient.Damage")
    public double ancientDamage = 2.25;

    @ConfigOptions(name = "Classes.Ancient.Price")
    public int ancientPrice = 7400;

    @ConfigOptions(name = "Classes.Ancient.EPH")
    public int ancientEPH = 9;

    @ConfigOptions(name = "Classes.Ancient.EPS")
    public int ancientEPS = 0;

    @ConfigOptions(name = "Classes.Elenne.Enabled")
    public boolean elenneEnabled = true;

    @ConfigOptions(name = "Classes.Elenne.Damage")
    public double elenneDamage = 4.0;

    @ConfigOptions(name = "Classes.Elenne.Price")
    public int elennePrice = 23000;

    @ConfigOptions(name = "Classes.Elenne.EPH")
    public int elenneEPH = 12;

    @ConfigOptions(name = "Classes.Elenne.EPS")
    public int elenneEPS = 0;

    @ConfigOptions(name = "Classes.Shadow.Enabled")
    public boolean shadowEnabled = true;

    @ConfigOptions(name = "Classes.Shadow.Damage")
    public double shadowDamage = 3.25;

    @ConfigOptions(name = "Classes.Shadow.Price")
    public int shadowPrice = 0;

    @ConfigOptions(name = "Classes.Shadow.EPH")
    public int shadowEPH = 12;

    @ConfigOptions(name = "Classes.Shadow.EPS")
    public int shadowEPS = 0;

    @ConfigOptions(name = "Classes.Fighter.Enabled")
    public boolean fighterEnabled = true;

    @ConfigOptions(name = "Classes.Fighter.Damage")
    public double fighterDamage = 4.5;

    @ConfigOptions(name = "Classes.Fighter.Price")
    public int fighterPrice = 28000;

    @ConfigOptions(name = "Classes.Fighter.EPH")
    public int fighterEPH = 7;

    @ConfigOptions(name = "Classes.Fighter.EPS")
    public int fighterEPS = 1;

    @ConfigOptions(name = "Classes.Squid.Enabled")
    public boolean squidEnabled = true;

    @ConfigOptions(name = "Classes.Squid.Damage")
    public double squidDamage = 2.75;

    @ConfigOptions(name = "Classes.Squid.Price")
    public int squidPrice = 8600;

    @ConfigOptions(name = "Classes.Squid.EPH")
    public int squidEPH = 9;

    @ConfigOptions(name = "Classes.Squid.EPS")
    public int squidEPS = 0;

    @ConfigOptions(name = "Classes.Automaton.Enabled")
    public boolean automatonEnabled = true;

    @ConfigOptions(name = "Classes.Automaton.Damage")
    public double automatonDamage = 3.0;

    @ConfigOptions(name = "Classes.Automaton.Price")
    public int automatonPrice = 35000;

    @ConfigOptions(name = "Classes.Automaton.EPH")
    public int automatonEPH = 1;

    @ConfigOptions(name = "Classes.Automaton.EPS")
    public int automatonEPS = 7;

    @ConfigOptions(name = "Classes.Hunter.Enabled")
    public boolean hunterEnabled = true;

    @ConfigOptions(name = "Classes.Hunter.Price")
    public int hunterPrice = 30000;

    @ConfigOptions(name = "Classes.Hunter.EPH")
    public int hunterEPH = 1;

    @ConfigOptions(name = "Classes.Hunter.EPS")
    public int hunterEPS = 5;

}
