package com.overlands.kitpvp;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.calc.TimeUtils;

public class MapBackupHandler {
	
	/**
	 * This is a File Stream operation, so we will run on a new Single Thread Executor.
	 */
	public static void executeBackup(String name) {
		Main.get().getLogger().info("Starting backup task in a new Thread Executor.");
		MessageUtil.sendRawStaffMessage("&7&l&o>>> Preparando um novo executor...");
		long now = System.currentTimeMillis();
		
		ExecutorService singleExecutor = Executors.newSingleThreadExecutor();
		
		singleExecutor.submit(() -> {
			File world = Bukkit.getWorlds().get(0).getWorldFolder();
			String path = world.getAbsolutePath();
			Path sourceDir = Paths.get(world.getAbsolutePath());
			try {
	            ZipOutputStream outputStream = new ZipOutputStream(new FileOutputStream(path.concat(name + ".zip")));
	            Files.walkFileTree(sourceDir, new SimpleFileVisitor<Path>() {
	                @Override
	                public FileVisitResult visitFile(Path file, BasicFileAttributes attributes) {
	                    try {
	                        Path targetFile = sourceDir.relativize(file);
	                        outputStream.putNextEntry(new ZipEntry(targetFile.toString()));
	                        byte[] bytes = Files.readAllBytes(file);
	                        outputStream.write(bytes, 0, bytes.length);
	                        outputStream.closeEntry();
	                    } catch (IOException e) {
	                        e.printStackTrace();
	                    }
	                    return FileVisitResult.CONTINUE;
	                }
	            });
	            outputStream.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	            MessageUtil.sendRawStaffMessage("&c&l&o>>> Ocorreu um erro na tarefa! " + e.getMessage());
	        } finally {
	        	Main.get().getLogger().info("Executor shutting down, it took " + TimeUtils.sinceExact(now) + " to complete.");
	        	MessageUtil.sendRawStaffMessage("&7&l&o>>> Tarefa executada, " + TimeUtils.since(now) + " Desligando executor.");
	            singleExecutor.shutdownNow();
	    		Main.get().getExplosionManager().setExplosions(true);
	    	   	MessageUtil.sendStaffMessage("&7&l&o>>> Executor finalizado.");
	        }
		});
    }
	
	public static void onEnable() {
		Main.get().getLogger().info("Starting an world update before starting.");
		
		File file = new File(Main.get().getDataFolder() + File.separator + "backups" + File.separator + "world");
		if(!file.exists()) {
			try {
				create();
			}catch(IOException ex) {
				Main.get().getLogger().severe("Failed to execute UnZip stream.");
			}finally {
				if(!file.exists())
					return;
				
				File server = new File(Main.get().getServer().getWorldContainer().getAbsolutePath(), "world");
				if(server.exists())
					server.delete();
				try {
					FileUtils.copyDirectory(file, new File(Main.get().getServer().getWorldContainer().getAbsolutePath()));
				}catch(IOException ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	
	static void create() throws IOException {
		Main.get().saveResource("world.zip", false);
		File zip = new File(Main.get().getDataFolder(), "world.zip");
		File destDir = new File(Main.get().getDataFolder(), "backups" + File.separator + "backups" + File.separator + "world");
		if (!destDir.exists()) {
			destDir.mkdirs();
		}
		ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zip));
		ZipEntry entry = zipIn.getNextEntry();
		while (entry != null) {
			String filePath = destDir + File.separator + entry.getName();
			if (!entry.isDirectory()) {
				BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
				byte[] bytesIn = new byte[4096];
				int read = 0;
				while ((read = zipIn.read(bytesIn)) != -1) {
					bos.write(bytesIn, 0, read);
				}
				bos.close();
			} else {
				File dir = new File(filePath);
				dir.mkdir();
			}
			zipIn.closeEntry();
			entry = zipIn.getNextEntry();
		}
		zipIn.close();
		zip.delete();
	}

}
