package com.overlands.kitpvp.achievements;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Util;
import com.overlands.kitpvp.game.GamePlayer;
import org.apache.commons.lang.StringUtils;

public enum AchievementType {

	
	//General
	EVENT_WIN("event_win"),
	EVENT_KILL("event_kill"),
	LEGENDARY_BOX("legendary_box"),
	TOP_KILLS("top_kills"),
	PRESTIGE("prestige_player"),
	ONE_VS_ONE_WIN("one_vs_one_win"),
	KILLS_100("kills_100"),
	KILLS_1000("kills_1000"),
	KILLS_5000("kills_5000"),
	KILLS_10000("kills_10000");

	String type;

	private AchievementType(String type) {
		this.type = type;
	}

	public void progressKills(GamePlayer player, int count) {
		if(!type.startsWith("kills"))
			return;

		if(player.getKills() == Integer.parseInt(StringUtils.substringAfter(type, "_"))) {
			player.getPlayer().sendMessage(MessageUtil.color("&a&m----------------------------"));
			MessageUtil.sendCentredMessage(player.getPlayer(), "&f&lConquista Desbloqueada!");
			player.getPlayer().sendMessage(" ");
			MessageUtil.sendCentredMessage(player.getPlayer(), "&e+500 coins");
		}
	}
}
