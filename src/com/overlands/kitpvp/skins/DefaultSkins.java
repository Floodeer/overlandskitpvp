package com.overlands.kitpvp.skins;

import java.util.UUID;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import com.overlands.core.utils.MessageUtil;
import com.overlands.kitpvp.game.classes.ClassType;

public class DefaultSkins implements SkinDataManager {

    @Override
    public GameProfile getType(ClassType c) {
        GameProfile skin = null;
        switch (c) {
            case FIGHTER:
                GameProfile skin10070 = new GameProfile(UUID.fromString("e1b951c3-21ff-31b9-9a5f-0c7412dea1e9"), "Fighter");
                skin10070.getProperties().put("textures", new Property(
                        "textures",
                        "eyJ0aW1lc3RhbXAiOjE0OTE5NTE1OTg2MDQsInByb2ZpbGVJZCI6IjNlMjZiMDk3MWFjZDRjNmQ5MzVjNmFkYjE1YjYyMDNhIiwicHJvZmlsZU5hbWUiOiJOYWhlbGUiLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzNjNTRhNzRlZTBhNTg2ZmM3OTc2Y2ViZGU1YzViNWE4Y2I4NTYwYmRmZjUyNGFkYzRjNjlhNjMxYTM5ZGE4In19fQ==",
                        "FAcj0b38p+O+GMUXyEqrNvrE9AapnA8Ts2qTZRO3OQ641ScYRwyN8G5NponF1fpYuepvmKlbwvGRlXt7+ipxs2fNFW/phbvbnbKo5bc8VGrRgL4CSdDITskg5LrPCShFnhWoBiKiZNRuCryP6MfL7pSzjyUaT40bneBLphcZKRNZvKI/XgKqJAFwwedCAXItSq2mwGAiBb+5UphELU5mdiCoKrMdp5PM5Mmznu3WJeI+o/2H8JfuNFWokRXoqerqw6LThrSAejx5BiIjy0z30nbM+nhdYJ/Lw/ZS6FwtNTi/I/YqiC1XglMZDFFWtS7/07N4D9QF/qDn7eseLLdkv7k9MVKPuWKcdDk+dIlrWuN7QT3xAsN7QErP1smdyey9vGfgKthYRuZvacQPOf05s1ObIandCVDGCpfS2RtYLWAfILW2GHU/4ItO7gRXuRVoJaeSrKXO4lcDJnMdQGAf6v16RcwQcnvCk9Z1W1DFzHibct+AnzFhgC+RDMbbz5T2owCmaBcT+bo2mRYZ2ii1gIrOJ3XhFHyZ0U09oyvasN8ZBzsxs3OFbomfEKPaV2LNVSXxe3cNM742vAIyL0yOWedxWADkdCilw7A38PwKuJxTnlxB5y28oRb0LUlVa5NWaKRJNbirdHh2mOgXr2GQfAbSotN+nnIiwsPB7S6E6OQ="));
                return skin10070;
            case ELENNE:
                GameProfile skin408588 = new GameProfile(UUID.fromString("0cb8b105-9061-4649-a610-fc23638f3357"), MessageUtil.color("&c&lELENNE"));
                skin408588.getProperties().put("textures", new Property("textures", "eyJ0aW1lc3RhbXAiOjE1NDI2NDQ0MjM1NTEsInByb2ZpbGVJZCI6ImRhNzQ2NWVkMjljYjRkZTA5MzRkOTIwMTc0NDkxMzU1IiwicHJvZmlsZU5hbWUiOiJEYW5jaW5nRG9nZ29fIiwic2lnbmF0dXJlUmVxdWlyZWQiOnRydWUsInRleHR1cmVzIjp7IlNLSU4iOnsidXJsIjoiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS81OGNmMzEyZjQxZTljOTg1ZjQ4M2FkNmVkYjk0MGU4NzdhZGVhNzM0MTgzNmFkMWIyYTZkMWE3NWEyNTRmMjYifX19", "HsDeVlffgUdMU7/RCqmYrMt8Ry+b/5SVwli2J9iqmAHyQXpbYjA04SXfLdtDoRsvkPeQAx5Bmxo8cDNNu6fmlTFLpCjnr8XhLs0dnn0aeOC3pDMEF1vHvGyOCGMcYako6ahWyisdQmuKrJ1Nm3txqiwBWo+Gc9eokiL8P+BakxlvFGfWej6Qqnl4vIuWGX9hq9r+aBtrQ0aEdc9WwfTaZFgVHz3qMCM3Kxwd75qgbY/ybBY1OzQWWyNZfxuABSWnj1prNt3ANJs/F1kgM6ZuNA/GXeeNdTG1M7d69yG1DtU/kV5qr8JUVxDR4n5PjsQYTI2oTFvluf1KGgvTQwTMCEyZRRKWm31ibUWjrYLvoGAQ46UYL5nblE0JYOOOhjBiwfGqHVqkVhwpm/7snm8ue75Wvc4oaS+R1HPMaleJ47DGvXZFs42mPXq0cpZwfEBA+K5LaACKoNFhZu99OhaZzoGCXkSQRsklv7BWvsqUnBLRO9aksnhhhPaT8e1lQKYfIQpizpuvJPuXn4P3zPT2M1TARxeNn3ASnl9P0eeJB46BARrjaIuRNOABjwderSVGBFixLjv5KHU8WqTNvVEjxx/+zhW+l1+Z+y+kyqxkki5SkGKRP60/9HJQJQrbXXgURvV+Gx28SONuG8fq+1jW0cHbw8T2PsdDX2TZqGLGe0o="));
                skin = skin408588;
                break;
            case ANCIENT:
                GameProfile ancient = new GameProfile(UUID.fromString("f901d2d7-f70f-373b-9686-6ec054a18095"), MessageUtil.color("&6&lANCIENT"));
                ancient.getProperties().put("textures", new Property(
                        "textures",
                        "eyJ0aW1lc3RhbXAiOjE1MDEwOTA3MjAyMzYsInByb2ZpbGVJZCI6ImE5MGI4MmIwNzE4NTQ0ZjU5YmE1MTZkMGY2Nzk2NDkwIiwicHJvZmlsZU5hbWUiOiJJbUZhdFRCSCIsInNpZ25hdHVyZVJlcXVpcmVkIjp0cnVlLCJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvODc1M2VhOWQ3NDM0MmZhYmIzODlmN2EyZjE5NzhlZjhkNWQ2OTJkYzIwNjgzZmRiZjVkNWQ2NGQ1NTg5YTY4ZiJ9fX0=",
                        "sJlM044bE80v/gdrXIdYrYCdzVAwPdhSsIIpc1cKv1GM3U//Q72etTqxDLnHMrkZNOKOQe60sq+cLRmpevlqAyp2i7pXeAlYD1gwG9Q4NyAjktwfTuKBFay3wP7ucE8MpPwntF0D4W/pPOpXZQo/SYYwnICsd1rVqkVyYmOtXTU12GEuOWCtddua1zSvzzd7XrLDI//L6Eb7FDnitTY7AiedL37ItbTMhmoUG2NYXiS4Zxb3e2aPN0s48sxZoIoJVyrvUTEf0ZIygsOZzKEs/ZbCzjG3HsHbymiTZ/IXIrbTWidGKqBUQ9b5Y1TEZKSTeQr8YQ59cALZ8nTUZ5jXvx/JdWA4+RA0zUOIGuRxV/2zwEdAOljmIaI6VEq6F+GInviGysv4Hjzmkd2m8hZyKZOTE7iVp8i/1jqDnT4ihnR7KAFA+2ssGOhJsUFupA5S+GqEhjJI7fh2llRRXiNW1cekecSs1uiEjwC7nq9laDQS9uEanH5XX1oaaIoO00IRTMcWuksCMEcDRmU30oipn5tfVS7kJzqr1PYhWt30Nyj2gIFXScdwAJnj4TPh8Nheeb10xcCWVJMNZTI0r8Baglu7XlVnwdsPhaJzzfMqBb3mPkfB9Y+OMvfFfE91bBmKZlok7iw+mTEVdg3Ev1W98SgUCJsaPnKTQFT8dGUXm1g="));
                skin = ancient;
                break;
            case LANA:
                GameProfile skin25088 = new GameProfile(UUID.fromString("799df640-f93e-3690-8faa-67aaa60f82ad"), MessageUtil.color("&e&lLANA"));
                skin25088.getProperties().put("textures", new Property(
                        "textures",
                        "eyJ0aW1lc3RhbXAiOjE1MDExMzU1MDYzODAsInByb2ZpbGVJZCI6IjNlMjZiMDk3MWFjZDRjNmQ5MzVjNmFkYjE1YjYyMDNhIiwicHJvZmlsZU5hbWUiOiJOYWhlbGUiLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlL2U4ODljMTgyMTAxOGY3MmEzNmFkZDRhNmU3MWZiZTU3ZGViZmY1ZjI1MTM5NTUzZTY4NmZiNTRjZjlhNzYxIn19fQ==",
                        "ZA005kz1PTDwPXGhrvkifJcCwizDe4zKQioHTblsk2w66QqXTtq+u1bcX3Ssi5i/Wj/xJxG8ro9f0KkvXN1n7vU8pR+Nhz0ZbdhQU2+JDXQy4OC/CFtwjXfwIreLITjrsSiffhvah+FUg5pbBG20AOnH3kh5Z7iiSuWHZXxdX/MuqHXAjp4zcYMFqH6IWAg3dKKR3jhTQHsz0490QaKUKXAdFVZz5kk2bWhiUTAWhLJdL53+wimaXVdCwLIL3TWUsJmk5yRc9OFJlEKUlvUEBviLdpo1d4G1JztHnM4Y8FRdVo5R0qvuiMa2vMflFuTHaAX/J5Vskfp53Wgm6cyIFWuTxHLC1BUuXEhdgx9OuCadJHtj/mA7MxyFnnGnOTFwJRgl1dFyto2OmvQHTb2sO+WtbQURv83lJLmyyLYdWOofvyxNkWkfRhsKWDgfSy3STTtu9PGux1/gPIAk2+uCPhzky0xvg1cdMqLKGuzQ8Z1E++/3SYBv3UMXhpAaBlTzoCwzTipfSVAkhZNw+l2FZiujNJzYzBWnIPXWZ4ZgFMDCm3NwHaR2Gyh8IyKpsLso8DsOoieJsec6zxd7evnzrTE9UV75lFnN7LbOxhjMdn42Ef14GvSEfc1yQlR23WfmMsCpfeAqBGLz2VrBIYsk94OIoRA57siY/mjSv3Xczns="));
                return skin25088;
            case SPIDER:
                GameProfile spider = new GameProfile(UUID.fromString("f901d2d7-f70f-373b-9686-6ec054a18095"), MessageUtil.color("&6&lSPIDER"));
                spider.getProperties().put("textures", new Property(
                        "textures",
                        "eyJ0aW1lc3RhbXAiOjE1MTIyMTEzNzIyMzIsInByb2ZpbGVJZCI6IjVhZDU1ZjM0NDFiNjRiZDI5YzMyMTg5ODNjNjM1OTM2IiwicHJvZmlsZU5hbWUiOiJNSEZfU3BpZGVyIiwic2lnbmF0dXJlUmVxdWlyZWQiOnRydWUsInRleHR1cmVzIjp7IlNLSU4iOnsidXJsIjoiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS9jZDU0MTU0MWRhYWZmNTA4OTZjZDI1OGJkYmRkNGNmODBjM2JhODE2NzM1NzI2MDc4YmZlMzkzOTI3ZTU3ZjEifX19",
                        "c50aiuTruMyb/XzT2Yt24i8+LriQDNO22xht90CrYfcoI+fq3m7R9tUxxFDgYif9qVibSBK1H1SRACkbiFeqo/uKtrYIQ3XiutM+2QKxsveA/yeRQaO3b2mc3rUNRImza0++MNrrHYS8PU61FGwa41Id65lWVHie2DWqGcEppb7BgDgwOyMyUMiepr95mI+P+4oLeeGWwUlfN+cqLCB8qs4nKP6MsgHgoUkudKMvvKq84FoX028RMxpQ6Y29z7nrRklnVpGUL13ftJ3zAfBE5WUpwvcMCcBRsuA7LIESLw2Fwv3bAp8ft2na0Tv3UcIVf3yLPhA7u/GNaPO8yz5gAVWKpNOScQflMNAHSQJ+39xcNdAfle3YOebcdVKMyJLuxDaCcaN5J13ZSpFwA1DsRphP880TVPynI9h0MT8Hy26KIPZy9sngyes0h+YKGLqPDd/N/i4GX8vziPUbjkN9/R9wxdAP2JIp5SqQa09xtJFScrUSfUXiRAN7r5aTArt0QpJAaaMu4ThOy4qMmikBkgBjwE2ANETGcmWtjPjXIvahskHdVZXpcuhZiGQd3oYFmrY3AWEKQHoUdEtyVrXiaefshyW/U7qCyv7zQF+m7jUQ5ptqngg8E2K2vgJPHXiXX1YSmdtjDHG56daRgu5xxeB+W2OInU8i/uz815ZKDnk="));
                skin = spider;
                break;
            case ARCANIST:
                GameProfile arcanist = new GameProfile(UUID.fromString("0ef9b9d5-769b-35e7-9717-4ac758876ebb"), MessageUtil.color("&6&lARCANIST"));
                arcanist.getProperties().put("textures", new Property("textures","eyJ0aW1lc3RhbXAiOjE0ODIyNTQ5MTcwMTgsInByb2ZpbGVJZCI6IjQzYTgzNzNkNjQyOTQ1MTBhOWFhYjMwZjViM2NlYmIzIiwicHJvZmlsZU5hbWUiOiJTa3VsbENsaWVudFNraW42Iiwic2lnbmF0dXJlUmVxdWlyZWQiOnRydWUsInRleHR1cmVzIjp7IlNLSU4iOnsidXJsIjoiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS84ZjNjZGRmZGVhOWE2MTg1ZTY4OTQ1ZTQ0OTcwNTk3Y2MzYjY0MDIxOTA3MjE5MTY2YWRjOWZkMTc4OGUyNCJ9fX0=","m4gIfFj3NZaySyolywCbikvkSB2x6eGGQ+HyvkM5HAAU16e9L8bawbu8leGiXe/8+5TCg+dtVGOoG2zeCXKxb5N2Y6RHapQ2G5tYfOUZOkfC7UHYRon1RsBnFJ/yHqfDHZxuJ59wRNSqFCdAbgVUMS4aU6t6kPefsaLdxIZdl/REOpPNVhfdjl0B3lsGP7okpZQ4mZtYbdGsBeAvcfwofKfoezi4wNme0jVX6boX1qaKNTzV5yldViJCBf9DqEejzFu31FTvjNTd50s2+ouNH+dU9nKAdkl2yrZrDpZ/gbSr3NtL8Fxd8Wgo5wGKLfZbkiLx903mWDCSUy5xOrj+B6CFkLN2yEgQgYyXtAqD0qxpXr3rhhInsUrXst5oAPLTs1ORQNFwwxWDJ1rDDCMAHB3PN1tlkO7pLJb0jDZjrNbY2yhK3Sll5uUp3Jxt7gOlrtvM57u4Eiwn1D5wUNZ5nX72L/TamhKZNN6SuTZAMPCIjjTcSX5P0yds9OJbOwcsltJA3vtHZmv6mmozCTTojLHxETxEJR4uH2sK2BF+OcZlJcrxgFmasjClqoHLB4J/VeU1KVuhNdHB4Byc1eyN0NaUdpReTQjUtnj57yAquKKUj+jya+x4Hl6RsJuVydhr19Nn17OJo9FRTFw8QtXNoM3OaobXSHn1yIn8KX0imcc="));
                skin = arcanist;
                break;
            case BLAZE:
                GameProfile blaze = new GameProfile(UUID.fromString("3c9a8292-0fa7-4e7c-8413-2bcb15968204"), MessageUtil.color("&6&lBLAZE"));
                blaze.getProperties().put("textures", new Property("textures","eyJ0aW1lc3RhbXAiOjE0ODExMDgzMTk4NjEsInByb2ZpbGVJZCI6IjNjOWE4MjkyMGZhNzRlN2M4NDEzMmJjYjE1OTY4MjA0IiwicHJvZmlsZU5hbWUiOiJSb2NvbG9nbyIsInNpZ25hdHVyZVJlcXVpcmVkIjp0cnVlLCJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMzU2NTg2ODlkMmQ2MWE3YTc1YjRmNWM0MTkyZTg5OWVjYWMxNmEwOGZmNDE2ZDk5NWY1YzEwYzc3YWEifX19","okbpQ6hIIpOKG1mKTnmiNrx14cpeOogmz0jbDJ4RQLzSyfW/yAhvzeIeI1qZa2TuMlfheiSsY+wU+TPdu2v61mycYnxglj4tj+/P5wXWV43QogZTCnvn4BEifMz/IBZKhbxdTJuDaSz1BEiLjAi10M8JFzF6Cl5hTTmeyBFVE502p7xUEbi0+4mYABTLxyZhwR/Uz9ziR3ZvVAzLB6SLLOxlqBPeUa72ic69xh/fW7BA15CQTZ9NGTxHvJ2nImELUjdQ5eCL9q9K4dke46MTgvz9AYD4t66n/nLPPRaFsKKG/WG1LTsBTctMUbiS2hYfGQh0cTK2zz9BhxD6iSj6c/QawWL3dN1ot0MPLRPdTw58DMPnWVec2ixgjnbd4ja2cSj8cyDF0S1YtMwVk2JE7apgx5xDcyByuJRoCtRHcZQ9lbLKK+znNFDM+ymIvcM2v+itMDf/kdz/410ecmPZO0pSwlUvJ10+C7uhfeYyN4v2cFZr3WucLxp/zh/dglneSfWyC3i/OdeXEk7k7JFBQw7BlEYHo/gPbj/qJBU5EPwOsvZcZcRxVgtcBqhr8FwvX7T1d1Pbz9NaMpNg8B4CCg2SJ5RYt040ST/PA0JaF33DvTgx0Bpm43pxRKSm9k58wzbtphGa58BCx2x9WH1hneVOkbbpjrr/eNw575YKTLI="));
                skin = blaze;
                break;
            case TEST:
                GameProfile shadow = new GameProfile(UUID.fromString("f6111460-1845-3785-ab15-fc0ccc403f84"), MessageUtil.color("&e&lSHADOW"));
                shadow.getProperties().put("textures", new Property(
                        "textures",
                        "eyJ0aW1lc3RhbXAiOjE1MDExMTI2ODYxMjYsInByb2ZpbGVJZCI6IjIzZjFhNTlmNDY5YjQzZGRiZGI1MzdiZmVjMTA0NzFmIiwicHJvZmlsZU5hbWUiOiIyODA3Iiwic2lnbmF0dXJlUmVxdWlyZWQiOnRydWUsInRleHR1cmVzIjp7IlNLSU4iOnsidXJsIjoiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS8zOTMyODhjMWUzYmUyNjM4ZTVmYTc3ZjY3OWJjYzQ2N2NhOTg0OGViNTYxNGNhNGI5YmNlOWVkYTkwYTgyMiJ9fX0=",
                        "FjbKqC3xZf1yjlFzgDt7aXcAXm8+0Bx1IRYgHxHfYmGO9aI+bgQZUHN8ibPCHlCfM8eVHAxIPV8+yhbs4hp6N+UCioYN2UwUyLj55w2IOxvF1r1BPAN1jbBEulE6bQo1yitLTLKdKbOlWGC+uZ6zaBoiCjF6gQhnjPNfYmapeVHea+vMBvCN+s02IK75c4A/Xjn10unm31rrnyCbZfNtjM5M0LX5+7h5jBxIGnDlxHkNvWuoWLTESifsKQgzCH1o7MicA1W5So/+D+GejqkEvk+jBbYoaL5iJB5hI5sQxFeFrPL/lz2Cc+cCasJUIR67po55aqGxYIKKQ97lwAFLGCWFjomtAURu05UNysOd5hvKwdcEvNtYeN5ITX4zFLK3MSdO4DLaw6fIWnbaQOlNOtCFn0ZHNpZJvO/bwSTDh3qoQlQ7fGgXvsvNSM64otvPz6V2qus/DGkMLGd5by3rkfk7O7iOWjvXtO8e7F+N4JFZiYym5HCYJkCm+tBY+5FAWC9M0GYLMmX8F1ggfEq+r5+lOEYN4zFoY0gP7kIz9CtbN6UUGLxpSbCELC5bX4/WTmieSQh0rPHgTIUv44eMKsHmS3QbTMMBbPhUZ7B4dCHkcpMGa0BRr7nKSxlAAvW38U4Migd6f6sMLKe5+nQWqHosr48mcR7jxlfJ/NxVfh8="));
                skin = shadow;
                break;
            case RUMBLER:
                GameProfile boulder = new GameProfile(UUID.fromString("5effb536-8536-371f-9ebc-575ce6b51353"), MessageUtil.color("&6&lBOULDER"));
                boulder.getProperties().put("textures", new Property(
                        "textures",
                        "eyJ0aW1lc3RhbXAiOjE1MDExMTI1MzkyNjAsInByb2ZpbGVJZCI6ImJkM2M2NDhiZDZhMDRmMDM5NDkzMjc1MzVjYjgzMWViIiwicHJvZmlsZU5hbWUiOiJFbW1hYVBsYXl6Iiwic2lnbmF0dXJlUmVxdWlyZWQiOnRydWUsInRleHR1cmVzIjp7IlNLSU4iOnsidXJsIjoiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS8yMWNkZWM4MTZmNzNiNmFlOWJlNDFiYTIzZmM2YWY4ZjMzOTYyNGYxNzNkNTYzMTk0NGE3M2EwOWJmMTU1ODAifX19",
                        "TIQel27ztvsABptn/lR9ueua3aTVBho3cOHj/8yyVuKejy1RcXJ4BlRfUTUW7UeloQXksXfM19l8uZ9t6K4Js9ARhy+3DVnnokoo+Ml/+QSPuiv4jlYV6SaDHfOpyqCknV5YkNYZogMqiAORLgkJtMvoNSFtrxe1121mAArrKUifa1CZudkQILqSi5A1mpXsEynqNDImq5/ixDukgT6sIBJfVvkVGQBBsBi8qBwzXxfYbn+p0RCVi3Z9Gni8rNUkWi3ikObBPFVJS0mNWRAo1xx2R/hnw1BTr+fK/07GOLuK/YgKMuyCA65iC8wyz8TDiTqiFTBeNIhxaK219KWP6ZrgllK535vZRTdNM0BRQl2SomeynzBvP8iP2/qEz4J12ZBOqkWgfgCb88vlzUoBujnIZUVu8yd421gT+XC7NkE+4ZqlreNWNOdZE1S/scZDHR+oOSpOsy20ZfmsO5UT8tN7Vdo0iuIqOejo67eCf92VGl+7lwtFCPBFdhiGJcH9x4txGJAW85jJxKrgqwg3XwYfKaI2p+rACeYCaex71Sg5/PJVUtIAO3zvC2dR1Kwx6gYZtCNMKa3p14649my//9r3izEmmTfQvSs9SvV+TcURjWvzKKxUt4EQoPIUD2OsvmRpJPgTFd6EkTddwYRD7+I5iDWS8rIiVdrgtCRpPJc="));
                skin = boulder;
                break;
            case CREEPER:
                GameProfile creeper = new GameProfile(UUID.fromString("6748b252-557a-3aba-9fa3-69ddc7db176c"), MessageUtil.color("&6&lCREEPER"));
                creeper.getProperties().put("textures", new Property("textures","eyJ0aW1lc3RhbXAiOjE0ODIzNjYxODk3MzgsInByb2ZpbGVJZCI6ImIwZDRiMjhiYzFkNzQ4ODlhZjBlODY2MWNlZTk2YWFiIiwicHJvZmlsZU5hbWUiOiJJbnZlbnRpdmVHYW1lcyIsInNpZ25hdHVyZVJlcXVpcmVkIjp0cnVlLCJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZDgxZTNkNTY1OGZhYmFjMTQ4NTIwZjlhZmZhNjZiZTdkMzU1ZTYxNzI5ZWMwMWRlZjg2YzM0MWVjODU1MTcwIn19fQ==","HlrYtZeILRl4Bp7VXcuKKT03vDlZ9Ei3BerxFmFiszU3e+OM/uJMWMpJU9PK+uCnbIbVVcjx1qm+Ac3m5P9NHAkmrP39CP/MLlCfEZYPR+jO9dRmPCsWcOOOkEfA5xUUpjhWiDlR6KwTg6HplzERCVpGhNsmcVv7d5k858L9WdR0BLfjnsaAjhIzt0Pk4N9TP5SFzTDvxGnDRhzCMVjtJMMwPVlI+gfyxDdwOZD6AQnDfQM3o2jottKCH8y+AFj6gDJcwEI2t9iP3H8Ndm7twHNGTkMz8Vt4GF1QfuVpKOaT6bbhhB0aTXPOXE6j6e8QI8M/yuf5KW3wRTRfGqCC2RjELYYSsEGxqSPZekobqSmY9Nh76yZwOyMC/XdAR7QCuQkKwEj+fOOWeSeuOTgHzk1PtwosVQjFmgTt3JZacBAgIu37LfdMsoYx0Q76mgoOg5WJObZJNvHL2ZhCjYGdY39b4BgEWPVKM61+H045tvhgM+d6S/GEgbZAigSD6Eo5mBueqoiWVK5eKdD8TL6AJGs18bouY+5wpPsZA/ppmTj7sXlxCXBynwJbJdotz6FJiBTbFXd9Qp8EBKGyyKiJI1O3cYo3b4Mo/ddnv6dQwQgJ0JnOrV8sKONG6SfaYVvrmVOU3oSUVkI29HZ5xb709uvG5vFdXKPTip+xQvHRifU="));
                skin = creeper;
                break;
            case CRYOMANCER:
                GameProfile cryo = new GameProfile(UUID.fromString("4f0029e0-ee99-3fe4-83cc-9cb7788b6517"), MessageUtil.color("&6&lCRYOMANCER"));
                cryo.getProperties().put("textures", new Property(
                        "textures",
                        "eyJ0aW1lc3RhbXAiOjE1MDExMTI0MTA3NjUsInByb2ZpbGVJZCI6IjVjN2ZiNzhhMmQxMzQ5NTZhNWE1M2EyNGQ5NWY1YjRmIiwicHJvZmlsZU5hbWUiOiJQZWFyc29uSW5tYW4iLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlL2Y0MmQ4MjQ5MDM1YmUxMWQ1MGVmOGMyYTk0N2QxZmZlM2ViOGQ5MTk4OWRlNWIyNzc0ODE5MGViMjMxMjk1In19fQ==",
                        "CKUP8yPhyPs2J81Gigs8uL6HJFLEW4ZXWB0Bsdv9pO42RPHN6Q1MOs2BDto8lZsJuZWvW0siwrROa88aLtAViL0JUSzZmMv0/S9nEzswTc3aRM+agdgzwv6CUURkfoX4rEoC5LuvTSEHmBLA9yXR0KEizbqy7jMEp6A1uv7z3yCZJSeGM7w2Zw+UxTpm7AleIaZGwrAbMSwqWiJsRiSWzq7PlPw4PYk/6YceKVMA6uvcK/EUGj3t7lu45QWh8QUnHlNgcsrw7jhkO7JUesc3D1mGA/VXhy+Ph5suHyz6fjdhRfOIHBWHS4otRk4OaD7MnLMYYKg90+T31M0bHQbTaVpzaO+RiPNUHiGVoPlzxgCuX8eLnOylF1I8vj3yC7RHkAISjFCDbEKVzoaKeCBOca3hhSM/m0E5Zt0sEVLwCx0z2iGAkqgBDL3zMMD6iAqtMromJMDTAaVP7rkNnEJv4KI/QFiTfCczfIMqq3AM9TIF0eiK3S6cW9ctJdW8/kUXe4wGlYvifCcbHezpxiE/exMBBYykSmF/Wk/Zpp6Utf4u6MQIXA3Juyjjl8zAn9MIeirp7niXBAys+ZyOZ/8szc85zDroZpEvaHC2p2QY4e2W88vtpEprvd1CXk2mbyrk8WsravPPjHtPM/fIIpTcJX0Xb230wMKtP2IVQjgSd5w="));
                skin = cryo;
                break;
            case INFECTED:
                GameProfile quartz = new GameProfile(UUID.fromString("762e57da-b5a5-3d19-a5ac-802a05c01a7d"), MessageUtil.color("&e&lINFECTED"));
                quartz.getProperties().put("textures", new Property(
                        "textures",
                        "eyJ0aW1lc3RhbXAiOjE1MDExMTA3NTY5MDMsInByb2ZpbGVJZCI6IjkxOGEwMjk1NTlkZDRjZTZiMTZmN2E1ZDUzZWZiNDEyIiwicHJvZmlsZU5hbWUiOiJCZWV2ZWxvcGVyIiwic2lnbmF0dXJlUmVxdWlyZWQiOnRydWUsInRleHR1cmVzIjp7IlNLSU4iOnsidXJsIjoiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS9kZmNhYjE0M2ExMmUyNTNkNDUyNzM0ZmIyMWVmMjIyNTlkMWQwYTEzN2Q0NjBiNjc2Njg0OTFlOWQyNCJ9fX0=",
                        "S2emdQxM6P8ImRy4dRrYVhT5ClkoHR5+b9nB9MgRdpBkjxvC9eaOAUVMgObyrp3JI4HEN0Wb8effLV9OatedR9NmkLo3/VWpnqaKtEsTT8JdJ2bvHhvJ83KVXB11PhZY6jXHmU0yUtF/JRJfcin6l5luIbpXN4+Ex8mu8oO0HHE16GbGxW4Jn1Je6a8tUjTse0ogwBNnG2ElBE/jALiJQopAH/ixqnEOzYPNEOV8lEQ1dQj9RniMk6aDSdx2MhB787FbaApnScC8lulc5huYIw4IFncQe3r4N6R6HnIASFdyxQMnWqqsCJZgZ1fDEnWcsVGodXZ1BKpZf4s1JiS6AgYxBjEKon9Hb2V8Ctqf5XGPDDD1dMMBvpYb03wsxFkJM4dPWR7GVZeq7YZoW0z02jkuUC2Xmz7tl13u2BcVnagwGKqGBg6tFmhpsCJ3B+kF/W2I3n5OgNF38xGm+1BW3RTiqfjZziWTk9vWvK5PYzoV1UdCVx2+thWel5Fn/kS+WpcQDgEIvgEAoieaZRYOQj7AJrZEzJWVNbYFeg/PSN+Yy5g45cOHVh+sM79aqa5/2U1/vgTh0mU+KWHtapE4PVPny6c5cgFleNxcnWQrDO83ed0+Jm+rl2j6vCVjECZHi1S2ayZh5oW6o4UD5Bd6jkGwY4tAi2haoPYg3UFYNss="));
                skin = quartz;
                break;
            case DREADLORD:
                GameProfile dreadlord = new GameProfile(UUID.fromString("d4005954-9693-3e12-a5ce-95818ca17590"), MessageUtil.color("&6&lDREADLORD"));
                dreadlord.getProperties().put("textures", new Property("textures","eyJ0aW1lc3RhbXAiOjE0ODIzNjY0NDg1NzYsInByb2ZpbGVJZCI6IjQzYTgzNzNkNjQyOTQ1MTBhOWFhYjMwZjViM2NlYmIzIiwicHJvZmlsZU5hbWUiOiJTa3VsbENsaWVudFNraW42Iiwic2lnbmF0dXJlUmVxdWlyZWQiOnRydWUsInRleHR1cmVzIjp7IlNLSU4iOnsidXJsIjoiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS9iMjJhYTQ2NWI3NWExYmNiNWU5ZGQwOTU0MzRkODg5ZDNmZTY5ZWM4N2FkM2ExMzFlZTlhN2YyZGMwZjFmNjMifX19","mX/ygAugXZr1yjAUhjlMW4ZSOh3x5V629wRtplC2DiMEe2NflEZZrcmS+Cn1iBvUaKy/Dt4yb+abMoMKTpSp986eLgVlwG+zXWT4M+iWbKkkz94w8WIpv/YkmvG4w/i1kCHZfMtIsXzWcMAtfmd9oC9bnEmOVBSi/WDK1c2UOeTzwl1oeimpb8dgcDh64eLXYLg831rHvBhtH/XdVGF/JwP2OpkQplB1pMIVuITl2DnQqRWJp9EsL208CHYy/iNynz+VOZltVl2nNqxFchkar7oOlKDwoE4GjNc9MZZ1aGZIMwj1PGYMKnsZ5KnMeSgyFZ5QRkfpVb+9Q6DFwFmYByx25ATIrSgnRBkz2Wu+fCCb1VWG4DBltuTlwkDk1rRe+15h86A1d1tHN/tx6tL58vQDnUwSMFmV39qw09Lu9Ye/OIavj2KFeOOTkWvWgk0aF4x20VI2SVEwfOWlGfIMOcnllb/yBtHCxfQjODsTd3T+YOhtNqFs5jQQNCDm/OKeEGm2PRQtTIaExVXjtU0BED7dW6yY1KYFnkjT4+5eSsCvkc8pqLPPsM3Bz20kYYaJ/uItq0nN7IeEQDJ5Wi04p6JA7PP0iibzbg5uSAc1vZDced8psZI0vX+QlKmNi2AMmKCCA6KKs+n/6vnkmIqWKq9h3UFZAq0LrF5/V/7PGko="));
                skin = dreadlord;
                break;
            case ENDERMAN:
                GameProfile enderman = new GameProfile(UUID.fromString("05fb8abe-be4c-3454-af47-ad0f8bf4f4cf"), MessageUtil.color("&6&lENDERMAN"));
                enderman.getProperties().put("textures", new Property("textures","eyJ0aW1lc3RhbXAiOjE0ODIzNjY4MjA3NTIsInByb2ZpbGVJZCI6ImRhNzQ2NWVkMjljYjRkZTA5MzRkOTIwMTc0NDkxMzU1IiwicHJvZmlsZU5hbWUiOiJJc2F5bGEiLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzI1M2YwNjM4YzkzODE2MjZhZDE2NjVmY2RjYTU3MWE4OTMyYTIwY2U0MzZlOGUzNTcwZmJiYTE4M2ZjOGU0In19fQ==","kbx6ySV2M/8yuVT/zzGBVlZdFqv2haed6F41Tl1iHrZQY0fyMCMhfaoXrYLodR5K0aVwAy4dmbB2oaDwqNASvUn+WkeXc5Znn8DW6azPcuoyMIsTHzDh3VeYty6EmtpaNCal6zJ85OfrQhFTIcLJPmFWYNhkdl5NmneLgq+iacf9ruZeXSuKReRh4CmyCKI2c2yGwVCfgmGIeLuDRxTvskV8QbexWMKg58Hj24X0FFRlIhTbe67hgyznH2OS9c3dp+ROCPkDXQAVknOD4lqZqvCz0WJbD7CoqKfY9YEQpCkXOD4SL1W+DIDjUqTA9kULe1Z3vYbPuWN95o6e0tzdvbyNQvnyjTxX53/igEQzjh1o/NhApselOvxN3J2B217lPLjZsvxmuNq1tKnYD7hhzVaXyMydSDbPX5lCxz4aUwkKF8dLjIrzWoSx/FsocGryaMysjUI5YjVQQ5kTohxY4BuDMF/KLFsI18poRAflIrYWRgNaSdZy9aHI6o3A5Z4qNRlV8SwLG3BkS8vM/loEy2mZ1B3S9ZeEuQTt9qqBF6JMDiY5YVRWobM2OwkJb4nEAaLffbRvFM84R9a8BJfndLqXoFvn5W1n6mQpRXLF5ZvxyChxcHyNgSIQkrc+j4sNnrtLhelAdQobqJe+/mHlDASWUhOzszrx+hREIvTVZPs="));
                skin = enderman;
                break;
            case GOLEM:
                GameProfile golem = new GameProfile(UUID.fromString("909b87ec-c364-32e6-bbd2-3a5db09f4b57"), MessageUtil.color("&6&lGOLEM"));
                golem.getProperties().put("textures", new Property("textures","eyJ0aW1lc3RhbXAiOjE0ODIzNjY4ODY4OTAsInByb2ZpbGVJZCI6IjNlMjZiMDk3MWFjZDRjNmQ5MzVjNmFkYjE1YjYyMDNhIiwicHJvZmlsZU5hbWUiOiJOYWhlbGUiLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzJmYjU0MDQxZDIzMDU5ZmFlZjVlY2M4Y2FlM2M4YjQzMDhjNTc5YmY0YmNiZjM1YmE1ZTI4YWM5NzIyNjc4MCJ9fX0=","GrMGmEDiwQIb8le9hStfVewoD70pNqgjLc1GgQYioPymQ1mymVLS6/KRnr5VqIvQhSsgqvd+izTs89IoKfIr2Aj1INvoDm415AocEGe9lCxEUrGu9kWT7Y5660iIf6q1ECAD71ZTJm8qeLSSUTo8soLpTn9sCILktuKa439PeoVNY6YznUi0QQ/zlm8Cw5N6P9mdNq5gwiBJwVPMu/nlgG0n9MmCCwdsLt3dw5FmWFQcwdwYZfJw+VIHR31mO6emm5y3MOHO0/QjYfLyg4/69IVh8ldOitydng0TEhRUoTIeChkI/DDvLVu3ukycmdi7idysZleeaPR23GXTrMyAC2UA/nROKNe8AljmLKhnkxTBbKbd/tBwiXddL+M/GDckCp2RlPBEvGgcTmK95Y7QexA3yyErrgR2+NO6kAMo7Ohq3zwt/m3m7sAGYUZIKNhHoUDyL9lxut61Fj7GL2Ybkv9rzisUHp8ooYIclnxAZaCi1ohb4qYjBwGW5iIqwDkptufMbaoizxKXWP7JhBJe5SZRdEpDwaRvNxRDKrtSTVE85+OYFBFRZXgHtlJZstrN3X/jLWiU77H97DIVCZvGaAXhkuDI8XMQLIs07gSAJDJOnxiSi3wZ9V6uJR43J1iIbFcS5eqNc/tNg5mQoOcujUs8lyys47+JoUvu5dTGmVQ="));
                skin = golem;
                break;
            case EPSILON:
                GameProfile epsilon = new GameProfile(UUID.fromString("225f52cd-926a-393e-a80b-928e80964203"), MessageUtil.color("&6&lEPSILON"));
                epsilon.getProperties().put("textures", new Property(
                        "textures",
                        "eyJ0aW1lc3RhbXAiOjE1MDExMDU4OTI3NzIsInByb2ZpbGVJZCI6IjNlMjZiMDk3MWFjZDRjNmQ5MzVjNmFkYjE1YjYyMDNhIiwicHJvZmlsZU5hbWUiOiJOYWhlbGUiLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlL2QxZTg4MjI3ZTc4ZmE1NmFkZTgxZGQzZDg4YmFjMjI3NDQ0ZDIzZWFiNmQ4NDUxNDQ5NGViMjllMjk1N2I1ODUifX19",
                        "QLU4DlRdd74hFJimAHPoDbSf85fqO4LjhrOqJSbN0B1ijZqvhpKG/kU3dVXCBcSbeU1P0ZWt2q705RxZH9v+YndYpiugYgMiQOf2b3fwVyzoXDk0uXMkc/rVE4Mjx4EfDo9X53ubu6VuUUxu9jeeKfSOImrJj+RXAJf9T9MurbUMUnf6E6TYCBk95ZCIjKncfEAqT7RLydhG1Aoml/3ayRphd58D1aqccmpI1Ahs8QIzDSFiIZUogS4fL2PXZF56/+4CIG+g9t62OdjuYPYuveJmSvkqyK8WI44BWguH5aQdIXw/N5ftZTUr0FzABPg2ANPhjQbIhHztjmB7DuMGSeUw8YpEv1cSGvcy1NRzXgPFTmH6kb6sJefHaNIhJJLbR14h/BBJ9ynTMWt/0I3UGv629GhJ0fGEptqRSCTuqsm6EmS+VgslA5FX6lDDhsY45H+IUiLRq9XYWJdO5anDz5chv/CQRetGWDVwWDw/6adE6ms9sjnHBpwJ9tYZI7j4yMcqL+zXj9wg7i63ny2IjWp8zbUpMpp9ubVfwbGHYu8JL2iXK6Wyv22kzfejUXmlRKlUG/NzpqdKJJZmUGqzMu3A+iv1otWx7nGwSjOoyFOxfhpWwpwzXKgC+3dhPdqQQ1I3Ymukgbf7Ft+bSY+s9KTq83HDlCiKQQGtwbZUUUE="));

                skin = epsilon;
                break;
            case FALCON:
                GameProfile falcon = new GameProfile(UUID.fromString("7345c060-f754-36b7-bf86-5c884f56031a"), MessageUtil.color("&e&lFALCON"));
                falcon.getProperties().put("textures", new Property(
                        "textures",
                        "eyJ0aW1lc3RhbXAiOjE1MDExMTE5NDE5MDgsInByb2ZpbGVJZCI6IjBiZTU2MmUxNzIyODQ3YmQ5MDY3MWYxNzNjNjA5NmNhIiwicHJvZmlsZU5hbWUiOiJ4Y29vbHgzIiwic2lnbmF0dXJlUmVxdWlyZWQiOnRydWUsInRleHR1cmVzIjp7IlNLSU4iOnsidXJsIjoiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS85NDg5MWI3MDExZTA4MTRlYWZkZTA2YTUyZjRlNGE2ZDE3ZmRkYmZjYjMxNjI4OThlOTkxODFiNmNmMjhjIn19fQ==",
                        "ChjzKkG9UxltNgAo8JMo2WMYCSqoqzYTsZ9Pjsl49VdqpxKPykJ1IcWtVQtNfvzmSwvWjZ8L6dxn6cOjkY+v2PPfKXF8+Rmt1C2Pxl1HLxPINSZ+YPVEkPLSth3BOGX9beSFR6vHyOjb7l/MRxlQUzFXlsCZQiJy6c9xRHoZ0L+8mD2Qfg9+FUxkaZojYdpiojQ/29pJdzHukJI1DeWhdjYLGORBcmhl0OTcVf8BUgzqwnunBt908ZOGBLsQpy1DlTndK7M/jkVFwifSK270iN4nVt8ikEI+SHuv9GjzPsuZJ0hI2SdWj5+lNw6JLDaUhNIbobrlG3ZpV7T5pEl0CcMm3fGDgm+9KE6g9bRM2bbo0vK5YnBGhAQ3Uo23eYYeGPUPsCObkPkuiTYH5FYNLE096XbMNSF1x0d04HgyDm99a+/8wh+E5jMVkF0kFdCaTAZInpRoy/0O5r/vcGLErCyRUHzVz2f/xTcWFM8VNSs8racZ4Cri0cbNKluF2F5bO3gOgTuLQAw+ygCKWckek/KyTlp/M7D5VE7IyG2mb3bHFCqUphjXF+GUQoLEfD2Z+2z9KNmJMgO0sbtdK9IwPcQEyXF298K9rIcHR6jLETkXbxT5ANpotlWal7hQy+Brag/4lAC3DzTxFrtiX4gCMVvDNdciB8utyIlV/PGAgaQ="));
                skin = falcon;
                break;
            case ELECTRO:
                GameProfile ent = new GameProfile(UUID.fromString("4f2fc07d-6fe1-35cd-a5ec-9a9364bd4ce1"), MessageUtil.color("&e&lENTITY 303"));
                ent.getProperties().put("textures", new Property("textures","eyJ0aW1lc3RhbXAiOjE0OTE5NzgyNTE1MzYsInByb2ZpbGVJZCI6ImRhNzQ2NWVkMjljYjRkZTA5MzRkOTIwMTc0NDkxMzU1IiwicHJvZmlsZU5hbWUiOiJJc2F5bGEiLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzhmNDI5MzNhMjI0MzFhOGUxNzFjYTliYTNlODBiOWEyNThmNjViOTdiNzczNmZhZWE4OTM4NjA1NjRiOTkifX19"));
                skin = ent;
                break;
            case HEROBRINE:
                GameProfile herobrine = new GameProfile(UUID.fromString("c932fa01-7e8d-3b06-a797-b30bc067cd29"), MessageUtil.color("&e&lHEROBRINE"));
                herobrine.getProperties().put("textures", new Property("textures","eyJ0aW1lc3RhbXAiOjE0ODIzNjc1OTc0MjAsInByb2ZpbGVJZCI6IjQzYTgzNzNkNjQyOTQ1MTBhOWFhYjMwZjViM2NlYmIzIiwicHJvZmlsZU5hbWUiOiJTa3VsbENsaWVudFNraW42Iiwic2lnbmF0dXJlUmVxdWlyZWQiOnRydWUsInRleHR1cmVzIjp7IlNLSU4iOnsidXJsIjoiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS85YWIxN2U1ODg3NDg5ZTJkMzQ4NjRjMzQ5YjhmZDNhOGZjZWFhNDY3ZWUwZmNhMzUzNjZiNDIwODQ0MGUxMzgifX19","I3jmOu1uXJrsqJlPhwP0FW2AnB4JMh0DG9mKU+C+u+yaTK67TEVcy/haE4nVVaUGFwz9x1js7N68WsohcxpD5DxQdElmpbpJG96XQ1c14NIL3qmHKZ59N+O4rcVDCjzyWUvHiA7J5EIXPk5N6mHZJZ3vCOaY+RvxEvEoNnGOp047rEpSOjs8sKg4DKiFkMT05LQxVrVMN6QhHpD4Yr7BIVWoTW8MlRvfbGOsxFXjUQclk/df17XSyYWZD/LEfuuXvtmx3zz35MTIWcaX6vP3Q2qy+T/Khyb05NFQFKch7PMjizOC+yip2oSiiVv90b7FTH6s6AKowoDx/4t8WKASwfKRHI56/VGgv4youY1Sm6hEVHKjdMWzYxALcG7aU8LYVjVqYCgFCdspOJTtZQBRV3sEwwkHsesWegIhMm3ykI+c3bQHy3qftsKETF3nyYiuuY3MZZvbzfEpX9NkCj2jbgZ7prZQHecHNz8wNPWCUtogkj2SyCwsDiao5ddYlojTkgj9Lke/HNy6qqiOyl76isqHhHB+LQFWmRG+5+9VAVuiP2Sd1R/oE+t4shY7aiBS+SClHrD/F0wQusuOchCetGouIIrAuHEkPllR1aguko0XezRsleamZo3Zx0Zc0KEH5VOOwRY0gyhQxMu810LlUimn45trfTD0dBipf7r2Uoc="));
                skin = herobrine;
                break;
            case HUNTER:
                GameProfile hunter = new GameProfile(UUID.fromString("c8f56187-e198-35b0-8197-aca9ca02f22a"), MessageUtil.color("&e&lHUNTER"));
                hunter.getProperties().put("textures", new Property("textures","eyJ0aW1lc3RhbXAiOjE0ODIzNjY5OTczNDMsInByb2ZpbGVJZCI6ImIwZDRiMjhiYzFkNzQ4ODlhZjBlODY2MWNlZTk2YWFiIiwicHJvZmlsZU5hbWUiOiJJbnZlbnRpdmVHYW1lcyIsInNpZ25hdHVyZVJlcXVpcmVkIjp0cnVlLCJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZWVmZThjNDZiMmY4NmM3YzQzODY4NDY5MDhjYmI2ZWVmNzcyYmU4MWIzNDM1N2IyYWJiNTk4ZjRlNzAzYzQwIn19fQ==","di5uLbg+rRwe30/CzzoWDceed2Gr5W2unP7MagH0ZYC0LlbgmVZAsoXBxOF/ne2kD74y3Wyg4EFR2Jlx7OkWnezLqc32H5D1W1JUUaUEIn7zD/xE9Vm7rrTpG/hM2+YDpKua5lMTB8/zHDOH1ZlhNyPdtBbWsow1yi3/LfWcM5Gb+FNWh36Ouoranh8NM3XNpgkXyIbQrCLamqdgiPNT/bfhA8YfOZJU8DSqFA+Ye1nZooIKI93d6K8lRyIcuy9NxuS5W2xyWkoxwqmWxJXNAlTtsBRsy8gB6tzdUNpfsWbjhOQm0dvElXFbZgxZy3w7mvD4KpUKAq4sBfUJ//1vph1iXPUmlI+glfVnFnii7/C5AyIoopylwd/Lx1RrTq7E8sI7YavmWg1fHwRtIR6HtV3YWmMZuQzACdVGChplAyzqLITebtdZMjeWoS50K9awB9lIPw+7feA0oCcu2ODjeyPFmjLTP6Ex1HPDkLx/UNlf4Urqx/P5hF4b6OZRIglPpWZPrlj28WvyQuJkBiCxQ7eFT3DmeSgJ+HDyc8OZQF20A7CHQdWkRJGayOOQ/WGPNvSGuICxk0zxgoQaH+mgfg1dOtfQk89Z9sVg02HowTrK04EnwVG7pIGqG2ywAMDyc9Y6DmRpHHLW63gsMGc2FP1FIlOqUb2YiRitJK6g95w="));
                skin = hunter;
                break;
            case CRUSADER:
                GameProfile star = new GameProfile(UUID.fromString("0ddb35f2-9709-37d1-af26-dc4078b6dc5c"), MessageUtil.color("&e&lCRUSADER"));
                star.getProperties().put("textures", new Property(
                        "textures",
                        "eyJ0aW1lc3RhbXAiOjE1MDExMTMxOTYxNjIsInByb2ZpbGVJZCI6ImFkMWM2Yjk1YTA5ODRmNTE4MWJhOTgyMzY0OTllM2JkIiwicHJvZmlsZU5hbWUiOiJGdXJrYW5iejAwIiwic2lnbmF0dXJlUmVxdWlyZWQiOnRydWUsInRleHR1cmVzIjp7IlNLSU4iOnsidXJsIjoiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS82MjFlOWE0ZGJlYmNjZGIwODRiZWQxODMzNmM0ZGIzMWNhZGNhODgzYTJiOTYxYmRlYjY0NDExZmZkZGRlNjcifX19",
                        "tZDXOvuhT7pQx0ve+bvbijBqvarDD2IRaVcemM0L7HEI5kRCunjFskWcM27dEeW8nrTF4tNuoyp2Foo1ZwZY9cZWDuwfFJNr1dzlYfCy+//7wut3YFQWAV8IeGeF/RytXHvSkN75Z3VgdK37nR89f94cikdFX00ZOjhyNBBkHKek/VR8LVnxXoMRIJaPXXV+N1UPXLjPUYMn1m9nnOTXua3yXJ6lzD7Lp/J2X4M8jq6n2dgv95uQHG6Lt/2hDe2XY7njlgCM4WS1zQS8LKZa1o5TTa+9LwnL2r19Zi5iqNJIKGGvd3br5yRoQghcgzfV1dr2Od62XEv2x149AhnDvxlUCkBMdq86ONHNEGXm5oP1nPzbwLEnsVmojWTl7JtxC2cSImQdyuz6K6Crfjq5E0o0jTh54q8KV/bX7txvF3mpZ7daxeE20x5bEoRUYyhVsqNKaEmzykpT/xSfJKYRmjr6qi7QN83MEK6DuEk/qj+ZWUU2e2zecQ3cfR+Qx+JHYzXHIHlGFnQwMDjvskAJ1Q4Rc3OOR6TV6pkUJ9omISI/nond0NRRjXrmm6knMmRMGiqEGvjp4R+jY8xOMeqkQjzcDH22sCpSW8DCFJsOkuFs0a9VTj77xfi5WE/JYO9Beawr9Alyfgio0BM5N3LZd9rH0RmE29kN01D498e0ZA4="));
                skin = star;
                break;
            case MAGNATITION:
                GameProfile magnetic = new GameProfile(UUID.fromString("69455443-9e31-3b72-9fa4-cf5db815b4cd"), MessageUtil.color("&e&lMAGNETIC"));
                magnetic.getProperties().put("textures", new Property(
                        "textures",
                        "eyJ0aW1lc3RhbXAiOjE1MDEwOTMzNzk2MjYsInByb2ZpbGVJZCI6ImJmMzU3YmY0MDcyMTRiN2RiYTA0MjU3MWVjMzJjYTZmIiwicHJvZmlsZU5hbWUiOiJDb2xieUJlZWVlIiwic2lnbmF0dXJlUmVxdWlyZWQiOnRydWUsInRleHR1cmVzIjp7IlNLSU4iOnsidXJsIjoiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS81M2VmMWZkMmNjOGQ3YTk5ZGI1ZTA2ZjU5ZjhiMjNmNDgwYmZjZjEwZmNjMDVmMjNmMDljZDI2YzE1ZGY4NiJ9fX0=",
                        "e9y0LxBNkIwUqpKkQTNL9DlCNR2eFud3yoE4kh3A981gDE7+ZWA45yfpAlz9nOcWj98c0k1tx2Jr2BDHRWdeAGyx9M0VCnA22wjWRMCbkyu/LaTIIXrxzF1DVMNz141lt52poQP/Km6LOKt11fqedPJE0yXO3nFyiVYTvppRjxRriRL3W8euI2qp8DgOpRHiuVBFig38c+gKB1gGCIUCUrzejp/eIoPdDuDcDQq3QZdadVdxb6pxKOFWq3n+JAHaftiu7p6KlOeQzNGaBLrjaXlG9vY0H7gPiLbAknIU0lGM1VIfNpMTy7FOnpsTbeSqyuAsntXr77Qwi/o6fMOJLHM060/oMQGVhzenfNJKx8IP8PweQSxzlpPpL/XwA1+ABg2NGkEFJ8oiaB1qKaHUSPJP0egG0BC0o74zzE02dLjosIk3RAFOrOZr8Fcfp0AX+DRRsu2AYrPC7nP5bqw8PPrHbNsOEwccOtsKg12R8uZyn8ZNHzpHdzbeupZjuSbzccKFx59gWqDEPhAQ0KNAIjL8jzDc6TRVnjwlWidrmKckYMJ4zZwsVHN5yOVsi5kTpUCh64XnGz7zU8YpRc1kzvm4ukLVgOVNHKvOojgUgQWk5oJISYyNMDbj0r3ZJbuK9Z5WArZ/x8+JbfBxV/dN3aFF8a85fuQxs54v1yX8VhE="));
                skin = magnetic;
                break;
            case WIZARD:
                GameProfile skin6001 = new GameProfile(UUID.fromString("fd363183-6ef5-418f-b8bf-257adb11cce5"), "skin6001");
                skin6001.getProperties().put("textures", new Property(
                        "textures",
                        "eyJ0aW1lc3RhbXAiOjE0ODQwNTk1NjI0OTcsInByb2ZpbGVJZCI6ImZkMzYzMTgzNmVmNTQxOGZiOGJmMjU3YWRiMTFjY2U1IiwicHJvZmlsZU5hbWUiOiJXaXphcmQiLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlL2NkMTdiMTliNWVjNGJlNTE2NTExYTI1MzkzNmQ2ZjEyYjkzNzcwODBiZjU4MmE4MGI0YzkzYTczMTdlY2MyM2EifX19",
                        "TKWmGiu2gKTbD0GRuzT3m8MOQOhdwvagIPOZZDDxTK5z6qW8W7vHyele7hCoXEuD01brrNoQO1K8ISjzaoHdmbGCch0id+FglxrXmpUeQN0TjRGRS+7qgQcuQm4z9mx2FyH7iHV5UO/oO8KoowWTsnlusE5Dy3Bp0TjEbcEwpuIEIoEDMukvrVHEB0uTqBSllt8M1ZlPOhhbNwzO7C481p6S9Uyp7sOY5v05ufCf09TGd/B6SLt0txs9WsnPQs0xQA5hf/RTu6mSb61ULJ8NRVitpVVDOzH3CVHqThOdqqtj+xDE/Eybewm0O2fuC+dQKYAU/O3hryafhOSZKobrF+B8XypamHotAqeoi/av9N/0YphubBy0QgMqUSvVK+3WNl++HQKIz+yyNrRj8LCUoViyCVXiXttTrfsXZzR02SLH28HCaPc9RWE/uk0IghkwSy0u1OAIbzHZXwZc6BRiQIInRVW0OaqoyhImhpBiAqpLZBarNgHrz2DKdksukbSQh6T0411uwZ6yAX5KU0wZ5P2I23el0KGLT+GJefsId0AyuYci64KEPZRiYkF24MyB32S2kgMsfOGh4sSyy3IKiz/gtci0Ur1vK6ZKKWa/7kIjr7Tjts0lyJArceC3MZIwM+Zk9yhYJ+jQXYq+VX/pYOaCJFhTsJnku6QFOyJiQpE="));
                skin = skin6001;
                break;
            case PALADIN:
                GameProfile paladin = new GameProfile(UUID.fromString("0d3d8b37-5113-38a9-8120-b65daf00d365"), MessageUtil.color("&e&lPALADIN"));
                paladin.getProperties().put("textures", new Property("textures","eyJ0aW1lc3RhbXAiOjE1MDA5NTE3NTU5MTIsInByb2ZpbGVJZCI6IjdjZjc2MTFkYmY2YjQxOWRiNjlkMmQzY2Q4NzUxZjRjIiwicHJvZmlsZU5hbWUiOiJrYXJldGg5OTkiLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzE2OTA5OTM2YjVmYTE5MjRmOTFkMWRlM2U4NzBlZDcxNDBiZjQzNWMxYTFhNTVkYTU3M2ZjM2E4OGViZDViZWMifX19","DW0Oiz6g0qEHdVFnEO/SIN1VY/REMxI6YwzenMBHQfvtYFvsS08LmJ3ulFddAuAKt0/LwEHzm5lb0TH4eWdTntZ9zhocYnScBKvjz4BjzRbQx/d4Vp4oLigXh6X/v+RTGYbfCbOM1RJtTfd8lxm0aNa7jSuYz0p0kwxMSSUYW63Aj6WVWpZdroaRiFVLyb53fjDgp+ipXZiZU2pLVocD+Lh22ve+6IFrPR4g+3xEhTD+WZKnsHAFjAOrNYB5pfEzdbKXlpAxgPw44oi1uJK85ZnvEeDpK4IO0zKWjbeFLrRVLy3rXm2jxXBNUJB8zL0I+cZyXpMgaYsNs4JR/W+ParyK6t921eiIZtwnw78NDcKLH8zrd/Y5pYG8SKSLVRTmYSmpkO6r5oBGiR38iL4Gv9pV12u4iUuJN0LxRbFE4QD1r5FBBTuyGuuuG05ia8lmeAdaQQQL3lud5qz22au6SaQabeB/muK3z/dkTh+wAY2F3t6zjioU5Q//zYEXDYQsGdlRp1weNeWJ7kONllcfHAYOvSvKTDKUiBWytcJcXlkqLksIOPj4rmX9o1Lt4/n4/HGHQRajqYf19WXKDDngdZCmvjgRsm1avjOlZ1aQ6UITHC/mZwdT5JJQowf9rNQTOLt8ujE1gCOEHhDz61a3cxv+093pIDylRzEMmKoLnz8="));
                skin = paladin;
                break;
            case PIGMAN:
                GameProfile pigman = new GameProfile(UUID.fromString("a35e2f45-f86c-3cf1-84a9-7f80a0017fe0"), MessageUtil.color("&e&lPIGMAN"));
                pigman.getProperties().put("textures", new Property("textures","eyJ0aW1lc3RhbXAiOjE0NjczMTMwNTE5MTcsInByb2ZpbGVJZCI6IjY0MjQ4NTljNTk0NDQwNzdiZTllMzJiMTY0MjdjMTIwIiwicHJvZmlsZU5hbWUiOiIyMDkiLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzIxYTEzZGE3NTU4ZmViZjc4NjFkYjA0MTM4NGUxNmM4MTU2ZDBjNWExN2RhZmMyYjlkNjc4ZThmZCJ9fX0=","Vt41qNNKTIBOsnpJma+DLX5bdK46KvC1m+EUUZ3xZwhqiuAceLJEMIrA2BApbkswCLoze7zNWKVdD1WqbQPYJQr/Vh5B9DtIK4E7MkSLOcuaIomMf9OVAuqtCKomywiWhhDyCTx+xjjnG14VPIrt8ZgNl1OSEwKod3ARyzCxYPbtiUUBGOgCSS7vFkptoMhJi/12iL7TW+yAojHuJ/vJLOjrF2Q+e0kufj8IstvdzE64H5YLiBtXxDyOVMx/GOO9yvKvtg/cO/xHi5c+6AJGzwO3OYJ2FX1m2Tqx4l9BW0NTQscAyVMGFI7Keeg8JckWxvvJzlmiuS2uSUAp0W03PkpLYQ5YmrkC2SCho3uEE7MvMQbPgshH4fsHO9XlFgWWBnEfWfeGKhn3Qfn9M7SaTTyReAQbLqa2vBbXBCTm3JtzfyRjEwnTaLlXp3Hbask1rUPxFttbiyarztmpLnwpeiVwqenGc7TxC1uItNz/YmTx+Pinr9aL+atyqyB4MSVKyqJGQELUtGWoxqSbBt4wcW14GMS7PL3vaoXdx9ySYS25wm0e6/4GdC/z+b2FpeThZfowVo/AmVH4Qf+FHm6JAlDg+m8ZYZnfM3a2WhswpasUmjtk3eH+3kHQUFl1lqkcnkJ6S10/R1XcMKZXM4zJq1VDssY++UbvRgGJASK0kAg="));
                skin = pigman;
                break;
            case PIRATA:
                GameProfile pirate = new GameProfile(UUID.fromString("004ceb07-884e-3047-b48a-f2208f2fb928"), MessageUtil.color("&e&lPIRATA"));
                pirate.getProperties().put("textures", new Property("textures","eyJ0aW1lc3RhbXAiOjE0Njg0OTMyMDcyNzYsInByb2ZpbGVJZCI6ImRhNzQ2NWVkMjljYjRkZTA5MzRkOTIwMTc0NDkxMzU1IiwicHJvZmlsZU5hbWUiOiJJc2F5bGEiLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzU5NzNmNGVjYmJiMmY2ZTlmMWU3ZTgzOTQyNjE3ZGJiN2MzYzExZDIyOTQwNTkzNjFmYWI0OGQxNGE3NzhmZTcifX19","xpgQzSy8luOLbDpLfNJj6tTznMv5GsqzsmQ01Y9prUnsNFU7VWs/S+eP1OpgtV526gc8HC+YvIBpYU/v+X7FyrhDliuHPOYcslxOvA4cxj9LAD68bcAoPJKNv4KcbjfhIws3Jo7efLLzAbXoRw6haad8VOPXRaisQe6t29nt6s4ZcPFo1yYl2M44zdqROCTSVJilHfDvJLcLbh7lcH7Rw1kUhfzzsbq19YaKKm2hRLmn/lZ17ly4TL8nb8M00lMbXxgvnrAUOF7wCs0HoY0Es8cL7wIVmPUeCoYlbPuHUjtQ/3iYhPnQkLpw1vgZV2UtABjqklBKiJeeboKkl5aXUUiQ7QtsARcMKXW4UUL3m+0ZUrO6bTaas//CN17wI8PC1kaw6RryYoEye8sw2s4ImNKGEw9LkCnLSXuBvInlyraI3teTa2oYlxToI88hBvpFdgWnwe0gWz5wCuPgsvuF4kF6mqjIwKliK41mZm2JW8/iLvjlFj2DcHRq7jI+uAkIe/mxXhM2rJNP73ZooQnz0eMzEySfVNzJ6dEoCT0WGTQuuWnm8Czd2+dOTp2NYlYyZ3YHduy8CiBw5328GrJMlpUG7001MqgbiTWxLadMGpvXi2D8ciHfvcmkcMCABcBgkvca+YHeITxj68LzOSUkwv1nSSAI8UZJbVjO5YL5yxs="));
                skin = pirate;
                break;
            case PYROMANCER:
                GameProfile pyromancer = new GameProfile(UUID.fromString("24518abf-edc5-39af-ba23-35dee5fd1a6e"), MessageUtil.color("&e&lPYROMANCER"));
                pyromancer.getProperties().put("textures", new Property("textures","eyJ0aW1lc3RhbXAiOjE1MDA5NTE1NzU4NDgsInByb2ZpbGVJZCI6ImNiZDIwN2M3ZDNkZDQwYTg5NTBiZGQ4ZTU4M2IyYjY1IiwicHJvZmlsZU5hbWUiOiJBbGVzc2FuZHJvQ2Vydm8iLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlL2IzYjZhY2RmMTFkYThiZTNmOGJhOGMzMWU1N2VhMTg3ZTJjYWQxMjZlY2FhYmYwNTA0M2E4NjliNzIyY2VjYSJ9fX0=","sDpxlH2DsmhhZps7M5bbUq6NRbpbSXxiklfIyhwIOCq+3nR7lcp4zcIucVlJPSV1WogLwU43uGVaPL9+mqKDH3i2fWH7jeGUGm3+lLYdcf0qOyT04yi0KvGmswyhz3WEZ8ZidTvfPLm/apDw6rPrmlSlJzc7TllIb7cyDNp4DX0mr+XeL7n2Wx6jOTZubG2Apeib+Ek+8JISw4w7sIGYf0uVNW4/8C5IkNbTe6+qIlqL/63SoQBcr+h0uErsYvX3Vn+W7siFypgT/PVFOyHr3CP7Ix4F/CgtOmUJA6dzhkMr6+Sqc4l0rQUX4g0slqMLVg3qL9dMRkCrJY7oKz5WhuXGtCGgzKFLHNeZ4Q6MM+4wy9bdAIdIhLnof6z6Qh+UgcGMt6ysKmu1OC4JRcCqjEFNIypZJT4UquZFOClGm3QxKkqJy6CD/LZutprZkQ+wVtwgnY3y614AQVnQkjLXtilBnIjoO8gjZn4O0w4cw9qDcjTOOYNj/Pzx8Y40L7GEwduY9NTohl6gkp3WNUVmZWILEVtOeAhd5DF9kYIGKArBt770HIgfJeo6AC8YFaa0KaobCDzV4a6/1E8wzttwV67VRUARBIp8EfCS+GbcFYCSlRahKKdsS+QqubixUejFzqbUywOC8+zk9pbRlI8rSIzFhOPDfK+2Mmt7D3euCqA="));
                skin = pyromancer;
                break;
            case MAGMA:
                GameProfile skin159696 = new GameProfile(UUID.fromString("edebd288-a08d-41eb-aa64-b6d06d40913e"), "skin159696");
                skin159696.getProperties().put("textures", new Property(
                        "textures",
                        "eyJ0aW1lc3RhbXAiOjE1MjIzNjIxNzcyMDQsInByb2ZpbGVJZCI6IjkxOGEwMjk1NTlkZDRjZTZiMTZmN2E1ZDUzZWZiNDEyIiwicHJvZmlsZU5hbWUiOiJCZWV2ZWxvcGVyIiwic2lnbmF0dXJlUmVxdWlyZWQiOnRydWUsInRleHR1cmVzIjp7IlNLSU4iOnsidXJsIjoiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS82MzQ3YjNjZjcwZjZhMDg5ZmYzZTg2MWQxODE2MzM2NTI1Yjk5ZTYxODhmMGE4NjRiMmYyMWVjMmI1YjZlNTY5In19fQ==",
                        "AZzG0ABLicUlDVzRHUt0vFAotE5UOduAMW0n4KFOdAkbtwGljoUCeDdG0j+Sp1meLU9UvoM/n7j5pm7NzxRtvpBLxHnGbCoDSKs8O3rtn7rtHLA/sa0+mORkS4kcK6OgdNcU+Ov+/De16qQntH2oaHWsi/qGJ9vy2ZOHsjgqrc35EXq1H6iybVs8VY86yFO8ShgWl7iAFJl8A9WxjSff4jaXooibVZXlv5RIh/xHIN31jHnhDNuGCRwZ07GqYCHDu18JlVy7JrCbKniVexArGJAMZItUMmhWCX7ytb/IMnrGDm3NFBbCukDixpLsdd3MRrmwiF+8oeY22NFQbDzYeC9kIZc/BBjAlpDUJ1qngO6eRe5rYmkAoOab7Z/K4OOctdnVfDqgukRfB35AhV/ahvn9MtLTumAYxrlGf361e5vZkZ9W5UIGhuqGWCiutbkWb+6GD3xpI6/A6xcmmDAh+nWGRqguAVUDvUFio4B6XV0eAjYT/IBsgWGUT3qzsdBFvPRbi05Hn2ndE6wOzPwmstols1+774Bdsw8UGdUrVJKDFu+M8J8cFLqVlfZ488WnsLDWX87TgBBOOfLR+wEW5A47AifTIDrrG2rrv4b0HeA9YxwWgS6AG7DTc/YX5i0VeS4FY0VjQ2f7Nj++t2fkhr9dFtSxddjY2NSC4TLBygM="));

                return skin159696;
            case SHADOW:
                GameProfile skin413958 = new GameProfile(UUID.fromString("7460a21a-ff18-4dc7-87bf-7b8e51bf86f7"), "SHADOW");
                skin413958.getProperties().put("textures", new Property("textures", "eyJ0aW1lc3RhbXAiOjE1NDM5NTg5MDg1MjUsInByb2ZpbGVJZCI6IjU2Njc1YjIyMzJmMDRlZTA4OTE3OWU5YzkyMDZjZmU4IiwicHJvZmlsZU5hbWUiOiJUaGVJbmRyYSIsInNpZ25hdHVyZVJlcXVpcmVkIjp0cnVlLCJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNDUyN2U2ZDZhMTQ5YmQ5YzEyNmNiMTg5MGNmOGQyZjgwMGMwOTYwZmM4Y2Q2Y2UzZGMxYmIwYzc0MGY0MTM0OCJ9fX0=", "Ve7JnxACGfm+nSStJgqtrNe4y9ue9WXvcsIJaHcLg5k/6loKFdVkt3VFgVIs/ikOzFXilV/+piQXVTKPYArNWqKMa/7vP5brqhpoHYe/P0GqkKurr1N2MRWY302aowcFLCzJgtHNd1cak7OGDVVMGyywmz7jGhwJwaphbl7auoHkM0GBS1GcDv7owEa+PUb6TAkhQ/Dump0qRUxFl4o/odmbBDwVgzy0a3+nenRww6BHZYR42trJE1q6Xf9igxXPUI3WXsfqmPjMk41JhigVPkvdZA+jg8V9gdm/A7GmNqgpMlRPhkHULIgHBDbDWU6pMlbSMHwgRQ5LrBLXw7FPzuD6O2QmmvG6fxoBDpo/5STgaMoXobeDWcRkXvyOHzVvnggemgx5HujOTROW34coiB1t2Y1DCvEez6q1Ysm+OfqKOjPlLSLBvvkNIOcaRx80eJZozYe4bjkVoPwWucelowntpEJgA0QHBFSkXJn5vIF6ubJh2MQ4Mw4VykNOfLI7AwZyOQhjpAgMie2rUaaEtVMg4cRaV+wD9b3E393J5A869s8RimH/ZHzGn2DbsgTl3gz8dCF43k8QltzvhieoKVocnkfOhFZEpqPCRdm94C70GdKxvYvbL0jWESSCrUsAi1/12++NNqYKmQ7I9xif8s+kzyJ4j1CQBr54Tf1JYD8="));
                return skin413958;
            case SHAMAN:
                GameProfile shaman = new GameProfile(UUID.fromString("85ce599c-c4e2-3b26-8dc1-b8486ea4a0b1"), MessageUtil.color("&e&lSHAMAN"));
                shaman.getProperties().put("textures", new Property("textures","eyJ0aW1lc3RhbXAiOjE1MDA5NTMwNjAwNjAsInByb2ZpbGVJZCI6IjNkOWNmOTZiN2MyNzRiZWVhZDFiOWQ0NTM3NTRjYjc2IiwicHJvZmlsZU5hbWUiOiJOaWtha2EiLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzFhNTFkMDExZDljZTdiOTUyMjcyNjE3N2UxZTFkNTdlZDM5YmI4OTI5MTg5NmI5Y2JhM2M3Y2Y5Y2NlYyJ9fX0=","FHwU/ozEnprSov3hiFjezFjLX621BxqAUEynMzU0xtoR5EqkKxaXcnnX6bkyo8pg24eh8TG2eNVa/pkexnwdXJXsSMbnNxbKJr5Wc/8tJwA3XTBywYOX9X+LDAZtMScFp5IrXXPP+avhnHTH0CSKi+RcpSrHCKuPr3xHQoTm35z0K4+iIujmEB6hN3TkWoEBFyYe2e68GB0l8X91qRhi/Dhn5CRsehywg3J7duKOczSioC6mzNEg+odYC+CX5AQ7ZYyxcFlHKAcGIMkx9sT6IaTjK80keS4Vx22z+41yNOFw5zon5XXeaeca5hptcI0siWhKQuCWR2RPYvhYtwNo6nkqQWbzAkY6pIE353Bkgeik+QxNcqRgs9ZhU9SEW6IrjWgdQFpSQrdlOGxXHki7v0PlEBl7wypj2gYQuCrD2F0LZDKRr6BNFp5yanpTc6nIGDZts7cc6PEZNGZTVaq+JNG/15VLgYhV1V8PL7D3mT/A1SQYK3V13gZQSvrt0jftO2Z9Fzoq6HsyfHlIBoSUpbBupHQDV3vG3rmJqR+GXlsp5Me4lkDt94c8ycLq9289NQ9ioufIABRfPKGdsHvpD8D7oibRIGkYIv2JPsJCYH0demvYSzJMBEJlOXVTxSdClZfbd/uIQzP8ClPDhd0RhvCsvSQ+2yOaOeIfxKRuVfM="));
                skin = shaman;
                break;
            case SKELETON:
                GameProfile skeleton = new GameProfile(UUID.fromString("c27ee917-f8c9-3fb0-89f1-717f77bf666d"), MessageUtil.color("&e&lSKELETON"));
                skeleton.getProperties().put("textures", new Property("textures","eyJ0aW1lc3RhbXAiOjE1MDA5NDQ5Mzg3OTcsInByb2ZpbGVJZCI6IjQzYTgzNzNkNjQyOTQ1MTBhOWFhYjMwZjViM2NlYmIzIiwicHJvZmlsZU5hbWUiOiJTa3VsbENsaWVudFNraW42Iiwic2lnbmF0dXJlUmVxdWlyZWQiOnRydWUsInRleHR1cmVzIjp7IlNLSU4iOnsidXJsIjoiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS85MjIxMTA1Y2E5M2E0ODJlM2I2MTE4NTk0ZmRhMDYzYWRlOTg2ZmFhZmQ1N2ZlNGNmYzk5NDUzNjU2OTczIn19fQ==","r7ch7WmM19OlUdx7jriWl9DJuFck6T3+nFwzP4RYOstqeyhDyVT7XptYFdJtc4t4Z0f1+hNiC75nNL28+dQs62Udy9l3i83mHgjHfWSsWdxRIvwNDRAKAMrnW6OLbFu6WnstNNzm9fB8bRCiEI/V8ZWwvrYHhleAOHOTiwv0SKfI9YWpIpoJxQXeNQ3VlXUKc5I0tdodkZUV+xmWujbvFRxnsIt18SUhBxF8LjI8lBsxP6t0PEAsP2LlHmZoGfft7oUqTmuDstBm+ozLrVB5khKj3kL6QhGq8RZUvwsOn6HSFUaaV9DCEDk7FodLnYeSl4b7QE6i9xJkK108ueq/su2aaC+TJxdNpWgZSUHNnsRTgUyC0/F+XwScPD+qSbQ16AQtdRNWCeJXjvRCa2jCBduXeTbPvNdLZbzIzsSHHxbK3kMtPkyqBJkxToYKIQXq0eNPFclJbRjzpAqvA5s3gXEX1/jGI/PBW3hvmmPy1n0kaLguYr2R3JgWh5Sn6mfeq28VLfmWRFwMGtQEp4DiB8zrRPpsJE26tQfh5ef/RKO5CKad77fhBvqdiWL6BqKNsGNtyWaIaFCUN4VQ26e/+FvYdwr16iz/JwLf2C1pGmTAB8WpgccEzcPc/CKcReU+SmhnwZkHv/GeChgCBkU/CVudFYdReLXZYdjBHCdFckI="));
                skin = skeleton;
                break;
            case SQUID:
                GameProfile squid = new GameProfile(UUID.fromString("fa63c5e9-dc56-366c-98ad-a33b53b0a3db"), MessageUtil.color("&e&lSQUID"));
                squid.getProperties().put("textures", new Property("textures","eyJ0aW1lc3RhbXAiOjE1MDA5NDQ3OTI0NDYsInByb2ZpbGVJZCI6ImJiMDgwOTFhOTJlZDQzYWQ5NGYxZWRmZDc0NmIwYTJjIiwicHJvZmlsZU5hbWUiOiJJcDRuZHgiLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlL2ZiZWIyMDU2ZDc0YmM5OTIyZmNhY2U4NGU1OTRjYTMzZjI4YWNlYjVhY2QwNGY1YWE2ZjU0N2IxM2YxMDQ3MiJ9fX0=","Dd+cGO81SByHM/84Ot0X3VzHf5+G6ItYntCydFLcgLOV3DK4B7UQw8NhTJ+44x0gHrTSJLmewjPaLuRb7RreFwFpGZMX2pVHIHma22rrHL5BQxS96zvQCHRaTATMzAhzGRidyisZaRHj5T4TDAzcKe35FIEJRKeizoBkwLDU2KykfW+C2kwXz+f/BWwAUCdqAijcjita2ZkzL2P2ByY2VGdgR0JYG/LQbpe1BBUyGlU3QX55w2iUxHKuET8zjTaSSts+WnIHL4L2SsHeKH82Kr6AFfQgydoXW8DKiUc6Yk9ssluhGb51nfvAPb0rqAmhIA46dzr+byrLVjj5oeQKX9Mt8+DTTv0oY8/BiCp1AdVEJc7uRv32RzQJ1ytAhgW6mDdvae27mWlphoSHjc0jmKomnjdzuRAQJoa+o91Ez5haW4aXXunWRV+FhpTqWHfye69imaSfOjIReurrYRcjwf7goeDDownuhvzxpRfz2TDscbjM4hTUU+IJAd8b3AUcDXCcw9iLglzmbqJ2FxsMYwwk8qW09Xe6B96U1G4CzA9DgtIKji0nYDldfrGtEu+4FxE1m/rpCqnEAfA6dlEvZZ96X8gxdLZVMn55Lgnr+LPT8Fm89vj61DM1hgtG6bxEsgzEM8CPc4Oc+GozTIyshDZ6eUVnt8agp+77S1ZjqaA="));
                skin = squid;
                break;
            case ZOMBIE:
                GameProfile zombie = new GameProfile(UUID.fromString("02b0e86d-c86a-4ae7-bc41-015d21f80c1c"), MessageUtil.color("&e&lZOMBIE"));
                zombie.getProperties().put("textures", new Property("textures","eyJ0aW1lc3RhbXAiOjE0ODYzOTg3NDAxODYsInByb2ZpbGVJZCI6IjAyYjBlODZkYzg2YTRhZTdiYzQxMDE1ZDIxZjgwYzFjIiwicHJvZmlsZU5hbWUiOiJab21iaWUiLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzc2NzY0YmZlYmIzZjEzMmI1OGFlNmYyOGU4OTNjYzJlOTk2ODU2Yjg0NDgzMDEzMjUxNzVhYzZkMWVmOSJ9fX0=","ENb9UZAK5wuSk1QeHtsR03JYx9dwXJUU4XUnSSI8BqwamxZmDpK6tHgkmifcTcGSChHNXxbJvGip2ZV9ALQfA23xcrkOet5Gpz55woBMOYySeEpzSuM+o/kc55Z5EU8W6ezRwV4cKXISHbHbkWahSVgf4vilz5H8yfmfqf+1E5CKV6aXrsPdCXyYva1QlCcKrugNUb4n6bOjGhLkPtKky+Om7sECN9fimhhcGcFeLqxvUFpXP2RBjwir1M8goPCZopONeHu9242khy4EA0BeSx4e3iq3QPASn+lAfvnNpqGWiGk7+JzVNvodtxefyjyb3HBA7F00RMbdhzQBdyfvrkmNRiDyk8aoOIIDdFqhpRaxObnM1h74pLCEG0nc5JwFt6BP7ihlkss5v3SbPNaFEgCNWLeP4Nk5RsIVaG3f4Xed9ltjjD6jiN501HBSjC8EoJVIFZp/X/f7SIAtRdHkVA7s+myT6TqbpsRxciB89ogPhhgP35IWcxmjp0CeHVWiF2REW3HLwev+xY2sVPS6sMSwzIfifgY2JWF0d81xC8BlQ3JRBVXBA2I/6ZeQ/SlyrZfDkKYuHt/8XrGDyhfdgh3/hIjPYj1IutinDXPUTCoxIggVpWz5niAFznlbJ3PNh1nHW9bo3x/NQHp0EojPqErsddrNpINBNQwF7oUd3cY="));
                skin = zombie;
                break;
            case AUTOMATON:
                GameProfile auto = new GameProfile(UUID.fromString("02b0e86d-c86a-4ae7-bc41-015d21f80c3c"), MessageUtil.color("&e&lAUTOMATON"));
                auto.getProperties().put("textures", new Property("textures","ewogICJ0aW1lc3RhbXAiIDogMTU5MzczOTQ3MjU4NiwKICAicHJvZmlsZUlkIiA6ICIyM2YxYTU5ZjQ2OWI0M2RkYmRiNTM3YmZlYzEwNDcxZiIsCiAgInByb2ZpbGVOYW1lIiA6ICIyODA3IiwKICAic2lnbmF0dXJlUmVxdWlyZWQiIDogdHJ1ZSwKICAidGV4dHVyZXMiIDogewogICAgIlNLSU4iIDogewogICAgICAidXJsIiA6ICJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzFjYzhjMTA1NWYzOTIwMDRkYTZhOTJiMmQ0ZWUyMzc1NTZlYTUwMWMwZWM1ZmRmOGUwZTU0YjQ0ZTJmNTFiYSIKICAgIH0KICB9Cn0=\",Signature:\"pdb4kH3Au1kr6FyTIwsbwPX3mvFHCl7xv7BkCUoy8Ol/ezKuMrN5/QddhdF7J6ito9j/KBajwVtplybxRR9j/J4dKPdKaPErc7wxTFvlwU2X0UEghu7N0mv1QH78O7Bz/ekEfN54rPIt1IQUXg0eQ5GONF055ZdBCzCLHiC88sdXTPmvBgFDME0G+2tRVqaA9soTrumOJQ8xxjWkNL3i2vvM7em1BLo9jlQWdHFE0QWRaByXpvXVpUDC6Tlaub669g+HGwcJc/eQLlspLxEOP2SZeTmhm7EqGzwDRQRJVSfQV2IORiLBWI9JYkrA7Ybb1Hct5OMRbwNDfaILTRnkvtTRenDihxylZAlx+RK4hmzya5YQoE1DwzKpg4s861hbknc8AO5vsbWFxUiaQcLzOVUMOyn3mcrGt3kVJ4feY4xtJ4KoP5/vzSENiQprXFAalZdgUoGR6SAxMace6M5FrV6CCZrQZ7V0kELEHFO2vtPMulQBx/unN/WBEXNNYjvimzpMymtCOIiFg1S2PenML7/ZG8U+pHMzpbrEQ3tgDncvDR2Zv0N1cAb0AZQloZt/xTI1rruqaOAHG9L0T0P2sDfX8BqBEIu0w8mbXnkcCuSZKdhpHi1w8WyKdvKIp3r8+cw0mp3IU3639mpErAVnuottKbSATJJGFtLxMPfJIjY="));
                skin = auto;
            default:
                break;
        }
        return skin;
    }

    @Override
    public String extractTexture(GameProfile g) {
        if(g != null)
            return g.getProperties().get("textures").iterator().next().getValue();
        else
            return "eyJ0aW1lc3RhbXAiOjE1MDAzOTMyMDI4NDksInByb2ZpbGVJZCI6IjYwNmUyZmYwZWQ3NzQ4NDI5ZDZjZTFkMzMyMWM3ODM4IiwicHJvZmlsZU5hbWUiOiJNSEZfUXVlc3Rpb24iLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzUxNjNkYWZhYzFkOTFhOGM5MWRiNTc2Y2FhYzc4NDMzNjc5MWE2ZTE4ZDhmN2Y2Mjc3OGZjNDdiZjE0NmI2In19fQ==";
    }
}