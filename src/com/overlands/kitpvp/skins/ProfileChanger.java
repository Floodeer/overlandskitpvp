package com.overlands.kitpvp.skins;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.google.common.collect.Lists;
import com.overlands.kitpvp.game.GamePlayer;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.common.collect.Maps;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import com.overlands.core.utils.Runner;
import com.overlands.core.utils.reflection.AccessUtil;
import com.overlands.kitpvp.Main;

import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo.EnumPlayerInfoAction;

public class ProfileChanger {

	private final Map<UUID, GameProfile> originalProfile;
	private final List<String> disabledProfileNames;
	
	public ProfileChanger() {
		originalProfile = Maps.newHashMap();
		disabledProfileNames = Lists.newArrayList("Touyama");
	}

	public boolean isInvalid(String name) {
		return disabledProfileNames.contains(name);
	}
	
	public void applyGameProfile(Player player, GameProfile newProfile, boolean respawn) {
		CraftPlayer cp = ((CraftPlayer)player);
		GameProfile skin = null;
		if(hasCustomProfile(player)) {
			restoreProfile(player);
		}
		skin = cp.getProfile();
		
		originalProfile.put(player.getUniqueId(), skin);
	
        try {
            Class<?> entityHuman = cp.getHandle().getClass().getSuperclass();
			AccessUtil.setAccessible(entityHuman.getDeclaredField("bH")).set(cp.getHandle(), newProfile);
		} catch (IllegalArgumentException | SecurityException | ReflectiveOperationException e) {
			e.printStackTrace();
		}
		
		PacketPlayOutEntityDestroy packet1 = new PacketPlayOutEntityDestroy(cp.getEntityId());
		sendPacket(packet1);
		
		PacketPlayOutPlayerInfo packet2 = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER, cp.getHandle());
		sendPacket(packet2);

		disabledProfileNames.add(newProfile.getName());
		Runner.make(Main.get()).delay(5).run(() -> {
			if(respawn)
				refreshPlayer(player);
			
			PacketPlayOutPlayerInfo packet3 = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER,cp.getHandle());
			sendPacket(packet3);
			
			PacketPlayOutNamedEntitySpawn spawn = new PacketPlayOutNamedEntitySpawn(cp.getHandle());
			for (Player pls : Bukkit.getOnlinePlayers()) {
				if (!pls.getName().equals(cp.getName())) {
					((CraftPlayer) pls).getHandle().playerConnection.sendPacket(spawn);
				}
			}
		});
	}
	
	public void applySkin(Player player, GameProfile newSkin, boolean respawn) {
		CraftPlayer cp = ((CraftPlayer)player);
		GameProfile skin = null;
		if(hasCustomProfile(player)) {
			restoreSkin(player);
		}
		skin = cp.getProfile();
		
		originalProfile.put(player.getUniqueId(), skin);
		
		skin.getProperties().putAll("textures", newSkin.getProperties().get("textures"));
		
		Collection<Property> prop = skin.getProperties().get("textures");
		cp.getProfile().getProperties().putAll("textures", prop);
		
		PacketPlayOutEntityDestroy pds = new PacketPlayOutEntityDestroy(cp.getEntityId());
		sendPacket(pds);
		PacketPlayOutPlayerInfo tab = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER, cp.getHandle());
		sendPacket(tab);
		cp.setMetadata("skinChanger", new FixedMetadataValue(Main.get(), null));

		disabledProfileNames.add(newSkin.getName());

		Runner.make(Main.get()).delay(4).run(() -> {
			if(respawn)
				refreshPlayer(player);

			cp.setMetadata("null", new FixedMetadataValue(Main.get(), null));
			PacketPlayOutPlayerInfo tabadd = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER,cp.getHandle());
			sendPacket(tabadd);
			
			PacketPlayOutNamedEntitySpawn spawn = new PacketPlayOutNamedEntitySpawn(cp.getHandle());
			for (Player pls : Bukkit.getOnlinePlayers()) {
				if (!pls.getName().equals(cp.getName())) {
					((CraftPlayer) pls).getHandle().playerConnection.sendPacket(spawn);
				}
			}
		});
	}
	
	public void applySkin(Player player, String value, boolean respawn) {
		CraftPlayer cp = ((CraftPlayer)player);
		GameProfile skingp = null;
		if(hasCustomProfile(player)) {
			restoreSkin(player);
		}
		skingp = cp.getProfile();
		
		originalProfile.put(player.getUniqueId(), skingp);
		
		skingp.getProperties().put("textures", new Property("textures", value));
		Collection<Property> prop = skingp.getProperties().get("textures");
		cp.getProfile().getProperties().putAll("textures", prop);
		
		
		PacketPlayOutEntityDestroy pds = new PacketPlayOutEntityDestroy(cp.getEntityId());
		sendPacket(pds);
		PacketPlayOutPlayerInfo tab = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER, cp.getHandle());
		sendPacket(tab);

		disabledProfileNames.add(skingp.getName());
		Runner.make(Main.get()).delay(4).run(() -> {
			if(respawn)
				refreshPlayer(player);
			
			PacketPlayOutPlayerInfo tabadd = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER,cp.getHandle());
			sendPacket(tabadd);
			
			PacketPlayOutNamedEntitySpawn spawn = new PacketPlayOutNamedEntitySpawn(cp.getHandle());
			for (Player pls : Bukkit.getOnlinePlayers()) {
				if (!pls.getName().equals(cp.getName())) {
					((CraftPlayer) pls).getHandle().playerConnection.sendPacket(spawn);
				}
			}
		});
	}
	
	public void applyName(Player player, String name, boolean respawn) {
		CraftPlayer cp = ((CraftPlayer)player);
		GameProfile skin = cp.getProfile();
		if(hasCustomProfile(player)) {
			restoreProfile(player);
		}
		originalProfile.put(player.getUniqueId(), skin);
	
        try {
        	  Field nameF = GameProfile.class.getDeclaredField("name");
              nameF.setAccessible(true);
              AccessUtil.setAccessible(nameF).set(skin, name);
              
		} catch (IllegalArgumentException | SecurityException | ReflectiveOperationException e) {
			e.printStackTrace();
		}
		
		PacketPlayOutEntityDestroy packet1 = new PacketPlayOutEntityDestroy(cp.getEntityId());
		sendPacket(packet1);
		
		PacketPlayOutPlayerInfo packet2 = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER, cp.getHandle());
		sendPacket(packet2);
		
		Runner.make(Main.get()).delay(4).run(() -> {
			if(respawn)
				refreshPlayer(player);
			
			PacketPlayOutPlayerInfo packet3 = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER,cp.getHandle());
			sendPacket(packet3);
			
			PacketPlayOutNamedEntitySpawn spawn = new PacketPlayOutNamedEntitySpawn(cp.getHandle());
			for (Player pls : Bukkit.getOnlinePlayers()) {
				if (!pls.getName().equals(cp.getName())) {
					((CraftPlayer) pls).getHandle().playerConnection.sendPacket(spawn);
				}
			}
			disabledProfileNames.add(name);
		});
	}

	public void applySkinAndName(Player player, String name, boolean respawn) {
		CraftPlayer cp = ((CraftPlayer)player);
		GameProfile skin = cp.getProfile();

		if(hasCustomProfile(player)) {
			restoreProfile(player);
		}
		originalProfile.put(player.getUniqueId(), skin);
		try {
			Field nameF = GameProfile.class.getDeclaredField("name");
			nameF.setAccessible(true);
			AccessUtil.setAccessible(nameF).set(skin, name);

		} catch (IllegalArgumentException | SecurityException | ReflectiveOperationException e) {
			e.printStackTrace();
		}

		PacketPlayOutEntityDestroy packet1 = new PacketPlayOutEntityDestroy(cp.getEntityId());
		sendPacket(packet1);

		PacketPlayOutPlayerInfo packet2 = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER, cp.getHandle());
		sendPacket(packet2);

		Runner.make(Main.get()).delay(4).run(() -> {
			if(respawn)
				refreshPlayer(player);

			PacketPlayOutPlayerInfo packet3 = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER,cp.getHandle());
			sendPacket(packet3);

			PacketPlayOutNamedEntitySpawn spawn = new PacketPlayOutNamedEntitySpawn(cp.getHandle());
			for (Player pls : Bukkit.getOnlinePlayers()) {
				if (!pls.getName().equals(cp.getName())) {
					((CraftPlayer) pls).getHandle().playerConnection.sendPacket(spawn);
				}
			}
			disabledProfileNames.add(name);
		});

	}
	
	public void restoreProfile(Player player) {
		if(originalProfile.containsKey(player.getUniqueId())) {
			CraftPlayer cp = ((CraftPlayer)player);
			disabledProfileNames.remove(cp.getProfile().getName());
			try {
				Class<?> entityHuman = cp.getHandle().getClass().getSuperclass();
				AccessUtil.setAccessible(entityHuman.getDeclaredField("bH")).set(cp.getHandle(), Main.getPM().getPlayer(player.getUniqueId()).getOriginalProfile());
			} catch (IllegalArgumentException | SecurityException | ReflectiveOperationException e) {
				e.printStackTrace();
			}
			 
			PacketPlayOutEntityDestroy packet1 = new PacketPlayOutEntityDestroy(cp.getEntityId());
			sendPacket(packet1);
			
			PacketPlayOutPlayerInfo packet2 = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER, cp.getHandle());
			sendPacket(packet2);
			new BukkitRunnable() {
				public void run() {
					PacketPlayOutPlayerInfo packet3 = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER,cp.getHandle());
					sendPacket(packet3);
					
					PacketPlayOutNamedEntitySpawn spawn = new PacketPlayOutNamedEntitySpawn(cp.getHandle());
					for (Player pls : Bukkit.getOnlinePlayers()) {
						if (!pls.getName().equals(cp.getName())) {
							((CraftPlayer) pls).getHandle().playerConnection.sendPacket(spawn);
						}
					}
					refreshPlayer(player);
				}
			}.runTaskLater(Main.get(), 4);
		}
		originalProfile.remove(player.getUniqueId());
	}
	
	public void restoreSkin(Player player) {
		if(originalProfile.containsKey(player.getUniqueId())) {
			CraftPlayer cp = ((CraftPlayer)player);
			GameProfile skin = cp.getProfile();
			skin.getProperties().removeAll("textures");
			skin.getProperties().putAll("textures", Main.getPM().getPlayer(player.getUniqueId()).getOriginalProfile().getProperties().get("textures"));
			
			Collection<Property> prop = skin.getProperties().get("textures");
			cp.getProfile().getProperties().putAll("textures", prop);
			
			PacketPlayOutEntityDestroy pds = new PacketPlayOutEntityDestroy(cp.getEntityId());
			sendPacket(pds);
			PacketPlayOutPlayerInfo tab = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER, cp.getHandle());
			sendPacket(tab);

			Runner.make(Main.get()).delay(4).run(() -> {
				
				PacketPlayOutPlayerInfo tabadd = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER,cp.getHandle());
				sendPacket(tabadd);
				
				PacketPlayOutNamedEntitySpawn spawn = new PacketPlayOutNamedEntitySpawn(cp.getHandle());
				for (Player pls : Bukkit.getOnlinePlayers()) {
					if (!pls.getName().equals(cp.getName())) {
						((CraftPlayer) pls).getHandle().playerConnection.sendPacket(spawn);
					}
				}
			});
			originalProfile.remove(player.getUniqueId());
		}
	}
	
	public boolean hasCustomProfile(Player p) {
		return originalProfile.containsKey(p.getUniqueId());
	}
	
	public void sendPacket(@SuppressWarnings("rawtypes") Packet packet) {

		for (Player pls : Bukkit.getOnlinePlayers()) {
			((CraftPlayer) pls).getHandle().playerConnection.sendPacket(packet);
		}
	}

	public void refreshPlayer(Player player) {
		player.setHealth(0);
		Location loc = player.getLocation();
		loc.setYaw(player.getLocation().getYaw());
		loc.setPitch(player.getLocation().getPitch());
		EntityPlayer ep = ((CraftPlayer) player).getHandle();
		new BukkitRunnable() {
			@Override
			public void run() {
				ep.playerConnection.sendPacket(new PacketPlayOutRespawn(ep.dimension, ep.getWorld().getDifficulty(),ep.getWorld().getWorldData().getType(), ep.playerInteractManager.getGameMode()));
				player.teleport(loc);
				for (Player online : Bukkit.getOnlinePlayers()) {
					online.hidePlayer(player);
					online.showPlayer(player);
				}
			}
		}.runTaskLater(Main.get(), 5);
	}
}
