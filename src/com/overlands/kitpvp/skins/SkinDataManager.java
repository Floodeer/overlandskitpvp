package com.overlands.kitpvp.skins;

import com.mojang.authlib.GameProfile;
import com.overlands.kitpvp.game.classes.ClassType;

public abstract interface SkinDataManager {
	
	public abstract GameProfile getType(ClassType c);
	
	public abstract String extractTexture(GameProfile g);

}
