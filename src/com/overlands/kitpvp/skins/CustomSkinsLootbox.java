package com.overlands.kitpvp.skins;

import java.util.UUID;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import com.overlands.core.utils.MessageUtil;
import com.overlands.kitpvp.game.classes.ClassType;

public class CustomSkinsLootbox implements SkinDataManager {

	@Override
	public GameProfile getType(ClassType c) {
		GameProfile skin = null;
		switch (c) {
		case SPIDER:
			break;
		case ARCANIST:
			break;
		case BLAZE:
			break;
		case TEST:
			break;
		case RUMBLER:
			break;
		case CREEPER:
			GameProfile skin25477 = new GameProfile(UUID.fromString("01d79073-90d6-3f14-98e5-8aa9e707a7bb"),  MessageUtil.color("&e&lCREEPER"));
			skin25477.getProperties().put("textures", new Property(
			    "textures",
			    "eyJ0aW1lc3RhbXAiOjE1MDExODc0OTM1MDMsInByb2ZpbGVJZCI6ImE5MGI4MmIwNzE4NTQ0ZjU5YmE1MTZkMGY2Nzk2NDkwIiwicHJvZmlsZU5hbWUiOiJJbUZhdFRCSCIsInNpZ25hdHVyZVJlcXVpcmVkIjp0cnVlLCJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMjM2ZjhhNWZmMjZkMTU4ZjhmZmVjZmYxY2VlZDhjOWRjMDEwZDU3NjJhMzg2ODI4Mjc4MjQ4NTVlNTJjNSJ9fX0=",
			    "mmyISuwuOSOxJfkjynKIRESvbfkRtZKbWlKFQE57cM9VlatiBiD918SE4bOXrBz4ggJdFIQhISyFC04qb3cK3K7DuIj5GNCjHHTfzWFLxivQItHJbnOzMdGoJtFimc3sbWZLnP3hbmrqR42JDnDKSNegaC/t6fBGPJoQRFPo2Ej45RnI4ECJ1TVnFMIqpTrE66aFX+6RMdCraCkFzmgaDOHbI8MosbzMe3c67bNzRu0KWI7v1IwmxVK36t3rgBfxtskfEop0EHjlblFOShd1DxH/TcPwKvOj6lSF3J5Os1XfhZ221zXm78RawGD/jTLuzeVRUySWvkffPzkfqmfTVo7alkfdpo6sBpWrQZx5sxjmQF2+ao/kVJj+LSJBFIkQWUwD+qrzgmvkR0uTmNOqVrA7irPlPYDPLlDxhbcYSTSBHIEDx+BYSAsIvrJa2Jp60cf5EEyfLDOWqt9Okdo/6T11i1S3rZi0pF3BnxsdvCBGW+7Avl+ya+m+AGtosZreRjmZ0FigCC8rYcgEiDslU6+iv7KwF+eFVbTOoN3Rsoxj+Ve+cFyDIKS43cOgx0wzWZlqIvJ/k5Xvq0M2oct/kJcGs/Ch0MKZoc/pAqzsM+yS1HXF/lteLanwEsnqwD90HQf8EOh905fl7oe/r5x57HTl+tjezS+avlf+4kO2n2I="));
			skin = skin25477;
			break;
		case CRYOMANCER:
			break;
		case INFECTED:
			break;
		case DREADLORD:
			GameProfile skin10829 = new GameProfile(UUID.fromString("c12859f1-3e2c-3ab7-b45f-9b75a5b61ccd"), MessageUtil.color("&e&lDREADLORD"));
			skin10829.getProperties().put("textures", new Property(
			    "textures",
			    "eyJ0aW1lc3RhbXAiOjE0OTMxNTgwNDk2NDUsInByb2ZpbGVJZCI6ImUzYjQ0NWM4NDdmNTQ4ZmI4YzhmYTNmMWY3ZWZiYThlIiwicHJvZmlsZU5hbWUiOiJNaW5pRGlnZ2VyVGVzdCIsInNpZ25hdHVyZVJlcXVpcmVkIjp0cnVlLCJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMzk1Y2IxNjVlYzhmYjNmZmIwZTA3MDg4NTExNDkwODM1Y2YzMzFkODdkZjc4YTVkNDE5OTJkODc2MWVjMzYxIn19fQ==",
			    "CX2RIGTGI7Dbp4F/qnMEjDUTdXhHABPRCsMoml9CtLJThmJLIom3hPpRAGELvi740vdzYx1XAghMA9g+VhBUkj9vZMcbB5v4BCyAiQorItw6mAgVcrAd+ngiAW7KIhpKKPfByTVFVY2L8pq2vXcn7m7EWaxjNu45Z9H2alRqU5LJtqZhqIkBp+6z4MZNqrBZzqnY71XcFksMC66YAnY+EYk1tcONfyFxOozCt7kX2mYfwhAz9AWOo4jkS2eVjaByOtlwEEw0ab5iOYpGKAp14Iu+kwX3kk2jr0fo/HyJcFz3jjEo42fcxvaxHcWouzBm8QD3J4T9PEMYnRHIeKIkcwrbo3sSlhEB9ExjhY7KhSBMHzWA7XcJoxwLGVvt4lhVEE/DLV+CpZuKjkhk/wYMJ2zc4/iVRgguFqsAFLArp88J/PGi1KkrlIg15IwmXagmMMiI8M7I18aENp6Q9D46FRDdgHnXtcmJVh6jyI8Zjj2phqayFlBTMzT62doGIQyZMkK+Dd4XgPp2ApByM1AjtN82/K+kRY5vvWLBy6rAzFV5VrLduyYX2iXnXRxhOYpWECa4G0/x8UgTLxot6ZE9IbBlUA3S4HN2xc+iHDUGEbLsqeVDU/uSdKCLRUlkeLvEIpJ0R66jYIEnIgNY5x7qwa2v9b856aIKQvSAlSxCtUc="));
			skin = skin10829;
			break;
		case ENDERMAN:
			GameProfile skin24487 = new GameProfile(UUID.fromString("9a804852-00f0-3105-aa4c-7148f3680156"), MessageUtil.color("&e&lENDERMAN"));
			skin24487.getProperties().put("textures", new Property(
				    "textures",
				    "eyJ0aW1lc3RhbXAiOjE1MDEwNDE4OTU5NDEsInByb2ZpbGVJZCI6ImIwZDczMmZlMDBmNzQwN2U5ZTdmNzQ2MzAxY2Q5OGNhIiwicHJvZmlsZU5hbWUiOiJPUHBscyIsInNpZ25hdHVyZVJlcXVpcmVkIjp0cnVlLCJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNTA5YmZkMTNmNTRhYzQyZjkyMjZmZDhkNzVhMjkzY2U0N2ZjZTc1MWZjY2E3Y2U1MzJkN2IzMmI2NzdkMzIwIn19fQ==",
				    "eKRfeByWaHnH28qLkeIQ4mXx8NrPiFb2PxCrnWfNoO43qYnXjNvoR1XpHbnyR8/tZeiMjGcKT1r8LLWz/mwnNYY0DUeRSe9g7ZU/MbvzbTZma1VNe3DjRHZfAkZCZaSisw6CKxly5F3O6eYwiavLmLi1YOA1BO5OYlp80U/wMGInVxXWba77ry1t2NRwzRIQ7yu56tOtoFQ/+WyfZE80qc8BVkNK/vsfcidhGxYI4rXMFapkMkcKlghI3oFTuDL5i5d+8SHNVKgTY8NDrmv8pP9ciC3wgC4N83HcLvy0ouQsqpE1wQYYeygv9a751kx4lg6IxJLvP949AzAH4XYj0QtvMruxoGu8qivccMIeImMtvtATwBt4fODsmypaWdoWP5c9VhAHHUUK6PvS+9g+E1p8Tbzqz9JzeFm+ucaA8ktkpO+O8C18bK8LTNo1nUPYCsXLsBHcRl6FHvIG2h7w2ZrzsOrrIEGBg7E8GZWfLWt07jiqnobM+2YUgs3tWLh+sMrcD5mLXI/yWera5Fv2d0bt2qyRaL0DYF7D1q5XPDPJBphPkncl3v1bAiPsP9LRDQTkcG+WzVON2K5XR8RJCSZeZdAWzdGZOSqRSvpAQao/095x70ByBE8HNXNZKG+M7mowIDLiqO8G7gnLAwT8Sc5EBz+XC7XHqBaiRBPA0Dk="));
			skin = skin24487;
			break;
		case MAGNATITION:
			break;
		case EPSILON:
			break;
		case FALCON:
			GameProfile skin25348 = new GameProfile(UUID.fromString("9cc32bca-5c7b-3fb9-86a7-d162cbf07c9b"), MessageUtil.color("&e&lFALCON"));
			skin25348.getProperties().put("textures", new Property(
			    "textures",
			    "eyJ0aW1lc3RhbXAiOjE1MDExNzQ4MDA4MjAsInByb2ZpbGVJZCI6IjQzYTgzNzNkNjQyOTQ1MTBhOWFhYjMwZjViM2NlYmIzIiwicHJvZmlsZU5hbWUiOiJTa3VsbENsaWVudFNraW42Iiwic2lnbmF0dXJlUmVxdWlyZWQiOnRydWUsInRleHR1cmVzIjp7IlNLSU4iOnsidXJsIjoiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS82Nzk3YWNmYmMyNTM3ZjViYjljZGVkNTQyNGU3M2M4ZjdkNDc4NTFmOTEwNWJjN2YyMTFjMDU2ZDkwNDY2In19fQ==",
			    "ekwi4ZW+QJZgrnTzzrxMs23niH0bA7fC9pqR639KM0rNkLRn/H//L4HPMO3DB2heJsMa0qdo3gYne45fqf97bs62UjAX009pxISN04W/lN5rS3KGLGiupTpZflAeMbahdNvrjDsVzBMFGLtCpRr5Hlvd3UUDHRj2StSITX7JfwqirDauZJQik5wry+NLZToLsg0fyN8uBqyeKOR5lwMh2pDafkECpLYkfxRrc945tvDmQoM10TQNjui5aYOH18A6RRXasi+nxKhu7UV+I1yPv14skUA610T9MarIIOA68a/jMBvbGGzodpv4wsR9KZ61x/V2ofSyeBi1meHAOfppi0USah6DhrHYatlIxV0xY0zcGW4I+sT62OKx0wHFwddiy/vozj+oz4jhFMvIIPqp2GVZ/QJUspe8N4mjdO6lwfxIOV40DSCUlLAf76OyGFcjvaxfeaJ6OfDh0gFUkcTev5fXrT3VN6fTZBzXm3crMDgUcHIlBBUWJN/Ni04ab88Z0qpSPn2ghR+UQLX8+zEDf2PgiSRR7HjVo/e9N6e2kViOyBMsTbzgbdtTpMjbslqG6dlVuKVaQ8/xPczBeyLMSV/YxJqDyxvArPpDUzkht73Fu97YaD7xE/clj00Rm3lRbvLl0uZOffXN+ALwNHorh7AzCVWe/Fui1GqcFp5FTjo="));
			skin = skin25348;
			break;
		case GOLEM:
			break;
		case HEROBRINE:
			GameProfile skin10070 = new GameProfile(UUID.fromString("e1b951c3-21ff-31b9-9a5f-0c7412dea1e9"), MessageUtil.color("&e&lHEROBRINE"));
			skin10070.getProperties().put("textures", new Property(
			    "textures",
			    "eyJ0aW1lc3RhbXAiOjE0OTE5NTE1OTg2MDQsInByb2ZpbGVJZCI6IjNlMjZiMDk3MWFjZDRjNmQ5MzVjNmFkYjE1YjYyMDNhIiwicHJvZmlsZU5hbWUiOiJOYWhlbGUiLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzNjNTRhNzRlZTBhNTg2ZmM3OTc2Y2ViZGU1YzViNWE4Y2I4NTYwYmRmZjUyNGFkYzRjNjlhNjMxYTM5ZGE4In19fQ==",
			    "FAcj0b38p+O+GMUXyEqrNvrE9AapnA8Ts2qTZRO3OQ641ScYRwyN8G5NponF1fpYuepvmKlbwvGRlXt7+ipxs2fNFW/phbvbnbKo5bc8VGrRgL4CSdDITskg5LrPCShFnhWoBiKiZNRuCryP6MfL7pSzjyUaT40bneBLphcZKRNZvKI/XgKqJAFwwedCAXItSq2mwGAiBb+5UphELU5mdiCoKrMdp5PM5Mmznu3WJeI+o/2H8JfuNFWokRXoqerqw6LThrSAejx5BiIjy0z30nbM+nhdYJ/Lw/ZS6FwtNTi/I/YqiC1XglMZDFFWtS7/07N4D9QF/qDn7eseLLdkv7k9MVKPuWKcdDk+dIlrWuN7QT3xAsN7QErP1smdyey9vGfgKthYRuZvacQPOf05s1ObIandCVDGCpfS2RtYLWAfILW2GHU/4ItO7gRXuRVoJaeSrKXO4lcDJnMdQGAf6v16RcwQcnvCk9Z1W1DFzHibct+AnzFhgC+RDMbbz5T2owCmaBcT+bo2mRYZ2ii1gIrOJ3XhFHyZ0U09oyvasN8ZBzsxs3OFbomfEKPaV2LNVSXxe3cNM742vAIyL0yOWedxWADkdCilw7A38PwKuJxTnlxB5y28oRb0LUlVa5NWaKRJNbirdHh2mOgXr2GQfAbSotN+nnIiwsPB7S6E6OQ="));
			skin = skin10070;
			break;
		case HUNTER:
			GameProfile skin25088 = new GameProfile(UUID.fromString("799df640-f93e-3690-8faa-67aaa60f82ad"), MessageUtil.color("&e&lHUNTER"));
			skin25088.getProperties().put("textures", new Property(
			    "textures",
			    "eyJ0aW1lc3RhbXAiOjE1MDExMzU1MDYzODAsInByb2ZpbGVJZCI6IjNlMjZiMDk3MWFjZDRjNmQ5MzVjNmFkYjE1YjYyMDNhIiwicHJvZmlsZU5hbWUiOiJOYWhlbGUiLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlL2U4ODljMTgyMTAxOGY3MmEzNmFkZDRhNmU3MWZiZTU3ZGViZmY1ZjI1MTM5NTUzZTY4NmZiNTRjZjlhNzYxIn19fQ==",
			    "ZA005kz1PTDwPXGhrvkifJcCwizDe4zKQioHTblsk2w66QqXTtq+u1bcX3Ssi5i/Wj/xJxG8ro9f0KkvXN1n7vU8pR+Nhz0ZbdhQU2+JDXQy4OC/CFtwjXfwIreLITjrsSiffhvah+FUg5pbBG20AOnH3kh5Z7iiSuWHZXxdX/MuqHXAjp4zcYMFqH6IWAg3dKKR3jhTQHsz0490QaKUKXAdFVZz5kk2bWhiUTAWhLJdL53+wimaXVdCwLIL3TWUsJmk5yRc9OFJlEKUlvUEBviLdpo1d4G1JztHnM4Y8FRdVo5R0qvuiMa2vMflFuTHaAX/J5Vskfp53Wgm6cyIFWuTxHLC1BUuXEhdgx9OuCadJHtj/mA7MxyFnnGnOTFwJRgl1dFyto2OmvQHTb2sO+WtbQURv83lJLmyyLYdWOofvyxNkWkfRhsKWDgfSy3STTtu9PGux1/gPIAk2+uCPhzky0xvg1cdMqLKGuzQ8Z1E++/3SYBv3UMXhpAaBlTzoCwzTipfSVAkhZNw+l2FZiujNJzYzBWnIPXWZ4ZgFMDCm3NwHaR2Gyh8IyKpsLso8DsOoieJsec6zxd7evnzrTE9UV75lFnN7LbOxhjMdn42Ef14GvSEfc1yQlR23WfmMsCpfeAqBGLz2VrBIYsk94OIoRA57siY/mjSv3Xczns="));
			skin = skin25088;
			break;
		case CRUSADER:
			break;
		case ELECTRO:
			break;
		case WIZARD:
			break;
		case PALADIN:
			break;
		case PIGMAN:
			GameProfile skin6985 = new GameProfile(UUID.fromString("a2385439-4bf0-4cf3-a736-9a5ef7a6d660"), MessageUtil.color("&e&lPIGMAN"));
			skin6985.getProperties().put("textures", new Property(
			    "textures",
			    "eyJ0aW1lc3RhbXAiOjE0ODYzOTczNjg2MTQsInByb2ZpbGVJZCI6ImEyMzg1NDM5NGJmMDRjZjNhNzM2OWE1ZWY3YTZkNjYwIiwicHJvZmlsZU5hbWUiOiJQaWdtYW4iLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzJkYzkyMTlhM2I5N2RmYWQ5NGQ5NjQ0M2NkOGE3Zjk3NmFiMzBkZGYyZDkxMzFkOTVjODY0NTdiYzMyYTgifX19",
			    "q3WAM7S/IJSfYTO54ONAejFvY5cKyugoqVwhEEbAQZ0vOnbcKQqZaO2zE3VixL0gPa/7PpucZ30Dn3yGxAtgun7SgwR+zXmk1P3AN0rKIuvavr6C1qbRcMC3Uxk2/6lwGFJ7alF9Ceq7uRVmsDYKon10AmkPSLDy+OOfGWjLPWhL6O5qZwB0e5mwjxYZ2fxmOaTpiO8V9JaEI/fYFNjpowWcbHgYwb1jGcMVQwnwV0jkZVU8axZBwetrVxQadAfFJbMOqKh6l4VzlwA+FfVqYpBJNfrJmgngJImv8lfLSgwqKkdu5knCno14+plBlmVVfsdKGPnan/uC7slxNULqme3rVNXwFfoOJoeRdleErlY3Kn7mtHNIEfUtHvPvQrgOQMhR5JdQZZU3MQVLdEHQBuCpWrs1+N5jCZ/0Jm0bWk1UHSz00bZRfi3bnUhzAF/3dHKz4rK9c/xiXR8ASG5adl+ZzpIHTxHGn6C90iHrOy57+e/IZGdo6ki+z2DxNuzEtXN6fXD33gSZp/40l8M8s1IY9vQlDMgpwouGk+8+NAXZvyXmXOx2TxhLg1bf2y7U2w+bHtzn9IdO8tzGTMLKPdYKH7e0wCgI9jpXCnG1H75m4XfbVezz625Eif0rCK4q+V7E5qruqQIsJTYrpu5ALFs3n4MekFDbe2OztK2WAcg="));
			skin =  skin6985;
			break;
		case PIRATA:
			break;
		case PYROMANCER:
			break;
		case MAGMA:
			GameProfile skin159696 = new GameProfile(UUID.fromString("edebd288-a08d-41eb-aa64-b6d06d40913e"), "skin159696");
			skin159696.getProperties().put("textures", new Property(
			    "textures",
			    "eyJ0aW1lc3RhbXAiOjE1MjIzNjIxNzcyMDQsInByb2ZpbGVJZCI6IjkxOGEwMjk1NTlkZDRjZTZiMTZmN2E1ZDUzZWZiNDEyIiwicHJvZmlsZU5hbWUiOiJCZWV2ZWxvcGVyIiwic2lnbmF0dXJlUmVxdWlyZWQiOnRydWUsInRleHR1cmVzIjp7IlNLSU4iOnsidXJsIjoiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS82MzQ3YjNjZjcwZjZhMDg5ZmYzZTg2MWQxODE2MzM2NTI1Yjk5ZTYxODhmMGE4NjRiMmYyMWVjMmI1YjZlNTY5In19fQ==",
			    "AZzG0ABLicUlDVzRHUt0vFAotE5UOduAMW0n4KFOdAkbtwGljoUCeDdG0j+Sp1meLU9UvoM/n7j5pm7NzxRtvpBLxHnGbCoDSKs8O3rtn7rtHLA/sa0+mORkS4kcK6OgdNcU+Ov+/De16qQntH2oaHWsi/qGJ9vy2ZOHsjgqrc35EXq1H6iybVs8VY86yFO8ShgWl7iAFJl8A9WxjSff4jaXooibVZXlv5RIh/xHIN31jHnhDNuGCRwZ07GqYCHDu18JlVy7JrCbKniVexArGJAMZItUMmhWCX7ytb/IMnrGDm3NFBbCukDixpLsdd3MRrmwiF+8oeY22NFQbDzYeC9kIZc/BBjAlpDUJ1qngO6eRe5rYmkAoOab7Z/K4OOctdnVfDqgukRfB35AhV/ahvn9MtLTumAYxrlGf361e5vZkZ9W5UIGhuqGWCiutbkWb+6GD3xpI6/A6xcmmDAh+nWGRqguAVUDvUFio4B6XV0eAjYT/IBsgWGUT3qzsdBFvPRbi05Hn2ndE6wOzPwmstols1+774Bdsw8UGdUrVJKDFu+M8J8cFLqVlfZ488WnsLDWX87TgBBOOfLR+wEW5A47AifTIDrrG2rrv4b0HeA9YxwWgS6AG7DTc/YX5i0VeS4FY0VjQ2f7Nj++t2fkhr9dFtSxddjY2NSC4TLBygM="));
			            
			return skin159696;
		case SHADOW:
			break;
		case SHAMAN:
			GameProfile skin6079 = new GameProfile(UUID.fromString("d19e1ff1-628f-3fb8-8063-2cc0f8d1ddda"), MessageUtil.color("&e&lSHAMAN"));
			skin6079.getProperties().put("textures", new Property(
			    "textures",
			    "eyJ0aW1lc3RhbXAiOjE0ODQyNDI5MzkyMjUsInByb2ZpbGVJZCI6IjQzYTgzNzNkNjQyOTQ1MTBhOWFhYjMwZjViM2NlYmIzIiwicHJvZmlsZU5hbWUiOiJTa3VsbENsaWVudFNraW42Iiwic2lnbmF0dXJlUmVxdWlyZWQiOnRydWUsInRleHR1cmVzIjp7IlNLSU4iOnsidXJsIjoiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS8yNTViNjVlMzNjMDc1MzY5Nzc0YThiNmNkMTc2YTkyNjE1NWJiNzNkYjlhM2RkY2E5NThhOWU2ZGE1MWVhIn19fQ==",
			    "AJS1td31YnXep3M+9iMuACEXc7ovymce7Gj19lO0Je6zFlJXc8nEMd5C3nOEFEpGJC42AdEhVTJMwxILYFrhHiJ8lJhpruQ421G0t0q1b98Qj+enKEox69jf08ZS5Sh5ZqnySKY0LPUWPqDnTBuu9b1MEKyN+EtEi2vIuKhi4+Re41XAy2/vHDh0Y5D2EMs+Q6DgcZRRroDWMdEyM2h/o5AK0d2CxYjw4sHMfXmsoIwxY7kGvlhuynqQ3H4+B3wz9QPW7wKcpTjpurmimAza4Y9UwlQJyUJYnGSQMRvV5WwCJ1llrKz/Sg0Na1S9xCp/P63M6J2ZZAVD5rEXiwUpvG6Z7W9913Lurs6gM9j8i2MmSbmRja1fvie11e6Droa/libaexlc9IwlH/lfOw5O5jJVdwYuYrncad4PsyFPnLPJXk/F5F02LjmmDNPCLxFFiRJEb2XclvpiMyIWDWsdqnSR/s5qTjS6DxuZ6AYV4GYcVxSDJZXJnTTG6Cop/ZKKk+lSMrnnpXucRsrhvL5n8RP88VGtmZht9EkWfGT61AolfMsmG86writxrQ3+HoAFkCgY78ztpP4pi011YbtjloabS/hrQHCWCw3KpYt0r/2JuQ5YMb19AlO5XYjKyf8Nm45tcH6Tla0wUpTY285u4wfOLHDx5MGuQ2/RYecTCCI="));
			skin = skin6079;
			break;
		case SKELETON:
			GameProfile skin25087 = new GameProfile(UUID.fromString("b48d5633-7880-33e3-bffc-32b255aeb352"), MessageUtil.color("&e&lSKELETON"));
			skin25087.getProperties().put("textures", new Property(
			    "textures",
			    "eyJ0aW1lc3RhbXAiOjE1MDExMzU0MTI5NDgsInByb2ZpbGVJZCI6ImQxY2VjOWFkMWRhODQxNzliMWU1NjA0ZjcyYmZiMjI2IiwicHJvZmlsZU5hbWUiOiJydXRnZXI0NjUiLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlL2QzYmVkYzE4Y2Y2Y2QxNGM1MjhiMWZhNzliOWJlY2U5MmU2Y2JjMTBmNmNjZTg2NmU0NWE3ZDc4NjZjZGQ0ZiJ9fX0=",
			    "D7EbMulUI9WmRky2tYeoavrh7z2BcqypCK2aBISsX+2906oKVYMLDG/ghZAZsVO3wpOdf5tVFTKXVAYzhKAv1aVHrUdXGJhh9HQ4Slr9lZfNPDhgtC25vZWjS0VbyCbeb2hHv1lltYCVlwRdrWmctKQIKgMGN2D/0T3hGR6Z18zdH/JNxe3QaXfIBMP7Gi97NRBk45nTfiF7FM7Ki/69qn9xT0xuVqIOKhFG/JsS0Ek3dRPp1kVY4KGiYZX1hBZMFT8c/cWN89hDbqUkqzyJ4KyMzf4uNM14WnD7KriLKdB16F+XTvBqnUfrP2qzpQhmEfuKb3IYieebwCz9qVj0fhFYpKqGoWjaYW7terKEXh1SGeNr4c8QYEWcLPyQ5h8vF2q/9ex+kGztfeXAnHLuMEiiVq8tD5qmHbOr63ZZaXHyrOcCZJ4WpF/EsqxvDGtEAPzdxgfCslgC8L9zJWTAolzRJrtlly2vZKyKklv58a8/kjAq3qUBW0uveIeZJxfiq4tALcYysCTjOOESRT6eJyXsHSdj9lVNrM5HCjrH+UyWTbf2snIZ9i60upsgnlu+PxR4LyGWCA2IAI78GF2CTDx3p2oQRdiE0Stl9qkpb2grBAQz5vSFuhIoCtDkmtukU5egOzeQvrKTOy3YKz+zAQ1kjNFG978SDtjWBNsMbi4="));
			skin = skin25087;
			break;
		case SQUID:
			break;
		case ZOMBIE:
			break;
		default:
			break;
		}
		if(skin == null) {
			return new DefaultSkins().getType(c);
		}
		return skin;
	}

	@Override
	public String extractTexture(GameProfile g) {
		return g.getProperties().get("textures").iterator().next().getValue();
	}
}
