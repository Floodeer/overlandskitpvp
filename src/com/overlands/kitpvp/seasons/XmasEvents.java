package com.overlands.kitpvp.seasons;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.util.Vector;

import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.calc.particle.ParticleListen;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.core.utils.particles.ParticleEffect.OrdinaryColor;
import com.overlands.core.utils.scheduler.SchedulerEvent;
import com.overlands.core.utils.scheduler.UpdateType;
import com.overlands.core.utils.world.WorldUtils;
import com.overlands.kitpvp.Main;

public class XmasEvents implements Listener {
	
	public static boolean ENABLED = false;
	
	private static final Color LOG_COLOR = Color.fromRGB(101, 67, 33);
	private int pp = 12;
	
	private Location loc = null;
	
	public XmasEvents() {
		Bukkit.getPluginManager().registerEvents(this, Main.get());
		if(Bukkit.getWorlds().get(0) != null) {
			loc = new Location(Bukkit.getWorlds().get(0), 160.500, 106, 513.500);
		}
	}

	@EventHandler
	public void onScheduler(SchedulerEvent e) {
		if(e.getType() != UpdateType.TICKS_2) {
			return;
		}
		if(!ENABLED)
			return;
		
		drawLog();
		drawLeavesAndBalls();
		drawStar();
		drawSnow();
		
		if(Bukkit.getOnlinePlayers().size() > 0) {
			Bukkit.getOnlinePlayers().forEach(player -> {
				if(WorldUtils.inSpawn(player.getLocation())) {
					if(!ParticleListen.isMoving(player)) {
						Location location = player.getEyeLocation().add(0, 0.3, 0);
						float radius = 0.25f;
						drawHat(radius + 0.1f, -0.05f, location, false);
						for (int i = 0; i < 5; i++) {
							double x = MathUtils.randomDouble(-0.05, 0.05);
							double z = MathUtils.randomDouble(-0.05, 0.05);
							location.add(x, 0.46f, z);
							ParticleEffect.REDSTONE.display(new OrdinaryColor(255, 255, 255),  location, 60);
							location.subtract(x, 0.46f, z);
						}
						for (float f = 0; f <= 0.4f; f += 0.1f) {
							if (radius >= 0) {
								drawHat(radius, f, location, true);
								radius -= 0.09f;
							}
						}
					}
				}
			});
		}
	}
	
	private void drawHat(float radius, float height, Location location, boolean red) {
		for (int i = 0; i < pp; i++) {
			double inc = (2 * Math.PI) / pp;
			float angle = (float) (i * inc);
			float x = (float) Math.cos(angle) * radius;
			float z = (float) Math.sin(angle) * radius;
			location.add(x, height, z);
			ParticleEffect.REDSTONE.display(new OrdinaryColor(255, red ? 0 : 255, red ? 0 : 255),  location, 60);
			location.subtract(x, height, z);
		}
	}
	
	private void drawSnow() {
		loc.add(0, 3, 0);
		ParticleEffect.FIREWORKS_SPARK.display(4, 3, 4, 0, 1, loc, 60);
		loc.subtract(0, 3, 0);
	}
	
	private void drawLog() {
		Location current = loc.clone();
		Location to = loc.clone().add(0, 3.0, 0);
		Vector link = to.toVector().subtract(current.toVector());
		float length = (float) link.length();
		link.normalize();
		float ratio = length / 10;
		Vector vector = link.multiply(ratio);
		for (int i = 0; i < 10; i++) {
			ParticleEffect.REDSTONE.display(new OrdinaryColor(LOG_COLOR.getRed(), LOG_COLOR.getGreen(), LOG_COLOR.getBlue()), current, 60);
			current.add(vector);
		}
	}
	
	private void drawLeavesAndBalls() {
		float radius = 0.7f;
		for (float f = 0.8f; f <= 3.0f; f += 0.2f) {
			if (radius >= 0) {
				float d = 13f / f;
				float g = MathUtils.random(0, d);
				int e = MathUtils.random(0, 2);
				if (e == 1) {
					double inc = (2 * Math.PI) / d;
					float angle = (float) (g * inc);
					float x = MathUtils.cos(angle) * (radius + 0.05f);
					float z = MathUtils.sin(angle) * (radius + 0.05f);
					loc.add(x, f, z);
					ParticleEffect.REDSTONE.display(new OrdinaryColor(MathUtils.random(255), MathUtils.random(255), MathUtils.random(255)), loc, 60);
					loc.subtract(x, f, z);
				}
				for (int i = 0; i < d; i++) {
					double inc = (2 * Math.PI) / d;
					float angle = (float) (i * inc);
					float x = MathUtils.cos(angle) * radius;
					float z = MathUtils.sin(angle) * radius;
					loc.add(x, f, z);
					ParticleEffect.REDSTONE.display(new OrdinaryColor(0, 100, 0), loc, 60);
					loc.subtract(x, f, z);
				}
				radius = radius - (0.7f / 8.5f);
			}
		}
	}
	
	private void drawStar() {
		loc.add(0, 3.1, 0);
		ParticleEffect.REDSTONE.display(new OrdinaryColor(255, 255, 0), loc, 60);
		loc.subtract(0, 3.1, 0);
	}
}
