package com.overlands.kitpvp.database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;

import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.database.interfaces.IDatabase;

public class SQLite implements IDatabase {

	private AtomicReference<Connection> connection;
	private ExecutorService pool;

	public SQLite() throws ClassNotFoundException, SQLException {
		try {
			pool = Executors.newCachedThreadPool();
			connection = new AtomicReference<Connection>();
			Class.forName("org.sqlite.JDBC");
			connect();
		} catch (SQLException ex) {
			close();
			ex.printStackTrace();
		}
	}

	@Override
	public void connect() throws SQLException {
		if (connection.get() != null) {
			try {
				connection.get().createStatement().execute("SELECT 1;");
			} catch (SQLException sqlException) {
				if (sqlException.getSQLState().equals("08S01")) {
					try {
						connection.get().close();
					} catch (SQLException ignored) {
					}
				}
			}
		}
		if (connection.get() == null || connection.get().isClosed()) {
			connection.set(DriverManager.getConnection("jdbc:sqlite:" + Main.get().getDataFolder() + "/players.db"));
		}
	}

	@Override
	public void close() {
		try {
			if (connection.get() != null && !connection.get().isClosed()) {
				connection.get().close();
			}

		} catch (SQLException ignored) {

		}
		connection.set(null);
	}

	@Override
	public AtomicReference<Connection> getConnection() {
		return connection;
	}
	
	@Override
	public Executor getExecutor() {
		return pool;
	}

	@Override
	public boolean checkConnection() {
		try {
			connect();
		} catch (SQLException ex) {
			close();
			ex.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public void createTables() throws IOException, SQLException {
		Statement statement = null;
		try {
			statement = getConnection().get().createStatement();
			
			StringBuilder query = new StringBuilder();
			
			Main.get().getLogger().info("Query cleared, loading/creating table `player_info`...");
			
			query.append("CREATE TABLE IF NOT EXISTS `player_info` ( ");
			query.append("`id` INTEGER PRIMARY KEY AUTOINCREMENT, ");
			query.append("`uuid`  VARCHAR(100)     NOT NULL, ");
			query.append("`playername`  VARCHAR(40)     NOT NULL, ");
			query.append("`rank`  VARCHAR(8)     NOT NULL NULL DEFAULT 'Default', ");
			query.append("`password` VARCHAR(1000) NOT NULL DEFAULT '', ");
			query.append("`first_login` DATETIME NOT NULL, ");
			query.append("`last_login` DATETIME NOT NULL, ");
			query.append("`protocolVersion`  INT(1) NOT NULL DEFAULT 47, ");
			query.append("`muted` BOOLEAN NOT NULL DEFAULT 0, ");
			query.append("`muteReason` VARCHAR(1000) NOT NULL DEFAULT '', ");
			query.append("`banned` BOOLEAN NOT NULL DEFAULT 0, ");
			query.append("`banReason` VARCHAR(1000) NOT NULL DEFAULT '', ");
			query.append("`banEnd` INTEGER NOT NULL DEFAULT -1);");
			statement.execute(query.toString());
			
			Main.get().getLogger().info("Query cleared, loading/creating table `player_data`...");
			query.setLength(0);
			
			query.append("CREATE TABLE IF NOT EXISTS `player_profile` ( ");
			query.append("`id` INTEGER PRIMARY KEY AUTOINCREMENT, ");
			query.append("`uuid`  VARCHAR(100)     NOT NULL, ");
			query.append("`playername`  VARCHAR(40)     NOT NULL, ");
			query.append("`titles` BOOLEAN NOT NULL DEFAULT 1, ");
			query.append("`chatToggled` BOOLEAN NOT NULL DEFAULT 0, ");
			query.append("`specialEffects` BOOLEAN NOT NULL DEFAULT 1, ");
			query.append("`warnings` BOOLEAN NOT NULL DEFAULT 1, ");
			query.append("`fastRespawn` BOOLEAN NOT NULL DEFAULT 0, ");
			query.append("`gameQuality` int NOT NULL DEFAULT 3, ");
			query.append("`private_profile` BOOLEAN NOT NULL DEFAULT 0, ");
			query.append("`pmEnabled` BOOLEAN NOT NULL DEFAULT 1, ");
			query.append("`respawn_with_kit` BOOLEAN NOT NULL DEFAULT 0);");
			statement.execute(query.toString());
				
			Main.get().getLogger().info("Statement executed, loading/creating table `player_data`...");
			query.setLength(0);
			
			query.append("CREATE TABLE IF NOT EXISTS `player_data` ( ");
			query.append("`id` INTEGER PRIMARY KEY AUTOINCREMENT, ");
			query.append("`uuid`  VARCHAR(100)     NOT NULL, ");
			query.append("`playername`  VARCHAR(40)     NOT NULL, ");
			query.append("`classes`  VARCHAR(9999)     NOT NULL DEFAULT 'HEROBRINE, CREEPER', ");
			query.append("`perks`  VARCHAR(255)     NOT NULL DEFAULT '', ");
			query.append("`selectedPerks`  VARCHAR(255)     NOT NULL DEFAULT '', ");
			query.append("`effects`  VARCHAR(255)     NOT NULL DEFAULT '', ");
			query.append("`selectedEffects`  VARCHAR(255)     NOT NULL DEFAULT '', ");
			query.append("`achievements`  VARCHAR(9999)     NOT NULL DEFAULT '', ");
			query.append("`gameClass`  VARCHAR(60)     NOT NULL DEFAULT 'HEROBRINE', ");
			query.append("`balance` INT(6) NOT NULL DEFAULT 0, ");
			query.append("`exp` INT(6) NOT NULL DEFAULT 0, ");
			query.append("`level` INT(6) NOT NULL DEFAULT 1, ");
			query.append("`general_level` INT(6) NOT NULL DEFAULT 1, ");
			query.append("`prestigeLevel` INT(6) NOT NULL DEFAULT 0, ");
			query.append("`kills` INT(6) NOT NULL DEFAULT 0, ");
			query.append("`deaths` INT(6) NOT NULL DEFAULT 0, ");		
			query.append("`arrowsShot` INT(6) NOT NULL DEFAULT 0, ");
			query.append("`arrowsHit` INT(6) NOT NULL DEFAULT 0, ");
			query.append("`skillsUsed` INT(6) NOT NULL DEFAULT 0, ");
			query.append("`score` INT(6) NOT NULL DEFAULT 0, ");
			query.append("`healingDone` DOUBLE NOT NULL DEFAULT 0, ");
			query.append("`damageDone` DOUBLE NOT NULL DEFAULT 0, ");
			query.append("`healingReceived` DOUBLE NOT NULL DEFAULT 0, ");
			query.append("`damageReceived` DOUBLE NOT NULL DEFAULT 0, ");
			query.append("`timePlayed` BIGINT(20) NOT NULL DEFAULT 0, ");
			query.append("`mysteryBoxes` int NOT NULL DEFAULT 0);");
			
			statement.execute(query.toString());

			Main.get().getLogger().info("Statement executed, loading/creating table `player_contracts`...");
			query.setLength(0);




		} finally {
			connection.get().setAutoCommit(true);
			if (statement != null) {
				statement.close();
			}
			
			Main.get().getLogger().info("SQLite finished the creating table process.");
		}
	}
	
	@Override
	public boolean columnExists(String table, String column) {
		Connection conn = getConnection().get();
		DatabaseMetaData metadata;
		try {
			metadata = conn.getMetaData();
			ResultSet resultSet;
			resultSet = metadata.getTables(null, null, table, null);
			if (resultSet.next()) {
				resultSet = metadata.getColumns(null, null, table, column);
				if (!resultSet.next()) {
					return false;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	@Override
	public void addColumn(String table, String value, String after) throws IOException, SQLException {
		Statement statement = null;
		try {
			connection.get().setAutoCommit(false);
			statement = connection.get().createStatement();

			String query = "ALTER TABLE " + table + " ADD " + value + " AFTER " + after;
			statement.execute(query);
			connection.get().commit();

		} finally {
			connection.get().setAutoCommit(true);
			if (statement != null) {
				statement.close();
			}
		}
	}
}
