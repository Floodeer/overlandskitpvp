package com.overlands.kitpvp.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

import com.overlands.kitpvp.database.util.ResultSetDeserializer;
import com.overlands.kitpvp.database.util.SQLOperation;
import com.overlands.kitpvp.database.util.StatementSerializer;
import org.bukkit.Bukkit;

import com.overlands.core.utils.java.FinalReference;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.database.interfaces.Callback;
import com.overlands.kitpvp.database.interfaces.IDatabase;
import com.overlands.kitpvp.game.GamePlayer;

public class DataManager {

	private IDatabase database;

	public DataManager(IDatabase db) {
		this.database = db;
	}
	
	public void doesPlayerExists(String name, Callback<Boolean> result) {
		if(!database.checkConnection())
			return;
		
		database.getExecutor().execute(new Runnable() {
			PreparedStatement preparedStatement = null;
			ResultSet resultSet = null;
			@Override
			public void run() {
				String query = "SELECT Count(`id`) FROM `player_info` WHERE `playername` = ? LIMIT 1;"; 
				try {
					preparedStatement = database.getConnection().get().prepareStatement(query);
					preparedStatement.setString(1, name);
					resultSet = preparedStatement.executeQuery();
					if (resultSet.next()) {
						if(resultSet.getInt(1) > 0) 
							result.onCall(true);
						else
							result.onCall(false);
					}
				}catch(SQLException ex) {
					ex.printStackTrace();
				}finally {
					try {
						if(preparedStatement != null)
							preparedStatement.close();
						if(resultSet != null)
							resultSet.close();
					}catch(final SQLException ex) {}
				}
			}
		});
	}
	
	public void doesPlayerExists(UUID uuid, Callback<Boolean> result) {
		if(!database.checkConnection())
			return;
		
		database.getExecutor().execute(new Runnable() {
			PreparedStatement preparedStatement = null;
			ResultSet resultSet = null;
			@Override
			public void run() {
				String query = "SELECT Count(`id`) FROM `player_info` WHERE `uuid` = ? LIMIT 1;"; 
				try {
					preparedStatement = database.getConnection().get().prepareStatement(query);
					preparedStatement.setString(1, uuid.toString());
					resultSet = preparedStatement.executeQuery();
					if (resultSet.next()) {
						if(resultSet.getInt(1) > 0) 
							result.onCall(true);
						else
							result.onCall(false);
					}
				}catch(SQLException ex) {
					ex.printStackTrace();
				}finally {
					try {
						if(preparedStatement != null)
							preparedStatement.close();
						if(resultSet != null)
							resultSet.close();
					}catch(final SQLException ignored) {}
				}
			}
		});
	}
	
	public void findPlayerId(String playerOrUid, Callback<Integer> callback) {
		Bukkit.getScheduler().runTaskAsynchronously(Main.get(), () -> {
			PreparedStatement preparedStatement = null;
			FinalReference<ResultSet> ref = new FinalReference<>(null);
			boolean uuid = false;
			if (playerOrUid.matches("[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}")) {
				uuid = true;
			}
			try {
				String query = "SELECT Count(`id`) FROM `player_info` WHERE " + (uuid ? "`uuid` =" : "`playername` = ") + playerOrUid + " LIMIT 1;";
				preparedStatement = database.getConnection().get().prepareStatement(query);
				ref.setAndGet(preparedStatement.executeQuery());
				try {
					if (ref.get().next()) {
						callback.onCall(ref.get().getInt("id"));
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			} finally {
				try {
					if (preparedStatement != null)
						preparedStatement.close();
					if (ref.get() != null)
						ref.get().close();

				} catch (SQLException ex) {
				}
			}
		});
	}

	public int savePlayerAsync(GamePlayer player, TABLE_TYPE table) {
		
		AtomicInteger operationResult = new AtomicInteger(0);
		if(!database.checkConnection())
			return operationResult.get();
		
		String uuid = player.getUUID().toString();
		
		new SQLOperation() {
			PreparedStatement preparedStatement = null;
			@Override
			public void onExecute() throws SQLException {
				StringBuilder query = new StringBuilder();	
				if(table == TABLE_TYPE.PLAYER_INFO) {
					query.append("UPDATE `player_info` SET ");
					query.append("`playername` = ?, `rank` = ?, `last_login` = DateTime('now'), `protocolVersion` = ?, `muted` = ?, `muteReason` = ?, ");
					query.append("`banned` = ?, `banReason` = ?, `banEnd` = ?");
					query.append("WHERE `uuid` = ?;");
					
					try {
						preparedStatement = database.getConnection().get().prepareStatement(query.toString());
						preparedStatement.setString(1, player.getPlayer().getName());
						preparedStatement.setString(2, player.getRank());
						preparedStatement.setInt(3, player.getProtocolVersion());
						preparedStatement.setBoolean(4, player.isMuted());
						preparedStatement.setString(5, player.getMuteReason());
						preparedStatement.setBoolean(6, player.isBanned());
						preparedStatement.setString(7, player.getBanReason());
						preparedStatement.setLong(8, player.getBanEnd());
						preparedStatement.setString(9, player.getUUID().toString());
						operationResult.set(preparedStatement.executeUpdate());
					}finally {
						if(preparedStatement != null)
							preparedStatement.close();
					}
				}else if(table == TABLE_TYPE.PLAYER_PROFILE) {
					query.append("UPDATE `player_profile` SET ");
					query.append("`playername` = ?, `titles` = ?, `chatToggled` = ?, `specialEffects` = ?, `warnings` = ?, ");
					query.append("`fastRespawn` = ?, `respawn_with_kit` = ?, `gameQuality` = ?, `private_profile` = ?, ");
					query.append("`pmEnabled` = ? ");
					query.append("WHERE `uuid` = ?;");
					
					try {
						preparedStatement = database.getConnection().get().prepareStatement(query.toString());
						preparedStatement.setString(1, player.getPlayer().getName());
						preparedStatement.setBoolean(2, player.hasTitles());
						preparedStatement.setBoolean(3, player.isChatToggled());
						preparedStatement.setBoolean(4, player.isSpecialEffects());
						preparedStatement.setBoolean(5, player.isWarnings());
						preparedStatement.setBoolean(6, player.isFastRespawn());
						preparedStatement.setBoolean(7, player.respawnWithKit());
						preparedStatement.setInt(8, player.getParticleQuality());
						preparedStatement.setBoolean(9, player.hasPrivateProfile());
						preparedStatement.setBoolean(10, player.isPMEnabled());
						preparedStatement.setString(11, player.getUUID().toString());
						operationResult.set(preparedStatement.executeUpdate());
					}finally {
						if(preparedStatement != null)
							preparedStatement.close();
					}
					
				}else if(table == TABLE_TYPE.PLAYER_DATA) {
					query.append("UPDATE `player_data` SET ");
					query.append("`classes` = ?, `perks` = ?, `achievements` = ?, `gameClass` = ?, `balance` = ?, `exp` = ?, ");
					query.append("`level` = ?, `prestigeLevel` = ?, `kills` = ?, `deaths` = ?, `arrowsShot` = ?, `arrowsHit` = ?, ");
					query.append("`skillsUsed` = ?, `score` = ?, `healingDone` = ?, `damageDone` = ?, `healingReceived` = ?, ");
					query.append("`damageReceived` = ?, `timePlayed` = ?, `mysteryboxes` = ?, `effects` = ?, `general_level` = ?, ");
					query.append("`selectedPerks` = ?, `selectedEffects` = ?, `playername` = ? ");
					query.append("WHERE `uuid` = ?;");
					try {
						preparedStatement = database.getConnection().get().prepareStatement(query.toString());
						preparedStatement.setString(1, new StatementSerializer(player.getKits()).execute().getDecodedString());
						preparedStatement.setString(2, new StatementSerializer(player.getPerks()).execute().getDecodedString());
						preparedStatement.setString(3, new StatementSerializer(player.getAchievements()).execute().getDecodedString());
						preparedStatement.setString(4, player.getClassTypeFromEnum().toString().toUpperCase());
						preparedStatement.setDouble(5, player.getCoins());
						preparedStatement.setInt(6, player.getExp());
						preparedStatement.setInt(7, player.getLevel());
						preparedStatement.setInt(8, player.getPrestigeLevel());
						preparedStatement.setInt(9, player.getKills());
						preparedStatement.setInt(10, player.getDeaths());
						preparedStatement.setInt(11, player.getBowShots());
						preparedStatement.setInt(12, player.getBowHits());
						preparedStatement.setInt(13, player.getSkillUsed());
						preparedStatement.setInt(14, player.getScore());
						preparedStatement.setDouble(15, player.getHealingDone());
						preparedStatement.setDouble(16, player.getDamageDone());
						preparedStatement.setDouble(17, player.getHealingReceived());
						preparedStatement.setDouble(18, player.getDamageReceived());
						preparedStatement.setLong(19, player.getTimePlayed());
						preparedStatement.setLong(20, player.getMysteryBoxes());
						preparedStatement.setString(21, new StatementSerializer(player.getEffects()).execute().getDecodedString());
						preparedStatement.setInt(22, player.getGeneralLevel());
						preparedStatement.setString(23, new StatementSerializer(player.getSelectedPerks()).execute().getDecodedString());
						preparedStatement.setString(24, new StatementSerializer(player.getSelectedEffects()).execute().getDecodedString());
						preparedStatement.setString(25, player.getPlayer().getName());
						preparedStatement.setString(26, uuid);
						
						operationResult.set(preparedStatement.executeUpdate());
						
					}finally {
						if(preparedStatement != null)
							preparedStatement.close();
					}
				}
			}
			
		}.writeOperation(database.getExecutor(), Logger.getLogger("Minecraft"), "Erro ao salvar data uuid = " + player.getPlayer().getName() + ".");
		
		return operationResult.get();
	}
	
	public void savePlayerAsync(GamePlayer player) {
		Arrays.stream(TABLE_TYPE.values()).forEachOrdered(table -> savePlayerAsync(player, table));
	}
	
	public void savePlayer(GamePlayer player) {
		if(!database.checkConnection())
			return;

		PreparedStatement preparedStatementInfo = null;
		PreparedStatement preparedStatementData = null;
		PreparedStatement preparedStatementProfile = null;
		String uuid = player.getUUID().toString();
		StringBuilder query = new StringBuilder();	
		try {
			query.append("UPDATE `player_info` SET ");
			query.append("`playername` = ?, `rank` = ?, `last_login` = DateTime('now'), `protocolVersion` = ?, `muted` = ?, `muteReason` = ?, ");
			query.append("`banned` = ?, `banReason` = ?, `banEnd` = ? ");
			query.append("WHERE `uuid` = ?;");
			
			preparedStatementInfo = database.getConnection().get().prepareStatement(query.toString());
			preparedStatementInfo.setString(1, player.getPlayer().getName());
			preparedStatementInfo.setString(2, player.getRank());
			preparedStatementInfo.setInt(3, player.getProtocolVersion());
			preparedStatementInfo.setBoolean(4, player.isMuted());
			preparedStatementInfo.setString(5, player.getMuteReason());
			preparedStatementInfo.setBoolean(6, player.isBanned());
			preparedStatementInfo.setString(7, player.getBanReason());
			preparedStatementInfo.setLong(8, player.getBanEnd());
			preparedStatementInfo.setString(9, player.getUUID().toString());
			
			query.setLength(0);
			
			query.append("UPDATE `player_data` SET ");
			query.append("`classes` = ?, `perks` = ?, `achievements` = ?, `gameClass` = ?, `balance` = ?, `exp` = ?, ");
			query.append("`level` = ?, `prestigeLevel` = ?, `kills` = ?, `deaths` = ?, `arrowsShot` = ?, `arrowsHit` = ?, ");
			query.append("`skillsUsed` = ?, `score` = ?, `healingDone` = ?, `damageDone` = ?, `healingReceived` = ?, ");
			query.append("`damageReceived` = ?, `timePlayed` = ?, `mysteryboxes` = ?, `effects` = ?, `general_level` = ?, ");
			query.append("`selectedPerks` = ?, `selectedEffects` = ?, `playername` = ? ");
			query.append("WHERE `uuid` = ?;");

			preparedStatementData = database.getConnection().get().prepareStatement(query.toString());
			preparedStatementData.setString(1, new StatementSerializer(player.getKits()).execute().getDecodedString());
			preparedStatementData.setString(2, new StatementSerializer(player.getPerks()).execute().getDecodedString());
			preparedStatementData.setString(3, new StatementSerializer(player.getAchievements()).execute().getDecodedString());
			preparedStatementData.setString(4, player.getClassTypeFromEnum().toString().toUpperCase());
			preparedStatementData.setDouble(5, player.getCoins());
			preparedStatementData.setInt(6, player.getExp());
			preparedStatementData.setInt(7, player.getLevel());
			preparedStatementData.setInt(8, player.getPrestigeLevel());
			preparedStatementData.setInt(9, player.getKills());
			preparedStatementData.setInt(10, player.getDeaths());
			preparedStatementData.setInt(11, player.getBowShots());
			preparedStatementData.setInt(12, player.getBowHits());
			preparedStatementData.setInt(13, player.getSkillUsed());
			preparedStatementData.setInt(14, player.getScore());
			preparedStatementData.setDouble(15, player.getHealingDone());
			preparedStatementData.setDouble(16, player.getDamageDone());
			preparedStatementData.setDouble(17, player.getHealingReceived());
			preparedStatementData.setDouble(18, player.getDamageReceived());
			preparedStatementData.setLong(19, player.getTimePlayed());
			preparedStatementData.setLong(20, player.getMysteryBoxes());
			preparedStatementData.setString(21, new StatementSerializer(player.getEffects()).execute().getDecodedString());
			preparedStatementData.setInt(22, player.getGeneralLevel());
			preparedStatementData.setString(23, new StatementSerializer(player.getSelectedPerks()).execute().getDecodedString());
			preparedStatementData.setString(24, new StatementSerializer(player.getSelectedEffects()).execute().getDecodedString());
			preparedStatementData.setString(25, player.getPlayer().getName());
			preparedStatementData.setString(26, uuid);
			
			query.setLength(0);
			
			query.append("UPDATE `player_profile` SET ");
			query.append("`playername` = ?, `titles` = ?, `chatToggled` = ?, `specialEffects` = ?, `warnings` = ?, ");
			query.append("`fastRespawn` = ?, `respawn_with_kit` = ?, `gameQuality` = ?, `private_profile` = ?, ");
			query.append("`pmEnabled` = ? ");
			query.append("WHERE `uuid` = ?;");
			
			preparedStatementProfile = database.getConnection().get().prepareStatement(query.toString());
			preparedStatementProfile.setString(1, player.getPlayer().getName());
			preparedStatementProfile.setBoolean(2, player.hasTitles());
			preparedStatementProfile.setBoolean(3, player.isChatToggled());
			preparedStatementProfile.setBoolean(4, player.isSpecialEffects());
			preparedStatementProfile.setBoolean(5, player.isWarnings());
			preparedStatementProfile.setBoolean(6, player.isFastRespawn());
			preparedStatementProfile.setBoolean(7, player.respawnWithKit());
			preparedStatementProfile.setInt(8, player.getParticleQuality());
			preparedStatementProfile.setBoolean(9, player.hasPrivateProfile());
			preparedStatementProfile.setBoolean(10, player.isPMEnabled());
			preparedStatementProfile.setString(11, player.getUUID().toString());

			preparedStatementData.executeUpdate();
			preparedStatementInfo.executeUpdate();
			preparedStatementProfile.executeUpdate();
			
		}catch(SQLException ex) {
			ex.printStackTrace();
		}finally {
			if(preparedStatementData != null)
				try {
					preparedStatementData.close();
				} catch (SQLException e) {
					e.printStackTrace();
			 }
			if(preparedStatementInfo != null)
				try {
					preparedStatementInfo.close();
				} catch (SQLException e) {
					e.printStackTrace();
			 }
			if(preparedStatementProfile != null)
				try {
					preparedStatementProfile.close();
				} catch (SQLException e) {
					e.printStackTrace();
			 }
		}
	}

	
	/*
	 * Loads GamePlayer, the Client is loaded before the JoinEvent.
	 * 
	 * String uuid - Player's uuid to load (After handshake; Offline UUID is not player's UUID).
	 * TABLE_TYPE type - The table to load.
	 * 
	 */
	public void loadPlayer(String uuid, TABLE_TYPE type) {
		GamePlayer player = GamePlayer.get(UUID.fromString(uuid));
		if(!database.checkConnection())
			return;
		
		new SQLOperation() {
			PreparedStatement preparedStatement = null;
			ResultSet resultSet = null;
			@Override
			public void onExecute() throws SQLException {
				if(!doesPlayerExists(UUID.fromString(uuid)))
					insertPlayer(UUID.fromString(uuid));
				
				StringBuilder query = new StringBuilder();
				
				if(type == TABLE_TYPE.PLAYER_INFO) {
					query.setLength(0);
					query.append("SELECT `rank`, `protocolVersion`, `muted`, `muteReason`, `banned`, `banReason`, `banEnd` ");
					query.append("FROM `player_info` ");
					query.append("WHERE `uuid` = ? ");
	                query.append("LIMIT 1;");
	                try {
	                	preparedStatement = database.getConnection().get().prepareStatement(query.toString());
						preparedStatement.setString(1, uuid);
						resultSet = preparedStatement.executeQuery();
						
						if(resultSet != null && resultSet.next()) {
							player.setRank(resultSet.getString("rank"));
							player.setProtocolVersion(resultSet.getInt("protocolVersion"));
							player.setMuted(resultSet.getBoolean("muted"));
							player.setBanned(resultSet.getBoolean("banned"));
							player.setBanReason(resultSet.getString("banReason"));
							player.setMuteReason(resultSet.getString("muteReason"));
							player.setBanEnd(resultSet.getLong("banEnd"));
							player.getLoadedTables().add(type);
						}
	                }finally {
	                	if(resultSet != null)
	                		resultSet.close();
	                	if(preparedStatement != null)
	                		preparedStatement.close();
	                }
				}else if(type == TABLE_TYPE.PLAYER_PROFILE) {
					query.setLength(0);
					query.append("SELECT `titles`, `chatToggled`, ");
					query.append("`specialEffects`, `warnings`, `fastRespawn`, `respawn_with_kit`, ");
					query.append("`gameQuality`, `private_profile`, ");
					query.append("`pmEnabled` ");
					query.append("FROM `player_profile` ");
					query.append("WHERE `uuid` = ? ");
	                query.append("LIMIT 1;");
	                
	                try {
	                	preparedStatement = database.getConnection().get().prepareStatement(query.toString());
						preparedStatement.setString(1, uuid);
						resultSet = preparedStatement.executeQuery();
						
	                	if(resultSet != null && resultSet.next()) {
		                	player.setTitles(resultSet.getBoolean("titles"));
		                	player.setChatToggled(resultSet.getBoolean("chatToggled"));
		                	player.setSpecialEffects(resultSet.getBoolean("specialEffects"));
		                	player.setWarnings(resultSet.getBoolean("warnings"));
		                	player.setFastRespawn(resultSet.getBoolean("fastRespawn"));
		                	player.setRespawnWithKit(resultSet.getBoolean("respawn_with_kit"));
							player.setParticleQuality(Math.min(resultSet.getInt("gameQuality"), 3));
		                	player.setPrivateProfile(resultSet.getBoolean("private_profile"));
		                	player.setPMEnabled(resultSet.getBoolean("pmEnabled"));
		                	player.getLoadedTables().add(type);
	                	}
	                }finally {
	                	if(resultSet != null)
	                		resultSet.close();
	                	if(preparedStatement != null)
	                		preparedStatement.close();
	                }
				}else if(type == TABLE_TYPE.PLAYER_DATA) {
					query.setLength(0);
					query.append("SELECT `classes`, `perks`, `achievements`, `gameClass`, `balance`, `exp`, ");
					query.append("`level`, `prestigeLevel`, `kills`, `deaths`, `arrowsShot`, `arrowsHit`, ");
					query.append("`skillsUsed`, `score`, `healingDone`, `damageDone`, `healingReceived`, ");
					query.append("`damageReceived`, `timePlayed`,  ");
					query.append("`mysteryBoxes`, `effects`, `general_level`, ");
					query.append("`selectedPerks`, `selectedEffects` ");
					query.append("FROM `player_data` ");
					query.append("WHERE `uuid` = ? ");
	                query.append("LIMIT 1;");
	                
	                try {
	                	preparedStatement = database.getConnection().get().prepareStatement(query.toString());
	 					preparedStatement.setString(1, uuid);
	 					resultSet = preparedStatement.executeQuery();
	 					
	 					if(resultSet != null && resultSet.next()) {
	 						player.setKits(new ResultSetDeserializer(resultSet.getString("classes")).execute().toList());
	 						player.setPerks(new ResultSetDeserializer(resultSet.getString("perks")).execute().toList());
	 						player.setEffects(new ResultSetDeserializer(resultSet.getString("effects")).execute().toList());
	 						player.setSelectedEffects(new ResultSetDeserializer(resultSet.getString("selectedEffects")).execute().toList());
	 						player.setSelectedPerks(new ResultSetDeserializer(resultSet.getString("selectedPerks")).execute().toList());
	 						player.setMysteryBoxes(resultSet.getInt("mysteryBoxes"));
	 						player.setAchievements(new ResultSetDeserializer(resultSet.getString("achievements")).execute().toList());
	 						player.setClassType(resultSet.getString("gameClass"));
	 						player.setCoins(resultSet.getInt("balance"));
	 						player.setExp(resultSet.getInt("exp"));
	 						player.setLevel(resultSet.getInt("level"));
	 						player.setPrestigeLevel(resultSet.getInt("prestigeLevel"));
	 						player.setKills(resultSet.getInt("kills"));
	 						player.setDeaths(resultSet.getInt("deaths"));
	 						player.setBowShots(resultSet.getInt("arrowsShot"));
	 						player.setBowHits(resultSet.getInt("arrowsHit"));
	 						player.setSkillUsed(resultSet.getInt("skillsUsed"));
	 						player.setScore(resultSet.getInt("score"));
	 						player.setHealingDone(resultSet.getDouble("healingDone"));
	 						player.setHealingReceived(resultSet.getDouble("healingReceived"));
	 						player.setDamageDone(resultSet.getDouble("damageDone"));
	 						player.setDamageReceived(resultSet.getDouble("damageReceived"));
	 						player.setTimePlayed(resultSet.getLong("timePlayed"));
	 						player.setGeneralLevel(resultSet.getInt("general_level"));
	 						player.getLoadedTables().add(type);
	 					}
	                }finally {
	                	if(resultSet != null)
	                		resultSet.close();
	                	if(preparedStatement != null)
	                		preparedStatement.close();
	                }  
				}
			}
		}.writeOperation(database.getExecutor(), Logger.getLogger("Minecraft"), "Erro ao carregar data uuid = " + uuid + ".");
	}
	
	public void insertPlayer(UUID uuid) {
		if(!database.checkConnection())
			return;
		
		PreparedStatement preparedStatementHunter = null;
		PreparedStatement preparedStatementData = null;
		PreparedStatement preparedStatementProfile = null;
		try {
			preparedStatementHunter = database.getConnection().get().prepareStatement("INSERT INTO `hunter_player` (`id`, `uuid`, `playername`) VALUES (NULL, ?, ?);");
			preparedStatementHunter.setString(1, uuid.toString());
			preparedStatementHunter.setString(2, Bukkit.getServer().getPlayer(uuid).getName());
			preparedStatementHunter.executeUpdate();
			
			preparedStatementData = database.getConnection().get().prepareStatement("INSERT INTO `player_data` (`id`, `uuid`, `playername`) VALUES (NULL, ?, ?);");
			preparedStatementData.setString(1, uuid.toString());
			preparedStatementData.setString(2, Bukkit.getServer().getPlayer(uuid).getName());
			preparedStatementData.executeUpdate();
			
			preparedStatementProfile = database.getConnection().get().prepareStatement("INSERT INTO `player_profile` (`id`, `uuid`, `playername`) VALUES (NULL, ?, ?);");
			preparedStatementProfile.setString(1, uuid.toString());
			preparedStatementProfile.setString(2, Bukkit.getServer().getPlayer(uuid).getName());
			preparedStatementProfile.executeUpdate();
		}catch(SQLException ex) {
			ex.printStackTrace();
		}finally {
			try {
				if(preparedStatementData != null)
					preparedStatementData.close();
				if(preparedStatementProfile != null)
					preparedStatementProfile.close();
			}catch(final SQLException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public boolean doesPlayerExists(UUID uuid) {
		if(!database.checkConnection())
			return false;
		int count = 0;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			String query = "SELECT Count(`id`) FROM `player_data` WHERE `uuid` = ? LIMIT 1;"; 
			preparedStatement = database.getConnection().get().prepareStatement(query);
			preparedStatement.setString(1, uuid.toString());
			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				count = resultSet.getInt(1);
			}
		}catch(SQLException ex) {
			ex.printStackTrace();
		}finally {
			try {
				if(preparedStatement != null)
					preparedStatement.close();
				if(resultSet != null)
					resultSet.close();
			}catch(final SQLException ex) {
				ex.printStackTrace();
			}
		}
		return count > 0;
	}
	
	public enum TABLE_TYPE {
		PLAYER_INFO("`player_info`"),
		PLAYER_DATA("`player_data`"),
		PLAYER_PROFILE("`player_profile`"),
		HUNTER_PLAYER("`hunter_player`");
		
		String type;
		
	    TABLE_TYPE(String type) {
	    	this.type = type;
	    }
	    
	    @Override
	    public String toString() {
	    	return type;
	    }
	}
}
