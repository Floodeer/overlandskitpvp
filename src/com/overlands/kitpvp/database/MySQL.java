package com.overlands.kitpvp.database;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.database.interfaces.IDatabase;

public class MySQL implements IDatabase {

	private final String connectionUri;
	private final String username;
	private final String password;

	private AtomicReference<Connection> connection;
	private ExecutorService pool;

	public MySQL(String hostname, int port, String database, String user, String password) throws ClassNotFoundException, SQLException {

		connectionUri = String.format("jdbc:mysql://%s:%d/%s", hostname, port, database);
		this.username = user;
		this.password = password;

		try {
			pool = Executors.newCachedThreadPool();
			connection = new AtomicReference<>();
			Class.forName("com.mysql.jdbc.Driver");
			connect();

		} catch (SQLException sqlException) {
			close();
			throw sqlException;
		}
	}
	
	@Override
	public void connect() throws SQLException {
		if (connection.get() != null) {
			try {
				connection.get().createStatement().execute("SELECT 1;");
			} catch (SQLException sqlException) {
				if (sqlException.getSQLState().equals("08S01")) {
					try {
						connection.get().close();
					} catch (SQLException ignored) {
					}
				}
			}
		}
		if (connection.get() == null || connection.get().isClosed())
			connection.set(DriverManager.getConnection(connectionUri, username, password));
	}

	@Override
	public void close() {
		try {
			if (connection.get() != null && !connection.get().isClosed()) {
				connection.get().close();
			}

		} catch (SQLException ignored) {

		}
		connection.set(null);
	}
	
	@Override
	public AtomicReference<Connection> getConnection() {
		return connection;
	}

	@Override
	public Executor getExecutor() {
		return pool;
	}

	@Override
	public boolean checkConnection() {
		try {
			connect();
		} catch (SQLException ex) {
			close();
			ex.printStackTrace();
			return false;
		}
		return true;
	}
	
	@Override
	public void createTables() throws IOException, SQLException {
		URL resource = Resources.getResource(Main.class, "/tables.sql");
		String[] databaseStructure = Resources.toString(resource, Charsets.UTF_8).split(";");

		if (databaseStructure.length == 0) {
			return;
		}

		Statement statement = null;

		try {
			connection.get().setAutoCommit(false);
			statement = connection.get().createStatement();
			for (String query : databaseStructure) {
				query = query.trim();

				if (query.isEmpty()) {
					continue;
				}
				statement.execute(query);
			}
			connection.get().commit();
		} finally {
			connection.get().setAutoCommit(true);
			if (statement != null && !statement.isClosed()) {
				statement.close();
			}
		}
	}
	
	@Override
	public boolean columnExists(String table, String column) {
		Connection conn = getConnection().get();
		DatabaseMetaData metadata;
		try {
			metadata = conn.getMetaData();
			ResultSet resultSet;
			resultSet = metadata.getTables(null, null, table, null);
			if (resultSet.next()) {
				resultSet = metadata.getColumns(null, null, table, column);
				if (!resultSet.next()) {
					return false;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public void addColumn(String table, String value, String after) throws IOException, SQLException {
		Statement statement = null;
		try {
			connection.get().setAutoCommit(false);
			statement = connection.get().createStatement();

			String query = "ALTER TABLE " + table + " ADD " + value + " AFTER " + after;
			statement.execute(query);
			connection.get().commit();

		} finally {
			connection.get().setAutoCommit(true);
			if (statement != null) {
				statement.close();
			}
		}
	}
}