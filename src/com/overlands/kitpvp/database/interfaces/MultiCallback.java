package com.overlands.kitpvp.database.interfaces;

public interface MultiCallback<T, E> {

	public void onCall(T resultA, E resultB);

}
