package com.overlands.kitpvp.database.interfaces;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;

public interface IDatabase {
	
	 void connect() throws SQLException;
		
	 void close();
	
	 void createTables() throws IOException, SQLException;
	
	 AtomicReference<Connection> getConnection();
	
	 Executor getExecutor();
	
	 boolean checkConnection();
	
	 boolean columnExists(String table, String column);
	
	 void addColumn(String table, String value, String after) throws IOException, SQLException;

}
