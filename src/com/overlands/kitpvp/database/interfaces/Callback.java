package com.overlands.kitpvp.database.interfaces;

public interface Callback<T> {

	public void onCall(T result);

}
