package com.overlands.kitpvp.database.util;

import java.util.List;

public class StatementSerializer {

	private String result;
	private List<String> list;

	public StatementSerializer(List<String> list) {
		this.list = list;
	}

	public StatementSerializer execute() {
		StringBuilder tList = new StringBuilder();
		String toSepare = "";
		for(String tL : list) {
			tList.append(toSepare);
			tList.append(tL);
			toSepare = ", ";
		}
		if(list.isEmpty())
			this.result = "";
		else
			this.result = tList.toString();
		return this;
	}

	public String getDecodedString() {
		return result;
	}
}
