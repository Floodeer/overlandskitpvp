package com.overlands.kitpvp.database.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.calc.TimeUtils;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.database.interfaces.Callback;
import org.bukkit.command.CommandSender;

public class QueryUtils {
	
	public static void getUUID(String name, Callback<String> result, boolean asyncCall) {
		if(asyncCall) {
			new SQLOperation() {
				ResultSet resultSet = null;
				PreparedStatement preparedStatement = null;
				@Override
				public void onExecute() throws SQLException {
					String SELECT_QUERY = "SELECT `uuid` FROM `player_info` WHERE `playername` = ? LIMIT 1;";
					try {
						preparedStatement = Main.get().getDB().getConnection().get().prepareStatement(SELECT_QUERY);
						preparedStatement.setString(1, name);
						resultSet = preparedStatement.executeQuery();
						if(resultSet != null && resultSet.next()) {
							result.onCall(resultSet.getString(1));
						}
					}finally {
						if(preparedStatement != null)
							preparedStatement.close();
						if(resultSet != null)
							resultSet.close();
					}
				}
			}.writeOperation(Main.get().getDB().getExecutor(), Main.get().getLogger(), "Ocorreu um eror ao acessar banco de dados.");
		}else {
			ResultSet resultSet = null;
			PreparedStatement preparedStatement = null;
			try {
				String SELECT_QUERY = "SELECT `uuid` FROM `player_info` WHERE `playername` = ? LIMIT 1;";
				preparedStatement = Main.get().getDB().getConnection().get().prepareStatement(SELECT_QUERY);
				preparedStatement.setString(1, name);
				resultSet = preparedStatement.executeQuery();
				if(resultSet != null && resultSet.next()) {
					result.onCall(resultSet.getString(1));
				}
			}catch(SQLException ex) {
				ex.printStackTrace();
			}finally {
				try {
					if(preparedStatement != null)
						preparedStatement.close();
					if(resultSet != null)
						resultSet.close();
				}catch(SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
	}

	public static void modifyOfflinePlayer(CommandSender sender, String player, String what, String value) {
		long t = System.currentTimeMillis();
		Main.get().getDB().getExecutor().execute(new Runnable() {
			PreparedStatement preparedStatement = null;

			@Override
			public void run() {
				try {
					String query = "UPDATE `player_data` SET `" + what + "` = ? WHERE `playername` = ?;";
					preparedStatement = Main.get().getDB().getConnection().get().prepareStatement(query.toString());
					preparedStatement.setString(1, value);
					preparedStatement.setString(2, player);
					preparedStatement.executeUpdate();
					sender.sendMessage(MessageUtil.color("&9Ação concluída, levou " + TimeUtils.getSince(t) + " para modificar o valor de &c" + what + "&9para &a" + value + "&9."));
				} catch (SQLException ex) {
					ex.printStackTrace();
					sender.sendMessage(
							MessageUtil.color("&cOcorreu um erro ao executar operação; " + ex.getLocalizedMessage()));
				} finally {
					if (preparedStatement != null)
						try {
							preparedStatement.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
				}
			}
		});
	}

}
