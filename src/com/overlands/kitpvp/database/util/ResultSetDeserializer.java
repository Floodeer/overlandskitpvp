package com.overlands.kitpvp.database.util;

import java.util.ArrayList;
import java.util.List;

public class ResultSetDeserializer {

	private List<String> newList;
	private String list;

	public ResultSetDeserializer(String list) {
		this.list = list;
	}

	public ResultSetDeserializer execute() {
		this.newList = new ArrayList<>();
		String[] parts = list.split(", ");
		for (String part : parts)
			newList.add(part);
		return this;
	}

	public List<String> toList() {
		return newList;
	}
}
