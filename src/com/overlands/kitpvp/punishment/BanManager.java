package com.overlands.kitpvp.punishment;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.google.common.collect.Maps;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.calc.TimeUtils;
import com.overlands.core.utils.search.PlayerSearcher;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.database.util.QueryUtils;
import com.overlands.kitpvp.database.util.SQLOperation;
import com.overlands.kitpvp.database.interfaces.Callback;
import com.overlands.kitpvp.database.interfaces.MultiCallback;
import com.overlands.kitpvp.game.GamePlayer;

public class BanManager {

	public enum BanReason {
		HACKING_TEMP,
		HACKING_PERM,
		NICK,
		INSULT,
		OTHER,
		ABUSE;
		
		public String formatString(boolean autoBanned) {
			switch (this) {
			case ABUSE:
				break;
			case HACKING_PERM:
				return "&cVocê foi banido permanentemente.";
			case HACKING_TEMP:
				break;
			case INSULT:
				break;
			case NICK:
				break;
			case OTHER:
				break;
			default:
				break;
			}
			return "&cVocê foi banido.";
		}
	}
	
	public void unbanPlayer(CommandSender sender, String player, boolean async) {
		long now = System.currentTimeMillis();
		sender.sendMessage(MessageUtil.color("&9Aguarde..."));
		if(async) {
			Main.getDataManager().doesPlayerExists(player, new Callback<Boolean>() {		
				@Override
				public void onCall(Boolean result) {
					if(result) {
						
						new SQLOperation() {
							PreparedStatement preparedStatement = null;
							String uuid = "";
							@Override
							public void onExecute() throws SQLException {
								QueryUtils.getUUID(player, new Callback<String>() {
									@Override
									public void onCall(String result) {
										uuid = result;
									}
								}, false);
								if(uuid.isEmpty()) {
									sender.sendMessage(MessageUtil.color("&cOcorreu um erro ao processar UUID de &9" + player + "&c."));
									return;
								}
								String query = "UPDATE `player_info` SET `banned` = ?, `banReason` = ?, `banEnd` = ? WHERE `uuid` = ?;";
								try {
									preparedStatement = Main.get().getDB().getConnection().get().prepareStatement(query.toString());
									preparedStatement.setBoolean(1, false);
									preparedStatement.setString(2, "");
									preparedStatement.setLong(3, 0);
									preparedStatement.setString(4, uuid);
									preparedStatement.executeUpdate();
								}finally {
									if(preparedStatement != null)
										preparedStatement.close();
									
									sender.sendMessage(MessageUtil.color("&aPlayer desbanido com sucesso. Operação levou " + TimeUtils.getSince(now)));
								}
							}
						}.writeOperation(Main.get().getDB().getExecutor(), Main.get().getLogger(), "Erro ao acessar data de " + player + ".");
					}else {
						sender.sendMessage(MessageUtil.color("&cO player não foi encontrado no banco de dados. Operação levou " + TimeUtils.getSince(now)));
					}
				}
			});
		}else {
			PreparedStatement preparedStatement = null;
			String query = "UPDATE `player_info` SET `banned` = ?, `banReason` = ?, `banEnd` = ? WHERE `playername` = ?;";
			try {
				preparedStatement = Main.get().getDB().getConnection().get().prepareStatement(query.toString());
				preparedStatement.setBoolean(1, false);
				preparedStatement.setString(2, "");
				preparedStatement.setString(3, player);
				preparedStatement.setLong(4, 0);
				preparedStatement.executeUpdate();
			}catch(SQLException ex) {
				ex.printStackTrace();
			}finally {
				try {
					if(preparedStatement != null)
						preparedStatement.close();
				}catch(SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	
	public void unbanPlayer(CommandSender sender, UUID uuid, boolean async) {
		long now = System.currentTimeMillis();
		sender.sendMessage(MessageUtil.color("&9Aguarde..."));
		if(async) {
			Main.getDataManager().doesPlayerExists(uuid, new Callback<Boolean>() {		
				@Override
				public void onCall(Boolean result) {
					if(result) {
						new SQLOperation() {
							PreparedStatement preparedStatement = null;
							@Override
							public void onExecute() throws SQLException {
								String query = "UPDATE `player_info` SET `banned` = ?, `banReason` = ?, `banEnd` = ? WHERE `uuid` = ?;";
								try {
									preparedStatement = Main.get().getDB().getConnection().get().prepareStatement(query.toString());
									preparedStatement.setBoolean(1, false);
									preparedStatement.setString(2, "");
									preparedStatement.setLong(3, 0);
									preparedStatement.setString(4, uuid.toString());
									preparedStatement.executeUpdate();
									sender.sendMessage(MessageUtil.color("&aPlayer desbanido com sucesso. Operação levou " + TimeUtils.getSince(now)));
								}finally {
									if(preparedStatement != null)
										preparedStatement.close();
								}
							}
						}.writeOperation(Main.get().getDB().getExecutor(), Main.get().getLogger(), "Erro ao acessar data de " + uuid.toString() + ".");
					}else {
						sender.sendMessage(MessageUtil.color("&cO player não foi encontrado no banco de dados. Operação levou " + TimeUtils.getSince(now)));
					}
				}
			});
		}else {
			PreparedStatement preparedStatement = null;
			String query = "UPDATE `player_info` SET `banned` = ?, `banReason` = ?, `banEnd` = ? WHERE `uuid` = ?;";
			try {
				preparedStatement = Main.get().getDB().getConnection().get().prepareStatement(query.toString());
				preparedStatement.setBoolean(1, false);
				preparedStatement.setString(2, "");
				preparedStatement.setLong(3, 0);
				preparedStatement.setString(4, uuid.toString());
				preparedStatement.executeUpdate();
			}catch(SQLException ex) {
				ex.printStackTrace();
			}finally {
				try {
					if(preparedStatement != null)
						preparedStatement.close();
				}catch(SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	
	public void tempBanPlayer(CommandSender sender, String target, String reason, int seconds) {
		long endToMillis = seconds * 1000;
        long end = seconds == -1 ? -1 : endToMillis + System.currentTimeMillis();
        if(PlayerSearcher.searchOnline(sender, target, true) != null) {
			Player player = PlayerSearcher.searchOnline(sender, target, false);
			GamePlayer gp = GamePlayer.get(player);
			gp.setBanReason(reason);
			gp.setBanEnd(end);
			gp.setBanned(true);
			player.kickPlayer(reason);
			sender.sendMessage(MessageUtil.color("&9Player &c" + target + " &9banido."));
        }else {
        	long now = System.currentTimeMillis();
			sender.sendMessage(MessageUtil.color("&9Aguarde..."));
			Main.getDataManager().doesPlayerExists(target, new Callback<Boolean>() {
				
				@Override
				public void onCall(Boolean result) {
					if(result) {
						new SQLOperation() {
							PreparedStatement preparedStatement = null;
							@Override
							public void onExecute() throws SQLException {
								String query = "UPDATE `player_info` SET `banned` = ?, `banReason` = ?, `banEnd` = ? WHERE `playername` = ?;";
								try {
									preparedStatement = Main.get().getDB().getConnection().get().prepareStatement(query.toString());
									preparedStatement.setBoolean(1, true);
									preparedStatement.setString(2, reason);
									preparedStatement.setLong(3, end);
									preparedStatement.setString(4, target);
									preparedStatement.executeUpdate();
									sender.sendMessage(MessageUtil.color("&aPlayer banido. Operação levou " + TimeUtils.getSince(now)));
								}finally {
									if(preparedStatement != null)
										preparedStatement.close();
								}
							}
						}.writeOperation(Main.get().getDB().getExecutor(), Main.get().getLogger(), "Erro ao acessar data de " + target + ".");
					}else {
						sender.sendMessage(MessageUtil.color("&cO player não foi encontrado no banco de dados. Operação levou " + TimeUtils.getSince(now)));
					}
				}
			});
        }
	}
	
	public void banPlayer(CommandSender sender, String target, String reason) {
		
		if(PlayerSearcher.searchOnline(sender, target, true) != null) {
			Player player = PlayerSearcher.searchOnline(sender, target, false);
			GamePlayer gp = GamePlayer.get(player);
			gp.setBanReason(reason);
			gp.setBanned(true);
			gp.setBanEnd(-1);
			player.kickPlayer(reason);
			sender.sendMessage(MessageUtil.color("&9Player &c" + target + " &9banido."));
		}else {
			long now = System.currentTimeMillis();
			sender.sendMessage(MessageUtil.color("&9Aguarde..."));
			Main.getDataManager().doesPlayerExists(target, new Callback<Boolean>() {
				
				@Override
				public void onCall(Boolean result) {
					if(result) {
						new SQLOperation() {
							PreparedStatement preparedStatement = null;
							@Override
							public void onExecute() throws SQLException {
								String query = "UPDATE `player_info` SET `banned` = ?, `banReason` = ?, `banEnd` = ? WHERE `playername` = ?;";
								try {
									preparedStatement = Main.get().getDB().getConnection().get().prepareStatement(query.toString());
									preparedStatement.setBoolean(1, true);
									preparedStatement.setString(2, reason);
									preparedStatement.setLong(3, -1);
									preparedStatement.setString(4, target);
									preparedStatement.executeUpdate();
									sender.sendMessage(MessageUtil.color("&aPlayer banido. Operação levou " + TimeUtils.getSince(now)));
								}finally {
									if(preparedStatement != null)
										preparedStatement.close();
								}
							}
						}.writeOperation(Main.get().getDB().getExecutor(), Main.get().getLogger(), "Erro ao acessar data de " + target + ".");
					}else {
						sender.sendMessage(MessageUtil.color("&cO player não foi encontrado no banco de dados. Operação levou " + TimeUtils.getSince(now)));
					}
				}
			});
		}
	}
	
	public void isBanned(String targetAsName, Callback<Boolean> result)  {
		new SQLOperation() {
			ResultSet resultSet = null;
			PreparedStatement preparedStatement = null;
			String uuid = "";
			@Override
			public void onExecute() throws SQLException {
				QueryUtils.getUUID(targetAsName, new Callback<String>() {
					@Override
					public void onCall(String result) {
						uuid = result;
					}
				}, false);
				if(uuid.isEmpty()) {
					result.onCall(null);
					return;
				}
				String QUERY = "SELECT `banned` FROM `player_info` WHERE `uuid` = " + uuid;
				try {
					preparedStatement = Main.get().getDB().getConnection().get().prepareStatement(QUERY);
					resultSet = preparedStatement.executeQuery();
					if(resultSet != null && resultSet.next()) {
						result.onCall(resultSet.getBoolean("banned"));
					}
				}finally {
					if(preparedStatement != null)
						preparedStatement.close();
					if(resultSet != null)
						resultSet.close();
				}
				
			}
		}.writeOperation(Main.get().getDB().getExecutor(), Main.get().getLogger(), "Erro ao verificar tabela player_info");
	}
	
	public void isBannedByTime(String targetAsName, MultiCallback<Boolean, Long> result) {
		new SQLOperation() {
			ResultSet resultSet = null;
			PreparedStatement preparedStatement = null;
			String uuid = "";
			@Override
			public void onExecute() throws SQLException {
				QueryUtils.getUUID(targetAsName, new Callback<String>() {
					@Override
					public void onCall(String result) {
						uuid = result;
					}
				}, false);
				if(uuid.isEmpty()) {
					result.onCall(null, null);
					return;
				}
				
				String QUERY = "SELECT `banned`, `banEnd` FROM `player_info` WHERE `uuid` = " + uuid;
				try {
					preparedStatement = Main.get().getDB().getConnection().get().prepareStatement(QUERY);
					resultSet = preparedStatement.executeQuery();
					if(resultSet != null && resultSet.next()) {
						result.onCall(resultSet.getBoolean("banned"), resultSet.getLong("banEnd"));
					}
				}finally {
					if(preparedStatement != null)
						preparedStatement.close();
					if(resultSet != null)
						resultSet.close();
				}
				
			}
		}.writeOperation(Main.get().getDB().getExecutor(), Main.get().getLogger(), "Erro ao verificar tabela player_info");
	}
	
	public void getEnd(String targetAsName, Callback<Long> result) {
		new SQLOperation() {
			ResultSet resultSet = null;
			PreparedStatement preparedStatement = null;
			String uuid = "";
			@Override
			public void onExecute() throws SQLException {
				QueryUtils.getUUID(targetAsName, new Callback<String>() {
					@Override
					public void onCall(String result) {
						uuid = result;
					}
				}, false);
				if(uuid.isEmpty()) {
					result.onCall(null);
					return;
				}
				
				String QUERY = "SELECT `banEnd` FROM `player_info` WHERE `uuid` = " + uuid;
				try {
					preparedStatement = Main.get().getDB().getConnection().get().prepareStatement(QUERY);
					resultSet = preparedStatement.executeQuery();
					if(resultSet != null && resultSet.next()) {
						result.onCall(resultSet.getLong("banEnd"));
					}
				}finally {
					if(preparedStatement != null)
						preparedStatement.close();
					if(resultSet != null)
						resultSet.close();
				}
				
			}
		}.writeOperation(Main.get().getDB().getExecutor(), Main.get().getLogger(), "Erro ao verificar tabela player_info");
	}

	public void getEnd(UUID uuid, Callback<Long> result) {
		new SQLOperation() {
			ResultSet resultSet = null;
			PreparedStatement preparedStatement = null;
			@Override
			public void onExecute() throws SQLException {			
				String QUERY = "SELECT `banEnd` FROM `player_info` WHERE `uuid` = " + uuid.toString() +  " LIMIT 1;";
				try {
					preparedStatement = Main.get().getDB().getConnection().get().prepareStatement(QUERY);
					resultSet = preparedStatement.executeQuery();
					if(resultSet != null && resultSet.next()) {
						result.onCall(resultSet.getLong("banEnd"));
					}
				}finally {
					if(preparedStatement != null)
						preparedStatement.close();
					if(resultSet != null)
						resultSet.close();
				}	
			}
		}.writeOperation(Main.get().getDB().getExecutor(), Main.get().getLogger(), "Erro ao verificar tabela player_info");
	}
	
	public void getReason(String targetAsName, Callback<String> result) {
		new SQLOperation() {
			ResultSet resultSet = null;
			PreparedStatement preparedStatement = null;
			String uuid = "";
			@Override
			public void onExecute() throws SQLException {
				QueryUtils.getUUID(targetAsName, new Callback<String>() {
					@Override
					public void onCall(String result) {
						uuid = result;
					}
				}, false);
				if(uuid.isEmpty()) {
					result.onCall(null);
					return;
				}
				
				String QUERY = "SELECT `banReason` FROM `player_info` WHERE `uuid` = " + uuid;
				try {
					preparedStatement = Main.get().getDB().getConnection().get().prepareStatement(QUERY);
					resultSet = preparedStatement.executeQuery();
					if(resultSet != null && resultSet.next()) {
						result.onCall(resultSet.getString(1));
					}
				}finally {
					if(preparedStatement != null)
						preparedStatement.close();
					if(resultSet != null)
						resultSet.close();
				}		
			}
		}.writeOperation(Main.get().getDB().getExecutor(), Main.get().getLogger(), "Erro ao verificar tabela player_info");
	}

	
	public String getTimeLeft(long time){
        if(time == -1) {
        	return MessageUtil.color("Permanente");
        }
 
        long timeLeft = (time - System.currentTimeMillis()) / 1000;
        int months = 0;
        int days = 0;
        int hours = 0;
        int minutes = 0;
        int secods = 0;
 
        while(timeLeft >= TimeUnit.MONTHS.getToSecond()){
            months++;
            timeLeft -= TimeUnit.MONTHS.getToSecond();
        }
 
        while(timeLeft >= TimeUnit.DAYS.getToSecond()){
            days++;
            timeLeft -= TimeUnit.DAYS.getToSecond();
        }
 
        while(timeLeft >= TimeUnit.HOURS.getToSecond()){
            hours++;
            timeLeft -= TimeUnit.HOURS.getToSecond();
        }
 
        while(timeLeft >= TimeUnit.MINUTES.getToSecond()){
            minutes++;
            timeLeft -= TimeUnit.MINUTES.getToSecond();
        }
 
        while(timeLeft >= TimeUnit.SECONDS.getToSecond()){
            secods++;
            timeLeft -= TimeUnit.SECONDS.getToSecond();
        }
 
        return months + " " + TimeUnit.MONTHS.getName() + ", " + days + " " + TimeUnit.DAYS.getName() + ", " + hours + " " + TimeUnit.HOURS.getName() + ", " + minutes + " " + TimeUnit.MINUTES.getName() + ", " + secods + " " + TimeUnit.SECONDS.getName();
    }
	
	public class BanMessage {
		private String title, appealInfo, timeLeft;
		private String[] lines;
		
		public BanMessage(String title, String appealInfo, String timeLeft, String[] lines) {
			this.timeLeft = timeLeft;
			this.title = title;
			this.lines = lines;
			this.appealInfo = appealInfo;
		}
		
		@Override
		public String toString() {
			StringBuilder msg = new StringBuilder();
			msg.append(title + "\n");
			for(String s : lines) {
				msg.append(s + "\n");
			}
			if(timeLeft != null && !timeLeft.isEmpty())
				msg.append(timeLeft);
			
			msg.append(appealInfo);
			return msg.toString();
		}
		
		public String getTitle() {
			return title;
		}
		
		public String getAppealInfo() {
			return appealInfo;
		}
		
		public String getTimeLeft()  {
			return timeLeft;
		}
		
		public String[] getLines() {
			return lines;
		}
	}
	
	public enum TimeUnit {
		 
	    SECONDS("Segundo(s)", "sec", 1),
	    MINUTES("Minuto(s)", "min", 60),
	    HOURS("Hora(s)", "h", 60 * 60),
	    DAYS("Dia(s)", "d", 60 * 60 * 24),
	    MONTHS("Mêses", "m", 60 * 60 * 24 * 30);
	 
	    private String name;
	    private String shortcut;
	    private long toSecond;
	 
	    private static Map<String, TimeUnit> id_shortcuts = Maps.newHashMap();
	 
	    TimeUnit(String name, String shortcut, long toSecond) {
	        this.name = name;
	        this.shortcut = shortcut;
	        this.toSecond = toSecond;
	    }
	 
	    static {
	        for(TimeUnit units : values()){
	            id_shortcuts.put(units.shortcut, units);
	        }
	    }
	    public static TimeUnit getFromShortcut(String shortcut){
	        return id_shortcuts.get(shortcut);
	    }
	 
	    public String getName(){
	        return name;
	    }
	 
	    public String getShortcut(){
	        return shortcut;
	    }
	 
	    public long getToSecond() {
	        return toSecond;
	    }
	 
	    public static boolean existFromShortcut(String shortcut){
	        return id_shortcuts.containsKey(shortcut);
	    }
	}
}
