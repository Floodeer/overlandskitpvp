package com.overlands.kitpvp.manager;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

import com.google.common.collect.Lists;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.scheduler.SchedulerEvent;
import com.overlands.core.utils.scheduler.UpdateType;
import com.overlands.kitpvp.Main;

public class ServerManager implements Listener {

	public String loadingReason;
	public boolean isLoaded;
	public boolean networkBooster = false;
	public String boosterPlayer = null;

	public boolean isOwnerOnline = false;
	public boolean setOwnerInvisible = false;

	public boolean lagCheck = false;

	public List<Player> checkListeners = Lists.newArrayList();

	private long _lastRun = -1;
	private int _count;
	private double _ticksPerSecond;
	private double _ticksPerSecondAverage;
	private long _lastAverage;
	
	public ServerManager() {
		Main.get().getServer().getPluginManager().registerEvents(this, Main.get());
	}

	public void activateBooster(String who) {
		this.boosterPlayer = who;
		networkBooster = true;
		if (who == null) {
			Bukkit.getServer().broadcastMessage(MessageUtil.color("&b&lUm booster de coins foi ativado."));
		} else {
			Bukkit.getServer().broadcastMessage(
					MessageUtil.color("&b&lUm booster de coins foi ativado por &e&l" + who + "&b&l."));
		}
		_lastRun = System.currentTimeMillis();
		_lastAverage = System.currentTimeMillis();
	}

	public void desactiveBooster() {
		networkBooster = false;
		String who = boosterPlayer;
		if (who != null) {
			Bukkit.getServer().broadcastMessage(MessageUtil.color("&c&lO booster &e&l" + who + " &c&lfoi desativado."));
		} else {
			Bukkit.getServer().broadcastMessage(MessageUtil.color("&c&lO booster foi desativado."));
		}
	}

	public void call(org.bukkit.event.Event event) {
		Main.get().getServer().getPluginManager().callEvent(event);
	}

	public boolean callCancellable(org.bukkit.event.Event event) {
		if (event instanceof Cancellable) {
			if (!((Cancellable) event).isCancelled()) {
				call(event);
				return true;
			}
		}
		return false;
	}
	
	public boolean somethingWentWrong() {
		return isLoaded;

	}
	
	@EventHandler
	public void handlerMotd(ServerListPingEvent e) {
		e.setMotd(MessageUtil.color("&a              &6&lOverlands &f&lKitPvP &c[1.8-1.13] &a \n" + "  &bSKILLS &f- &aMYSTERY BOXES &f- &dEVENTOS &f- &9PRESTIGES"));
	}

	@EventHandler
	public void update(SchedulerEvent event) {
		if (event.getType() != UpdateType.SEC)
			return;
		if(!lagCheck)
			return;

		long now = System.currentTimeMillis();
		_ticksPerSecond = 1000D / (now - _lastRun) * 20D;

		checkListeners.forEach(p -> {
			
			p.sendMessage(" ");
			p.sendMessage(ChatColor.YELLOW + "TPS");
			p.sendMessage(" ");
			p.sendMessage(ChatColor.GRAY + "TPS-------" + ChatColor.YELLOW + String.format("%.00f", _ticksPerSecond));
			p.sendMessage(ChatColor.GRAY + "Media--------" + ChatColor.YELLOW + String.format("%.00f", _ticksPerSecondAverage * 20));
			p.sendMessage(" ");
			p.sendMessage(ChatColor.YELLOW + "MEMORIA");
			p.sendMessage(" ");
			p.sendMessage(ChatColor.GRAY + "Livre-------" + ChatColor.YELLOW + (Runtime.getRuntime().freeMemory() / 1048576) + "MB");
		    p.sendMessage(ChatColor.GRAY + "Maximo--------" + ChatColor.YELLOW + (Runtime.getRuntime().maxMemory() / 1048576) + "MB");
		});

		if (_count % 30 == 0) {
			_ticksPerSecondAverage = 30000D / (now - _lastAverage) * 20D;
			_lastAverage = now;
		}

		_lastRun = now;

		_count++;
	}
	
	public double getTPS() {
		return _ticksPerSecond;
	}
	
	public double getTPSAverage() {
		return _ticksPerSecondAverage * 20;
	}
	
	public long getFreeMemory() {
		return Runtime.getRuntime().freeMemory() / 1048576;
	}
	
	public long getMaxMemory() {
		return Runtime.getRuntime().maxMemory() / 1048576;
	}

	public void sendInfo(String str) {
		Main.get().getLogger().info(str);
	}
	
	public void sendError(String str) {
		Main.get().getLogger().severe(str);
	}
}
