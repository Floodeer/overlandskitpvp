package com.overlands.kitpvp.manager;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.menus.IconMenu.OptionClickEvent;
import com.overlands.core.utils.menus.IconMenu.OptionClickEventHandler;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.levels.LevelFormatter;

public class ProfileManager {
	
	public void open(Player player) {
		GamePlayer gp = GamePlayer.get(player);
		Main.getIconCore().create(player, MessageUtil.color("&7Perfil"), 5 * 9, event -> {
            if(event.getPosition() == 36) {
                event.setWillClose(true);
                event.setWillDestroy(true);
            }else if(event.getPosition() == 41) {
                event.setWillDestroy(true);
                Main.getPreferences().show(gp);
            }
        });
		
		icons(gp);
	}
	
	public void open(Player request, Player profileOwner) {
		GamePlayer player = GamePlayer.get(profileOwner);
		Main.getIconCore().create(request, MessageUtil.color("&7Perfil"), 5 * 9, event -> {
            event.setWillClose(true);
            event.setWillDestroy(true);
        });
		
		Main.getIconCore().setOption(player.getPlayer(), 1,
				ItemFactory.create(Material.IRON_SWORD), 
				MessageUtil.color("&7Estatísticas de combate"),
				getKDStats(player));
		
		Main.getIconCore().setOption(player.getPlayer(), 3,
				ItemFactory.create(Material.DIAMOND_SWORD), 
				MessageUtil.color("&7Estatísticas de dano"),
				getDamageStats(player));
		
		Main.getIconCore().setOption(player.getPlayer(), 5,
				ItemFactory.create(Material.BOW), 
				MessageUtil.color("&7Estatísticas de arco"),
				getBowStats(player));
		
		Main.getIconCore().setOption(player.getPlayer(), 7,
				ItemFactory.create(Material.GOLDEN_APPLE), 
				MessageUtil.color("&7Estatísticas de vida"),
				getHealingStats(player));
		
		Main.getIconCore().setOption(player.getPlayer(), 20,
				ItemFactory.create(Material.IRON_CHESTPLATE), 
				MessageUtil.color("&7Estatísticas competitivas &c&lEM BREVE"),
				getScore(player));
		
		Main.getIconCore().setOption(player.getPlayer(), 22,
				ItemFactory.create(Material.PAPER), 
				MessageUtil.color("&7Estatísticas de contrato &c&lEM BREVE"),
				getContrStats(player));
		
		Main.getIconCore().setOption(player.getPlayer(), 24,
				ItemFactory.create(Material.EXP_BOTTLE), 
				MessageUtil.color("&7Estatísticas de experiência"),
				getExpStats(player));
		
		Main.getIconCore().setOption(player.getPlayer(), 39,
				ItemFactory.create(Material.GOLD_NUGGET), 
				MessageUtil.color("&6" + player.getCoins() + " coins."));
		
		Main.getIconCore().setOption(player.getPlayer(), 40,
				ItemFactory.create(Material.ENCHANTED_BOOK), 
				MessageUtil.color("&7Conquistas"));
		
		Main.getIconCore().setOption(player.getPlayer(), 36,
				ItemFactory.create(Material.BARRIER), 
				MessageUtil.color("&cFechar"));
		
		Main.getIconCore().show(player.getPlayer());
		
	}
	
	public void icons(GamePlayer player) {
		Main.getIconCore().setOption(player.getPlayer(), 1,
				ItemFactory.create(Material.IRON_SWORD), 
				MessageUtil.color("&7Estatísticas de combate"),
				getKDStats(player));
		
		Main.getIconCore().setOption(player.getPlayer(), 3,
				ItemFactory.create(Material.DIAMOND_SWORD), 
				MessageUtil.color("&7Estatísticas de dano"),
				getDamageStats(player));
		
		Main.getIconCore().setOption(player.getPlayer(), 5,
				ItemFactory.create(Material.BOW), 
				MessageUtil.color("&7Estatísticas de arco"),
				getBowStats(player));
		
		Main.getIconCore().setOption(player.getPlayer(), 7,
				ItemFactory.create(Material.GOLDEN_APPLE), 
				MessageUtil.color("&7Estatísticas de vida"),
				getHealingStats(player));
		
		Main.getIconCore().setOption(player.getPlayer(), 20,
				ItemFactory.create(Material.IRON_CHESTPLATE), 
				MessageUtil.color("&7Estatísticas competitivas &c&lEM BREVE"),
				getScore(player));
		
		Main.getIconCore().setOption(player.getPlayer(), 22,
				ItemFactory.create(Material.PAPER), 
				MessageUtil.color("&7Estatísticas de contrato &c&lEM BREVE"),
				getContrStats(player));
		
		Main.getIconCore().setOption(player.getPlayer(), 24,
				ItemFactory.create(Material.EXP_BOTTLE), 
				MessageUtil.color("&7Estatísticas de experiência"),
				getExpStats(player));
		
		Main.getIconCore().setOption(player.getPlayer(), 39,
				ItemFactory.create(Material.GOLD_NUGGET), 
				MessageUtil.color("&6" + player.getCoins() + " coins."));
		
		Main.getIconCore().setOption(player.getPlayer(), 40,
				ItemFactory.create(Material.ENCHANTED_BOOK), 
				MessageUtil.color("&7Conquistas"));
		
		Main.getIconCore().setOption(player.getPlayer(), 41,
				ItemFactory.create(Material.REDSTONE_COMPARATOR), 
				MessageUtil.color("&7Preferências"));
		
		Main.getIconCore().setOption(player.getPlayer(), 36,
				ItemFactory.create(Material.BARRIER), 
				MessageUtil.color("&cFechar"));
		
		Main.getIconCore().show(player.getPlayer());
	}
	
	public String[] getExpStats(GamePlayer gp) {
		return MessageUtil.color(
				"&7Exp: &b" + gp.getExp(),
				"&7Nível: &b" + gp.getLevel(),
				"&7Próximo nível: &b" + LevelFormatter.expUntilNextLevel(gp),
				"&7Prestiges: &b" + gp.getPrestigeLevel());
				
	}
	
	public String[] getContrStats(GamePlayer gp) {
		return MessageUtil.color(
				"&7Contratos concluídos: &b0",
				"&7Contratos quebrados: &b0",
				"&7Contratos especiais: &b0");
				
	}

	public String[] getKDStats(GamePlayer gp) {
		return MessageUtil.color(
				"&7Skills usadas: &b" + gp.getSkillUsed(),
				"&7Kills: &b" + gp.getKills(), 
				"&7Mortes: &b" + gp.getDeaths(),
				"&7KD/R: &b" + Math.floor((gp.getDeaths() > 0 && gp.getKills() > 0 ? gp.getKills() / gp.getDeaths() : 0)));
				
	}
	
	public String[] getDamageStats(GamePlayer gp) {
		return MessageUtil.color(
				"&7Dano causado: &b" + Math.floor(gp.getDamageDone()), 
				"&7Dano recebido: &b" + Math.floor(gp.getDamageReceived()));
				
	}
	
	public String[] getHealingStats(GamePlayer gp) {
		return MessageUtil.color(
				"&7Cura causada: &b" + Math.floor(gp.getHealingDone()), 
				"&7Cura recebida: &b" + Math.floor(gp.getHealingReceived()));
				
	}
	
	public String[] getScore(GamePlayer gp) {
		return MessageUtil.color(
				"&7Pontuação: &b" + gp.getScore(), 
				"&7Prêmios &b0&e☆");
				
	}
	
	public String[] getBowStats(GamePlayer gp) {
		return MessageUtil.color(
				"&7Tiros: &b" + gp.getBowShots(), 
				"&7Hits: &b" + gp.getBowHits(),
				"&7Precisão: &b" + calculateAccuracy(gp));
	}

	private double calculateAccuracy(GamePlayer gp) {
		int shots = gp.getBowShots();
		int hits = gp.getBowHits();

		if (shots == 0) {
			return 0.0;
		}

		return (double) hits / shots * 100;
	}
}
