package com.overlands.kitpvp.manager;

import java.lang.management.ManagementFactory;

import com.overlands.kitpvp.game.GamePlayer;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.floodeer.scoreboards.global.GlobalScoreboard;
import com.overlands.core.utils.ActionBar;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.calc.TimeUtils;
import com.overlands.core.utils.scheduler.SchedulerEvent;
import com.overlands.core.utils.scheduler.UpdateType;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.ranks.Rank;
import com.sun.management.OperatingSystemMXBean;

public class AdminManager implements Listener {
	
	private final GlobalScoreboard scoreboard;
	
	public AdminManager() {
		this.scoreboard = new GlobalScoreboard("OVERLANDS", true);
	}

	public boolean isAnyoneInAdminMode() {
		return Main.getPM().getPlayers().stream().anyMatch(GamePlayer::isAdminMode);
	}
	
	@EventHandler
	public void onScheduler(SchedulerEvent event) {
		if(event.getType() == UpdateType.SEC) {
			Main.getPM().getPlayers().forEach(player -> {
				if(!player.isInGame() && player.isAdminMode()) {
					if(player.getPlayer().getScoreboard() != scoreboard.getScoreboard())
						player.getPlayer().setScoreboard(scoreboard.getScoreboard());			
					ActionBar.sendActionBar(player.getPlayer(), MessageUtil.color("&c&lESCONDIDO, MODO ADMIN"));
					Bukkit.getOnlinePlayers().stream().filter(all -> all != player.getPlayer() && !Rank.isStaff(all)).forEach(all -> {
						if(all.canSee(player.getPlayer())) {
							Util.hidePlayer(all, player.getPlayer(), true);
						}
					});
				}
			});
			if(!isAnyoneInAdminMode())
				return;

			scoreboard.reset();
			scoreboard.write("&7" + TimeUtils.today());
			scoreboard.writeBlank();
			scoreboard.write("&fCPU: &e" + getCpuProcess() + "%");
			scoreboard.write("&fThreads: &e" + ManagementFactory.getThreadMXBean().getThreadCount());
			scoreboard.writeBlank();
			scoreboard.write("&fTPS: &e" + String.format("%.00f", Main.get().getServerManager().getTPS()));
			scoreboard.writeBlank();
			scoreboard.write("&fLIVRE: &e" + Main.get().getServerManager().getFreeMemory() + "MB");
			scoreboard.write("&fTOTAL: &e" + Main.get().getServerManager().getMaxMemory() + "MB");
			scoreboard.writeBlank();
			scoreboard.write("&fPlayers: &e" + Bukkit.getOnlinePlayers().size() + "&f/&e" + Bukkit.getMaxPlayers());
			scoreboard.write("&fOcultos: &e" + Main.getPM().getPlayers().stream().filter(GamePlayer::isAdminMode).count());
			scoreboard.writeBlank();
			scoreboard.write("&7&l&oEscondido");
			scoreboard.draw();
			
		}else if(event.getType() == UpdateType.TICKS_2) {
			if(!isAnyoneInAdminMode())
				return;
			scoreboard.updateTitle();
		}
	}
	
	private double getCpuProcess() {
		OperatingSystemMXBean op = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
		return ((int)(op.getProcessCpuLoad() * 1000) / 10.0);
	}
}
