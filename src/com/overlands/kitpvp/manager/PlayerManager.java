package com.overlands.kitpvp.manager;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;

import com.google.common.collect.Maps;
import com.overlands.kitpvp.game.GamePlayer;

public class PlayerManager {

	private Map<UUID, GamePlayer> players = Maps.newHashMap();

	public void addPlayer(UUID uuid) {
		players.put(uuid, new GamePlayer(uuid));
	}
	
	public void removePlayer(UUID uuid) {
		if(players.containsKey(uuid))
			players.remove(uuid);
	
	}
	
	public Collection<GamePlayer> getPlayers() {
		return players.values();
	}
	
	public Collection<UUID> getUUIDs() {
		return players.keySet();
	}

	public GamePlayer getPlayer(UUID uniqueId) {
		return players.get(uniqueId);
	}
}
 