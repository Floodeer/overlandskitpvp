package com.overlands.kitpvp.manager;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Player;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.song.NBSDecoder;
import com.overlands.core.utils.song.RadioSongPlayer;
import com.overlands.core.utils.song.Song;
import com.overlands.core.utils.song.SongPlayer;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;

public class MusicManager {
	
	private Map<UUID, SongPlayer> songPlayers;
	private File songFile = null;
	
	public MusicManager() {
		songPlayers = Maps.newHashMap();
		songFile = new File(Main.get().getDataFolder() + File.separator + "songs");
		if(!songFile.exists()) {
			songFile.mkdirs();
		}	
	}
	
	public File getMusicFile() {
		return songFile;
	}

	public boolean play(String name, Collection<GamePlayer> players) {
		File sound = new File(songFile, name + ".nbs");
		if(!sound.exists()) {
			return false;
		}
		Song s = NBSDecoder.parse(sound);
		RadioSongPlayer sp = new RadioSongPlayer(s);
		sp.setAutoDestroy(true);
		for(GamePlayer gps : players) {
			Player p = gps.getPlayer();
			if(songPlayers.containsKey(p.getUniqueId())) {
				songPlayers.get(p.getUniqueId()).destroy();
				songPlayers.remove(p.getUniqueId());
			}
			sp.addPlayer(p);
			songPlayers.put(p.getUniqueId(), sp);
		}
		sp.setPlaying(true);
	
		return sp.isPlaying();
	}
	
	public void addPlayer(GamePlayer gp) {
		SongPlayer s = songPlayers.values().stream().findAny().get();
		songPlayers.put(gp.getUUID(), s);
	}
	
	public String getRandom() {
		List<String> files = Lists.newArrayList();
		for(File perk : Main.get().getMusicCore().getMusicFile().listFiles()) {
			files.add(perk.getName());
		}
		return files.get(MathUtils.r(files.size()));
	}
	
	public void stop() {
		if(songPlayers.isEmpty())
			return;
		
		songPlayers.keySet().forEach(cur -> {
			songPlayers.get(cur).destroy();
		});
		songPlayers.clear();
	}
}
