package com.overlands.kitpvp.manager;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.database.util.SQLOperation;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Stream;

public class LeaderboardManager {

    private final Map<LeaderType, List<LeaderType.LeaderData>> topLeaders = Maps.newHashMap();
    private final Map<LeaderType, ArrayList<LeaderType.LeaderData>> leaders = Maps.newHashMap();
    private final Map<LeaderType, Boolean> loaded = Maps.newHashMap();

    public LeaderboardManager() {
        Stream.of(LeaderType.values()).forEach(type -> loaded.put(type, false));

        for (LeaderType type : LeaderType.values()) {
            leaders.put(type, Lists.newArrayList());
        }

        new BukkitRunnable() {
            @Override
            public void run() {
                for (LeaderType type : LeaderType.values()) {
                    updateTop(type);
                }
            }
        }.runTaskTimerAsynchronously(Main.get(), 0,6000);
    }

    public void resetLeader(LeaderType type) {
        leaders.get(type).clear();
    }

    public void updateLocalData(LeaderType type) {
        loaded.put(type, false);
        topLeaders.remove(type);
        topLeaders.put(type, getTop(type));
        loaded.put(type, true);
    }

    public void updateData(LeaderType type, String name, UUID uuid, int level, int generalLevel, int prestigeLevel, int kills, int score, long timePlayed, int skillsUsed, double damage) {
        leaders.get(type).add(new LeaderType.LeaderData(name, uuid, level, generalLevel, prestigeLevel, kills, score, timePlayed, skillsUsed, damage));
    }

    public void requestUpdate() {
        for (LeaderType type : LeaderType.values()) {
           updateTop(type);
        }
    }

    public boolean loaded(LeaderType type) {
        return loaded.get(type);
    }

    private List<LeaderType.LeaderData> getTop(LeaderType type) {
        final ArrayList<LeaderType.LeaderData> pData = new ArrayList<>(leaders.get(type));
        pData.sort(new LeaderType.LeaderComparator(type));
        return pData.subList(0, Math.min(15, pData.size()));
    }

    public List<LeaderType.LeaderData> getTopList(LeaderType type) {
        return topLeaders.get(type);
    }

    public LeaderType.LeaderData getData(String name, LeaderType type) {
        return leaders.get(type).stream().filter(cur -> cur.getName().equalsIgnoreCase(name)).findAny().orElse(null);
    }

    private void updateTop(LeaderType type) {
        new SQLOperation() {
            final Connection connection = Main.get().getDB().getConnection().get();
            PreparedStatement preparedStatement = null;
            ResultSet resultSet = null;
            @Override
            public void onExecute() throws SQLException {
                String query = "SELECT `uuid`, `playername`, `level`, `general_level`, `prestigeLevel`, `kills`, " +
                        "`score`, `timePlayed`, `skillsUsed`, `damageDone`" +
                        " FROM `player_data` GROUP BY `uuid`" +
                        " ORDER BY `" + type.toString() +
                        "` DESC LIMIT " + 15 + ";";

                preparedStatement = connection.prepareStatement(query);
                resultSet = preparedStatement.executeQuery();
                resetLeader(type);
                try {
                    while (resultSet.next()) {
                        String uuid = resultSet.getString("uuid");
                        String name = Bukkit.getOfflinePlayer(UUID.fromString(uuid)).getName();
                        if (name == null) name = resultSet.getString("playername");

                        updateData(type, name, UUID.fromString(uuid),
                                resultSet.getInt("level"),
                                resultSet.getInt("general_level"),
                                resultSet.getInt("prestigeLevel"),
                                resultSet.getInt("kills"),
                                resultSet.getInt("score"),
                                resultSet.getLong("timePlayed"),
                                resultSet.getInt("skillsUsed"),
                                resultSet.getDouble("damageDone"));
                    }

                } finally {
                    if (preparedStatement != null)
                        preparedStatement.close();
                    if (resultSet != null)
                        resultSet.close();
                }
                updateLocalData(type);
            }

        }.writeOperation(Main.get().getDB().getExecutor(), Main.get().getLogger(), "An internal error occurred while updating leaderboard data.");
    }


    public enum LeaderType {
        LEVEL,
        GENERAL_LEVEL,
        PRESTIGE_LEVEL,
        KILLS,
        SCORE,
        TIME_PLAYED,
        SKILLS_USED,
        DAMAGE_DONE;

        public static LeaderType matchType(String string) {
            for (LeaderType type : LeaderType.values()) {
                if (type.toString().equalsIgnoreCase(string)) {
                    return type;
                }
            }
            return null;
        }

        @Override
        public String toString() {
            switch (this) {
                case KILLS:
                    return "kills";
                case GENERAL_LEVEL:
                    return "general_level";
                case PRESTIGE_LEVEL:
                    return "prestigeLevel";
                case LEVEL:
                    return "level";
                case SCORE:
                    return "score";
                case TIME_PLAYED:
                    return "timePlayed";
                case DAMAGE_DONE:
                    return "damageDone";
                case SKILLS_USED:
                    return "skillsUsed";
            }
            return null;
        }

        public static class LeaderData {
            private String name;
            private String uuid;
            private int level;
            private int generalLevel;
            private int prestigeLevel;
            private int kills;
            private int score;
            private long timePlayed;
            private int skillsUsed;
            private double damageDone;

            public LeaderData(String name, UUID uuid, int level, int generalLevel, int prestigeLevel, int kills, int score, long timePlayed, int skillsUsed, double damage) {
                this.name = name;
                this.uuid = uuid.toString();
                this.level = level;
                this.generalLevel = generalLevel;
                this.prestigeLevel = prestigeLevel;
                this.kills = kills;
                this.score = score;
                this.timePlayed = timePlayed;
                this.skillsUsed = skillsUsed;
                this.damageDone = damage;
            }

            public String getName() {
                return name;
            }

            public String getUUID() {
                return uuid;
            }

            public int getLevel() {
                return level;
            }

            public int getGeneralLevel() {
                return generalLevel;
            }

            public int getPrestigeLevel() {
                return prestigeLevel;
            }

            public int getKills() {
                return kills;
            }

            public int getScore() {
                return score;
            }

            public long getTimePlayed() {
                return timePlayed;
            }

            public int getSkillsUsed() {
                return skillsUsed;
            }

            public double getDamageDone() {
                return damageDone;
            }
        }

        public static class LeaderComparator implements Comparator<LeaderData> {
            private final LeaderType type;

            public LeaderComparator(LeaderType type) {
                this.type = type;
            }

            @Override
            public int compare(final LeaderType.LeaderData f1, final LeaderType.LeaderData f2) {
                switch (type) {
                    case LEVEL:
                        return f2.getLevel() - f1.getLevel();
                    case GENERAL_LEVEL:
                        return f2.getGeneralLevel() - f1.getGeneralLevel();
                    case PRESTIGE_LEVEL:
                        return f2.getPrestigeLevel() - f1.getPrestigeLevel();
                    case KILLS:
                        return f2.getKills() - f1.getKills();
                    case SCORE:
                        return f2.getScore() - f1.getScore();
                    case TIME_PLAYED:
                        return (int) (f2.getTimePlayed() - f1.getTimePlayed());
                    case SKILLS_USED:
                        return f2.getSkillsUsed() - f1.getSkillsUsed();
                    case DAMAGE_DONE:
                        return Double.compare(f2.getDamageDone(), f1.getDamageDone());
                    default:
                        return 0;
                }
            }
        }
    }
}
