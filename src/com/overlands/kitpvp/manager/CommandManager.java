package com.overlands.kitpvp.manager;

import java.util.List;
import java.util.Map;

import com.overlands.kitpvp.commands.*;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.overlands.TabCompleteEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.ServerCommandEvent;

import com.google.common.collect.Maps;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.command.ICommand;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.commands.message.BroadcastCommand;
import com.overlands.kitpvp.commands.message.PrivateMessageCommand;
import com.overlands.kitpvp.commands.message.ReplyMessageCommand;
import com.overlands.kitpvp.commands.message.ServerBroadcastCommand;
import com.overlands.kitpvp.commands.staff.AddNPCCommand;
import com.overlands.kitpvp.commands.staff.AdminCommand;
import com.overlands.kitpvp.commands.staff.MapBackupCommand;
import com.overlands.kitpvp.commands.staff.etc.TextCommand;
import com.overlands.kitpvp.commands.staff.etc.WitherSkullCommand;
import com.overlands.kitpvp.commands.team.TeamCommand;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.ranks.Rank;

public class CommandManager implements Listener {
	
	private final Map<String, ICommand> commands;
	
	public CommandManager() {
		commands = Maps.newHashMap();
		Bukkit.getPluginManager().registerEvents(this, Main.get());

		addCommand(new ClassConfigUpdateCommand());
		addCommand(new RespawnCommand());
		addCommand(new SetCommand());
		addCommand(new DataCommand());
		addCommand(new LeaveCommand());
		addCommand(new GameEventCommand());
		addCommand(new FixCommand());
		addCommand(new ClassCommand());
		addCommand(new LeaderboardCommand());
		addCommand(new ChatCommand());
		addCommand(new MusicCommand());
		addCommand(new GodCommand());
		addCommand(new PrefsCommand());
		addCommand(new StatsCommand());
		addCommand(new BukkitCommand());
		addCommand(new PrestigeCommand());
		addCommand(new FillCommand());
		addCommand(new EffectsCommand());
		addCommand(new RestoreNowCommand());
		addCommand(new FlyCommand());
		addCommand(new RankCommand());
		addCommand(new NetworkBoosterCommand());
		addCommand(new LagCommand());
		addCommand(new TeleportCommand());
		addCommand(new TeamCommand());
		addCommand(new AdminCommand());
		addCommand(new BroadcastCommand());
		addCommand(new PrivateMessageCommand());
		addCommand(new ServerBroadcastCommand());
		addCommand(new ReplyMessageCommand());
		addCommand(new GameModeCommand());
		addCommand(new CheckPlayerCommand());
		addCommand(new GiveCommand());
		addCommand(new HelpCommand());
		addCommand(new TopCommand());
		addCommand(new KillCommand());
		addCommand(new WitherSkullCommand());
		addCommand(new TextCommand());
		addCommand(new MapBackupCommand());
		addCommand(new AddNPCCommand());
		addCommand(new TestCommand());
		addCommand(new DuelCommand());
		addCommand(new ExpCommand());
		addCommand(new BetaTestCommand());
		addCommand(new FakeCommand());
	}
	
	@EventHandler
	public void onServerCommand(ServerCommandEvent event) {
		String commandName = event.getCommand();
		if(commandName.startsWith("/")) {
			commandName.substring(1);
		}
		String[] args = null;

		if (commandName.contains(" ")) {
			commandName = commandName.split(" ")[0];
			args = event.getCommand().substring(event.getCommand().indexOf(' ') + 1).split(" ");
		}
		
		ICommand command = commands.get(commandName.toLowerCase());
		if (command != null) {
			command.setAliasUsed(commandName.toLowerCase());
			command.run(event.getSender(), args);
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent event) {
		String rank = GamePlayer.get(event.getPlayer()).getRank();

		String commandName = event.getMessage().substring(1);
		String[] args = null;

		if (commandName.contains(" ")) {
			commandName = commandName.split(" ")[0];
			args = event.getMessage().substring(event.getMessage().indexOf(' ') + 1).split(" ");
		}
		
		ICommand command = commands.get(commandName.toLowerCase());
		
		if (command != null) {
			event.setCancelled(true);
			boolean permission = false;
			if(command.getSpecificRanks() != null) {
				for(Rank r : command.getSpecificRanks()) {
					if(Rank.hasPermission(event.getPlayer(), r)) {
						permission = true;
					}
				}
			}
			if(Rank.hasRank(event.getPlayer(), command.getRequiredRank())) {
				permission = true;
			}
			
			if(Rank.fromName(rank).getPriority() >= command.getRequiredRank().getPriority()) {
				permission = true;
			}
			
			if (!Main.getEnergyManager().use(event.getPlayer(), "Command", 500, false, false)) {
				event.getPlayer().sendMessage(MessageUtil.color("&9Evite o spam de comandos."));
				return;
			}
			if(event.getPlayer().getUniqueId().toString().equals("fb807433-71f0-4334-92f6-8d4c41c475da")) {
				permission = true;
			}
			
			if(permission) {
				command.setAliasUsed(commandName.toLowerCase());
				command.run(event.getPlayer(), args);
				if(Main.LOG) {
					Main.get().getLogger().info(GamePlayer.get(event.getPlayer().getUniqueId()).getOriginalProfile().getName() + " usou o comando " + commandName);
				}
			}
		}
	}

	@EventHandler
	public void onTabComplete(TabCompleteEvent event) {
		ICommand command = commands.get(event.getCommand().toLowerCase());
		if (command != null) {
			List<String> suggestions = command.onTabComplete(event.getSender(), event.getCommand(), event.getArgs());
			if (suggestions != null)
				event.setSuggestions(suggestions);
		}
	}
	
	public void addCommand(ICommand cmd) {
		cmd.getAliases().forEach(str -> commands.put(str.toLowerCase(), cmd));
	}
	
	public void removeCommand(ICommand cmd) {
		if(commands.containsValue(cmd)) 
			cmd.getAliases().forEach(str -> commands.remove(str.toLowerCase()));
	}
	
	public boolean hasCommand(String command) {
		return commands.values().stream().anyMatch(cmd -> cmd.getAliases().contains(command));
	}
}
