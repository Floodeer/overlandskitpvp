package com.overlands.kitpvp.ranks;

import static java.util.Comparator.comparing;
import static java.util.Comparator.reverseOrder;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.google.common.collect.Maps;
import com.overlands.core.utils.Util;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;

public enum Rank {

	OWNER("Owner", ChatColor.DARK_RED, 9),
	ADMIN("Admin", ChatColor.RED, 8),
	DEVELOPER("Dev", ChatColor.RED, 8),
	MOD("Mod", ChatColor.DARK_GREEN, 7),
	LEAD_BUILDER("L_Builder", ChatColor.DARK_AQUA, 6),
	BUILDER("Builder", ChatColor.DARK_AQUA, 6),
	YOUTUBER("Youtuber", ChatColor.GOLD, 5),
	STREAMER("Streamer", ChatColor.LIGHT_PURPLE, 5),
	TESTER("Tester", ChatColor.LIGHT_PURPLE, 5),
	MVPPLUS("Mvpplus", ChatColor.AQUA, 4),
	MVP("Mvp", ChatColor.AQUA, 3),
	VIP("Vip", ChatColor.GREEN, 2),
	DEFAULT("Default", ChatColor.GRAY, 1);
	
	String name;
	ChatColor color;
	int priority;

	Rank(String name, ChatColor color, int priority) {
		this.name = name;
		this.color = color;
		this.priority = priority;
	}
	
	public ChatColor getColor() {
		return color;
	}
	
	public String getChatTag() {
		if(this == Rank.MVPPLUS) {
			return color + Util.Bold + "MVP" + ChatColor.GOLD + "+" + ChatColor.RESET + color +  " ";
		}else if(this == DEFAULT) {
			return color + "";
		}
		return color +  Util.Bold +  name.toUpperCase() + ChatColor.RESET + color + " ";
	}
	
	public String getScoreboardTag() {
		if(this == Rank.MVPPLUS) {
			return color + "MVP" + ChatColor.GOLD + "+" + getColor() + " ";
		}else if(this == DEFAULT) {
			return color + "";
		}else{
			return color + Util.Bold + toString().toUpperCase() + getColor() + " ";
		}
	}
	
	public String getRawTag() {
		return this == DEFAULT ? "" : name.toUpperCase();
	}
	
	public String getColoredTag() {
		return this == DEFAULT ? "" : color +  Util.Bold + name.toUpperCase();
	}
	
	public int getPriority() {
		return priority;
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	public static Rank fromName(String name) {
		for(Rank r : Rank.values()) {
			if(r.toString().equalsIgnoreCase(name))
				return r;
		}
		return null;
	}
	
	public static ChatColor color(Player p){
		return Rank.fromName(GamePlayer.get(p.getUniqueId()).getRank()).getColor();
	}
	
	
	public static boolean isStaff(Player player) {
		String r = GamePlayer.get(player).getRank();
		Rank rr = fromName(r);
		
		return rr.getPriority() >= 6;
	}
	
	public static boolean isMod(Player player) {
		return Rank.fromName(GamePlayer.get(player.getUniqueId()).getRank()).getPriority() >= 7;
	}
	
	public static boolean isAdmin(Player player) {
		return Rank.fromName(GamePlayer.get(player.getUniqueId()).getRank()).getPriority() >= 8;
	}
	
	public static boolean isYoutuber(Player player) {
		return Rank.fromName(GamePlayer.get(player.getUniqueId()).getRank()).getPriority() == 5;
	}

	public static boolean isVIP(Player player) {
		return Rank.fromName(GamePlayer.get(player.getUniqueId()).getRank()).getPriority() >= 2;
	}
	
	public static boolean isRank(Player player, Rank rank) {
		return Rank.fromName(GamePlayer.get(player.getUniqueId()).getRank()) == rank;
	}
	
	public static boolean hasRank(Player player, Rank rank) {
		String r = GamePlayer.get(player).getRank();

		return r.equalsIgnoreCase(rank.toString());
	}
	
	public static boolean hasRank(Player player, Rank[] rank) {
		for(Rank r : rank) {
			if(GamePlayer.get(player.getUniqueId()).getRank().equalsIgnoreCase(r.toString())) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean hasPermission(Player player, Rank rank) {
		String r = GamePlayer.get(player).getRank();
		Rank rr = fromName(r);
        return rr.getPriority() >= rank.getPriority();
    }
}
