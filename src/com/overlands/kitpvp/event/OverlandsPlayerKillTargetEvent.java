package com.overlands.kitpvp.event;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class OverlandsPlayerKillTargetEvent extends Event {
	
	private Player player;
	private Player target;
	private Location loc;
	
	public OverlandsPlayerKillTargetEvent(Player player, Player target, Location loc) {
		this.player = player;
		this.target = target;
		this.loc = loc;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public Player getTarget() {
		return target;
	}
	
	public Location getLocation() {
		return loc;
	}

	private static final HandlerList handlers = new HandlerList();
	 
	public HandlerList getHandlers() {
	    return handlers;
	}
	 
	public static HandlerList getHandlerList() {
	    return handlers;
	}

}
