package com.overlands.kitpvp.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class OverlandsPlayerLevelUpEvent extends Event {
	
	private Player player;
	private int rank;
	private int from;
	
	public OverlandsPlayerLevelUpEvent(Player player, int from, int rank) {
		this.player = player;
		this.rank = rank;
		this.from = from;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public int getFrom() {
		return from;
	}
	
	public int getRank() {
		return rank;
	}

	private static final HandlerList handlers = new HandlerList();
	 
	public HandlerList getHandlers() {
	    return handlers;
	}
	 
	public static HandlerList getHandlerList() {
	    return handlers;
	}

}
