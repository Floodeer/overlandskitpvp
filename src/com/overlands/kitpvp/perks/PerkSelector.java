package com.overlands.kitpvp.perks;

import com.google.common.collect.Lists;
import com.overlands.core.utils.MenuUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class PerkSelector implements Listener {

    private static ItemStack no(String name, List<String> lore) {
        lore.add(MessageUtil.color(" "));
        lore.add(MessageUtil.color("§cVocê não tem esse perk."));
        return ItemFactory.create(Material.STAINED_GLASS_PANE, name, lore, (byte) 14);
    }

    private static boolean has(Player p, PerkType c) {
        return GamePlayer.get(p).getPerks().contains(c.toString().toUpperCase());
    }

    private static boolean sel(Player p, PerkType c) {
        return GamePlayer.get(p).getSelectedPerks().contains(c.toString().toUpperCase());
    }

    private static boolean hasReachedPerkLimit(Player p) {
        GamePlayer gp = GamePlayer.get(p);
        if (gp.getPrestigeLevel() != 0) {
            return false;
        }

        int perks = gp.getSelectedPerks().size();
        int level = gp.getLevel();

        int maxPerksAllowed;
        String message = null;

        if (level >= 120) {
            return true;
        } else if (level >= 100) {
            maxPerksAllowed = 9;
            if (perks >= maxPerksAllowed) {
                message = "&cVocê precisa alcançar o nível &e120 &cpara desbloquear todos os perks.";
            }
        } else if (level >= 65) {
            maxPerksAllowed = 5;
            if (perks >= maxPerksAllowed) {
                message = "&cVocê precisa alcançar o nível &e100 &cpara desbloquear slots para 9 perks.";
            }
        } else if (level >= 30) {
            maxPerksAllowed = 3;
            if (perks >= maxPerksAllowed) {
                message = "&cVocê precisa alcançar o nível &e65 &cpara desbloquear slots para 5 perks.";
            }
        } else if (level >= 10) {
            maxPerksAllowed = 2;
            if (perks >= maxPerksAllowed) {
                message = "&cVocê precisa alcançar o nível &e30 &cpara desbloquear slots para 3 perks.";
            }
        } else {
            maxPerksAllowed = 0;
            if (perks > maxPerksAllowed) {
                message = "&cVocê precisa alcançar o nível &e10 &cpara desbloquear o uso de perks.";
            }
        }

        if (message != null) {
            p.sendMessage(MessageUtil.color(message));
        }

        return perks >= maxPerksAllowed;
    }

    private static ItemStack green(String name) {
        return ItemFactory.create(Material.INK_SACK, name, (byte) 10);
    }

    private static ItemStack cyan(String name) {
        return ItemFactory.create(Material.INK_SACK, name, (byte) 8);
    }

    static ItemStack soon(String nome, String lore) {
        return ItemFactory.create(Material.STAINED_GLASS_PANE, nome, lore, 1, (byte) 14);
    }

    static ItemStack soonDye() {
        return ItemFactory.create(Material.INK_SACK, MessageUtil.color("§c???"), (byte) 5);
    }


    private static List<String> getPerkInfo(PerkType perk) {
        ArrayList<String> perkInfo = Lists.newArrayList();
        switch (perk) {
            case VAMPIRE:
				perkInfo.add("§7A cura das sopas é modifica para apenas §c§l0.5❤§7.");
				perkInfo.add("§7A cada hit recupera §c§l0.5❤ §7de vida.");
				break;
            case BUFF:
                perkInfo.add("§7Recebe §eregeneração II §7por");
                perkInfo.add("§7§e4 §7segundos após uma kill.");
                break;
            case KILLER:
                perkInfo.add("§7Recebe §eforça I §7por §e3");
                perkInfo.add("§7segundos após uma kill.");
                break;
            case SAFER:
                perkInfo.add("§7Muda o estado para safe");
                perkInfo.add("§7após uma kill.");
                break;
            case SHARPNESS:
                perkInfo.add("§7A cada 5 kills recebe +1 nível");
                perkInfo.add("§7de §eSharpness §7na espada.");
                perkInfo.add(" ");
                perkInfo.add("§7Limite: §8Sharpness III");
                break;
            case TANKER:
                perkInfo.add("§7Recebe §eresistência II §7por");
                perkInfo.add("§e4 §7segundos após uma kill.");
                break;
            case ENDERFALL:
                perkInfo.add("§7Reduz o dano de queda recebido");
                perkInfo.add("§7em §c36%§7.");
                break;
            case EXPERIENT:
                perkInfo.add("§7Recebe §c+25% §7de");
                perkInfo.add("§7experiência.");
                break;
            case STREAKER:
                perkInfo.add("§7Recebe mais experiência a");
                perkInfo.add("§7cada §c3 killstreak§7.");
                break;
            case EXTRA:
                perkInfo.add("§7Recebe §cabsorção III §7");
                perkInfo.add("§7por §e14 §7segundos");
                perkInfo.add("§7após uma kill.");
                break;
            case REFIL:
                perkInfo.add("§7Recebe §c2 §7sopas");
                perkInfo.add("§7após uma kill.");
                break;
            case HEAD_HUNTER:
                perkInfo.add("§7Matar o §ctarget §7da partida");
                perkInfo.add("§7consede §c+50% §7de");
                perkInfo.add("§7recompensas.");
                break;

            case BETTER_SOUP:
                perkInfo.add("§7Sopa consede regeneração §cII");
                perkInfo.add("§7durante §e3 §7segundos,");
                perkInfo.add("§7cura §a+0.5 de vida.");
                break;

            default:
                break;
        }
        return perkInfo;
    }

    public static void show(Player p) {
        MenuUtils menu = new MenuUtils(Main.get(), p, "§e§lPerks", 6);
        ItemStack killer = ItemFactory.create(Material.DIAMOND_SWORD, "§7Killer", getPerkInfo(PerkType.KILLER));
        ItemStack safer = ItemFactory.create(Material.GOLDEN_APPLE, "§7Safer", getPerkInfo(PerkType.SAFER));
        ItemStack experient = ItemFactory.create(Material.EXP_BOTTLE, "§7Experient", getPerkInfo(PerkType.EXPERIENT));
        ItemStack buff = ItemFactory.create(Material.GOLDEN_APPLE, "§7Buff", getPerkInfo(PerkType.BUFF));
        ItemStack extra = ItemFactory.create(Material.APPLE, "§7Extra", getPerkInfo(PerkType.EXTRA));
        ItemStack enderfall = ItemFactory.create(Material.ENDER_PEARL, "§7Enderfall", getPerkInfo(PerkType.ENDERFALL));
        ItemStack refil = ItemFactory.create(Material.EYE_OF_ENDER, "§7Refil", getPerkInfo(PerkType.REFIL));
        ItemStack tanker = ItemFactory.create(Material.IRON_CHESTPLATE, "§7Tanker", getPerkInfo(PerkType.TANKER));
        ItemStack sharpness = ItemFactory.create(Material.IRON_SWORD, "§7Sharpness", getPerkInfo(PerkType.SHARPNESS));
        ItemStack hhunter = ItemFactory.createSkull(p, "§7Head Hunter", getPerkInfo(PerkType.HEAD_HUNTER));
        ItemStack explosive = ItemFactory.create(Material.ARROW, "§7Explosive", getPerkInfo(PerkType.STREAKER));
        ItemStack soup = ItemFactory.create(Material.MUSHROOM_SOUP, "§7Sopa§b+", getPerkInfo(PerkType.BETTER_SOUP));
        ItemStack vampire = ItemFactory.create(Material.SPIDER_EYE, "§7Vampire§b+", getPerkInfo(PerkType.VAMPIRE));
        ItemStack none = soon("§c???", null);

        //uplines
        menu.setItem(10, has(p, PerkType.KILLER) ? ItemFactory.clearClientAttributes(killer) : no("§cKiller", getPerkInfo(PerkType.KILLER)));
        menu.setItem(11, has(p, PerkType.SAFER) ? ItemFactory.clearClientAttributes(safer) : no("§cSafer", getPerkInfo(PerkType.SAFER)));
        menu.setItem(12, has(p, PerkType.EXPERIENT) ? experient : no("§cExperient", getPerkInfo(PerkType.EXPERIENT)));
        menu.setItem(13, has(p, PerkType.BUFF) ? buff : no("§7Buff", getPerkInfo(PerkType.BUFF)));
        menu.setItem(14, has(p, PerkType.EXTRA) ? extra : no("§cExtra", getPerkInfo(PerkType.EXTRA)));
        menu.setItem(15, has(p, PerkType.ENDERFALL) ? enderfall : no("§cEnderfall", getPerkInfo(PerkType.ENDERFALL)));
        menu.setItem(16, has(p, PerkType.REFIL) ? refil : no("§cRefil", getPerkInfo(PerkType.REFIL)));
        menu.setItem(28, has(p, PerkType.TANKER) ? tanker : no("§cTanker", getPerkInfo(PerkType.TANKER)));
        menu.setItem(29, has(p, PerkType.SHARPNESS) ? sharpness : no("§cSharpness", getPerkInfo(PerkType.SHARPNESS)));
        menu.setItem(30, has(p, PerkType.HEAD_HUNTER) ? hhunter : no("§cHead Hunter", getPerkInfo(PerkType.HEAD_HUNTER)));
        menu.setItem(31, has(p, PerkType.STREAKER) ? explosive : no("§cStreaker", getPerkInfo(PerkType.STREAKER)));
        menu.setItem(32, has(p, PerkType.BETTER_SOUP) ? soup : no("§cSopa§b+", getPerkInfo(PerkType.BETTER_SOUP)));
        menu.setItem(33, has(p, PerkType.VAMPIRE) ? vampire : no("§cVampire", getPerkInfo(PerkType.VAMPIRE)));
        menu.setItem(34, none);

        //downlines
        menu.setItem(19, sel(p, PerkType.KILLER) ? green("§7Killer") : cyan("§7Killer"));
        menu.setItem(20, sel(p, PerkType.SAFER) ? green("§7Safer") : cyan("§7Safer"));
        menu.setItem(21, sel(p, PerkType.EXPERIENT) ? green("§7Experient") : cyan("§7Experient"));
        menu.setItem(22, sel(p, PerkType.BUFF) ? green("§7Buff") : cyan("§7Buff"));
        menu.setItem(23, sel(p, PerkType.EXTRA) ? green("§7Extra") : cyan("§7Extra"));
        menu.setItem(24, sel(p, PerkType.ENDERFALL) ? green("§7Enderfall") : cyan("§7Enderfall"));
        menu.setItem(25, sel(p, PerkType.REFIL) ? green("§7Refil") : cyan("§7Refil"));
        menu.setItem(37, sel(p, PerkType.TANKER) ? green("§7Tanker") : cyan("§7Tanker"));
        menu.setItem(38, sel(p, PerkType.SHARPNESS) ? green("§7Sharpness") : cyan("§7Sharpness"));
        menu.setItem(39, sel(p, PerkType.HEAD_HUNTER) ? green("§7Head Hunter") : cyan("§7Head Hunter"));
        menu.setItem(40, sel(p, PerkType.STREAKER) ? green("§7Streaker") : cyan("§7Streaker"));
        menu.setItem(41, sel(p, PerkType.BETTER_SOUP) ? green("§7Sopa§b+") : cyan("§7Sopa§b+"));
        menu.setItem(42, sel(p, PerkType.VAMPIRE) ? green("§7Vampire") : cyan("§7Vampire"));
        menu.setItem(43, soonDye());

        menu.setItem(45, ItemFactory.EasyFactory.getXArrow());
        menu.build();
        menu.showMenu(p);
    }

    @EventHandler
    public void onInventory(InventoryClickEvent e) {
        if (e.getInventory().getName().equalsIgnoreCase("§e§lPerks")) {
            Player p = (Player) e.getWhoClicked();
            GamePlayer gp = GamePlayer.get(p);
            e.setCancelled(true);
            e.setResult(Result.DENY);
            if (e.getCurrentItem() != null && e.getCurrentItem().getType() != Material.AIR) {
                String name = ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName());
                if (name.equalsIgnoreCase("Head Hunter")) {
                    name = "HEAD_HUNTER";
                } else if (name.equalsIgnoreCase("Sopa+")) {
                    name = "BETTER_SOUP";
                }
                PerkType perk = PerkType.fromString(name.toUpperCase());
                if (perk == null) {
                    Main.get().getLogger().info("Perk invalido: " + name.toUpperCase());
                    return;
                }
                if (!has(p, perk)) {
                    if (!p.hasPermission("kitpvp.unlockall")) {
                        p.sendMessage(MessageUtil.color("&9Você não tem esse perk."));
                        p.closeInventory();
                        return;
                    } else {
                        gp.getPerks().add(perk.toString().toUpperCase());
                        p.sendMessage(MessageUtil.color("&9Perk desbloqueado por permissão."));
                    }
                }

                if (!p.hasPermission("kitpvp.perkbypass")) {
                    if (hasReachedPerkLimit(p))
                        return;
                }
                if (sel(p, perk)) {
                    gp.getSelectedPerks().remove(perk.toString().toUpperCase());
                } else {
                    gp.getSelectedPerks().add(perk.toString().toUpperCase());
                }

                show(p);
            } else if (e.getRawSlot() == 45) {
                p.closeInventory();
            }
        }
    }
}
