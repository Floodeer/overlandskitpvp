package com.overlands.kitpvp.perks;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.overlands.core.utils.item.ItemFactory;

public enum PerkType {

	KILLER("KILLER", 0.24, ItemFactory.create(Material.DIAMOND_SWORD)), //
	SAFER("SAFER", 0.7, ItemFactory.create(Material.GOLDEN_APPLE)), //
	EXPERIENT("EXPERIENT", 0.3, ItemFactory.create(Material.EXP_BOTTLE)),
	BUFF("BUFF", 0.4, ItemFactory.create(Material.GOLD_INGOT)),
	EXTRA("EXTRA", 0.8, ItemFactory.create(Material.APPLE)),
	ENDERFALL("ENDERFALL", 0.9, ItemFactory.create(Material.ENDER_PEARL)), //
	REFIL("REFIL", 0.5, ItemFactory.create(Material.EYE_OF_ENDER)), //
	TANKER("TANKER", 0.2, ItemFactory.create(Material.IRON_CHESTPLATE)), //
	SHARPNESS("SHARPNESS", 0.1, ItemFactory.create(Material.IRON_SWORD)), //
	HEAD_HUNTER("HEAD_HUNTER", 0.6, ItemFactory.create(Material.SKULL_ITEM)), //
	STREAKER("STREAKER", 0.8, ItemFactory.create(Material.ARROW)), //
	BETTER_SOUP("BETTER_SOUP", 0.75, ItemFactory.create(Material.MUSHROOM_SOUP)),
	VAMPIRE("VAMPIRE", 0.21, ItemFactory.create(Material.SPIDER_EYE));
	
	String perk;
	double rarity;
	ItemStack item;
	
	PerkType(String perk) {
		this.perk = perk;
	}
	
	PerkType(String perk, double rarity) {
		this.perk = perk;
		this.rarity = rarity;
	}
	
	PerkType(String perk, double rarity, ItemStack item) {
		this.perk = perk;
		this.rarity = rarity;
		this.item = item;
	}
	
	public String getByName() {
		return this.perk;
	}
	
	public double getRarity() {
		return rarity;
	}
	
	public ItemStack getDisplay() {
		return item;
	}
	
	@Override
	public String toString() {
		return perk;
	}
	
	public static PerkType fromString(String str) {
		for(PerkType types : PerkType.values()) {
			if(types.getByName().equalsIgnoreCase(str)) {
				return types;
			}
		}
		return null;
	}
}
