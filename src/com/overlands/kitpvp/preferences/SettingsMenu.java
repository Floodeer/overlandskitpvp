package com.overlands.kitpvp.preferences;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.menus.IconMenu.OptionClickEvent;
import com.overlands.core.utils.menus.IconMenu.OptionClickEventHandler;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;

public class SettingsMenu {
	
 	public void show(GamePlayer gp) {
		Player player = gp.getPlayer();
		Main.getIconCore().create(player, MessageUtil.color("&7Preferências"), 9 * 6,
				new OptionClickEventHandler() {

					@Override
					public void onOptionClick(OptionClickEvent event) {
						if (event.getPosition() == 1 || event.getPosition() == 10) {
							if (gp.isFastRespawn()) {
								gp.setFastRespawn(false);
								player.sendMessage(MessageUtil.modulate("Respawn alternativo desativado."));
							} else {
								gp.setFastRespawn(true);
								player.sendMessage(MessageUtil.modulate("Respawn alternativo ativado"));
							}
						} else if (event.getPosition() == 3 || event.getPosition() == 12) {
							if (gp.isSpecialEffects()) {
								gp.setSpecialEffects(false);
								player.sendMessage(MessageUtil.modulate("Os efeitos especiais foram desativados."));
							} else {
								gp.setSpecialEffects(true);
								player.sendMessage(MessageUtil.modulate("Os efeitos especiais foram ativados."));
							}
						} else if (event.getPosition() == 5 || event.getPosition() == 14) {
							if (gp.isWarnings()) {
								gp.setWarnings(false);
								player.sendMessage(MessageUtil.modulate("As notificações foram desativadas."));
							} else {
								gp.setWarnings(true);
								player.sendMessage(MessageUtil.modulate("As notificações foram ativadas."));
							}
						} else if (event.getPosition() == 7 || event.getPosition() == 16) {
							if (gp.hasTitles()) {
								gp.setTitles(false);
								player.sendMessage(MessageUtil.modulate("Os títulos foram desativados."));
							} else {
								gp.setTitles(true);
								player.sendMessage(MessageUtil.modulate("Os títulos foram ativados."));
							}
						} else if (event.getPosition() == 19 || event.getPosition() == 28) {
							if (gp.respawnWithKit()) {
								gp.setRespawnWithKit(false);
								player.sendMessage(MessageUtil.modulate("Respawn com kit desativado."));
							} else {
								gp.setRespawnWithKit(true);
								player.sendMessage(MessageUtil.modulate("Respawn com kit ativado."));
							}
						} else if (event.getPosition() == 21 || event.getPosition() == 30) {
							if (gp.isChatToggled()) {
								gp.setChatToggled(false);
								player.sendMessage(MessageUtil.modulate("O seu chat foi habilitado."));
							} else {
								gp.setChatToggled(true);
								player.sendMessage(MessageUtil.modulate("O seu chat foi desabilitado."));
							}
						} else if (event.getPosition() == 23 || event.getPosition() == 32) {
							if (gp.isPMEnabled()) {
								gp.setPMEnabled(false);
								player.sendMessage(MessageUtil.modulate("As mensagens privadas foram desabilitadas."));
							} else {
								gp.setPMEnabled(true);
								player.sendMessage(MessageUtil.modulate("As mensagens privadas foram habilitadas."));
							}
						} else if (event.getPosition() == 25 || event.getPosition() == 34) {
							if (gp.hasPrivateProfile()) {
								gp.setPrivateProfile(false);
								player.sendMessage(MessageUtil.modulate("O seu perfil foi modificado para público."));
							} else {
								gp.setPrivateProfile(true);
								player.sendMessage(MessageUtil.modulate("O seu perfil foi modificado para privado."));
							}
						} else if(event.getPosition() == 49) {
							if(gp.getParticleQuality() == 1) {
								gp.setParticleQuality(2);
								player.sendMessage(MessageUtil.modulate("Qualidade modificada para &a&lRECOMENDADO&9."));
							}else if(gp.getParticleQuality() == 2) {
								gp.setParticleQuality(3);
								player.sendMessage(MessageUtil.modulate("Qualidade modificada para &4&lULTRA&9."));
							}else if(gp.getParticleQuality() == 3) {
								player.sendMessage(MessageUtil.modulate("Qualidade modificada para &e&lBAIXA&9"));
								gp.setParticleQuality(1);
							}
							
						} else if (event.getPosition() == 45) {
							event.setWillDestroy(true);
							event.setWillClose(true);
							return;
						}
						icons(player);
						Main.getIconCore().update(player);
					}
				});
		icons(player);
		Main.getIconCore().show(player);
	}

	private void icons(Player player) {
		GamePlayer gp = GamePlayer.get(player);

		List<String> qualityWarning = Lists.newArrayList();
		qualityWarning.add("&7Controla a qualidade de partículas,");
		qualityWarning.add("&7processamento de imagens e packets.");
		qualityWarning.add(" ");

		if (gp.getParticleQuality() == 1) {
			qualityWarning.add("&a&lBAIXA");

			qualityWarning.add("&7Desabilita grande parte das");
			qualityWarning.add("&7partículas.");
			
		} else if (gp.getParticleQuality() == 2) {
			qualityWarning.add("&e&lRECOMENDADO");

			qualityWarning.add("&7Qualidade padrão oferecida");
			qualityWarning.add("&7pelo Minecraft.");

		} else if (gp.getParticleQuality() == 3) {
			qualityWarning.add("&4&lULTRA");
			qualityWarning.add("&7Mostra todas as partículas que");
			qualityWarning.add("&7são criadas.");
			qualityWarning.add(" ");
			qualityWarning.add("&c&lAviso: &7Pode causar problemas de");
			qualityWarning.add("&7desempenho em computadores");
			qualityWarning.add("&7fracos.");
		}

		ItemStack item = ItemFactory.create(Material.STAINED_GLASS_PANE, (byte) 5);
		if (gp.getParticleQuality() == 2) {
			item = ItemFactory.create(Material.STAINED_GLASS_PANE, (byte) 4);
		} else if (gp.getParticleQuality() == 3) {
			item = ItemFactory.create(Material.STAINED_GLASS_PANE, (byte) 14);
		}

		ItemStack on = ItemFactory.create(Material.INK_SACK, MessageUtil.color("&aAtivado"),MessageUtil.color("&7Clique para desativar!"), (byte) 10);
		ItemStack off = ItemFactory.create(Material.INK_SACK, MessageUtil.color("&cDesativado"),MessageUtil.color("&7Clique para ativar!"), (byte) 8);

		Main.getIconCore().setOption(player, 1, ItemFactory.create(Material.COMPASS),
				MessageUtil.color("&9Spawn alternativo"),
				MessageUtil.color("&7Seu respawn será mais perto", "&7do campo de batalha."));

		Main.getIconCore().setOption(player, 3, ItemFactory.create(Material.REDSTONE),
				MessageUtil.color("&9Efeitos de tela"),
				MessageUtil.color("&7Permite que sua tela fique vermelha", "&7e habilita partículass especiais."));

		Main.getIconCore().setOption(player, 5, ItemFactory.create(Material.COMMAND),
				MessageUtil.color("&9Notificações"),
				MessageUtil.color("&7Permite mostrar notificações como", "&7de coins e exp após kill e avisos de", "&7habilidade pronta para ser usada."));

		Main.getIconCore().setOption(player, 7, ItemFactory.create(Material.SIGN), MessageUtil.color("&9Títulos"),
				MessageUtil.color("&7Permite que você veja", "&7títulos e subtítulos."));
		
		Main.getIconCore().setOption(player, 19, ItemFactory.create(Material.CHEST), MessageUtil.color("&9Respawnar com Kit"),
				MessageUtil.color("&7Após morrer você receberá", "&7os itens daclasse selecionada."));
		
		Main.getIconCore().setOption(player, 21, ItemFactory.create(Material.PAPER), MessageUtil.color("&9Mensagens"),
				MessageUtil.color("&7Permite que você receba", "&7mensagens no chat.", "&cVocê ainda receberá mensagens de", "&cadministradores e moderadores."));
		
		Main.getIconCore().setOption(player, 23, ItemFactory.create(Material.NAME_TAG), MessageUtil.color("&9Mensagem privada"),
				MessageUtil.color("&7Permite que você receba", "&7mensagens privadas."));

		Main.getIconCore().setOption(player, 25, ItemFactory.create(Material.BOOK), MessageUtil.color("&9Perfil privado"),
				MessageUtil.color("&7Permite que outros players", "&7acessem seu &6/perfil&7."));

		Main.getIconCore().setOption(player, 49, item, MessageUtil.color("&9Qualidade"),
				qualityWarning.toArray(qualityWarning.stream().toArray(String[]::new)));

		if (gp.isFastRespawn()) {
			Main.getIconCore().setOption(player, 10, on, on.getItemMeta().getDisplayName(),
                    on.getItemMeta().getLore().get(0));
		} else {
			Main.getIconCore().setOption(player, 10, off, off.getItemMeta().getDisplayName(),
                    off.getItemMeta().getLore().get(0));
		}
		if (gp.isSpecialEffects()) {
			Main.getIconCore().setOption(player, 12, on, on.getItemMeta().getDisplayName(),
                    on.getItemMeta().getLore().get(0));
		} else {
			Main.getIconCore().setOption(player, 12, off, off.getItemMeta().getDisplayName(),
                    off.getItemMeta().getLore().get(0));
		}
		if (gp.isWarnings()) {
			Main.getIconCore().setOption(player, 14, on, on.getItemMeta().getDisplayName(),
                    on.getItemMeta().getLore().get(0));
		} else {
			Main.getIconCore().setOption(player, 14, off, off.getItemMeta().getDisplayName(),
                    off.getItemMeta().getLore().get(0));
		}
		
		if (gp.hasTitles()) {
			Main.getIconCore().setOption(player, 16, on, on.getItemMeta().getDisplayName(),
                    on.getItemMeta().getLore().get(0));
		} else {
			Main.getIconCore().setOption(player, 16, off, off.getItemMeta().getDisplayName(),
                    off.getItemMeta().getLore().get(0));
		}

		if (gp.respawnWithKit()) {
			Main.getIconCore().setOption(player, 28, on, on.getItemMeta().getDisplayName(),
                    on.getItemMeta().getLore().get(0));
		} else {
			Main.getIconCore().setOption(player, 28, off, off.getItemMeta().getDisplayName(),
                    off.getItemMeta().getLore().get(0));
		}
		
		if (!gp.isChatToggled()) {
			Main.getIconCore().setOption(player, 30, on, on.getItemMeta().getDisplayName(),
					on.getItemMeta().getLore().get(0));
		} else {
			Main.getIconCore().setOption(player, 30, off, off.getItemMeta().getDisplayName(),
					off.getItemMeta().getLore().get(0));
		}
		
		if (gp.isPMEnabled()) {
			Main.getIconCore().setOption(player, 32, on, on.getItemMeta().getDisplayName(),
                    on.getItemMeta().getLore().get(0));
		} else {
			Main.getIconCore().setOption(player, 32, off, off.getItemMeta().getDisplayName(),
					off.getItemMeta().getLore().get(0));
		}
		
		if (gp.hasPrivateProfile()) {
			Main.getIconCore().setOption(player, 34, on, on.getItemMeta().getDisplayName(),
					on.getItemMeta().getLore().get(0));
		} else {
			Main.getIconCore().setOption(player, 34, off, off.getItemMeta().getDisplayName(),
					off.getItemMeta().getLore().get(0));
		}
		
		
		Main.getIconCore().setOption(player, 45, ItemFactory.EasyFactory.getXArrow(),
				MessageUtil.color("&7Fechar"));
		
		Main.getIconCore().setOption(player, 50, ItemFactory.create(Material.ENCHANTED_BOOK),
				MessageUtil.color("&cConquistas"));
		
		Main.getIconCore().setOption(player, 48, ItemFactory.create(Material.DIAMOND_SWORD),
				MessageUtil.color("&cEstatísticas de Jogos"));
	}

}
