package com.overlands.kitpvp.preferences;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import com.overlands.core.utils.location.LocationUtils;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.game.GamePlayer;

public class ParticleFilter {

	public static void playToLowFilter(ParticleEffect particle, float offsetX, float offsetY, float offsetZ, float speed,
			int amount, Location center, double range) {
		
		LocationUtils.getNearbyPlayers(center.getBlock().getLocation(), range).forEach(p -> {
			GamePlayer gp = GamePlayer.get(p);
			if (gp.getParticleQuality() < 1) {
				return;
			}
			particle.display(offsetX, offsetY, offsetZ, speed, amount, center, p);
		});
	}
	
	public static void playToLowFilter(ParticleEffect particle, Vector v, float speed, Location loc, double range) {
		LocationUtils.getNearbyPlayers(loc.getBlock().getLocation(), range).forEach(p -> {
			GamePlayer gp = GamePlayer.get(p);
			if (gp.getParticleQuality() < 1) {
				return;
			}
			particle.display(v, speed, loc, p);
		});
	}


	public static void playToMediumFilter(ParticleEffect particle, float offsetX, float offsetY, float offsetZ, float speed,
			int amount, Location center, double range) {
		
		LocationUtils.getNearbyPlayers(center.getBlock().getLocation(), range).forEach(p -> {
			GamePlayer gp = GamePlayer.get(p);
			if (gp.getParticleQuality() < 2) {
				return;
			}
			particle.display(offsetX, offsetY, offsetZ, speed, amount, center, p);
		});
	}
	
	public static void playToMediumFilter(ParticleEffect particle, Vector v, float speed, Location loc, double range) {
		LocationUtils.getNearbyPlayers(loc.getBlock().getLocation(), range).forEach(p -> {
			GamePlayer gp = GamePlayer.get(p);
			if (gp.getParticleQuality() < 2) {
				return;
			}
			particle.display(v, speed, loc, p);
		});
	}
	
	public static void playWithFilter(ParticleEffect particle, float offsetX, float offsetY, float offsetZ, float speed,
			int amount, Location center, double range, int requiredQuality) {
		
		LocationUtils.getNearbyPlayers(center.getBlock().getLocation(), range).forEach(p -> {
			GamePlayer gp = GamePlayer.get(p);
			if (gp.getParticleQuality() < requiredQuality) {
				return;
			}
			particle.display(offsetX, offsetY, offsetZ, speed, amount, center, p);
		});
	}

	public static List<Player> filter(Collection<Player> collection, int quality) {
		return collection
				.stream()
				.map(player -> (GamePlayer) GamePlayer.get(player))
				.filter(gp -> gp.getParticleQuality() >= quality)
				.map(gp -> (Player) gp.getPlayer())
				.collect(Collectors.toList());
	}

	public static List<Player> filterNearby(Location center, int area, int quality) {
		return  LocationUtils.getNearbyPlayers(center, area)
				.stream()
				.map(player -> (GamePlayer) GamePlayer.get(player))
				.filter(gp -> gp.getParticleQuality() >= quality)
				.map(gp -> (Player) gp.getPlayer())
				.collect(Collectors.toList());
	}
}
