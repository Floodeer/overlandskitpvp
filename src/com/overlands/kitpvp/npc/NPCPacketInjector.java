package com.overlands.kitpvp.npc;

import java.lang.reflect.Field;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import com.overlands.kitpvp.npc.NPCInteractEvent.Action;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import net.minecraft.server.v1_8_R3.Packet;

public class NPCPacketInjector {

	Player player;
	Channel channel;

	public NPCPacketInjector(Player player) {
		this.player = player;
	}

	public void registerChannel(String after, String name) {
		CraftPlayer cPlayer = (CraftPlayer) this.player;
		channel = cPlayer.getHandle().playerConnection.networkManager.channel;
		channel.pipeline().addAfter(after, name, new MessageToMessageDecoder<Packet<?>>() {
			@Override
			protected void decode(ChannelHandlerContext arg0, Packet<?> packet, List<Object> arg2) throws Exception {
				arg2.add(packet);
				readPacket(packet);
			}
		});
	}

	public void uninject() {
		if (channel.pipeline().get("PacketInjector") != null) {
			channel.pipeline().remove("PacketInjector");
		}
	}

	public void readPacket(Packet<?> packet) {
		if (packet.getClass().getSimpleName().equalsIgnoreCase("PacketPlayInUseEntity")) {
			int id = (Integer) getValue(packet, "a");
			if(getValue(packet, "action").toString().equalsIgnoreCase("ATTACK")) {
				NPCInteractEvent event = new NPCInteractEvent(player, id, Action.LEFT_CLICK);
				if(!event.isCancelled()) {
					Bukkit.getPluginManager().callEvent(event);
				}
			}
			if(getValue(packet, "action").toString().equalsIgnoreCase("INTERACT")) {
				NPCInteractEvent event = new NPCInteractEvent(player, id, Action.RIGHT_CLICK);
				if(!event.isCancelled()) {
					Bukkit.getPluginManager().callEvent(event);
				}
			}
		}
	}

	public void setValue(Object obj, String name, Object value) {
		try {
			Field field = obj.getClass().getDeclaredField(name);
			field.setAccessible(true);
			field.set(obj, value);
		} catch (Exception e) {
		}
	}

	public Object getValue(Object obj, String name) {
		try {
			Field field = obj.getClass().getDeclaredField(name);
			field.setAccessible(true);
			return field.get(obj);
		} catch (Exception e) {
		}
		return null;
	}

}