package com.overlands.kitpvp.npc;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class NPCInteractEvent extends Event implements Cancellable {

	private boolean cancelled;
	private int entityID;
	private Player who;
	private Action action;
	
	private static final HandlerList handlers = new HandlerList();
	
	public NPCInteractEvent(Player who, int ID, Action action) {
		this.entityID = ID;
		this.who = who;
		this.action = action;
	}
	 
	public HandlerList getHandlers() {
	    return handlers;
	}
	 
	public static HandlerList getHandlerList() {
	    return handlers;
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean value) {
		cancelled = value;
	}

	public int getEntityID() {
		return entityID;
	}

	public Player getPlayer() {
		return who;
	}
	
	public Action getAction() {
		return action;
	}
	
	public enum Action {
		RIGHT_CLICK,
		LEFT_CLICK;
	}
}