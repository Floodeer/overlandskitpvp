package com.overlands.kitpvp.npc;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang.RandomStringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Warning;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.gmail.filoghost.holographicdisplays.api.VisibilityManager;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Runner;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.levels.LevelFormatter;

import net.minecraft.server.v1_8_R3.DataWatcher;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.MinecraftServer;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntity.PacketPlayOutEntityLook;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityHeadRotation;
import net.minecraft.server.v1_8_R3.PacketPlayOutNamedEntitySpawn;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo.EnumPlayerInfoAction;
import net.minecraft.server.v1_8_R3.PlayerConnection;
import net.minecraft.server.v1_8_R3.PlayerInteractManager;
import net.minecraft.server.v1_8_R3.WorldServer;

public class StatsNPC {

	private static Map<Player, EntityPlayer> entityPacket = Maps.newHashMap();
	public static Map<Player, EntityPlayer> perPlayerPacket = Maps.newHashMap();
	private static  Map<Player, List<Hologram>> hologramsMap = Maps.newHashMap();
	private static Map<Player, ArmorStand> plates = Maps.newHashMap();
	
	public static void spawnPlayerNPC(Player p) {
		 removeNPC(p, false);
			if(Main.mapConfig.profile == null)
				return;
		Location l = Main.mapConfig.profile;
		

		MinecraftServer server = ((CraftServer) Bukkit.getServer()).getServer();
		WorldServer world = ((CraftWorld) Bukkit.getWorlds().get(0)).getHandle();
	
		GameProfile profile = ((CraftPlayer)p).getHandle().getProfile();
		Property property = profile.getProperties().get("textures").iterator().next();
		String texture = property.getValue();
	    String signature = property.getSignature();
	    
		GameProfile newProfile = new GameProfile(UUID.randomUUID(), RandomStringUtils.randomAlphanumeric(12));
	    newProfile.getProperties().put("textures", new Property("textures", texture, signature));
		setValue(newProfile, "name", MessageUtil.color("inv"));
		
		EntityPlayer npc = new EntityPlayer(server, world, newProfile, new PlayerInteractManager(world));
		npc.getBukkitEntity().setMetadata("NPC", new FixedMetadataValue(Main.get(), 0));
		npc.setLocation(l.getX(), l.getY(), l.getZ(), l.getYaw(), l.getPitch());
		PacketPlayOutNamedEntitySpawn packetN = new PacketPlayOutNamedEntitySpawn(npc);
		DataWatcher w = new DataWatcher(null);
		w.a(6, (float) 20);
		w.a(10, (byte) 127);
		setValue(packetN, "i", w);

		PlayerConnection connection = ((CraftPlayer) p).getHandle().playerConnection;
		connection.sendPacket(new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER, npc));
		connection.sendPacket(packetN);
		PacketPlayOutEntityLook packet = new PacketPlayOutEntityLook(npc.getId(), getFixRotation(l.getYaw()),getFixRotation(l.getPitch()), true);
		PacketPlayOutEntityHeadRotation packetHead = new PacketPlayOutEntityHeadRotation();
		setValue(packetHead, "a", npc.getId());
		setValue(packetHead, "b", getFixRotation(l.getYaw()));
		connection.sendPacket(packet);
		connection.sendPacket(packetHead);
		
		Runner.make(Main.get()).delay(25).run(() -> connection.sendPacket(new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER, npc)));
	    perPlayerPacket.put(p, npc);
	    
		ArmorStand namePlate = l.getWorld().spawn(l, ArmorStand.class);
		namePlate.setSmall(true);
		namePlate.setVisible(false);
		namePlate.setCustomNameVisible(false);
		npc.getBukkitEntity().setPassenger(namePlate);
		plates.put(p, namePlate);
		holograms(p);
	}
	
	public static void spawnPlayerNPCOnly(Player p) {
		 removeNPC(p, false);
			if(Main.mapConfig.profile == null)
				return;
		Location l = Main.mapConfig.profile;
		

		MinecraftServer server = ((CraftServer) Bukkit.getServer()).getServer();
		WorldServer world = ((CraftWorld) Bukkit.getWorlds().get(0)).getHandle();
	
		GameProfile profile = ((CraftPlayer)p).getHandle().getProfile();
		Property property = profile.getProperties().get("textures").iterator().next();
		String texture = property.getValue();
	    String signature = property.getSignature();
	    
		GameProfile newProfile = new GameProfile(UUID.randomUUID(), RandomStringUtils.randomAlphanumeric(12));
	    newProfile.getProperties().put("textures", new Property("textures", texture, signature));
		setValue(newProfile, "name", MessageUtil.color("inv"));
		
		EntityPlayer npc = new EntityPlayer(server, world, newProfile, new PlayerInteractManager(world));
		npc.getBukkitEntity().setMetadata("NPC", new FixedMetadataValue(Main.get(), 0));
		npc.setLocation(l.getX(), l.getY(), l.getZ(), l.getYaw(), l.getPitch());
		PacketPlayOutNamedEntitySpawn packetN = new PacketPlayOutNamedEntitySpawn(npc);
		DataWatcher w = new DataWatcher(null);
		w.a(6, (float) 20);
		w.a(10, (byte) 127);
		setValue(packetN, "i", w);

		PlayerConnection connection = ((CraftPlayer) p).getHandle().playerConnection;
		connection.sendPacket(new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER, npc));
		connection.sendPacket(packetN);
		PacketPlayOutEntityLook packet = new PacketPlayOutEntityLook(npc.getId(), getFixRotation(l.getYaw()),getFixRotation(l.getPitch()), true);
		PacketPlayOutEntityHeadRotation packetHead = new PacketPlayOutEntityHeadRotation();
		setValue(packetHead, "a", npc.getId());
		setValue(packetHead, "b", getFixRotation(l.getYaw()));
		connection.sendPacket(packet);
		connection.sendPacket(packetHead);
		
		Runner.make(Main.get()).delay(25).run(() -> connection.sendPacket(new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER, npc)));
	    perPlayerPacket.put(p, npc);
	}
	
	private static byte getFixRotation(float yawpitch) {
		return (byte) ((int) (yawpitch * 256.0F / 360.0F));
	}

	private static void setValue(Object obj, String name, Object value) {
		try {
			Field field = obj.getClass().getDeclaredField(name);
			field.setAccessible(true);
			field.set(obj, value);
		} catch (Exception e) {
		}
	}
	
	public static void clear(Player p) {
		if(hologramsMap.containsKey(p)) {
			hologramsMap.get(p).forEach(Hologram::delete);
			hologramsMap.remove(p);
		}
		if(plates.containsKey(p)) {
			plates.get(p).remove();
			plates.remove(p);
		}
	}
	
	public static void update(Player p) {
		clear(p);
		Runner.make(Main.get()).delay(3).run(() -> {
			holograms(p);
		});
	}
	
	private static void holograms(Player p) {
		GamePlayer gp = GamePlayer.get(p);
		
		List<Hologram> holos = Lists.newArrayList();
		
		Location location  = Main.mapConfig.profile;
		if(location == null)
			return;
		
		if(hologramsMap.containsKey(p)) {
			hologramsMap.get(p).forEach(Hologram::delete);
		}
		Location l = location.clone().add(0, 4.35, 0);
		
		Hologram h = HologramsAPI.createHologram(Main.get(), l);
		VisibilityManager v = h.getVisibilityManager();
		v.setVisibleByDefault(false);
		h.insertTextLine(0, MessageUtil.color("&6&lSeu perfil"));
		v.showTo(p);
		holos.add(h);
		
		List<String> holograms = new ArrayList<>();
		holograms.add(MessageUtil.color("Nível: &a" + LevelFormatter.format(gp.getLevel(), gp.getPrestigeLevel(), false, true)));
		holograms.add(MessageUtil.color("Exp: &a" + gp.getExp() + "&7/&b" + LevelFormatter.formatExpToNextLevel(gp.getLevel())));
		holograms.add(MessageUtil.color("Kills: &a" + gp.getKills()));
		holograms.add(MessageUtil.color("Score: &a" + gp.getScore()));
		holograms.add(MessageUtil.color("Conquistas: &a" + 0 + "&f/&b" + "???"));
		holograms.add(MessageUtil.color("&6&lCLIQUE PARA VER MAIS"));
		for(String s : holograms) {
			Hologram h1 = HologramsAPI.createHologram(Main.get(), l.subtract(0, 0.35, 0));
			VisibilityManager v1 = h1.getVisibilityManager();
			v1.setVisibleByDefault(false);
			h1.insertTextLine(0, s);
			v1.showTo(p);
			holos.add(h1);
		}
		hologramsMap.put(p, holos);
	}
	
	public static void removeNPC(Player p, boolean classNPC) {
		if(classNPC) {
			if (entityPacket.containsKey(p)) {
				PlayerConnection con = ((CraftPlayer) p).getHandle().playerConnection;
				con.sendPacket(new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER, entityPacket.get(p)));
				con.sendPacket(new PacketPlayOutEntityDestroy(entityPacket.get(p).getId()));
			}
		}else{
			if(perPlayerPacket.containsKey(p)) {
				PlayerConnection con = ((CraftPlayer) p).getHandle().playerConnection;
				con.sendPacket(new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER, perPlayerPacket.get(p)));
				con.sendPacket(new PacketPlayOutEntityDestroy(perPlayerPacket.get(p).getId()));
			}
		}
		if(hologramsMap.containsKey(p)) {
			hologramsMap.get(p).forEach(Hologram::delete);
		}
	}
}
