package com.overlands.kitpvp.game.team;

import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

import com.overlands.core.utils.JsonBuilder;
import com.overlands.core.utils.JsonBuilder.ClickAction;
import com.overlands.core.utils.MessageUtil;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;

public class TeamManager {
	
	public static void createInvite(GamePlayer player, GamePlayer target) {
		target.getRequests().add(player);
		target.setTeamRequest(true);
		
		JsonBuilder json = new JsonBuilder(player.getPlayer().getName())
				.withColor(ChatColor.AQUA)
				.withText(" convidou você para formar uma dupla. ").withColor(ChatColor.YELLOW)
				.withText("Clique para ").withColor(ChatColor.YELLOW)
				.withText("ACEITAR").withColor(ChatColor.GREEN).withColor(ChatColor.BOLD)
				.withClickEvent(ClickAction.RUN_COMMAND, "/time aceitar " + player.getPlayer().getName())
				.withText(" ou ").withColor(ChatColor.YELLOW)
				.withText("NEGAR").withColor(ChatColor.RED).withColor(ChatColor.BOLD)
				.withClickEvent(ClickAction.RUN_COMMAND, "/time ignorar " + player.getPlayer().getName())
				.withText(".").withColor(ChatColor.YELLOW);
		
		json.sendJson(target.getPlayer());
		
		new BukkitRunnable() {
			int seconds = 0;
			@Override
			public void run() {
				++seconds;
				if(seconds == 60 || target.hasTeamRequest()) {
					cancel();
					return;
				}
				if(seconds >= 0 && seconds % 25 == 0) {
					json.sendJson(target.getPlayer());
				}
			}
		}.runTaskTimer(Main.get(), 0, 20);
	}
	
	public static void denyInvite(GamePlayer inviter, GamePlayer target) {
		if(target.hasTeamRequest()) {
			if(target.getRequests().contains(inviter))
				target.getRequests().remove(inviter);
			target.setTeamRequest(false);
			if(inviter != null) {
				inviter.getPlayer().sendMessage(ChatColor.RED + target.getPlayer().getName() + MessageUtil.color(" &7ignorou seu pedido."));
			}
		}
	}
	
	public static void accept(GamePlayer inviter, GamePlayer target) {
		inviter.setTeamOwner(true);
		target.setTeamOwner(false);
		target.getRequests().clear();
		target.setTeamRequest(false);
		target.setTeam(inviter);
		inviter.setTeam(target);
		inviter.getPlayer().sendMessage(ChatColor.YELLOW + target.getPlayer().getName() + MessageUtil.color(" &7aceitou seu pedido!"));
		target.getPlayer().sendMessage(MessageUtil.color(" &9Você agora está em dupla com &e" + inviter.getPlayer().getName() + "&9."));
	}
	
	public static void disband(GamePlayer player, boolean msg) {
		if(player != null && player.getPlayer().isOnline()) {
			player.getPlayer().sendMessage(MessageUtil.color("&9Dupla desfeita."));
		}
		player.setTeamOwner(false);
		player.getTeam().setTeamOwner(false);
		player.getTeam().setTeam(null);
		player.setTeam(null);
	}
}
