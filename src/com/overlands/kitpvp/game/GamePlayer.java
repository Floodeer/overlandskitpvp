package com.overlands.kitpvp.game;


import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import com.overlands.kitpvp.npc.NPCPacketInjector;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import com.google.common.collect.Lists;
import com.mojang.authlib.GameProfile;
import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.database.DataManager.TABLE_TYPE;
import com.overlands.kitpvp.event.OverlandsPlayerLevelUpEvent;
import com.overlands.kitpvp.game.GamePlayer.PlayerState.State;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;
import com.overlands.kitpvp.game.levels.LevelFormatter;
import com.overlands.kitpvp.ranks.Rank;

public class GamePlayer {
	
	private UUID uuid;
	private Player player;
	
	//Database
	private String rank;
	
	private int kills;
	private int deaths;
	private int bowShots;
	private int bowHits;
	private int skillUsed;
	private int score;
	
	private double healingDone;
	private double damageDone;
	
	private double damageReceived;
	private double healingReceived;
	
	private int coins;
	private int mysteryBoxes;
	private int level;
	private int exp;
	private int generalLevel;
	private int prestigeLevel;
	
	private boolean isMuted;
	private String muteReason;
	
	private boolean isBanned;
	private String banReason;
	private long banEnd;
	
	private long timePlayed;
	
	private List<String> kits = Lists.newArrayList();
	private List<String> perks = Lists.newArrayList();
	private List<String> selectedPerks = Lists.newArrayList();
	private List<String> achievements = Lists.newArrayList();
	private List<String> contracts = Lists.newArrayList();
	
	//private List<FormattedKitLevel> kitLevels = Lists.newArrayList(); removed
	
	private List<String> effects = Lists.newArrayList();
	private List<String> selectedEffects = Lists.newArrayList();
	
	private GameClass selectedClass;
	private ClassType classType;
	
	//Preferences
	private boolean titles;
	private boolean warnings;
	private boolean specialEffects;
	private boolean fastRespawn;
	private boolean respawnWithKit;
	private boolean privateProfile;
	private boolean pmEnabled;
	private boolean isChatToggled;
	private int particleQuality;

	
	//Now to database;
	private int protocolVersion;
	
	//In-Game
	private PlayerState state;
	private int killStreak;
	private int energy;
	
	private GamePlayer lastMessage;
	private Game game;
	
	private LinkedList<GameTagged> taggedList = Lists.newLinkedList();
	
	private boolean hasResourcePack = false;
	private boolean fallDamage = false;
	private boolean openingMysteryBox = false;
	private boolean canGainEnergy = true;
	private boolean launched = false;
	private boolean hasShield = false;
	private boolean hasMoved = false;
	private boolean hasSpiritShield = false;
	private boolean EMPd = false;
	private double shieldPercentage;
	private double  protectedDamage = 0;
	private int tickets = 0;


	
	//Team
	private GamePlayer team = null;
	private List<GamePlayer> requests = Lists.newArrayList();
	private boolean isTeamOwner = false;
	private boolean hasTeamRequest = false;
	
	//Duel
	private boolean isInDuel;
	private GamePlayer duelRequest; 
	private boolean hasDuelRequest = false;
	
	//Loaded tables;
	private List<TABLE_TYPE> loadedTables = Lists.newArrayList();
	
	//staff
	private boolean isAdminMode;
	
	//player bukkit data
	private GameProfile originalGameProfile;

	//Minecraft Data
	private NPCPacketInjector packetInjectorket;
	
	public GamePlayer(UUID uuid) {
		this.setUUID(uuid);
		this.setPlayer(Bukkit.getPlayer(uuid));
		this.setState(new PlayerState(this, State.SAFE, 0));
		this.originalGameProfile = ((CraftPlayer)getPlayer()).getProfile();
	}
	
	public GameProfile getOriginalProfile() {
		return originalGameProfile;
	}
	
	public static class PlayerState {
		public enum State {
			SAFE("&aSeguro"),
			FIGHTING("&cLutando"),
			MVP("&4MVP"),
			EVENT("&6EVENTO"),
			DUELO("&cDUELO");
			
			String state;
			
			State(String state) {
				this.state = state;
			}
			
			@Override
			public String toString() {
				return state;
			}
		}
		
		private State state;
		private long time;
		private boolean out;
		
		public PlayerState(GamePlayer gp, State state, long time) {
			this.setState(state);
			this.setTime(time);
		}

		public long getTime() {
			return time;
		}

		public void setTime(long time) {
			this.time = time;
		}

		public State getState() {
			return state;
		}

		public void setState(State state) {
			this.state = state;
		}
		
		public String getName() {
			return state.toString();
		}
		
		public boolean isOut() {
			return out;
		}
		
		public void setOut(boolean b) {
			this.out = b;
		}
	}
	
	/** removed
	 * 	public static class FormattedKitLevel {
		String kit;
		String level;
		
		public FormattedKitLevel(String kit, String level2) {
			this.kit = kit;
			this.level = level2;	
		}
		
		public String getName() {
			return kit;
		}
		
		public int getLevelNumber() {
			return Integer.parseInt(level);
		}
		
		public void setLevel(int level) {
			this.level = Integer.toString(level);
		}
		
		@Override
		public String toString() {
			 return kit + ":" + level;
		}
	}
	
	 public void setKitsLevels(String resultCallback) {
		String[] parts = resultCallback.split(", ");
		for(String str : parts) {
			String name = Util.before(str, ":");
			String level = Util.after(str, ":");
			kitLevels.add(new FormattedKitLevel(name, level));
		}	
	}
	
	public List<FormattedKitLevel> getKitLevel() {
		return kitLevels;
	}
	
	public List<String> getFormattedKitLevel() {
		List<String> t = Lists.newArrayList();
		kitLevels.forEach(k -> t.add(k.toString()));
		return t;
	}
	
	public FormattedKitLevel getClass(String clazz) {
		return getKitLevel().stream().filter(k -> k.getName().equalsIgnoreCase(clazz)).findFirst().get();
	}
	 * 
	 */

	public int getKills() {
		return kills;
	}

	public void setKills(int kills) {
		this.kills = kills;
	}

	public int getDeaths() {
		return deaths;
	}

	public void setDeaths(int deaths) {
		this.deaths = deaths;
	}

	public int getBowShots() {
		return bowShots;
	}

	public void setBowShots(int bowShots) {
		this.bowShots = bowShots;
	}

	public int getBowHits() {
		return bowHits;
	}

	public void setBowHits(int bowHits) {
		this.bowHits = bowHits;
	}

	public int getSkillUsed() {
		return skillUsed;
	}

	public void setSkillUsed(int skillUsed) {
		this.skillUsed = skillUsed;
	}

	public double getHealingDone() {
		return healingDone;
	}

	public void setHealingDone(double healingDone) {
		this.healingDone = healingDone;
	}

	public double getDamageDone() {
		return damageDone;
	}

	public void setDamageDone(double damageDone) {
		this.damageDone = damageDone;
	}

	public double getDamageReceived() {
		return damageReceived;
	}

	public void setDamageReceived(double damageReceived) {
		this.damageReceived = damageReceived;
	}

	public double getHealingReceived() {
		return healingReceived;
	}

	public void setHealingReceived(double healingReceived) {
		this.healingReceived = healingReceived;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getExp() {
		return exp;
	}

	public void setExp(int exp) {
		this.exp = exp;
	}

	public int getPrestigeLevel() {
		return prestigeLevel;
	}

	public void setPrestigeLevel(int prestigeLevel) {
		this.prestigeLevel = prestigeLevel;
	}

	public List<String> getKits() {
		return kits;
	}

	public void setKits(List<String> kits) {
		this.kits = kits;
	}

	public List<String> getPerks() {
		return perks;
	}

	public void setPerks(List<String> perks) {
		this.perks = perks;
	}

	public int getCoins() {
		return coins;
	}

	public void setCoins(int coins) {
		this.coins = coins;
	}

	public int getKillStreak() {
		return killStreak;
	}

	public void setKillStreak(int killStreak) {
		this.killStreak = killStreak;
	}

	public GamePlayer getTeam() {
		return team;
	}

	public void setTeam(GamePlayer team) {
		this.team = team;
	}

	public UUID getUUID() {
		return uuid;
	}

	public void setUUID(UUID uuid) {
		this.uuid = uuid;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}
	
	public boolean isInGame() {
		return game != null;
	}

	public GameClass getSelectedClass() {
		return selectedClass;
	}

	public void setClass(GameClass selectedClass) {
		this.selectedClass = selectedClass;
	}

	public void setClassType(String clazz) {
		this.classType = ClassType.fromName(clazz);
		if(isInGame()) {
			Main.getGame().updateScoreboard(this, 1, MessageUtil.color("&fClasse: &b" +  getClassTypeFromEnum().toString().substring(0, 3).toUpperCase()));
		    Main.getGame().updateTeams(getPlayer());
		}
	}
	
	public void setClassType(ClassType clazz) {
		this.classType = clazz;
		if(isInGame()) {
			Main.getGame().updateScoreboard(this, 1, MessageUtil.color("&fClasse: &b" +  getClassTypeFromEnum().toString().substring(0, 3).toUpperCase()));
		    Main.getGame().updateTeams(getPlayer());
		}
	}
	
	public ClassType getClassTypeFromEnum() {
		return classType;
	}
	
	public boolean hasTitles() {
		return titles;
	}

	public void setTitles(boolean titles) {
		this.titles = titles;
	}

	public boolean isWarnings() {
		return warnings;
	}

	public void setWarnings(boolean warnings) {
		this.warnings = warnings;
	}

	public boolean isSpecialEffects() {
		return specialEffects;
	}

	public void setSpecialEffects(boolean specialEffects) {
		this.specialEffects = specialEffects;
	}

	public int getParticleQuality() {
		return particleQuality;
	}

	public void setParticleQuality(int particleQuality) {
		this.particleQuality = particleQuality;
	}

	public int getEnergy() {
		return energy;
	}

	public void setEnergy(int energy) {
		this.energy = energy;
	}

	public PlayerState getState() {
		return state;
	}

	public void setState(PlayerState state) {
		this.state = state;
	}
	
	public static GamePlayer get(Player p) {
		return Main.getPM().getPlayer(p.getUniqueId());
	}

	public static GamePlayer get(UUID id) {
		return Main.getPM().getPlayer(id);
	}

	
	public LinkedList<GameTagged> getTaggedList() {
		return taggedList;
	}

	public void addBalance(int x, boolean msg, String complement) {
		int multiplier = 1;
		String msgComplement = "";

		if (Rank.isStaff(player) || Rank.isYoutuber(player)) {
			multiplier = 5;
			msgComplement = "(5x rank";
		} else if (Rank.hasRank(player, Rank.MVPPLUS)) {
			multiplier = 4;
			msgComplement = "(4x rank";
		} else if (Rank.hasPermission(player, Rank.MVP)) {
			multiplier = 3;
			msgComplement = "(3x rank";
		} else if (Rank.hasPermission(player, Rank.VIP)) {
			multiplier = 2;
			msgComplement = "(2x rank";
		}

		if (Main.get().getServerManager().networkBooster) {
			msgComplement += ", 2x " + Main.get().getServerManager().boosterPlayer + "'s network booster) ";
			multiplier *= 2;
		} else {
			if (!Rank.hasRank(player, Rank.DEFAULT))
				msgComplement += ") ";
		}

		if (msgComplement.equals(") ")) {
			msgComplement = msgComplement.substring(2);
		}

		this.coins += x * multiplier;

		if (msg && isWarnings()) {
			String message = "&6+" + x + " coins! ";
			if (complement != null && !complement.isEmpty()) {
				message += "(" + complement + ") ";
			}
			message += msgComplement;

			getPlayer().sendMessage(MessageUtil.color(message));
		}

		if (isInGame()) {
			getGame().updateScoreboard(this, 6, MessageUtil.color("&fCoins: &b" + getCoins()));
		}
	}
	
	public void addExp(int x, boolean msg, String complement) {
		String msgComplement = "";
		if(Main.RUNNING_BOOSTER) {
			msgComplement = msgComplement + "(2x network booster) ";
			x = x*2;
		}
		if(msg && isWarnings()) {
			if(complement != null)
			getPlayer().sendMessage(MessageUtil.color("&9+" + x + " exp! (" + complement + ") " + msgComplement));
			else
				getPlayer().sendMessage(MessageUtil.color("&9+" + x + " exp! " + msgComplement));
		}
		
		int until = LevelFormatter.expUntilNextLevel(this);
		
		setExp(getExp()+x);
		
		if(getLevel() < 120 && getExp() + x >= until) {
			setExp(0);
			setLevel(getLevel()+1);
			setGeneralLevel(getGeneralLevel()+1);
			Bukkit.getPluginManager().callEvent(new OverlandsPlayerLevelUpEvent(getPlayer(), getLevel()-1, getLevel()));
		}
		if(isInGame()) {
			getGame().updateScoreboard(this, 8, MessageUtil.color("&fPróximo: &b" + LevelFormatter.expUntilNextLevel(this)));
		}
	}

	public long getTimePlayed() {
		return timePlayed;
	}

	public void setTimePlayed(long timePlayed) {
		this.timePlayed = timePlayed;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public boolean canTakeFallDamage() {
		return fallDamage;
	}

	public void setFallDamage(boolean fallDamage) {
		this.fallDamage = fallDamage;
	}

	public int getMysteryBoxes() {
		return mysteryBoxes;
	}

	public void setMysteryBoxes(int mysteryBoxes) {
		this.mysteryBoxes = mysteryBoxes;
	}

	public boolean isOpeningMysteryBox() {
		return openingMysteryBox;
	}

	public void setOpeningMysteryBox(boolean openingMysteryBox) {
		this.openingMysteryBox = openingMysteryBox;
	}

	public boolean isFastRespawn() {
		return fastRespawn;
	}

	public void setFastRespawn(boolean fastRespawn) {
		this.fastRespawn = fastRespawn;
	}

	public boolean canGainEnergy() {
		return canGainEnergy;
	}

	public void setCanGainEnergy(boolean canGainEnergy) {
		this.canGainEnergy = canGainEnergy;
	}

	public void setEMPd(boolean emp) {
		this.EMPd = emp;
	}

	public boolean isEMPd() {
		return EMPd;
	}

	public boolean isChatToggled() {
		return isChatToggled;
	}

	public void setChatToggled(boolean isChatToggled) {
		this.isChatToggled = isChatToggled;
	}

	public int getGeneralLevel() {
		return generalLevel;
	}

	public void setGeneralLevel(int generalLevel) {
		this.generalLevel = generalLevel;
	}

	public List<String> getSelectedPerks() {
		return selectedPerks;
	}

	public void setSelectedPerks(List<String> selectedPerks) {
		this.selectedPerks = selectedPerks;
	}
	
	public void giveSpawnItems() {
		InventoryUtils.nullInventory(getPlayer());
	    getPlayer().getInventory().addItem(ItemFactory.create(Material.CHEST, MessageUtil.color("&7Classes")));
	    getPlayer().getInventory().setItem(4, ItemFactory.createSkull(getPlayer(), MessageUtil.color("&7Perfil")));
	    getPlayer().getInventory().setItem(6, ItemFactory.create(Material.ANVIL, MessageUtil.color("&7Efeitos")));
	    getPlayer().getInventory().setItem(7, ItemFactory.create(Material.NETHER_STAR, MessageUtil.color("&7Perks")));
	    getPlayer().getInventory().setItem(8, ItemFactory.create(Material.REDSTONE_COMPARATOR, MessageUtil.color("&7Preferências")));
	}

	public boolean isLaunched() {
		return launched;
	}

	public void setLaunched(boolean launched) {
		this.launched = launched;
	}

	public boolean respawnWithKit() {
		return respawnWithKit;
	}

	public void setRespawnWithKit(boolean respawnWithKit) {
		this.respawnWithKit = respawnWithKit;
	}

	public List<String> getEffects() {
		return effects;
	}

	public void setEffects(List<String> effects) {
		this.effects = effects;
	}

	public List<String> getSelectedEffects() {
		return selectedEffects;
	}

	public void setSelectedEffects(List<String> selectedEffects) {
		this.selectedEffects = selectedEffects;
	}

	public boolean hasResourcePack() {
		return hasResourcePack;
	}

	public void setResourcePack(boolean hasResourcePack) {
		this.hasResourcePack = hasResourcePack;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public boolean hasShield() {
		return hasShield;
	}

	public void setShield(boolean hasShield) {
		this.hasShield = hasShield;
	}

	public double getProtectionPercentage() {
		return shieldPercentage;
	}

	public void setProtectionPercentage(double shieldPercentage) {
		this.shieldPercentage = shieldPercentage;
	}

	public GamePlayer getLastMessage() {
		return lastMessage;
	}

	public void setLastMessage(GamePlayer lastMessage) {
		this.lastMessage = lastMessage;
	}

	public boolean hasTeamRequest() {
		return hasTeamRequest;
	}

	public void setTeamRequest(boolean hasTeamRequest) {
		this.hasTeamRequest = hasTeamRequest;
	}

	public List<GamePlayer> getRequests() {
		return requests;
	}

	public void setRequests(List<GamePlayer> requests) {
		this.requests = requests;
	}

	public int getProtocolVersion() {
		return protocolVersion;
	}

	public void setProtocolVersion(int protocolVersion) {
		this.protocolVersion = protocolVersion;
	}

	public boolean isMuted() {
		return isMuted;
	}

	public void setMuted(boolean isMuted) {
		this.isMuted = isMuted;
	}

	public String getMuteReason() {
		return muteReason;
	}

	public void setMuteReason(String muteReason) {
		this.muteReason = muteReason;
	}

	public boolean isBanned() {
		return isBanned;
	}

	public void setBanned(boolean isBanned) {
		this.isBanned = isBanned;
	}

	public String getBanReason() {
		return banReason;
	}

	public void setBanReason(String banReason) {
		this.banReason = banReason;
	}

	public boolean isPMEnabled() {
		return pmEnabled;
	}

	public void setPMEnabled(boolean pmEnabled) {
		this.pmEnabled = pmEnabled;
	}

	public boolean hasPrivateProfile() {
		return privateProfile;
	}

	public void setPrivateProfile(boolean privateProfile) {
		this.privateProfile = privateProfile;
	}

	public List<TABLE_TYPE> getLoadedTables() {
		return loadedTables;
	}

	public boolean isTeamOwner() {
		return isTeamOwner;
	}

	public void setTeamOwner(boolean isTeamOwner) {
		this.isTeamOwner = isTeamOwner;
	}

	public boolean isAdminMode() {
		return isAdminMode;
	}

	public void setAdminMode(boolean isAdminMode) {
		this.isAdminMode = isAdminMode;
	}

	public long getBanEnd() {
		return banEnd;
	}

	public void setBanEnd(long banEnd) {
		this.banEnd = banEnd;
	}

	public int getTickets() {
		return tickets;
	}

	public void setTickets(int tickets) {
		this.tickets = tickets;
	}

	public boolean isInDuel() {
		return isInDuel;
	}

	public void setInDuel(boolean isInDuel) {
		this.isInDuel = isInDuel;
	}

	public GamePlayer getDuelRequester() {
		return duelRequest;
	}

	public void setDuelRequester(GamePlayer duelRequest) {
		this.duelRequest = duelRequest;
	}

	public boolean hasDuelRequest() {
		return hasDuelRequest;
	}

	public void setDuelRequest(boolean hasDuelRequest) {
		this.hasDuelRequest = hasDuelRequest;
	}

	public List<String> getAchievements() {
		return achievements;
	}

	public void setAchievements(List<String> achievements) {
		this.achievements = achievements;
	}

	public boolean hasMoved() {
		return hasMoved;
	}

	public void setMoved(boolean hasMoved) {
		this.hasMoved = hasMoved;
	}

	public void setSpiritShield(boolean x) {
		hasSpiritShield = x;
	}

	public boolean hasSpiritShield() {
		return hasSpiritShield;
	}

	public double getProtectedDamage() {
		return protectedDamage;
	}

	public void setProtectedDamage(double x) {
		this.protectedDamage = x;
	}

	public NPCPacketInjector getPacketInjector() {
		return packetInjectorket;
	}

	public void setPacketInjector() {
		this.packetInjectorket = new NPCPacketInjector(player);
		packetInjectorket.registerChannel("decoder", "PacketInjector");
	}
}
