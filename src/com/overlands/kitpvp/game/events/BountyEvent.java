package com.overlands.kitpvp.game.events;

import com.overlands.core.damage.OverlandsDamageEvent;
import com.overlands.core.utils.ActionBar;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Runner;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.core.utils.reflection.ReflectedArmorStand;
import com.overlands.core.utils.world.WorldUtils;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.event.OverlandsPlayerKillTargetEvent;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.GamePlayer.PlayerState;
import com.overlands.kitpvp.game.GamePlayer.PlayerState.State;
import floodeer.arcade.core.bossbar.BossBarAPI;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.util.EulerAngle;

import java.util.stream.Collectors;

public class BountyEvent extends GameEvent {

	private int duration = 230;
	private Player target;

	private ReflectedArmorStand targetMarker;
	private int step = 0;
	private float time = 0.0F;
	private double y  = 3;
	private boolean live = true;
	private boolean  up = false;
	
	public BountyEvent() {
		super(180 * 20);
	}

	@Override
	public void onStart() {
		Main.getGame().setEvent(this);

		target = MathUtils.random(Main.getGame().getPlayers().stream()
				.filter(cur -> !cur.isInDuel()).map(GamePlayer::getPlayer)
				.filter(cur -> !WorldUtils.inSpawn(cur.getLocation())).collect(Collectors.toList()));

		if(target == null) {
			HandlerList.unregisterAll(this);
			this.target = null;
			return;
		}

		targetMarker = new ReflectedArmorStand(target.getLocation().clone().add(0, y, 0));
		targetMarker.setEquipment(4, ItemFactory.create(Material.EMERALD_BLOCK));
		targetMarker.setHeadPose(new EulerAngle(0.0D, Math.toRadians(40.0D), Math.toRadians(60.0D)));
		targetMarker.setVisible(false);
		targetMarker.setSmall(true);
		targetMarker.spawnArmorStand();

		Main.get().getMusicCore().play("StarWars", Main.getPM().getPlayers());

		Main.getGame().getPlayers().forEach(player -> {
			Player p = player.getPlayer();
			if (player.hasTitles()) {
				Util.sendTitle(p, 9, 38, 12, "&r&k:::&9&lEVENTO&r&k:::", "&e&lBounty Hunter");
			}
			p.playSound(p.getLocation(), Sound.AMBIENCE_THUNDER, 5.0F, 0.3F);

			p.sendMessage(MessageUtil.color("&2&m-------------&r &6&lEvento &2&m------------&r"));
			p.sendMessage(" ");
			p.sendMessage(MessageUtil.color("&c&l• &eEliminte &4&l" + target.getName() + "&epara ser o próximo target!"));
			p.sendMessage(MessageUtil.color("&c&l• &eReceba &c1500 coins &ee &c500 &eEXP por matar o target!"));
			p.sendMessage(MessageUtil.color("&c&l• &eTarget recebe EXP e coins extras por kill!"));
			p.sendMessage(" ");
			p.sendMessage(MessageUtil.color("&bDuração do evento: &c3 minutos!"));
			p.sendMessage(" ");
			p.sendMessage(MessageUtil.color("&2&m---------------------------------&r"));
		});
	}

	@Override
	public void onUpdate() {
		++step;
		if(step == 20) {
			--duration;
			Main.getGame().getPlayers().forEach(cur -> ActionBar.sendActionBar(cur.getPlayer(), MessageUtil.color("&6&lAlvo: &c&l" + target.getName())));
			step = 0;
		}
		
		if (duration <= 0) {
			targetMarker.remove();
			return;
		}
		
		if (targetMarker != null) {
			targetMarker.teleport(target.getLocation().add(0.0D, 1.8D, 0.0D));
			targetMarker.setRotation(time, 0.0F);
			ParticleEffect.REDSTONE.display(new ParticleEffect.OrdinaryColor(Color.GREEN), targetMarker.getLocation().add(MathUtils.randomRange(-0.5F, 0.5F),2.4D + MathUtils.randomRange(-0.2F, 0.2F), MathUtils.randomRange(-0.5F, 0.5F)),180);
			time += 8.0F;
			if (time >= 360.0F) {
				time = 0.0F;
			}
		}
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Runner.make(Main.get()).delay(60).run(() -> Main.get().getMusicCore().addPlayer(GamePlayer.get(e.getPlayer())));
	}
	
	@Override
	public void onEnd() {
		HandlerList.unregisterAll(this);
		this.target = null;
		Main.get().getMusicCore().stop();
		Main.getGame().setCoinMultiplier(1);
		Main.getGame().setEvent(null);
		Main.getGame().getPlayers().forEach(gp -> {
			if(gp.getState().getState() == State.EVENT) {
				gp.setState(new PlayerState(gp, State.SAFE, 0L));
			}
			gp.getPlayer().sendMessage(MessageUtil.color("&eO evento acabou."));
		});
		if(targetMarker != null) {
			targetMarker.remove();
		}
	}

	@EventHandler
	public void onTargetKill(OverlandsPlayerKillTargetEvent e) {
		if(e.getTarget().equals(this.target)) {
			this.target = e.getPlayer();
			GamePlayer gpTarget = GamePlayer.get(target);
			e.getTarget().sendMessage("&c&lVocê não conseguiu manter seu bônus de Bounty!");
			Main.getGame().getPlayers().forEach(p -> {
				p.getPlayer().sendMessage(MessageUtil.color("&6&lElimine &c&l" + target.getName() + " &6&lpara receber recompensa e ser o próximo target!"));

				if (p.hasTitles()) {
					Util.sendTitle(p.getPlayer(), 9, 38, 12, "&r&k:::&9&lNOVO ALVO&r&k:::", "&e&l" + target.getName());
				}

				p.addBalance(25, true, "Global Bounty Kill Bonus");
				GamePlayer.get(target).addExp(5, true, "Global Bounty Kill Bonus");

				GamePlayer.get(e.getPlayer()).addBalance(1500, true, "Bounty Hunter!");
				GamePlayer.get(e.getPlayer()).addExp(500, true, "Bounty Hunter!");
			});
			gpTarget.addBalance(15, false, null);
			gpTarget.addExp(25, true, "Bounty Exp");
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onTargetDamage(OverlandsDamageEvent e) {
		if(e.getTargetPlayer().equals(this.target)) {
			if(Main.getEnergyManager().use(this.target, "Target Damage Reason Changer", 2500, false, false)) {
				e.changeReason("Generic Damage Event", "Bounty Target");
			}
		}
	}
}
