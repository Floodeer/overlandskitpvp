package com.overlands.kitpvp.game.events;

import com.overlands.core.utils.Runner;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.scheduler.BukkitRunnable;

import com.overlands.core.utils.ActionBar;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.event.OverlandsPlayerKillTargetEvent;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.GamePlayer.PlayerState;
import com.overlands.kitpvp.game.GamePlayer.PlayerState.State;

import de.slikey.effectlib.EffectType;
import de.slikey.effectlib.effect.BleedEffect;
import de.slikey.effectlib.effect.StarEffect;
import de.slikey.effectlib.util.DynamicLocation;
import de.slikey.effectlib.util.ParticleEffect;

public class BleedingStar extends GameEvent {

	private int duration = 180;
	
	private int steps = 0;
	private int steps2 = 0;
	private StarEffect star;
	
	public BleedingStar() {
		super(180*20);
	}
	
	@Override
	public void onStart() {
		Main.getGame().setEvent(this);
		
		Main.getGame().getPlayers().forEach(player -> {
			Player p = player.getPlayer();
			if(player.hasTitles()) {
				Util.sendTitle(p, 9, 38, 12, "&r&k:::&9&lEVENTO&r&k:::", "&c&lBLEEDING STAR");
			}
			p.playSound(p.getLocation(), Sound.AMBIENCE_THUNDER, 5.0F, 0.3F);

			p.sendMessage(MessageUtil.color("&2&m-------------&r &6&lEvento &2&m------------&r"));
			p.sendMessage(" ");
			p.sendMessage(MessageUtil.color("&c&l• &eKills na área da estrela dão EXP e coins extras!"));
			p.sendMessage(MessageUtil.color("&c&l• &eA vida de todos os players é diminuida em &c&l1❤&e!"));
			p.sendMessage(" ");
			p.sendMessage(MessageUtil.color("&bDuração do evento: &c3 minutos!"));
			p.sendMessage(" ");
			p.sendMessage(MessageUtil.color("&2&m---------------------------------&r"));

			p.setMaxHealth(18);
		});
		
		star = new StarEffect(Main.get().effectManager);
		star.setDynamicOrigin(new DynamicLocation(Main.mapConfig.center.clone().add(0, 8, 0)));
		star.particle = ParticleEffect.REDSTONE;
		star.particles = 50;
		star.type = EffectType.REPEATING;
		star.infinite();
		star.visibleRange = 156;
		star.start();

	}

	@EventHandler
	public void onRespawn(PlayerRespawnEvent event) {
		if(Main.getGame().getEvent().equals(this)) {
			Runner.make(Main.get()).delay(10).run(() -> event.getPlayer().setMaxHealth(18));
		}
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		if(Main.getGame().getEvent().equals(this)) {
			event.getPlayer().setMaxHealth(20);
		}
	}

	@Override
	public void onUpdate() {
		++steps;
		++steps2;
		if(steps == 20) {
			--duration;
			drawCircle(Main.mapConfig.center, (float) Math.PI * 16, 0.04F, 50, 20, 1);
			steps = 0;
		}
		if(steps2 == 8*20) {
			LocationUtils.getNearbyPlayers(Main.mapConfig.center, Math.PI * 16).stream()
			.filter(player -> GamePlayer.get(player).isInGame()
					&& !GamePlayer.get(player).getSelectedClass().getName().equalsIgnoreCase("Cosmic"))
			.forEach(player -> {
				BleedEffect b = new BleedEffect(Main.get().effectManager);
				b.setDynamicTarget(new DynamicLocation(player));
				b.iterations = 20;
				b.start();
			});
			steps2 = 0;
		}
		
		Main.getGame().getPlayers().forEach(player -> {
			ActionBar.sendActionBar(player.getPlayer(), MessageUtil.color("&c&lDuração do evento: " + (duration)));
		});
	}
	
	@EventHandler
	public void onTargetKill(OverlandsPlayerKillTargetEvent e) {
		GamePlayer killer = GamePlayer.get(e.getPlayer());
		
		if(LocationUtils.isInArea(e.getTarget(), Main.mapConfig.center, Math.PI * 16)) {
			killer.addBalance(MathUtils.random(255), true, "bleeding star kill");
			killer.addExp(MathUtils.random(50), true, "bleeding star kill");
		}
	}

	@Override
	public void onEnd() {
		HandlerList.unregisterAll(this);
		
		Main.getGame().setEvent(null);
		Main.getGame().getPlayers().forEach(gp -> {
			if(gp.getState().getState() == State.EVENT) {
				gp.setState(new PlayerState(gp, State.SAFE, 0L));
			}
			gp.getPlayer().sendMessage(MessageUtil.color("&9&lO evento acabou."));
		});
		star.cancel();
		
		
	}

	public static void drawCircle(final Location l, final float f, final float f2, final int f3, final int i, int i2) {
		
		new BukkitRunnable() {
			int step = 0;
			float size2 = f;
			@Override
			public void run() {
				++this.step;
				this.size2 += f2;
				if (this.step <= i) {
					for (Location loc : LocationUtils.getCircle(l, size2, f3)) {
						com.overlands.core.utils.particles.ParticleEffect.REDSTONE.display(new com.overlands.core.utils.particles.ParticleEffect.OrdinaryColor(204, 0, 0), loc, 196);
					}
				} else {
					this.cancel();
				}
			}
		}.runTaskTimer(Main.get(), 0L, i2);
	}
}

