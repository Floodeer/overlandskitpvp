package com.overlands.kitpvp.game.events;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Blaze;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Skeleton.SkeletonType;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.overlands.core.utils.HeadUtils.Head;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.calc.TimeUtils;
import com.overlands.core.utils.calc.VelocityUtils;
import com.overlands.core.utils.entity.EntityUtils;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.java.FinalReference;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.core.utils.scheduler.SchedulerEvent;
import com.overlands.core.utils.scheduler.UpdateType;
import com.overlands.core.utils.world.WorldUtils;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.Game;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.GamePlayer.PlayerState;
import com.overlands.kitpvp.game.GamePlayer.PlayerState.State;

public class DemonBossEvent extends GameEvent implements Listener {
	
	public long updateBossA = 0;
	public long updateBossB = 0;
	public long updateBossC = 0;
	public long updateBossD = 0;
	public long updateBossE = 0;
	public long updateBossF = 0;
	public long updateBossG = 0;
	public long updateBossH = 0;
	public long updateBossI = 0;
	public long updateBossJ = 0;
	public long updateBossK = 0;
	public long updateBossL = 0;
	public long updateBossM = 0;
	public long updateBossN = 0;
	public long updateBossO = 0;
	
	private Skeleton ent = null;

	private int state = 0;
	private long stateTime = System.currentTimeMillis();

	private List<Skeleton> minions = Lists.newArrayList();
	private int minionsMax = 12; // 12
	private boolean minionSpawn = true;
	private Map<Entity, Player> minionTargets = Maps.newHashMap();
	private Map<Skeleton, Long> minionAttack = Maps.newHashMap();

	private List<Entity> shields = new ArrayList<Entity>();
	private int shieldsMax = 6; // 6
	private long shieldSpawn = 0;

	private Location kingLocation;
	private Player kingTarget = null;

	private Set<Arrow> arrows = Sets.newHashSet();
	
	private Map<GamePlayer, Double> damageCaused = Maps.newHashMap();
	

	private boolean announcedHalfHealth = false;

	public DemonBossEvent() {
		super(-1);
		
		kingLocation = Main.mapConfig.center;
		ent = Main.mapConfig.center.getWorld().spawn(Main.mapConfig.center, Skeleton.class);
		
		ent.setSkeletonType(SkeletonType.WITHER);
		ent.getEquipment().setHelmet(ItemFactory.createSkull(Head.DEVIL.getTexture(), 1));
		ent.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.MAX_VALUE, 0, false, false));
		ent.setMaxHealth(500);
		ent.setHealth(ent.getMaxHealth());
		ent.getWorld().strikeLightningEffect(ent.getLocation());
		
		EntityUtils.clearSelectorsAI(ent);
	}
	
	@Override
	public void onStart() {
		Main.mapConfig.center.getWorld().setTime(140000);
		Game game = Main.getGame();
		game.setEvent(this);
		if(game.canGenerateInstances())
			game.setInstances(false);
		
		getPlayers().forEach(p -> {
			if(GamePlayer.get(p).hasTitles()) {
				Util.sendTitle(p, 16, 50, 13, "&r&k:::&9&lEVENTO&r&k:::", "&c&lBOSS");
			}

			p.playSound(p.getLocation(), Sound.AMBIENCE_THUNDER, 5.0F, 0.3F);
			
			p.sendMessage(MessageUtil.color("&c&lAs habilidades foram desativadas, derrote o boss para continuar jogando!"));
			p.sendMessage(MessageUtil.color("&9Seu inventário foi modificado."));
		    
			GamePlayer gp = GamePlayer.get(p);
			gp.setEnergy(0);
			gp.setCanGainEnergy(false);
			gp.getSelectedClass().giveItems(p);
			ItemStack bow = ItemFactory.create(Material.BOW, MessageUtil.color("&7Arco"));
			ItemFactory.applyEnchantment(bow, Enchantment.ARROW_INFINITE, 10);
			ItemFactory.unbreakable(bow);
			p.getInventory().setItem(1, bow);
			p.getInventory().setItem(9, ItemFactory.create(Material.ARROW, 1));
		});
	}

	@EventHandler
	public void onScheduler(SchedulerEvent event) {
		long start = System.currentTimeMillis();

		if (event.getType() == UpdateType.FASTER)
			stateUpdate();
		updateBossA += System.currentTimeMillis() - start;
		start = System.currentTimeMillis();
		if (event.getType() == UpdateType.FAST)
			//TODO destroyBlocks();
		updateBossB += System.currentTimeMillis() - start;
		start = System.currentTimeMillis();
		if (event.getType() == UpdateType.TICK)
			bossUpdateHealth();
		updateBossC += System.currentTimeMillis() - start;
		start = System.currentTimeMillis();

	
		if (event.getType() == UpdateType.TICK)
			minionOrbit();
		updateBossD += System.currentTimeMillis() - start;
		start = System.currentTimeMillis();
		if (event.getType() == UpdateType.FASTER)
			minionMove();
		updateBossE += System.currentTimeMillis() - start;
		start = System.currentTimeMillis();
		if (event.getType() == UpdateType.TICK)
			minionAttackDamage();
		updateBossF += System.currentTimeMillis() - start;
		start = System.currentTimeMillis();
		if (event.getType() == UpdateType.FASTEST)
			minionArrow();
		updateBossG += System.currentTimeMillis() - start;
		start = System.currentTimeMillis();
		if (event.getType() == UpdateType.FAST)
			minionSpawn();
		updateBossH += System.currentTimeMillis() - start;
		start = System.currentTimeMillis();


		if (event.getType() == UpdateType.FAST)
			bossControl();
		updateBossI += System.currentTimeMillis() - start;
		start = System.currentTimeMillis();
		if (event.getType() == UpdateType.SEC)
			bossLeap();
		updateBossJ += System.currentTimeMillis() - start;
		start = System.currentTimeMillis();
		if (event.getType() == UpdateType.SEC)
			bossBomb();
		updateBossK += System.currentTimeMillis() - start;
		start = System.currentTimeMillis();
		if (event.getType() == UpdateType.SEC)
			bossTarget();
		updateBossL += System.currentTimeMillis() - start;
		start = System.currentTimeMillis();
		if (event.getType() == UpdateType.TICK)
			bossTrail();
		updateBossM += System.currentTimeMillis() - start;
		start = System.currentTimeMillis();

		if (event.getType() == UpdateType.TICK)
			shieldOrbit(false);
		updateBossN += System.currentTimeMillis() - start;
		start = System.currentTimeMillis();
		if (event.getType() == UpdateType.FAST)
			shieldSpawn();
		updateBossO += System.currentTimeMillis() - start;
		start = System.currentTimeMillis();
	}
	
	public int getState() {
		return state;
	}

	private void bossTrail() {
		if (getState() >= 4) {
			ParticleEffect.FIREWORKS_SPARK.display(0.2f, 0.4f, 0.2f, 0, 1, ent.getLocation().add(0, 1.5, 0), 250);
		}
	}

	private void bossTarget() {
		if (Math.random() > 0.80) {
			kingTarget = getRandomPlayer();
			bossLeap();
		}
	}

	private void bossControl() {
		if (getState() >= 4) {
			if (kingTarget == null)
				kingTarget = getRandomPlayer();

			ent.setTarget(kingTarget);

			Location loc = kingTarget.getLocation();
			if (MathUtils.offset(loc, ent.getLocation()) > 16)
				loc = ent.getLocation().add(VelocityUtils.getTrajectory(ent.getLocation(), loc).multiply(16));

		
			EntityUtils.nav(ent, loc, 1);
		} else {
			ent.teleport(kingLocation);
		}
	}

	private void bossLeap() {
		if (getState() < 4)
			return;

		if (kingTarget == null)
			return;

		if (Math.random() > 0.4)
			return;

		VelocityUtils.velocity(ent, VelocityUtils.getTrajectory2d(ent, kingTarget), 1.2, false, 0, 0.4, 10, true);
	}

	private void bossBomb() {
		if (getState() < 4)
			return;

		if (kingTarget == null)
			return;

		if (Math.random() > 0.4)
			return;

		TNTPrimed tnt = ent.getWorld().spawn(ent.getEyeLocation().add(ent.getLocation().getDirection()), TNTPrimed.class);
		tnt.setMetadata("customDamaged", new FixedMetadataValue(Main.get(), null));
		Player target = getRandomPlayer();

		VelocityUtils.velocity(tnt, VelocityUtils.getTrajectory(tnt, target), 1.2, false, 0, 0.4, 10, false);
	}
	
	private void bossUpdateHealth() {
		FinalReference<String> reference = new FinalReference<String>("");
		String text = "";
		double percent = 0;

		if (shields.size() > 0) {
			percent = (double) shields.size() / (double) shieldsMax;
			text = Util.cGreen + Util.Bold + "Quebre os escudos de fogo!";
			reference.set(text);
		} else {
			if (minions.size() > 0) {
				percent = (double) minions.size() / (double) minionsMax;
				text = Util.cYellow + Util.Bold + "Mate os minions!";
				reference.set(text);
			} else {
				percent = ent.getHealth() / ent.getMaxHealth();
				text = Util.cGold + Util.Bold + "Mate o chefe!";
				reference.set(text);
			}
		}
		

		for(Player p : getPlayers())  {
			 Util.sendActionBar(p, reference.get());
		}
	}

	
	public void minionSpawn() {
		Iterator<Skeleton> shieldIterator = minions.iterator();
		while (shieldIterator.hasNext()) {
			Skeleton skel = shieldIterator.next();

			if (!skel.isValid())
				shieldIterator.remove();
		}

		if (!minionSpawn)
			return;

		for (int i = 0; i < minionsMax; i++) {
			Skeleton skel = ent.getWorld().spawn(ent.getLocation(), Skeleton.class);
			skel.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 0, false, false));

			skel.getEquipment().clear();
			skel.getEquipment().setHelmet(ItemFactory.createSkull(Head.GHOST_REDEYE.getTexture(), 1));
			skel.getEquipment().setItemInHand(new ItemStack(Material.BOW));

			skel.setMaxHealth(50);
			skel.setHealth(skel.getMaxHealth());
			
			EntityUtils.clearSelectorsAI(skel);
			minions.add(skel);
		}

		minionSpawn = false;
	}

	public void minionOrbit() {
		if (getState() != 0 && getState() != 1 && getState() != 2)
			return;

		for (int i = 0; i < minions.size(); i++) {
			Skeleton minion = minions.get(i);

			ParticleEffect.SPELL_WITCH.display(0.1f, 0.1f, 0.1f, 0, 1, minion.getEyeLocation(), 96);

			minion.setTarget(null);

			double radius = i * ((2d * Math.PI) / minions.size());

			double size = 2 + (minions.size() / 12);

			double speed = 20D;
			double oX = Math.sin(ent.getTicksLived() / speed + radius) * 2 * size;
			double oY = 1;
			double oZ = Math.cos(ent.getTicksLived() / speed + radius) * 2 * size;
			Location loc = ent.getLocation().add(oX, oY, oZ);

			if (MathUtils.offset(loc, minion.getLocation()) > 16) {
				minion.teleport(loc);
				continue;
			}

			loc.setYaw(VelocityUtils.getYaw(VelocityUtils.getTrajectory(ent, minion)));

			EntityUtils.moveFast(minion, loc, 1.4F);
		}
	}

	public void minionMove() {
		if (getState() != 3)
			return;

		if (minions.isEmpty())
			return;

		Skeleton minion = minions.remove(0);

		LivingEntity target = minionTargets.get(minion);
		if (target == null)
			return;

		minion.setTarget(target);

		EntityUtils.move(minion, target.getLocation(), 1f);

		minions.add(minion);
	}

	private void minionAttackDamage() {
		if (getState() != 3)
			return;

		for (int i = 0; i < minions.size(); i++) {
			final Skeleton minion = minions.get(i);

			ParticleEffect.SPELL_WITCH.display(0.1f, 0.1f, 0.1f, 0, 1, minion.getEyeLocation(), 96);

			LivingEntity target = minionTargets.get(minion);
			if (target == null)
				continue;

			if (MathUtils.offset(minion, target) > 2)
				continue;

			if (minionAttack.containsKey(minion) && !TimeUtils.elapsed(minionAttack.get(minion), 500))
				continue;
		
			
			Bukkit.getPluginManager().callEvent(new EntityDamageByEntityEvent(minion, target, DamageCause.ENTITY_ATTACK, 4));

			minion.getWorld().playSound(minion.getLocation(), Sound.ENDERMAN_SCREAM, 2f, 2f);
			minion.getWorld().playSound(minion.getLocation(), Sound.ENDERMAN_SCREAM, 2f, 2f);

			minion.getEquipment().setHelmet(new ItemStack(Material.JACK_O_LANTERN));

			new BukkitRunnable() {

				@Override
				public void run() {
					minion.getEquipment().setHelmet(new ItemStack(Material.PUMPKIN));
				}
			}.runTaskTimer(Main.get(), 0, 4);
		}
	}
	
	public void minionArrow() {
		Iterator<Arrow> arrowIterator = arrows.iterator();
		while (arrowIterator.hasNext()) {
			Arrow arrow = arrowIterator.next();

			if (arrow.getLocation().getY() > 30 && arrow.getVelocity().getY() > 0) {
				Player target = getRandomPlayer();
				arrow.teleport(target.getLocation().add(Math.random() * 8 - 4, Math.random() * 10 + 30, Math.random() * 8 - 4));
				arrow.setVelocity(arrow.getVelocity().setY(-0.1));
				continue;
			}

			if (arrow.getTicksLived() > 200 || arrow.isOnGround()) {
				arrow.remove();
				arrowIterator.remove();
			}
		}

		if (getState() == 1) {
			for (int i = 0; i < minions.size(); i++) {
				Skeleton minion = minions.get(i);

				if (!minion.isValid())
					continue;

				Vector traj = VelocityUtils.getTrajectory2d(ent, minion);
				traj.add(new Vector(0, Math.random() * 0.25, 0));

				Arrow arrow = ent.getWorld().spawnArrow(minion.getEyeLocation().add(traj), traj, 2f, 16f);
				arrow.setShooter(minion);

				arrows.add(arrow);
			}
		}

		else if (getState() == 2) {
			for (int i = 0; i < minions.size(); i++) {
				Skeleton minion = minions.get(i);

				if (!minion.isValid())
					continue;

				Vector traj = new Vector(0, 1, 0);

				Arrow arrow = ent.getWorld().spawnArrow(minion.getEyeLocation().add(traj), traj, 2f, 16f);
				arrow.setShooter(minion);

				arrows.add(arrow);
			}
		}
	}

	public void shieldSpawn() {
		if (getState() == 3 || getState() == 4)
			return;

		shields.removeIf(ent -> !ent.isValid());

		if (!TimeUtils.elapsed(shieldSpawn, 16000))
			return;

		if (shields.size() >= shieldsMax)
			return;

		shieldSpawn = System.currentTimeMillis();

		int toSpawn = 1;
		if (shields.size() == 0) {
			toSpawn = shieldsMax;
			ent.getWorld().playSound(ent.getLocation(), Sound.WITHER_HURT, 10f, 1.5f);

			if (ent.getTicksLived() > 100)
				broadcast("&c&lOs escudos de fogo foram renegerados.");
		} else {
			ent.getWorld().playSound(ent.getLocation(), Sound.WITHER_HURT, 1f, 2f);
		}

		for (int i = 0; i < toSpawn; i++) {

			Blaze blaze = ent.getWorld().spawn(ent.getLocation().add(0, 6, 0), Blaze.class);
			blaze.setHealth(1);
			blaze.getEquipment().setHelmet(new ItemStack(Material.PUMPKIN));
			shields.add(blaze);
			EntityUtils.clearSelectorsAI(blaze);

			shieldOrbit(false);
		}
	}

	public void shieldOrbit(boolean teleport) {
		for (int i = 0; i < shields.size(); i++) {
			Entity shield = shields.get(i);

			ParticleEffect.FLAME.display(0.1f, 0.1f, 0.1f, 0, 1, shield.getLocation(), 96);

			double lead = i * ((2d * Math.PI) / shields.size());

			double sizeMod = 2;

			double speed = 10d;
			double oX = -Math.sin(ent.getTicksLived() / speed + lead) * 2 * sizeMod;
			double oY = 6;
			double oZ = Math.cos(ent.getTicksLived() / speed + lead) * 2 * sizeMod;

			if (teleport) {
				shield.teleport(ent.getLocation().add(oX, oY, oZ));
			} else {
				VelocityUtils.velocity(shield,
						VelocityUtils.getTrajectory(shield.getLocation(), ent.getLocation().add(oX, oY, oZ)),
						0.4, false, 0, 0.1, 1, true);
			}
		}

		if (shields.size() > 0)
			ent.getWorld().playEffect(ent.getLocation().add(0, 6, 0), Effect.ENDER_SIGNAL, 0);
	}

	public void SetState(int state) {
		this.state = state;
		stateTime = System.currentTimeMillis();

		if (state == 3) {
			for (int i = 0; i < minions.size(); i++) {
				Skeleton minion = minions.get(i);

				minion.getEquipment().setItemInHand(null);

				minion.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 15, 0, false, false));
				minionTargets.put(minion, getRandomPlayer());
			}

			broadcast("&c&lMate os minions antes dos escudos se regenerarem!");
			
			minionMove();
			getPlayers()
					.forEach((player) -> playSound(Sound.ENDERMAN_STARE, 0.3f, player.getPlayer()));
		}

		if (state == 4) {
			getPlayers().forEach(
					(player) -> playSound(Sound.AMBIENCE_THUNDER, 0.7f, player.getPlayer()));
		}
	}

	public void stateUpdate() {
		if (ent == null)
			return;

		if (getState() == 0) {
			if (TimeUtils.elapsed(stateTime, 10000)) {
				if (Math.random() > 0.5)
					SetState(1);
				else
					SetState(2);
			}
		}
		else if (getState() == 1) {
			if (TimeUtils.elapsed(stateTime, 5000)) {
				SetState(0);
			}
		}
		else if (getState() == 2) {
			if (TimeUtils.elapsed(stateTime, 5000)) {
				SetState(0);
			}
		} else if (getState() == 3) {
			if (TimeUtils.elapsed(stateTime, 20000)) {
				SetState(0);

				for (int i = 0; i < minions.size(); i++) {
					Skeleton minion = minions.get(i);
					minion.setTarget(null);
					minion.getEquipment().setHelmet(new ItemStack(Material.PUMPKIN));
					minion.getEquipment().setItemInHand(new ItemStack(Material.BOW));
					EntityUtils.clearSelectorsAI(minion);
				}

				shieldSpawn();

				getPlayers().forEach((player) -> playSound(Sound.ZOMBIE_REMEDY, 0.7f, player.getPlayer()));

				minionTargets.clear();
			}
		} else if (getState() == 4) {
			if (!announcedHalfHealth) {
				if (ent.getHealth() < ent.getMaxHealth() / 2) {
					getPlayers().forEach((player) -> playSound(Sound.LEVEL_UP, 0.2f, player.getPlayer()));
					broadcast("&cVida do boss em &l50% &c!");
					announcedHalfHealth = true;
				}

			}
		}

		if (getState() != 3 && TimeUtils.elapsed(stateTime, 2000)) {
			if (shields.size() == 0 && minions.size() > 0) {
				SetState(3);
			}
		}

		if (getState() != 4 && TimeUtils.elapsed(stateTime, 2000)) {
			if (shields.size() == 0 && minions.size() == 0) {
				SetState(4);

				broadcast("&c&lMate o boss!");
				ent.getWorld().playSound(ent.getLocation(), Sound.WITHER_SPAWN, 10f, 1.5f);
				ent.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1, false, false));
				ent.getEquipment().setItemInHand(new ItemStack(Material.DIAMOND_AXE));
			}
		}
	}
	
	@Override
	public void onEnd() {
		HandlerList.unregisterAll(this);
		
		damageCaused.keySet().stream().forEach(gp -> {
		    gp.addBalance((int)Math.round(damageCaused.get(gp)), true, "dano causado");
		    gp.addBalance(500, true, "participação do evento");
		    gp.addExp(95, true, "participação do evento");
			gp.getSelectedClass().giveItems(gp.getPlayer());
		});
		
		Main.mapConfig.center.getWorld().setTime(2200);
		
		if(!minions.isEmpty())
			minions.forEach(m -> m.remove());
		if(!shields.isEmpty())
			shields.forEach(s -> s.remove());
		
		Main.getGame().setEvent(null);
		Main.getGame().getPlayers().forEach(gp -> {
			if(gp.getState().getState() == State.EVENT) {
				gp.setState(new PlayerState(gp, State.SAFE, 0L));
			}
			if(!gp.canGainEnergy())
				gp.setCanGainEnergy(true);
			
			gp.getPlayer().sendMessage(MessageUtil.color("&9Habilidades reativadas."));
			gp.getPlayer().sendMessage(MessageUtil.color("&9Seu inventário foi modificado."));
		});
	}



	@Override
	public void onUpdate() {}
	
	@EventHandler
	public void target(EntityTargetEvent event) {
		if (event.getEntity().equals(ent)) {
			if (getState() != 4 || kingTarget == null || !kingTarget.equals(event.getTarget())) {
				event.setCancelled(true);
			}
		}

		if (minions.contains(event.getEntity())) {
			if (getState() != 3) {
				event.setCancelled(true);
			} else {
				if (!minionTargets.containsKey(event.getEntity())) {
					event.setCancelled(true);
				} else {
					Player player = minionTargets.get(event.getEntity());

					if (!player.equals(event.getTarget()))
						event.setCancelled(true);

					  minionTargets.put(event.getEntity(), player);
				}
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		if(event.getDamager() instanceof Player) {
			if(event.getEntity().equals(ent)) {
				Player player = (Player)event.getDamager();
				if (shields.size() > 0) {
					player.sendMessage(MessageUtil.encapsulate("Você precisa destruir os escudos de fogo primeiro!"));
					event.setCancelled(true);
				}
				if (minions.size() > 0) {
					player.sendMessage(MessageUtil.encapsulate("Você precisa matar os minions primeiro!"));
					event.setCancelled(true);
				}
				if(!damageCaused.containsKey(GamePlayer.get(player)))
					damageCaused.put(GamePlayer.get(player), 0D);
				
				damageCaused.put(GamePlayer.get(player), damageCaused.get(GamePlayer.get(player)) + event.getDamage());
				return;
			}
		}
		if(minions.contains(event.getEntity())) {
			if(event.getDamager() instanceof Arrow) {
				Arrow arrow = (Arrow)event.getDamager();
				if(arrow.getShooter() instanceof Player) {
					((Player)arrow.getShooter()).sendMessage(MessageUtil.encapsulate("Minions não tomam dano de projéteis."));
				    ((Player)arrow.getShooter()).setFoodLevel(20);
				    arrow.remove();
				}
				event.setCancelled(true);
			}else if(event.getDamager() instanceof Player) {
				Player player = (Player)event.getDamager();
				if (shields.size() > 0) {
					player.sendMessage(MessageUtil.encapsulate("Você precisa destruir os escudos de fogo primeiro!"));
					event.setCancelled(true);
				}
				player.setFoodLevel(20);
				if(!damageCaused.containsKey(GamePlayer.get(player)))
					damageCaused.put(GamePlayer.get(player), 0D);
				
				damageCaused.put(GamePlayer.get(player), damageCaused.get(GamePlayer.get(player)) + event.getDamage());
			}else{
				event.setCancelled(true);
			}
		}else if(shields.contains(event.getEntity())) {
			if(event.getDamager() instanceof Player || event.getDamager() instanceof Arrow && (((Arrow)event.getDamager()).getShooter() instanceof Player)) {
					Player player = null;
					if(event.getDamager() instanceof Arrow) {
						player = (Player) (((Arrow)event.getDamager()).getShooter());
					}else{
						player = (Player)event.getDamager();
					}
 					if(event.getCause() != DamageCause.PROJECTILE) {
	 					player.sendMessage(MessageUtil.encapsulate("Use seu arco para destruir os escudos!"));
						event.setCancelled(true);
						return;
	 				}
					player.setFoodLevel(20);
					event.getEntity().getWorld().playEffect(event.getEntity().getLocation(), Effect.STEP_SOUND, 51);
					shields.remove(event.getEntity());
					event.getEntity().remove();
					if(!damageCaused.containsKey(GamePlayer.get(player)))
						damageCaused.put(GamePlayer.get(player), 0D);
					
					damageCaused.put(GamePlayer.get(player), damageCaused.get(GamePlayer.get(player)) + 20D);
				}else{
					event.setCancelled(true);
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onEntityDeath(EntityDeathEvent event) {
		if(event.getEntityType() == EntityType.SKELETON && ((Skeleton)event.getEntity()).getSkeletonType() == SkeletonType.WITHER) {
			getPlayers().forEach(p -> playSound(Sound.ENDERDRAGON_DEATH, 0.6F, p));
			onEnd();
		}
		event.getDrops().clear();
		event.setDroppedExp(0);
	}
	
	@EventHandler
	public void onExplode(EntityExplodeEvent e) {
		if(e.getEntity() instanceof Fireball) {
			Fireball f = (Fireball)e.getEntity();
			if(f.getShooter() instanceof Blaze)
				if(shields.contains((Blaze)f.getShooter()))
					e.setCancelled(true);
		}
	}
	
	private Player getRandomPlayer() {
		List<Player> matches = Main.getGame().getPlayers()
				.stream()
				.map(g -> g.getPlayer())
				.filter(p -> !WorldUtils.inSpawn(p.getLocation())).collect(Collectors.toList());
		return MathUtils.random(matches);
	}
	
	private void broadcast(String str) {
		Main.getGame().getPlayers().forEach(g -> g.getPlayer().sendMessage(MessageUtil.color(str)));
	}
	
	private List<Player> getPlayers() {
		return Main.getGame().getPlayers().stream().map(gp -> gp.getPlayer()).collect(Collectors.toList());
	}
	
	private void playSound(Sound sound, float pitch, Player player) {
		player.playSound(player.getLocation(), sound, 1, pitch);
	}
}
