package com.overlands.kitpvp.game.events;

import java.util.ArrayList;
import java.util.Map;
import java.util.stream.Collectors;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.google.common.collect.Maps;
import com.overlands.core.utils.ActionBar;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Runner;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.world.WorldUtils;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.event.OverlandsPlayerKillTargetEvent;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.GamePlayer.PlayerState;
import com.overlands.kitpvp.game.GamePlayer.PlayerState.State;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;

public class MysteryClasses extends GameEvent {

	private int duration = 142;
	private int step = 0;
	private int step2;
	private Map<Player, ClassBackup> oldClasses;
	
	public MysteryClasses() {
		super(140*20);
	}
	
	@Override
	public void onStart() {
		Main.getGame().setEvent(this);
		Main.get().getMusicCore().play("StarWars", Main.getPM().getPlayers());
		oldClasses = Maps.newHashMap();
		
		Main.getGame().getPlayers().forEach(player -> {
			Player p = player.getPlayer();
			if(player.hasTitles()) {
				Util.sendTitle(p, 9, 38, 12, "&r&k:::&9&lEVENTO&r&k:::", "&c&lMYSTERY CLASSES");
			}
			p.playSound(p.getLocation(), Sound.AMBIENCE_THUNDER, 5.0F, 0.3F);
			p.sendMessage(MessageUtil.color("&b&lVocê trocará de classe aleatóriamente com sua habilidade carregada!"));

			p.sendMessage(MessageUtil.color("&2&m-------------&r &6&lEvento &2&m------------&r"));
			p.sendMessage(" ");
			p.sendMessage(MessageUtil.color("&c&l• &eSua classe será trocada aleatoriamente!"));
			p.sendMessage(MessageUtil.color("&c&l• &eAo trocar de classe sua habilidade estará pronta!"));
			p.sendMessage(" ");
			p.sendMessage(MessageUtil.color("&bDuração do evento: &c2 minutos!"));
			p.sendMessage(" ");
			p.sendMessage(MessageUtil.color("&2&m---------------------------------&r"));
		});
		
		Main.getPM().getPlayers().forEach(gp -> {
			oldClasses.put(gp.getPlayer(), new ClassBackup(gp.getSelectedClass(), gp.getClassTypeFromEnum(), gp));
		});
		
		Main.getGame().getPlayers().forEach(gp -> {
			if(!WorldUtils.inSpawn(gp.getPlayer().getLocation())) {
				giveRandomClass(gp);
			}
		});
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Runner.make(Main.get()).delay(60).run(() -> Main.get().getMusicCore().addPlayer(GamePlayer.get(e.getPlayer())));
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onQuit(PlayerQuitEvent e) {
		GamePlayer gp = GamePlayer.get(e.getPlayer());
		if(oldClasses.containsKey(gp.getPlayer())) {
			Player player = e.getPlayer();
			gp.setClass(oldClasses.get(player).clazz);
			gp.setClassType(oldClasses.get(player).type);
		}
		oldClasses.remove(gp.getPlayer());
	}
	
	@Override
	public void onUpdate() {
		++step;
		++step2;
		if(step == 20) {
			--duration;
			step = 0;
		}
		if(step2 == 100) {
			step2 = 0;
			if(MathUtils.randomBoolean(60)) {
				Main.getGame().getPlayers().forEach(gp -> {
					if(!WorldUtils.inSpawn(gp.getPlayer().getLocation())) {
						giveRandomClass(gp);
					}
				});
			}
		}
		Main.getGame().getPlayers().forEach(player -> {
			ActionBar.sendActionBar(player.getPlayer(), MessageUtil.color("&c&lDuração do evento: " + (duration)));
		});
	}
	
	@EventHandler
	public void onTargetKill(OverlandsPlayerKillTargetEvent e) {
		GamePlayer gp = GamePlayer.get(e.getPlayer());
		ClassType type = MathUtils.random(new ArrayList<>(Main.getGame().gameClasses.keySet()));
		gp.setClass(Main.getGame().gameClasses.get(type));
		gp.setClassType(type);
		gp.setEnergy(100);
		gp.getPlayer().sendMessage(MessageUtil.color("&c&lSua classe foi modificada!"));
	}

	@Override
	public void onEnd() {
		HandlerList.unregisterAll(this);
		
		Main.getGame().setEvent(null);
		Main.getGame().getPlayers().forEach(gp -> {
			if(gp.getState().getState() == State.EVENT) {
				gp.setState(new PlayerState(gp, State.SAFE, 0L));
			}
			gp.getPlayer().sendMessage(MessageUtil.color("&9&lO evento acabou."));
		});
		oldClasses.keySet().forEach(player -> {
			GamePlayer gp = GamePlayer.get(player);
			gp.setClass(oldClasses.get(player).clazz);
			gp.setClassType(oldClasses.get(player).type);
			gp.getSelectedClass().giveItems(player.getPlayer());
		});
		
		Main.get().getMusicCore().stop();
	}
	
	public static void giveRandomClass(GamePlayer gp) {
		Player p = gp.getPlayer();
		ClassType type = MathUtils.random(new ArrayList<>(Main.getGame().gameClasses.keySet()));
		gp.setClass(Main.getGame().gameClasses.get(type));
		gp.setClassType(type);
		gp.setEnergy(100);
		gp.getPlayer().sendMessage(MessageUtil.color("&c&lSua classe foi modificada!"));
		gp.getSelectedClass().giveItems(gp.getPlayer());
		Main.getGame().getPlayers().forEach(gpp -> Main.getGame().updateTeams(gpp.getPlayer()));
		Main.getGame().updateScoreboard(gp, 1, MessageUtil.color("&fClasse: &b" + gp.getClassTypeFromEnum().toString().substring(0, 3).toUpperCase()));
		
		p.closeInventory();
		p.updateInventory();
		
		p.playSound(p.getLocation(), Sound.CAT_MEOW, 2.5F, 0.2f);
	}
	
	private class ClassBackup {
		public GameClass clazz;
		public ClassType type;
		public GamePlayer owner;
		
		public ClassBackup(GameClass zz, ClassType t, GamePlayer g) {
			this.clazz = zz;
			this.type = t;
			this.owner = g;
		}
	}
}
