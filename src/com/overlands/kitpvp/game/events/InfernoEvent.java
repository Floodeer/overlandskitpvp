package com.overlands.kitpvp.game.events;

import com.overlands.core.utils.ActionBar;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.event.OverlandsPlayerKillTargetEvent;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.GamePlayer.PlayerState;
import com.overlands.kitpvp.game.GamePlayer.PlayerState.State;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;


import java.util.HashSet;
import java.util.Set;

public class InfernoEvent extends GameEvent {

    private int duration = 180;

    private int steps = 0;
    private int steps2 = 0;

    public InfernoEvent() {
        super(180 * 20);
    }

    /**
     * 2: Brown sky
     * 3: Red
     * 4: Darker red
     * 5: Black/red
     * 6: Multicolored
     */
    public static void changeSky() {
        for (Player p : Main.mapConfig.center.getWorld().getPlayers()) {
            Util.sendSkyPacket(p, 6);
        }
    }

    public static void resetSky() {
        for (Player p : Main.mapConfig.center.getWorld().getPlayers()) {
            Util.sendSkyPacket(p, 0);
        }
    }

    @Override
    public void onStart() {
        Main.getGame().setEvent(this);

        Main.getGame().getPlayers().forEach(player -> {
            Player p = player.getPlayer();
            if (player.hasTitles()) {
                Util.sendTitle(p, 9, 38, 12, "&r&k:::&9&lEVENTO&r&k:::", "&c&lINFERNO");
            }
            p.playSound(p.getLocation(), Sound.AMBIENCE_THUNDER, 5.0F, 0.3F);
            p.sendMessage(MessageUtil.color("&e&lCuidado com portal!"));
        });


        Location portal = Main.mapConfig.center.clone().add(0, 2.5, 0);
        changeSky();
        createGateRing(portal, "event", 1, Material.OBSIDIAN, Material.STAINED_CLAY, Material.REDSTONE_BLOCK, Material.GLOWSTONE);
        createEventHorizon();
        Main.get().getMusicCore().play("PokemonRed-BlueTitle", Main.getPM().getPlayers());

        new BukkitRunnable() {
            int abilityStep = 0;
            int damageStep = 9;

            public void run() {
                this.abilityStep += 1;
                if ((this.abilityStep == 320)) {
                    cancel();
                    return;
                }
                this.damageStep += 1;
                if (this.damageStep == 10) {
                    this.damageStep = 0;
                    LocationUtils.getCircle(portal.clone().add(0.0D, 1.0D, 0.0D), 5.0D, 40).stream().forEach(l -> {
                        ParticleEffect.FIREWORKS_SPARK.display(0.0F, 0.0F, 0.0F, 0.1F, 3, l, 126);
                        ParticleEffect.CLOUD.display(0.0F, 0.0F, 0.0F, 0.01F, 1, l, 126);
                    });
                }
            }
        }.runTaskTimer(Main.get(), 0L, 1L);
    }

    @Override
    public void onUpdate() {
        ++steps;
        ++steps2;
        if (steps == 20) {
            --duration;
            steps = 0;
        }
        if (steps2 == 8 * 20) {
            //TODO
            steps2 = 0;
        }

        Main.getGame().getPlayers().forEach(player -> {
            ActionBar.sendActionBar(player.getPlayer(), MessageUtil.color("&c&lDuração do evento: " + (duration)));
        });
    }

    @EventHandler
    public void onTargetKill(OverlandsPlayerKillTargetEvent e) {
        GamePlayer killer = GamePlayer.get(e.getPlayer());

        killer.addBalance(MathUtils.random(255), true, "Inferno kill");
        killer.addExp(MathUtils.random(50), true, "Inferno kill");
    }

    @Override
    public void onEnd() {
        HandlerList.unregisterAll(this);

        Main.getGame().setEvent(null);
        Main.getGame().getPlayers().forEach(gp -> {
            if (gp.getState().getState() == State.EVENT) {
                gp.setState(new PlayerState(gp, State.SAFE, 0L));
            }
            gp.getPlayer().sendMessage(MessageUtil.color("&9&lO evento acabou."));
        });

        Main.mapConfig.center.getWorld().getNearbyEntities(Main.mapConfig.center, 12, 12, 12).stream().filter(type -> type.getType() == EntityType.ARMOR_STAND).forEach(a -> a.remove());
        resetSky();
        Main.get().getMusicCore().stop();
    }

    static class HorizonAnimation extends BukkitRunnable {
        Set<ArmorStand> stnads;
        Location gateloc;
        float angle = 0.0F;
        int waitforrailsound = 0;

        public HorizonAnimation(Set<ArmorStand> stands, Location l) {
            this.stnads = stands;
            this.gateloc = l;
        }

        @Override
        public void run() {
            if(Main.getGame().getEvent() == null) {
                cancel();
            }
            for (ArmorStand stand : this.stnads) {
                EulerAngle headangle = stand.getHeadPose();
                EulerAngle newheadangle = headangle.add(-Math.cos(this.angle) / 60.0D, -Math.sin(this.angle) / 60.0D, Math.sin(this.angle) / 60.0D);
                stand.setHeadPose(newheadangle);
            }
            this.angle = ((float) (this.angle + 0.3141592653589793D));
            if (this.waitforrailsound < 0) {
                this.gateloc.getWorld().playSound(this.gateloc, Sound.WATER, 1.0F, 1.0F);
                this.waitforrailsound = 50;
            }
            this.waitforrailsound -= 1;
        }
    }

    public static void createEventHorizon() {
        Location ringloc = Main.mapConfig.center.clone().add(0, 2.5, 0);

        Set<ArmorStand> stands = new HashSet();

        ringloc.setX(ringloc.getX() + 0.5D);
        ringloc.setY(ringloc.getY() + 1.5D);
        ringloc.setZ(ringloc.getZ() + 0.5D);

        int facing = 1;
        String Address = "event";
        Material horizonmat = Material.COAL_BLOCK;

        double angle2 = 0.0D;
        if (facing == 0) {
            angle2 = 0.0D;
        } else if (facing == 1) {
            angle2 = Math.toRadians(90.0D);
        } else if (facing == 2) {
            angle2 = Math.toRadians(180.0D);
        } else if (facing == 3) {
            angle2 = Math.toRadians(270.0D);
        }
        for (int j = 0; j < 3; j++) {
            for (int i = 0; i < 360; i = (int) (i + (13.0D + j * 5.8D))) {
                double angle = Math.toRadians(i);
                double r = 2.6D - j * 0.6D;
                Vector rotvec = getRotBaseVector(new Vector(0.0D, 0.0D, -0.125D), new Vector(1, 0, 0), angle, angle2);
                Location l2 = new Location(ringloc.getWorld(), ringloc.getX() + r * rotvec.getX(), ringloc.getY() + r * rotvec.getY(), ringloc.getZ() + r * rotvec.getZ());
                l2.setDirection(getFacingVector(new Vector(0, 0, -1), angle2));
                stands.add(CreateArmorstandHeadDefaultPose(l2, horizonmat, "Horizon" + Address, 0.0D, 0.0D, Math.toRadians(i + (15 + j * 5))));
            }
        }
        for (int i = 0; i < 360; i += 40) {
            double angle = Math.toRadians(i);
            double r = 0.8D;
            Vector rotvec = getRotBaseVector(new Vector(0.0D, 0.0D, -0.125D), new Vector(1, 0, 0), angle, angle2);
            Location l2 = new Location(ringloc.getWorld(), ringloc.getX() + r * rotvec.getX(), ringloc.getY() + r * rotvec.getY(), ringloc.getZ() + r * rotvec.getZ());

            l2.setDirection(getFacingVector(new Vector(0, 0, -1), angle2));

            stands.add(CreateArmorstandHeadDefaultPose(l2, horizonmat, "Horizon" + Address, 0.0D, 0.0D, Math.toRadians(i + 30)));
        }
        Vector rotvec = getRotBaseVector(new Vector(-0.45D, 0.35D, -0.125D), new Vector(1, 0, 0), 0.0D, angle2);
        Location l1 = new Location(ringloc.getWorld(), ringloc.getX() + 1.0D * rotvec.getX(), ringloc.getY() - 0.5D + 1.0D * rotvec.getY(), ringloc.getZ() + 1.0D * rotvec.getZ());

        l1.setDirection(getFacingVector(new Vector(0, 0, -1), angle2));
        stands.add(CreateArmorstandHeadDefaultPose(l1, horizonmat, "Horizon" + Address, 0.0D, 0.0D, Math.toRadians(45.0D)));

        Vector rotvec2 = getRotBaseVector(new Vector(-0.45D, 0.75D, -0.125D), new Vector(1, 0, 0), 0.0D, angle2);
        Location l2 = new Location(ringloc.getWorld(), ringloc.getX() + 1.0D * rotvec2.getX(), ringloc.getY() - 0.5D + 1.0D * rotvec2.getY(), ringloc.getZ() + 1.0D * rotvec2.getZ());

        l2.setDirection(getFacingVector(new Vector(0, 0, -1), angle2));
        stands.add(CreateArmorstandHeadDefaultPose(l2, horizonmat, "Horizon" + Address, 0.0D, 0.0D, Math.toRadians(135.0D)));

        Vector rotvec3 = getRotBaseVector(new Vector(-1.85D, 1.1D, -0.125D), new Vector(1, 0, 0), 0.0D, angle2);
        Location l3 = new Location(ringloc.getWorld(),
                ringloc.getX() + 1.0D * rotvec3.getX(),
                ringloc.getY() - 0.5D + 1.0D * rotvec3.getY(),
                ringloc.getZ() + 1.0D * rotvec3.getZ());

        l3.setDirection(getFacingVector(new Vector(0, 0, -1), angle2));
        stands.add(CreateArmorstandHeadDefaultPose(l3, horizonmat, "Horizon" + Address, 0.0D, 0.0D, Math.toRadians(225.0D)));

        Vector rotvec4 = getRotBaseVector(new Vector(-1.2D, 0.0D, -0.125D), new Vector(1, 0, 0), 0.0D, angle2);
        Location l4 = new Location(ringloc.getWorld(),
                ringloc.getX() + 1.0D * rotvec4.getX(),
                ringloc.getY() - 0.5D + 1.0D * rotvec4.getY(),
                ringloc.getZ() + 1.0D * rotvec4.getZ());

        l4.setDirection(getFacingVector(new Vector(0, 0, -1), angle2));
        stands.add(CreateArmorstandHeadDefaultPose(l4, horizonmat, "Horizon" + Address, 0.0D, 0.0D, Math.toRadians(315.0D)));

        HorizonAnimation horizonanime = new HorizonAnimation(stands, ringloc);
        horizonanime.runTaskTimer(Main.get(), 10L, 1L);
    }

    public static void createGateRing(Location loc0, String Address, int facing, Material ring, Material chevronbot, Material chevronLight, Material chevronFrame) {
        if ((facing != 1) && (facing != 2) && (facing != 3) && (facing != 0)) {
            return;
        }
        Location ringloc = new Location(loc0.getWorld(), loc0.getX(), loc0.getY(), loc0.getZ());

        ringloc.setX(ringloc.getX() + 0.5D);
        ringloc.setY(ringloc.getY() + 1.5D);
        ringloc.setZ(ringloc.getZ() + 0.5D);

        int gateAngle = 360;
        if (CheckforBlocksGate(loc0, facing)) {
            gateAngle = 270;
        }
        double angle2 = 0.0D;
        if (facing == 0) {
            angle2 = 0.0D;
        } else if (facing == 1) {
            angle2 = Math.toRadians(90.0D);
        } else if (facing == 2) {
            angle2 = Math.toRadians(180.0D);
        } else if (facing == 3) {
            angle2 = Math.toRadians(270.0D);
        }
        for (int i = 0; i < gateAngle; i += 10) {
            float angleoffset = -45.0F;
            double angle = Math.toRadians(i + angleoffset);
            double r = 3.1D;
            Vector rotvec = getRotBaseVector(new Vector(0, 0, 0), new Vector(1, 0, 0), angle, angle2);
            Location l2 = new Location(ringloc.getWorld(), ringloc.getX() + r * rotvec.getX(), ringloc.getY() + r * rotvec.getY(), ringloc.getZ() + r * rotvec.getZ());

            l2.setDirection(getFacingVector(new Vector(-1, 0, 0), angle2));

            CreateArmorstandHeadDefaultPose(l2, ring, "Ring" + Address, Math.toRadians(i + 4.5D + angleoffset), 0.0D, 0.0D);
        }
        int Symbolindex = 0;
        for (float i = 0.0F; i < 360.0F; i = (float) (i + 22.5D)) {
            double angle = Math.toRadians(i);
            float r = 2.36F;
            float d = 0.02F;

            Vector rotvec = getRotBaseVector(new Vector(0, 0, 0), new Vector(1.0F, 0.0F, d), angle, angle2);
            Location l2 = new Location(ringloc.getWorld(), ringloc.getX() + r * rotvec.getX(), ringloc.getY() + r * rotvec.getY(), ringloc.getZ() + r * rotvec.getZ());

            l2.setDirection(getFacingVector(new Vector(0, 0, -1), angle2));

            CreateArmorstandHeadDefaultPose(l2, Material.DIAMOND_BLOCK, "Ring" + Address + "Symbol", 0.0D, 0.0D, Math.toRadians(i - 90.0F));

            Symbolindex++;
        }
        int ChevronCount = 0;
        boolean groundblocked = CheckforBlocksGate(loc0, facing);
        for (int i = 0; i < 360; i += 40) {
            double r = 3.3499999046325684D;

            double iangoff = -i + 95;

            Vector rotvecBot = getRotBaseVector(new Vector(0.09D, 0.02D, 0.0D), new Vector(1.0D, 0.0D, -0.04D), Math.toRadians(iangoff - 0.5D), angle2);
            Location lbot = new Location(ringloc.getWorld(), ringloc.getX() + r * rotvecBot.getX(), ringloc.getY() + r * rotvecBot.getY(), ringloc.getZ() + r * rotvecBot.getZ());
            lbot.setDirection(getFacingVector(new Vector(0, 0, 1), angle2));
            createArmorStandHandDefaultPos(lbot, 0.0D, 0.0D, Math.toRadians(i - 0.15D), chevronbot, Address);

            r = 3.509999990463257D;
            Vector rotLight1 = getRotBaseVector(new Vector(0.09D, 0.02D, 0.0D), new Vector(1.0D, 0.0D, -0.025D), Math.toRadians(iangoff - 0.5D), angle2);
            Location lLight1 = new Location(ringloc.getWorld(), ringloc.getX() + r * rotLight1.getX(),
                    ringloc.getY() + r * rotLight1.getY(), ringloc.getZ() + r * rotLight1.getZ());
            lLight1.setDirection(getFacingVector(new Vector(0, 0, 1), angle2));
            createArmorStandHandDefaultPos(lLight1, 0.0D, 0.0D, Math.toRadians(i - 0.15D), chevronLight, Address + "Light" + "_" + ChevronCount);

            r = 3.700000047683716D;
            Vector rotLight2 = getRotBaseVector(new Vector(0.085D, 0.02D, 0.0D), new Vector(1.0D, 0.0D, -0.025D),
                    Math.toRadians(iangoff - 0.8D), angle2);
            Location lLight2 = new Location(ringloc.getWorld(), ringloc.getX() + r * rotLight2.getX(),
                    ringloc.getY() + r * rotLight2.getY(), ringloc.getZ() + r * rotLight2.getZ());
            lLight2.setDirection(getFacingVector(new Vector(0, 0, 1), angle2));
            createArmorStandHandDefaultPos(lLight2, 0.0D, 0.0D, Math.toRadians(i - 0.15D), chevronLight,
                    Address + "Light" + "_" + ChevronCount);

            r = 3.4000000953674316D;
            Vector rotFrame1 = getRotBaseVector(new Vector(0.08D, 0.02D, 0.0D), new Vector(1.0D, 0.0D, -0.04D),
                    Math.toRadians(iangoff + 3.5D), angle2);
            Location lFrame1 = new Location(ringloc.getWorld(), ringloc.getX() + r * rotFrame1.getX(),
                    ringloc.getY() + r * rotFrame1.getY(), ringloc.getZ() + r * rotFrame1.getZ());
            lFrame1.setDirection(getFacingVector(new Vector(0, 0, 1), angle2));
            createArmorStandHandDefaultPos(lFrame1, -1.0D, 0.35D, Math.toRadians(i) - 0.32D, chevronFrame, Address + "Frame_" + ChevronCount + "_LL");

            r = 3.700000047683716D;
            Vector rotFrame2 = getRotBaseVector(new Vector(0.08D, 0.02D, 0.0D), new Vector(1.0D, 0.0D, -0.04D), Math.toRadians(iangoff + 3.5D), angle2);
            Location lFrame2 = new Location(ringloc.getWorld(), ringloc.getX() + r * rotFrame2.getX(), ringloc.getY() + r * rotFrame2.getY(), ringloc.getZ() + r * rotFrame2.getZ());
            lFrame2.setDirection(getFacingVector(new Vector(0, 0, 1), angle2));
            createArmorStandHandDefaultPos(lFrame2, -1.0D, 0.35D, Math.toRadians(i) - 0.32D, chevronFrame, Address);

            r = 3.4000000953674316D;
            Vector rotFrame3 = getRotBaseVector(new Vector(0.08D, 0.02D, 0.0D), new Vector(1.0D, 0.0D, -0.04D), Math.toRadians(iangoff - 14.5D), angle2);
            Location lFrame3 = new Location(ringloc.getWorld(), ringloc.getX() + r * rotFrame3.getX(), ringloc.getY() + r * rotFrame3.getY(), ringloc.getZ() + r * rotFrame3.getZ());
            lFrame3.setDirection(getFacingVector(new Vector(0, 0, 1), angle2));
            createArmorStandHandDefaultPos(lFrame3, -1.0D, 1.1D, Math.toRadians(i) + 0.28D, chevronFrame, Address + "Frame_" + ChevronCount + "_LR");

            r = 3.700000047683716D;
            Vector rotFrame4 = getRotBaseVector(new Vector(0.08D, 0.02D, 0.0D), new Vector(1.0D, 0.0D, -0.04D), Math.toRadians(iangoff - 14.0D), angle2);
            Location lFrame4 = new Location(ringloc.getWorld(), ringloc.getX() + r * rotFrame4.getX(), ringloc.getY() + r * rotFrame4.getY(), ringloc.getZ() + r * rotFrame4.getZ());
            lFrame4.setDirection(getFacingVector(new Vector(0, 0, 1), angle2));
            createArmorStandHandDefaultPos(lFrame4, -1.0D, 1.1D, Math.toRadians(i) + 0.28D, chevronFrame, Address);
            if ((CheckforBlocksGate(loc0, facing)) && (ChevronCount == 3)) {
                i += 80;
                ChevronCount++;
                ChevronCount++;
            }
            ChevronCount++;
        }
        CreateBarrier(loc0, facing);
    }

    public static Vector getRotBaseVector(Vector offset, Vector StartVector, double ax0, double ax1) {
        double x = StartVector.getX();
        double y = StartVector.getY();
        double z = StartVector.getZ();

        double dx = x * Math.cos(ax0) - y * Math.sin(ax0);
        double dy = x * Math.sin(ax0) + y * Math.cos(ax0);
        double dz = z;

        double dx2 = dx * Math.cos(ax1) - dz * Math.sin(ax1);
        double dy2 = dy;
        double dz2 = dx * Math.sin(ax1) + dz * Math.cos(ax1);

        double ox = offset.getX();
        double oy = offset.getY();
        double oz = offset.getZ();

        double ox2 = ox * Math.cos(ax1) - oz * Math.sin(ax1);
        double oy2 = oy;
        double oz2 = ox * Math.sin(ax1) + oz * Math.cos(ax1);

        dx2 += ox2;
        dy2 += oy2;
        dz2 += oz2;

        return new Vector(dx2, dy2, dz2);
    }

    public static Vector getFacingVector(Vector baseVector, double ax0) {
        double x = baseVector.getX();
        double z = baseVector.getZ();

        double dx = x * Math.cos(ax0) - z * Math.sin(ax0);
        double dz = x * Math.sin(ax0) + z * Math.cos(ax0);

        return new Vector(dx, baseVector.getY(), dz);
    }

    private static ArmorStand CreateArmorstandHeadDefaultPose(Location loc, Material headMaterial, String name, double rotationx, double rotationy, double rotationz) {
        ArmorStand stand = loc.getWorld().spawn(loc, ArmorStand.class);
        stand.setCustomName(name);
        stand.setVisible(false);
        stand.setGravity(false);
        stand.setBasePlate(false);
        stand.setMetadata("nodamage", new FixedMetadataValue(Main.get(), null));

        stand.setArms(true);
        stand.setHelmet(new ItemStack(headMaterial));
        EulerAngle newrot = stand.getHeadPose().add(rotationx, rotationy, rotationz);
        stand.setHeadPose(newrot);
        return stand;
    }

    public static void createArmorStandHandDefaultPos(Location loc, double x, double y, double z, Material mat, String Address) {
        if (loc == null) {
            return;
        }
        ArmorStand stand = loc.getWorld().spawn(loc, ArmorStand.class);
        stand.setCustomName("chevron" + Address);
        stand.setVisible(false);
        stand.setGravity(false);
        stand.setMetadata("nodamage", new FixedMetadataValue(Main.get(), null));

        stand.setBasePlate(false);
        stand.setArms(false);
        stand.setItemInHand(new ItemStack(mat));
        EulerAngle eul = stand.getRightArmPose().add(Math.toRadians(0.0D + x), Math.toRadians(-45.0D) + y, Math.toRadians(-10.0D) + z);
        stand.setRightArmPose(eul);
    }

    @EventHandler
    public void questionMark(EntityDamageEvent e) {
        if(e.getEntity().hasMetadata("nodamage")) {
            e.setCancelled(true);
        }
    }

    public static boolean CheckforBlocksGate(Location loc, int facing) {
        for (int i = 0; i < 5; i++) {
            if ((facing == 0) || (facing == 2)) {
                Block b = loc.getWorld().getBlockAt(loc.getBlockX() - 2 + i, loc.getBlockY(), loc.getBlockZ());
                if ((b.getType().equals(Material.AIR)) || (b.getType().toString().toLowerCase().contains("glass")) || (b.getType().toString().toLowerCase().contains("water"))) {
                    return false;
                }
            } else if ((facing == 1) || (facing == 3)) {
                Block b = loc.getWorld().getBlockAt(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ() - 2 + i);
                if ((b.getType().equals(Material.AIR)) || (b.getType().toString().toLowerCase().contains("glass")) || (b.getType().toString().toLowerCase().contains("water"))) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void CreateBarrier(Location loc0, int facing) {
        Location ringloc = new Location(loc0.getWorld(), loc0.getX(), loc0.getY(), loc0.getZ());
        if ((facing == 0) || (facing == 2)) {
            for (int i = 0; i < 5; i++) {
                if (ringloc.getWorld().getBlockAt(ringloc.getBlockX() - 3, ringloc.getBlockY() + 4 - i, ringloc.getBlockZ()).getType().equals(Material.AIR)) {
                    ringloc.getWorld().getBlockAt(ringloc.getBlockX() - 3, ringloc.getBlockY() + 4 - i, ringloc.getBlockZ()).setType(Material.BARRIER);
                }
                if (ringloc.getWorld().getBlockAt(ringloc.getBlockX() - 2 + i, ringloc.getBlockY() + 5, ringloc.getBlockZ()).getType().equals(Material.AIR)) {
                    ringloc.getWorld().getBlockAt(ringloc.getBlockX() - 2 + i, ringloc.getBlockY() + 5, ringloc.getBlockZ()).setType(Material.BARRIER);
                }
                if (ringloc.getWorld().getBlockAt(ringloc.getBlockX() + 3, ringloc.getBlockY() + 4 - i, ringloc.getBlockZ()).getType().equals(Material.AIR)) {
                    ringloc.getWorld().getBlockAt(ringloc.getBlockX() + 3, ringloc.getBlockY() + 4 - i, ringloc.getBlockZ()).setType(Material.BARRIER);
                }
                if (ringloc.getWorld().getBlockAt(ringloc.getBlockX() - 2 + i, ringloc.getBlockY() - 1, ringloc.getBlockZ()).getType().equals(Material.AIR)) {
                    ringloc.getWorld().getBlockAt(ringloc.getBlockX() - 2 + i, ringloc.getBlockY() - 1, ringloc.getBlockZ()).setType(Material.BARRIER);
                }
            }
        } else if ((facing == 1) || (facing == 3)) {
            for (int i = 0; i < 5; i++) {
                if (ringloc.getWorld().getBlockAt(ringloc.getBlockX(), ringloc.getBlockY() + 4 - i, ringloc.getBlockZ() - 3).getType().equals(Material.AIR)) {
                    ringloc.getWorld().getBlockAt(ringloc.getBlockX(), ringloc.getBlockY() + 4 - i, ringloc.getBlockZ() - 3).setType(Material.BARRIER);
                }
                if (ringloc.getWorld().getBlockAt(ringloc.getBlockX(), ringloc.getBlockY() + 5, ringloc.getBlockZ() - 2 + i).getType().equals(Material.AIR)) {
                    ringloc.getWorld().getBlockAt(ringloc.getBlockX(), ringloc.getBlockY() + 5, ringloc.getBlockZ() - 2 + i).setType(Material.BARRIER);
                }
                if (ringloc.getWorld().getBlockAt(ringloc.getBlockX(), ringloc.getBlockY() + 4 - i, ringloc.getBlockZ() + 3).getType().equals(Material.AIR)) {
                    ringloc.getWorld().getBlockAt(ringloc.getBlockX(), ringloc.getBlockY() + 4 - i, ringloc.getBlockZ() + 3).setType(Material.BARRIER);
                }
                if (ringloc.getWorld().getBlockAt(ringloc.getBlockX(), ringloc.getBlockY() - 1, ringloc.getBlockZ() - 2 + i).getType().equals(Material.AIR)) {
                    ringloc.getWorld().getBlockAt(ringloc.getBlockX(), ringloc.getBlockY() - 1, ringloc.getBlockZ() - 2 + i).setType(Material.BARRIER);
                }
            }
        }
    }

    public static void RemoveBarrier(Location loc0, int facing) {
        Location ringloc = new Location(loc0.getWorld(), loc0.getX(), loc0.getY(), loc0.getZ());
        if ((facing == 0) || (facing == 2)) {
            for (int i = 0; i < 5; i++) {
                if (ringloc.getWorld().getBlockAt(ringloc.getBlockX() - 3, ringloc.getBlockY() + 4 - i, ringloc.getBlockZ()).getType().equals(Material.BARRIER)) {
                    ringloc.getWorld().getBlockAt(ringloc.getBlockX() - 3, ringloc.getBlockY() + 4 - i, ringloc.getBlockZ()).setType(Material.AIR);
                }
                if (ringloc.getWorld().getBlockAt(ringloc.getBlockX() - 2 + i, ringloc.getBlockY() + 5, ringloc.getBlockZ()).getType().equals(Material.BARRIER)) {
                    ringloc.getWorld().getBlockAt(ringloc.getBlockX() - 2 + i, ringloc.getBlockY() + 5, ringloc.getBlockZ()).setType(Material.AIR);
                }
                if (ringloc.getWorld().getBlockAt(ringloc.getBlockX() + 3, ringloc.getBlockY() + 4 - i, ringloc.getBlockZ()).getType().equals(Material.BARRIER)) {
                    ringloc.getWorld().getBlockAt(ringloc.getBlockX() + 3, ringloc.getBlockY() + 4 - i, ringloc.getBlockZ()).setType(Material.AIR);
                }
                if (ringloc.getWorld().getBlockAt(ringloc.getBlockX() - 2 + i, ringloc.getBlockY() - 1, ringloc.getBlockZ()).getType().equals(Material.BARRIER)) {
                    ringloc.getWorld().getBlockAt(ringloc.getBlockX() - 2 + i, ringloc.getBlockY() - 1, ringloc.getBlockZ()).setType(Material.AIR);
                }
            }
        } else if ((facing == 1) || (facing == 3)) {
            for (int i = 0; i < 5; i++) {
                if (ringloc.getWorld().getBlockAt(ringloc.getBlockX(), ringloc.getBlockY() + 4 - i, ringloc.getBlockZ() - 3).getType().equals(Material.BARRIER)) {
                    ringloc.getWorld().getBlockAt(ringloc.getBlockX(), ringloc.getBlockY() + 4 - i, ringloc.getBlockZ() - 3).setType(Material.AIR);
                }
                if (ringloc.getWorld().getBlockAt(ringloc.getBlockX(), ringloc.getBlockY() + 5, ringloc.getBlockZ() - 2 + i).getType().equals(Material.BARRIER)) {
                    ringloc.getWorld().getBlockAt(ringloc.getBlockX(), ringloc.getBlockY() + 5, ringloc.getBlockZ() - 2 + i).setType(Material.AIR);
                }
                if (ringloc.getWorld().getBlockAt(ringloc.getBlockX(), ringloc.getBlockY() + 4 - i, ringloc.getBlockZ() + 3).getType().equals(Material.BARRIER)) {
                    ringloc.getWorld().getBlockAt(ringloc.getBlockX(), ringloc.getBlockY() + 4 - i, ringloc.getBlockZ() + 3).setType(Material.AIR);
                }
                if (ringloc.getWorld().getBlockAt(ringloc.getBlockX(), ringloc.getBlockY() - 1, ringloc.getBlockZ() - 2 + i).getType().equals(Material.BARRIER)) {
                    ringloc.getWorld().getBlockAt(ringloc.getBlockX(), ringloc.getBlockY() - 1, ringloc.getBlockZ() - 2 + i).setType(Material.AIR);
                }
            }
        }
    }
}

