package com.overlands.kitpvp.game.events;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import com.overlands.core.utils.scheduler.SchedulerEvent;
import com.overlands.core.utils.scheduler.UpdateType;
import com.overlands.kitpvp.Main;

public abstract class GameEvent implements Listener {
	
	private boolean hasTimeLimit;
	private int maxDuration;
	
	protected int time;
	
	public abstract void onStart();
	public abstract void onUpdate();
	public abstract void onEnd();
	
	public GameEvent(boolean timeLimit) {
		this.hasTimeLimit = timeLimit;
		this.maxDuration = 0;
		onStart();
		
		Bukkit.getPluginManager().registerEvents(this, Main.get());
	}
	
	public GameEvent(int duration) {
		this.maxDuration = duration;
		this.hasTimeLimit = true;
		onStart();
		
		Bukkit.getPluginManager().registerEvents(this, Main.get());
	}
	
	@EventHandler
	public void onScheduler(SchedulerEvent event) {
		if(event.getType() == UpdateType.TICK) {
			++time;
			if(hasTimeLimit && time == maxDuration) {
				onEnd();
				return;
			}
			onUpdate();
		}
	}
	
	public int getCurrentTicks() {
		return time;
	}
	
	public void shutdown() {
		HandlerList.unregisterAll(this);
	}
	
	public void forceEnd() {
		onEnd();
	}
}
