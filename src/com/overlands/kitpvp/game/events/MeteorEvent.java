package com.overlands.kitpvp.game.events;

import java.util.List;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

import com.google.common.collect.Lists;
import com.overlands.core.utils.ActionBar;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Sounds;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.core.utils.world.WorldUtils;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.GamePlayer.PlayerState;
import com.overlands.kitpvp.game.GamePlayer.PlayerState.State;
import com.overlands.kitpvp.seasons.XmasEvents;

public class MeteorEvent extends GameEvent {

	private int duration = 60;
	private int secStep = 0;
	
	private Location start;
	private int met = 0;
	
	public MeteorEvent() {
		super(60 * 20);
	}

	@Override
	public void onStart() {
		this.start = Main.mapConfig.center;
		Main.getGame().setEvent(this);
		
		Main.getGame().getPlayers().forEach(player -> {
			Player p = player.getPlayer();
			if (player.hasTitles()) {
				Util.sendTitle(p, 9, 38, 12, "&r&k:::&9&lEVENTO&r&k:::", "&e&lCHUVA DE METEOROS");
			}
			p.playSound(p.getLocation(), Sound.AMBIENCE_THUNDER, 5.0F, 0.3F);

			p.sendMessage(MessageUtil.color("&2&m-------------&r &6&lEvento &2&m------------&r"));
			p.sendMessage(" ");
			p.sendMessage(MessageUtil.color("&c&l• &eEvite dos meteoros para ganhar coins!"));
			p.sendMessage(" ");
			p.sendMessage(MessageUtil.color("&bDuração do evento: &c1 minuto!"));
			p.sendMessage(" ");
			p.sendMessage(MessageUtil.color("&2&m---------------------------------&r"));
		});
	}

	@Override
	public void onUpdate() {
		++met;
		++secStep;
		if(secStep == 20) {
			--duration;
			secStep = 0;
		}
		if(met == 49) {
			Player p = Main.getGame().getPlayers().stream()
					.map(player -> (Player)player.getPlayer())
					.filter(player -> !WorldUtils.inSpawn(player.getLocation()))
					.filter(player -> player.getWorld().getHighestBlockAt(player.getLocation()).getType() == Material.AIR)
					.findAny().orElse(null);
			
			if(MathUtils.randomBoolean(~ 67)) {
				new Meteor(start.clone().add((double) MathUtils.randomRange(-16, 16), 0.0D,(double) MathUtils.randomRange(-16, 16)));
			}else{
				if(p != null)
					new Meteor(p.getLocation());
				else
					new Meteor(start.clone().add((double) MathUtils.randomRange(-16, 16), 0.0D,(double) MathUtils.randomRange(-16, 16)));
				
			}
			met = 0;
			
		}
		Main.getGame().getPlayers().forEach(player -> {
			ActionBar.sendActionBar(player.getPlayer(), MessageUtil.color("&c&lDuração do evento: " + (duration)));
		});
	}

	@Override
	public void onEnd() {
		HandlerList.unregisterAll(this);
		
		if(!XmasEvents.ENABLED)
			Main.mapConfig.center.getWorld().setTime(2200);
		Main.getGame().setEvent(null);
		Main.getGame().getPlayers().forEach(gp -> {
			if(gp.getState().getState() == State.EVENT) {
				gp.setState(new PlayerState(gp, State.SAFE, 0L));
			}
			gp.getPlayer().sendMessage(MessageUtil.color("&9&lO evento acabou."));
		});
	}
	
	private static class Meteor {
		private Location start;
		public Location end;
		private Vector v;
		private ArmorStand a;

		public Meteor(Location loc) {
			this.end = loc.clone().add(0, -1.0D, 0);
			this.start = loc.clone().add(0, 50.0D, 0);
			this.v = this.end.toVector().subtract(this.start.toVector()).normalize();
			this.v.multiply(0.7D);
			this.spawnMeteor();
			new BukkitRunnable() {

				@Override
				public void run() {
					if (start.distance(end) <= 1.0) {
						removeMeteor();
						explode(end.clone().add(0.0D, 1.0D, 0.0D));
						cancel();
					} else {
						move();
					}
				}
			}.runTaskTimer(Main.get(), 0, 1);
		}

		public void spawnMeteor() {
			ArmorStand stand = (ArmorStand) start.getWorld().spawnEntity(this.start.clone().subtract(0.0D, 1.7D, 0.0D),
					EntityType.ARMOR_STAND);
			stand.setVisible(false);
			stand.setSmall(false);
			stand.setHelmet(new ItemStack(Material.COAL_BLOCK));
			stand.setHeadPose(new EulerAngle((double) MathUtils.random(-30, 30), (double) MathUtils.random(-30, 30),
					(double) MathUtils.random(-30, 30)));
			a = stand;
		}

		public void removeMeteor() {
			if (a != null)
				a.remove();
		}

		public void move() {
			this.start.getWorld().playSound(this.start, Sound.ZOMBIE_INFECT, 1.05F, 0.0F);
			this.start.add(this.v);
			a.teleport(this.start.clone().add(0, 1.5D + (double) MathUtils.random(-0.5F, 0.5F), 0));
			Location stand = this.start.clone().add(this.v.clone().multiply(-6));
			ParticleEffect.EXPLOSION_NORMAL.display(0.0F, 0.0F, 0.0F, 0.1F, 1, stand.add(0.0D, 1.0D, 0.0D), 120);
			ParticleEffect.EXPLOSION_LARGE.display(0.0F, 0.0F, 0.0F, 0F, 1, stand.add(0.0D, 1.0D, 0.0D), 120);

			ParticleEffect.FLAME.display(0.2F, 0.2F, 0.2F, 0.1F, 1, a.getLocation().add(0.0D, 1.0D, 0.0D), 120);

			stand.getWorld().spigot().playEffect(stand, Effect.STEP_SOUND, Material.REDSTONE_BLOCK.getId(), (byte) 0,0.5F, 0.5F, 0.5F, 1, 1, 250);
			stand.getWorld().spigot().playEffect(stand, Effect.STEP_SOUND, Material.COAL_BLOCK.getId(), (byte) 0, 0.5F,0.5F, 0.5F, 1, 1, 250);	

		}

		public void explode(Location loc) {
			List<Player> nearbyPlayer = Lists.newArrayList();
			LocationUtils.getNearbyPlayers(loc, 3.5).stream().filter(nearby -> !WorldUtils.inSpawn(nearby.getLocation()) && GamePlayer.get(nearby).isInGame()).forEach((c) -> {
				c.damage(MathUtils.random(20));
				c.sendMessage(MessageUtil.color("&cVocê foi atingido por um meteoro."));
				nearbyPlayer.add(c);
			});
			Main.getGame().getPlayers().stream().filter(player -> !WorldUtils.inSpawn(player.getPlayer().getLocation()) && !nearbyPlayer.contains(player.getPlayer())).forEach((p) -> {
				int r = MathUtils.random(5, 60);
				p.getPlayer().sendMessage(MessageUtil.color("&a+" + r + " &9coins."));
				p.setCoins(p.getCoins()+r);
			});
			nearbyPlayer.clear();
			loc.getWorld().playSound(loc, Sounds.EXPLODE.bukkitSound(), 3.0F, 0.0F);
			ParticleEffect.EXPLOSION_HUGE.display(0.0F, 0.0F, 0.0F, 0F, 1, loc.add(0.0D, 1.0D, 0.0D), 120);
			removeMeteor();
		}
	}
}
