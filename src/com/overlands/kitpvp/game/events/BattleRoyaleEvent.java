package com.overlands.kitpvp.game.events;

import com.overlands.core.utils.ActionBar;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Runner;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.event.OverlandsPlayerKillTargetEvent;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.GamePlayer.PlayerState;
import com.overlands.kitpvp.game.GamePlayer.PlayerState.State;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.WorldBorder;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class BattleRoyaleEvent extends GameEvent {

	private int duration = 80;
	private int secStep = 0;

	private WorldBorder border;

	public BattleRoyaleEvent() {
		super(120 * 20);
	}

	@Override
	public void onStart() {
		Main.getGame().setEvent(this);
		Main.getGame().setCoinMultiplier(5);
		Main.get().getMusicCore().play("Clocks", Main.getPM().getPlayers());

		Main.getGame().getPlayers().forEach(player -> {
			Player p = player.getPlayer();
			if (player.hasTitles()) {
				Util.sendTitle(p, 9, 38, 12, "&r&k:::&9&lEVENTO&r&k:::", "&e&lBattle Royale");
			}
			p.playSound(p.getLocation(), Sound.AMBIENCE_THUNDER, 5.0F, 0.3F);
			p.sendMessage(MessageUtil.color("&eFique no centro! &c&lx5 coins!"));
		});

		Runner.make(Main.get()).delay(5*20).run(() -> {
			border = Bukkit.getWorlds().get(0).getWorldBorder();
			border.setWarningDistance(5);
			border.setDamageAmount(3);//1.5 damage per sec
			border.setWarningDistance(5);
			border.setDamageAmount(0.2);
			border.setCenter(Main.mapConfig.center);
			border.setSize(MathUtils.random(260, 350));
		});
	}

	@Override
	public void onUpdate() {

		++secStep;
		if(secStep == 20) {
			--duration;
			secStep = 0;
		}

		if(border != null) {
			if(border.getSize() >= 30) {
				border.setSize(border.getSize()-MathUtils.randomDouble(0.3d, 1.25d));
			}else{
				border.setSize(30);
			}

			Main.getGame().getPlayers().forEach(gp -> {
				if(!LocationUtils.isInArea(gp.getPlayer(), Main.mapConfig.center,32)) {
					ActionBar.sendActionBar(gp.getPlayer(), MessageUtil.color("&c&lCORRA PARA O CENTRO DO MAPA"));
				}else{
					ActionBar.sendActionBar(gp.getPlayer(), MessageUtil.color("&a&lSEGURO - FIQUE NO CENTRO"));
				}
			});
		}
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Runner.make(Main.get()).delay(60).run(() -> Main.get().getMusicCore().addPlayer(GamePlayer.get(e.getPlayer())));
	}
	
	@Override
	public void onEnd() {
		HandlerList.unregisterAll(this);
		WorldBorder border = Bukkit.getWorlds().get(0).getWorldBorder();
		border.reset();
		
		Main.get().getMusicCore().stop();
		Main.getGame().setCoinMultiplier(1);
		Main.getGame().setEvent(null);
		Main.getGame().getPlayers().forEach(gp -> {
			if(gp.getState().getState() == State.EVENT) {
				gp.setState(new PlayerState(gp, State.SAFE, 0L));
			}
			gp.getPlayer().sendMessage(MessageUtil.color("&9&lO evento acabou, borda removida."));
		});
	}

	@EventHandler
	public void onTargetKill(OverlandsPlayerKillTargetEvent e) {
		GamePlayer gp = GamePlayer.get(e.getTarget());
		gp.getPlayer().sendMessage(MessageUtil.color("&9&lVocê foi eliminado."));
	}
}
