package com.overlands.kitpvp.game.events;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerJoinEvent;

import com.overlands.core.utils.ActionBar;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Runner;
import com.overlands.core.utils.Util;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.GamePlayer.PlayerState;
import com.overlands.kitpvp.game.GamePlayer.PlayerState.State;

public class PartyEvent extends GameEvent {
	
	private int duration = 80;
	private int secStep = 0;
	
	public PartyEvent() {
		super(90 * 20);
	}

	@Override
	public void onStart() {
		Main.getGame().setEvent(this);
		Main.getGame().setCoinMultiplier(3);
		
		Main.get().getMusicCore().play("MortalKombat", Main.getPM().getPlayers());
		Main.getGame().getPlayers().forEach(player -> {
			Player p = player.getPlayer();
			if (player.hasTitles()) {
				Util.sendTitle(p, 9, 38, 12, "&r&k:::&9&lEVENTO&r&k:::", "&e&lFESTA!");
			}
			p.playSound(p.getLocation(), Sound.AMBIENCE_THUNDER, 5.0F, 0.3F);

			p.sendMessage(MessageUtil.color("&2&m-------------&r &6&lEvento &2&m------------&r"));
			p.sendMessage(" ");
			p.sendMessage(MessageUtil.color("&c&l• &eMate! &c&l3x &ecoins e &c&l3x &eEXP!"));
			p.sendMessage(" ");
			p.sendMessage(MessageUtil.color("&bDuração do evento: &c1 minuto e 30s!"));
			p.sendMessage(" ");
			p.sendMessage(MessageUtil.color("&2&m---------------------------------&r"));
		});
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Runner.make(Main.get()).delay(60).run(() -> Main.get().getMusicCore().addPlayer(GamePlayer.get(e.getPlayer())));
	}
	
	@Override
	public void onEnd() {
		HandlerList.unregisterAll(this);
		
		Main.get().getMusicCore().stop();
		Main.getGame().setCoinMultiplier(1);
		Main.getGame().setEvent(null);
		Main.getGame().getPlayers().forEach(gp -> {
			if(gp.getState().getState() == State.EVENT) {
				gp.setState(new PlayerState(gp, State.SAFE, 0L));
			}
			gp.getPlayer().sendMessage(MessageUtil.color("&9&lO evento acabou."));
		});
	}
	
	@Override
	public void onUpdate() {
		++secStep;
		if(secStep == 20) {
			--duration;
			secStep = 0;
		}
		Main.getGame().getPlayers().forEach(player -> {
			ActionBar.sendActionBar(player.getPlayer(), MessageUtil.color("&9&lDuração do evento: " + (duration)));
		});
	}
}
