package com.overlands.kitpvp.game.events;

import java.util.List;

import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Chicken;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityUnleashEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.google.common.collect.Lists;
import com.overlands.core.utils.FireworkUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.core.utils.reflection.ReflectedArmorStand;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;

public class Package extends GameEvent {

	private Location start;
	private Block block;
	private ArmorStand aPackage;
	private List<Chicken> chickens;
	private ReflectedArmorStand hologram;

	public Package() {
		super(-1);
	}

	public void setStart(Location l) {
		this.start = l;
	}

	@Override
	public void onStart() {
		chickens = Lists.newArrayList();

		if(start == null) {
			start = Main.mapConfig.center;
		}

		start = start.clone().add((double) MathUtils.randomRange(-16, 16), 0.0D,(double) MathUtils.randomRange(-16, 16));

		Main.getGame().getPlayers().forEach(player -> {
			Player p = player.getPlayer();
			if(player.hasTitles()) {
				Util.sendTitle(p, 9, 38, 12, "&r&k:::&9&lPACKAGE&r&k:::", "");
			}
			p.playSound(p.getLocation(), Sound.AMBIENCE_THUNDER, 5.0F, 0.3F);

			p.sendMessage(MessageUtil.color("&2&m-------------&r &6&lEvento &2&m------------&r"));
			p.sendMessage(" ");
			p.sendMessage(MessageUtil.color("&c&l• &eUm care package de coins e exp está prestes a cair!"));
			p.sendMessage(" ");
			p.sendMessage(MessageUtil.color("&2&m---------------------------------&r"));
		});

		Location corner = start.clone().add(0, 50, 0);
		aPackage = corner.getWorld().spawn(corner, ArmorStand.class);
		aPackage.setVelocity(new Vector(0, 0, 0));
		aPackage.setVisible(false);
		aPackage.setCanPickupItems(false);
		aPackage.setSmall(false);
		aPackage.setHelmet(ItemFactory.create(Material.CHEST));
		for (int i = 0; i < 20; i++) {
			Chicken chicken = (Chicken) aPackage.getWorld().spawnEntity(aPackage.getLocation().add(MathUtils.randomDouble(0, 0.5), 3, MathUtils.randomDouble(0, 0.5)), EntityType.CHICKEN);
			chickens.add(chicken);
			chicken.setLeashHolder(aPackage);
		}
		new BukkitRunnable() {
			int step = 0;
			@Override
			public void run() {
				++step;
				if(step == 20) {
					FireworkUtils.playInstantFirework(aPackage.getLocation(), FireworkEffect.builder().with(Type.BALL).withColor(FireworkUtils.getRandomBukkitColor()).build());
					startCircles(start, 1.5F, 0.04F, 50, 20, 1);
					step = 0;
				}
				if (!checkForAir(aPackage) && aPackage.getVelocity().getY() < -0.3)
					aPackage.setVelocity(aPackage.getVelocity().add(new Vector(0, 0.1, 0)));
				if (checkForAir(aPackage)) {
					onEnd();
					cancel();
				}
			}
		}.runTaskTimer(Main.get(), 0, 1);
	}

	private boolean checkForAir(Entity e) {
		return e.getLocation().getBlock().getRelative(BlockFace.DOWN).getType() != Material.AIR;
	}

	@EventHandler
	public void onLeashBreak(EntityUnleashEvent event) {
		if (chickens.contains(event.getEntity())) {
			event.getEntity().getNearbyEntities(3, 3, 3).stream().filter(ent -> ent instanceof Item && ((Item) ent).getItemStack().getType() == Material.LEASH).forEachOrdered(Entity::remove);
		}
	}

	@EventHandler
	public void onChest(PlayerInteractEvent e) {
		if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Block b = e.getClickedBlock();
			if(b.hasMetadata("package")) {
				b.setType(Material.AIR);
				e.setCancelled(true);
				b.getLocation().getWorld().playSound(b.getLocation(), Sound.ENDERDRAGON_GROWL, 1, 1);
				GamePlayer p = GamePlayer.get(e.getPlayer());
				p.getPlayer().sendMessage(MessageUtil.color("&e&lVocê coletou o care package!"));
				p.addBalance(MathUtils.random(150, 500), true, "care package");
				p.addExp(MathUtils.random(15, 60), true, "care package");
				FireworkUtils.playInstantFirework(b.getLocation());
				if(hologram != null)
					hologram.remove();
				shutdown();
			}
		}
	}

	@Override
	public void onEnd() {
		Location endLoc = aPackage.getLocation();
		endLoc.getWorld().strikeLightningEffect(endLoc);
		Block b = endLoc.clone().subtract(0, 0.5, 0).getBlock();
		b.setType(Material.CHEST);
		b.setMetadata("package", new FixedMetadataValue(Main.get(), null));
		hologram = new ReflectedArmorStand(b.getLocation().clone().add(0.5, 0.2, 0.5));
		hologram.setSmall(true);
		hologram.setDisplayName(MessageUtil.color("&b&lCLIQUE PARA COLETAR"));
		hologram.setVisible(false);
		hologram.spawnArmorStand();
		chickens.forEach(chickens -> {
			chickens.setLeashHolder(null);
			chickens.remove();
		});
		if(aPackage != null)
			aPackage.remove();
		chickens.clear();
		Main.getGame().getPlayers().forEach(player -> {
			Player p = player.getPlayer();
			if(player.hasTitles()) {
				Util.sendTitle(p, 9, 63, 12, "&c&lCare Package!", "&e" + endLoc.getBlockX() + "&f, &e" + endLoc.getBlockY() + "&f, &e" + endLoc.getBlockZ());
			}
		});

		block = b;
	}

	@Override
	public void onUpdate() {
		// TODO Auto-generated method stub

	}

	public static void startCircles(final Location l, final float f, final float f2, final int f3, final int i, int i2) {
		new BukkitRunnable() {
			int step = 0;
			float size2 = f;
			@Override
			public void run() {
				++this.step;
				this.size2 += f2;
				if (this.step <= i) {
					for (Location loc : LocationUtils.getCircle(l, size2, f3)) {
						ParticleEffect.REDSTONE.display(new ParticleEffect.OrdinaryColor(150, 255, 255), loc, 25);
					}
				} else {
					this.cancel();
				}
			}
		}.runTaskTimer(Main.get(), 0L, i2);
	}

	protected boolean hasCollision(Location l) {
		if (!(l.getWorld().getBlockAt((l.getBlockX() + 1), l.getBlockY(), l.getBlockZ()).getType() == Material.AIR)) {
			return true;
		}

		if (!(l.getWorld().getBlockAt((l.getBlockX() - 1), l.getBlockY(), l.getBlockZ()).getType() == Material.AIR)) {
			return true;
		}

		if (!(l.getWorld().getBlockAt(l.getBlockX(), l.getBlockY(), (l.getBlockZ() + 1)).getType() == Material.AIR)) {
			return true;
		}

		if (!(l.getWorld().getBlockAt(l.getBlockX(), l.getBlockY(), (l.getBlockZ() - 1)).getType() == Material.AIR)) {
			return true;
		}
		return false;

	}
}
