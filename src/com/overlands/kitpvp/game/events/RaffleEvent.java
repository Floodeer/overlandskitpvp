package com.overlands.kitpvp.game.events;

import static java.util.Comparator.comparing;
import static java.util.Comparator.reverseOrder;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.gmail.filoghost.holographicdisplays.api.handler.PickupHandler;
import com.gmail.filoghost.holographicdisplays.api.line.ItemLine;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.overlands.core.utils.ActionBar;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Runner;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.event.OverlandsPlayerKillTargetEvent;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.GamePlayer.PlayerState;
import com.overlands.kitpvp.game.GamePlayer.PlayerState.State;

public class RaffleEvent extends GameEvent {
	
	public RaffleEvent() {
		super(false);
	}
	
	private List<Location> locations;
	private List<Hologram> holograms;
	private int step = 0;

	@Override
	public void onStart() {
		locations = Lists.newArrayList();
		holograms = Lists.newArrayList();
		
		Main.getGame().setEvent(this);
		Main.getGame().setInstances(false);

		Main.getGame().getPlayers().forEach(player -> {
			Player p = player.getPlayer();
			if (player.hasTitles()) {
				Util.sendTitle(p, 9, 38, 12, "&r&k:::&9&lEVENTO&r&k:::", "&e&lLOTERIA!");
			}
			p.playSound(p.getLocation(), Sound.AMBIENCE_THUNDER, 5.0F, 0.3F);

			p.sendMessage(MessageUtil.color("&2&m-------------&r &6&lEvento &2&m------------&r"));
			p.sendMessage(" ");
			p.sendMessage(MessageUtil.color("&c&l• &eColete &c&lTICKETS &eespalhados pelo mapa!"));
			p.sendMessage(MessageUtil.color("&c&l• &eMate players para conseguir &c&lTICKETS &eextras!"));
			p.sendMessage(MessageUtil.color("&c&l• &eVence quem atingir &c&l30 TICKETS&e!"));
			p.sendMessage(" ");
			p.sendMessage(MessageUtil.color("&2&m---------------------------------&r"));
		});
		spawnTicket(getRandomLoc(), null);
	}
	
	@Override
	public void onEnd() {
		HandlerList.unregisterAll(this);
		
		Map<Player, Integer> playerTickets = Maps.newHashMap();
		holograms.forEach(h -> h.delete());
		Main.getGame().setInstances(true);
		Main.getGame().setEvent(null);
		Main.getGame().getPlayers().forEach(gp -> {
			if(gp.getState().getState() == State.EVENT) {
				gp.setState(new PlayerState(gp, State.SAFE, 0L));
			}
			playerTickets.put(gp.getPlayer(), gp.getTickets());
			gp.setTickets(0);
		});
		
		List<Entry<Player, Integer>> places = playerTickets.entrySet().stream()
				.sorted(comparing(Entry::getValue, reverseOrder()))
                .limit(4)
                .collect(toList());
		
		Bukkit.broadcastMessage("§2§m-------------§r §6§lEvento §2§m------------§r");
		Bukkit.broadcastMessage(" ");
		Bukkit.broadcastMessage(MessageUtil.color("&7Vencedor: §a" + places.get(0).getKey().getName()));
		Bukkit.broadcastMessage(" ");
		if (places.size() >= 2) {
			Bukkit.broadcastMessage(MessageUtil.color("&e2º &7- &b" + places.get(1).getKey().getName() + " &7" + playerTickets.get(places.get(1).getKey()) + " tickets"));
		}
		if (places.size() >= 3) {
			Bukkit.broadcastMessage(MessageUtil.color("&e3º &7- &b" + places.get(2).getKey().getName() + " &7" + playerTickets.get(places.get(2).getKey()) + " tickets"));
		}
		if (places.size() >= 4) {
			Bukkit.broadcastMessage(MessageUtil.color("&e4º &7- &b" + places.get(3).getKey().getName() + " &7"+ playerTickets.get(places.get(3).getKey()) + " tickets"));
		}
		Bukkit.broadcastMessage(" ");
		Bukkit.broadcastMessage("§2§m---------------------------------§r");
	}
	
	@Override
	public void onUpdate() {
		++step;
		if(step == 10) {
			spawnTicket(getRandomLoc(), null);
			step = 0;
		}
		Main.getGame().getPlayers().forEach(player -> {
			ActionBar.sendActionBar(player.getPlayer(), MessageUtil.color("&9&lTickets: " + player.getTickets()));
		});
	}
	
	@EventHandler
	public void onTargetKill(OverlandsPlayerKillTargetEvent event) {
		Runner.make(Main.get()).delay(2).run(() -> spawnTicket(event.getLocation(), event.getTarget().getPlayer().getName()));
	}
	
	private void spawnTicket(Location loc, String player) {
		if(locations.contains(loc)) {
			return;
		}
		if(hasCollision(loc))
			return;
		final Hologram hologram = HologramsAPI.createHologram(Main.get(), loc);
		hologram.appendTextLine(MessageUtil.color(player == null ? "&c&lTicket" : "&c&lTicket de &b&l" + player));
		ItemLine line = hologram.insertItemLine(1, ItemFactory.create(Material.PAPER));
		locations.add(loc);
		line.setPickupHandler(player1 -> {
            GamePlayer gp = GamePlayer.get(player1);
            player1.sendMessage(MessageUtil.color("&b&lVocê pegou um ticket!"));
            gp.addBalance(5, true, "ticket");
            gp.addExp(5, true, "ticket");
            gp.setTickets(gp.getTickets()+1);
            if(gp.getTickets() == 50) {
                forceEnd();
            }else if(gp.getTickets() == 45) {
                Bukkit.broadcastMessage(MessageUtil.color("&c" + player1.getName() + " &eprecisa de mais 5 tickets para vencer!"));
            }else if(gp.getTickets() == 30) {
                Bukkit.broadcastMessage(MessageUtil.color("&c" + player1.getName() + " &eprecisa de mais 20 tickets para vencer!"));
            }else if(gp.getTickets() == 40) {
                Bukkit.broadcastMessage(MessageUtil.color("&c" + player1.getName() + " &eprecisa de mais 10 tickets para vencer!"));
            }
            player1.playSound(player1.getLocation(), Sound.LEVEL_UP, 1, 1);
            hologram.delete();
            locations.remove(loc);
        });
		holograms.add(hologram);
	}
	
	private Location getRandomLoc() {
		Random random = MathUtils.random;
		Location startFrom =  Main.mapConfig.center;
		Location output = startFrom.clone();
		output.add((random.nextBoolean() ? 1 : -1) * random.nextInt(100), 2.0, (random.nextBoolean() ? 1 : -1) * random.nextInt(100)); 
		return output;
	}
	
	protected boolean hasCollision(Location l) {
		if (!(l.getWorld().getBlockAt((l.getBlockX() + 1), l.getBlockY(), l.getBlockZ()).getType() == Material.AIR)) {
	        return true;  
        }
     
        if (!(l.getWorld().getBlockAt((l.getBlockX() - 1), l.getBlockY(), l.getBlockZ()).getType() == Material.AIR)) {
         	return true;        
        }
     
        if (!(l.getWorld().getBlockAt(l.getBlockX(), l.getBlockY(), (l.getBlockZ() + 1)).getType() == Material.AIR)) {
        	return true;        
        }
     
        if (!(l.getWorld().getBlockAt(l.getBlockX(), l.getBlockY(), (l.getBlockZ() - 1)).getType() == Material.AIR)) {
         	return true;
        }
		return false;

	}
}
