package com.overlands.kitpvp.game;

public class GameTagged {

	private GamePlayer player;
	private Long time;
	
	public GameTagged(GamePlayer player, Long time) {
		this.player = player;
		this.time = time;
	}
	
	public GamePlayer getPlayer() {
		return player;
	}
	
	public Long getTime() {
		return time;
	}
}
