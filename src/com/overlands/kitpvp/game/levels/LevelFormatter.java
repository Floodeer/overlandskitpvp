package com.overlands.kitpvp.game.levels;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.kitpvp.game.GamePlayer;

public class LevelFormatter {
	
	public static String withPlayerName(Player player) {
		GamePlayer gp = GamePlayer.get(player);
		return MessageUtil.color("&8[&6" + gp.getClassTypeFromEnum().toString().substring(0, 3).toUpperCase() + "&8] &7") + LevelFormatter.format(gp.getLevel(), gp.getPrestigeLevel(), false, false) + " " + ChatColor.GRAY + gp.getPlayer().getName();
	}
	
	public static String withPlayerNameIgnoringClass(Player player) {
		GamePlayer gp = GamePlayer.get(player);
		return MessageUtil.color(LevelFormatter.format(gp.getLevel(), gp.getPrestigeLevel(), false, false) + " " + ChatColor.GRAY + gp.getPlayer().getName());
	}
	
	public static String format(int level, int prestige, boolean space, boolean prestigePrefix) {
		ChatColor border = ChatColor.GRAY;
		if(prestige == 1) {
			border = ChatColor.BLUE;
		}else if(prestige == 2) {
			border = ChatColor.DARK_GREEN;
		}else if(prestige == 3) {
			border = ChatColor.GREEN;
		}else if(prestige == 4) {
			border = ChatColor.YELLOW;
		}else if(prestige == 5) {
			border = ChatColor.GOLD;
		}else if(prestige == 6) {
			border = ChatColor.RED;
		}else if(prestige == 7) {
			border = ChatColor.DARK_RED;
		}else if(prestige == 8) {
			border = ChatColor.DARK_PURPLE;
		}else if(prestige == 9) {
			border = ChatColor.LIGHT_PURPLE;
		}else if(prestige == 10) {
			border = ChatColor.WHITE;
		}else if(prestige == 11) {
			border = ChatColor.BLACK;
		}else if(prestige >= 12) {
			border = ChatColor.DARK_AQUA;
		}
		if(level > 120)
			level = 120;
		
		String withPrestige = "";
		if(prestige >= 1 && prestigePrefix)
			withPrestige = " " + MathUtils.toRomanNumeral(prestige);
		
		if(level >= 1 && level < 10) { 
			return border + "[" + level + "]" + withPrestige + (space ? " " : "");
		}else if(level >= 10 && level < 20) {
			return border + "[" + ChatColor.BLUE +  level + border + "]" + withPrestige + (space ? " " : "");
		}else if(level >= 20 && level < 30) {
			return border + "[" + ChatColor.DARK_GREEN +  level + border + "]" + withPrestige +  (space ? " " : "");
		}else if(level >= 30 && level < 40) {
			return border + "[" + ChatColor.GREEN +  level + border + "]" + withPrestige +  (space ? " " : "");
		}else if(level >= 40 && level < 50) {
			return border + "[" + ChatColor.YELLOW +  level + border + "]" + withPrestige +  (space ? " " : "");
		}else if(level >= 50 && level < 60) {
			return border + "[" + ChatColor.GOLD + ChatColor.BOLD +  level + border + "]" + withPrestige +  (space ? " " : "");
		}else if(level >= 60 && level < 70) {
			return border + "[" + ChatColor.RED + ChatColor.BOLD +  level + border + "]" + withPrestige +  (space ? " " : "");
		}else if(level >= 70 && level < 80) {
			return border + "[" + ChatColor.DARK_RED + ChatColor.BOLD +  level + border + "]" + withPrestige +  (space ? " " : "");
		}else if(level >= 80 && level < 90) {
			return border + "[" + ChatColor.DARK_PURPLE + ChatColor.BOLD +  level + border + "]" + withPrestige +  (space ? " " : "");
		}else if(level >= 90 && level < 110) {
			return border + "[" + ChatColor.LIGHT_PURPLE + ChatColor.BOLD +  level + border + "]" + withPrestige +  (space ? " " : "");
		}else if(level >= 110) {
			return border + "[" + ChatColor.WHITE + ChatColor.BOLD +  level + border + "]" + withPrestige + (space ? " " : "");
		}
		return border + "[" + level + "]" + (space ? " " : "");
	}
	
	public static int expToNextLevel(int level){
		if(level >= 1 && level < 10) { 
			return 100;
		}else if(level >= 10 && level < 20) {
			return 150;
		}else if(level >= 20 && level < 30) {
			return 200;
		}else if(level >= 30 && level < 40) {
			return 250;
		}else if(level >= 40 && level < 50) {
			return 300;
		}else if(level >= 50 && level < 60) {
			return 600;
		}else if(level >= 60 && level < 70) {
			return 800;
		}else if(level >= 70 && level < 80) {
			return 900;
		}else if(level >= 80 && level < 90) {
			return 1000;
		}else if(level >= 90 && level < 110) {
			return 1800;
		}else if(level >= 120) {
			return 0;
		}
		return level;
	}
	
	public static String formatExpToNextLevel(int level) {
		int i = 0;
		if(level >= 1 && level < 10) { 
			i = 100;
		}else if(level >= 10 && level < 20) {
			i = 150;
		}else if(level >= 20 && level < 30) {
			i = 200;
		}else if(level >= 30 && level < 40) {
			i = 250;
		}else if(level >= 40 && level < 50) {
			i = 300;
		}else if(level >= 50 && level < 60) {
			i = 600;
		}else if(level >= 60 && level < 70) {
			i = 800;
		}else if(level >= 70 && level < 80) {
			i = 900;
		}else if(level >= 80 && level < 90) {
			i = 1000;
		}else if(level >= 90 && level < 119) {
			i = 1800;
		}
		return i == 0 ? "&b&lMAX" : Integer.toString(i);
	}
	
	
	public static int expUntilNextLevel(GamePlayer gp) {
		return expToNextLevel(gp.getLevel()) - gp.getExp();
	}
}
