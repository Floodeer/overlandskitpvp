package com.overlands.kitpvp.game.levels;

import com.overlands.core.utils.GlowUtils;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.calc.MathUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;

import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.event.OverlandsPlayerPrestigeLevelUpEvent;
import com.overlands.kitpvp.game.GamePlayer;
import org.bukkit.inventory.ItemStack;

public class Prestige {

	private static boolean unlockTier(GamePlayer gp, int prestige) {
		int price = prestige == 1 ? 10000 : 15000*prestige;
		if(gp.getLevel() == 120) {
			if(gp.getCoins() >= price) {
				gp.setCoins(gp.getCoins()-price);
				gp.setExp(0);
				gp.setLevel(1);
				Bukkit.getPluginManager().callEvent(new OverlandsPlayerPrestigeLevelUpEvent(gp.getPlayer(), gp.getPrestigeLevel(), prestige));
				gp.setPrestigeLevel(prestige);
				return true;
			}else{
				gp.getPlayer().sendMessage(MessageUtil.color("&9Você não tem dinheiro para comprar o &bPrestige " + prestige + "&9."));
			}
		}else{
			gp.getPlayer().sendMessage(MessageUtil.color("&9Você precisa do nível &f&l120 &9para desbloquear o Prestige."));
		}
		return false;
	}
	
	public static void open(GamePlayer gp) {

		Main.getIconCore().create(gp.getPlayer(), MessageUtil.color("&a&lProgresso"), 9*3, event -> {
            if(event.getPosition() == 16) {
                unlockTier(gp, gp.getPrestigeLevel()+1);
            }
            event.setWillClose(true);
            event.setWillDestroy(true);

        });

		int price = gp.getPrestigeLevel() == 0 ? 10000 : 15000*(gp.getPrestigeLevel()+1);

		Main.getIconCore().setOption(gp.getPlayer(), 10, gp.getPrestigeLevel() < 1 && gp.getLevel() < 10 ? createRed() : createGreen(true),
				MessageUtil.color("&eFighter I"),
				MessageUtil.color("",
						"&7Chegue ao nível 25 para desbloquear:",
						"&7",
						"&7▪ &ex1 Slot de perk"));

				Main.getIconCore().setOption(gp.getPlayer(), 11, gp.getPrestigeLevel() < 1 && gp.getLevel() < 30 ? createRed() : createGreen(true),
				MessageUtil.color("&eFighter II"),
				MessageUtil.color("",
						"&7Chegue ao nível 30 para desbloquear:",
						"&7",
						"&7▪ &ex3 Slots de perk",
						"&7▪ &eAcesso a classes &cHero"));

		Main.getIconCore().setOption(gp.getPlayer(), 12, gp.getPrestigeLevel() < 1 && gp.getLevel() < 65 ? createRed() : createGreen(true),
				MessageUtil.color("&eFighter III"),
				MessageUtil.color("",
						"&7Chegue ao nível 65 para desbloquear:",
						"&7",
						"&7▪ &ex5 Slots de perk"));

		Main.getIconCore().setOption(gp.getPlayer(), 13, gp.getPrestigeLevel() < 1 && gp.getLevel() < 86 ? createRed() : createGreen(true),
				MessageUtil.color("&eFighter IV"),
				MessageUtil.color("",
						"&7Chegue ao nível 86 para desbloquear:",
						"&7",
						"&7▪ &eAcesso a efeitos cosmeticos",
						"&7▪ &ex15 MysteryBoxes",
						"&7▪ &ex10000 coins"));

		Main.getIconCore().setOption(gp.getPlayer(), 14, gp.getPrestigeLevel() < 1 && gp.getLevel() < 100 ? createRed() : createGreen(true),
				MessageUtil.color("&eFighter V"),
				MessageUtil.color("",
						"&7Chegue ao nível 100 para desbloquear:",
						"&7",
						"&7▪ &ex9 Slots de perk"));

		Main.getIconCore().setOption(gp.getPlayer(), 15, gp.getPrestigeLevel() < 1 && gp.getLevel() < 120 ? createRed() : createGreen(true),
				MessageUtil.color("&eFighter VI"),
				MessageUtil.color("",
						"&7Chegue ao nível 120 para desbloquear:",
						"&7",
						"&7▪ &eSlots de perk ilimitados",
						"&7▪ &eMelhoria prestígio"));



		if(gp.getLevel() == 120) {
			if(gp.getPrestigeLevel() == 0) {
				Main.getIconCore().setOption(gp.getPlayer(), 16, createRed(),
						MessageUtil.color("&ePrestígio I"),
						MessageUtil.color("",
								"&7Libere níveis prestígio:",
								"&7",
								"&7▪ &eDesbloqueia classes místicas!",
								"&7▪ &eDesbloqueia próximo nível prestígio!",
								" ",
								"&7Preço: &b" + price));
			}else{
				String next = MathUtils.toRomanNumeral(gp.getPrestigeLevel() + 1);
				Main.getIconCore().setOption(gp.getPlayer(), 16, createGreen(false),
						MessageUtil.color("&ePrestígio " + MathUtils.toRomanNumeral(gp.getPrestigeLevel()) + " &a➔ &ePrestígio " + next),
						MessageUtil.color("",
								"&7Libere seu próximo nível prestígio:",
								"&7",
								"&7▪ &eDesbloqueia próximo nível prestígio!"));
			}
		}else{
			if(gp.getPrestigeLevel() == 0) {
				Main.getIconCore().setOption(gp.getPlayer(), 16, createRed(),
						MessageUtil.color("&ePrestígio I"),
						MessageUtil.color("",
								"&7Libere níveis prestígio:",
								"&7",
								"&7▪ &eDesbloqueia classes místicas!",
								"&7▪ &eDesbloqueia próximo nível prestígio!",
								" ",
								"&7Preço: &b" + price));
			}else{
				Main.getIconCore().setOption(gp.getPlayer(), 16, createRed(),
						MessageUtil.color("&c&lPrestígio Upgrade"),
						MessageUtil.color("",
								"&eRequer nível &c120"));
			}
		}
		
		Main.getIconCore().show(gp.getPlayer());
	}

	private static ItemStack createGreen(boolean glow) {
		if(glow) {
			return GlowUtils.addGlow(ItemFactory.create(Material.STAINED_GLASS, (byte)13));
		}
		return ItemFactory.create(Material.STAINED_GLASS, (byte)13);

	}

	private static ItemStack createRed() {
		return ItemFactory.create(Material.STAINED_GLASS, (byte)14);
	}
}
