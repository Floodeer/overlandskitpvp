package com.overlands.kitpvp.game.classes.prestige;

import java.util.Collection;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.google.common.collect.Lists;
import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Runner;
import com.overlands.core.utils.Sounds;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;

public class Fighter extends GameClass {

	public Fighter() {
		super(ClassType.FIGHTER, "Fighter", Main.classConfig.fighterPrice, Main.classConfig.fighterEPH, Main.classConfig.fighterEPS);
	}

	private final List<Player> players = Lists.newArrayList();

	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()), ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.DIAMOND_SWORD, MessageUtil.color("&cSuper Punch &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);

		player.getInventory().setItem(0, sword);

		player.getInventory().setHelmet(ItemFactory.anUnbreakable(ItemFactory.create(Material.DIAMOND_HELMET)));
		player.getInventory().setChestplate(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_CHESTPLATE)));
		player.getInventory().setLeggings(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_LEGGINGS)));
		player.getInventory().setBoots(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_BOOTS)));
	}

	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		if ((!(event.getDamager() instanceof Player)) || (!(event.getEntity() instanceof Player))) {
			return;
		}
		Player damager = (Player) event.getDamager();
		Player target = (Player) event.getEntity();
		if ((!players.contains(damager))) {
			return;
		}

		if (!Util.canBeDamaged(damager, target))
			return;

		for (int i = 0; i < 40; i++) {
			Location loc = target.getLocation();
			loc.add((MathUtils.random.nextDouble() - 0.5D) * 1.25D, 0.0D, (MathUtils.random.nextDouble() - 0.5D) * 1.25D);
			ParticleEffect.FIREWORKS_SPARK.display(new Vector(0, 1, 0), MathUtils.random.nextFloat(), loc, 120);
		}
		damager.getWorld().playSound(target.getLocation(), Sounds.EXPLODE.bukkitSound(), 1.0F, 0.7F);
		damager.getWorld().playSound(target.getLocation(), Sounds.IRONGOLEM_THROW.bukkitSound(), 1.0F, 1.0F);

		damager.removePotionEffect(PotionEffectType.SPEED);
		players.remove(damager);

		punch(damager, target);
	}

	@Override
	public boolean onInteract(Player p) {
		p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 200, 1));
		p.getWorld().playSound(p.getLocation(), Sounds.FIREWORK_LAUNCH.bukkitSound(), 1.0F, 1.0F);
		GamePlayer.get(p).setCanGainEnergy(false);
		players.add(p);
		new BukkitRunnable() {
			int step = 0;
			boolean highPitch = true;
			int step2 = 4;
			@Override
			public void run() {
				if ((!p.isOnline()) || (!players.contains(p))) {
					GamePlayer.get(p).setCanGainEnergy(true);
					cancel();
					return;
				}
				if (GamePlayer.get(p).isEMPd()) {
					GamePlayer.get(p).setCanGainEnergy(true);
					p.removePotionEffect(PotionEffectType.SPEED);
					players.remove(p);
					cancel();
					return;
				}
				step2 += 1;
				if (step2 == 5) {
					p.getWorld().playSound(p.getLocation(), Sounds.NOTE_PIANO.bukkitSound(), 0.5F,highPitch ? 1.85F : 1.7F);
					highPitch = (!highPitch);
					step2 = 0;
				}
				step += 1;
				if (step <= 100) {
					ParticleEffect.FLAME.display(0.2F, 0.3F, 0.2F, 0.0F, 1,p.getLocation().clone().add(0.0D, 1.0D, 0.0D), 20);
					ParticleEffect.CLOUD.display(0.2F, 0.3F, 0.2F, 0.1F, 2,p.getLocation().clone().add(0.0D, 1.0D, 0.0D), 20);
				} else {
					p.removePotionEffect(PotionEffectType.SPEED);
					players.remove(p);
				}
			}
		}.runTaskTimer(Main.get(), 0L, 1L);

		return true;
	}

	private void punch(Player p, Player target) {
		Runner.make(Main.get()).delay(1).run(() -> target.setVelocity(p.getLocation().getDirection().multiply(6).setY(0.4)));
		
		ClassDamage.handle(p, target, 6.0);
		
		new BukkitRunnable() {
			int timeOut = 0;

			@Override
			public void run() {
				timeOut++;
				if (timeOut >= 4 * 20) {
					cancel();
					return;
				}

				if (hasCollision(target.getLocation())) {
					target.setVelocity(target.getLocation().getDirection().multiply(0.3).normalize().zero());					
					if(!GamePlayer.get(p).isInDuel()) {
						Collection<Block> blocks = LocationUtils.getNearbyBlocks(target.getLocation(), 5);				
						Main.get().getExplosionManager().blockExplosion(blocks, target.getLocation(), true, true);
					}
					target.getWorld().playSound(target.getLocation(), Sounds.EXPLODE.bukkitSound(), 3.0F, 0.0F);
					ParticleEffect.EXPLOSION_HUGE.display(0.0F, 0.0F, 0.0F, 1.0F, 1, target.getLocation(), 126);
					ClassDamage.handle(p, target, getType());
					cancel();
					return;
				}
			}
		}.runTaskTimer(Main.get(), 0, 1);
	}
	
	protected boolean hasCollision(Location l) {
		if (!(l.getWorld().getBlockAt((l.getBlockX() + 1), l.getBlockY(), l.getBlockZ()).getType() == Material.AIR)) {
	        return true;  
        }
     
        if (!(l.getWorld().getBlockAt((l.getBlockX() - 1), l.getBlockY(), l.getBlockZ()).getType() == Material.AIR)) {
         	return true;        
        }
     
        if (!(l.getWorld().getBlockAt(l.getBlockX(), l.getBlockY(), (l.getBlockZ() + 1)).getType() == Material.AIR)) {
        	return true;        
        }
     
        if (!(l.getWorld().getBlockAt(l.getBlockX(), l.getBlockY(), (l.getBlockZ() - 1)).getType() == Material.AIR)) {
         	return true;
        }
		return false;

	}
}
