package com.overlands.kitpvp.game.classes.prestige;

import java.util.List;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.google.common.collect.Lists;
import com.overlands.core.utils.ActionBar;
import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.core.utils.world.WorldUtils;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.Game;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;

public class Hunter extends GameClass {

	private final List<Player> players = Lists.newArrayList();

	public Hunter() {
		super(ClassType.HUNTER, "Hunter", Main.classConfig.hunterPrice, Main.classConfig.hunterEPH, Main.classConfig.hunterEPS);
	}

	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()),ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.IRON_SWORD, MessageUtil.color("&cEagle's Eye &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);

		ItemStack bow = ItemFactory.create(Material.BOW, MessageUtil.color("&cEagle's Eye &8(Clique-Direito)"), 10,Enchantment.ARROW_INFINITE);
		ItemFactory.unbreakable(bow);
		
		ItemFactory.applyEnchant(sword, 1, Enchantment.DAMAGE_ALL, true);

		player.getInventory().setItem(9, ItemFactory.create(Material.ARROW, 1));
		player.getInventory().setItem(0, sword);
		player.getInventory().setItem(1, bow);

		player.getInventory().setHelmet(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_HELMET)));
		player.getInventory().setChestplate(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_CHESTPLATE)));
		player.getInventory().setLeggings(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_LEGGINGS)));
		player.getInventory().setBoots(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_BOOTS)));
	}

	@Override
	public boolean onInteract(Player player) {
		if (players.contains(player)) {
			return false;
		}
		players.add(player);
		player.sendMessage(MessageUtil.color("&c&lCarregue o tiro completamente para atirar uma flecha teleguiada!"));
		player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 14*20, 0));
		GamePlayer.get(player).setCanGainEnergy(false);
		new BukkitRunnable() {
			int step = 0;
			int secs = 14;

			@Override
			public void run() {
				if (!player.isOnline() || GamePlayer.get(player).isEMPd()) {
					GamePlayer.get(player).setCanGainEnergy(true);
					players.remove(player);
					cancel();
				}
				++step;
				if (step == 14 * 20) {
					GamePlayer.get(player).setCanGainEnergy(true);
					players.remove(player);
					cancel();
					return;
				}
				if(step % 20 == 0) {
					--secs;
				}
				ActionBar.sendActionBar(player, MessageUtil.color("&b&lEagle's Eye: " + secs));
				Location loc = player.getLocation();
				for (int i = 1; i < 3; i++) {
					double d = i % 2 == 0 ? -0.8D : 0.8D;
					double x = Math.cos(Game.step * Math.PI / 18.0D) * d;
					double y = 0.1;
					double z = Math.sin(Game.step * Math.PI / 18.0D) * d;
					Vector v = new Vector(x, y, z);
					ParticleEffect.FIREWORKS_SPARK.display(v, 0.3F, loc, 180);
					ParticleEffect.REDSTONE.display(new ParticleEffect.OrdinaryColor(Color.BLACK), loc.add(v.multiply(2)), 180);
					loc.subtract(v);
				}
			}
		}.runTaskTimer(Main.get(), 0, 1);
		return true;
	}

	@EventHandler
	public void onPlayerShoot(ProjectileLaunchEvent event) {
		if (event.getEntity() instanceof Arrow) {
			Arrow arrow = (Arrow) event.getEntity();
			if (arrow.getShooter() instanceof Player) {
				Player player = (Player) arrow.getShooter();
				if ((arrow.isCritical()) && (players.contains(player))) {
					setHomingArrow(player, arrow);
				}
			}
		}
	}

	private static void setHomingArrow(Player player, Arrow arrow) {
		new BukkitRunnable() {
			Player target = null;
			final double targetRadius = 14;

			public void run() {
				if ((arrow.isDead()) || (arrow.isOnGround())) {
					cancel();
					return;
				}
				ParticleEffect.REDSTONE.display(new ParticleEffect.OrdinaryColor(255, 50, 255), arrow.getLocation(),36);
				Vector vector;
				if (target != null) {
					vector = target.getEyeLocation().toVector().subtract(arrow.getLocation().toVector()).normalize();
					arrow.setVelocity(vector);
					if ((target.isDead()) || (WorldUtils.inSpawn(target.getLocation()))) {
						target = null;
					}
				} else {
					Util.filterNearby(player, arrow.getLocation(), targetRadius).forEach(targets -> target = targets);
				}
			}
		}.runTaskTimer(Main.get(), 0L, 1L);
	}
}
