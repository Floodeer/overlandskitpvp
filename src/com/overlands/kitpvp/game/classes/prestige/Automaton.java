package com.overlands.kitpvp.game.classes.prestige;

import com.overlands.core.utils.*;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;
import com.overlands.kitpvp.preferences.ParticleFilter;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.Iterator;

public class Automaton extends GameClass {

    public Automaton() {
        super(ClassType.AUTOMATON, "Automaton", Main.classConfig.automatonPrice, Main.classConfig.automatonEPH, Main.classConfig.automatonEPS);
    }

    @Override
    public void giveItems(Player player) {
        InventoryUtils.nullInventory(player);
        InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()), ItemFactory.create(Material.MUSHROOM_SOUP));
        ItemStack sword = ItemFactory.create(Material.DIAMOND_SWORD, MessageUtil.color("&cEMP &8(Clique-Direito)"));
        ItemFactory.unbreakable(sword);

        player.getInventory().setItem(0, sword);

        player.getInventory().setHelmet(ItemFactory.anUnbreakable(ItemFactory.create(Material.DIAMOND_HELMET)));
        player.getInventory().setChestplate(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_CHESTPLATE)));
        player.getInventory().setLeggings(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_LEGGINGS)));
        player.getInventory().setBoots(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_BOOTS)));
    }

    @Override
    public boolean onInteract(Player p) {
        GamePlayer.get(p).setCanGainEnergy(false);
        final Location localLoc = p.getLocation().add(0, 0.5, 0).clone();
        p.getWorld().playSound(p.getLocation(), Sounds.ENDERMAN_DEATH.bukkitSound(), 2.3F, 0.1F);
        p.getWorld().playSound(p.getLocation(), Sounds.ZOMBIE_REMEDY.bukkitSound(), 2.5F, 0.2F);
        new BukkitRunnable() {
            double radius = 0.0D;
            float speed = 0.0F;
            float speed2 = 0.0F;

            @Override
            public void run() {
                radius += 0.35D;
                speed = ((float) (speed + 0.15D));
                speed2 = ((float) (speed2 + 0.45D));
                if (radius == 0.35D) {
                    Util.filterNearby(p, localLoc, 13.5).forEach(cur -> {
                        GamePlayer target = GamePlayer.get(cur);
                        if(target.isEMPd())
                            return;

                        ClassDamage.handle(p, cur, getType());

                        target.setCanGainEnergy(false);
                        target.setEMPd(true);
                        target.setEnergy(0);
                        Util.sendTitle(target.getPlayer(), 1, 25, 1, "&c&lHACKED", "");

                        Runner.make(Main.get()).delay(60).run(() -> {
                            target.setCanGainEnergy(true);
                            target.setEMPd(false);
                        });
                    });
                    GamePlayer.get(p).setCanGainEnergy(true);
                }
                Iterator<Location> iterator;
                if (radius < 13.5D) {
                    iterator = LocationUtils.getCircle(localLoc, radius, (int) (radius * 20)).iterator();
                    while (iterator.hasNext()) {
                        Location location = iterator.next();
                        ParticleFilter.playToLowFilter(ParticleEffect.SPELL_WITCH, 0, 0.1F, 0, speed, 1, location, 100);
                        ParticleFilter.playToMediumFilter(ParticleEffect.REDSTONE, 0, 0.1F, 0, speed, 1, location, 100);
                    }

                    iterator = LocationUtils.getCircle(localLoc, radius, (int) (radius * 15)).iterator();
                    while (iterator.hasNext()) {
                        Location location = iterator.next();
                        ParticleFilter.playToLowFilter(ParticleEffect.SPELL_WITCH, 0, 0.1F, 0, speed2, 1, location, 100);
                        ParticleFilter.playToMediumFilter(ParticleEffect.REDSTONE, 0, 0.1F, 0, speed, 1, location, 100);
                    }
                } else {
                    cancel();
                }
            }
        }.runTaskTimer(Main.get(), 0L, 1L);

        return true;
    }
}
