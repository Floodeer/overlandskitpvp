package com.overlands.kitpvp.game.classes.prestige;

import com.overlands.core.utils.calc.MathUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;

import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Runner;
import com.overlands.core.utils.Sounds;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.calc.VelocityUtils;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.core.utils.scheduler.SchedulerEvent;
import com.overlands.core.utils.scheduler.UpdateType;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;

import de.slikey.effectlib.effect.ShieldEffect;
import de.slikey.effectlib.util.DynamicLocation;
import org.bukkit.util.Vector;

public class Crusader extends GameClass {
	
	public Crusader() {
		super(ClassType.CRUSADER, "Crusader", Main.classConfig.crusaderPrice, Main.classConfig.crusaderEPH, Main.classConfig.crusaderEPS);
	}

	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()),ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.DIAMOND_SWORD,MessageUtil.color("&cDefender &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);

		player.getInventory().setItem(0, sword);

		player.getInventory().setHelmet(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_HELMET)));
		player.getInventory().setChestplate(ItemFactory.anUnbreakable(ItemFactory.create(Material.DIAMOND_CHESTPLATE)));
		player.getInventory().setLeggings(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_LEGGINGS)));
		player.getInventory().setBoots(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_BOOTS)));
	}

	@Override
	public boolean onInteract(Player player) {	
		ShieldEffect shield = new ShieldEffect(Main.get().effectManager);
		shield.setDynamicOrigin(new DynamicLocation(player.getLocation()));
		shield.radius = 5;
		shield.particle = de.slikey.effectlib.util.ParticleEffect.CRIT_MAGIC;
		shield.infinite();
		shield.start();
		Runner.make(Main.get()).delay(20).run(shield::cancel);
		
		Util.filterNearby(player, player.getLocation(), 5).forEach(cur -> {
			ClassDamage.handle(player, cur, getType());
			VelocityUtils.knockback(cur, cur.getEyeLocation(), 1.4, 0.2, true);
		});
		
		GamePlayer gp = GamePlayer.get(player);
		gp.setShield(true);
		gp.setCanGainEnergy(false);
		gp.setProtectionPercentage(.35);
		Runner.make(Main.get()).delay(90).run(() -> {
			gp.setShield(false);
			gp.setProtectionPercentage(0);
			gp.setCanGainEnergy(true);
		});
		
		player.getLocation().getWorld().playSound(player.getLocation(), Sounds.ZOMBIE_REMEDY.bukkitSound(), 1f, 1f);
		
		return true;
	}

	private final int particles = 120;
	private final int particlesPerIteration = 8;
	private final float size = 1.0F;
	private final float xFactor = 1.4F;
	private final float yFactor = 1.4F;
	private final float zFactor = 1.4F;
	private final float yOffset = 1.0F;
	private final double zRotation = 0.0D;
	private double xRotation;
	private double yRotation;
	private int step;


	@EventHandler
	public void onScheduler(SchedulerEvent event) {
		if(event.getType() == UpdateType.TICK) {
			Main.getGame().getPlayers().stream().filter(gp -> gp.hasShield() && gp.getSelectedClass().getType().equals(getType())).forEach(gp -> {
				Location loc = gp.getPlayer().getLocation();
				Vector vec = new Vector();
				for (int i = 0; i < this.particlesPerIteration; i++) {
					this.step += 1;

					float f1 = MathUtils.PI / this.particles * this.step;
					float f2 = (float) (Math.sin(f1) * this.size);
					float f3 = f2 * MathUtils.PI * f1;

					vec.setX(this.xFactor * f2 * Math.cos(f3));
					vec.setZ(this.zFactor * f2 * Math.sin(f3));
					vec.setY(this.yFactor * Math.cos(f1) + this.yOffset);

					VelocityUtils.rotateVector(vec, this.xRotation, this.yRotation, this.zRotation);

					ParticleEffect.CRIT_MAGIC.display(0.0F, 0.0F, 0.0F, 0.0F, 2, loc.add(vec), 96);
					loc.subtract(vec);
				}
			});
		}
	}
	
	@EventHandler
	public void onDamage(EntityDamageEvent e) {
		if(e.getEntity() instanceof Player) {
			Player p = (Player)e.getEntity();
			GamePlayer gp = GamePlayer.get(p);
			if(gp.hasShield()) {
				double damage = e.getDamage() - (e.getDamage() * gp.getProtectionPercentage());
				e.setDamage(damage);
				p.getLocation().getWorld().playSound(p.getLocation(), Sounds.ZOMBIE_WOOD.bukkitSound(), 1f, 0.7f);
			}
		}
	}
}
