package com.overlands.kitpvp.game.classes.prestige;

import java.util.List;

import com.overlands.kitpvp.game.GamePlayer;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.common.collect.Lists;
import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.core.utils.world.WorldUtils;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;
import com.overlands.kitpvp.preferences.ParticleFilter;

public class Shadow extends GameClass {

	public Shadow() {
		super(ClassType.SHADOW, "Shadow", Main.classConfig.shadowPrice, Main.classConfig.shadowEPH, Main.classConfig.shadowEPH);
	}

	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()), ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.DIAMOND_SWORD, MessageUtil.color("&cShadow Step &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);

		player.getInventory().setItem(0, sword);

		player.getInventory().setHelmet(ItemFactory.anUnbreakable(ItemFactory.create(Material.DIAMOND_HELMET)));
		player.getInventory().setChestplate(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_CHESTPLATE)));
		player.getInventory().setLeggings(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_LEGGINGS)));
		player.getInventory().setBoots(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_BOOTS)));
	}

	@Override
	public boolean onInteract(Player player) {
	      Location location = player.getLocation();
	      
		  List<Player> blacklisted = Lists.newArrayList();
	      List<Player> players = Lists.newArrayList(Util.filterNearby(player, location, 50));
	      if(players.isEmpty())
	    	  return false;
	      
	      players.forEach(target -> target.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 120, 0)));
	      
	      location.getWorld().playSound(location, Sound.WITHER_SPAWN, 1.0F, 0.0F);
	      player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 120, 0));
	      new BukkitRunnable() {
	    	int step = 0;
	        int step2 = 0;
	        boolean forceTeleport = false;
			@Override
			public void run() {
				if(players.isEmpty() || player.isDead() || !player.isOnline() || WorldUtils.inSpawn(player.getLocation()) ||GamePlayer.get(player).isEMPd()) {
					cancel();
					return;
				}
				players.forEach(target-> {
					  ++step;
					  if(step <= 5) {
                          if(!blacklisted.contains(target)) {
                              if(step > 1) {
                                  displaySmoke(player.getLocation());
                                  blacklisted.add(target);
                                  ClassDamage.handle(player, target, getType());
                              } else {
                                  displaySmoke(location);
                              }
                          }
                          
                          Location loc = target.getLocation();
                          if(WorldUtils.inSpawn(loc)) {
                        	  return;
                          }
                          loc.setPitch(0.0F);
                          loc.subtract(loc.getDirection().multiply(1));
                          player.teleport(loc);
                          player.getWorld().playSound(player.getLocation(), Sound.ENDERMAN_TELEPORT, 5.0F, 2.0F);
                          ++step2;
                          if(step2 == players.size()) {
                              step2 = 0;
                          }
					  }else {
						  if(target.hasPotionEffect(PotionEffectType.BLINDNESS))
							  target.removePotionEffect(PotionEffectType.BLINDNESS);
						  if(player.hasPotionEffect(PotionEffectType.BLINDNESS))
							  player.removePotionEffect(PotionEffectType.BLINDNESS);
						  
						  if(!players.isEmpty() || this.forceTeleport) {
                              player.teleport(location);
                          }

                          this.cancel();
					  }
					  player.removePotionEffect(PotionEffectType.BLINDNESS);
	                  players.remove(player);
	                  if(players.isEmpty()) {
	                       forceTeleport = true;
	                 }
				});
			}
		}.runTaskTimer(Main.get(), 20L, 12L);
		
		return true;
	}
	
	private static void displaySmoke(final Location paramLocation) {
		final Location localLocation = LocationUtils.getLocation(paramLocation, 0.0D, 0.0D, 0.0D);
		new BukkitRunnable() {
			int step = 0;
			int step2 = 0;
			int step3 = 0;
			int step4 = 0;

			public void run() {
				step += 1;
				if (step <= 160) {
					List<Location> circle1 = LocationUtils.getCircle(LocationUtils.getLocation(localLocation, 0.0D, 0.0D, 0.0D), 0.6D, 50);
					List<Location> circle2 = LocationUtils.getCircle(LocationUtils.getLocation(localLocation, 0.0D, 0.5D, 0.0D), 0.5D, 50);
					List<Location> circle3 = LocationUtils.getCircle(LocationUtils.getLocation(localLocation, 0.0D, 1.0D, 0.0D), 0.6D, 50);

					ParticleFilter.playWithFilter(ParticleEffect.SMOKE_LARGE, 0.0F, 0.0F, 0.0F, 0.0F, 1, circle1.get(step2), 126, 2);	
					ParticleFilter.playWithFilter(ParticleEffect.SMOKE_LARGE, 0.0F, 0.0F, 0.0F, 0.0F, 1, circle2.get(circle2.size() - 1 - step3), 126, 2);	
					ParticleFilter.playWithFilter(ParticleEffect.SMOKE_LARGE, 0.0F, 0.0F, 0.0F, 0.0F, 1, circle3.get(step4), 126, 1);	

					step2 += 1;
					if (step2 == circle1.size()) {
						step2 = 0;
					}
					step3 += 1;
					if (step3 == circle2.size()) {
						step3 = 0;
					}
					step4 += 1;
					if (step4 == circle3.size()) {
						step4 = 0;
					}
				} else {
					cancel();
				}
			}
		}.runTaskTimer(Main.get(), 0L, 1L);
	}
}
