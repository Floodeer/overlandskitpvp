package com.overlands.kitpvp.game.classes.prestige;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.calc.VelocityUtils;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;

public class Elenne extends GameClass {

	public Elenne() {
		super(ClassType.ELENNE, "Elenne", Main.classConfig.elennePrice, Main.classConfig.elenneEPH, Main.classConfig.elenneEPS);
	}

	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()),ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.DIAMOND_SWORD,MessageUtil.color("&cHoly Missile &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);

		player.getInventory().setItem(0, sword);

		player.getInventory().setHelmet(ItemFactory.anUnbreakable(ItemFactory.create(Material.GOLD_HELMET)));
		player.getInventory().setChestplate(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_CHESTPLATE)));
		player.getInventory().setLeggings(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_LEGGINGS)));
		player.getInventory().setBoots(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_BOOTS)));
	}

	@Override
	public boolean onInteract(Player player) {
		Util.heal(player, player, MathUtils.random(6), true, MathUtils.randomBoolean(), false);
		
		new BukkitRunnable() {
			final Vector v = player.getEyeLocation().getDirection();
			final Location loc = player.getEyeLocation();
			double ti = 0.0;

			public void explode(Location loc) {
				Util.filterNearby(player, loc, 2.5).forEach(cur -> {
					ClassDamage.handle(player, cur, getType());	
				});
				ParticleEffect.EXPLOSION_LARGE.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc, 250.0D);
				ParticleEffect.FIREWORKS_SPARK.display(0.0F, 0.0F, 0.0F, 0.2F, 32, loc, 250.0D);
			}

			@Override
			public void run() {
				for (int j = 0; j < 2; j++) {
					this.ti += 0.45D;
					Location loc1 = this.loc.clone().add(this.v.getX() * this.ti, this.v.getY() * this.ti, this.v.getZ() * this.ti);
					if (loc1.getBlock().getType().isSolid()) {
						explode(loc1);
						cancel();
						return;
					}
					loc1.getWorld().playSound(loc1, Sound.ORB_PICKUP, 0.7F, 0);
					for (double i = -3.141592653589793D; i < 3.141592653589793D; i += 1.5707963267948966D) {
						Vector v = new Vector(Math.cos(i + this.ti / 4.0D), Math.sin(i + this.ti / 4.0D), 0.0D);
						v = VelocityUtils.rotateAroundFixedLoc(v, this.loc);
						ParticleEffect.FIREWORKS_SPARK.display(v, 0.08F, loc1, 250.0D);
					}
					LocationUtils.getNearbyPlayers(loc1, 2.5).forEach(nearby -> {
						if(Util.canBeDamaged(player, nearby)) {
							explode(nearby.getLocation());
							cancel();
						}else if(GamePlayer.get(player).getTeam() == GamePlayer.get(nearby)) {
							Util.heal(player, nearby, MathUtils.random(6), true, MathUtils.randomBoolean(), false);
						}
					});
					if (this.ti > 250.0D) {
						cancel();
					}
				}
			}
		}.runTaskTimer(Main.get(), 0L, 1L);
		return true;
	}
}
