package com.overlands.kitpvp.game.classes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.WordUtils;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;
import com.mojang.authlib.GameProfile;
import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MenuUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.skins.DefaultSkins;

public class ClassSelector implements Listener {
 
	private List<String> hunter() {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §c§lSniper§e");
		l.add("§7Dificuldade: §e••••§7•");
		l.add(" ");
		l.add("§eHabilidade §7- §bEagle's Eye");
		l.add("§8▪ §7Ao atirar com arco carregado com");
		l.add("§7  §7sua habilidade ativa a flecha");
		l.add("§7  §7será teleguiada até acertar");
		l.add("§7  §7um player próximo ou morrer.");
		l.add("§8▪ §7Aplica regeneração §cI §7(§c14s§7).");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.hunterEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.hunterEPH);
		l.add(" ");
		l.add("§4§lRequer §cPrestige I");
		return l;
	}
	
	private List<String> squid(GamePlayer gp) {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §bSuporte");
		l.add("§7Dificuldade: §e•••§7••");
		l.add(" ");
		l.add("§eHabilidade §7- §bSplash");
		l.add("§8▪ §7Sumona círculos de partículas");
		l.add("§7  §7em players próximos.");
		l.add("§8▪ §7Cura §a0.75§c❤ §7inicialmente");
		l.add("§7  §7e mais §a0.5§c❤ §7a cada player");
		l.add("§7  §7atingido pela habilidade.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(gp, ClassType.SQUID) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.squidEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.squidEPH);
		return l;
	}
	
	private List<String> herobrine(GamePlayer gp) {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §cOfensivo§7, §4Dano");
		l.add("§7Dificuldade: §e••§7•••");
		l.add(" ");
		l.add("§eHabilidade §7- §cLightstrike");
		l.add("§8▪ §7Sumona um raio em players");
		l.add("§7  §7próximos.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(gp, ClassType.HEROBRINE) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.herobrineEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.herobrineEPH);
		return l;
	}
		
	private List<String> creeper(GamePlayer gp) {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §cOfensivo§7, §4Dano");
		l.add("§7Dificuldade: §e••§7•••");
		l.add(" ");
		l.add("§eHabilidade §7- §cDetonate");
		l.add("§8▪ §7Explode após 3 segundos causando");
		l.add("§7  §7dano em players próximos.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(gp, ClassType.CREEPER) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.creeperEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.creeperEPH);
		return l;
	}
	
	private List<String> fighter() {
		List<String> l = Lists.newArrayList();
		l.add("§4§lPrestige Fighter");
		l.add(" ");
		l.add("§7Gameplay: §cDano");
		l.add("§7Dificuldade: §e•••••");
		l.add(" ");
		l.add("§eHabilidade §7- §cSuper Punch");
		l.add("§8▪ §7Aplica um super soco que causará");
		l.add("§8  §7mais dano se o inimigo acertar");
		l.add("§7  §7uma parede.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(null, ClassType.FIGHTER) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.fighterEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.fighterEPH);
		l.add(" ");
		l.add("§4§lRequer §cPrestige I");
		return l;
	}
	
	private List<String> shadow() {
		List<String> l = Lists.newArrayList();
		l.add("§4§lPrestige Shadow");
		l.add(" ");
		l.add("§7Gameplay: §cDano");
		l.add("§7Dificuldade: §e••••§7•");
		l.add(" ");
		l.add("§eHabilidade §7- §cShadow Step");
		l.add("§8▪ §7Teleporta você para players");
		l.add("§8  §7aleatórios durante um período");
		l.add("§7  §7de tempo.");
		l.add("§8▪ §7Aplica cegueira em área");
		l.add("§7  §7durante a habilidade.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(null, ClassType.SHADOW) + "§l❤");
		l.add("§8▪ §7Dano falso: §bProteção da armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.shadowEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.shadowEPH);
		l.add(" ");
		l.add("§4§lRequer §cPrestige I");
		return l;
	}

	private List<String> automaton() {
		List<String> l = Lists.newArrayList();
		l.add("§4§lPrestige Automaton");
		l.add(" ");
		l.add("§7Gameplay: §bDefensivo");
		l.add("§7Dificuldade: §e•••••");
		l.add(" ");
		l.add("§eHabilidade §7- §cEMP");
		l.add("§8▪ §7Causa dano em área,");
		l.add("§8  §7e aplica o debuff '§dhacked§7.'");
		l.add(" ");
		l.add("§8▪ '§dhacked§7' aplica os seguintes");
		l.add("§8   §7efeitos:");
		l.add("§c▪  §7Desativa o carregamento de habilidade");
		l.add("§c▪  §7Desativa as habilidades ativas");
		l.add("§c▪  §7Desativa o uso de habilidades");
		l.add("§c▪  §7Zera energia de habilidades");
		l.add(" ");
		l.add("§8▪ §7Debuff '§dhacked§7' não se aplica a");
		l.add("§8  §7inimigos com escudo.");
		l.add(" ");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(null, ClassType.AUTOMATON) + "§l❤");
		l.add("§8▪ §7Dano falso: §bProteção da armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.automatonEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.automatonEPH);
		l.add(" ");
		l.add("§4§lRequer §cPrestige I");
		return l;
	}
	
	private List<String> crusader() {
		List<String> l = Lists.newArrayList();
		l.add("§4§lPrestige Crusader");
		l.add(" ");
		l.add("§7Gameplay: §4Tank§7, §bDefensivo");
		l.add("§7Dificuldade: §e•••§7••");
		l.add(" ");
		l.add("§eHabilidade §7- §bDefender");
		l.add("§8▪ §7Cria uma pequena explosão causando");
		l.add("§7  §7dano em área.");
		l.add("§8▪ §7Aplica um escudo que consome §c35%");
		l.add("§7  §7do dano recebido durante §c4.5s§7.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(null, ClassType.CRUSADER) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.crusaderEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.crusaderEPH);
		l.add(" ");
		l.add("§4§lRequer §cPrestige I");
		return l;
	}
	
	private List<String> elenne() {
		List<String> l = Lists.newArrayList();
		l.add("§4§lPrestige Elenne");
		l.add(" ");
		l.add("§7Gameplay: §bSuporte");
		l.add("§7Dificuldade: §e•••••");
		l.add(" ");
		l.add("§eHabilidade §7- §bHoly Missile");
		l.add("§8▪ §7Atira um míssil em linha reta");
		l.add("§7  §7causando dano em inimigos e");
		l.add("§8▪ §7regenerando um aliado.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(null, ClassType.ELENNE) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b5");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.elenneEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.elenneEPH);
		l.add(" ");
		l.add("§4§lRequer §cPrestige I");
		return l;
	}
	
	private List<String> pirate(GamePlayer gp) {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §cOfensivo§7, §4Dano");
		l.add("§7Dificuldade: §e•••••");
		l.add(" ");
		l.add("§eHabilidade §7- §cFire Cannon");
		l.add("§8▪ §7Atira uma bola de canhão em");
		l.add("§7  §7linha reta causando dano");
		l.add("§7  §7ao criar impacto.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(gp, ClassType.PIRATA) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.pirataEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.pirataEPH);
		return l;
	}
	
	private List<String> wizard(GamePlayer gp) {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §c§lSniper§e");
		l.add("§7Dificuldade: §e•••••");
		l.add(" ");
		l.add("§eHabilidade §7- §cDust Spell");
		l.add("§8▪ §7Atira uma partículas teleguiadas");
		l.add("§7  §7que causando dano ao");
		l.add("§7  §7criar impacto.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(gp, ClassType.WIZARD) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.wizardEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.wizardEPH);
		return l;
	}
	
	private List<String> falcon(GamePlayer gp) {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §cOfensivo§7, §4Dano§e");
		l.add("§7Dificuldade: §e••§7•••");
		l.add(" ");
		l.add("§eHabilidade §7- §cFalcon Punch");
		l.add("§8▪ §7Carrega um super soco que");
		l.add("§7  §7será ativado no próximo");
		l.add("§7  §7inimigo que você atingir.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(gp, ClassType.FALCON) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.falconEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.falconEPH);
		return l;
	}
	
	private List<String> magma(GamePlayer gp) {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §8Mobilidade§e");
		l.add("§7Dificuldade: §e••••§7•");
		l.add(" ");
		l.add("§eHabilidade §7- §cFlame Dash");
		l.add("§8▪ §7Recebe um boost de velcoidade");
		l.add("§7  §7pra frente causando dano.");
		l.add("§8▪ §7Aplica fogo por §b3s §7em");
		l.add("§7  §7inimigos atingidos.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(gp, ClassType.MAGMA) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.magmaEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.magmaEPH);
		return l;
	}
	
	private List<String> magnatition(GamePlayer gp) {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §4Dano");
		l.add("§7Dificuldade: §e••§7•••");
		l.add(" ");
		l.add("§eHabilidade §7- §cMagnetic Pulse");
		l.add("§8▪ §7Puxa os players próximos para");
		l.add("§7  §7seu local atual causando dano.");
		l.add("§8▪ §7Atordoa os players puxados");
		l.add("§7  §7por §b1.25s§7.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(gp, ClassType.MAGMA) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.magnatitionEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.magnatitionEPH);
		return l;
	}

	private List<String> electro(GamePlayer gp) {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §4Dano");
		l.add("§7Dificuldade: §e•••§7••");
		l.add(" ");
		l.add("§eHabilidade §7- §cElectronic Pulse");
		l.add("§8▪ §7Cria um circulo de partículas");
		l.add("§7  §7que atinge players com raios.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(gp, ClassType.ELECTRO) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.electroEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.electroEPH);
		return l;
	}
	
	private List<String> epsilon(GamePlayer gp) {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §4Dano");
		l.add("§7Dificuldade: §e••••§7•");
		l.add(" ");
		l.add("§eHabilidade §7- §cStatic Blast");
		l.add("§8▪ §7Cria uma onda de partículas");
		l.add("§7  §7causando dano em área.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(gp, ClassType.EPSILON) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.epsilonEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.epsilonEPH);
		return l;
	}
	
	private List<String> cryomancer(GamePlayer gp) {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §cOfensivo");
		l.add("§7Dificuldade: §e•••§7••");
		l.add(" ");
		l.add("§eHabilidade §7- §cFreezing Breath");
		l.add("§8▪ §7Cria um cone de partículas");
		l.add("§7  §7causando dano em área.");
		l.add("§8▪ §7Aplica lentidão §cI §7por");
		l.add("§7  §c3s §7em players atingidos.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(gp, ClassType.CRYOMANCER) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.cryomancerEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.cryomancerEPH);
		return l;
	}
	
	private List<String> spider(GamePlayer gp) {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §8Mobilidade§7, §cOfensivo");
		l.add("§7Dificuldade: §e•••••");
		l.add(" ");
		l.add("§eHabilidade §7- §cDrop Shock");
		l.add("§8▪ §7Lançará você para a direção");
		l.add("§7  §7que está olhando aplicando");
		l.add("§7  §7uma proteção temporária.");
		l.add("§8▪ §7Qualquer dano de queda");
		l.add("§7  §7causará dano em inimigos");
		l.add("§7  §7próximos.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(gp, ClassType.SPIDER) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.spiderEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.spiderEPH);
		return l;
	}
	
	private List<String> enderman(GamePlayer gp) {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §8Mobilidade§7, §cOfensivo");
		l.add("§7Dificuldade: §e•§7••••");
		l.add(" ");
		l.add("§eHabilidade §7- §bTeleport");
		l.add("§8▪ §7Teleporta você para o inimigo");
		l.add("§7  §7em sua linha de visão até");
		l.add("§7  §a16 §7blocos de distância.");
		l.add("§8▪ §7Recebe velocidade §cIII");
		l.add("§7  §7por §c4s §7após o teleport.");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.endermanEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.endermanEPH);
		return l;
	}
	
	private List<String> golem(GamePlayer gp) {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §dTank");
		l.add("§7Dificuldade: §e••§7•••");
		l.add(" ");
		l.add("§eHabilidade §7- §cIron Punch");
		l.add("§8▪ §7Cria um hexágono de blocos");
		l.add("§7  §7puxando inimigos e");
		l.add("§7  §7causando dano.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(gp, ClassType.GOLEM) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.golemEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.golemEPH);
		return l;
	}
	
	private List<String> rumbler(GamePlayer gp) {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §dTank§7, §4Dano§e");
		l.add("§7Dificuldade: §e•••••");
		l.add(" ");
		l.add("§eHabilidade §7- §cRumble");
		l.add("§8▪ §7Cria um estrondo progressivo");
		l.add("§7  §7no chão que colide com");
		l.add("§7  §7players.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(gp, ClassType.RUMBLER) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b5");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.rumblerEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.rumblerEPH);
		return l;
	}

	private List<String> dreadlord(GamePlayer gp) {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §cOfensivo§7, §4Dano§e");
		l.add("§7Dificuldade: §e••§7•••");
		l.add(" ");
		l.add("§eHabilidade §7- §cWither Blast");
		l.add("§8▪ §7Atira 3 cabeças de wither");
		l.add("§7  §7em direções aleatórias apartir");
		l.add("§7  §7de sua linha de visão.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(gp, ClassType.DREADLORD) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.dreadlordEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.dreadlordEPH);
		return l;
	}
	
	private List<String> skeleton(GamePlayer gp) {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §c§lSniper");
		l.add("§7Dificuldade: §e•••§7••");
		l.add(" ");
		l.add("§eHabilidade §7- §cExplosive Arrow");
		l.add("§8▪ §7Atira uma flecha explosiva");
		l.add("§7  §7que causa dano em área.");
		l.add("§8▪ §7Aplica regeneração §cIII §7(§c3s§7)");
		l.add("§7  §7e velocidade §cII §7(§c4s§7)");
		l.add("§7  §7ao acertar uma flecha.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(gp, ClassType.SKELETON) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.skeletonEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.skeletonEPH);
		return l;
	}
	
	private List<String> blaze(GamePlayer gp) {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §c§lSniper");
		l.add("§7Dificuldade: §e••§7•••");
		l.add(" ");
		l.add("§eHabilidade §7- §cImmolating Burst");
		l.add("§8▪ §7Atira 3 bolas de fogo.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(gp, ClassType.BLAZE) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.blazeEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.blazeEPH);
		return l;
	}
	
	private List<String> lana(GamePlayer gp) {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §c§lSniper§7, §4Dano");
		l.add("§7Dificuldade: §e•••••");
		l.add(" ");
		l.add("§eHabilidade §7- §cPistol");
		l.add("§8▪ §7Arma hitscan de média longa");
		l.add("§7  §7distância que atira 5 vezes");
		l.add("§7  §7a cada 100 de energia.");
		l.add("§8▪ §7Multiplica o dano ao acertar");
		l.add("§7  §7headshot.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(gp, ClassType.LANA) + "§l❤");
		l.add("§8▪ §7Dano falso: §bProteção da armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.lanaEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.lanaEPH);
		return l;
	}

	private List<String> incendiary(GamePlayer gp) {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §4Dano");
		l.add("§7Dificuldade: §e••§7•••");
		l.add(" ");
		l.add("§eHabilidade §7- §cDoomShroom");
		l.add("§8▪ §7Constroi uma arvore de cogumelos");
		l.add("§7  §7que causa dano por segundo e");
		l.add("§7  §7explode após alguns segundos.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(gp, ClassType.INFECTED) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.infectedEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.infectedEPH);
		return l;
	}

	private List<String> shaman(GamePlayer gp) {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §4Dano");
		l.add("§7Dificuldade: §e•••§7••");
		l.add(" ");
		l.add("§eHabilidade §7- §cTornado");
		l.add("§8▪ §7Cria um tornado puxando players");
		l.add("§7  §7próximos.");
		l.add("§8▪ §7Aplica lentidão §cII §7por");
		l.add("§7  §c1.5s §7em players puxados.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(gp, ClassType.SHAMAN) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.shamanEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.shamanEPH);
		return l;
	}
	
	private List<String> pigman(GamePlayer gp) {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §dTank§7, §4Dano");
		l.add("§7Dificuldade: §e•••§7••");
		l.add(" ");
		l.add("§eHabilidade §7- §cBurning Soul");
		l.add("§8▪ §7Cria um círculo de fogo ");
		l.add("§7  §7causando dano por segundo");
		l.add("§7  §7durante §c6 §7segundos.");
		l.add("§8▪ §7Recebe força §cI §7por §c0.75s");
		l.add("§7  §7e regeneração §cII §7por §c6s");
		l.add("§7  §7após ativar a habilidade.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(gp, ClassType.PIGMAN) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.pigmanEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.pigmanEPH);
		return l;
	}
	
	private List<String> paladin(GamePlayer gp) {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §4Dano");
		l.add("§7Dificuldade: §e••§7•••");
		l.add(" ");
		l.add("§eHabilidade §7- §cMeteor Strike");
		l.add("§8▪ §7Seleciona um player para saltar");
		l.add("§7  §7e criar um impacto causando");
		l.add("§7  §7dano em área.");
		l.add("§8▪ §7Aplica lentidão §cII §7por");
		l.add("§7  §c1.5s §7em players puxados.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(gp, ClassType.PALADIN) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.paladinEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.paladinEPH);
		return l;
	}
	
	private List<String> arcanist(GamePlayer gp) {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §4Dano");
		l.add("§7Dificuldade: §e••••§7•");
		l.add(" ");
		l.add("§eHabilidade §7- §cBeam");
		l.add("§8▪ §7Atira partículas em linha");
		l.add("§7  §7reta que causam dano ao");
		l.add("§7  §7colidir com inimigos.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(gp, ClassType.ARCANIST) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.arcanistEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.arcanistEPH);
		return l;
	}
	
	private List<String> pyromancer(GamePlayer gp) {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §cOfensivo");
		l.add("§7Dificuldade: §e••§7•••");
		l.add(" ");
		l.add("§eHabilidade §7- §cFreezing Breath");
		l.add("§8▪ §7Cria um cone de partículas");
		l.add("§7  §7causando dano em área.");
		l.add("§8▪ §7Aplica fogo §7por §c2s");
		l.add("§7  §7em players atingidos.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(gp, ClassType.PYROMANCER) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.pyromancerEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.pyromancerEPH);
		return l;
	}
	
	private List<String> ancient(GamePlayer gp) {
		List<String> l = Lists.newArrayList();
		l.add("§7Gameplay: §cOfensivo");
		l.add("§7Dificuldade: §e••§7•••");
		l.add(" ");
		l.add("§eHabilidade §7- §cFreezing Breath");
		l.add("§8▪ §7Cria um cone de partículas");
		l.add("§7  §7causando dano em área.");
		l.add("§8▪ §7Aplica stun §7por §c2s");
		l.add("§7  §7em players atingidos.");
		l.add("§8▪ §7Dano: §c" + ClassDamage.get(gp, ClassType.ANCIENT) + "§l❤");
		l.add("§8▪ §7Dano verdadeiro: §bIgnora armadura");
		l.add("§8▪ §7Energia por segundo: §b" + Main.classConfig.ancientEPS);
		l.add("§8▪ §7Energia por hit: §b" + Main.classConfig.ancientEPH);
		return l;
	}
	
	public void showNormal(Player p) {
		GamePlayer gp = GamePlayer.get(p);
		MenuUtils classes = new MenuUtils(Main.get(), p, MessageUtil.color("&7Classes - Normais"), 6);
		
		//Above
		ItemStack herobrine = builder(new DefaultSkins().getType(ClassType.HEROBRINE), "§7Herobrine", herobrine(gp), 1);
		classes.setItem(10, has(p, "Herobrine") ? herobrine : no("§cHerobrine", 0, herobrine(gp)));
		
		ItemStack creeper = builder(new DefaultSkins().getType(ClassType.CREEPER), "§7Creeper", creeper(gp), 1);
		classes.setItem(11, has(p, "Creeper") ? creeper : no("§cCreeper", 0, creeper(gp)));
		
		ItemStack enderman = builder(new DefaultSkins().getType(ClassType.ENDERMAN), "§7Enderman", enderman(gp), 1);
		classes.setItem(12, has(p, "Enderman") ? enderman : no("§cEnderman", 0, enderman(gp)));
		
		ItemStack cryomancer = builder(new DefaultSkins().getType(ClassType.CRYOMANCER), "§7Cryomancer", cryomancer(gp), 1);
		classes.setItem(13, has(p, "Cryomancer") ? cryomancer : no("§cCryomancer", 0, cryomancer(gp)));
		
		ItemStack spider = builder(new DefaultSkins().getType(ClassType.SPIDER), "§7Spider", spider(gp), 1);
		classes.setItem(14, has(p, "Spider") ? spider : no("§cSpider", 0, spider(gp)));
		
		ItemStack incendiary = builder(new DefaultSkins().getType(ClassType.INFECTED), "§7Infected", incendiary(gp), 1);
		classes.setItem(15, has(p, "Infected") ? incendiary : no("§cInfected", 0, incendiary(gp)));
		
		ItemStack pyromancer = builder(new DefaultSkins().getType(ClassType.PYROMANCER), "§7Pyromancer", pyromancer(gp), 1);
		classes.setItem(16, has(p, "Pyromancer") ? pyromancer : no("§cPyromancer", 0, pyromancer(gp)));
		
		ItemStack falcon = builder(new DefaultSkins().getType(ClassType.FALCON), "§7Falcon", falcon(gp), 1);
		classes.setItem(19, has(p, "Falcon") ? falcon : no("§cFalcon", 0, falcon(gp)));
		
		ItemStack epsilon = builder(new DefaultSkins().getType(ClassType.EPSILON), "§7Epsilon", epsilon(gp), 1);
		classes.setItem(20, has(p, "Epsilon") ? epsilon : no("§cEpsilon", 0, epsilon(gp)));
		
		ItemStack skeleton = builder(new DefaultSkins().getType(ClassType.SKELETON), "§7Skeleton", skeleton(gp), 1);
		classes.setItem(21, has(p, "Skeleton") ? skeleton : no("§cSkeleton", 0, skeleton(gp)));
		
		ItemStack blaze = builder(new DefaultSkins().getType(ClassType.BLAZE), "§7Blaze", blaze(gp), 1);
		classes.setItem(22, has(p, "Blaze") ? blaze : no("§cBlaze", 0, blaze(gp)));
		
		ItemStack wizard = builder(new DefaultSkins().getType(ClassType.WIZARD), "§7Wizard", wizard(gp), 1);
		classes.setItem(23, has(p, "Wizard") ? wizard : no("§cWizard", 0, wizard(gp)));
		
		ItemStack ancient = builder(new DefaultSkins().getType(ClassType.ANCIENT), "§7Ancient", ancient(gp), 1);
		classes.setItem(24, has(p, "Ancient") ? ancient : no("§cAncient", 0, ancient(gp)));
		
		ItemStack squid = builder(new DefaultSkins().getType(ClassType.SQUID), "§7Squid", squid(gp), 1);
		classes.setItem(25, has(p, "Ancient") ? squid : no("§cSquid", 0, squid(gp)));
		
		classes.setItem(45, ItemFactory.EasyFactory.getLeftArrow());
		
		classes.showMenu(p);
		
	}
	
	public void showHero(Player p) {
		GamePlayer gp = GamePlayer.get(p);
		MenuUtils classes = new MenuUtils(Main.get(), p, MessageUtil.color("&7Classes - Heróis"), 6);
		
		ItemStack dreadlord = builder(new DefaultSkins().getType(ClassType.DREADLORD), "§7Dreadlord", dreadlord(gp), 1);
		classes.setItem(10, has(p, "Dreadlord") ? dreadlord : no("§cDreadlord", 5000, dreadlord(gp)));
		
		ItemStack shaman = builder(new DefaultSkins().getType(ClassType.SHAMAN), "§7Shaman", shaman(gp), 1);
		classes.setItem(11, has(p, "Shaman") ? shaman : no("§cShaman", 5000, shaman(gp)));
		
		ItemStack golem = builder(new DefaultSkins().getType(ClassType.GOLEM), "§7Golem", golem(gp), 1);
		classes.setItem(12, has(p, "Golem") ? golem : no("§cGolem", 5000, golem(gp)));
		
		ItemStack pigman = builder(new DefaultSkins().getType(ClassType.PIGMAN), "§7Pigman", pigman(gp), 1);
		classes.setItem(13, has(p, "Pigman") ? pigman : no("§cPigman", 5000, pigman(gp)));
		
		ItemStack arcanist = builder(new DefaultSkins().getType(ClassType.ARCANIST), "§7Arcanist", arcanist(gp), 1);
		classes.setItem(14, has(p, "Arcanist") ? arcanist : no("§cArcanist", 5000, arcanist(gp)));
		
		ItemStack paladin = builder(new DefaultSkins().getType(ClassType.PALADIN), "§7Paladin", paladin(gp), 1);
		classes.setItem(15, has(p, "Paladin") ? paladin : no("§cPaladin", 5000, paladin(gp)));
		
		ItemStack pirate = builder(new DefaultSkins().getType(ClassType.PIRATA), "§7Pirata", pirate(gp), 1);
		classes.setItem(16, has(p, "Pirata") ? pirate : no("§cPirata", 5000, pirate(gp)));

		ItemStack magma = builder(new DefaultSkins().getType(ClassType.MAGMA), "§7Magma", magma(gp), 1);
		classes.setItem(19, has(p, "Magma") ? magma : no("§cMagma", 5000, magma(gp)));
		
		ItemStack electro = builder(new DefaultSkins().getType(ClassType.ELECTRO), "§7Electro", electro(gp), 1);
		classes.setItem(20, has(p, "Electro") ? electro : no("§cElectro", 5000, electro(gp)));
		
		ItemStack magnatition = builder(new DefaultSkins().getType(ClassType.MAGNATITION), "§7Magnatition", magnatition(gp), 1);
		classes.setItem(21, has(p, "Magnatition") ? magnatition : no("§cMagnatition", 5000, magnatition(gp)));
		
		ItemStack rumbler = builder(new DefaultSkins().getType(ClassType.RUMBLER), "§7Rumbler", rumbler(gp), 1);
		classes.setItem(22, has(p, "Rumbler") ? rumbler : no("§cRumbler", 5000, rumbler(gp)));
		
		ItemStack lana = builder(new DefaultSkins().getType(ClassType.LANA), "§7Lana", lana(gp), 1);
		classes.setItem(23, has(p, "Lana") ? lana : no("§cLana", 5000, lana(gp)));
		
		classes.setItem(45, ItemFactory.EasyFactory.getLeftArrow());
		
		classes.showMenu(p);
		
	}
	
	public void showPrestige(Player p) {
		MenuUtils classes = new MenuUtils(Main.get(), p, MessageUtil.color("&7Classes - Míticas"), 6);
		
		ItemStack crusader = builder(new DefaultSkins().getType(ClassType.CRUSADER), "§7Crusader", crusader(), 1);
		classes.setItem(10, has(p, "Crusader") ? crusader : no("§cCrusader", 10000, crusader()));
		
		ItemStack elenne = builder(new DefaultSkins().getType(ClassType.ELENNE), "§7Elenne", elenne(), 1);
		classes.setItem(11, has(p, "Elenne") ? elenne : no("§cElenne", 10000, elenne()));
		
		ItemStack shadow = builder(new DefaultSkins().getType(ClassType.SHADOW), "§7Shadow", shadow(), 1);
		classes.setItem(12, has(p, "Shadow") ? shadow : no("§cShadow", 10000, shadow()));
		
		ItemStack fighter = builder(new DefaultSkins().getType(ClassType.FIGHTER), "§7Fighter", fighter(), 1);
		classes.setItem(13, has(p, "Fighter") ? fighter : no("§cFighter", 10000, fighter()));
		
		ItemStack hunter = builder(new DefaultSkins().getType(ClassType.HUNTER), "§7Hunter", hunter(), 1);
		classes.setItem(14, has(p, "Hunter") ? hunter : no("§cHunter", 10000, hunter()));

		ItemStack automaton = builder(new DefaultSkins().getType(ClassType.AUTOMATON), "§7Automaton", automaton(), 1);
		classes.setItem(15, has(p, "Automaton") ? automaton : no("§cAutomaton", 50000, automaton()));
		
		classes.setItem(45, ItemFactory.EasyFactory.getLeftArrow());
		
		classes.showMenu(p);
		
	}
	
	public void show(Player p) {
		GamePlayer gp = GamePlayer.get(p);
		MenuUtils classes = new MenuUtils(Main.get(), p, "§7Classes", 4);
		ItemStack normal = ItemFactory.create(Material.IRON_SWORD, "§7Classes - §aNormais",
				Arrays.asList(
						"§7Veja as classes normais que você tem",
						"§7ou pode comprar."));

		classes.setItem(11, normal);
		
		ItemStack heros = ItemFactory.create(Material.DIAMOND_SWORD, "§7Classes - §cHeróis",
				Arrays.asList(
						"§7Veja as classes heróis que você tem",
						"§7ou pode comprar. Essas classes",
						"§7são mais poderosas que as",
						"§7normais.",
						"§7",
						"§7Classes §cHeróis §7requerem nível 30."));

		classes.setItem(15, heros);
		
		ItemStack prestiges = ItemFactory.create(Material.NETHER_STAR, "§7Classes - §bMíticas",
				Arrays.asList(
						"§7Veja as classes míticas que você tem",
						"§7ou pode comprar. Essas classes são",
						"§7muito poderosas e oferecem",
						"§7utilidades.",
						"§7",
						"§7Classes §bMíticas §7requerem §cPrestige§7."));

		classes.setItem(13, prestiges);
		
		classes.setItem(30, ItemFactory.EasyFactory.getXArrow());

		classes.setItem(31, ItemFactory.create(Material.EMERALD, MessageUtil.color("&7Coins: &6" + gp.getCoins()),
				Arrays.asList(
						"§7",
						"§7Classe selecionada: §6" + gp.getSelectedClass().getName(),
						"§7Mystery Boxes: §6" + gp.getMysteryBoxes())));


		classes.setItem(32, ItemFactory.create(Material.STAINED_GLASS_PANE, MessageUtil.color("&cResetar"), (byte)14));
		
		classes.showMenu(p);
	}
	
	@EventHandler
	public void handleSelector(InventoryClickEvent e) {
		Player p = (Player)e.getWhoClicked();
		GamePlayer gp = GamePlayer.get(p);
		int slot = e.getRawSlot();
		ClassType type = null;
		int price = 0;
		if(e.getInventory().getName().equalsIgnoreCase(MessageUtil.color("&7Classes - Normais"))) {
			e.setCancelled(true);
			e.setResult(Result.DENY);
			if (slot == 10) {
				type = ClassType.HEROBRINE;
			}else if (slot == 11) {
				type = ClassType.CREEPER;
			}else if (slot == 12) {
				type = ClassType.ENDERMAN;
			}else if (slot == 13) {
				type = ClassType.CRYOMANCER;
			}else if (slot == 14) {
				type = ClassType.SPIDER;
			}else if (slot == 15) {
				type = ClassType.INFECTED;
			}else if (slot == 16) {
				type = ClassType.PYROMANCER;
			}else if (slot == 19) {
				type = ClassType.FALCON;
			}else if (slot == 20) {
				type = ClassType.EPSILON;
			}else if (slot == 21) {
				type = ClassType.SKELETON;
			}else if (slot == 22) {
				type = ClassType.BLAZE;
			}else if (slot == 23) {
				type = ClassType.WIZARD;
			}else if (slot == 24) {
				type = ClassType.ANCIENT;
			}else if (slot == 25) {
				type = ClassType.SQUID;
			}else if(slot == 45) {
				show(p);
				return;
			}
			
			price = Main.getGame().gameClasses.get(type).getCost();
			
			if(type != null && !gp.getKits().contains(type.toString().toUpperCase())) {
				buy(gp, type, price);
				showNormal(p);
			}else{
				gp.setClass(gp.getGame().gameClasses.get(type));
				gp.setClassType(gp.getGame().gameClasses.get(type).getType().toString());
				gp.getSelectedClass().giveItems(gp.getPlayer());
				p.closeInventory();
				p.updateInventory();
			}
		}else if(e.getInventory().getName().equalsIgnoreCase(MessageUtil.color("&7Classes - Heróis"))) {
			e.setCancelled(true);
			e.setResult(Result.DENY);
			if(gp.getPrestigeLevel() < 1) {
				if(gp.getLevel() < 30) {
					p.sendMessage(MessageUtil.color("&eVocê precisa de pelo menos nível &c&l30 &epara jogar com classes Hero."));
					p.closeInventory();
					p.updateInventory();
					return;
				}
			}

			if (slot == 10) {
				type = ClassType.DREADLORD;
			}else if (slot == 11) {
				type = ClassType.SHAMAN;
			}else if (slot == 12) {
				type = ClassType.GOLEM;
			}else if (slot == 13) {
				type = ClassType.PIGMAN;
			}else if (slot == 14) {
				type = ClassType.ARCANIST;
			}else if (slot == 15) {
				type = ClassType.PALADIN;
			}else if (slot == 16) {
				type = ClassType.PIRATA;
			}else if (slot == 19) {
				type = ClassType.MAGMA;
			}else if (slot == 20) {
				type = ClassType.ELECTRO;
			}else if (slot == 21) {
				type = ClassType.MAGNATITION;
			}else if (slot == 22) {
				type = ClassType.RUMBLER;
			}else if (slot == 23) {
				type = ClassType.LANA;
			}else if(slot == 45) {
				show(p);
				return;
			}
			
			price = Main.getGame().gameClasses.get(type).getCost();
			if(!gp.getKits().contains(type.toString().toUpperCase())) {
				buy(gp, type, price);
				showHero(p);
			}else{
				gp.setClass(gp.getGame().gameClasses.get(type));
				gp.setClassType(gp.getGame().gameClasses.get(type).getType().toString());
				gp.getSelectedClass().giveItems(gp.getPlayer());
				p.closeInventory();
				p.updateInventory();
			}
		}else if(e.getInventory().getName().equalsIgnoreCase(MessageUtil.color("&7Classes - Míticas"))) {
			e.setCancelled(true);
			e.setResult(Result.DENY);
			if (slot == 10) {
				type = ClassType.CRUSADER;
			}else if (slot == 11) {
				type = ClassType.ELENNE;
			}else if (slot == 12) {
				type = ClassType.SHADOW;
			}else if (slot == 13) {
				type = ClassType.FIGHTER;
			}else if (slot == 14) {
				type = ClassType.HUNTER;
			}else if (slot == 15) {
				type = ClassType.AUTOMATON;
			}else if(slot == 45) {
				show(p);
				return;
			}
			
			price = Main.getGame().gameClasses.get(type).getCost();
			if(!gp.getKits().contains(type.toString().toUpperCase())) {
				buy(gp, type, price);
				showPrestige(p);
			}else{
				gp.setClass(gp.getGame().gameClasses.get(type));
				gp.setClassType(gp.getGame().gameClasses.get(type).getType().toString());
				gp.getSelectedClass().giveItems(gp.getPlayer());
				p.closeInventory();
				p.updateInventory();
			}
				
		}else if(e.getInventory().getName().equalsIgnoreCase(MessageUtil.color("&7Classes"))) { 
			e.setCancelled(true);
			e.setResult(Result.DENY);
			if(slot == 11) {
				showNormal(p);
			}else if(slot == 15) {
				showHero(p);
			}else if(slot == 13) {
				if(gp.getPrestigeLevel() >= 1) {  
					showPrestige(p);
				}else {
					gp.getPlayer().sendMessage(MessageUtil.color("&9Você precisa de pelo menos Prestige I para acessar esse menu."));
				}
			}else if(slot == 30) {
				p.closeInventory();
			}else if(slot == 32) {
				InventoryUtils.nullInventory(p);
				GamePlayer.get(p).setEnergy(0);
				GamePlayer.get(p).giveSpawnItems();
			}
		}
	}
	
	private boolean addIf(GamePlayer gp, ClassType type) {
		if(!gp.getKits().contains(type.toString())) {
			//gp.getKitLevel().add(new FormattedKitLevel(WordUtils.capitalize(type.toString()), "1"));
			gp.getKits().add(type.toString().toUpperCase());
			gp.setClass(gp.getGame().gameClasses.get(type));
			gp.setClassType(gp.getGame().gameClasses.get(type).getType().toString());
			gp.getSelectedClass().giveItems(gp.getPlayer());
			gp.getPlayer().closeInventory();
			gp.getPlayer().updateInventory();
			return true;
		}
		return false;
	}
	
	private void buy(GamePlayer gp, ClassType type, int price) {
		if(gp.getCoins() >= price) {
			//gp.getKitLevel().add(new FormattedKitLevel(WordUtils.capitalize(type.toString()), "1"));
			gp.getKits().add(type.toString().toUpperCase());
			gp.setCoins(gp.getCoins()-price);
			gp.getPlayer().playSound(gp.getPlayer().getLocation(), Sound.NOTE_PLING, 1, 2);
			gp.getPlayer().sendMessage(MessageUtil.color("&aVocê comprou a classe &6" + WordUtils.capitalize(type.toString()).replaceAll("_", " ") + "&a."));
		}else{
			gp.getPlayer().sendMessage(MessageUtil.color("&cVocê precisa de mais &6" + (price-gp.getCoins()) + " &ccoins para comprar essa classe."));
		}
	}
	
	public boolean has(Player p, String kit) {
		return GamePlayer.get(p).getKits().contains(kit.toUpperCase());
	}

	private ItemStack no(String name, int price, List<String> kitAbilities) {
		List<String> lore = new ArrayList<>();
		lore.addAll(kitAbilities);
		lore.add(" ");
		lore.add("§e§l>> §bClique para comprar por §e§l" + price);
		return ItemFactory.create(Material.STAINED_GLASS_PANE, name, lore, (byte) 14);
	}
	
	private ItemStack builder(GameProfile skin, String nome, List<String> lore, int quantidade) {
		return ItemFactory.createSkull(skin, quantidade, nome, lore);
	}
}
