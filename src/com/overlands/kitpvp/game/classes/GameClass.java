package com.overlands.kitpvp.game.classes;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import com.overlands.kitpvp.Main;

public abstract class GameClass implements Listener {
	
	private String name;
	private int cost;
	private int eps;
	private int eph;
	private int epgh;
	private ClassType type;

	private int prestigeRequired;
	private int levelRequired;

	public GameClass(ClassType type, String name, int cost, int eph, int eps) {
		this.type = type;
		this.name = name;
		this.setEPH(eph);
		this.setEPS(eps);
		this.cost = cost;
		Bukkit.getPluginManager().registerEvents(this, Main.get());;
	}
	
	public GameClass(ClassType type, String name, int cost, int eph, int eps, int epgh) {
		this.type = type;
		this.name = name;
		this.setEPH(eph);
		this.setEPS(eps);
		this.cost = cost;
		Bukkit.getPluginManager().registerEvents(this, Main.get());;
	}
	
	public GameClass(ClassType type, String name, int cost, int eph, int eps, int level, int prestige) {
		this.type = type;
		this.name = name;
		this.setEPH(eph);
		this.setEPS(eps);
		this.cost = cost;
		this.setPrestigeRequired(prestige);
		this.setLevelRequired(level);
		Bukkit.getPluginManager().registerEvents(this, Main.get());;
	}

	public abstract boolean onInteract(Player player);
	
	public void giveItems(Player player) {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getEPS() {
		return eps;
	}

	public void setEPS(int eps) {
		this.eps = eps;
	}

	public int getEPH() {
		return eph;
	}

	public void setEPH(int eph) {
		this.eph = eph;
	}
	
	public ClassType getType() {
		return type;
	}

	public int getPrestigeRequired() {
		return prestigeRequired;
	}

	public void setPrestigeRequired(int prestigeRequired) {
		this.prestigeRequired = prestigeRequired;
	}

	public int getLevelRequired() {
		return levelRequired;
	}

	public void setLevelRequired(int levelRequired) {
		this.levelRequired = levelRequired;
	}

	public int getEPGH() {
		return epgh;
	}

	public void setEPGH(int epgh) {
		this.epgh = epgh;
	}
}
