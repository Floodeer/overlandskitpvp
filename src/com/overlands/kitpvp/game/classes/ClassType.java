package com.overlands.kitpvp.game.classes;

import com.overlands.kitpvp.game.classes.hero.Arcanist;
import com.overlands.kitpvp.game.classes.hero.Dreadlord;
import com.overlands.kitpvp.game.classes.hero.Electro;
import com.overlands.kitpvp.game.classes.hero.Golem;
import com.overlands.kitpvp.game.classes.hero.Lana;
import com.overlands.kitpvp.game.classes.hero.Magma;
import com.overlands.kitpvp.game.classes.hero.Magnatition;
import com.overlands.kitpvp.game.classes.hero.Paladin;
import com.overlands.kitpvp.game.classes.hero.Pigman;
import com.overlands.kitpvp.game.classes.hero.Pirate;
import com.overlands.kitpvp.game.classes.hero.Rumbler;
import com.overlands.kitpvp.game.classes.hero.Shaman;
import com.overlands.kitpvp.game.classes.normal.*;
import com.overlands.kitpvp.game.classes.prestige.*;

public enum ClassType {

	DREADLORD("Dreadlord", "Wither Blast", Dreadlord.class),
	EPSILON("Epsilon", "Static Blast", Epsilon.class),
	SKELETON("Skeleton", "Explosive Arrow", Skeleton.class),
	SPIDER("Spider", "Drop Shock", Spider.class),
	HEROBRINE("Herobrine", "Lightstrike", Herobrine.class), 
	SQUID("Squid", "Splash", Squid.class),
	HUNTER("Hunter", "Eagle Eye", Hunter.class),
	ZOMBIE("Zombie"),
	SHADOW("Shadow", "Shadow Step", Shadow.class),
	FIGHTER("Fighter", "Super Punch", Fighter.class),
	ELENNE("Elenne", "Holy Missile", Elenne.class),
	CRYOMANCER("Cryomancer", "Freezing Breath", Cryomancer.class), 
	PYROMANCER("Pyromancer", "Flame Breath", Pyromancer.class),
	ANCIENT("Ancient", "Ancient Breath", Ancient.class),
	ARCANIST("Arcanist", "Beam", Arcanist.class),
	GOLEM("Golem", "Iron Punch", Golem.class),
	PIRATA("Pirata", "Fire Cannon", Pirate.class),
	CREEPER("Creeper", "Detonate", Creeper.class),
	BLAZE("Blaze", "Immolating Burst", Blaze.class),
	WIZARD("Wizard", "Dust Spell", Wizard.class),
	PIGMAN("Pigman", "Burning Soul", Pigman.class),	
	CRUSADER("Crusader", "Defender", Crusader.class),
	SHAMAN("Shaman", "Tornado", Shaman.class),
	MAGNATITION("Magnatition", "Magnetic Pulse", Magnatition.class),
	ENDERMAN("Enderman", "Teleport", Enderman.class),
	RUMBLER("Rumbler", "Rumble", Rumbler.class),
	ELECTRO("Electro", "Electronic Pulse", Electro.class),
	PALADIN("Paladin", "Meteor Strike", Paladin.class),
	INFECTED("Infected", "DoomShroom", Infected.class),
	MAGMA("Magma", "Flame Dash", Magma.class),
	FALCON("Falcon", "Falcon Punch", Falcon.class),
	LANA("Lana", "Pistol", Lana.class),
	AUTOMATON("Automaton", "EMP", Automaton.class),
	TEST("Test", "Test", TestClass.class);

	String classeName;
	String skillName;
	Class<? extends GameClass> clazz;
	
	ClassType(String classeName) {
		this.classeName = classeName;
	}
	
	ClassType(String classeName, String skillName) {
		this.classeName = classeName;
		this.skillName = skillName;
	}
	
	ClassType(String classeName, String skillName, Class<? extends GameClass> clazz) {
		this.classeName = classeName;
		this.skillName = skillName;
		this.clazz = clazz;
	}
	
	public Class<? extends GameClass> getClazz() {
		return clazz;
	}
	
	public String getSkillName() {
		return skillName;
	}

	@Override
	public String toString() {
		return classeName;
	}
	

	public static ClassType fromName(String clazz) {
		for(ClassType c : ClassType.values()){
			if(c.toString().equalsIgnoreCase(clazz)) {
				return c;
			}
		}
		return null;
	}
}
