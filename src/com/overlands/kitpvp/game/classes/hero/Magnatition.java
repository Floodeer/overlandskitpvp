package com.overlands.kitpvp.game.classes.hero;

import com.overlands.kitpvp.game.GamePlayer;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.overlands.core.utils.Imobilizer;
import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Runner;
import com.overlands.core.utils.Sounds;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.effects.ClassEffectType;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;

import de.slikey.effectlib.util.ParticleEffect;

public class Magnatition extends GameClass {

	public Magnatition() {
		super(ClassType.MAGNATITION, "Magnatition", Main.classConfig.magnatitionPrice, Main.classConfig.magnatitionEPH, Main.classConfig.magnatitionEPS);
	}

	@Override
	public boolean onInteract(Player player) {
		final Location l = player.getLocation();
		startPuch(player);
		Util.filterNearby(player, l, 10).forEach(cur -> {
			ClassDamage.handle(player, cur, getType());
			Imobilizer.ImobilizePlayer(cur, 30);
			cur.teleport(l.clone().add(0, 0.5, 0));
		});
		Runner.make(Main.get()).delay(30).run(() -> {
			new BukkitRunnable() {
				double t = Math.PI / 4;
				public void run() {
					t = t + 0.1 * Math.PI;
					for (double theta = 0; theta <= 2 * Math.PI; theta = theta + Math.PI / 32) {
						double x = t * Math.cos(theta);
						double y = 2 * Math.exp(-0.1 * t) * Math.sin(t) + 1.5;
						double z = t * Math.sin(theta);
						l.add(x, y, z);
						if(ClassEffectType.FLAME_IMPULSE.hasSelected(player)) {
							ParticleEffect.FIREWORKS_SPARK.display(0.0F, 0.0F, 0.0F, 0.0F, 1, l, 126);
						}else{
							ParticleEffect.SPELL_WITCH.display(0.0F, 0.0F, 0.0F, 0, 1, l, 126);
						}

						l.subtract(x, y, z);

						theta = theta + Math.PI / 64;

						x = t * Math.cos(theta);
						y = 2 * Math.exp(-0.1 * t) * Math.sin(t) + 1.5;
						z = t * Math.sin(theta);
						l.add(x, y, z);
						if(ClassEffectType.FLAME_IMPULSE.hasSelected(player)) {
							ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, l, 126);
						}else{
							ParticleEffect.FIREWORKS_SPARK.display(0.0F, 0.0F, 0.0F, 0.0F, 1, l, 126);
						}

						l.subtract(x, y, z);
					}
					if (t > 20 || !player.isOnline() || GamePlayer.get(player).isEMPd()) {
						this.cancel();
					}
				}
			}.runTaskTimer(Main.get(), 0, 1);
		});
		
		return true;
	}
	
	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()),  ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.IRON_SWORD, MessageUtil.color("&cMagnetic Pulse &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);
		
		player.getInventory().setItem(0, sword);
		
		player.getInventory().setHelmet(ItemFactory.anUnbreakable(ItemFactory.create(Material.DIAMOND_HELMET, Enchantment.PROTECTION_ENVIRONMENTAL, 1)));
		player.getInventory().setChestplate(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_CHESTPLATE)));
		player.getInventory().setLeggings(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_LEGGINGS)));
		player.getInventory().setBoots(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_BOOTS)));
	}
	
	public void startPuch(Player paramPlayer) {
		Location localLocation = paramPlayer.getLocation().clone().add(0.0D, 1.0D, 0.0D);
		paramPlayer.getWorld().playSound(localLocation, Sounds.ENDERMAN_TELEPORT.bukkitSound(), 2.0F, 2.0F);
		paramPlayer.getWorld().playSound(localLocation, Sounds.IRONGOLEM_DEATH.bukkitSound(), 2.0F, 2.0F);
		for (Location loc : LocationUtils.getCircle(LocationUtils.getLocation(localLocation, 0.0D, 1.0D, 0.0D), 2.5D, 25)) {
			Vector v = localLocation.toVector().subtract(loc.toVector()).normalize();
			ParticleEffect.FIREWORKS_SPARK.display(v, 0.6F, loc, 78.0000000039972);
		}
		for (Location loc : LocationUtils.getCircle(localLocation, 3.0D, 25)) {
			Vector v = localLocation.toVector().subtract(loc.toVector()).normalize();
			ParticleEffect.FIREWORKS_SPARK.display(v, 0.6F, loc, 78.0000000039972);
		}
		for (Location loc : LocationUtils.getCircle(LocationUtils.getLocation(localLocation, 0.0D, -1.0D, 0.0D), 2.5D, 25)) {
			Vector v = localLocation.toVector().subtract(loc.toVector()).normalize();
			ParticleEffect.FIREWORKS_SPARK.display(v, 0.6F, loc, 78.0000000039972);
		}
		for (Location loc : LocationUtils.getCircle(localLocation, 3.0D, 80)) {
			Vector v = localLocation.toVector().subtract(loc.toVector()).normalize();
			ParticleEffect.FIREWORKS_SPARK.display(v, 0.8F, loc, 78.0000000039972);
		}
		for (Location loc : LocationUtils.getCircle(localLocation, 3.0D, 80)) {
			Vector v = localLocation.toVector().subtract(loc.toVector()).normalize();
			ParticleEffect.FIREWORKS_SPARK.display(v, 0.6F, loc, 78.0000000039972);
		}
		for (Location loc : LocationUtils.getCircle(localLocation, 3.0D, 80)) {
			Vector v = localLocation.toVector().subtract(loc.toVector()).normalize();
			ParticleEffect.FIREWORKS_SPARK.display(v, 0.4F, localLocation, 78.0000000039972);
		}
	}
}
