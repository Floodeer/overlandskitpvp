package com.overlands.kitpvp.game.classes.hero;

import java.util.Map;

import com.overlands.kitpvp.Main;
import org.bukkit.Color;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.overlands.core.utils.FireworkUtils;
import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.block.BlockUtils;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;

import de.slikey.effectlib.util.ParticleEffect;

public class Arcanist extends GameClass {

	public Arcanist() {
		super(ClassType.ARCANIST, "Arcanist", Main.classConfig.arcanistPrice, Main.classConfig.arcanistEPH, Main.classConfig.arcanistEPS);
	}

	@Override
	public boolean onInteract(Player player) {
		
		double curRange = 0;
		while (curRange <= 20 + 10 * 8) {
			Location newTarget = player.getEyeLocation().add(player.getLocation().getDirection().multiply(curRange));

			Map<LivingEntity, Double> hits = LocationUtils.getLivingInRadius(newTarget, 2);
			hits.remove(player);
			if (!hits.isEmpty())
				break;
			if (!BlockUtils.airFoliage(newTarget.getBlock()))
				break;
				
			curRange += 0.2;
			ParticleEffect.FIREWORKS_SPARK.display(0, 0, 0, 0, 1, newTarget, 120);
		}

		Location target = player.getLocation().add(player.getLocation().getDirection().multiply(curRange));
		ParticleEffect.EXPLOSION_LARGE.display(0, 0, 0, 0, 1, target, 120);
		FireworkUtils.playFirework(player.getLocation().add(player.getLocation().getDirection().multiply(Math.max(0, curRange - 0.6))), Type.BURST, Color.WHITE, false, false);
		
		Map<LivingEntity, Double> hit = LocationUtils.getLivingInRadius(target.subtract(0, 1, 0), 4);
		
		hit.keySet().stream()
		            .map(entity -> (Player)entity)
		            .filter(players -> !players.equals(player))
		            .forEach(cur -> ClassDamage.handle(player, cur, getType()));
		
		player.getEyeLocation().getWorld().playSound(player.getEyeLocation(), Sound.FIREWORK_LAUNCH, 3.5F, 0.0F);
		player.getEyeLocation().getWorld().playSound(player.getEyeLocation(), Sound.GHAST_FIREBALL, 3.5F, 2.0F);
		
		return true;
	}

	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()), ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.DIAMOND_SWORD, MessageUtil.color("&cBeam &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);

		player.getInventory().setItem(0, sword);

		player.getInventory().setHelmet(ItemFactory.createUnbreakable(Material.IRON_HELMET));
		player.getInventory().setChestplate(ItemFactory.createUnbreakable(Material.IRON_CHESTPLATE));
		player.getInventory().setLeggings(ItemFactory.createUnbreakable(Material.DIAMOND_LEGGINGS, Enchantment.PROTECTION_ENVIRONMENTAL, 3));
		player.getInventory().setBoots(ItemFactory.createUnbreakable(Material.IRON_BOOTS));
	}
}
