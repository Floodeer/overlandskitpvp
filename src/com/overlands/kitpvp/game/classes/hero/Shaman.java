package com.overlands.kitpvp.game.classes.hero;

import java.util.HashMap;
import java.util.Map;

import com.overlands.kitpvp.game.GamePlayer;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Sounds;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.calc.VelocityUtils;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.particles.UtilParticle;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;
import com.overlands.kitpvp.sound.SoundManager;

public class Shaman extends GameClass {

	private final Map<Player, Location> tornados = new HashMap<>();
	
	public Shaman() {
		super(ClassType.SHAMAN, "Shaman", Main.classConfig.shamanPrice, Main.classConfig.shamanEPH, Main.classConfig.shamanEPS);
	}

	@Override
	public boolean onInteract(Player player) {
		final Location local2 = player.getLocation().getBlock().getLocation();
		local2.add(0.5D, 0.0D, 0.5D);
		tornados.put(player, player.getLocation());
		
		new BukkitRunnable() {
			final float LineNumber = 6.0F;
			float j = 45.0F;
			final float radius = 0.4F;
			final float heightEcart = 0.02F;
			int steps = 140;
			@Override
			public void run() {
				Location local = tornados.get(player);
				Location localLocation = local.clone();
				if (!player.isOnline() || GamePlayer.get(player).isEMPd()) {
					cancel();
				}
				--steps;
				if(steps <= 0) {
					cancel();
					return;
				}
				localLocation.setY(localLocation.getY() - 0.5);
				for (int i = 0; i < this.LineNumber; i++) {
					localLocation.add(
							Math.cos(this.j) * this.radius, this.j * this.heightEcart,
							Math.sin(this.j) * this.radius);

					new UtilParticle(UtilParticle.ParticleType.FIREWORKS_SPARK, 0.0D, 1, 0.0D).sendToLocation(localLocation);
				}
				this.j += 0.2F;
				if (this.j > 70.0F) {
					this.j = 45.0F;
				}
			}
		}.runTaskTimer(Main.get(), 0, 1);

		new BukkitRunnable() {
			int step = 0;
			@Override
			public void run() {
				++step;
				if(step >= 160/20  || !player.isOnline() || GamePlayer.get(player).isEMPd()) {
					cancel();
					return;
				}
				if (player.isOnline()) {
					    Util.filterNearby(player, local2, 5).forEach(p -> {
			            ClassDamage.handle(player, p, getType());
					    VelocityUtils.knockback(p, local2, 0.7D, 0.2D, true);
					    Util.reAddEffect(p, PotionEffectType.SLOW, 30, 1);
					});
				}
			}
		}.runTaskTimer(Main.get(), 0L, 20L);
		
		local2.getWorld().playSound(local2, Sounds.ENDERDRAGON_WINGS.bukkitSound(), 2.0F, 0.0F);
		SoundManager.SHAMAN.playForAll(player.getLocation(), 1.0F, 1.0F);
		return true;
	}

	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()), ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.DIAMOND_SWORD, MessageUtil.color("&cTornado &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);

		player.getInventory().setItem(0, sword);

		player.getInventory().setHelmet(ItemFactory.createUnbreakable(Material.IRON_HELMET));
		player.getInventory().setChestplate(ItemFactory.createUnbreakable(Material.IRON_CHESTPLATE));
		player.getInventory().setLeggings(ItemFactory.createUnbreakable(Material.IRON_LEGGINGS));
		player.getInventory().setBoots(ItemFactory.createUnbreakable(Material.DIAMOND_BOOTS, Enchantment.PROTECTION_ENVIRONMENTAL, 2));
	}
}
