package com.overlands.kitpvp.game.classes.hero;

import java.util.ArrayList;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import com.overlands.core.utils.ActionBar;
import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.block.BlockUtils;
import com.overlands.core.utils.block.ShapeUtils;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.effects.ClassEffectType;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;

import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.PacketPlayOutWorldEvent;

public class Rumbler extends GameClass {

	private final BlockFace[] radial = { BlockFace.SOUTH, BlockFace.SOUTH_WEST, BlockFace.WEST, BlockFace.NORTH_WEST,
			BlockFace.NORTH, BlockFace.NORTH_EAST, BlockFace.EAST, BlockFace.SOUTH_EAST };

	public Rumbler() {
		super(ClassType.RUMBLER, "Rumbler", Main.classConfig.rumblerPrice, Main.classConfig.rumblerEPH, Main.classConfig.rumblerEPS);
	}

	@Override
	public boolean onInteract(Player p) {
		Block target = LocationUtils.getTargetBlock(p, 25).getBlock();
		if (target == null) {
			ActionBar.sendActionBar(p, MessageUtil.color("&cMire em algum bloco para ativar a habilidade!"));
			return false;
		}
		if (!BlockUtils.solid(target)) {
			ActionBar.sendActionBar(p, MessageUtil.color("&cMire em algum bloco para ativar a habilidade!"));
			return false;
		}
		cast(p, target);
		return true;
	}

	public void cast(final Player player, final Block target) {
		final BlockFace moveDirection = radial[Math.round(player.getEyeLocation().getYaw() / 45f) & 0x7];
		final int maxDist = 20;

		playBlockEffect(target, player);

		new BukkitRunnable() {
			private Block currentBlock = target;
			private int distTravelled = 0;
			private final ArrayList<Integer> effected = new ArrayList<Integer>();
			private ArrayList<Block> previousBlocks = new ArrayList<Block>();

			private void endRun() {
				ArrayList<Block> bs = new ArrayList<Block>();

				BlockFace[] faces = ShapeUtils.getSideBlockFaces(moveDirection);

				bs.add(currentBlock);

				for (int i = 1; i <= Math.min(4, Math.floor(distTravelled / (8D - 4))) + 1; i++) {
                    for (BlockFace face : faces) {
                        Block b = currentBlock.getRelative(face, i);

                        if (BlockUtils.solid(b)) {
                            bs.add(b);
                        }
                    }
				}

				for (Block block : bs) {

					ArrayList<Block> toExplode = new ArrayList<Block>();

					toExplode.add(block);

					for (BlockFace face : new BlockFace[] { BlockFace.EAST, BlockFace.WEST, BlockFace.SOUTH,
							BlockFace.NORTH, BlockFace.UP, BlockFace.DOWN }) {
						if (MathUtils.random.nextBoolean()) {
							Block b = block.getRelative(face);

							if (b.getType() != Material.AIR && b.getType() != Material.BEDROCK) {
								if (!toExplode.contains(b)) {
									toExplode.add(b);
									b.getWorld().playEffect(b.getLocation(), Effect.STEP_SOUND, b.getTypeId());
								}
							}
						}
					}

					for (Player entity : block.getWorld().getPlayers()) {
						double dist = 999;

						for (Block b : toExplode) {
							double currentDist = b.getLocation().add(0.5, 0.5, 0.5).distance(entity.getLocation());

							if (dist > currentDist) {
								dist = currentDist;
							}
						}

						if (dist < 2) {
							if (Util.canBeDamaged(player, entity)) {
								ClassDamage.handle(player, entity, getType());
							}
						}
					}
				}

				cancel();
			}

			@Override
			public void run() {
				if (!player.isOnline() || GamePlayer.get(player).isEMPd()) {
					endRun();
					return;
				}

				boolean found = false;

				for (int y : new int[] { 0, 1, -1, -2 }) {
					if (currentBlock.getY() + y <= 0) {
						continue;
					}

					Block b = currentBlock.getRelative(moveDirection).getRelative(0, y, 0);

					if (BlockUtils.solid(b) && !BlockUtils.solid(b.getRelative(0, 1, 0))) {
						found = true;
						currentBlock = b;

						break;
					}
				}

				if (!found) {
					endRun();
					return;
				}

				ArrayList<Block> effectedBlocks = new ArrayList<Block>();

				BlockFace[] faces = ShapeUtils.getSideBlockFaces(moveDirection);

				effectedBlocks.add(currentBlock);

				playBlockEffect(currentBlock, player);

				int size = (int) (Math.min(4, Math.floor(distTravelled / (8D - 4))) + 1);

				for (int i = 1; i <= size; i++) {
					for (int a = 0; a < faces.length; a++) {
						Block b = currentBlock.getRelative(faces[a], i);

						if (BlockUtils.solid(b)) {
							effectedBlocks.add(b);
							playBlockEffect(b, player);
						}
					}
				}

				for (Block b : ShapeUtils.getDiagonalBlocks(currentBlock, moveDirection, size - 2)) {
					if (BlockUtils.solid(b)) {
						effectedBlocks.add(b);
						playBlockEffect(b, player);
					}
				}

				previousBlocks.addAll(effectedBlocks);

				for (Block b : previousBlocks) {
					for (Entity entity : b.getChunk().getEntities()) {
						if (entity instanceof Player && player != entity && !effected.contains(entity.getEntityId())) {
							Location loc = entity.getLocation();
							if (loc.getBlockX() == b.getX() && loc.getBlockZ() == b.getZ()) {
								double height = loc.getY() - b.getY();
								if (height >= 0 && height <= 3) {
									if (Util.canBeDamaged(player, (Player) entity)) {
										ClassDamage.handle(player, (Player) entity, getType());
									}
								}

								effected.add(entity.getEntityId());
							}
						}
					}
				}

				previousBlocks = effectedBlocks;

				if (distTravelled++ >= maxDist) {
					endRun();
				}
			}
		}.runTaskTimer(Main.get(), 5, 1);
	}

	@SuppressWarnings("deprecation")
	private void playBlockEffect(Block block, Player user) {
		BlockPosition pos = new BlockPosition(block.getLocation().getX(), block.getLocation().getY(), block.getLocation().getZ());
		
		PacketPlayOutWorldEvent packet = new PacketPlayOutWorldEvent(2001, pos, block.getTypeId(), false);
		LocationUtils.getNearbyPlayers(block.getLocation(), 256).stream().map(GamePlayer::get).forEach(gp -> {
			if(!ClassEffectType.BLOCK_RUMBLER.hasSelected(user)) {
				if(gp.getParticleQuality() == 3) {
					Util.sendPacket(gp.getPlayer(), packet);
				}else{
					ParticleEffect.FLAME.display(0,0,0, 0, 2, block.getLocation(), 256);
				}
			}else{
				BlockUtils.setToRestore(block, Material.STAINED_CLAY, (byte)MathUtils.random(1, 15), 80);
			}
		});
	}

	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()), ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.IRON_SWORD, MessageUtil.color("&cRumble &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);

		player.getInventory().setItem(0, sword);

		player.getInventory().setHelmet(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_HELMET)));
		player.getInventory().setChestplate(ItemFactory.anUnbreakable(ItemFactory.create(Material.DIAMOND_CHESTPLATE, Enchantment.PROTECTION_ENVIRONMENTAL, 2)));
		player.getInventory().setLeggings(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_LEGGINGS)));
		player.getInventory().setBoots(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_BOOTS)));
	}
}
