package com.overlands.kitpvp.game.classes.hero;

import java.util.Map;

import com.overlands.kitpvp.Main;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Maps;
import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;

public class Lana extends GameClass {

	private Map<Player, Integer> shots = Maps.newHashMap();
	
	public Lana() {	
		super(ClassType.LANA, "Lana", Main.classConfig.lanaPrice, Main.classConfig.lanaEPH, Main.classConfig.lanaEPS);
	}

	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()),ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.DIAMOND_SWORD,MessageUtil.color("&cPistol &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);

		player.getInventory().setItem(0, sword);

		player.getInventory().setHelmet(ItemFactory.anUnbreakable(ItemFactory.create(Material.DIAMOND_HELMET)));
		player.getInventory().setChestplate(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_CHESTPLATE)));
		player.getInventory().setLeggings(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_CHESTPLATE)));
		player.getInventory().setBoots(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_BOOTS)));
	}

	public void hit(Player entity, Player attacker, boolean headshot) {
		double damage = 3.0;
		if (headshot) {
			attacker.playSound(attacker.getLocation(), Sound.SUCCESSFUL_HIT, 5f, 1.8f);
			damage = damage * 2;
		}

		ClassDamage.handle(attacker, entity, MathUtils.floor(damage));
		entity.getWorld().playEffect(entity.getEyeLocation(), Effect.STEP_SOUND, 55);
		attacker.playSound(attacker.getLocation(), Sound.SUCCESSFUL_HIT, 5f, 0.9f);
	}


	@Override
	public boolean onInteract(Player player) {
		GamePlayer gp = GamePlayer.get(player); 
		if(!shots.containsKey(player))
			shots.put(player, 0);
		if(shots.get(player) < 5) {
			gp.setCanGainEnergy(false);
			shots.put(player, shots.get(player)+1);;
			gp.setEnergy(100);
		}else {
			shots.put(player, 0);
			gp.setEnergy(0);
			gp.setCanGainEnergy(true);
		}

		double sharpness = 0.1;
		double travel = 0;
		double maxTravel = 100;
		double hitBox = 0.5;

		player.getWorld().playEffect(player.getEyeLocation().add(player.getLocation().getDirection()), Effect.SMOKE, 4);
		player.getWorld().playSound(player.getEyeLocation(), Sound.EXPLODE, 0.6f, 2f);
		while (travel < maxTravel) {
			Location loc = player.getEyeLocation().add(player.getLocation().getDirection().multiply(travel));

			for(Player cur : player.getWorld().getPlayers()) {
				if(cur != player && GamePlayer.get(cur).getTeam() != GamePlayer.get(player)) {
					if (MathUtils.offset(loc, cur.getEyeLocation()) < 0.3) {
						hit(cur, player, true);
						player.getWorld().playSound(loc, Sound.BLAZE_HIT, 0.4f, 2f);
						break;
					} else if (MathUtils.offset2d(loc, cur.getLocation()) < hitBox) {
						if (loc.getY() > cur.getLocation().getY() && loc.getY() < cur.getEyeLocation().getY()) {
							hit(cur, player, false);
							player.getWorld().playSound(loc, Sound.BLAZE_HIT, 0.4f, 2f);
							break;
						}
					}
				}
			}
			travel += sharpness;
		}
		return true;
	}
}
