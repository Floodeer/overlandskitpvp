package com.overlands.kitpvp.game.classes.hero;

import com.overlands.kitpvp.game.GamePlayer;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.effects.ClassEffectType;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;
import com.overlands.kitpvp.preferences.ParticleFilter;
import com.overlands.kitpvp.sound.SoundManager;

import de.slikey.effectlib.EffectType;
import de.slikey.effectlib.effect.ShieldEffect;
import de.slikey.effectlib.util.DynamicLocation;

public class Pigman extends GameClass {

	public Pigman() {
		super(ClassType.PIGMAN, "Pigman", Main.classConfig.pigmanPrice, Main.classConfig.pigmanEPH, Main.classConfig.pigmanEPS);
	}

	@Override
	public boolean onInteract(Player p) {
		SoundManager.PIGMAN.playForAll(p.getLocation(), 2.0F, 1.0F);
		final ShieldEffect area = new ShieldEffect(Main.get().effectManager);
		area.setDynamicOrigin(new DynamicLocation(p));
		area.particle = ClassEffectType.WATER_BUBBLE.hasSelected(p) ? de.slikey.effectlib.util.ParticleEffect.DRIP_WATER : de.slikey.effectlib.util.ParticleEffect.FLAME;
		area.type = EffectType.REPEATING;
		area.radius = 7;
		area.sphere = true;
		area.particles = 20;
		area.infinite();
		area.start();
		p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 25, 0));
		p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 6 * 20, 1));
		new BukkitRunnable() {
			int step = 0;
			int step2 = 0;
			int pStep = 0;

			@Override
			public void run() {
				++step;
				++step2;
				++pStep;
				if (step <= 120 && p.isOnline() && !GamePlayer.get(p).isEMPd()) {
					if (pStep == 10) {
						pStep = 0;
						LocationUtils.getCircle(p.getLocation(), 7.0D, 12).forEach(cur -> ParticleFilter.playWithFilter(ParticleEffect.SMOKE_LARGE, 0.0F, 0.0F, 0.0F, 0.12F, 10, cur, 126, 3));

					}
					if (step2 == 20) {
						step2 = 0;
						Util.filterNearby(p, p.getLocation(), 7.0).forEach(cur -> {
							ClassDamage.handleFalse(p, cur, getType());
							ParticleFilter.playWithFilter(ParticleEffect.FLAME, 0.4F, 0.4F, 0.4F, 0.1F, 60, cur.getLocation().add(0.0D, 0.5D, 0.0D), 126, 2);
						});
					}
				} else {
					cancel();
					area.cancel();
				}
			}
		}.runTaskTimer(Main.get(), 0L, 1L);
		return true;
	}

	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()), ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.DIAMOND_SWORD, MessageUtil.color("&cBurning Soul &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);

		player.getInventory().setItem(0, sword);

		player.getInventory().setHelmet(ItemFactory.createUnbreakable(Material.IRON_HELMET));
		player.getInventory().setChestplate(ItemFactory.createUnbreakable(Material.DIAMOND_CHESTPLATE, Enchantment.PROTECTION_ENVIRONMENTAL, 1));
		player.getInventory().setLeggings(ItemFactory.createUnbreakable(Material.IRON_LEGGINGS));
		player.getInventory().setBoots(ItemFactory.createUnbreakable(Material.IRON_BOOTS));
	}
}
