package com.overlands.kitpvp.game.classes.hero;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.overlands.core.utils.FireworkUtils;
import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.effects.ClassEffectType;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;

import de.slikey.effectlib.util.ParticleEffect;

public class Paladin extends GameClass {

	public Paladin() {
		super(ClassType.PALADIN, "Paladin", Main.classConfig.paladinPrice, Main.classConfig.paladinEPH, Main.classConfig.paladinEPS);
	}

	@Override
	public boolean onInteract(Player p) {
		Player t = LocationUtils.getTarget(p, 20, 20, 20, 20);
		if(t == null)
			return false;
		GamePlayer target = GamePlayer.get(t);
		if(!Util.canBeDamaged(target.getPlayer(), p))
			return false;
		
		p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, -9));
		p.setWalkSpeed(0.0F);
		p.setFlySpeed(0.0F);

		p.getWorld().playSound(p.getLocation(), Sound.IRONGOLEM_THROW, 2.0F, 0.0F);
		p.getWorld().playSound(p.getLocation(), Sound.FIREWORK_LAUNCH, 2.0F, 0.0F);
        Location l = t.getLocation();
		new BukkitRunnable() {
			int flyStep = 0;
			final double multiply = p.getLocation().distance(l) / 12.0D + 3.5D;
			int particleStep = 4;

			public void run() {
				if (!p.isOnline() || GamePlayer.get(p).isEMPd()) {
					stop(p);
					cancel();
					return;
				}
				this.particleStep += 1;
				if (this.particleStep == 5) {
					for (int i = 0; i < 12; i += 3) {
						for (Location location : LocationUtils.getCircle(l, i, i * 12))  {
							if(!ClassEffectType.FLAME_STRIKE.hasSelected(p)) {
								ParticleEffect.FIREWORKS_SPARK.display(0.0F, 0.0F, 0.0F, 0.0F, 1, location.add(0.0D, 0.1D, 0.0D), 120);
							}else{
								ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, location.add(0.0D, 0.1D, 0.0D), 120);
							}
						}
					}
					this.particleStep = 0;
				}
				this.flyStep += 2;
				if (this.flyStep < 15) {
					ParticleEffect.CLOUD.display(0.1F, 0.1F, 0.1F, 0.05F, 3, p.getLocation().add(0.0D, 1.0D, 0.0D), 120);
					p.teleport(p.getLocation().add(0.0D, 2.0D, 0.0D));
				} else if (this.flyStep < 20) {
					p.setVelocity(p.getVelocity().zero());
				} else if (this.flyStep == 25) {
					ParticleEffect.EXPLOSION_HUGE.display(0.5F, 0.5F, 0.5F, 1.0F, 5, p.getLocation().add(0.0D, 1.0D, 0.0D), 120);
					ParticleEffect.CLOUD.display(0.0F, 0.0F, 0.0F, 0.7F, 250,p.getLocation().add(0.0D, 1.0D, 0.0D), 120);
				} else if (this.flyStep > 25) {
					Vector localVector = l.toVector().subtract(p.getLocation().toVector()).normalize();
					Location l2 = p.getLocation().add(localVector.multiply(this.multiply));
					l2.setDirection(localVector);

					ParticleEffect.EXPLOSION_LARGE.display(0.0F, 0.0F, 0.0F, 1.0F, 1,p.getLocation().add(0.0D, 1.0D, 0.0D), 120);
					ParticleEffect.CLOUD.display(0.0F, 0.0F, 0.0F, 0.1F, 5,p.getLocation().add(0.0D, 1.0D, 0.0D), 120);

					p.teleport(l2);
					if (p.getLocation().distance(l) <= 4.0D) {
						l.setDirection(localVector);
						p.teleport(l);
						ParticleEffect.FIREWORKS_SPARK.display(0.0F, 0.0F, 0.0F, 1.0F, 300,p.getLocation().add(0.0D, 1.0D, 0.0D), 120);
						ParticleEffect.EXPLOSION_HUGE.display(3.0F, 0.0F, 3.0F, 1.0F, 10,p.getLocation().add(0.0D, 1.0D, 0.0D), 120);
						p.getWorld().playSound(l, Sound.EXPLODE, 3.0F, 0.0F);
						FireworkUtils.playInstantFirework(l,FireworkEffect.builder().with(Type.BALL_LARGE).withFlicker().withColor(Color.WHITE).build());
						Util.filterNearby(p, l, 12).forEach(player ->  ClassDamage.handle(p, player, getType()));
						stop(p);
						cancel();
					}
				}
			}
		}.runTaskTimer(Main.get(), 0L, 1L);
		
		return true;
	}
	
	private void stop(Player p) {
		p.removePotionEffect(PotionEffectType.JUMP);
		p.setWalkSpeed(0.2F);
		p.setFlySpeed(0.1F);
	}

	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()), ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.IRON_SWORD, MessageUtil.color("&cMeteor Strike &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);

		player.getInventory().setItem(0, sword);

		player.getInventory().setHelmet(ItemFactory.createUnbreakable(Material.IRON_HELMET));
		player.getInventory().setChestplate(ItemFactory.createUnbreakable(Material.DIAMOND_CHESTPLATE, Enchantment.PROTECTION_ENVIRONMENTAL, 1));
		player.getInventory().setLeggings(ItemFactory.createUnbreakable(Material.IRON_LEGGINGS));
		player.getInventory().setBoots(ItemFactory.createUnbreakable(Material.IRON_BOOTS));
	}
}
