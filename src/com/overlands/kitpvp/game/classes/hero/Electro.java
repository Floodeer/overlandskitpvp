package com.overlands.kitpvp.game.classes.hero;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;
import com.overlands.kitpvp.sound.SoundManager;

public class Electro extends GameClass {	

	public Electro() {
		super(ClassType.ELECTRO, "Electro", Main.classConfig.electroPrice, Main.classConfig.electroEPH, Main.classConfig.electroEPS);
	}

	@Override
	public boolean onInteract(Player p) {
		GamePlayer.get(p).setCanGainEnergy(false);
		SoundManager.ELECTRO.playForAll(p.getLocation(), 1.0F, 1.0F);
	    new BukkitRunnable() {
	      int abilityStep = 0;
	      int damageStep = 9;
	      Location location = p.getLocation();
	      public void run() {
	        this.abilityStep += 1;
	        if ((this.abilityStep == 80) || (!p.isOnline()) || (GamePlayer.get(p).isEMPd())) {
	          cancel();
	  		  GamePlayer.get(p).setCanGainEnergy(true);
	          return;
	        }
	        this.damageStep += 1;
	        if (this.damageStep == 10){
	          this.damageStep = 0;
	          LocationUtils.getCircle(this.location.clone().add(0.0D, 1.0D, 0.0D), 5.0D, 40).stream().forEach(l -> {
	        	  ParticleEffect.FIREWORKS_SPARK.display(0.0F, 0.0F, 0.0F, 0.1F, 3, l, 126);
		          ParticleEffect.CLOUD.display(0.0F, 0.0F, 0.0F, 0.01F, 1, l, 126);
	          });
	          Util.filterNearby(p, location, 5.0).forEach(cur -> {
	        	   ClassDamage.handle(p, cur, getType());
	        	   cur.getWorld().strikeLightningEffect(cur.getLocation());
				});
	        }
	      }
	    }.runTaskTimer(Main.get(), 0L, 1L);

		return true;
	}
	
	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()),  ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.IRON_SWORD, MessageUtil.color("&cElectronic Pulse &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);
		
		player.getInventory().setItem(0, sword);
		
		player.getInventory().setHelmet(ItemFactory.anUnbreakable(ItemFactory.create(Material.DIAMOND_HELMET)));
		player.getInventory().setChestplate(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_CHESTPLATE)));
		player.getInventory().setLeggings(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_LEGGINGS)));
		player.getInventory().setBoots(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_BOOTS)));
	}
}
