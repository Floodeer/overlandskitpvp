package com.overlands.kitpvp.game.classes.hero;

import com.overlands.kitpvp.game.GamePlayer;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.overlands.core.utils.ActionBar;
import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;
import com.overlands.kitpvp.preferences.ParticleFilter;

public class Magma extends GameClass {

	public Magma() {
		super(ClassType.MAGMA, "Magma", Main.classConfig.magmaPrice, Main.classConfig.magmaEPH, Main.classConfig.magmaEPS);
	}

	@Override
	public boolean onInteract(Player p) {
		final Location localLocation = p.getLocation();
		localLocation.setPitch(0.0F);
		new BukkitRunnable() {
			int step = 0;
			int step2 = 3;
			final Vector direction = localLocation.getDirection();

			public void run() {
				if (!p.isOnline() || GamePlayer.get(p).isEMPd()) {
					cancel();
					return;
				}
				this.step += 1;
				if ((!p.isSneaking()) && (this.step <= 25)) {
					ParticleFilter.playWithFilter(ParticleEffect.EXPLOSION_LARGE, 0.0F, 0.0F, 0.0F, 1.0F, 5,p.getLocation().add(0.0D, 1.0D, 0.0D), 126, 3);
					ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.2F, 5, p.getLocation().add(0.0D, 1.0D, 0.0D),126);
					ParticleFilter.playWithFilter(ParticleEffect.SMOKE_LARGE,0.0F, 0.0F, 0.0F, 0.2F, 100, p.getLocation().add(0.0D, 1.0D, 0.0D), 126, 3);			
					ActionBar.sendActionBar(p, MessageUtil.color("&9&lPressione shift para cancelar antes da hora!"));
					p.setVelocity(this.direction);
					for (Player localLivingEntity : LocationUtils.getNearbyPlayers(p.getLocation(), 5)) {
						if (localLivingEntity != p)
							localLivingEntity.setFireTicks(60);
					}

					this.step2 += 1;
					if (this.step2 == 4) {
						p.getWorld().playSound(p.getLocation(), Sound.EXPLODE, 1.0F, 1.5F);
						p.getWorld().playSound(p.getLocation(), Sound.EXPLODE, 1.0F, 0F);
						this.step2 = 0;
					}
				} else {
					float f2 = 7.0F;
					if (this.step <= 5) {

						f2 = 5.0F;
					} else if (this.step <= 10) {
						f2 = 5.5F;
					} else if (this.step <= 15) {
						f2 = 6.0F;
					} else if (this.step <= 20) {

						f2 = 6.5F;
					}

					ParticleEffect.EXPLOSION_LARGE.display(0.0F, 0.0F, 0.0F, 0.0F, 1, p.getLocation().add(0.0D, 1.0D, 0.0D), 126);
					Util.filterNearby(p, p.getLocation(), f2).forEach(player -> {
						ClassDamage.handle(p, player, getType());
						player.setFireTicks(60);
					});
					displayFlames(p.getLocation());
					ActionBar.sendActionBar(p, MessageUtil.color(""));
					cancel();
				}
			}
		}.runTaskTimer(Main.get(), 0L, 1L);

		return true;
	}

	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()),
				ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.IRON_SWORD, MessageUtil.color("&cFlame Dash &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);

		player.getInventory().setItem(0, sword);

		player.getInventory().setHelmet(ItemFactory.createUnbreakable(Material.DIAMOND_HELMET, Enchantment.PROTECTION_ENVIRONMENTAL, 1));
		player.getInventory().setChestplate(ItemFactory.createUnbreakable(Material.DIAMOND_CHESTPLATE, Enchantment.PROTECTION_FIRE, 3));
		player.getInventory().setLeggings(ItemFactory.createUnbreakable(Material.IRON_LEGGINGS));
		player.getInventory().setBoots(ItemFactory.createUnbreakable(Material.IRON_BOOTS));
	}

	private void displayFlames(Location paramLocation) {
		Location localLocation1 = paramLocation.clone();
		final Location localLocation2 = paramLocation.clone().add(MathUtils.random(-10, 10),MathUtils.random.nextInt(5) + 5, MathUtils.random(-10, 10));
		new BukkitRunnable() {
			int step = 20 * MathUtils.random.nextInt(5) + 1;

			public void run() {
				this.step -= 1;
				if ((this.step == 0) || (localLocation1.distance(localLocation2) <= 0.5D)) {
					cancel();
					return;
				}
				Vector localVector = localLocation2.toVector().subtract(localLocation1.toVector()).normalize();
				localVector = new Vector(localVector.getX() / 4.0D, localVector.getY() / 4.0D,localVector.getZ() / 4.0D);
				localLocation1.add(localVector);

				ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.1F, 3, localLocation1, 126);
				ParticleEffect.SMOKE_LARGE.display(0.0F, 0.0F, 0.0F, 0.1F, 1, localLocation1,126);
			}
		}.runTaskTimer(Main.get(), 0L, 1L);
	}
}
