package com.overlands.kitpvp.game.classes.hero;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.entity.WitherSkull;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Runner;
import com.overlands.core.utils.Sounds;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.calc.particle.Wings;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.effects.ClassEffectType;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;

public class Dreadlord extends GameClass {

	public Dreadlord() {
		super(ClassType.DREADLORD, "Dreadlord", Main.classConfig.dreadlordPrice, Main.classConfig.dreadlordEPH, Main.classConfig.dreadlordEPS);
	}

	@Override
	public boolean onInteract(Player player) {

		Vector localVector = player.getLocation().getDirection();
		for (int i = 0; i < 3; i++) {
			WitherSkull ws = player.launchProjectile(WitherSkull.class);
			ws.setMetadata("skills", new FixedMetadataValue(Main.get(), null));
			if (i > 0) {
				localVector.setX(localVector.getX() + (MathUtils.random.nextDouble() - 0.5D) * 1.3D);
				localVector.setZ(localVector.getZ() + (MathUtils.random.nextDouble() - 0.5D) * 1.3D);
			}
			ws.setVelocity(ws.getVelocity().zero());
			ws.setVelocity(localVector.multiply(1.05));
		}
		
		if(!Main.getGame().isGodMode()) {
			if(ClassEffectType.DEMON_WINGS.hasSelected(player)) {
				Runner.make(Main.get()).interval(1).limit(3*20).run(new Wings(player));
			}
		}
		return true;
	}

	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()),ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.DIAMOND_SWORD, MessageUtil.color("&cWither Blast &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);

		player.getInventory().setItem(0, sword);

		player.getInventory().setHelmet(ItemFactory.createUnbreakable(Material.DIAMOND_HELMET, Enchantment.PROTECTION_ENVIRONMENTAL, 1));
		player.getInventory().setChestplate(ItemFactory.createUnbreakable(Material.IRON_CHESTPLATE));
		player.getInventory().setLeggings(ItemFactory.createUnbreakable(Material.IRON_LEGGINGS));
		player.getInventory().setBoots(ItemFactory.createUnbreakable(Material.IRON_BOOTS));
	}

	@EventHandler
	public void onEntityExplode(EntityExplodeEvent event) {
		if (event.getEntity() instanceof WitherSkull) {
			event.setCancelled(true);
			WitherSkull ws = (WitherSkull) event.getEntity();
			if (!(ws.getShooter() instanceof Player))
				return;
			if(!(ws.hasMetadata("skills")))
				return;

			Player shooter = (Player) ws.getShooter();
			new BukkitRunnable() {
				int step = 0;

				@Override
				public void run() {
					if (shooter != null && shooter.isOnline()) {
						++step;
						if (step <= 2) {
							ParticleEffect.EXPLOSION_LARGE.display(0.0F, 0.0F, 0.0F, 0.3F, 1, ws.getLocation().add(0.0D, 0.5D, 0.0D), 25 * 3);
							ws.getWorld().playSound(ws.getLocation(), Sounds.EXPLODE.bukkitSound(), 3.0F, 4.0F);
							if (this.step == 1) {
								Util.filterNearby(shooter, ws.getLocation(), 5).forEach(player ->
								        ClassDamage.handle(shooter, player, getType()));
							}
						} else {
							cancel();
						}
					} else {
						cancel();
					}
				}
			}.runTaskTimer(Main.get(), 0L, 7L);
		}
	}
	
	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e) {
		if (e.getDamager() instanceof WitherSkull) {
				e.setCancelled(true);
		}
	}
}
