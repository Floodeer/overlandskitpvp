package com.overlands.kitpvp.game.classes.hero;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Sounds;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.effects.ClassEffectType;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;

public class Golem extends GameClass {

	public Golem() {
		super(ClassType.GOLEM, "Golem", Main.classConfig.golemPrice, Main.classConfig.golemEPH, Main.classConfig.golemEPS);
	}

	@Override
	public boolean onInteract(Player p) {

		Location loc = p.getLocation();
		Location loc2 = p.getLocation().clone().add(0.0D, 10.0D, 0.0D);
		loc.getWorld().playSound(loc, Sounds.EXPLODE.bukkitSound(), 1.0F, 1.0F);
		loc.getWorld().playSound(loc, Sounds.ANVIL_LAND.bukkitSound(), 1.0F, 2.0F);
		LocationUtils.getNearbyPlayers(p.getLocation(), 5.0).stream()
				.filter((cur) -> GamePlayer.get(cur).getTeam() != GamePlayer.get(p) && !cur.equals(p))
				.forEach((entity) -> ClassDamage.handle(p, entity, getType()));
		int i = 4;
		int j = 3;

		for (Location locals : LocationUtils.getCircle(LocationUtils.getLocation(loc, 0.0D, 1.0D, 0.0D), i - 1, 70)) {
			ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, locals, 25);
		}
		for (float f = j; f > 0.3D; f -= 0.3F) {
			for (Location locals : LocationUtils.getCircle(LocationUtils.getLocation(loc, 0.0D, 1.0D, 0.0D), f, 6)) {
				ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, locals, 25);
			}
		}

		Material mat = ClassEffectType.DIAMOND_PUNCH.hasSelected(p) ? Material.DIAMOND_BLOCK : Material.IRON_BLOCK;
		FallingBlock fa = loc2.getWorld().spawnFallingBlock(LocationUtils.getLocation(loc2, i, 0.0D, 0.0D), mat, (byte) 0);
		FallingBlock fa0 = loc2.getWorld().spawnFallingBlock(LocationUtils.getLocation(loc2, 0.0D, 0.0D, i), mat, (byte) 0);
		FallingBlock fa1 = loc2.getWorld().spawnFallingBlock(LocationUtils.getLocation(loc2, -i, 0.0D, 0.0D), mat, (byte) 0);
		FallingBlock fa2 = loc2.getWorld().spawnFallingBlock(LocationUtils.getLocation(loc2, 0.0D, 0.0D, -i), mat, (byte) 0);
		FallingBlock fa3 = loc2.getWorld().spawnFallingBlock(LocationUtils.getLocation(loc2, j, 0.0D, j), mat, (byte) 0);
		FallingBlock fa4 = loc2.getWorld().spawnFallingBlock(LocationUtils.getLocation(loc2, -j, 0.0D, -j), mat, (byte) 0);
		FallingBlock fa5 = loc2.getWorld().spawnFallingBlock(LocationUtils.getLocation(loc2, j, 0.0D, -j), mat, (byte) 0);
		FallingBlock fa6 = loc2.getWorld().spawnFallingBlock(LocationUtils.getLocation(loc2, -j, 0.0D, j), mat, (byte) 0);

		FallingBlock[] fallingBlocks = { fa, fa0, fa1, fa2, fa3, fa4, fa5, fa5, fa6 };
		for(FallingBlock f : fallingBlocks) {
			if (!f.isDead()) {
				f.setMetadata("golem", new FixedMetadataValue(Main.get(), true));
				f.setVelocity(new Vector(0, -2, 0));
				f.setDropItem(false);
				ParticleEffect.EXPLOSION_NORMAL.display(0.4F, 0.4F, 0.4F, 0.2F, 10, f.getLocation(), 25 * 4);
			}
		}
		return true;
	}

	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()), ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.IRON_SWORD, MessageUtil.color("&cIron Punch &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);

		player.getInventory().setItem(0, sword);

		player.getInventory().setHelmet(ItemFactory.createUnbreakable(Material.IRON_HELMET));
		player.getInventory().setChestplate(ItemFactory.createUnbreakable(Material.DIAMOND_CHESTPLATE,Enchantment.PROTECTION_ENVIRONMENTAL, 1));
		player.getInventory().setLeggings(ItemFactory.createUnbreakable(Material.IRON_LEGGINGS));
		player.getInventory().setBoots(ItemFactory.createUnbreakable(Material.DIAMOND_BOOTS, Enchantment.PROTECTION_ENVIRONMENTAL, 2));
	}

	@EventHandler
	public void onEntityChangeBlock(final EntityChangeBlockEvent e) {

		if (((e.getEntity() instanceof FallingBlock)) && (e.getEntity().hasMetadata("golem")) && (e.getTo() == Material.IRON_BLOCK || e.getTo() == Material.DIAMOND_BLOCK)) {
			Location loc = e.getBlock().getLocation();
			ParticleEffect.EXPLOSION_HUGE.display(0.0F, 0.0F, 0.0F, 1.0F, 1, loc, 120);
			loc.getWorld().playSound(loc, Sounds.EXPLODE.bukkitSound(), 1.0F, 0.7F);
			e.getBlock().setType(Material.AIR);
			e.setCancelled(true);
		}
	}
}
