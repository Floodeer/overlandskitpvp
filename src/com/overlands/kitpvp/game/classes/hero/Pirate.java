package com.overlands.kitpvp.game.classes.hero;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Sounds;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;

public class Pirate extends GameClass {
	public Pirate() {
		super(ClassType.PIRATA, "Pirata", Main.classConfig.pirataPrice, Main.classConfig.pirataEPH, Main.classConfig.pirataEPS);
	}

	@Override
	public boolean onInteract(Player p) {
		final Vector localVector = p.getLocation().getDirection();

		final ArmorStand localArmorStand = p.getWorld().spawn(p.getLocation().add(0.0D, 1.0D, 0.0D), ArmorStand.class);
		localArmorStand.setMetadata("skillarmor", new FixedMetadataValue(Main.get(), true));
		localArmorStand.setHelmet(new ItemStack(Material.COAL_BLOCK));
		localArmorStand.setSmall(true);
		localArmorStand.setVisible(false);
		p.getWorld().playSound(p.getLocation(), Sounds.EXPLODE.bukkitSound(), 1.0F, 1.0F);
		new BukkitRunnable() {
			int step = 0;
			boolean explode = false;

			@Override
			public void run() {
				this.step += 1;
				if ((this.step <= 100) || (this.explode)) {
					Location localLocation1 = localArmorStand.getLocation();
					ParticleEffect.SMOKE_NORMAL.display(0.0F, 0.0F, 0.0F, 0.0F, 1, localArmorStand.getEyeLocation(), 250);

					Location localLocation2 = localArmorStand.getLocation().add(localVector.multiply(1));
					Location localLocation3 = localArmorStand.getEyeLocation().add(localVector.multiply(1));
					if ((!localLocation2.getBlock().isEmpty()) || (!localLocation3.getBlock().isEmpty())) {
						this.explode = true;
					}
					localArmorStand.setVelocity(localVector.multiply(1));
					if (this.explode) {
						ParticleEffect.EXPLOSION_NORMAL.display(0.0F, 0.0F, 0.0F, 1.0F, 15, localArmorStand.getEyeLocation(), 250);
						Util.filterNearby(p, localLocation1, 6).forEach(player ->  ClassDamage.handle(p, player, getType()));
						localArmorStand.remove();
						cancel();
					}
				} else {
					this.explode = true;
				}
			}
		}.runTaskTimer(Main.get(), 0L, 1L);
		
		return true;
	}

	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()), ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.IRON_SWORD, MessageUtil.color("&cFire Cannon &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);

		player.getInventory().setItem(0, sword);

		player.getInventory().setHelmet(ItemFactory.createUnbreakable(Material.DIAMOND_HELMET));
		player.getInventory().setChestplate(ItemFactory.createUnbreakable(Material.IRON_CHESTPLATE));
		player.getInventory().setLeggings(ItemFactory.createUnbreakable(Material.IRON_LEGGINGS));
		player.getInventory().setBoots(ItemFactory.createUnbreakable(Material.IRON_BOOTS));
	}
}
