package com.overlands.kitpvp.game.classes;

import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;

public class ClassDamage {

	public static void handle(Player damager, Player target, ClassType type) {
		double damage = get(null, type);
		GamePlayer gp = GamePlayer.get(target);
		if(gp.hasShield()) {
			damage = damage - (damage * gp.getProtectionPercentage());
		}
		Main.get().getDamageManager().newDamageEvent(target, damager, null, DamageCause.MAGIC, damage, true, false, true, type.getSkillName(), type.getSkillName());
	}
	
	public static void handle(Player damager, Player target, double damage) {
		GamePlayer gp = GamePlayer.get(target);
		if(gp.hasShield()) {
			damage = damage - (damage * gp.getProtectionPercentage());
		}
		Main.get().getDamageManager().newDamageEvent(target, damager, null, DamageCause.MAGIC, damage, true, false, true, "Random Damage", "Random Damage");
	}
	
	public static void handleFalseDamage(Player damager, Player target, double damage) {
		GamePlayer gp = GamePlayer.get(target);
		if(gp.hasShield()) {
			damage = damage - (damage * gp.getProtectionPercentage());
		}
		
		Main.get().getDamageManager().newDamageEvent(target, damager, null, DamageCause.MAGIC, damage, true, false, false, "Random Damage", "Random Damage");
	}
	
	public static void handleFalse(Player damager, Player target, ClassType type) {
		double damage = get(null, type);
		GamePlayer gp = GamePlayer.get(target);
		if(gp.hasShield()) {
			damage = damage - (damage * gp.getProtectionPercentage());
		}
		Main.get().getDamageManager().newDamageEvent(target, damager, null, DamageCause.MAGIC, damage, true, false, false, type.getSkillName(), type.getSkillName());
	    
	}
	
	
	public static double get(GamePlayer gp, ClassType type) {
		if(type == ClassType.HEROBRINE) {
			return Main.classConfig.herobrineDamage;
		}else if(type == ClassType.CREEPER) {
			return Main.classConfig.creeperDamage;
		}else if(type == ClassType.DREADLORD) {
			return Main.classConfig.dreadlordDamage;
		}else if(type == ClassType.CRYOMANCER) {
			return Main.classConfig.cryomancerDamage;
		}else if(type == ClassType.PIGMAN) {
			return Main.classConfig.pigmanDamage;
		}else if(type == ClassType.SHAMAN) {
			return Main.classConfig.shamanDamage;
		}else if(type == ClassType.SPIDER) {
			return Main.classConfig.spiderDamage;
		}else if(type == ClassType.ARCANIST) {
			return Main.classConfig.arcanistDamage;
		}else if(type == ClassType.INFECTED) {
			return Main.classConfig.infectedDamage;
		}else if(type == ClassType.GOLEM) {
			return Main.classConfig.golemDamage;
		}else if(type == ClassType.PALADIN) {
			return Main.classConfig.paladinDamage;
		}else if(type == ClassType.PYROMANCER) {
			return Main.classConfig.pyromancerDamage;
		}else if(type == ClassType.PIRATA) {
			return Main.classConfig.pirataDamage;
		}else if(type == ClassType.FALCON) {
			return Main.classConfig.falconDamage;
		}else if(type == ClassType.MAGMA) {
			return Main.classConfig.magmaDamage;
		}else if(type == ClassType.ELECTRO) {
			return Main.classConfig.electroDamage;
		}else if(type == ClassType.EPSILON) {
			return Main.classConfig.epsilonDamage;
		}else if(type == ClassType.SKELETON) {
			return Main.classConfig.skeletonDamage;
		}else if(type == ClassType.BLAZE) {
			return Main.classConfig.blazeDamage;
		}else if(type == ClassType.MAGNATITION) {
			return Main.classConfig.magnatitionDamage;
		}else if(type == ClassType.RUMBLER) {
			return Main.classConfig.rumblerDamage;
		}else if(type == ClassType.WIZARD) {
			return Main.classConfig.wizardDamage;
		}else if(type == ClassType.LANA) {
			return Main.classConfig.lanaDamage;
		}else if(type == ClassType.CRUSADER) {
			return Main.classConfig.crusaderDamage;
		}else if(type == ClassType.ANCIENT) {
			return Main.classConfig.ancientDamage;
		}else if(type == ClassType.ELENNE) {
			return Main.classConfig.elenneDamage;
		}else if(type == ClassType.SHADOW) {
			return Main.classConfig.shadowDamage;
		}else if(type == ClassType.FIGHTER) {
			return Main.classConfig.fighterDamage;
		}else if(type == ClassType.SQUID) {
			return Main.classConfig.squidDamage;
		}else if(type == ClassType.AUTOMATON) {
			return Main.classConfig.automatonDamage;
		}
		return 0;
	}

}
