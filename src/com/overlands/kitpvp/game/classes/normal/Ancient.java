package com.overlands.kitpvp.game.classes.normal;

import java.util.List;

import com.overlands.core.utils.location.LocationUtils;
import de.slikey.effectlib.util.MathUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;
import com.overlands.core.utils.Imobilizer;
import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.item.EntityItemFactory;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.effects.ClassEffectType;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;

import de.slikey.effectlib.EffectType;
import de.slikey.effectlib.effect.ConeEffect;
import de.slikey.effectlib.util.DynamicLocation;
import de.slikey.effectlib.util.ParticleEffect;

public class Ancient extends GameClass {

	public Ancient() {
		super(ClassType.ANCIENT, "Ancient", Main.classConfig.ancientPrice, Main.classConfig.ancientEPH, Main.classConfig.ancientEPS);
	}

	@Override
	public boolean onInteract(Player player) {
		Location localLocation1 = player.getEyeLocation().clone();
		localLocation1.setPitch(0.0F);

		ConeEffect localConeEffect1 = new ConeEffect(Main.get().effectManager);
		localConeEffect1.setDynamicOrigin(new DynamicLocation(localLocation1));
		localConeEffect1.particle = ParticleEffect.VILLAGER_HAPPY;
		localConeEffect1.type = EffectType.REPEATING;
		localConeEffect1.radiusGrow = 0.015F;
		localConeEffect1.lengthGrow = 0.04F;
		localConeEffect1.particles = 130;
		localConeEffect1.run();

		ConeEffect localConeEffect2 = new ConeEffect(Main.get().effectManager);
		localConeEffect2.setDynamicOrigin(new DynamicLocation(localLocation1));
		localConeEffect2.particle = ParticleEffect.ENCHANTMENT_TABLE;
		localConeEffect2.speed = 0.1F;
		localConeEffect2.type = EffectType.REPEATING;
		localConeEffect2.radiusGrow = 0.015F;
		localConeEffect2.lengthGrow = 0.04F;
		localConeEffect2.particles = 130;
		localConeEffect2.run();
		
	   player.getWorld().playSound(player.getLocation(), Sound.HORSE_DEATH, 3F, 1f); 
		
		List<Location> loc = Lists.newArrayList();
		for (int i = 0; i < 5; i++) {
			Location l= player.getEyeLocation();
			l.setPitch(0.0F);
			loc.add(l.add(l.getDirection().multiply(i)));
		}
		
		List<Player> damaged = Lists.newArrayList();
		loc.forEach(location -> Util.filterNearby(player, location, 3D).stream().filter(p -> !damaged.contains(p)).forEach(p -> {
            if(ClassEffectType.EMERALD.hasSelected(player)) {
                 for (int i = 0; i < 4; i++) {
                     EntityItemFactory.dropTemporary(LocationUtils.randomizeLocation(p.getLocation()), ItemFactory.create(Material.EMERALD_BLOCK), 50);
                 }
            }

            damaged.add(p);
            ClassDamage.handle(player, p, getType());
            Imobilizer.ImobilizePlayer(p, 15);
        }));
		
		return true;
	}
	
	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()),  ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.IRON_SWORD, MessageUtil.color("&cAncient Breath &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);
		
		player.getInventory().setItem(0, sword);
		
		player.getInventory().setHelmet(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_HELMET)));
		player.getInventory().setChestplate(ItemFactory.anUnbreakable(ItemFactory.create(Material.GOLD_CHESTPLATE, Enchantment.PROTECTION_ENVIRONMENTAL, 2)));
		player.getInventory().setLeggings(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_LEGGINGS)));
		player.getInventory().setBoots(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_BOOTS)));
	}
}
