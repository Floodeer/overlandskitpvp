package com.overlands.kitpvp.game.classes.normal;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Sounds;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.effects.ClassEffectType;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;

public class Blaze extends GameClass {

	public Blaze() {
		super(ClassType.BLAZE, "Blaze", 0, Main.classConfig.blazeEPH, Main.classConfig.blazeEPS);
	}

	@Override
	public boolean onInteract(Player player) {
		new BukkitRunnable() {
			public void run() {
				Location localLocation = player.getEyeLocation();
				localLocation.setPitch(0.0F);
				localLocation.subtract(localLocation.getDirection().multiply(3));
				for (int i = 0; i < 35; i++) {
					displayLine(player,
							localLocation.clone().add(MathUtils.random(-4, 4), -1.5D, MathUtils.random(-4, 4)));
				}
			}
		}.runTaskLater(Main.get(), 8L);

		new BukkitRunnable() {
			int abilityStep = 0;

			public void run() {
				List<Location> localList = LocationUtils.getCircle(player.getLocation(), 1.5D, 35);
				ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, (Location) localList.get(abilityStep), 35);

				abilityStep += 1;
				if (abilityStep == localList.size()) {
					cancel();
				}
			}
		}.runTaskTimer(Main.get(), 0L, 1L);

		new BukkitRunnable() {
			int abilityStep = 0;

			public void run() {
				if (!player.isOnline()) {
					cancel();
					return;
				}
				this.abilityStep += 1;
				if (this.abilityStep <= 3) {
					ParticleEffect.LAVA.display(0.0F, 0.0F, 0.0F, 1.0F, 15, player.getEyeLocation(), 35);
					player.getLocation().getWorld().playSound(player.getLocation(), Sounds.GHAST_FIREBALL.bukkitSound(),
							1.0F, 1.0F);
					shootFireball(player);
				} else {
					cancel();
				}
			}
		}.runTaskTimer(Main.get(), 20L, 5L);

		return true;
	}

	private static void shootFireball(Player player) {
		final Fireball fireball = (Fireball) player.launchProjectile(Fireball.class);
		fireball.setMetadata("skillsf", new FixedMetadataValue(Main.get(), true));
		new BukkitRunnable() {
			Vector direction = player.getLocation().getDirection();

			public void run() {
				if ((!fireball.isDead()) && (fireball.getLocation().distance(player.getLocation()) <= 100.0D)) {
					fireball.setVelocity(direction.multiply(1.75D));
				} else {
					fireball.remove();
					cancel();
				}
			}
		}.runTaskTimer(Main.get(), 0L, 1L);
	}

	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()),
				ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.DIAMOND_SWORD,
				MessageUtil.color("&cImmolating Burst &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);

		ItemStack bow = ItemFactory.create(Material.BOW, MessageUtil.color("&cImmolating Burst &8(Clique-Direito)"), 10,
				Enchantment.ARROW_INFINITE);
		ItemFactory.unbreakable(bow);

		player.getInventory().setItem(9, ItemFactory.create(Material.ARROW, 1));
		player.getInventory().setItem(0, sword);
		player.getInventory().setItem(1, bow);

		player.getInventory().setHelmet(ItemFactory.anUnbreakable(ItemFactory.create(Material.DIAMOND_HELMET)));
		player.getInventory().setChestplate(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_CHESTPLATE)));
		player.getInventory().setLeggings(ItemFactory
				.anUnbreakable(ItemFactory.create(Material.DIAMOND_LEGGINGS, Enchantment.PROTECTION_FIRE, 3)));
		player.getInventory().setBoots(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_BOOTS)));
	}

	private static void displayLine(final Player player, Location loc) {
		new BukkitRunnable() {
			int step = 0;

			public void run() {
				step += 1;
				if (step > 20) {
					cancel();
					return;
				}
				loc.add(0.0D, step <= 6 ? 0.3D : -0.1D, 0.0D);
				if (step <= 10) {
					loc.add(0.0D, 0.1D, 0.0D);
				}
				Vector localVector = player.getEyeLocation().toVector().subtract(loc.toVector()).normalize();
				loc.add(new Vector(localVector.getX() / 2.25D, localVector.getY() / 2.25D, localVector.getZ() / 2.25D));

				if (ClassEffectType.LOVE_BURST.hasSelected(player)) {
					ParticleEffect.HEART.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc, 35);
				} else {
					ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc, 35);
					ParticleEffect.SMOKE_LARGE.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc, 35);
				}
				if (loc.distance(player.getEyeLocation()) <= 1.0D) {
					cancel();
				}
			}
		}.runTaskTimer(Main.get(), 0L, 1L);
	}

	@EventHandler
	public void entityExplode(EntityExplodeEvent e) {
		if (e.getEntity() instanceof Fireball) {
			if (e.getEntity().hasMetadata("skillsf")) {
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e) {
		if (e.getDamager() instanceof Fireball && e.getDamager().hasMetadata("skillsf")) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onProjectile(ProjectileHitEvent e) {
		if (e.getEntity() instanceof Fireball && e.getEntity().hasMetadata("skillsf")) {
			Fireball fireball = (Fireball) e.getEntity();
			if (!(fireball.getShooter() instanceof Player)) {
				return;
			}

			Player shooter = (Player) fireball.getShooter();
			Location loc = fireball.getLocation();

			ParticleEffect.EXPLOSION_LARGE.display(0.0F, 0.0F, 0.0F, 0.3F, 5, loc, 75);
			ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.2F, 15, loc, 75);
			Util.filterNearby(shooter, loc, 3).forEach(p -> ClassDamage.handle(shooter, p, getType()));
		}
	}
}
