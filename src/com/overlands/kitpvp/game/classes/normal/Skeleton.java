package com.overlands.kitpvp.game.classes.normal;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;

public class Skeleton extends GameClass {

	public Skeleton() {
		super(ClassType.SKELETON, "Skeleton", Main.classConfig.skeletonPrice, Main.classConfig.skeletonEPH, Main.classConfig.skeletonEPS);
	}

	@Override
	public boolean onInteract(Player player) {
		Arrow arrow = (Arrow)player.launchProjectile(Arrow.class);
		arrow.setVelocity(player.getLocation().getDirection().multiply(3));
		arrow.setCritical(true);
		new BukkitRunnable() {
			
			@Override
			public void run() {
				if(!player.isOnline()) {
					arrow.remove();
					cancel();
				}else{
					if(arrow.isOnGround() || arrow.isDead()) {
						ParticleEffect.EXPLOSION_HUGE.display(0.0F, 0.0F, 0.0F, 1.0F, 1, arrow.getLocation(),  126);
						arrow.getWorld().playSound(arrow.getLocation(), Sound.EXPLODE, 4.0F, 0.5F);
						Util.filterNearby(player, arrow.getLocation(), 4.5).forEach(cur -> {
							ClassDamage.handle(player, cur, getType());
						});
						arrow.remove();
						cancel();
					}
				}
			}
		}.runTaskTimer(Main.get(), 0L, 1L);
		return true;
	}

	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()), ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.IRON_SWORD, MessageUtil.color("&cExplosive Arrow &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);
		
		ItemStack bow = ItemFactory.create(Material.BOW, MessageUtil.color("&cExplosive Arrow &8(Clique-Direito)"), 2, Enchantment.ARROW_DAMAGE);
		ItemFactory.applyEnchantment(bow, Enchantment.ARROW_INFINITE, 10);
		ItemFactory.unbreakable(bow);
		
		player.getInventory().setItem(9, ItemFactory.create(Material.ARROW, 1));
		player.getInventory().setItem(0, sword);
		player.getInventory().setItem(1, bow);

		player.getInventory().setHelmet(ItemFactory.createUnbreakable(Material.DIAMOND_HELMET, Enchantment.PROTECTION_PROJECTILE, 4));
		player.getInventory().setChestplate(ItemFactory.createUnbreakable(Material.IRON_CHESTPLATE));
		player.getInventory().setLeggings(ItemFactory.createUnbreakable(Material.IRON_LEGGINGS));
		player.getInventory().setBoots(ItemFactory.createUnbreakable(Material.IRON_BOOTS));
	}
	
	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e) {
		if (e.getDamager() instanceof Arrow && e.getEntity() instanceof Player) {
			Arrow a = (Arrow) e.getDamager();
			if (a.getShooter() instanceof Player) {
				Player shooter = (Player) a.getShooter();
				if(GamePlayer.get(shooter).getSelectedClass().equals(this)) {
					Util.addEffect(shooter, PotionEffectType.REGENERATION, 3*20, 2);
					Util.addEffect(shooter, PotionEffectType.SPEED, 4*20, 1);
					
				}
			}
		}
	}
}
