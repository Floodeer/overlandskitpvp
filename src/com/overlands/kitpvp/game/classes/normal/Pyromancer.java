package com.overlands.kitpvp.game.classes.normal;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;
import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;
import com.overlands.kitpvp.preferences.ParticleFilter;
import com.overlands.kitpvp.sound.SoundManager;

import de.slikey.effectlib.EffectType;
import de.slikey.effectlib.effect.ConeEffect;
import de.slikey.effectlib.util.DynamicLocation;
import de.slikey.effectlib.util.ParticleEffect;

public class Pyromancer extends GameClass {

	public Pyromancer() {
		super(ClassType.PYROMANCER, "Pyromancer", Main.classConfig.pyromancerPrice, Main.classConfig.pyromancerEPH, Main.classConfig.pyromancerEPS);
	}

	@Override
	public boolean onInteract(Player player) {
		Location pLoc = player.getEyeLocation();
		pLoc.setPitch(0.0F);

		ConeEffect c1 = new ConeEffect(Main.get().effectManager);
		c1.setDynamicOrigin(new DynamicLocation(pLoc));
		c1.particle = ParticleEffect.FLAME;
		c1.type = EffectType.REPEATING;
		c1.radiusGrow = 0.016F;
		c1.lengthGrow = 0.04F;
		c1.rotation = 600.0D;
		c1.particles = 4;
		c1.iterations = 35;
		c1.start();

		ConeEffect c2 = new ConeEffect(Main.get().effectManager);
		c2.setDynamicOrigin(new DynamicLocation(pLoc));
		c2.particle = ParticleEffect.FLAME;
		c2.type = EffectType.REPEATING;
		c2.radiusGrow = 0.016F;
		c2.lengthGrow = 0.04F;
		c2.particles = 4;
		c2.iterations = 35;
		c2.start();
		
		SoundManager.PYROMANCER.playForAll(player.getLocation(), 1.0F, 1.0F);
		
		List<Location> loc = Lists.newArrayList();
		for (int i = 0; i < 5; i++) {
			Location l= player.getEyeLocation();
			l.setPitch(0.0F);
			loc.add(l.add(l.getDirection().multiply(i)));
		}
		
		List<Player> damaged = Lists.newArrayList();
		loc.forEach(location -> {
			Util.filterNearby(player, location, 3D).forEach(p -> {
				if(!damaged.contains(p)) {
					damaged.add(p);
				    ClassDamage.handle(player, p, getType());
				    p.setFireTicks(60);
				    ParticleEffect.FLAME.display(0.5F, 0.5F, 0.5F, 0.1F, 50, p.getLocation(), ParticleFilter.filterNearby(p.getLocation(), 126, 2));
				}else{
					 ParticleEffect.FLAME.display(0.5F, 0.5F, 0.5F, 0.1F, 50, p.getLocation(), ParticleFilter.filterNearby(p.getLocation(), 126, 2));
					damaged.remove(p);
				}
			});
		});
		
		return true;
	}
	
	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()),  ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.IRON_SWORD, MessageUtil.color("&cFlame Breath &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);
		
		player.getInventory().setItem(0, sword);
		
		player.getInventory().setHelmet(ItemFactory.anUnbreakable(ItemFactory.create(Material.DIAMOND_HELMET, Enchantment.PROTECTION_FIRE, 10)));
		player.getInventory().setChestplate(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_CHESTPLATE)));
		player.getInventory().setLeggings(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_LEGGINGS)));
		player.getInventory().setBoots(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_BOOTS)));
	}
}
