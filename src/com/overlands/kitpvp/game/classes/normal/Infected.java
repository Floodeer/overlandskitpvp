package com.overlands.kitpvp.game.classes.normal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.overlands.kitpvp.game.GamePlayer;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.common.collect.Lists;
import com.overlands.core.utils.FireworkUtils;
import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;
import com.overlands.kitpvp.preferences.ParticleFilter;

public class Infected extends GameClass {

	public Infected() {
		super(ClassType.INFECTED, "Infected", Main.classConfig.infectedPrice, Main.classConfig.infectedEPH, Main.classConfig.infectedEPS);
	}

	@Override
	public boolean onInteract(Player player) {
		Block block = LocationUtils.getGround(player.getLocation()).getBlock();
		List<Block> arrayList = new ArrayList<Block>();
		for (int i = 1; i < 6; ++i) {
			arrayList.add(block.getLocation().add(0.0, i, 0.0).getBlock());
		}
		ArrayList<Block> arrayList2 = new ArrayList<Block>();
		for (Block blocks : LocationUtils.getCube(block.getLocation().add(0.0, 4.0, 0.0), (int) 3)) {
			if (blocks.getLocation().getBlockY() <= block.getLocation().add(0.0D, 4.0D, 0.0D).getBlockY()) {
		        arrayList2.add(blocks.getLocation().getBlock());
		     }
		}
		
		 for (Block block2 : arrayList) {
	            if (block2.isEmpty()) continue;
	            player.sendMessage(MessageUtil.color("&9Não há espaço!"));
	            return true;
	        }
	        for (Block block3 : arrayList2) {
	            if (block3.isEmpty()) continue;
	            player.sendMessage(MessageUtil.color("&9Não há espaço!"));
	            return true;
	        }
		
		List<Location> arrayList3 = Lists.newArrayList();
		arrayList3.add(block.getLocation().add(1.0, 0.0, 0.0));
		arrayList3.add(block.getLocation().add(0.0, 0.0, 1.0));
		arrayList3.add(block.getLocation().subtract(1.0, 0.0, 0.0));
		arrayList3.add(block.getLocation().subtract(0.0, 0.0, 1.0));

		new BukkitRunnable() {
			int step = 0;
			int step2 = 19;
			int logsIndex = 0;
			int leavesIndex = 0;

			@Override
			public void run() {
				step += 1;
				step2 += 1;
				if (step <= 160 && player.isOnline() && !GamePlayer.get(player).isEMPd()) {
					if (step2 == 20) {
						step2 = 0;
						if (this.logsIndex < arrayList.size()) {
							Block localBlock = arrayList.get(this.logsIndex);
							localBlock.setType(this.logsIndex == 4 ? Material.HUGE_MUSHROOM_2 : Material.HUGE_MUSHROOM_1);
							this.logsIndex += 1;
						}
						if (this.step * 20 >= 80) {
							for (int i = 0; i < 5; i++) {
								if (this.leavesIndex < arrayList2.size()) {
									Block block = arrayList2.get(this.leavesIndex);
									block.setType(Material.HUGE_MUSHROOM_2);
									this.leavesIndex += 1;
								}
							}
						}
						block.getWorld().playSound(block.getLocation(), Sound.VILLAGER_NO, 5.0F, 0.2F);
						ParticleFilter.playWithFilter(ParticleEffect.VILLAGER_ANGRY, 0.9F, 1.7F, 0.9F, 1.0F, 17,
								block.getLocation().add(0.5D, 3.0D, 0.5D), 126, 3);
						Util.filterNearby(player, block.getLocation(), 36.0).forEach(cur -> {
							ClassDamage.handle(player, cur, 2);
						});
						arrayList3.forEach(b -> {
							LocationUtils.getNearbyPlayers(b, 126).forEach(p -> {
								if (p != player) {
									p.sendBlockChange(b, Material.WOOL, (byte) 14);
								} else {
									p.sendBlockChange(b, Material.WOOL, (byte) 5);
								}
							});
						});
						ParticleEffect.VILLAGER_ANGRY.display(0.9F, 1.7F, 0.9F, 1.0F, 17, block.getLocation().add(0.5D, 3.0D, 0.5D), 126);
					}
				} else {
					FireworkUtils.playInstantFirework(block.getLocation().add(0.5D, 4.0D, 0.5D),
							FireworkEffect.Type.BALL, true, false,
							Arrays.asList(Color.RED, Color.ORANGE),
							Collections.singletonList(Color.WHITE));
					block.getWorld().playSound(block.getLocation(), Sound.EXPLODE, 5.0F, 0.0F);
					ParticleEffect.EXPLOSION_HUGE.display(0.0F, 0.0F, 0.0F, 1.0F, 1,
							block.getLocation().add(0.5D, 4.0D, 0.5D), 126);
					Util.filterNearby(player, block.getLocation(), 50.0).forEach(cur -> {
						ClassDamage.handle(player, cur, 4);
					});
					arrayList.forEach(b -> b.setType(Material.AIR));
					arrayList2.forEach(b -> b.setType(Material.AIR));
					arrayList3.forEach(b -> b.getBlock().getState().update());
					cancel();
				}
			}
		}.runTaskTimer(Main.get(), 0, 1);

		return true;
	}

	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()),
				ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.IRON_SWORD, MessageUtil.color("&cDoomShroom &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);

		player.getInventory().setItem(0, sword);

		player.getInventory().setHelmet(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_HELMET)));
		player.getInventory().setChestplate(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_CHESTPLATE)));
		player.getInventory().setLeggings(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_LEGGINGS)));
		player.getInventory().setBoots(ItemFactory.anUnbreakable(ItemFactory.create(Material.DIAMOND_BOOTS)));
	}
}
