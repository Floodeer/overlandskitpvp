package com.overlands.kitpvp.game.classes.normal;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Sounds;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.calc.VelocityUtils;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.java.FinalReference;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;

public class Squid extends GameClass {

	public Squid() {
		super(ClassType.SQUID, "Squid", Main.classConfig.squidPrice, Main.classConfig.squidEPH, Main.classConfig.squidEPS);
	}

	@Override
	public boolean onInteract(Player player) {
		Location loc = player.getLocation();
		loc.getWorld().playSound(loc, Sounds.SPLASH.bukkitSound(), 1.0F, 1.0F);
		FinalReference<Integer> t = new FinalReference<Integer>(0);
		Util.filterNearby(player, loc, 5.5).forEach(target -> {
			t.set(t.get()+1);
			VelocityUtils.knockback(target, loc, 1.2, 0.4, true);
			ClassDamage.handle(player, target, getType());
			if(t.get() <= 4) {
				circles(target.getLocation(), 0.8F, 0.04F, 50, 20, 1);
			}
		});
		if(t.get() == 0) {
			circles(player.getLocation(), 0.8F, 0.04F, 50, 20, 1);
		}
		final int total = t.get();
		FinalReference<Double> heals = new FinalReference<Double>(1.5);
		
		for(int i = 0; i < total; i++) {
			heals.set(heals.get()+0.5);
		}
		new BukkitRunnable() {
			@Override
			public void run() {
				LocationUtils.getNearbyPlayers(player.getLocation(), 6).forEach((p) -> {
					if(GamePlayer.get(player).getTeam() == GamePlayer.get(p)) {
						Util.heal(player, p, heals.get(), true, false, true);
					}
				});
				Util.heal(player, player, heals.get(), true, false, true);
			}
		}.runTaskLater(Main.get(), 5L);

		return true;
	}

	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()),ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.IRON_SWORD, MessageUtil.color("&cSplash &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);
		
		player.getInventory().setItem(0, sword);

		player.getInventory().setHelmet(ItemFactory.anUnbreakable(ItemFactory.create(Material.DIAMOND_HELMET)));
		player.getInventory().setChestplate(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_CHESTPLATE)));
		player.getInventory().setLeggings(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_LEGGINGS)));
		player.getInventory().setBoots(ItemFactory.anUnbreakable(ItemFactory.create(Material.DIAMOND_BOOTS)));
	}

	public static void circles(final Location l, final float f, final float f2, final int f3, final int i, int i2) {
		new BukkitRunnable() {
			int step = 0;
			float size2 = f;
			@Override
			public void run() {
				++this.step;
				this.size2 += f2;
				if (this.step <= i) {
					for (Location loc : LocationUtils.getCircle(l, size2, f3)) {
						ParticleEffect.REDSTONE.display(new ParticleEffect.OrdinaryColor(150, 255, 255), loc, 25);
					}
				} else {
					this.cancel();
				}
			}
		}.runTaskTimer(Main.get(), 0L, i2);
	}
}
