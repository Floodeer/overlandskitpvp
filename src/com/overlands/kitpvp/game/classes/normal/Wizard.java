package com.overlands.kitpvp.game.classes.normal;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.google.common.collect.Lists;
import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.block.BlockUtils;
import com.overlands.core.utils.block.ShapeUtils;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.effects.ClassEffectType;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;

public class Wizard extends GameClass {

	public Wizard() {
		super(ClassType.WIZARD, "Wizard", Main.classConfig.wizardPrice, Main.classConfig.wizardEPH, Main.classConfig.wizardEPS);
	}

	@Override
	public boolean onInteract(Player player) {
		final Location missileLocation = player.getEyeLocation();
		final Location shotFrom = missileLocation.clone();
		final Vector direction = missileLocation.getDirection().normalize().multiply(0.3);
		final int maxRange = 400;
		final int maxDings = maxRange * 3;
		final List<Player> damaged = Lists.newArrayList();

		new BukkitRunnable() {
			private int dingsDone;
			private Location previousLocation = missileLocation;

			private void burst() {

				Util.filterNearby(player, missileLocation, 2.5).forEach(cur -> {
					if(!damaged.contains(cur)) {
						ClassDamage.handle(player, cur, getType());
						damaged.add(cur);
					}
				});
				
				playParticle(missileLocation, previousLocation, player);

				for (int i = 0; i < 120; i++) {
					Vector vector = new Vector(new Random().nextFloat() - 0.5F, new Random().nextFloat() - 0.5F,
							new Random().nextFloat() - 0.5F);

					if (vector.length() >= 1) {
						i--;
						continue;
					}

					Location loc = missileLocation.clone();

					loc.add(vector.multiply(2));

					if(ClassEffectType.FLAME_DUST.hasSelected(player)) {
						ParticleEffect.FLAME.display(0, 0, 0, 0, 1, loc, 400);
					}else{
						ParticleEffect.REDSTONE.display(0, 0, 0, 0, 1, loc, 400);
					}
				}

				missileLocation.getWorld().playSound(missileLocation, Sound.BAT_TAKEOFF, 1.2F, 1);
				cancel();
			}

			@Override
			public void run() {
				if (dingsDone >= maxDings || !player.isOnline()) {
					burst();
				} else {
					for (int i = 0; i < 2; i++) {
						Player closestPlayer = null;
						double dist = 0;
						for (Player closest : player.getLocation().getWorld().getPlayers()) {

							Location loc = closest.getLocation();

							if (closest != player && GamePlayer.get(closest).getTeam() != GamePlayer.get(player)) {
								double dist1 = loc.distance(shotFrom);
								if (dist1 < maxRange + 10) {
									double dist2 = missileLocation.distance(loc);
									if (closestPlayer == null || dist2 < dist) {
										double dist3 = missileLocation.clone().add(direction).distance(loc);

										if (dist3 < dist2) {
											closestPlayer = closest;
											dist = dist2;
										}
									}
								}
							}
						}

						if (closestPlayer != null) {
							Vector newDirection = closestPlayer.getLocation().add(0, 1, 0).toVector().subtract(missileLocation.toVector());

							direction.add(newDirection.normalize().multiply(0.01)).normalize().multiply(0.3);
						}else{
							cancel();
							return;
						}

						if(!closestPlayer.getWorld().equals(shotFrom.getWorld())) {					
							cancel();
							return;
						}

						missileLocation.add(direction);

						if (BlockUtils.solid(missileLocation.getBlock())) {
							burst();
							return;
						}

						Util.filterNearby(player, missileLocation, 2.0).forEach(cur -> {
							if(!damaged.contains(cur)) {
								ClassDamage.handle(player, cur, getType());
								damaged.add(cur);
								burst();
							}
						});
						
						playParticle(missileLocation, previousLocation, player);
						previousLocation = missileLocation.clone();

						dingsDone++;
					}

					missileLocation.getWorld().playSound(missileLocation, Sound.ORB_PICKUP, 0.7F, 0);
				}
			}
		}.runTaskTimer(Main.get(), 0, 0);

		return true;
	}

	private void playParticle(Location start, Location end, Player player) {
		final ArrayList<Location> locations = ShapeUtils.getLinesDistancedPoints(start, end, 0.1);

		new BukkitRunnable() {
			int timesRan;

			public void run() {
				locations.forEach(loc -> {
					if(ClassEffectType.FLAME_DUST.hasSelected(player)) {
						ParticleEffect.FLAME.display(0, 0, 0, 0, 1, loc, 400);
					}else{
						ParticleEffect.REDSTONE.display(0, 0, 0, 0, 1, loc, 400);
					}
			    });

				if (timesRan++ > 1) {
					cancel();
				}
			}
		}.runTaskTimer(Main.get(), 0, 0);
	}

	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()),ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.IRON_SWORD, MessageUtil.color("&cDust Spell &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);

		player.getInventory().setItem(0, sword);

		player.getInventory().setHelmet(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_HELMET)));
		player.getInventory().setChestplate(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_CHESTPLATE)));
		player.getInventory().setLeggings(ItemFactory
				.anUnbreakable(ItemFactory.create(Material.GOLD_LEGGINGS, Enchantment.PROTECTION_ENVIRONMENTAL, 3)));
		player.getInventory().setBoots(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_BOOTS)));
	}
}
