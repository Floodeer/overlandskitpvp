package com.overlands.kitpvp.game.classes.normal;

import com.google.common.collect.Lists;
import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Runner;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class TestClass extends GameClass {

	public TestClass() {
		super(ClassType.TEST, "Test", 0, 2, 5);
	}

	@Override
	public boolean onInteract(Player player) {
		player.sendMessage(":D");
		return true;
	}

	private void effect(Entity ent) {
		new BukkitRunnable() {
			final Location loc2 = ent.getLocation();
			double y = 0;

			public void run() {
				for (int i = 0; i < 3; i++) {
					y += .05;
					for (int j = 0; j < 2; j++) {
						double xz = y * Math.PI * .8 + (j * Math.PI);

						ParticleEffect.SPELL_WITCH.display(0, 0, 0, 0, 1,
								loc2.clone().add(Math.cos(xz) * 2.5, y, Math.sin(xz) * 2.5), 256);
					}
				}
				if (y >= 3)
					cancel();
			}
		}.runTaskTimer(Main.get(), 0, 1);
	}

	private boolean prototype2(Player player) {
		Player t = LocationUtils.getTarget(player, 30);
		if (t == null)
			return false;

		new BukkitRunnable() {
			final Vector dir = t.getLocation().add(0, 1, 0).subtract(player.getLocation().add(0, 1.3, 0)).toVector().normalize().multiply(.3);
			final Location loc = player.getPlayer().getEyeLocation().clone();
			final double r = 0.4;
			int ti = 0;

			public void run() {
				ti++;
				if (ti > 50)
					cancel();

				List<Entity> entities = Arrays.stream(LocationUtils.getNearbyEntities(loc, 16)).filter(cur -> cur != player).collect(Collectors.toList());
				for (double j = 0; j < 4; j++) {
					loc.add(dir);
					for (double i = 0; i < Math.PI * 2; i += Math.PI / 6) {
						Vector vec = MathUtils.rotate(new Vector(r * Math.cos(i), r * Math.sin(i), 0), loc);
						loc.add(vec);
						ParticleEffect.SPELL_WITCH.display(0, 0, 0, 0, 1, loc, 256);
						loc.add(vec.multiply(-1));
					}

					for (Entity target : entities)
						effect(target);
				}
			}
		}.runTaskTimer(Main.get(), 0, 1);
		return true;
	}

	private boolean prototype1(Player player) {
		Player t = LocationUtils.getTarget(player, 16);
		if (t == null)
			return false;

		ParticleEffect.EXPLOSION_LARGE.display(0, 0, 0, 0, 1, t.getLocation().add(0, 1, 0), 256);
		t.getWorld().playSound(t.getLocation(), Sound.FIREWORK_TWINKLE, 2, 2);
		int count = 0;

		for (Entity ent : t.getNearbyEntities(12, 3, 14)) {
			if (count >= 5)
				break;

			count++;
			ParticleEffect.EXPLOSION_LARGE.display(0, 0, 0, 0, 1, ent.getLocation().add(0, 1, 0), 256);
			Location loc_t = t.getLocation().add(0, .75, 0);
			Location loc_ent = ent.getLocation().add(0, .75, 0);
			for (double j1 = 0; j1 < 1; j1 += .04) {
				Vector d = loc_ent.toVector().subtract(loc_t.toVector());
				ParticleEffect.FIREWORKS_SPARK.display(.1f, .1f, .1f, .008f, 3, loc_t.clone().add(d.multiply(j1)), 256);
			}
		}
		return true;
	}

	private boolean prototypeHypixelFrozenScythe(Player player) {

		Location loc = player.getEyeLocation();
		loc.setPitch(0.0F);

		AtomicBoolean disableGravity = new AtomicBoolean(false);

		for(int i = 1; i < 4; i++) {
			Vector vector = player.getLocation().getDirection();
			Vector vec = player.getEyeLocation().getDirection().multiply(i);

			ArmorStand stand = player.getWorld().spawn(player.getLocation().add(0.0D, 1.0D, 0.0D).add(vec), ArmorStand.class);
			stand.setMetadata("nodamage", new FixedMetadataValue(Main.get(), null));
			stand.setHelmet(new ItemStack(Material.ICE));
			stand.setSmall(true);
			stand.setVisible(false);

			new BukkitRunnable() {
				int step = 0;
				boolean explode = false;

				@Override
				public void run() {
					this.step += 1;
					if ((this.step <= 100) || (this.explode)) {
						ParticleEffect.FIREWORKS_SPARK.display(0.0F, 0.0F, 0.0F, 0.0F, 1, stand.getEyeLocation(), 250);

						Location loc2 = stand.getLocation().add(vector.multiply(1));
						Location loc3 = stand.getEyeLocation().add(vector.multiply(1));
						if ((!loc2.getBlock().isEmpty()) || (!loc3.getBlock().isEmpty())) {
							this.explode = true;
						}

						if(disableGravity.get()) {
							stand.setGravity(false);
							Runner.make(Main.get()).delay(100).run(stand::remove);
							cancel();
							return;
						}

						stand.setVelocity(vector.multiply(1));
						if (this.explode) {
							disableGravity.set(true);
							stand.setGravity(false);
							// ParticleEffect.EXPLOSION_NORMAL.display(0.0F, 0.0F, 0.0F, 1.0F, 15, stand.getEyeLocation(), 250);
							Runner.make(Main.get()).delay(100).run(stand::remove);
							cancel();
						}
					} else {
						this.explode = true;
					}
				}
			}.runTaskTimer(Main.get(), 0L, 1L);
		}

		return true;
	}

	public void ice(final Player player, int multiplier) {

	}

	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()), ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.DIAMOND_SWORD, MessageUtil.color("&cTest Class"));
		ItemFactory.unbreakable(sword);

		player.getInventory().setItem(0, sword);

		player.getInventory().setHelmet(ItemFactory.anUnbreakable(ItemFactory.create(Material.DIAMOND_HELMET)));
		player.getInventory().setChestplate(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_CHESTPLATE)));
		player.getInventory().setLeggings(ItemFactory.anUnbreakable(ItemFactory.create(Material.DIAMOND_LEGGINGS, Enchantment.PROTECTION_FIRE, 3)));
		player.getInventory().setBoots(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_BOOTS)));
	}

}