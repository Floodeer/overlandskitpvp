package com.overlands.kitpvp.game.classes.normal;

import java.util.List;

import com.overlands.kitpvp.Main;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;
import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Sounds;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;

public class Herobrine extends GameClass {

	public Herobrine() {
		super(ClassType.HEROBRINE, "Herobrine", Main.classConfig.herobrinePrice, Main.classConfig.herobrineEPH, Main.classConfig.herobrineEPS);
	}

	@Override
	public boolean onInteract(Player player) {

		List<Player> targets = Lists.newArrayList();
		LocationUtils.getNearbyPlayers(LocationUtils.getTargetBlock(player, 6), 4.5).stream().filter((cur) -> cur.getPlayer() != player && GamePlayer.get(cur).isInGame() && GamePlayer.get(cur).getTeam() != GamePlayer.get(player)).forEach(targets::add);

		if (!targets.isEmpty()) {
			targets.forEach((ent) -> {
				ent.getWorld().strikeLightningEffect(ent.getLocation());
				ent.getWorld().playSound(player.getLocation(), Sounds.ENDERMAN_DEATH.bukkitSound(), 1.0F, 0.0F);
				ParticleEffect.LAVA.display(0.0F, 0.0F, 0.0F, 1.0F, 10, ent.getLocation().add(0.0D, 1.0D, 0.0D), 25);
				ClassDamage.handle(player, ent, this.getType());
			});
		}
		return !targets.isEmpty();
	}
	
	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()),  ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.IRON_SWORD, MessageUtil.color("&cLightstrike &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);
		
		player.getInventory().setItem(0, sword);

		player.getInventory().setHelmet(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_HELMET)));
		player.getInventory().setChestplate(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_CHESTPLATE)));
		player.getInventory().setLeggings(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_LEGGINGS)));
		player.getInventory().setBoots(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_BOOTS)));
	}
}
