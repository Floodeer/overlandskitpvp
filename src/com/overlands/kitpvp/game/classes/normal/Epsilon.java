package com.overlands.kitpvp.game.classes.normal;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;
import com.overlands.kitpvp.preferences.ParticleFilter;

public class Epsilon extends GameClass {	

	public Epsilon() {
		super(ClassType.EPSILON, "Epsilon", Main.classConfig.epsilonPrice, Main.classConfig.epsilonEPH, Main.classConfig.epsilonEPS);
	}

	@Override
	public boolean onInteract(Player p) {
		GamePlayer.get(p).setCanGainEnergy(false);
		final Location localLoc = p.getLocation().add(0, 0.5, 0).clone();
        new BukkitRunnable() {
			int step = 0;
			int step2 = 0;
			float speed = 0.0F;
			float pitch = 2.0F;
			@Override
			public void run() {
				this.step += 1;
				this.step2 += 1;
				if (this.pitch > 0.15D) {
					this.pitch -= 0.05F;
				}
				if(GamePlayer.get(p).isEMPd()) {
					cancel();
				}
				if (this.step <= 60) {
					if (this.speed <= 1.5D) {
						this.speed += 0.04F;
					}
					for (Location localLocation : LocationUtils.getCircle(localLoc, 10.0D, 80)) {
						Vector localVector = localLoc.toVector().subtract(localLocation.toVector()).normalize();
						ParticleEffect.SNOW_SHOVEL.display(localVector, this.speed, localLocation, 100);
					}
					Location localLocation;
					for (Location location : LocationUtils.getCircle(localLoc, 10.0D, 60)) {
						localLocation = location;
						ParticleEffect.EXPLOSION_NORMAL.display(0.0F, 0.0F, 0.0F, 0.0F, 1, localLocation, 100);
					}
					ParticleEffect.SNOW_SHOVEL.display(5.0F, 5.0F, 5.0F, 0.0F, 5,LocationUtils.getLocation(localLoc, 0.0D, 5.0D, 0.0D), 100);
					if (this.step2 == 4) {
						this.step2 = 0;
						p.getWorld().playSound(localLoc, Sound.AMBIENCE_RAIN, 2.0F, this.pitch);
					}
					
				} else {
					cancel();
					new BukkitRunnable() {
						double radius = 0.0D;
						float speed = 0.0F;
						float speed2 = 0.0F;

						@Override
						public void run() {
							radius += 0.35D;
							speed = ((float) (speed + 0.15D));
							speed2 = ((float) (speed2 + 0.45D));
							if (radius == 0.35D) {
								localLoc.getWorld().playSound(localLoc, Sound.EXPLODE, 2.0F, 0.0F);
								ParticleEffect.EXPLOSION_NORMAL.display(5.0F, 3.0F, 5.0F, 1.0F, 5,LocationUtils.getLocation(localLoc, 0.0D, 3.0D, 0.0D), 100);
								Util.filterNearby(p, localLoc, 10).forEach(cur -> ClassDamage.handle(p, cur, getType()));
								GamePlayer.get(p).setCanGainEnergy(true);
							}
							Iterator<Location> localIterator;
							if (radius < 10.0D) {
								for (localIterator = LocationUtils.getCircle(localLoc, radius, (int) radius * 20).iterator(); localIterator.hasNext();) {
									Location localLocation = localIterator.next();
									ParticleFilter.playToMediumFilter(ParticleEffect.SNOW_SHOVEL, new Vector(0.0D, 0.1D, 0.0D), speed, localLocation, 100);
								}
								for (localIterator = LocationUtils.getCircle(localLoc, radius, (int) radius * 15).iterator(); localIterator.hasNext();) {
									Location localLocation = localIterator.next();
									ParticleFilter.playToMediumFilter(ParticleEffect.SNOW_SHOVEL, new Vector(0.0D, 0.1D, 0.0D), speed2, localLocation, 100);
								}
							} else {
								cancel();
							}
						}
					}.runTaskTimer(Main.get(), 60L, 1L);
				}
			}
		}.runTaskTimer(Main.get(), 0L, 2L);


		return true;
	}
	
	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()),  ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.IRON_SWORD, MessageUtil.color("&cStatic Blast &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);
		
		player.getInventory().setItem(0, sword);
		
		player.getInventory().setHelmet(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_HELMET, Enchantment.PROTECTION_ENVIRONMENTAL, 2)));
		player.getInventory().setChestplate(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_CHESTPLATE)));
		player.getInventory().setLeggings(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_LEGGINGS)));
		player.getInventory().setBoots(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_BOOTS)));
	}
}
