package com.overlands.kitpvp.game.classes.normal;

import com.overlands.kitpvp.Main;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;
import com.overlands.kitpvp.sound.SoundManager;

public class Enderman extends GameClass {	

	public Enderman() {
		super(ClassType.ENDERMAN, "Enderman", Main.classConfig.endermanPrice, Main.classConfig.endermanEPH, Main.classConfig.endermanEPS);
	}

	@Override
	public boolean onInteract(Player p) {
		Player t = LocationUtils.getTarget(p, 16);
		if (t == null)
			return false;

		GamePlayer target = GamePlayer.get(t);
		if(!Util.canBeDamaged(target.getPlayer(), p))
			return false;
		
		p.teleport(t);
		p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 4 * 20, 2));
		ParticleEffect.SPELL_WITCH.display(0.0F, 0.0F, 0.0F, 1.0F, 15, p.getLocation(), 35);
		SoundManager.ENDERMAN.playForAll(p.getLocation(), 1.0F, 1.0F);
		return true;

	}

	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()),
				ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.IRON_SWORD,
				MessageUtil.color("&cTeleport &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);

		player.getInventory().setItem(0, sword);

		player.getInventory().setHelmet(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_HELMET)));
		player.getInventory().setChestplate(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_CHESTPLATE)));
		player.getInventory().setLeggings(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_LEGGINGS)));
		player.getInventory().setBoots(
				ItemFactory.anUnbreakable(ItemFactory.create(Material.DIAMOND_BOOTS, Enchantment.PROTECTION_FALL, 4)));
	}
}
