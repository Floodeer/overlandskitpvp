package com.overlands.kitpvp.game.classes.normal;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import com.google.common.collect.Lists;
import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.item.EntityItemFactory;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.effects.ClassEffectType;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;
import com.overlands.kitpvp.sound.SoundManager;

import de.slikey.effectlib.EffectType;
import de.slikey.effectlib.effect.ConeEffect;
import de.slikey.effectlib.util.DynamicLocation;
import de.slikey.effectlib.util.ParticleEffect;

public class Cryomancer extends GameClass {

	public Cryomancer() {
		super(ClassType.CRYOMANCER, "Cryomancer", Main.classConfig.cryomancerPrice, Main.classConfig.cryomancerEPH, Main.classConfig.cryomancerEPS);
	}

	@Override
	public boolean onInteract(Player player) {
		Location localLocation1 = player.getEyeLocation().clone();
		localLocation1.setPitch(0.0F);

		ConeEffect c1 = new ConeEffect(Main.get().effectManager);
		c1.setDynamicOrigin(new DynamicLocation(localLocation1));
		c1.particle = ParticleEffect.DRIP_WATER;
		c1.type = EffectType.REPEATING;
		c1.radiusGrow = 0.016F;
		c1.lengthGrow = 0.04F;
		c1.particles = 130;
		c1.run();

		ConeEffect c2 = new ConeEffect(Main.get().effectManager);
		c2.setDynamicOrigin(new DynamicLocation(localLocation1));
		c2.particle = ParticleEffect.SNOW_SHOVEL;
		c2.type = EffectType.REPEATING;
		c2.radiusGrow = 0.016F;
		c2.lengthGrow = 0.04F;
		c2.particles = 130;
		c2.speed = 0.1F;
		c2.run();
		
		SoundManager.CRYOMANCER.playForAll(player.getLocation(), 1.0F, 1.0F);
		
		List<Location> loc = Lists.newArrayList();
		for (int i = 0; i < 5; i++) {
			Location l= player.getEyeLocation();
			l.setPitch(0.0F);
			loc.add(l.add(l.getDirection().multiply(i)));
		}
		
		List<Player> damaged = Lists.newArrayList();
		loc.forEach(location -> {
			Util.filterNearby(player, location, 3D).stream().filter(p -> !damaged.contains(p)).forEach(p -> {
				if(ClassEffectType.COLD.hasSelected(player)) {
					 for (int i = 0; i < 3; i++) {
						 EntityItemFactory.dropTemporary(p.getLocation().clone().add(0, 1, 0), ItemFactory.create(Material.ICE), 50);
					 }
				}
					 
				damaged.add(p);
			    ClassDamage.handle(player, p, getType());
			    Util.addEffect(p, PotionEffectType.SLOW, 3*20, 1);
			    com.overlands.core.utils.particles.ParticleEffect.SPELL_WITCH.display(0.1F, 0.5F, 0.1F, 1.0F, 15, p.getEyeLocation(), 126);
			});
		});
		
		return true;
	}
	
	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()),  ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.IRON_SWORD, MessageUtil.color("&cFreezing Breath &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);
		
		player.getInventory().setItem(0, sword);
		
		player.getInventory().setHelmet(ItemFactory.anUnbreakable(ItemFactory.create(Material.GOLD_HELMET, Enchantment.PROTECTION_ENVIRONMENTAL, 2)));
		player.getInventory().setChestplate(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_CHESTPLATE)));
		player.getInventory().setLeggings(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_LEGGINGS)));
		player.getInventory().setBoots(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_BOOTS)));
	}
}
