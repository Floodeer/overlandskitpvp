package com.overlands.kitpvp.game.classes.normal;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Sounds;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;

public class Spider extends GameClass {

	public Spider() {
		super(ClassType.SPIDER, "Spider", Main.classConfig.spiderPrice, Main.classConfig.spiderEPH, Main.classConfig.spiderEPS);
	}

	@Override
	public boolean onInteract(Player player) {
		player.setVelocity(player.getLocation().getDirection().multiply(1.25));
		player.setVelocity(new Vector(player.getVelocity().getX(), 1.25D, player.getVelocity().getZ()));
		player.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 6 * 20, 1));
		return true;
	}
	
	@EventHandler
	public void onFallDamage(EntityDamageEvent e) {
		if(!(e.getEntity() instanceof Player))
			return;
		if(e.getCause() != DamageCause.FALL)
			return;
		
		Player player = (Player)e.getEntity();
		GamePlayer gp = GamePlayer.get(player);
		
		if(!gp.getSelectedClass().getName().equalsIgnoreCase(getName()))
			return;
		
		if(!gp.canTakeFallDamage())
			return;
		
		
		e.setDamage(e.getDamage()/2);
		Util.filterNearby(player, player.getLocation(), 5.0).forEach(cur -> {
			ClassDamage.handle(player, cur, getType());
		});
		
		Location loc = player.getLocation().add(0.0D, 2.0D, 0.0D);
		ParticleEffect.LAVA.display(0.0F, 0.0F, 0.0F, 1.0F, 10, loc, 120);
		player.getWorld().playSound(player.getLocation(), Sounds.EXPLODE.bukkitSound(), 3.0F, 0.0F);
        ParticleEffect.EXPLOSION_HUGE.display(0.0F, 0.0F, 0.0F, 1.0F, 1, player.getLocation(), 15 * 3);

		FallingBlock fblock = (loc).getWorld().spawnFallingBlock(loc, Material.WEB, (byte) 0);
		FallingBlock fblock1 = (loc).getWorld().spawnFallingBlock(loc, Material.WEB, (byte) 0);
		FallingBlock fblock2 = (loc).getWorld().spawnFallingBlock(loc, Material.WEB, (byte) 0);
		FallingBlock fblock3 = (loc).getWorld().spawnFallingBlock(loc, Material.WEB, (byte) 0);

		FallingBlock fblock4 = (loc).getWorld().spawnFallingBlock(loc, Material.WEB, (byte) 0);
		FallingBlock fblock5 = (loc).getWorld().spawnFallingBlock(loc, Material.WEB, (byte) 0);
		FallingBlock fblock6 = (loc).getWorld().spawnFallingBlock(loc, Material.WEB, (byte) 0);
		FallingBlock fblock7 = (loc).getWorld().spawnFallingBlock(loc, Material.WEB, (byte) 0);

		double d1 = 0.4D;
		double d2 = 0.3D;

		fblock.setVelocity(new Vector(d1, 0.0D, 0.0D));
		fblock1.setVelocity(new Vector(0.0D, 0.0D, d1));
		fblock2.setVelocity(new Vector(-d1, 0.0D, 0.0D));
		fblock3.setVelocity(new Vector(0.0D, 0.0D, -d1));

		fblock4.setVelocity(new Vector(d2, 0.0D, d2));
		fblock5.setVelocity(new Vector(-d2, 0.0D, d2));
		fblock6.setVelocity(new Vector(d2, 0.0D, -d2));
		fblock7.setVelocity(new Vector(-d2, 0.0D, -d2));

		FallingBlock[] all = { fblock, fblock1, fblock2,fblock3, fblock4, fblock5, fblock6,fblock7 };
		for (FallingBlock blocks : all) {
			blocks.setMetadata("spider", new FixedMetadataValue(Main.get(), true));
			blocks.setDropItem(false);
		}
	}
	
	@EventHandler
	public void onEntityChangeBlock(final EntityChangeBlockEvent e) {

	    if (((e.getEntity() instanceof FallingBlock)) && 
	      (e.getEntity().hasMetadata("spider"))) {
	    	e.setCancelled(true);
	    } 
	}
	
	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()),  ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.IRON_SWORD, MessageUtil.color("&cDrop Shock &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);
		
		player.getInventory().setItem(0, sword);
		
		player.getInventory().setHelmet(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_HELMET)));
		player.getInventory().setChestplate(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_CHESTPLATE)));
		player.getInventory().setLeggings(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_LEGGINGS)));
		player.getInventory().setBoots(ItemFactory.anUnbreakable(ItemFactory.create(Material.DIAMOND_BOOTS, Enchantment.PROTECTION_FALL, 1)));
	}
}
