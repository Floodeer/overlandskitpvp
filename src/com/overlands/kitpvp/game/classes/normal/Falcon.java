package com.overlands.kitpvp.game.classes.normal;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Sounds;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;

public class Falcon extends GameClass {

	public Falcon() {
		super(ClassType.FALCON, "Falcon", Main.classConfig.falconPrice, Main.classConfig.falconEPH, Main.classConfig.falconEPS);
	}
	
	private List<Player> players = new ArrayList<>();

	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		if ((!(event.getDamager() instanceof Player))
				|| (!(event.getEntity() instanceof Player))) {
			return;
		}
		Player damager = (Player) event.getDamager();
		Player target = (Player) event.getEntity();
		if ((!players.contains(damager))) {
			return;
		}
		
		if(!Util.canBeDamaged(damager, target))
			return;
		
		for (int i = 0; i < 40; i++) {
			Location loc = target.getLocation();
			loc.add((MathUtils.random.nextDouble() - 0.5D) * 1.25D, 0.0D,(MathUtils.random.nextDouble() - 0.5D) * 1.25D);
			ParticleEffect.FIREWORKS_SPARK.display(new Vector(0, 1, 0), MathUtils.random.nextFloat(), loc, 120);
		}
		damager.getWorld().playSound(target.getLocation(), Sounds.EXPLODE.bukkitSound(), 1.0F, 0.7F);
		damager.getWorld().playSound(target.getLocation(), Sounds.IRONGOLEM_THROW.bukkitSound(), 1.0F,1.0F);

		damager.removePotionEffect(PotionEffectType.SPEED);
		players.remove(damager);

		assaultAndBattery(damager, target);
	}

	@Override
	public boolean onInteract(Player p) {
		if (players.contains(p)) {
			p.sendMessage(MessageUtil.color("&cHabilidade já carregada!"));
			return false;
		}

		p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 200, 1));
		p.getWorld().playSound(p.getLocation(), Sounds.FIREWORK_LAUNCH.bukkitSound(), 1.0F, 1.0F);
		GamePlayer.get(p).setCanGainEnergy(false);
		players.add(p);
		new BukkitRunnable() {
			int step = 0;
			boolean highPitch = true;
			int step2 = 4;
			@Override
			public void run() {
				if ((!p.isOnline()) || (!players.contains(p))) {
					if(GamePlayer.get(p) != null)
						GamePlayer.get(p).setCanGainEnergy(true);
					cancel();
					return;
				}else if(GamePlayer.get(p).isEMPd()) {
					p.removePotionEffect(PotionEffectType.SPEED);
					players.remove(p);
					cancel();
					return;
				}
				step2 += 1;
				if (step2 == 5) {
					p.getWorld().playSound(p.getLocation(), Sounds.NOTE_PIANO.bukkitSound(), 0.5F,highPitch ? 1.85F : 1.7F);
					highPitch = (!highPitch);
					step2 = 0;
				}
				step += 1;
				if (step <= 100) {
					ParticleEffect.FIREWORKS_SPARK.display(0.2F, 0.3F, 0.2F, 0.0F, 1,p.getLocation().clone().add(0.0D, 1.0D, 0.0D),20);
					ParticleEffect.SNOW_SHOVEL.display(0.2F, 0.3F, 0.2F, 0.1F, 2,p.getLocation().clone().add(0.0D, 1.0D, 0.0D),20);
				} else {
					p.removePotionEffect(PotionEffectType.SPEED);
					players.remove(p);
				}
			}
		}.runTaskTimer(Main.get(), 0L, 1L);
		
		return true;

	}

	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()),ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.IRON_SWORD, MessageUtil.color("&cFalcon Punch &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);

		player.getInventory().setItem(0, sword);

		player.getInventory().setHelmet(ItemFactory.anUnbreakable(ItemFactory.create(Material.DIAMOND_HELMET)));
		player.getInventory().setChestplate(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_CHESTPLATE)));
		player.getInventory().setLeggings(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_LEGGINGS)));
		player.getInventory().setBoots(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_BOOTS, Enchantment.PROTECTION_ENVIRONMENTAL, 1)));
	}
	
	private void assaultAndBattery(final Player p, final Player target) {
		new BukkitRunnable() {
			int step = 0;
			Location pLoc = p.getLocation();
			Location leLoc = target.getLocation();

			public void run() {
				step += 1;
				if (step <= 10) {
					pLoc.add(0.0D, 0.5D, 0.0D);
					leLoc.add(0.0D, 0.6D, 0.0D);
					p.teleport(pLoc);
					target.teleport(leLoc);
					return;
				}
				p.setVelocity(new Vector(0.0D, -0.7D, 0.0D));
				target.setVelocity(new Vector(0, -1, 0));
				if ((target.isOnGround()) || (p.isOnGround())) {
					ParticleEffect.EXPLOSION_LARGE.display(0.4F, 0.4F, 0.4F, 1.0F, 20, target.getLocation(),125);
					ParticleEffect.FIREWORKS_SPARK.display(0.5F, 0.5F, 0.5F, 0.5F, 200, target.getLocation(), 125);
					p.getWorld().playSound(target.getLocation(), Sounds.EXPLODE.bukkitSound(), 1.0F, 0.5F);
					for (Block b : LocationUtils.getCube(target.getLocation(), 2)) {
						if ((!b.isEmpty()) && (!b.isLiquid())&& (b.getLocation().getY() < target.getLocation().getY())&& (b.getLocation().getY() > target.getLocation().getY() - 2.0D)) {
							b.getWorld().playEffect(b.getLocation(), Effect.STEP_SOUND,b.getType(), 2);
						}
					}
					ClassDamage.handleFalse(p, target, getType());
					cancel();
				}
			}
		}.runTaskTimer(Main.get(), 0L, 1L);
	}
}
