package com.overlands.kitpvp.game.classes.normal;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Sounds;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.classes.ClassDamage;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;

public class Creeper extends GameClass {	

	public Creeper() {
		super(ClassType.CREEPER, "Creeper", 0, Main.classConfig.creeperEPH, Main.classConfig.creeperEPS);
	}

	@Override
	public boolean onInteract(Player player) {
		new BukkitRunnable() {
			int step = 3;

			@Override
			public void run() {
				if (player != null && player.isOnline() && !GamePlayer.get(player).isEMPd()) {
					--this.step;
					if (this.step >= 0) {
						ParticleEffect.EXPLOSION_LARGE.display(0.0F, 0.0F, 0.0F, 0.3F, 5, player.getLocation(), 16);
						ParticleEffect.VILLAGER_ANGRY.display(0.2F, 0.5F, 0.2F, 1.0F, 5,player.getLocation().add(0.0D, 1.0D, 0.0D), 16);
						ParticleEffect.EXPLOSION_NORMAL.display(0.0F, 0.5F, 0.0F, 1.0F, 10,player.getLocation().add(0.0D, 1.0D, 0.0D), 16);
						ParticleEffect.SMOKE_LARGE.display(0.0F, 0.5F, 0.0F, 1.0F, 10,player.getLocation().add(0.0D, 1.0D, 0.0D), 16);
						player.getWorld().playSound(player.getLocation(), Sounds.CREEPER_HISS.bukkitSound(), 3.0F, 0.0F);
						player.sendMessage("§aExplodindo em " + step);
					} else {
						player.sendMessage("§c§lBOOM");
						player.getWorld().playSound(player.getLocation(), Sounds.EXPLODE.bukkitSound(), 3.0F, 0.0F);
						ParticleEffect.EXPLOSION_HUGE.display(0.0F, 0.0F, 0.0F, 1.0F, 1, player.getLocation(), 15 * 3);
						LocationUtils.getNearbyPlayers(player.getLocation(), 7.5).stream()
								.filter((cur) -> GamePlayer.get(cur).getTeam() != GamePlayer.get(player) && !cur.equals(player))
								.forEach((entity) -> {
									ClassDamage.handle(player, entity, getType());
								});
						cancel();
					}
				} else {
					this.cancel();
				}
			}
		}.runTaskTimer(Main.get(), 0L, 20L);
		
		return true;
	}
	
	@Override
	public void giveItems(Player player) {
		InventoryUtils.nullInventory(player);
		InventoryUtils.fill(InventoryUtils.toCraftInventory(player.getInventory()),  ItemFactory.create(Material.MUSHROOM_SOUP));
		ItemStack sword = ItemFactory.create(Material.IRON_SWORD, MessageUtil.color("&cDetonate &8(Clique-Direito)"));
		ItemFactory.unbreakable(sword);
		
		player.getInventory().setItem(0, sword);
		
		player.getInventory().setHelmet(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_HELMET)));
		player.getInventory().setChestplate(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_CHESTPLATE)));
		player.getInventory().setLeggings(ItemFactory.anUnbreakable(ItemFactory.create(Material.DIAMOND_LEGGINGS, Enchantment.PROTECTION_EXPLOSIONS, 3)));
		player.getInventory().setBoots(ItemFactory.anUnbreakable(ItemFactory.create(Material.IRON_BOOTS)));
	}
}
