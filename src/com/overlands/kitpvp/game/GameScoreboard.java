package com.overlands.kitpvp.game;

import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import com.overlands.core.utils.MessageUtil;
import com.overlands.kitpvp.game.levels.LevelFormatter;
import com.overlands.kitpvp.ranks.Rank;

public class GameScoreboard {

	private Game game;
	
	public GameScoreboard(Game game) {
		this.game = game;
	}
	
	public void registerHealthBar(Player player) {
		Scoreboard sb = player.getPlayer().getScoreboard();
		if(sb.getObjective(DisplaySlot.BELOW_NAME) == null) {
		  	sb.registerNewObjective("showhealth", "health");
			Objective objective = sb.getObjective("showhealth");
	    	objective.setDisplaySlot(DisplaySlot.BELOW_NAME);
	    	objective.setDisplayName(MessageUtil.color("&c") + "❤");
		}
	}
	
	public void registerHealthBar() {
		game.getPlayers().forEach(player -> {
			Scoreboard sb = player.getPlayer().getScoreboard();
			if(sb.getObjective(DisplaySlot.BELOW_NAME) == null) {
			  	sb.registerNewObjective("showhealth", "health");
				Objective objective = sb.getObjective("showhealth");
		    	objective.setDisplaySlot(DisplaySlot.BELOW_NAME);
		    	objective.setDisplayName(MessageUtil.color("&c") + "❤");
			}
		});
	}
	
	public void registerTeams(Player player) {
		game.getPlayers().stream().filter(p -> !p.equals(GamePlayer.get(player))).forEach(p -> {
			if(p.getPlayer().getScoreboard().getTeam(player.getPlayer().getName()) == null){
				
			    Scoreboard s = p.getPlayer().getScoreboard();

				s.registerNewTeam(p.getPlayer().getName());
				s.getTeam(p.getPlayer().getName()).setPrefix(MessageUtil.color("&8[&6" + GamePlayer.get(player).getClassTypeFromEnum().toString().substring(0, 3).toUpperCase() + "&8] &7"));
				s.getTeam(player.getName()).setSuffix(" " + LevelFormatter.format(GamePlayer.get(player).getLevel(), GamePlayer.get(player).getPrestigeLevel(), false, false));
			    player.getScoreboard().getTeam(player.getName()).addEntry(player.getName());
			}
		});
	}
	
	public void registerTeams() {
		game.getPlayers().stream().forEach(gp -> {
			Player p = gp.getPlayer();
			if(p.getPlayer().getScoreboard().getTeam(p.getPlayer().getName()) == null) {
				Scoreboard s = p.getPlayer().getScoreboard();
				s.registerNewTeam(p.getPlayer().getName());
				s.getTeam(p.getPlayer().getName()).setPrefix(MessageUtil.color("&8[&6" + GamePlayer.get(p).getClassTypeFromEnum().toString().substring(0, 3).toUpperCase() + "&8] &7"));
				s.getTeam(p.getName()).setSuffix(" " + LevelFormatter.format(GamePlayer.get(p).getLevel(), GamePlayer.get(p).getPrestigeLevel(), false, false));
			    s.getTeam(p.getName()).addEntry(p.getName());
			}
		});
	}
	
	public void updateTeams() {
		game.getPlayers().forEach(player -> {
			Player p = player.getPlayer();
			Scoreboard s = p.getPlayer().getScoreboard();
			
			s.registerNewTeam(p.getPlayer().getName());
			s.getTeam(p.getPlayer().getName()).setPrefix(MessageUtil.color("&8[&6" + GamePlayer.get(p).getClassTypeFromEnum().toString().substring(0, 3).toUpperCase() + "&8] &7"));
			s.getTeam(p.getName()).setSuffix(" " + LevelFormatter.format(GamePlayer.get(p).getLevel(), GamePlayer.get(p).getPrestigeLevel(), false, false));
		    s.getTeam(p.getName()).addEntry(p.getName());
		});
	}
	
	public void addPlayerToPlayerTeam(Player scoreboardOwner, Player p) {
		if(scoreboardOwner.getScoreboard() == null)
			return;
		
		Scoreboard s = scoreboardOwner.getScoreboard();
		if(s.getTeam(p.getPlayer().getName()) == null){
			s.registerNewTeam(p.getPlayer().getName());
			s.getTeam(p.getPlayer().getName()).setPrefix(MessageUtil.color("&8[&6" + GamePlayer.get(p).getClassTypeFromEnum().toString().substring(0, 3).toUpperCase() + "&8] &7"));
			s.getTeam(p.getName()).setSuffix(" " + LevelFormatter.format(GamePlayer.get(p).getLevel(), GamePlayer.get(p).getPrestigeLevel(), false, false));
		    s.getTeam(p.getName()).addEntry(p.getName());
		}else {
			s.getTeam(p.getName()).setPrefix(MessageUtil.color("&8[&6" + GamePlayer.get(p).getClassTypeFromEnum().toString().substring(0, 3).toUpperCase() + "&8] &7"));
		}
	}
	
	public void removePlayerFromPlayerTeam(Player scoreboardOwner, Player p) {
		if(scoreboardOwner.getScoreboard() == null)
			return;
		Scoreboard s = scoreboardOwner.getScoreboard();
		if(s.getTeam(p.getPlayer().getName()) != null){
			s.getTeam(p.getName()).setPrefix(MessageUtil.color("&8[&6" + GamePlayer.get(p).getClassTypeFromEnum().toString().substring(0, 3).toUpperCase() + "&8] &7"));		
		}
	}
	
	public void updateTeams(Player p) {
		game.getPlayers().forEach(gp -> {
			if(gp.getPlayer().getScoreboard() == null)
				return;
			
			Scoreboard s = gp.getPlayer().getScoreboard();
			if(s.getTeam(p.getPlayer().getName()) == null) {
				s.registerNewTeam(p.getPlayer().getName());
				s.getTeam(p.getPlayer().getName()).setPrefix(MessageUtil.color("&8[&6" + GamePlayer.get(p).getClassTypeFromEnum().toString().substring(0, 3).toUpperCase() + "&8] " + Rank.fromName(GamePlayer.get(p).getRank()).getColor()));
				s.getTeam(p.getName()).setSuffix(" " + LevelFormatter.format(GamePlayer.get(p).getLevel(), GamePlayer.get(p).getPrestigeLevel(), false, false));
			    s.getTeam(p.getName()).addEntry(p.getName());
			}else{
				s.getTeam(p.getPlayer().getName()).setPrefix(MessageUtil.color("&8[&6" + GamePlayer.get(p).getClassTypeFromEnum().toString().substring(0, 3).toUpperCase() + "&8] " + Rank.fromName(GamePlayer.get(p).getRank()).getColor()));
				s.getTeam(p.getName()).setSuffix(" " + LevelFormatter.format(GamePlayer.get(p).getLevel(), GamePlayer.get(p).getPrestigeLevel(), false, false));
			    s.getTeam(p.getName()).addEntry(p.getName());
			}
		});
	}
}
