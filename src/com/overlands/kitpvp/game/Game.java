package com.overlands.kitpvp.game;

import java.util.*;
import java.util.stream.Collectors;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.kitpvp.game.events.*;
import com.overlands.kitpvp.game.events.Package;
import com.overlands.kitpvp.game.levels.Prestige;
import com.overlands.kitpvp.listeners.ChatResult;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.util.Vector;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.overlands.core.damage.OverlandsDamageEvent;
import com.overlands.core.utils.FireworkUtils;
import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MenuUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Runner;
import com.overlands.core.utils.Sounds;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.block.BlockUtils;
import com.overlands.core.utils.bouncing.BouncingProjectile;
import com.overlands.core.utils.bouncing.BouncyBlock;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.calc.TimeUtils;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.core.utils.java.FinalReference;
import com.overlands.core.utils.particles.ParticleEffect;
import com.overlands.core.utils.scheduler.SchedulerEvent;
import com.overlands.core.utils.scheduler.UpdateType;
import com.overlands.core.utils.world.WorldBorderEffect;
import com.overlands.core.utils.world.WorldUtils;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.database.DataManager.TABLE_TYPE;
import com.overlands.kitpvp.effects.EffectSelector;
import com.overlands.kitpvp.event.OverlandsChatEvent;
import com.overlands.kitpvp.event.OverlandsPlayerKillTargetEvent;
import com.overlands.kitpvp.event.OverlandsPlayerLevelUpEvent;
import com.overlands.kitpvp.event.OverlandsPlayerPrestigeLevelUpEvent;
import com.overlands.kitpvp.game.GamePlayer.PlayerState;
import com.overlands.kitpvp.game.GamePlayer.PlayerState.State;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;
import com.overlands.kitpvp.game.classes.ToggledClass;
import com.overlands.kitpvp.game.levels.LevelFormatter;
import com.overlands.kitpvp.game.team.TeamManager;
import com.overlands.kitpvp.npc.NPCInteractEvent;
import com.overlands.kitpvp.npc.StatsNPC;
import com.overlands.kitpvp.perks.PerkSelector;
import com.overlands.kitpvp.perks.PerkType;
import com.overlands.kitpvp.ranks.Rank;

import net.citizensnpcs.api.event.NPCRightClickEvent;
import net.minecraft.server.v1_8_R3.EntityPlayer;

public class Game implements Listener {

    private Map<Player, Map<Integer, String>> scoreboardMap = Maps.newHashMap();
    private Map<Player, Integer> kills = Maps.newHashMap();
    public Map<ClassType, GameClass> gameClasses = Maps.newHashMap();

    private List<Player> playersToClear;
    private List<GamePlayer> gamePlayers;
    private List<Package> packages;
    private WorldBorderEffect ef;
    private boolean godMode;
    private boolean pvp;

    private GameEvent event;
    private int coinMultiplier = 1;

    private GameScoreboard hs;
    public String title = "  " + "OVERLANDS" + "  ";
    private int shineIndex;
    private boolean shineDirection = true;

    private boolean canHaveEvents;
    public static int step = 0;

    private Player target;

    public Game() {
        gamePlayers = Lists.newArrayList();
        packages = Lists.newArrayList();
        playersToClear = Lists.newArrayList();
        ef = new WorldBorderEffect();
        hs = new GameScoreboard(this);

        Arrays.stream(ClassType.values()).filter(clazz -> clazz.getClazz() != null).forEach(clazz -> {
            Class<? extends GameClass> zz = clazz.getClazz();
            if(zz.isAssignableFrom(ToggledClass.class))
                return;
            try {
                gameClasses.put(clazz, zz.newInstance());
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        });
    }

    @EventHandler
    public void onDamageTaken(EntityDamageEvent e) {
        if(e.getEntity() instanceof Player) {
            Player p = (Player)e.getEntity();
            if(e.getCause() == DamageCause.FALL) {
                if(!GamePlayer.get(p).canTakeFallDamage()){
                    e.setCancelled(true);
                    GamePlayer.get(p).setFallDamage(true);
                }
            }else if(e.getCause() == DamageCause.ENTITY_ATTACK) {
                if(!GamePlayer.get(p).canTakeFallDamage()){
                    GamePlayer.get(p).setFallDamage(true);
                }
            }
        }
    }

    @EventHandler
    public void onWorld(PlayerChangedWorldEvent e) {
        if(e.getPlayer().getWorld() == Bukkit.getWorlds().get(0)) {
            Runner.make(Main.get()).delay(20).run(() -> StatsNPC.spawnPlayerNPC(e.getPlayer()));
        }
    }

    @EventHandler
    public void onCmd(PlayerCommandPreprocessEvent e) {
        if(!e.getPlayer().isOp())
            e.setCancelled(true);
    }

    @EventHandler
    public void onMelt(BlockPhysicsEvent event) {
        if(event.getBlock().getType() == Material.SNOW || event.getBlock().getType() == Material.SNOW_BLOCK) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onMelt(BlockFadeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void fromTo(BlockFromToEvent event) {
        if(event.getBlock().getType() == Material.SNOW || event.getBlock().getType() == Material.SNOW_BLOCK) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onExplosion(EntityExplodeEvent event) {
        if (event.getLocation().getWorld().equals(Main.mapConfig.center.getWorld())) {
            if(!event.getEntity().hasMetadata("customDamaged")) {
                return;
            }
            event.setCancelled(true);
            Main.get().getExplosionManager().setRegenerate(true);
            Main.get().getExplosionManager().blockExplosion(BlockUtils.getInRadius(event.getLocation(), 4).keySet(), event.getLocation(), true);
        }
        if(event.getEntity() instanceof Fireball) {
            event.setCancelled(true);
        }
    }


    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityDamageTaken(EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof Player && e.getDamager() instanceof Player) {
            Player ent = (Player) e.getEntity();
            if(WorldUtils.inSpawn(ent.getLocation())) {
                e.setCancelled(true);
                return;
            }
            GamePlayer target = GamePlayer.get(ent);
            if (e.getDamager() instanceof Player) {
                Player entD = (Player) e.getDamager();
                GamePlayer damager = GamePlayer.get(entD);
                damager.setDamageDone(damager.getDamageDone()+e.getDamage());
                if(damager.getEnergy()+damager.getSelectedClass().getEPH() >= 100) {
                    damager.setEnergy(100);
                }else{
                    int EPH = damager.getSelectedClass().getEPH();
                    if(getEvent() != null && getEvent() instanceof Overclocked) {
                        EPH = EPH*3;
                    }
                    damager.setEnergy(damager.getEnergy() + EPH);
                }
                if (!damager.isInGame()) {
                    e.setCancelled(true);
                    return;
                }
                if (damager.getTeam() == target) {
                    e.setCancelled(true);
                    return;
                }

                for(GameTagged taggeds : target.getTaggedList()) {
                    if(taggeds.getPlayer().equals(damager)) {
                        target.getTaggedList().remove(taggeds);
                    }
                }

                target.getTaggedList().add(new GameTagged(damager, System.currentTimeMillis()));

                target.setDamageReceived(target.getDamageReceived()+e.getDamage());
                damager.setState(new PlayerState(damager, State.FIGHTING, 10));
                target.setState(new PlayerState(target, State.FIGHTING, 10));

            } else if (e.getDamager() instanceof Arrow) {
                Arrow a = (Arrow) e.getDamager();
                Player entD = (Player) a.getShooter();
                GamePlayer damager = GamePlayer.get(entD);
                if(damager.getEnergy()+damager.getSelectedClass().getEPH() >= 100) {
                    damager.setEnergy(100);
                }else{
                    damager.setEnergy(damager.getEnergy()+damager.getSelectedClass().getEPH());
                }
                if (!damager.isInGame()) {
                    e.setCancelled(true);
                    return;
                }
                if (damager.getTeam().equals(target.getTeam())) {
                    e.setCancelled(true);
                    return;
                }
                if(e.isCancelled())
                    return;

                damager.setDamageDone(damager.getDamageDone()+e.getDamage());
                damager.setBowHits(damager.getBowHits()+1);

                FinalReference<GameTagged> tagged = new FinalReference<GameTagged>(null);
                if(!target.getTaggedList().isEmpty()) {
                    target.getTaggedList().stream().filter(cur -> cur.getPlayer().equals(damager)).findAny().ifPresent((c -> tagged.set(c)));
                }
                if(tagged.get() != null) {
                    GameTagged taggeds = tagged.get();
                    if(taggeds.getPlayer() == damager) {
                        target.getTaggedList().remove(taggeds);
                    }
                }
                target.getTaggedList().addFirst(new GameTagged(damager, System.currentTimeMillis()));

                target.setDamageReceived(target.getDamageReceived()+e.getDamage());
                damager.setState(new PlayerState(damager, State.FIGHTING, 10));
                target.setState(new PlayerState(target, State.FIGHTING, 10));
            }
        }
    }

    @EventHandler
    public void onSpawn(OverlandsDamageEvent event) {
        if(WorldUtils.inSpawn(event.getTargetEntity().getLocation()))
            event.setCancelled("In Spawn");

        if(event.getTargetPlayer() == null)
            return;
        if(event.getDamagerPlayer(true) == null)
            return;

        Player player = event.getDamagerPlayer(true);
        Player target = event.getTargetPlayer();

        if(GamePlayer.get(player).getTeam() == GamePlayer.get(target))
            event.setCancelled("Team cancel");

        if(isPvPDisabled())
            event.setCancelled("Cancelled other");


        if(GamePlayer.get(player).getSelectedPerks().contains(PerkType.VAMPIRE.toString())) {
            Util.healPlayer(player, 1);
        }
    }

    @EventHandler
    public void registerSkillDamage(OverlandsDamageEvent event) {
        boolean validReason = Arrays.stream(ClassType.values())
                .map(ClassType::getSkillName)
                .anyMatch(name -> event.getReason() == null || event.getReason().isEmpty() || event.getReason().equalsIgnoreCase(name));

        if (!validReason)
            return;
        if(event.getTargetPlayer() == null)
            return;
        if(event.getDamagerPlayer(true) == null)
            return;

        Player player = event.getDamagerPlayer(true);
        Player target = event.getTargetPlayer();

        GamePlayer.get(player).setDamageDone(GamePlayer.get(player).getDamageDone()+event.getDamage());
        GamePlayer.get(target).setDamageReceived(GamePlayer.get(target).getDamageReceived()+event.getDamage());

        Hologram hd = HologramsAPI.createHologram(Main.get(), LocationUtils.randomizeLocation(target.getLocation()));
        hd.appendTextLine("&c-" + event.getDamage());
        Runner.make(Main.get()).delay(35).run(hd::delete);
    }

    @EventHandler
    public void registerBow(OverlandsDamageEvent event) {
        if(event.getProjectile() == null)
            return;
        if(!(event.getProjectile() instanceof Arrow))
            return;
        if(event.getTargetPlayer() == null)
            return;
        if(event.getDamagerPlayer(true) == null)
            return;
        if(event.isCancelled())
            return;

        Player shooter = event.getDamagerPlayer(true);
        Player targetE = event.getTargetPlayer();
        if(shooter.equals(targetE)) {
            event.setCancelled("Self damage");
            return;
        }

        GamePlayer damager = GamePlayer.get(shooter);
        GamePlayer target = GamePlayer.get(targetE);
        if (damager.getTeam() != null && target.getTeam() != null && damager.getTeam().equals(target.getTeam())) {
            event.setCancelled("Team damage");
            return;
        }

        damager.setBowHits(damager.getBowHits()+1);
        int EPH = damager.getSelectedClass().getEPH();
        if(getEvent() != null && getEvent() instanceof Overclocked) {
            EPH = EPH*3;
        }
        damager.setEnergy(Math.min(damager.getEnergy() + EPH, 100));

        damager.setDamageDone(damager.getDamageDone()+event.getDamage());
        damager.setBowHits(damager.getBowHits()+1);

        FinalReference<GameTagged> tagged = new FinalReference<>(null);
        if(!target.getTaggedList().isEmpty()) {
            target.getTaggedList().stream().filter(cur -> cur.getPlayer().equals(damager)).findAny().ifPresent((tagged::set));
        }
        if(tagged.get() != null) {
            GameTagged taggeds = tagged.get();
            if(taggeds.getPlayer() == damager) {
                target.getTaggedList().remove(taggeds);
            }
        }
        target.getTaggedList().addFirst(new GameTagged(damager, System.currentTimeMillis()));

        target.setDamageReceived(target.getDamageReceived()+event.getDamage());
        damager.setState(new PlayerState(damager, State.FIGHTING, 10));
        target.setState(new PlayerState(target, State.FIGHTING, 10));
    }

    @EventHandler
    public void onBowShoot(EntityShootBowEvent e) {
        if(e.getEntity() instanceof Player) {
            Player ent = (Player) e.getEntity();
            if(WorldUtils.inSpawn(ent.getLocation())) {
                e.setCancelled(true);
                return;
            }
            GamePlayer target = GamePlayer.get(ent);
            if(!e.isCancelled())
                target.setBowShots(target.getBowShots()+1);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onDeath(PlayerDeathEvent e) {
        e.setDeathMessage(null);
        e.getDrops().clear();
        e.setDroppedExp(0);
        final Player p = e.getEntity();
        if(p.hasMetadata("skinChanger"))
            return;

        final Location loc = e.getEntity().getLocation();
        final GamePlayer gp = Main.getPM().getPlayer(p.getUniqueId());
        if (gp.isInGame()) {
            gp.setEnergy(0);
        }
        gp.setScore(Math.max(gp.getScore() - 1, 0));
        gp.setState(new PlayerState(gp, State.SAFE, 0L));
        gp.setKillStreak(0);
        gp.setDeaths(gp.getDeaths()+1);
        gp.setShield(false);
        gp.setProtectionPercentage(0);

        Runner.make(Main.get()).delay(2).run(() -> Util.sendRespawnPacket(p));
        if(p.getKiller() != null) {
            Player killer = p.getKiller();
            GamePlayer cur = GamePlayer.get(killer);
            cur.setKillStreak(cur.getKillStreak()+1);
            cur.setKills(cur.getKills()+1);
            cur.setScore(cur.getScore()+1);
            cur.addBalance(10 * getCoinMultiplier(), true, "kill");
            cur.addExp(MathUtils.random(5,10) * getCoinMultiplier(), true, "kill");
            updateScoreboard(cur, 3, MessageUtil.color("&fStreak: &b" + cur.getKillStreak()));
            Bukkit.getPluginManager().callEvent(new OverlandsPlayerKillTargetEvent(p.getKiller(), p, loc));
            if(cur.getKillStreak() % 5 == 0) {
                Bukkit.broadcastMessage(MessageUtil.color("&c&lSTREAK! " + LevelFormatter.withPlayerNameIgnoringClass(killer) + " &6&lcom &b&l" + cur.getKillStreak() + " &6&lkills"));
            }

            if(target != null && target.equals(p)) {
                int x = MathUtils.random(5,10);
                if(cur.getSelectedPerks().toString().contains(PerkType.HEAD_HUNTER.toString())) {
                    x = (int)(x*(25/100.0f));
                }
                cur.addBalance(x * getCoinMultiplier() * kills.get(target), true, "eliminar o target");
                cur.addExp(x * getCoinMultiplier() * kills.get(target), true, "eliminar o target");
            }
        }
        kills.put(target, 0);
        setTarget(null);
        if (!gp.getTaggedList().isEmpty()) {
            if(p.getKiller() != null) {
                gp.getTaggedList().stream().filter(cur -> !cur.getPlayer().getPlayer().equals(p.getKiller())).forEach(cur -> {
                    if(System.currentTimeMillis() - cur.getTime() <= 25000) {
                        cur.getPlayer().addBalance(MathUtils.random(3, 8) * getCoinMultiplier(), true, "assistência");
                        cur.getPlayer().addExp(MathUtils.random(2, 7) * getCoinMultiplier(), true, "assistência");
                    }
                });
            }else{
                gp.getTaggedList().forEach(cur -> {
                    if(System.currentTimeMillis() - cur.getTime() <= 25000) {
                        cur.getPlayer().addBalance(MathUtils.random(3, 8) * getCoinMultiplier(), true, "assistência");
                        cur.getPlayer().addExp(MathUtils.random(2, 7) * getCoinMultiplier() , true, "assistência");
                    }
                });
            }
        }
        Runner.make(Main.get()).delay(5).run(() -> gp.getTaggedList().clear());
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent e) {
        if(e.getPlayer().hasMetadata("skinChanger"))
            return;
        GamePlayer gp = GamePlayer.get(e.getPlayer());
        if(!gp.respawnWithKit()) {
            gp.giveSpawnItems();
        }else {
            gp.getSelectedClass().giveItems(gp.getPlayer());
        }
        if(gp.isFastRespawn() || Main.getGame().getEvent() != null && Main.getGame().getEvent().getClass() == MysteryClasses.class) {
            e.setRespawnLocation(Main.mapConfig.respawn);
        }else {
            e.setRespawnLocation(Main.mapConfig.spawn);
        }
        if(Main.getGame().getEvent() != null && Main.getGame().getEvent().getClass().getName().startsWith("Mystery")) {
            MysteryClasses.giveRandomClass(gp);
        }
        StatsNPC.spawnPlayerNPC(e.getPlayer());
    }

    @EventHandler
    public void handlePerks(OverlandsPlayerKillTargetEvent e) {
        GamePlayer killer = GamePlayer.get(e.getPlayer());
        if (killer.getSelectedPerks().contains(PerkType.BUFF.toString())) {
            Util.addEffect(e.getPlayer(), PotionEffectType.REGENERATION, 4 * 20, 1);

        } else if (killer.getSelectedPerks().contains(PerkType.KILLER.toString())) {
            Util.addEffect(e.getPlayer(), PotionEffectType.INCREASE_DAMAGE, 3 * 20, 0);

        } else if (killer.getSelectedPerks().contains(PerkType.SHARPNESS.toString())) {
            Arrays.stream(e.getPlayer().getInventory().getArmorContents()).forEach(i -> {
                if (ItemFactory.isSword(i)) {
                    if (i.hasItemMeta() && i.getItemMeta().hasEnchants() && i.getEnchantments().containsKey(Enchantment.DAMAGE_ALL)) {
                        if (i.getEnchantmentLevel(Enchantment.DAMAGE_ALL) >= 3)
                            return;
                        ItemFactory.applyEnchantment(i, Enchantment.DAMAGE_ALL, i.getEnchantmentLevel(Enchantment.DAMAGE_ALL) + 1);
                    } else {
                        ItemFactory.applyEnchantment(i, Enchantment.DAMAGE_ALL, 1);
                    }
                }
            });
        } else if (killer.getSelectedPerks().contains(PerkType.TANKER.toString())) {
            Util.addEffect(e.getPlayer(), PotionEffectType.DAMAGE_RESISTANCE, 4 * 20, 1);

        } else if (killer.getSelectedPerks().contains(PerkType.EXTRA.toString())) {
            Util.addEffect(e.getPlayer(), PotionEffectType.ABSORPTION, 14 * 20, 2);

        } else if (killer.getSelectedPerks().contains(PerkType.REFIL.toString())) {
            int adds = 0;
            while (!InventoryUtils.isFull(InventoryUtils.toCraftInventory(e.getPlayer().getInventory()))) {
                e.getPlayer().getInventory().setItem(e.getPlayer().getInventory().firstEmpty(), ItemFactory.create(Material.MUSHROOM_SOUP));
                ++adds;
                if (adds == 2) {
                    break;
                }
            }
        }
    }


    @EventHandler
    public void onMove(SchedulerEvent e) {
        if(e.getType() == UpdateType.TICK) {
            step += 1;
            if (step >= 3000) {
                step = 0;
            }
            Bukkit.getOnlinePlayers().forEach(p -> {
                if (p.getLocation().getBlock().getType() == Material.STONE_PLATE) {
                    p.setVelocity(p.getEyeLocation().getDirection().multiply(MathUtils.randomRange(1.5, 5.2)));
                    p.setVelocity(new Vector(p.getVelocity().getX(), 1.3D, p.getVelocity().getZ()));
                    GamePlayer.get(p).setFallDamage(false);
                    p.playSound(p.getLocation(), Sound.WITHER_SHOOT, 10.0F, 10.0F);
                    Runner.make(Main.get()).delay(5).run(() -> p.setFallDistance(0));
                }else if (p.getLocation().getBlock().getType() == Material.GOLD_PLATE) {
                    p.setVelocity(p.getLocation().getDirection().multiply(2.5));
                    p.setVelocity(new Vector(p.getVelocity().getX(), 0.6D, p.getVelocity().getZ()));
                    GamePlayer.get(p).setFallDamage(false);
                    p.playSound(p.getLocation(), Sound.WITHER_SHOOT, 10.0F, 10.0F);
                    Runner.make(Main.get()).delay(5).run(() -> p.setFallDistance(0));

                }else if (p.getLocation().getBlock().getType() == Material.ENDER_PORTAL_FRAME) {
                    p.setVelocity(new Vector(p.getVelocity().getX(), 5.0D, p.getVelocity().getZ()));
                    p.playSound(p.getLocation(), Sound.WITHER_SHOOT, 10.0F, 10.0F);
                    GamePlayer.get(p).setFallDamage(false);
                    Runner.make(Main.get()).delay(5).run(() -> p.setFallDistance(0));
                }
            });
        }
        /**
         * if(e.getType() == UpdateType.MIN_16) {
         if(Bukkit.getOnlinePlayers().size() > 0) {
         int r = MathUtils.random(0, 3);
         if(canHaveEvents || event == null) {
         if(r == 0) {
         new MeteorEvent();
         }else if(r == 1) {
         new PartyEvent();
         }else if(r == 2) {
         new BossEvent();
         }else if(r == 3) {
         new BleedingStar();
         }else if(r == 4) {
         new RaffleEvent();
         }
         }
         }
         }else if(e.getType() == UpdateType.MIN_04) {
         if(Bukkit.getOnlinePlayers().size() > 0) {
         if(event == null) {
         if(MathUtils.randomBoolean(MathUtils.random(1, 99))) {
         if(packages.isEmpty()) {
         Package pack = new Package();
         packages.add(pack);
         }else {
         packages.get(0).stop();
         }
         }
         }
         }
         */
    }


    @EventHandler
    public void handleInteract(PlayerInteractEvent e) {
        if (e.getAction() != Action.RIGHT_CLICK_AIR && e.getAction() != Action.RIGHT_CLICK_BLOCK)
            return;
        final Player p = e.getPlayer();
        final GamePlayer gp = Main.getPM().getPlayer(p.getUniqueId());
        if (gp.isInGame()) {
            if(BlockUtils.usable(e.getClickedBlock())) {
                e.setCancelled(true);
                return;
            }
            if(ItemFactory.isSword(e.getItem())) {
                if (gp.getEnergy() >= 100 && !WorldUtils.inSpawn(p.getLocation()) && !gp.isEMPd()) {
                    if (gp.getSelectedClass().onInteract(e.getPlayer())) {
                        if(!isGodMode()) {
                            if(gp.getSelectedClass().getType() != ClassType.LANA) {
                                gp.setEnergy(0);
                                gp.getPlayer().setLevel(0);
                                gp.getPlayer().setExp(0);
                            }
                        }
                        gp.setSkillUsed(gp.getSkillUsed()+1);
                    }
                } else {
                    p.playSound(p.getLocation(), Sounds.NOTE_BASS_DRUM.bukkitSound(), 1f, 0.3f);
                }
            }
            if(ItemFactory.isMat(e.getItem(), Material.MUSHROOM_SOUP)) {
                if(e.getPlayer().getHealth() < 20) {
                    if(gp.getSelectedPerks().contains(PerkType.VAMPIRE.toString())) {
                        Util.healPlayer(e.getPlayer(), 1);
                    }else if(gp.getSelectedPerks().contains(PerkType.BETTER_SOUP.toString())) {
                        Util.addEffect(p, PotionEffectType.REGENERATION, 3*20, 2);
                        Util.healPlayer(e.getPlayer(), 7);
                    }else{
                        Util.healPlayer(e.getPlayer(), 6);
                    }
                    e.getPlayer().getItemInHand().setType(Material.BOWL);
                    e.getPlayer().updateInventory();
                }
            }
            if(ItemFactory.hasName(e.getItem(), MessageUtil.color("&7Classes"))) {
                Main.getClassSelector().show(p);
            }else if(ItemFactory.hasName(e.getItem(), MessageUtil.color("&7Preferências"))) {
                Main.getPreferences().show(GamePlayer.get(p));
            }else if(ItemFactory.hasName(e.getItem(), MessageUtil.color("&7Perfil"))) {
                Main.getProfile().open(p);
            }else if(ItemFactory.hasName(e.getItem(), MessageUtil.color("&7Perks"))) {
                PerkSelector.show(e.getPlayer());
            }else if(ItemFactory.hasName(e.getItem(), MessageUtil.color("&7Efeitos"))) {
                EffectSelector.show(e.getPlayer());
            }else if(ItemFactory.hasName(e.getItem(), MessageUtil.color("&7Prestige"))) {
                Prestige.open(gp);
            }
        }

        if(ItemFactory.isMat(e.getItem(), Material.DIAMOND_BARDING)) {
            Snowball snow = e.getPlayer().launchProjectile(Snowball.class);
            snow.setMetadata("bouncing", new FixedMetadataValue(Main.get(), 0));
            BouncingProjectile.playEffect(snow, ParticleEffect.FLAME);

        }else if(ItemFactory.isMat(e.getItem(), Material.GOLD_BARDING)) {
            BouncyBlock b = new BouncyBlock(Material.ENDER_STONE, 12, Main.get());
            b.spawn(e.getPlayer().getLocation());
            b.setEffect(true);
        }
    }

    @EventHandler
    public void handleBowInteract(PlayerInteractEvent e) {
        if (e.getAction() != Action.LEFT_CLICK_AIR && e.getAction() != Action.LEFT_CLICK_BLOCK)
            return;
        final Player p = e.getPlayer();
        final GamePlayer gp = Main.getPM().getPlayer(p.getUniqueId());
        if (gp.isInGame()) {
            if(BlockUtils.usable(e.getClickedBlock())) {
                e.setCancelled(true);
                return;
            }
            if(ItemFactory.isBow(e.getItem())) {
                if (gp.getEnergy() >= 100 && !WorldUtils.inSpawn(p.getLocation())) {
                    if (gp.getSelectedClass().onInteract(e.getPlayer())) {
                        if(!isGodMode()) {
                            gp.setEnergy(0);
                            gp.getPlayer().setLevel(0);
                            gp.getPlayer().setExp(0);
                        }
                        gp.setSkillUsed(gp.getSkillUsed()+1);
                    }
                } else {
                    p.playSound(p.getLocation(), Sounds.NOTE_BASS_DRUM.bukkitSound(), 1f, 0.3f);
                }
            }
        }
    }

    @EventHandler
    public void drop(PlayerDropItemEvent e) {
        if(GamePlayer.get(e.getPlayer()).isInGame()) {
            if(e.getItemDrop().getItemStack().getType() == Material.BOWL) {
                e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ITEM_PICKUP, 1.0F, 1.0F);
                Runner.make(Main.get()).delay(10).run(() -> {
                    e.getItemDrop().remove();
                    e.getPlayer().updateInventory();
                });
            }else{
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onInventory(InventoryClickEvent e) {
        if(GamePlayer.get((Player)e.getWhoClicked()).isInGame()) {
            if(e.getInventory().getType() == InventoryType.PLAYER) {
                if(e.getRawSlot() >= 5 && e.getRawSlot() <= 8) {
                    e.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onPickup(PlayerPickupItemEvent e) {
        if(GamePlayer.get(e.getPlayer()).isInGame()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void food(FoodLevelChangeEvent e) {
        ((Player)e.getEntity()).setFoodLevel(20);
        e.setCancelled(true);
    }

    @EventHandler
    public void energyGain(SchedulerEvent event) {
        if (event.getType() == UpdateType.SEC) {
            Main.getPM().getPlayers().forEach(gp -> {
                if(gp.isInGame() && gp.getSelectedClass() != null && !WorldUtils.inSpawn(gp.getPlayer().getLocation())) {
                    if(gp.getState().getState() == State.FIGHTING) {
                        gp.getState().setTime(gp.getState().getTime()-1);
                        if(gp.getState().getTime() <= 4)
                            gp.getState().setOut(true);
                    }
                    String combatCheck = gp.getState().getName() + (gp.getState().getState() == State.FIGHTING && gp.getState().isOut() ?  "&c(" + gp.getState().getTime() + ")" : "");
                    if (gp.getEnergy() < 100) {
                        if(gp.canGainEnergy()) {
                            int EPS = gp.getSelectedClass().getEPS();
                            if(getEvent() != null && getEvent() instanceof Overclocked) {
                                EPS = EPS*3;
                            }
                            gp.setEnergy(Math.min(gp.getEnergy() + EPS, 100));
                        }

                    }
                    updateScoreboard(gp, 4, MessageUtil.color(combatCheck));
                }
            });
        } else if (event.getType() == UpdateType.TICK) {
            Main.getPM().getPlayers().forEach(gp -> {
                if(gp.isInGame() && !WorldUtils.inSpawn(gp.getPlayer().getLocation())) {
                    if (gp.getEnergy() >= 100) {
                        gp.setEnergy(100);
                    }else{
                        gp.getPlayer().setExp((float) (gp.getEnergy() % 100) / 100);
                    }
                    gp.getPlayer().setLevel(gp.getEnergy());
                    if(gp.getEnergy() >= 100) {
                        if(Main.getEnergyManager().use(gp.getPlayer(), "skillReady", 18000, false, false)) {
                            if(gp.isWarnings()) {
                                gp.getPlayer().sendMessage(MessageUtil.color("&aSua habilidade &b&l" + gp.getClassTypeFromEnum().getSkillName() + " &aestá pronta para ser usada!"));
                                gp.getPlayer().sendMessage(MessageUtil.color("&bClique-Direito &aem sua espada para ativar a habilidade!"));
                            }
                        }
                    }
                    if (gp.isSpecialEffects()) {
                        if (gp.getPlayer().getHealth() <= 8.5) {
                            if (gp.getPlayer().getHealth() <= 6 && gp.getPlayer().getHealth() >= 4) {
                                ef.setBorder(gp.getPlayer(), 100);
                            } else if (gp.getPlayer().getHealth() <= 6 && gp.getPlayer().getHealth() >= 4) {
                                ef.setBorder(gp.getPlayer(), 75);
                            } else {
                                ef.setBorder(gp.getPlayer(), 1);
                            }
                        } else {
                            ef.removeBorder(gp.getPlayer());
                        }
                    }
                    if(getEvent() != null) {
                        gp.setState(new PlayerState(gp, State.EVENT, 0));
                    }else{
                        if(gp.getState().getState() == State.FIGHTING) {
                            if(gp.getState().getTime() == 1) {
                                gp.setState(new PlayerState(gp, State.SAFE, 0L));
                            }
                        }
                    }
                }else{
                    gp.setState(new PlayerState(gp, State.SAFE, 0L));
                }
            });
        } else if (event.getType() == UpdateType.FAST) {
            Main.getPM().getPlayers().forEach(gp -> {
                if(gp.isInGame() && !WorldUtils.inSpawn(gp.getPlayer().getLocation())) {
                    if (gp.getEnergy() == 100) {
                        if (gp.getPlayer().getExp() == 0.99f) {
                            gp.getPlayer().setExp(0);
                        } else {
                            gp.getPlayer().setExp(0.99f);
                        }
                    }
                }else{
                    gp.setEnergy(0);
                    gp.getPlayer().setExp(0);
                    gp.getPlayer().setLevel(0);
                }
            });
        } else if (event.getType() == UpdateType.FASTEST) {

            String o = getString();
            getPlayers().forEach(gp -> {
                Player p = gp.getPlayer();
                if (p.getScoreboard() != null && p.getScoreboard().getObjective(DisplaySlot.SIDEBAR) != null)
                    p.getScoreboard().getObjective(DisplaySlot.SIDEBAR).setDisplayName(o);
            });

            this.shineIndex += 1;
            if (this.shineIndex == this.title.length() * 2) {
                this.shineIndex = 0;
                this.shineDirection = (!this.shineDirection);
            }
        }
    }

    private String getString() {
        String out;
        if (this.shineDirection) {
            out = Util.cGold + Util.Bold;
        } else {
            out = Util.cWhite + Util.Bold;
        }
        for (int i = 0; i < this.title.length(); i++) {
            char c = this.title.charAt(i);
            if (this.shineDirection) {
                if (i == this.shineIndex) {
                    out = out + Util.cYellow + Util.Bold;
                }
                if (i == this.shineIndex + 1) {
                    out = out + Util.cWhite + Util.Bold;
                }
            } else {
                if (i == this.shineIndex) {
                    out = out + Util.cYellow + Util.Bold;
                }
                if (i == this.shineIndex + 1) {
                    out = out + Util.cGold + Util.Bold;
                }
            }
            out = out + c;
        }

        String o = out;
        return o;
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onJoin(PlayerJoinEvent e) {
        e.setJoinMessage(null);
        Player player = e.getPlayer();
        String uuid = player.getUniqueId().toString();

        Runner.make(Main.get()).delay(1).run(() -> {
            Main.getPM().addPlayer(player.getUniqueId());
            Runner.make(Main.get()).delay(1).run(() -> Arrays.stream(TABLE_TYPE.values()).filter(type -> type != TABLE_TYPE.HUNTER_PLAYER).forEach(table -> {
                Main.getDataManager().loadPlayer(uuid, TABLE_TYPE.PLAYER_DATA);
                Main.getDataManager().loadPlayer(uuid, TABLE_TYPE.PLAYER_INFO);
                Main.getDataManager().loadPlayer(uuid, TABLE_TYPE.PLAYER_PROFILE);
            }));
            e.getPlayer().getPlayer().teleport(Main.mapConfig.spawn);
        });
        Runner.make(Main.get()).delay(10).run(() -> {
            GamePlayer gp = GamePlayer.get(player);
            gp.setEnergy(0);
            addPlayer(gp);
            StatsNPC.spawnPlayerNPC(player);
            gp.setPacketInjector();
            if(getEvent() instanceof Overclocked) {
                gp.getPlayer().setMaxHealth(40);
            }else if(getEvent() instanceof BleedingStar) {
                gp.getPlayer().setMaxHealth(18);
            }else{
                gp.getPlayer().setMaxHealth(20);
            }
        });
    }

    @EventHandler
    public void onChat(OverlandsChatEvent event) {
        Player p = event.getPlayer();
        if(event.getResult() == ChatResult.DENY_SPAM) {
            p.sendMessage(MessageUtil.color("&9Aguarde para executar o comando novamente."));
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onNPC(NPCInteractEvent e) {
        for(EntityPlayer entPlayer : StatsNPC.perPlayerPacket.values()) {
            if(e.getEntityID() == entPlayer.getId()) {
                Main.getProfile().open(e.getPlayer());
            }
        }
    }

    @EventHandler
    public void onCitizens(NPCRightClickEvent e) {
        Main.mapConfig.npcsCommands.keySet().stream().map(Integer::parseInt).forEach(i -> {
            if (i == e.getNPC().getId()) {
                e.getClicker().chat(Main.mapConfig.npcsCommands.get(Integer.toString(i)));
            }
        });
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        e.setQuitMessage(null);
        if(Main.getPM().getUUIDs().contains(e.getPlayer().getUniqueId())) {
            GamePlayer gp = GamePlayer.get(e.getPlayer());
            if(gp.getTeam() != null) {
                gp.getTeam().getPlayer().sendMessage(MessageUtil.color("&eSua dupla saiu do servidor, time desfeito."));
                TeamManager.disband(gp,false);
            }
            gp.getPacketInjector().uninject();
            removePlayer(GamePlayer.get(e.getPlayer()));
            Main.getDataManager().savePlayerAsync(GamePlayer.get(e.getPlayer()));
            Main.getPM().removePlayer(e.getPlayer().getUniqueId());
            StatsNPC.clear(e.getPlayer());
        }
    }

    @EventHandler
    public void onBlock(BlockBreakEvent e) {
        if(GamePlayer.get(e.getPlayer()).isInGame())
            e.setCancelled(true);
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent e) {
        if(GamePlayer.get(e.getPlayer()).isInGame())
            e.setCancelled(true);
    }


    public void addPlayer(GamePlayer player) {
        player.setGame(this);
        gamePlayers.add(player);
        kills.put(player.getPlayer(), 0);
        player.getPlayer().setGameMode(GameMode.ADVENTURE);
        player.getPlayer().setFlying(false);
        player.getPlayer().setAllowFlight(false);
        player.giveSpawnItems();

        if(player.getSelectedClass() == null) {
            player.setClass(player.getGame().gameClasses.get(player.getClassTypeFromEnum()));
        }
        setGameScoreboard(GamePlayer.get(player.getPlayer()));

        player.getPlayer().teleport(Main.mapConfig.spawn);

    }

    public void removePlayer(GamePlayer player) {
        player.setGame(null);
        player.setKillStreak(0);
        if(kills.containsKey(player.getPlayer()))
            kills.remove(player.getPlayer());

        if (gamePlayers.contains(player))
            gamePlayers.remove(player);

        if(player.getPlayer().isOnline()) {
            player.getPlayer().setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
            InventoryUtils.nullInventory(player.getPlayer());
            player.getPlayer().teleport(Main.mapConfig.spawn);
        }

    }

    @EventHandler
    public void onSoup(SignChangeEvent e) {
        if(e.getLine(0).equalsIgnoreCase("sopa") && Rank.isStaff(e.getPlayer())) {
            e.setLine(0, MessageUtil.color("&9&lSopa"));
        }
    }

    @EventHandler
    public void onSign(PlayerInteractEvent e) {
        if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            Block b = e.getClickedBlock();
            if(b.getState() instanceof Sign) {
                Sign s = (Sign)b.getState();
                if(s.getLine(0).equalsIgnoreCase(MessageUtil.color("&9&lSopa"))) {
                    MenuUtils menu = new MenuUtils(Main.get(), e.getPlayer(), MessageUtil.color("&9&lSopa"), 3);
                    menu.setAll(ItemFactory.create(Material.MUSHROOM_SOUP));
                    menu.build();
                    menu.showMenu(e.getPlayer());
                }
            }
        }
    }

    @EventHandler
    public void onLevelUp(OverlandsPlayerLevelUpEvent e) {
        GamePlayer gp = GamePlayer.get(e.getPlayer());
        Player player = e.getPlayer();
        update();
        getScoreboard().updateTeams(e.getPlayer());

        if(gp.hasTitles()) {
            Util.sendTitle(gp.getPlayer(), 12, 28, 9, "&a&lLevel Up!", LevelFormatter.format(gp.getLevel()-1, gp.getPrestigeLevel(), false, true) + " &9&l>>> " +  LevelFormatter.format(gp.getLevel(), gp.getPrestigeLevel(), false, true));
        }
        gp.getPlayer().playSound(gp.getPlayer().getLocation(), Sound.LEVEL_UP, 1, 2);
        StatsNPC.update(e.getPlayer());

        player.getPlayer().sendMessage(MessageUtil.color("&a&m------------------------------------------------"));
        MessageUtil.sendCentredMessage(player.getPlayer(), "&f&lVocê subiu de nível!");
        player.getPlayer().sendMessage(" ");
        MessageUtil.sendCentredMessage(player.getPlayer(), "&eVocê agora é nível &6" + e.getRank());
        if(e.getRank() == 120) {
            MessageUtil.sendCentredMessage(player.getPlayer(), "&ePrestige desbloqueado! Use &c/prestige&e.");
        }else{
            MessageUtil.sendCentredMessage(player.getPlayer(), "&eVocê precisa de &6" + LevelFormatter.expToNextLevel(e.getRank()) + " &ede xp para o próximo nível.");
        }
        if(e.getRank() == 20) {
            MessageUtil.sendCentredMessage(player.getPlayer(), "&eClasses &bHero &edesbloqueadas!");
        }
        player.getPlayer().sendMessage(MessageUtil.color("&a&m------------------------------------------------"));
    }

    @EventHandler
    public void onPrestige(OverlandsPlayerPrestigeLevelUpEvent e) {
        Player p = e.getPlayer();
        Runner.make(Main.get()).interval(10).limit(5).run(() -> {
            FireworkUtils.spawnRandomFirework(p.getLocation());
        });
        Bukkit.broadcastMessage(MessageUtil.color("&b" + p.getName() + " &9comprou Prestige &a" + MathUtils.toRomanNumeral(e.getRank()) + "&9."));
        p.getLocation().getWorld().playSound(p.getLocation(), Sound.ENDERDRAGON_DEATH, 3f, .1f);

        Runner.make(Main.get()).delay(5).run(() -> {
            this.update();
            this.update(GamePlayer.get(p));
            this.updateTeams(p);
            StatsNPC.update(p);
        });

        p.getPlayer().sendMessage(MessageUtil.color("&a&m------------------------------------------------"));
        MessageUtil.sendCentredMessage(p.getPlayer(), "&f&lVocê subiu de nível prestige!");
        p.getPlayer().sendMessage(" ");
        MessageUtil.sendCentredMessage(p.getPlayer(), "&eSeu prestige agora é nível &6" + e.getRank());
        if(e.getRank() == 1) {
            MessageUtil.sendCentredMessage(p.getPlayer(), "&eClasses &bMíticas &edesbloqueadas.");
        }
        p.getPlayer().sendMessage(MessageUtil.color("&a&m------------------------------------------------"));
    }



    public void setGameScoreboard(GamePlayer gp) {
        if(!gp.getLoadedTables().contains(TABLE_TYPE.PLAYER_DATA))
            return;

        Scoreboard c = Main.get().getServer().getScoreboardManager().getNewScoreboard();
        Objective o = c.registerNewObjective("game", "dummy");
        o.setDisplaySlot(DisplaySlot.SIDEBAR);
        o.setDisplayName(MessageUtil.color(title));

        Map<Integer, String> mapBoard = new HashMap<>();

        String combatCheck = gp.getState().getName() + (gp.getState().getState() == State.FIGHTING && gp.getState().isOut() || gp.getState().getState() == State.EVENT && gp.getState().getTime() > 0 ?  "&c(" + gp.getState().getTime() + ")" : "");
        mapBoard.put(11, MessageUtil.color("&7" + TimeUtils.today()));
        mapBoard.put(10, "  ");
        mapBoard.put(9, MessageUtil.color("&fNível: &7" + LevelFormatter.format(gp.getLevel(), gp.getPrestigeLevel(), false, true)));
        mapBoard.put(8, MessageUtil.color("&fPróximo: &b" + (LevelFormatter.expUntilNextLevel(gp) <= 0 ? "&a&lMAX" : LevelFormatter.expUntilNextLevel(gp))));
        mapBoard.put(7, "    ");
        mapBoard.put(6, MessageUtil.color("&fCoins: &b" + gp.getCoins()));
        mapBoard.put(5, "     ");
        mapBoard.put(4, MessageUtil.color(combatCheck));
        mapBoard.put(3, MessageUtil.color("&fStreak: &b" + gp.getKillStreak()));
        mapBoard.put(2, "      ");
        mapBoard.put(1, MessageUtil.color("&fClasse: &b" +  gp.getClassTypeFromEnum().toString().substring(0, 3).toUpperCase()));

        o.getScore(MessageUtil.color("&7" + TimeUtils.today())).setScore(11);
        o.getScore("  ").setScore(10);
        o.getScore(MessageUtil.color("&fNível: &7" + LevelFormatter.format(gp.getLevel(), gp.getPrestigeLevel(), false, true))).setScore(9);
        o.getScore(MessageUtil.color("&fPróximo: &b" + (LevelFormatter.expUntilNextLevel(gp) <= 0 ? "&a&lMAX" : LevelFormatter.expUntilNextLevel(gp)))).setScore(8);
        o.getScore("    ").setScore(7);
        o.getScore(MessageUtil.color("&fCoins: &b" + gp.getCoins())).setScore(6);
        o.getScore("     ").setScore(5);
        o.getScore(MessageUtil.color(combatCheck)).setScore(4);
        o.getScore(MessageUtil.color("&fStreak: &b" + gp.getKillStreak())).setScore(3);
        o.getScore("      ").setScore(2);
        o.getScore(MessageUtil.color("&fClasse: &b" + gp.getClassTypeFromEnum().toString().substring(0, 3).toUpperCase())).setScore(1);

        scoreboardMap.put(gp.getPlayer(), mapBoard);

        gp.getPlayer().setScoreboard(c);
        hs.registerHealthBar(gp.getPlayer());

        hs.registerTeams();
        getPlayers().forEach(p -> {
            hs.addPlayerToPlayerTeam(p.getPlayer(), gp.getPlayer());
            hs.addPlayerToPlayerTeam(gp.getPlayer(), p.getPlayer());
        });
    }

    public void updateScoreboard(GamePlayer gp, Integer id, String toUpdate) {
        if (gp.getPlayer().getScoreboard() != null
                && gp.getPlayer().getScoreboard().getObjective(DisplaySlot.SIDEBAR) != null) {
            Scoreboard s = gp.getPlayer().getScoreboard();
            Objective o = s.getObjective(DisplaySlot.SIDEBAR);
            s.resetScores(scoreboardMap.get(gp.getPlayer()).get(id));
            o.getScore(toUpdate).setScore(id);
            scoreboardMap.get(gp.getPlayer()).put(id, toUpdate);
        } else {
            setGameScoreboard(gp);
            hs.registerHealthBar(gp.getPlayer());
            hs.registerTeams(gp.getPlayer());
        }
    }

    public void updateTeams(Player p) {
        hs.updateTeams(p);
    }

    public void update() {
        getPlayers().forEach(gp -> {
            updateScoreboard(gp, 9, MessageUtil.color("&fNível: &7" + LevelFormatter.format(gp.getLevel(), gp.getPrestigeLevel(), false, true)));
            updateScoreboard(gp, 8, MessageUtil.color("&fPróximo: &b" + LevelFormatter.formatExpToNextLevel(gp.getLevel())));
            updateScoreboard(gp, 6, MessageUtil.color("&fCoins: &b" + gp.getCoins()));
        });
    }

    public void update(GamePlayer gp) {
        updateScoreboard(gp, 9, MessageUtil.color("&fNível: &7" + LevelFormatter.format(gp.getLevel(), gp.getPrestigeLevel(), false, true)));
        updateScoreboard(gp, 8, MessageUtil.color("&fPróximo: &b" + LevelFormatter.formatExpToNextLevel(gp.getLevel())));
        updateScoreboard(gp, 6, MessageUtil.color("&fCoins: &b" + gp.getCoins()));
    }

    public GameScoreboard getScoreboard() {
        return hs;
    }

    public List<GamePlayer> getPlayers() {
        return gamePlayers;
    }

    public boolean isGodMode() {
        return godMode;
    }

    public void setGodMode(boolean godMode) {
        this.godMode = godMode;
    }

    public GameEvent getEvent() {
        return event;
    }

    public void setEvent(GameEvent event) {
        this.event = event;
    }

    public int getCoinMultiplier() {
        return coinMultiplier;
    }

    public void setCoinMultiplier(int coinMultiplier) {
        this.coinMultiplier = coinMultiplier;
    }

    public boolean canGenerateInstances() {
        return canHaveEvents;
    }

    public void setInstances(boolean canHaveEvents) {
        this.canHaveEvents = canHaveEvents;
    }

    public Player getTarget() {
        return target;
    }

    public void setTarget(Player target) {
        this.target = target;
    }

    public boolean isPvPDisabled() {
        return pvp;
    }

    public void setPvPDisable(boolean pvp) {
        this.pvp = pvp;
    }

    public void shutdown() {
        if(!packages.isEmpty())
            packages.get(0).shutdown();
    }

    public List<GameClass> getClasses() {
        return new ArrayList<>(gameClasses.values());
    }
}