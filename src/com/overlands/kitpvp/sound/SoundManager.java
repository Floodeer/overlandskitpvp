package com.overlands.kitpvp.sound;

import java.util.Arrays;
import java.util.stream.Stream;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.location.LocationUtils;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.classes.ClassType;

public enum SoundManager {

	//Classes
	CRYOMANCER(Sound.ENDERDRAGON_GROWL, "cryostrike"),
	PYROMANCER(Sound.ENDERDRAGON_GROWL, "firestrike"),
	SHAMAN(Sound.ENDERDRAGON_WINGS, "tornado"),
	ELECTRO(Sound.AMBIENCE_THUNDER, "electrify"),
	ENDERMAN(Sound.ENDERMAN_TELEPORT, "teleport1", "teleport2"),
	PIGMAN(Sound.ENDERDRAGON_WINGS, "pigman_shield");
	
	Sound bukkit;
	String[] pack;

	SoundManager(String... packSound) {
		this.pack = packSound;
	}
	
	SoundManager(Sound bukkitSound, String... packSound) {
		this.bukkit = bukkitSound;
		this.pack = packSound;
	}
	
	public static Stream<SoundManager> collect() {
		return Arrays.stream(SoundManager.values());
	}
	
	public static SoundManager getByClass(ClassType zz) {
		return collect().filter(z -> z.toString().equalsIgnoreCase(zz.toString().toUpperCase())).findAny().get();
	}
	
	public String random() {
		if(pack.length >= 1)
		 return MathUtils.random(Arrays.asList(pack));
		else
			return pack[0];
	}
	
	public void play(Player player) {
		if(GamePlayer.get(player).hasResourcePack() || bukkit == null)
			player.playSound(player.getLocation(), random(),  1, 0);
		else
			player.playSound(player.getLocation(), bukkit, 1, 0);
		
	}
	
	public void play(Player player, float pitch) {
		if(GamePlayer.get(player).hasResourcePack() || bukkit == null)
			player.playSound(player.getLocation(), random(),  1, pitch);
		else
			player.playSound(player.getLocation(), bukkit, 1, pitch);
	}
	
	public void play(Player player, float volume, float pitch) {
		if(GamePlayer.get(player).hasResourcePack() || bukkit == null)
			player.playSound(player.getLocation(), random(),  volume, pitch);
		else
			player.playSound(player.getLocation(), bukkit, volume, pitch);
	}
	
	
	public void playForAll(Location origin, double radius, float volume, float pitch) {
			LocationUtils.getNearbyPlayers(origin, radius).forEach(p -> {
				if(GamePlayer.get(p).hasResourcePack() || bukkit == null)
				 p.playSound(origin.getBlock().getLocation(), random(),  volume, pitch);
				else
					 p.playSound(origin.getBlock().getLocation(), bukkit, volume, pitch);
			});
	}

	public void playForAll(Location origin, float volume, float pitch) {
		origin.getWorld().getPlayers().stream().forEach(p-> {
			if(GamePlayer.get(p).hasResourcePack() || bukkit == null)
				 p.playSound(origin.getBlock().getLocation(), random(),  volume, pitch);
				else
					 p.playSound(origin.getBlock().getLocation(), bukkit, volume, pitch);
		});
	}
}
