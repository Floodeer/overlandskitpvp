package com.overlands.kitpvp.duels;

import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mojang.authlib.GameProfile;
import com.overlands.core.utils.MenuUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Runner;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.classes.ClassType;
import com.overlands.kitpvp.game.classes.GameClass;
import com.overlands.kitpvp.skins.DefaultSkins;

public class DuelManager implements Listener {
	
	public static Map<Player, DuelBuilder> builders = Maps.newHashMap();
	private List<Player> building = Lists.newArrayList();
	
	public void showPart1(Player player, Player target) {
		DuelBuilder builder = null;
		if(!builders.containsKey(player)) {
		     builder = new DuelBuilder(player, target);
		     builder.get().setAllowSkills(true);
				builders.put(player, builder);
		}
		else {
			builder = builders.get(player);
			builder.setPlayer(target);
		}
		
		if(!building.contains(player))
			building.add(player);
		
		MenuUtils classes = new MenuUtils(Main.get(), player, MessageUtil.color("&9Construtor de duelo"), 4);
		if(builder.get().allowSkills()) {
			classes.setItem(10, ItemFactory.create(Material.INK_SACK, MessageUtil.color("&aHabilidades ativadas"), MessageUtil.color("&7Clique para desativar!"), (byte) 10));
		}else {
			classes.setItem(10, ItemFactory.create(Material.INK_SACK, MessageUtil.color("&cHabilidades desativadas"), MessageUtil.color("&7Clique para ativar!"), (byte) 8));
		}
		
		if(builder.get().getClazz() == null) {
			ItemStack stack = ItemFactory.EasyFactory.getQuestionMark();
			ItemFactory.applyName(stack, MessageUtil.color("&7Classe Aleatória"), MessageUtil.color("&6Clique para selecionar"));
			classes.setItem(13, stack);
		}else {
			ClassType type = builder.get().getClazz().getType();
			ItemStack stack = ItemFactory.createSkull(new DefaultSkins().getType(type), MessageUtil.color("&7Classe: &b" + type.toString()));
			ItemFactory.applyName(stack, MessageUtil.color("&7Classe: &b" + type.toString()), "&7Clique para modificar");
			classes.setItem(13, stack);
		}
		
		List<String> lore = Lists.newArrayList();
		try {
			lore.add(" ");
			if(!builder.get().getBlacklistedClasses().isEmpty()) {
				builder.get().getBlacklistedClasses().forEach(zz -> {
					lore.add(MessageUtil.color("&7- " + zz.getName()));
				});
			}else {
				lore.add(MessageUtil.color("&7Nenhuma"));
			}
		}catch(Exception ex) {
			player.sendMessage(MessageUtil.color("&cOcorreu um erro ao construir duelo, tente novamente."));
			if(building.contains(player))
				building.remove(player);
			player.closeInventory();
			
			builder.cancel();
			builders.remove(player);
		}

		ItemStack stack = ItemFactory.EasyFactory.getQuestionMark();
		ItemFactory.applyName(stack, MessageUtil.color("&7Classes desabilitadas"));
		ItemFactory.applyLoreArrays(stack, lore);
		classes.setItem(16, stack);
		
		classes.setItem(32, ItemFactory.createSkull(((CraftPlayer)target).getProfile(), MessageUtil.color("&aEnviar desafio")));
		classes.setItem(30, ItemFactory.EasyFactory.getXArrow());
		classes.showMenu(player);
	}
	
	
	@EventHandler
	public void onClick(InventoryClickEvent e) {
		if(e.getInventory().getName().equalsIgnoreCase(MessageUtil.color("&9Construtor de duelo"))) {
			Player p = (Player)e.getWhoClicked();
			int slot = e.getRawSlot();
			DuelBuilder builder = builders.get(p);
			e.setCancelled(true);
			e.setResult(Result.DENY);
			if(slot == 10) {
				if(builder.get().allowSkills()) {
					builder.get().setAllowSkills(false);
					p.sendMessage(MessageUtil.color("&cHabilidades desabilitadas."));
					p.playSound(p.getLocation(), Sound.NOTE_PLING, 1, 2);
				}else {
					builder.get().setAllowSkills(true);
					p.sendMessage(MessageUtil.color("&aHabilidades habilitadas."));
					p.playSound(p.getLocation(), Sound.NOTE_PLING, 1, 2);
				}
				showPart1(p, builder.get().getPlayer());
			}else if(slot == 13) {
				showPart3Menu1(p);
			}else if(slot == 16) {
				showPart2Menu1(p);
			}else if(slot == 32) {
				if(building.contains(p))
					building.remove(p);
				p.closeInventory();
				builder.build();
			}else if(slot == 30) {
				if(building.contains(p))
					building.remove(p);
				p.closeInventory();
				
				builder.cancel();
				builders.remove(p);
			}
			
		}else if(e.getInventory().getName().equalsIgnoreCase(MessageUtil.color("&7Selecionar classe 1/2"))) {
			Player p = (Player)e.getWhoClicked();
			int slot = e.getRawSlot();
			e.setCancelled(true);
			e.setResult(Result.DENY);
			if(slot == 45) {
				p.closeInventory();
				showPart1(p, builders.get(p).get().getPlayer());
				return;
			}else if(slot == 49) {
				DuelBuilder builder = builders.get(p);
				builder.get().setClass(null);
				p.sendMessage(MessageUtil.color("&eClasse modificada para &baleatória&e."));
				p.playSound(p.getLocation(), Sound.NOTE_PLING, 1, 2);
			}else if(slot == 53) {
				showPart3Menu2(p);
				return;
			}
			if(e.getCurrentItem() == null || e.getCurrentItem().getType() == Material.AIR)
				return;
			
			ClassType type = ClassType.fromName(ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()));
			DuelBuilder builder = builders.get(p);
			builder.get().setClass(Main.getGame().gameClasses.get(type));
			if(type != null)
				p.sendMessage(MessageUtil.color("&eClasse modificada para &b" + type.toString() + "&e."));
			p.playSound(p.getLocation(), Sound.NOTE_PLING, 1, 2);
			showPart1(p, builder.get().getPlayer());
		}else if(e.getInventory().getName().equalsIgnoreCase(MessageUtil.color("&7Selecionar classe 2/2"))) {
			Player p = (Player)e.getWhoClicked();
			int slot = e.getRawSlot();
			e.setCancelled(true);
			e.setResult(Result.DENY);
			if(slot == 45) {
				p.closeInventory();
				showPart3Menu1(p);
				return;
			}else if(slot == 53) {
				DuelBuilder builder = builders.get(p);
				builder.get().setClass(null);
				p.sendMessage(MessageUtil.color("&eClasse modificada para &baleatória&e."));
				p.playSound(p.getLocation(), Sound.NOTE_PLING, 1, 2);
			}
			if(e.getCurrentItem() == null || e.getCurrentItem().getType() == Material.AIR)
				return;
			
			ClassType type = ClassType.fromName(ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()));
			DuelBuilder builder = builders.get(p);
			builder.get().setClass(Main.getGame().gameClasses.get(type));
			p.sendMessage(MessageUtil.color("&eClasse modificada para &b" + type.toString() + "&e."));
			p.playSound(p.getLocation(), Sound.NOTE_PLING, 1, 2);
			showPart1(p, builder.get().getPlayer());
		}else if(e.getInventory().getName().equalsIgnoreCase(MessageUtil.color("&7Classes ativadas 1/3"))) {
			Player p = (Player)e.getWhoClicked();
			int slot = e.getRawSlot();
			e.setCancelled(true);
			e.setResult(Result.DENY);
			DuelBuilder builder = builders.get(p);
			ClassType type = null;
			if(slot == 10 || slot == 19) {
				type = ClassType.HEROBRINE;
			}else if(slot == 11 || slot == 20) {
				type = ClassType.CREEPER;
			}else if(slot == 12 || slot == 21) {
				type = ClassType.ENDERMAN;
			}else if(slot == 13 || slot == 22) {
				type = ClassType.CRYOMANCER;
			}else if(slot == 14 || slot == 23) {
				type = ClassType.SPIDER;
			}else if(slot == 15 || slot == 24) {
				type = ClassType.INFECTED;
			}else if(slot == 16 || slot == 25) {
				type = ClassType.PYROMANCER;
			}else if(slot == 28 || slot == 37) {
				type = ClassType.FALCON;
			}else if(slot == 29 || slot == 38) {
				type = ClassType.EPSILON;
			}else if(slot == 30 || slot == 39) {
				type = ClassType.SKELETON;
			}else if(slot == 31 || slot == 40) {
				type = ClassType.BLAZE;
			}else if(slot == 32 || slot == 41) {
				type = ClassType.WIZARD;
			}else if(slot == 33 || slot == 42) {
				type = ClassType.ANCIENT;
			}else if(slot == 34 || slot == 43) {
				type = ClassType.SQUID;
			}else if(slot == 45) {
				showPart1(p, builder.get().getPlayer());
				return;
			}else if(slot == 53) {
				showPart2Menu2(p);
				return;
			}
			
			GameClass clazz = Main.getGame().gameClasses.get(type);
			if(builder.get().getBlacklistedClasses().contains(clazz)) {
				builder.get().getBlacklistedClasses().remove(clazz);
				p.sendMessage(MessageUtil.color("&9Classe removida da lista negra."));
			}else {
				builder.get().getBlacklistedClasses().add(clazz);
				p.sendMessage(MessageUtil.color("&9Classe adicionada na lista negra."));
			}
			
			showPart2Menu1(p);
		}else if(e.getInventory().getName().equalsIgnoreCase(MessageUtil.color("&7Classes ativadas 2/3"))) {
			Player p = (Player)e.getWhoClicked();
			int slot = e.getRawSlot();
			e.setCancelled(true);
			e.setResult(Result.DENY);
			DuelBuilder builder = builders.get(p);
			ClassType type = null;
			
			if(slot == 10 || slot == 19) {
				type = ClassType.DREADLORD;
			}else if(slot == 11 || slot == 20) {
				type = ClassType.SHAMAN;
			}else if(slot == 12 || slot == 21) {
				type = ClassType.GOLEM;
			}else if(slot == 13 || slot == 22) {
				type = ClassType.PIGMAN;
			}else if(slot == 14 || slot == 23) {
				type = ClassType.ARCANIST;
			}else if(slot == 15 || slot == 24) {
				type = ClassType.PALADIN;
			}else if(slot == 16 || slot == 25) {
				type = ClassType.PIRATA;
			}else if(slot == 28 || slot == 37) {
				type = ClassType.MAGMA;
			}else if(slot == 29 || slot == 38) {
				type = ClassType.ELECTRO;
			}else if(slot == 30 || slot == 39) {
				type = ClassType.MAGNATITION;
			}else if(slot == 31 || slot == 40) {
				type = ClassType.RUMBLER;
			}else if(slot == 32 || slot == 41) {
				type = ClassType.LANA;
			}else if(slot == 33 || slot == 42) {
				type = ClassType.CRUSADER;
			}else if(slot == 34 || slot == 43) {
				type = ClassType.ELENNE;
			}else if(slot == 45) {
				showPart2Menu1(p);
				return;
			}else if(slot == 53) {
				showPart2Menu3(p);
				return;
			}
			
			GameClass clazz = Main.getGame().gameClasses.get(type);
			if(builder.get().getBlacklistedClasses().contains(clazz)) {
				builder.get().getBlacklistedClasses().remove(clazz);
				p.sendMessage(MessageUtil.color("&9Classe removida da lista negra."));
			}else {
				builder.get().getBlacklistedClasses().add(clazz);
				p.sendMessage(MessageUtil.color("&9Classe adicionada na lista negra."));
			}
			
			showPart2Menu2(p);
		}else if(e.getInventory().getName().equalsIgnoreCase(MessageUtil.color("&7Classes ativadas 3/3"))) {
			Player p = (Player)e.getWhoClicked();
			int slot = e.getRawSlot();
			e.setCancelled(true);
			e.setResult(Result.DENY);
			DuelBuilder builder = builders.get(p);
			ClassType type = null;
			
			if(slot == 10 || slot == 19) {
				type = ClassType.SHADOW;
			}else if(slot == 11 || slot == 20) {
				type = ClassType.FIGHTER;
			}else if(slot == 12 || slot == 21) {
				type = ClassType.HUNTER;
			}else if(slot == 45) {
				showPart2Menu2(p);
				return;
			}
			
			GameClass clazz = Main.getGame().gameClasses.get(type);
			if(builder.get().getBlacklistedClasses().contains(clazz)) {
				builder.get().getBlacklistedClasses().remove(clazz);
				p.sendMessage(MessageUtil.color("&9Classe removida da lista negra."));
			}else {
				builder.get().getBlacklistedClasses().add(clazz);
				p.sendMessage(MessageUtil.color("&9Classe adicionada na lista negra."));
			}
			
			showPart2Menu3(p);
		}
	}
	
	@EventHandler
	public void onClose(InventoryCloseEvent e) {
		if(e.getInventory().getName().equalsIgnoreCase(MessageUtil.color("&9Construtor de duelo"))) {
			Runner.make(Main.get()).delay(10).run(() -> {
				if(e.getPlayer().getOpenInventory() != null) {
					InventoryView inv = e.getPlayer().getOpenInventory();
					if(building.contains((Player)e.getPlayer())) {
						if(!inv.getTitle().startsWith(MessageUtil.color("&7Classes")) && !inv.getTitle().startsWith(MessageUtil.color("&7Selecionar"))) {
							showPart1((Player)e.getPlayer(), builders.get(e.getPlayer()).get().getPlayer());
						    e.getPlayer().sendMessage(MessageUtil.color("&eVocê não terminou de configurar o duelo, clique em cancelar para cancelar."));
						}
					}
				}
			});
		}
	}
	
	public void showPart2Menu1(Player p) {
		DuelBuilder builder = builders.get(p);
		
		MenuUtils classes = new MenuUtils(Main.get(), p, MessageUtil.color("&7Classes ativadas 1/3"), 6);
		ItemStack on = ItemFactory.create(Material.INK_SACK, MessageUtil.color("&aClasse habilitada"),MessageUtil.color("&7Clique para desabilitar!"), (byte) 10);
		ItemStack off = ItemFactory.create(Material.INK_SACK, MessageUtil.color("&cClasse desabilitada"),MessageUtil.color("&7Clique para habilitar!"), (byte) 8);

		ItemStack herobrine = builder(new DefaultSkins().getType(ClassType.HEROBRINE), "§7Herobrine");
		classes.setItem(10, herobrine);
		
		ItemStack creeper = builder(new DefaultSkins().getType(ClassType.CREEPER), "§7Creeper");
		classes.setItem(11, creeper);
		
		ItemStack enderman = builder(new DefaultSkins().getType(ClassType.ENDERMAN), "§7Enderman");
		classes.setItem(12, enderman);
		
		ItemStack cryomancer = builder(new DefaultSkins().getType(ClassType.CRYOMANCER), "§7Cryomancer");
		classes.setItem(13, cryomancer);
		
		ItemStack spider = builder(new DefaultSkins().getType(ClassType.SPIDER), "§7Spider");
		classes.setItem(14, spider);
		
		ItemStack incendiary = builder(new DefaultSkins().getType(ClassType.INFECTED), "§7Infected");
		classes.setItem(15, incendiary);
		
		ItemStack pyromancer = builder(new DefaultSkins().getType(ClassType.PYROMANCER), "§7Pyromancer");
		classes.setItem(16, pyromancer);
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.HEROBRINE))) {
			classes.setItem(19, on);
		}else {
			classes.setItem(19, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.CREEPER))) {
			classes.setItem(20, on);
		}else {
			classes.setItem(20, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.ENDERMAN))) {
			classes.setItem(21, on);
		}else {
			classes.setItem(21, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.CRYOMANCER))) {
			classes.setItem(22, on);
		}else {
			classes.setItem(22, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.SPIDER))) {
			classes.setItem(23, on);
		}else {
			classes.setItem(23, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.INFECTED))) {
			classes.setItem(24, on);
		}else {
			classes.setItem(24, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.PYROMANCER))) {
			classes.setItem(25, on);
		}else {
			classes.setItem(25, off);
		}
		
		ItemStack falcon = builder(new DefaultSkins().getType(ClassType.FALCON), "§7Falcon");
		classes.setItem(28, falcon);
		
		ItemStack epsilon = builder(new DefaultSkins().getType(ClassType.EPSILON), "§7Epsilon");
		classes.setItem(29, epsilon);
		
		ItemStack skeleton = builder(new DefaultSkins().getType(ClassType.SKELETON), "§7Skeleton");
		classes.setItem(30, skeleton);
	
		ItemStack blaze = builder(new DefaultSkins().getType(ClassType.BLAZE), "§7Blaze");
		classes.setItem(31, blaze);
		
		ItemStack wizard = builder(new DefaultSkins().getType(ClassType.WIZARD), "§7Wizard");
		classes.setItem(32, wizard);
		
		ItemStack ancient = builder(new DefaultSkins().getType(ClassType.ANCIENT), "§7Ancient");
		classes.setItem(33, ancient);
		
		ItemStack squid = builder(new DefaultSkins().getType(ClassType.SQUID), "§7Squid");
		classes.setItem(34, squid);
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.FALCON))) {
			classes.setItem(37, on);
		}else {
			classes.setItem(37, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.EPSILON))) {
			classes.setItem(38, on);
		}else {
			classes.setItem(38, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.SKELETON))) {
			classes.setItem(39, on);
		}else {
			classes.setItem(39, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.BLAZE))) {
			classes.setItem(40, on);
		}else {
			classes.setItem(40, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.WIZARD))) {
			classes.setItem(41, on);
		}else {
			classes.setItem(41, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.ANCIENT))) {
			classes.setItem(42, on);
		}else {
			classes.setItem(42, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.SQUID))) {
			classes.setItem(43, on);
		}else {
			classes.setItem(43, off);
		}
		
		classes.setItem(45, ItemFactory.EasyFactory.getLeftArrow());
		classes.setItem(53, ItemFactory.EasyFactory.getRightArrow());
		
		classes.build(); 
		classes.showMenu(p);
	}
	
	public void showPart2Menu2(Player p) {
		DuelBuilder builder = builders.get(p);
		
		MenuUtils classes = new MenuUtils(Main.get(), p, MessageUtil.color("&7Classes ativadas 2/3"), 6);
		ItemStack on = ItemFactory.create(Material.INK_SACK, MessageUtil.color("&aClasse habilitada"),MessageUtil.color("&7Clique para desabilitar!"), (byte) 10);
		ItemStack off = ItemFactory.create(Material.INK_SACK, MessageUtil.color("&cClasse desabilitada"),MessageUtil.color("&7Clique para habilitar!"), (byte) 8);
		
		ItemStack dreadlord = builder(new DefaultSkins().getType(ClassType.DREADLORD), "§7Dreadlord");
		classes.setItem(10, dreadlord);
		
		ItemStack shaman = builder(new DefaultSkins().getType(ClassType.SHAMAN), "§7Shaman");
		classes.setItem(11, shaman);
		
		ItemStack golem = builder(new DefaultSkins().getType(ClassType.GOLEM), "§7Golem");
		classes.setItem(12, golem);
		
		ItemStack pigman = builder(new DefaultSkins().getType(ClassType.PIGMAN), "§7Pigman");
		classes.setItem(13, pigman);
		
		ItemStack arcanist = builder(new DefaultSkins().getType(ClassType.ARCANIST), "§7Arcanist");
		classes.setItem(14, arcanist);
		
		ItemStack paladin = builder(new DefaultSkins().getType(ClassType.PALADIN), "§7Paladin");
		classes.setItem(15, paladin);
		
		ItemStack pirate = builder(new DefaultSkins().getType(ClassType.PIRATA), "§7Pirata");
		classes.setItem(16, pirate);

		ItemStack magma = builder(new DefaultSkins().getType(ClassType.MAGMA), "§7Magma");
		classes.setItem(28, magma);
		
		ItemStack electro = builder(new DefaultSkins().getType(ClassType.ELECTRO), "§7Electro");
		classes.setItem(29, electro);
		
		ItemStack magnatition = builder(new DefaultSkins().getType(ClassType.MAGNATITION), "§7Magnatition");
		classes.setItem(30, magnatition);
		
		ItemStack rumbler = builder(new DefaultSkins().getType(ClassType.RUMBLER), "§7Rumbler");
		classes.setItem(31, rumbler);
		
		ItemStack lana = builder(new DefaultSkins().getType(ClassType.LANA), "§7Lana");
		classes.setItem(32, lana);
		
		ItemStack crusader = builder(new DefaultSkins().getType(ClassType.CRUSADER), "§7Crusader");
		classes.setItem(33, crusader);
		
		ItemStack elenne = builder(new DefaultSkins().getType(ClassType.ELENNE), "§7Elenne");
		classes.setItem(34, elenne);
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.DREADLORD))) {
			classes.setItem(19, on);
		}else {
			classes.setItem(19, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.SHAMAN))) {
			classes.setItem(20, on);
		}else {
			classes.setItem(20, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.GOLEM))) {
			classes.setItem(21, on);
		}else {
			classes.setItem(21, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.PIGMAN))) {
			classes.setItem(22, on);
		}else {
			classes.setItem(22, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.ARCANIST))) {
			classes.setItem(23, on);
		}else {
			classes.setItem(23, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.PALADIN))) {
			classes.setItem(24, on);
		}else {
			classes.setItem(24, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.PIRATA))) {
			classes.setItem(25, on);
		}else {
			classes.setItem(25, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.MAGMA))) {
			classes.setItem(37, on);
		}else {
			classes.setItem(37, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.ELECTRO))) {
			classes.setItem(38, on);
		}else {
			classes.setItem(38, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.MAGNATITION))) {
			classes.setItem(39, on);
		}else {
			classes.setItem(39, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.RUMBLER))) {
			classes.setItem(40, on);
		}else {
			classes.setItem(40, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.LANA))) {
			classes.setItem(41, on);
		}else {
			classes.setItem(41, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.CRUSADER))) {
			classes.setItem(42, on);
		}else {
			classes.setItem(42, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.ELENNE))) {
			classes.setItem(43, on);
		}else {
			classes.setItem(43, off);
		}
		
		classes.setItem(45, ItemFactory.EasyFactory.getLeftArrow());
		classes.setItem(53, ItemFactory.EasyFactory.getRightArrow());
		
		classes.build(); 
		classes.showMenu(p);
	}
	
	public void showPart2Menu3(Player p) {
		DuelBuilder builder = builders.get(p);
		
		MenuUtils classes = new MenuUtils(Main.get(), p, MessageUtil.color("&7Classes ativadas 3/3"), 6);
		ItemStack on = ItemFactory.create(Material.INK_SACK, MessageUtil.color("&aClasse habilitada"),MessageUtil.color("&7Clique para desabilitar!"), (byte) 10);
		ItemStack off = ItemFactory.create(Material.INK_SACK, MessageUtil.color("&cClasse desabilitada"),MessageUtil.color("&7Clique para habilitar!"), (byte) 8);
		
		ItemStack shadow = builder(new DefaultSkins().getType(ClassType.SHADOW), "§7Shadow");
		classes.setItem(10, shadow);
		
		ItemStack fighter = builder(new DefaultSkins().getType(ClassType.FIGHTER), "§7Fighter");
		classes.setItem(11, fighter);
		
		ItemStack hunter = builder(new DefaultSkins().getType(ClassType.HUNTER), "§7Hunter");
		classes.setItem(12, hunter);
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.SHADOW))) {
			classes.setItem(19, on);
		}else {
			classes.setItem(19, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.FIGHTER))) {
			classes.setItem(20, on);
		}else {
			classes.setItem(20, off);
		}
		
		if(!builder.get().getBlacklistedClasses().contains(Main.getGame().gameClasses.get(ClassType.HUNTER))) {
			classes.setItem(21, on);
		}else {
			classes.setItem(21, off);
		}
		
		classes.setItem(45, ItemFactory.EasyFactory.getLeftArrow());
		
		classes.build(); 
		classes.showMenu(p);
	}
	
	public void showPart3Menu1(Player p) {
		
		MenuUtils classes = new MenuUtils(Main.get(), p, MessageUtil.color("&7Selecionar classe 1/2"), 6);
		
		ItemStack herobrine = builder(new DefaultSkins().getType(ClassType.HEROBRINE), "§7Herobrine");
		classes.setItem(10, herobrine);
		
		ItemStack creeper = builder(new DefaultSkins().getType(ClassType.CREEPER), "§7Creeper");
		classes.setItem(11, creeper);
		
		ItemStack enderman = builder(new DefaultSkins().getType(ClassType.ENDERMAN), "§7Enderman");
		classes.setItem(12, enderman);
		
		ItemStack cryomancer = builder(new DefaultSkins().getType(ClassType.CRYOMANCER), "§7Cryomancer");
		classes.setItem(13, cryomancer);
		
		ItemStack spider = builder(new DefaultSkins().getType(ClassType.SPIDER), "§7Spider");
		classes.setItem(14, spider);
		
		ItemStack incendiary = builder(new DefaultSkins().getType(ClassType.INFECTED), "§7Infected");
		classes.setItem(15, incendiary);
		
		ItemStack pyromancer = builder(new DefaultSkins().getType(ClassType.PYROMANCER), "§7Pyromancer");
		classes.setItem(16, pyromancer);
		
		ItemStack falcon = builder(new DefaultSkins().getType(ClassType.FALCON), "§7Falcon");
		classes.setItem(19, falcon);
		
		ItemStack epsilon = builder(new DefaultSkins().getType(ClassType.EPSILON), "§7Epsilon");
		classes.setItem(20, epsilon);
		
		ItemStack skeleton = builder(new DefaultSkins().getType(ClassType.SKELETON), "§7Skeleton");
		classes.setItem(21, skeleton);
	
		ItemStack blaze = builder(new DefaultSkins().getType(ClassType.BLAZE), "§7Blaze");
		classes.setItem(22, blaze);
		
		ItemStack wizard = builder(new DefaultSkins().getType(ClassType.WIZARD), "§7Wizard");
		classes.setItem(23, wizard);
		
		ItemStack ancient = builder(new DefaultSkins().getType(ClassType.ANCIENT), "§7Ancient");
		classes.setItem(24, ancient);
		
		ItemStack squid = builder(new DefaultSkins().getType(ClassType.SQUID), "§7Squid");
		classes.setItem(25, squid);
		
		ItemStack dreadlord = builder(new DefaultSkins().getType(ClassType.DREADLORD), "§7Dreadlord");
		classes.setItem(28, dreadlord);
		
		ItemStack shaman = builder(new DefaultSkins().getType(ClassType.SHAMAN), "§7Shaman");
		classes.setItem(29, shaman);
		
		ItemStack golem = builder(new DefaultSkins().getType(ClassType.GOLEM), "§7Golem");
		classes.setItem(30, golem);
		
		ItemStack pigman = builder(new DefaultSkins().getType(ClassType.PIGMAN), "§7Pigman");
		classes.setItem(31, pigman);
		
		ItemStack arcanist = builder(new DefaultSkins().getType(ClassType.ARCANIST), "§7Arcanist");
		classes.setItem(32, arcanist);
		
		ItemStack paladin = builder(new DefaultSkins().getType(ClassType.PALADIN), "§7Paladin");
		classes.setItem(33, paladin);
		
		ItemStack pirate = builder(new DefaultSkins().getType(ClassType.PIRATA), "§7Pirata");
		classes.setItem(34, pirate);

		ItemStack magma = builder(new DefaultSkins().getType(ClassType.MAGMA), "§7Magma");
		classes.setItem(37, magma);
		
		ItemStack electro = builder(new DefaultSkins().getType(ClassType.ELECTRO), "§7Electro");
		classes.setItem(38, electro);
		
		ItemStack magnatition = builder(new DefaultSkins().getType(ClassType.MAGNATITION), "§7Magnatition");
		classes.setItem(39, magnatition);
		
		ItemStack rumbler = builder(new DefaultSkins().getType(ClassType.RUMBLER), "§7Rumbler");
		classes.setItem(40, rumbler);
		
		ItemStack lana = builder(new DefaultSkins().getType(ClassType.LANA), "§7Lana");
		classes.setItem(41, lana);
		
		ItemStack crusader = builder(new DefaultSkins().getType(ClassType.CRUSADER), "§7Crusader");
		classes.setItem(42, crusader);
		
		ItemStack elenne = builder(new DefaultSkins().getType(ClassType.ELENNE), "§7Elenne");
		classes.setItem(43, elenne);
		
		classes.setItem(45, ItemFactory.EasyFactory.getLeftArrow());
		classes.setItem(49, ItemFactory.EasyFactory.getQuestionMark("§7Classe aleatória", null));
		classes.setItem(53, ItemFactory.EasyFactory.getRightArrow());
		
		classes.build(); 
		classes.showMenu(p);
	}
	
	public void showPart3Menu2(Player p) {

		MenuUtils classes = new MenuUtils(Main.get(), p, MessageUtil.color("&7Selecionar classe 2/2"), 6);
		ItemStack shadow = builder(new DefaultSkins().getType(ClassType.SHADOW), "§7Shadow");
		
		classes.setItem(10, shadow);
		
		ItemStack fighter = builder(new DefaultSkins().getType(ClassType.FIGHTER), "§7Fighter");
		classes.setItem(11, fighter);
		
		ItemStack hunter = builder(new DefaultSkins().getType(ClassType.HUNTER), "§7Hunter");
		classes.setItem(12, hunter);
		
		
		classes.setItem(45, ItemFactory.EasyFactory.getLeftArrow());
		classes.setItem(53, ItemFactory.EasyFactory.getQuestionMark("§7Classe aleatória", null));
		
		classes.build(); 
		classes.showMenu(p);
	}
	
	private ItemStack builder(GameProfile skin, String nome) {
		return ItemFactory.createSkull(skin, nome);
	}
}
