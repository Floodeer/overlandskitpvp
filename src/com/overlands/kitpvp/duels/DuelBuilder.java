package com.overlands.kitpvp.duels;

import org.bukkit.entity.Player;

import com.overlands.kitpvp.game.classes.GameClass;

public class DuelBuilder {

	private DuelGame game;
	
	public DuelBuilder(Player challenger, Player target) {
		this.game = new DuelGame(challenger, target, null, false);
	}
	
	public DuelBuilder setPlayer(Player player) {
		game.setPlayer(player);
		return this;
	}
	
	public DuelBuilder withClass(GameClass zz) {
		game.setClass(zz);
		return this;
	}
	
	public DuelBuilder addBlacklistedClass(GameClass clazz) {
		game.addBlacklisted(clazz);
		return this;
	}
	
	public DuelBuilder build() {
		game.invite();
		return this;
	}
	public DuelGame get() {
		return game;
	}

	public void cancel() {
		game.cancel();
	}
}
