package com.overlands.kitpvp.duels;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.overlands.core.damage.combat.CombatLog;
import com.overlands.core.damage.combat.event.OverlandsDeathEvent;
import com.overlands.core.utils.Imobilizer;
import com.overlands.core.utils.InventoryUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Runner;
import com.overlands.core.utils.Util;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.classes.GameClass;
import com.overlands.kitpvp.npc.StatsNPC;

public class DuelGame implements Listener {
	
	private int id;
	private Location spawn1;
	private Location spawn2;

	private final Map<Player, GameClass> oldClass = Maps.newHashMap();
	private final List<GameClass> blackListedClasses = Lists.newArrayList();
	
	private State state = State.STARTING;
	
	private Player challenger;
	private Player player;
	private GameClass selectedClass;
	private boolean allowSkills;
	
	public DuelGame(Player challenger, Player player, GameClass selectedClass, boolean allowSkills) {
		this.challenger = challenger;
		this.player = player;
		this.selectedClass = selectedClass;
		this.allowSkills = allowSkills;
		
		Main.get().getServer().getPluginManager().registerEvents(this, Main.get());
	}
	
	public void cancel() {
		HandlerList.unregisterAll(this);
		Main.get().getDuelGames().deleteGame(this);
		this.challenger = null;
		this.player = null;
	}
	
	public void prepareForBattle() {
		GamePlayer gpc = GamePlayer.get(challenger);
		GamePlayer gp = GamePlayer.get(player);
		
		gp.setDuelRequest(false);
		gp.setDuelRequester(null);
		
		oldClass.put(challenger, gpc.getSelectedClass());
		oldClass.put(player, gp.getSelectedClass());
		
		challenger.teleport(spawn1);
		player.teleport(spawn2);
		
		gpc.setInDuel(true);
		gp.setInDuel(true);
		gpc.setEnergy(0);
		gp.setEnergy(0);
		gpc.setCanGainEnergy(false);
		gp.setCanGainEnergy(false);
		if(selectedClass == null) {
			selectedClass = Main.getGame().gameClasses
					.get(MathUtils.random(new ArrayList<>(Main.getGame().gameClasses
							.keySet())));
			
			if(blackListedClasses.contains(selectedClass)) {
				while(blackListedClasses.contains(selectedClass)) {
					selectedClass = Main.getGame().gameClasses
							.get(MathUtils.random(new ArrayList<>(Main.getGame().gameClasses
									.keySet())));
				}
			}
			if(selectedClass == null) {
				selectedClass = Main.getGame().gameClasses
						.get(MathUtils.random(new ArrayList<>(Main.getGame().gameClasses
								.keySet())));
			}
		}
		GamePlayer.get(challenger).setClass(selectedClass);
		GamePlayer.get(challenger).setClassType(selectedClass.getType());
		GamePlayer.get(player).setClass(selectedClass);
		GamePlayer.get(player).setClassType(selectedClass.getType());
		
		Main.getGame().getPlayers().forEach(gpp -> Main.getGame().updateTeams(gpp.getPlayer()));
		
		InventoryUtils.nullInventory(player);
		InventoryUtils.nullInventory(challenger);
		
		selectedClass.giveItems(player);
		selectedClass.giveItems(challenger);
		
		Imobilizer.ImobilizePlayerClear(player, 7*20);
		Imobilizer.ImobilizePlayerClear(challenger, 7*20);
		new BukkitRunnable() {
			int step = 8;
			@Override
			public void run() {
				--step;
				if(step == 0) {
					sendGameMessage("&c&lLute!");
					playSound(Sound.ENDERDRAGON_GROWL, .2f);
					start();
					cancel();
				}else {
					if(step > 1)
					 sendGameMessage("&eO duelo começa em &6" + step + " &esegundos!");
					else
						sendGameMessage("&eO duelo começa em &6" + step + " &esegundo!");
					playSound(Sound.NOTE_PLING, .2f);
				}
			}
		}.runTaskTimer(Main.get(), 0, 20);
	}
	
	public void start() {
		state = State.IN_GAME;
		GamePlayer gpc = GamePlayer.get(challenger);
		GamePlayer gp = GamePlayer.get(player);
		
		if(allowSkills) {
			gpc.setCanGainEnergy(true);
			gp.setCanGainEnergy(true);
		}
	}
	
	public void end(Player winner) {
		state = State.ENDING;
		
		GamePlayer gpw = GamePlayer.get(winner);
		GamePlayer gpd = winner == challenger ? GamePlayer.get(player) : GamePlayer.get(challenger);
		
		Runner.make(Main.get()).delay(5).run(() -> {
			
			Util.sendTitle(gpw.getPlayer(), 8, 40, 6, "", "&aVocê venceu o duelo!");
			Util.sendTitle(gpd.getPlayer(), 8, 40, 6, "", "&cVocê perdeu o duelo");
			
			gpw.addBalance(250, true, "venceu um duelo");
			gpd.addBalance(50, true, "lutou em um duelo");
			
			gpw.addExp(25, true, "venceu um duelo");
			gpd.addExp(5, true, "lutou em um duelo");
			
			gpw.setInDuel(false);
			gpd.setInDuel(false);
			
			gpd.getPlayer().teleport(Main.mapConfig.spawn, TeleportCause.PLUGIN);
			gpd.giveSpawnItems();
			
			gpw.getPlayer().teleport(Main.mapConfig.spawn, TeleportCause.PLUGIN);
			gpw.giveSpawnItems();
			
			StatsNPC.spawnPlayerNPCOnly(gpw.getPlayer());
			StatsNPC.spawnPlayerNPCOnly(gpd.getPlayer());
			
			gpd.setCanGainEnergy(true);
			gpw.setCanGainEnergy(true);
		});

		gpw.getPlayer().sendMessage(MessageUtil.color("&a&m------------------------------------------------"));
		MessageUtil.sendCentredMessage(gpw.getPlayer(), "&f&lVocê venceu o duelo!");
		gpw.getPlayer().sendMessage(" ");
		MessageUtil.sendCentredMessage(gpw.getPlayer(), "&e+25 exp!");
		MessageUtil.sendCentredMessage(gpw.getPlayer(), "&e+250 coins!");
		gpw.getPlayer().sendMessage(MessageUtil.color("&a&m------------------------------------------------"));

		gpd.getPlayer().sendMessage(MessageUtil.color("&a&m------------------------------------------------"));
		MessageUtil.sendCentredMessage(gpd.getPlayer(), "&f&lVocê perdeu o duelo.");
		gpd.getPlayer().sendMessage(" ");
		MessageUtil.sendCentredMessage(gpd.getPlayer(), "&e+5 exp");
		MessageUtil.sendCentredMessage(gpd.getPlayer(), "&e+50 coins");
		gpd.getPlayer().sendMessage(MessageUtil.color("&a&m------------------------------------------------"));

		gpw.setState(new GamePlayer.PlayerState(gpw, GamePlayer.PlayerState.State.SAFE, 0L));
		gpd.setState(new GamePlayer.PlayerState(gpd, GamePlayer.PlayerState.State.SAFE, 0L));
		HandlerList.unregisterAll(this);

		Runner.make(Main.get()).delay(60).run(() -> Main.get().getDuelGames().deleteGame(this));
	}

	public void playerQuit(Player quitter) {
		Player winner = quitter == challenger ? player : challenger;

		GamePlayer gpw = GamePlayer.get(winner);
		gpw.getPlayer().sendMessage(MessageUtil.color("&a&m------------------------------------------------"));
		MessageUtil.sendCentredMessage(gpw.getPlayer(), "&f&lVocê venceu o duelo!");
		MessageUtil.sendCentredMessage(gpw.getPlayer(), "&e&lSeu inimigo saiu durante a luta!");
		gpw.getPlayer().sendMessage(" ");
		MessageUtil.sendCentredMessage(gpw.getPlayer(), "&e+25 exp!");
		MessageUtil.sendCentredMessage(gpw.getPlayer(), "&e+250 coins!");
		gpw.getPlayer().sendMessage(MessageUtil.color("&a&m------------------------------------------------"));

		gpw.setState(new GamePlayer.PlayerState(gpw, GamePlayer.PlayerState.State.SAFE, 0L));

		Runner.make(Main.get()).delay(5).run(() -> {

			Util.sendTitle(gpw.getPlayer(), 8, 40, 6, "", "&aVocê venceu o duelo!");

			gpw.addBalance(250, true, "venceu um duelo");

			gpw.addExp(25, true, "venceu um duelo");

			gpw.setInDuel(false);

			gpw.getPlayer().teleport(Main.mapConfig.spawn, TeleportCause.PLUGIN);
			gpw.giveSpawnItems();

			StatsNPC.spawnPlayerNPCOnly(gpw.getPlayer());

			gpw.setCanGainEnergy(true);
		});

		Runner.make(Main.get()).delay(60).run(() -> Main.get().getDuelGames().deleteGame(this));
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {
		playerQuit(e.getPlayer());
	}
	
	@EventHandler
	public void onDeath(OverlandsDeathEvent e) {
		CombatLog log = e.getLog();
		if(log.getPlayer().get() == challenger) {
			end(player);
		}else {
			end(challenger);
		}
	}
	
	public void sendGameMessage(String msg) {
		challenger.sendMessage(MessageUtil.color(msg));
		player.sendMessage(MessageUtil.color(msg));
	}
	
	public void playSound(Location loc, Sound sound, float pitch) {
		challenger.playSound(loc, sound, 1, pitch);
		player.playSound(loc, sound, 1, pitch);
	}
	
	public void playSound(Sound sound, float pitch) {
		challenger.playSound(challenger.getLocation(), sound, 1, pitch);
		player.playSound(player.getLocation(), sound, 1, pitch);
	}
	
	private enum State {
		STARTING,
		IN_GAME,
		ENDING;
	}

	public void invite() {
		DuelController.createInvite(GamePlayer.get(challenger), GamePlayer.get(player));
	}
	
	public void setAllowSkills(boolean x) {
		this.allowSkills = x;
	}
	
	public void setClass(GameClass zz) {
		this.selectedClass = zz;
	}
	
	public void addBlacklisted(GameClass zz) {
		this.blackListedClasses.add(zz);
	}
	
	public void setPlayer(Player player) {
		this.player = player;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public Player getChallenger() {
		return challenger;
	}
	
	public boolean allowSkills() {
		return allowSkills;
	}
	
	public List<GameClass> getBlacklistedClasses() {
		return blackListedClasses;
	}
	
	public GameClass getClazz() {
		return selectedClass;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public void setSpawn1(Location loc) {
		this.spawn1 = loc;
	}
	
	public void setSpawn2(Location loc) {
		this.spawn2 = loc;
	}
	
	@EventHandler
	public void onBow(EntityShootBowEvent e) {
		if(e.getEntity() instanceof Player) {
			if(e.getEntity() == challenger || e.getEntity() == player) {
				if(state == State.STARTING) {
					e.setCancelled(true);
				}
			}
		}
	}
}

