package com.overlands.kitpvp.duels;

import java.io.File;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import com.google.common.collect.Lists;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Runner;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.world.WorldUtils;
import com.overlands.kitpvp.Main;

public class DuelGameManager {
	
	private List<DuelGame> games;

	public DuelGameManager() {
		this.games = Lists.newArrayList();
	}
	
	public List<DuelGame> getGames() {
		return games;
	}
	
	public void createGame(DuelGame game) {
		int id = games.size() + MathUtils.randomRangeInt(0, 9999);
		String name = "Fractal" + id;
		File source = new File(Main.get().getDataFolder() + File.separator + "maps" + File.separator + "Fractal");
		File target = new File(Main.get().getServer().getWorldContainer().getAbsolutePath(), name);
		if(target.exists()) {
			//target.mkdirs();
			createGame(game);
			return;
		}
		
		WorldUtils.copyWorld(source, target);
		boolean loaded = WorldUtils.loadWorld(name);
		if (loaded) {
			game.setId(id);
			games.add(game);
			game.sendGameMessage(MessageUtil.color("&ePreparando batalha..."));
			Runner.make(Main.get()).delay(15).run(() -> {
				Location spawn1 = new Location(Bukkit.getWorld(name), 0, 65, -38, 1.1f, -2.4f);
				Location spawn2 = new Location(Bukkit.getWorld(name), 0, 65, 39, -179f, 1.0f);
				game.setSpawn1(spawn1);
				game.setSpawn2(spawn2);
				game.prepareForBattle();
			});
		}else {
			game.sendGameMessage("&c7Ocorreu um erro ao preparar para batalha, duelo cancelado.");
			game.cancel();
		}
	}
	
	public void deleteGame(DuelGame game) {
		String name = "Fractal" + game.getId();
		WorldUtils.deleteWorld(name);
		games.remove(game);
	}
}
