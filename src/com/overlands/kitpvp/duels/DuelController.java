package com.overlands.kitpvp.duels;

import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

import com.overlands.core.utils.JsonBuilder;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.JsonBuilder.ClickAction;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;

public class DuelController {

	public static void createInvite(GamePlayer player, GamePlayer target) {
		
		target.setDuelRequest(true);
		target.setDuelRequester(target);
		
		JsonBuilder json = new JsonBuilder(player.getPlayer().getName())
				.withColor(ChatColor.AQUA)
				.withText(" desafiou você para 1v1. ").withColor(ChatColor.YELLOW);
		
		JsonBuilder json2 = new JsonBuilder("Clique para ").withColor(ChatColor.YELLOW)
				.withText("ACEITAR").withColor(ChatColor.GREEN).withColor(ChatColor.BOLD)
				.withClickEvent(ClickAction.RUN_COMMAND, "/duelo aceitar " + player.getPlayer().getName())
				.withText(" ou ").withColor(ChatColor.YELLOW)
				.withText("NEGAR").withColor(ChatColor.RED).withColor(ChatColor.BOLD)
				.withClickEvent(ClickAction.RUN_COMMAND, "/duelo negar " + player.getPlayer().getName())
				.withText(".").withColor(ChatColor.YELLOW);
		
		json.sendJson(target.getPlayer());
		json2.sendJson(target.getPlayer());
		
		new BukkitRunnable() {
			int seconds = 0;
			@Override
			public void run() {
				++seconds;
				if(seconds == 60 || target.hasDuelRequest()) {
					cancel();
					return;
				}
				if(seconds >= 0 && seconds % 25 == 0) {
					json.sendJson(target.getPlayer());
					json2.sendJson(target.getPlayer());
				}
			}
		}.runTaskTimer(Main.get(), 0, 20);
	}
	
	public static void denyInvite(GamePlayer inviter, GamePlayer target) {
		if(target.hasDuelRequest()) {
			target.setDuelRequest(false);
			target.setDuelRequester(null);
			if(inviter != null) {
				inviter.getPlayer().sendMessage(ChatColor.RED + target.getPlayer().getName() + MessageUtil.color(" &7negou seu desafio."));
			}
		}
	}
	
	public static void accept(GamePlayer inviter, GamePlayer target) {
		if(DuelManager.builders.containsKey(inviter.getPlayer())) {
			Main.get().getDuelGames().createGame(DuelManager.builders.get(inviter.getPlayer()).get());
			DuelManager.builders.remove(inviter.getPlayer());
		}
	}
}
