package com.overlands.kitpvp.listeners;

public class ChatMessage {
	
	private String message;
	private long timeSent;

	public ChatMessage(String message) {
		this(message, System.currentTimeMillis());
	}

	public ChatMessage(String message, long timeSent) {
		this.message = message;
		this.timeSent = timeSent;
	}

	public String getMessage() {
		return message;
	}

	public long getTimeSent() {
		return timeSent;
	}
}
