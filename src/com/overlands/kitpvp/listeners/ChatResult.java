package com.overlands.kitpvp.listeners;

public enum ChatResult {
	
	DENY_TOGGLED,
	DENY_PERSONAL_MUTED,
	DENY_GLOBAL_MUTED,
	DENY_PLAYER_MUTED,
	DENY_BLOCKED,
	DENY_SLOW,
	DENY_SPAM,
	DENY_INVALID,
	ALLOW_MODIFIED,
	ALLOW;
	

}
