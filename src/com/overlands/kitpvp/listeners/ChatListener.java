package com.overlands.kitpvp.listeners;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.google.common.collect.Lists;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.calc.TimeUtils;
import com.overlands.core.utils.calc.TimeUtils.TimeUnit;
import com.overlands.core.utils.scheduler.SchedulerEvent;
import com.overlands.core.utils.scheduler.UpdateType;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.event.OverlandsChatEvent;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.levels.LevelFormatter;
import com.overlands.kitpvp.ranks.Rank;

public class ChatListener implements Listener {

	private List<String> goodMessagesSmile = Lists.newArrayList();
	private HashMap<UUID, ChatMessage> lastPlayerMessage = new HashMap<UUID, ChatMessage>();
	private List<UUID> staffChat = Lists.newArrayList();
	
	private String[] hack = {"hack", "hax", "hacker", "hacking", "cheat", "cheater", "cheating", "killaura", "forcefield", "flyhack", "flyhacking", "autoclick", "aimbot"};
	private String[] offenses = {"lixo", "ez", "easy", "eazy", "izi", "fácil", "ruim", "troxa", "trouxa"};
	
	private int chatSlow = 0;
	private long silenced = 0;
	
	public ChatListener() {
		String[] goodMessages = new String[] {
				"Eu curto minhas publicações... :/",
				"Espero que todos se divirtam!",
				"Ótimo jogo!",
				"Que dia lindo para jogar!",
				"Estou com muito ódio no coração você poderia me ajudar?",
				"Não acredito como estou feliz hoje!",
				"Estou tentando ser uma pessoa melhor...",
				
		};
		
		goodMessagesSmile.addAll(Arrays.asList(goodMessages));
	}

	public boolean matches(String[] values, String msg) {
		msg = " " + msg.toLowerCase().replaceAll("[^a-z ]", "") + " ";
		for (String s : values) {
			if (msg.contains(" " + s + " ")) {
				return true;
			}
		}
		return false;
	}
	
	public ChatResult canSendMessage(Player client, AsyncPlayerChatEvent e) {
		/**
		 * if (lastPlayerMessage.containsKey(e.getPlayer().getUniqueId())) {
		 * 			ChatMessage lastMessage = lastPlayerMessage.get(e.getPlayer().getUniqueId());
		 * 			long chatSlowTime = 1000L * chatSlow;
		 * 			long timeDiff = System.currentTimeMillis() - lastMessage.getTimeSent();
		 * 			if (timeDiff < chatSlowTime && !Rank.isStaff(client)) {
		 * 				client.sendMessage(MessageUtil.color("&9O chat está no slowmode. Aguarde " + TimeUtils.convertPrString(chatSlowTime - timeDiff, 1, TimeUnit.FIT)));
		 * 				return ChatResult.DENY_SLOW;
		 *                        }* 		}
		 * 		if(isInSilence(client)) {
		 * 			return ChatResult.DENY_GLOBAL_MUTED;
		 *        }
		 * 		if(!Main.getEnergyManager().use(e.getPlayer(), "Chat Message", 300, false, false)) {
		 * 			if(!Rank.isStaff(client)) {
		 * 				client.sendMessage(MessageUtil.color("&cMantenha o chat limpo, evite spam!"));
		 * 				return ChatResult.DENY_SPAM;
		 *            }
		 *        }
		 * 		if(matches(hack, e.getMessage())) {
		 * 			client.sendMessage(MessageUtil.color("&cEncontrou um hacker? Reporte-o com comando &6/reportar&c ou visite os fóruns para reportar."));
		 * 			return ChatResult.DENY_BLOCKED;
		 *        }
		 * 		if(matches(offenses, e.getMessage())) {
		 * 			e.setMessage(goodMessagesSmile.get(MathUtils.r(goodMessagesSmile.size() - 1)));
		 * 			return ChatResult.ALLOW_MODIFIED;
		 *        }
		 * 		if(StringUtils.isAllUpperCase(e.getMessage()) && !Rank.isStaff(client)) {
		 * 			e.setMessage(WordUtils.capitalize(e.getMessage()));
		 * 			return ChatResult.ALLOW_MODIFIED;
		 *        }
		 * 		if(GamePlayer.get(client).isChatToggled()) {
		 * 			client.sendMessage(MessageUtil.color("&cSeu chat está desligado."));
		 * 			return ChatResult.DENY_PERSONAL_MUTED;
		 *        }
		 */

		return ChatResult.ALLOW;
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onChat(AsyncPlayerChatEvent e) {
		
		GamePlayer gp = GamePlayer.get(e.getPlayer());
		OverlandsChatEvent event = new OverlandsChatEvent(e.getPlayer(), canSendMessage(e.getPlayer(), e), e.getMessage());
		
		if(staffChat.contains(e.getPlayer().getUniqueId())) {
			Main.get().getServerManager().callCancellable(event);
			
			String format = MessageUtil.color("&8[&6" + gp.getClassTypeFromEnum().toString().substring(0, 3).toUpperCase() + "&8] &7") + LevelFormatter.format(gp.getLevel(), gp.getPrestigeLevel(), false, false) + " " + ChatColor.GRAY + gp.getPlayer().getName() + ": " + ChatColor.RESET + e.getMessage();
			Bukkit.getOnlinePlayers().stream().filter(Rank::isStaff).forEach((players) -> {
				if(!event.isCancelled()) {
					players.sendMessage(MessageUtil.color("&9[STAFF] " + format));
				}
			});
			e.setCancelled(true);
		    return;
		}
		
		if(!Rank.isStaff(e.getPlayer()) && canSendMessage(e.getPlayer(), e) != ChatResult.ALLOW && canSendMessage(e.getPlayer(), e) != ChatResult.ALLOW_MODIFIED) {
			Main.get().getServerManager().callCancellable(event);
			event.setCancelled(true);
			e.setCancelled(true);
			return;
		}
		Main.get().getServerManager().callCancellable(event);
		String format = MessageUtil.color(Rank.fromName(gp.getRank()).getChatTag() + "&8[&6" + gp.getClassTypeFromEnum().toString().substring(0, 3).toUpperCase() + "&8] &7") + LevelFormatter.format(gp.getLevel(), gp.getPrestigeLevel(), false, false) + " " + ChatColor.GRAY + gp.getPlayer().getName() + ": " + ChatColor.RESET + e.getMessage();
		Bukkit.getOnlinePlayers().forEach((players) -> {
			if(!event.isCancelled()) {
				players.sendMessage(Rank.isStaff(players) ? MessageUtil.color(format) : format);
			}
		});
		
		lastPlayerMessage.put(e.getPlayer().getUniqueId(), new ChatMessage(e.getMessage()));
		
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onUpdate(SchedulerEvent event) {
		if (event.getType() != UpdateType.FAST)
			return;

		checkForEnd();
	}

	public void checkForEnd() {
		if (silenced <= 0)
			return;

		if (System.currentTimeMillis() > silenced)
			silence(0, true);
	}
	
	public void silence(long duration, boolean inform) {
		if (duration > 0)
			silenced = System.currentTimeMillis() + duration;
		else
			silenced = duration;

		if (!inform)
			return;

		if (duration == -1)
			Bukkit.broadcastMessage(MessageUtil.color("&9O chat foi desativado por tempo indeterminado."));
		else if (duration == 0)
			Bukkit.broadcastMessage(MessageUtil.color("&9O chat foi reativado."));
		else
			Bukkit.broadcastMessage(MessageUtil.color("&9O chat foi silenciado por &e" + TimeUtils.makePrStr(duration, 1) + "."));
	}
	
	public boolean isInSilence(Player client) {
		checkForEnd();
		
		if (silenced == 0)
			return false;
		
		if(Rank.isStaff(client))
			return false;
		
		if(silenced == -1)
			client.sendMessage(MessageUtil.color("&9O chat foi desativado por tempo indeterminado."));
		else
			client.sendMessage(MessageUtil.color("&9O chat está silenciado por mais &e" + TimeUtils.makePrStr(silenced - System.currentTimeMillis(), 1) + "."));
		
		return true;
	}
	
	public void setChatSlow(int seconds, boolean inform) {
		if (seconds < 0)
			seconds = 0;

		chatSlow = seconds;

		if (inform) {
			if (seconds == 0)
				Bukkit.broadcastMessage(MessageUtil.color("&9O modo slow foi desativado."));
			else
				Bukkit.broadcastMessage(MessageUtil.color("&9O chat foi colocado no slowmode de &e" + seconds + " &9segundos."));
		}
	}
	
	public List<UUID> getStaffChat() {
		return staffChat;
	}
	
	public HashMap<UUID, ChatMessage> getChatMessages() {
		return lastPlayerMessage;
	}
}
