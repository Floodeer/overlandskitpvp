package com.overlands.kitpvp;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import com.overlands.core.utils.calc.TimeUtils;
import com.overlands.kitpvp.manager.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.plugin.java.JavaPlugin;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.reflect.FieldAccessException;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.gmail.filoghost.holographicdisplays.api.placeholder.PlaceholderReplacer;
import com.google.common.collect.Lists;
import com.overlands.core.damage.DamageManager;
import com.overlands.core.damage.combat.CombatManager;
import com.overlands.core.utils.GlowUtils;
import com.overlands.core.utils.Imobilizer;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.Runner;
import com.overlands.core.utils.block.explosions.BlockRestore;
import com.overlands.core.utils.block.explosions.Explosions;
import com.overlands.core.utils.bouncing.BouncingProjectile;
import com.overlands.core.utils.calc.MathUtils;
import com.overlands.core.utils.calc.particle.ParticleListen;
import com.overlands.core.utils.menus.IconCore;
import com.overlands.core.utils.scheduler.Updater;
import com.overlands.core.utils.world.WorldUtils;
import com.overlands.kitpvp.database.DataManager;
import com.overlands.kitpvp.database.SQLite;
import com.overlands.kitpvp.database.interfaces.IDatabase;
import com.overlands.kitpvp.duels.DuelGameManager;
import com.overlands.kitpvp.duels.DuelManager;
import com.overlands.kitpvp.effects.EffectSelector;
import com.overlands.kitpvp.game.Game;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.game.classes.ClassSelector;
import com.overlands.kitpvp.game.levels.LevelFormatter;
import com.overlands.kitpvp.listeners.ChatListener;
import com.overlands.kitpvp.mysterybox.MysteryBox;
import com.overlands.kitpvp.perks.PerkSelector;
import com.overlands.kitpvp.preferences.SettingsMenu;
import com.overlands.kitpvp.seasons.XmasEvents;
import com.overlands.kitpvp.skins.ProfileChanger;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;

import de.slikey.effectlib.EffectLib;
import de.slikey.effectlib.EffectManager;
import floodeer.arcade.core.bossbar.BossBarCore;

public class Main extends JavaPlugin {
	
	private static Main main;
	
	public static String nmsver;
	public static MapConfig mapConfig;
	public static ClassConfig classConfig;
	public static boolean RUNNING_BOOSTER = false;
	public static boolean OFFLINE_MODE;
	public static boolean LOG = true;
	public static boolean ENABLE_LOGIN_SYSTEM = false;
	
	private static LeaderboardManager leaderboardManager;
	private static DataManager dm;
	private static Game game;
	private static PlayerManager pm;
	private static EnergyManager em;
	private static ClassSelector classSelector;
	private static IconCore icons;
	private static SettingsMenu pref;
	private static ProfileManager profm;
	private static ProfileChanger profileManager;
	
	private DuelManager dem;
	private IDatabase db;
	private ChatListener cl;
	private ServerManager sv;
	private DamageManager ddm;
	private MusicManager music;
	private Explosions exp;
	private DuelGameManager duels;
	private CommandManager cmd;
	
	private WorldEditPlugin we;
	
	public EffectManager effectManager;
	
	public static ProfileChanger getProfileManager() {
		return profileManager;
	}
	
	public DuelGameManager getDuelGames() {
		return duels;
	}
	
	public DuelManager getDuels() {
		return dem;
	}
	
	public static SettingsMenu getPreferences() {
		return pref;
	}
	
	public static ProfileManager getProfile() {
		return profm;
	}
	
	public static LeaderboardManager getLB() {
		return leaderboardManager;
	}
	
	public static PlayerManager getPM() {
		return pm;
	}
	
	public static IconCore getIconCore() {
		return icons;
	}
	
	public ChatListener getChatCore() {
		return cl;
	}
	
	public static EnergyManager getEnergyManager() {
		return em;
	}
	
	public static DataManager getDataManager() {
		return dm;
	}

	public static ClassSelector getClassSelector() {
		return classSelector;
	}
	
	public static Game getGame() {
		return game;
	}
	
	public static Main get() {
		return main;
	}
	
	public WorldEditPlugin getWE() {
		return we;
	}
	
	public IDatabase getDB() {
		return db;
	}
	
	public Explosions getExplosionManager() {
		return exp;
	}
	
	public MusicManager getMusicCore() {
		return music;
	}
	
	public ServerManager getServerManager() {
		return sv;
	}
	
	public DamageManager getDamageManager() {
		return ddm;
	}
	
	public CommandManager getCommandManager() {
		return cmd;
	}
	@Override
	public void onEnable() {
		super.onEnable();
	    OFFLINE_MODE = !Bukkit.getOnlineMode();
		nmsver = Bukkit.getServer().getClass().getPackage().getName();
		nmsver = nmsver.substring(nmsver.lastIndexOf(".") + 1);
		main = this;
		mapConfig = new MapConfig(new File(getDataFolder(), "config.yml"));
		try {
			mapConfig.load();
		} catch (InvalidConfigurationException e1) {
			e1.printStackTrace();
		}

		classConfig = new ClassConfig(new File(getDataFolder(), "classconfig.yml"));
		try {
			classConfig.load();
		}catch(InvalidConfigurationException e1) {
			e1.printStackTrace();
		}
		GlowUtils.register();
		
		try {
			db = new SQLite();
			db.createTables();
		} catch (ClassNotFoundException | SQLException | IOException e) {

			e.printStackTrace();
		}
		cmd = new CommandManager();
		
		pm = new PlayerManager();
		em = new EnergyManager();
		sv = new ServerManager();
		dm = new DataManager(db);
		leaderboardManager = new LeaderboardManager();
		classSelector = new ClassSelector();
		icons = new IconCore();
		profm = new ProfileManager();
		pref = new SettingsMenu();
		cl = new ChatListener();
		exp = new Explosions(new BlockRestore());
		music = new MusicManager();
		ddm = new DamageManager(new CombatManager());
		profileManager = new ProfileChanger();
		duels = new DuelGameManager();
		EffectLib lib = EffectLib.instance();
		effectManager = new EffectManager(lib);
		dem = new DuelManager();

		new XmasEvents();

		Bukkit.getPluginManager().registerEvents(icons, this);
		Bukkit.getPluginManager().registerEvents(cl, this);
		Bukkit.getPluginManager().registerEvents(dem, this);
		Bukkit.getPluginManager().registerEvents(classSelector, this);
		Bukkit.getPluginManager().registerEvents(new ParticleListen(), this);
		Bukkit.getPluginManager().registerEvents(new MysteryBox(), this);
		Bukkit.getPluginManager().registerEvents(new BossBarCore(), this);
		Bukkit.getPluginManager().registerEvents(new PerkSelector(), this);
		Bukkit.getPluginManager().registerEvents(new EffectSelector(), this);
		Bukkit.getPluginManager().registerEvents(new BouncingProjectile(), this);
		Bukkit.getPluginManager().registerEvents(new Imobilizer(), this);
		Bukkit.getPluginManager().registerEvents(new AdminManager(), this);
		game = new Game();
		Bukkit.getPluginManager().registerEvents(game, this);
		Bukkit.getPluginManager().registerEvents(em, this);
		we = (WorldEditPlugin) Bukkit.getPluginManager().getPlugin("WorldEdit");
		initHD();
		
		Runner.make(this).delay(1).run(new Updater(this));
		WorldUtils.putAll();
		runTips();
		
		Bukkit.getScheduler().runTaskTimerAsynchronously(this, () -> Bukkit.getOnlinePlayers().forEach(player ->  {
            if(getPM().getUUIDs().contains(player.getUniqueId())) {
                GamePlayer gp = GamePlayer.get(player);
                gp.getLoadedTables().forEach(table -> dm.savePlayerAsync(gp, table));
            }
        }), 20 * 20, 600 * 20);
		 
		 ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(this, ListenerPriority.NORMAL, PacketType.Play.Client.TAB_COMPLETE) {
				@Override
				public void onPacketReceiving(PacketEvent event) {
					if (event.getPacketType() == PacketType.Play.Client.TAB_COMPLETE) {
						try {
							PacketContainer packet = event.getPacket();
							String message = packet.getStrings().read(0).toLowerCase();
							if (((message.startsWith("/")) && (!message.contains(" ")))
									|| ((message.startsWith("/ver")) && (!message.contains("  ")))
									|| ((message.startsWith("/version")) && (!message.contains("  ")))
									|| ((message.startsWith("/?")) && (!message.contains("  ")))
									|| ((message.startsWith("/about")) && (!message.contains("  ")))
									|| ((message.startsWith("/help")) && (!message.contains("  ")))) {
								event.setCancelled(true);
							}
						} catch (FieldAccessException e) {
						}
					}
				}
			});
	}
	
	@Override
	public void onDisable() {
		getExplosionManager().getBlockRestore().restoreAll();
		Bukkit.getOnlinePlayers().forEach(player -> {
			try {
				dm.savePlayer(GamePlayer.get(player));
			}catch(Exception ignored) {
			}
		});
		getGame().shutdown();
		pm = null;

	}
	
	private void initHD() {
		for(int i = 0; i < 15; i++) {
			int p = i;
			HologramsAPI.registerPlaceholder(Main.get(), "{player_kills_" + (p + 1) + "}", 15D, () -> {
                String r = MessageUtil.color("&e " + (p+1) + ". &7Aguardando...");
                try {
                    if (getLB().getTopList(LeaderboardManager.LeaderType.KILLS).size() > p) {
                        int kills = getLB().getTopList(LeaderboardManager.LeaderType.KILLS).get(p).getKills();
                        String name = getLB().getTopList(LeaderboardManager.LeaderType.KILLS).get(p).getName();
                        return MessageUtil.color("&e" + (p+1) + "." + " &b" + name + " &7- " + "&b" + kills + " &ekill" + (kills == 0 || kills > 1 ? "s" : ""));
                    }
                }catch(Exception ex) {
                    return r;
                }
                return r;
            });

			HologramsAPI.registerPlaceholder(Main.get(), "{player_score_" + (p + 1) + "}", 15D, () -> {
				String r = MessageUtil.color("&e " + (p+1) + ". &7Aguardando...");
				try {
					if (getLB().getTopList(LeaderboardManager.LeaderType.SCORE).size() > p) {
						int score = getLB().getTopList(LeaderboardManager.LeaderType.SCORE).get(p).getScore();
						String name = getLB().getTopList(LeaderboardManager.LeaderType.SCORE).get(p).getName();
						return MessageUtil.color("&e" + (p+1) + "." + " &b" + name + " &7- " + "&b" + score + " &eponto" + (score == 0 || score > 1 ? "s" : ""));
					}
				}catch(Exception ex) {
					return r;
				}
				return r;
			});

			HologramsAPI.registerPlaceholder(Main.get(), "{player_level_" + (p + 1) + "}", 15D, () -> {
				String r = MessageUtil.color("&e " + (p+1) + ". &7Aguardando...");
				try {
					if (getLB().getTopList(LeaderboardManager.LeaderType.GENERAL_LEVEL).size() > p) {
						int level = getLB().getTopList(LeaderboardManager.LeaderType.GENERAL_LEVEL).get(p).getLevel();
						String name = getLB().getTopList(LeaderboardManager.LeaderType.GENERAL_LEVEL).get(p).getName();
						int prestige = getLB().getData(name, LeaderboardManager.LeaderType.PRESTIGE_LEVEL).getPrestigeLevel();
						return MessageUtil.color("&e" + (p+1) + "." + " &b" + name + " &7- " + "&b" + LevelFormatter.format(level, prestige, false, true));
					}
				}catch(Exception ex) {
					return r;
				}
				return r;
			});

			HologramsAPI.registerPlaceholder(Main.get(), "{player_timePlayed_" + (p + 1) + "}", 15D, () -> {
				String r = MessageUtil.color("&e " + (p+1) + ". &7Aguardando...");
				try {
					if (getLB().getTopList(LeaderboardManager.LeaderType.TIME_PLAYED).size() > p) {
						long timePlayed = getLB().getTopList(LeaderboardManager.LeaderType.TIME_PLAYED).get(p).getTimePlayed();
						String name = getLB().getTopList(LeaderboardManager.LeaderType.TIME_PLAYED).get(p).getName();
						return MessageUtil.color("&e" + (p+1) + "." + " &b" + name + " &7- " + "&b" + timePlayed);
					}
				}catch(Exception ex) {
					return r;
				}
				return r;
			});

			HologramsAPI.registerPlaceholder(Main.get(), "{player_damage_" + (p + 1) + "}", 15D, () -> {
				String r = MessageUtil.color("&e " + (p+1) + ". &7Aguardando...");
				try {
					if (getLB().getTopList(LeaderboardManager.LeaderType.DAMAGE_DONE).size() > p) {
						double damage = getLB().getTopList(LeaderboardManager.LeaderType.DAMAGE_DONE).get(p).getDamageDone();
						String name = getLB().getTopList(LeaderboardManager.LeaderType.DAMAGE_DONE).get(p).getName();
						return MessageUtil.color("&e" + (p+1) + "." + " &b" + name + " &7- " + "&b" + Math.round(damage) + " &edano");
					}
				}catch(Exception ex) {
					return r;
				}
				return r;
			});

			HologramsAPI.registerPlaceholder(Main.get(), "{player_skills_" + (p + 1) + "}", 15D, () -> {
				String r = MessageUtil.color("&e " + (p+1) + ". &7Aguardando...");
				try {
					if (getLB().getTopList(LeaderboardManager.LeaderType.SKILLS_USED).size() > p) {
						int skills = getLB().getTopList(LeaderboardManager.LeaderType.SKILLS_USED).get(p).getSkillsUsed();
						String name = getLB().getTopList(LeaderboardManager.LeaderType.SKILLS_USED).get(p).getName();
						return MessageUtil.color("&e" + (p+1) + "." + " &b" + name + " &7- " + "&b" + skills + " &eusos");
					}
				}catch(Exception ex) {
					return r;
				}
				return r;
			});
		}
	}
	
	public void runTips() {
		List<String> tips = Lists.newArrayList();
		tips.add("&eJogo travando com partículas e efeitos? Use &c/prefs &ee mude a qualidade dos gráficos.");
		tips.add("&eMude as partículas e efeitos de sua classe na loja de efeitos!");
		tips.add("&eSuba seu nível &c&lPrestige &ea cada 120 níveis.");
		tips.add("&eDesative notificações e jogue como quiser configurando seu perfil com &c/prefs&e.");
		Runner.make(this).delay(20).interval(MathUtils.random(210*20, 360*20)).run(() -> {
			Collections.shuffle(tips);
			Bukkit.broadcastMessage(MessageUtil.color(MathUtils.random(tips)));
			Collections.sort(tips);
		});
	}
}
