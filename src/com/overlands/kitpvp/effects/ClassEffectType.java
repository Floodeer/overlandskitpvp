package com.overlands.kitpvp.effects;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.overlands.core.utils.item.ItemFactory;
import com.overlands.kitpvp.game.GamePlayer;

public enum ClassEffectType {
	
	WATER_BUBBLE("WATER_BUBBLE", false, 1, ItemFactory.create(Material.WATER_BUCKET)),
	DEMON_WINGS("DEMON_WINGS", true, 0.8, ItemFactory.create(Material.FEATHER)),
	DIAMOND_PUNCH("DIAMOND_PUNCH", false, 1.68, ItemFactory.create(Material.DIAMOND_BLOCK)),
	FLAME_STRIKE("FLAME_STRIKE", false, 2.20, ItemFactory.create(Material.BLAZE_POWDER)),
	COLD("COLD", false, 1.75, ItemFactory.create(Material.ICE)),
	FLAME_IMPULSE("FLAME_IMPULSE", false, 1.46, ItemFactory.create(Material.LAVA_BUCKET)),
	LOVE_BURST("LOVE_BURST", false, 3.4, ItemFactory.create(Material.RED_ROSE)),
	FLAME_DUST("FLAME_DUST", false, 1.3, ItemFactory.create(Material.FLINT_AND_STEEL)),
	BLOCK_RUMBLER("BLOCK_RUMBLER", false, 2.7, ItemFactory.create(Material.STAINED_CLAY)),
	EMERALD("EMERALD_BREAH", false, 2.9, ItemFactory.create(Material.EMERALD_BLOCK));
	
	String name;
	boolean playable;
	double rarity;
	ItemStack display;
	
	ClassEffectType(String name) {
		this.name = name;
	}
	
	ClassEffectType(String name, boolean playable) {
		this.name = name;
		this.playable = playable;
	}

	ClassEffectType(String name, boolean playable, double rarity) {
		this.name = name;
		this.playable = playable;
		this.rarity = rarity;
	}

	ClassEffectType(String name, boolean playable, double rarity, ItemStack display) {
		this.name = name;
		this.playable = playable;
		this.rarity = rarity;
		this.display = display;
	}
	
	public ItemStack getDisplay() {
		return display;
	}
	
	public double getRarity() {
		return rarity;
	}
	

	@Override
	public String toString() {
		return name;
	}
	
	public static ClassEffectType fromString(String str) {
		return Arrays.stream(ClassEffectType.values()).filter(f -> f.toString().equalsIgnoreCase(str)).findAny().orElse(null);
	}
	
	public boolean hasSelected(Player p) {
		if(!GamePlayer.get(p).getEffects().contains(name.toUpperCase()))
			return false;
		
		return GamePlayer.get(p).getSelectedEffects().contains(name.toUpperCase());
	}
}
