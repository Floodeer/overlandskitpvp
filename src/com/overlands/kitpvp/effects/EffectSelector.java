package com.overlands.kitpvp.effects;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;
import com.overlands.core.utils.MenuUtils;
import com.overlands.core.utils.MessageUtil;
import com.overlands.core.utils.item.ItemFactory;
import com.overlands.kitpvp.Main;
import com.overlands.kitpvp.game.GamePlayer;
import com.overlands.kitpvp.ranks.Rank;

public class EffectSelector implements Listener {
	
	private static ItemStack no(String name, List<String> lore) {
		lore.add(MessageUtil.color(" "));
		lore.add(MessageUtil.color("§cVocê não tem esse effect."));
		return ItemFactory.create(Material.STAINED_GLASS_PANE, name, lore, (byte)14);
	}
	
	private static boolean has(Player p, ClassEffectType c) {
		return GamePlayer.get(p).getEffects().contains(c.toString().toUpperCase());
	}
	
	private static boolean sel(Player p, ClassEffectType c) {
		return GamePlayer.get(p).getSelectedEffects().contains(c.toString().toUpperCase());
	}
	 
	private static ItemStack green(String name) {
		return ItemFactory.create(Material.INK_SACK, name, (byte)10);
	}
	
	private static ItemStack cyan(String name) {
		return ItemFactory.create(Material.INK_SACK, name, (byte)8);
	}
	
	static ItemStack soon(String nome, String lore) {
		return ItemFactory.create(Material.STAINED_GLASS_PANE, nome, lore, 1, (byte)14);
	}
	
	static ItemStack soonDye() {
		return ItemFactory.create(Material.INK_SACK, MessageUtil.color("§c???"), (byte)5);
	}
	
	static boolean seach(Player p, ClassEffectType a) {
		return GamePlayer.get(p).getSelectedEffects().contains(a.toString().toUpperCase());
	}

	private static List<String> getInfo(ClassEffectType effect) {
		ArrayList<String> effectInfo = Lists.newArrayList();
		switch (effect) {
		case DEMON_WINGS:
			effectInfo.add("§7Classe: §bDreadlord");
			effectInfo.add(" ");
			effectInfo.add("§7Após ativar a habilidade");
			effectInfo.add("§7recebe partículas em");
			effectInfo.add("§7formato de asas por");
			effectInfo.add("§e3s§7.");
			break;
		case DIAMOND_PUNCH:
			effectInfo.add("§7Classe: §bGolem");
			effectInfo.add(" ");
			effectInfo.add("§7Os blocos de ferro da");
			effectInfo.add("§7habilidade serão ");
			effectInfo.add("§7transformados em");
			effectInfo.add("§7blocos de diamante.");
			break;
		case FLAME_STRIKE:
			effectInfo.add("§7Classe: §bPaladin");
			effectInfo.add(" ");
			effectInfo.add("§7As partículas da");
			effectInfo.add("§7habilidade serão");
			effectInfo.add("§7transformadas para ");
			effectInfo.add("§7partículas de fogo.");

			break;
		case WATER_BUBBLE:
			effectInfo.add("§7Classe: §bPigman");
			effectInfo.add(" ");
			effectInfo.add("§7As partículas da");
			effectInfo.add("§7habilidade serão");
			effectInfo.add("§7transformadas para ");
			effectInfo.add("§7partículas de água.");
			break;
		case COLD:
			effectInfo.add("§7Classe: §bCryomancer");
			effectInfo.add(" ");
			effectInfo.add("§7Dropa blocos de gelo");
			effectInfo.add("§7ao acertar a players");
			effectInfo.add("§7com a habilidade. ");
			break;
		case FLAME_IMPULSE:
			effectInfo.add("§7Classe: §bMagnatition");
			effectInfo.add(" ");
			effectInfo.add("§7Muda as partículas");
			effectInfo.add("§7para partículas");
			effectInfo.add("§7de fogo. ");
			break;
		case LOVE_BURST:
			effectInfo.add("§7Classe: §bBlaze");
			effectInfo.add(" ");
			effectInfo.add("§7Muda as linhas de fogo");
			effectInfo.add("§7para linhas de");
			effectInfo.add("§7coração. ");
			break;
		case BLOCK_RUMBLER:
			effectInfo.add("§7Classe: §bRumbler");
			effectInfo.add(" ");
			effectInfo.add("§7Muda os blocos da trilha");
			effectInfo.add("§7criada pelo abalo.");
			break;
		case FLAME_DUST:
			effectInfo.add("§7Classe: §bWizard");
			effectInfo.add(" ");
			effectInfo.add("§7Muda as partículas para");
			effectInfo.add("§7partículas de fogo.");
			break;
		case EMERALD:
			effectInfo.add("§7Classe: §bAncient");
			effectInfo.add(" ");
			effectInfo.add("§7Dropa blocos de esmeralda");
			effectInfo.add("§7ao acertar a players");
			effectInfo.add("§7com a habilidade. ");
			break;
		default:
			break;
		}
		effectInfo.add(" ");
		effectInfo.add("§8§oNenhum efeito na gameplay.");
		effectInfo.add(" ");
		effectInfo.add("§eRequer §aVIP§e.");
		return effectInfo;
	}
	
	public static void show(Player p) {
		MenuUtils menu = new MenuUtils(Main.get(), p, "§e§lEfeitos", 6);
		ItemStack demonWings = ItemFactory.create(Material.FEATHER, "§7Demon Wings", getInfo(ClassEffectType.DEMON_WINGS));
		ItemStack diamondPunch = ItemFactory.create(Material.DIAMOND_BLOCK, "§7Diamond Punch", getInfo(ClassEffectType.DIAMOND_PUNCH));
		ItemStack flameStrike = ItemFactory.create(Material.BLAZE_POWDER, "§7Flame Strike", getInfo(ClassEffectType.FLAME_STRIKE));
		ItemStack waterBubble = ItemFactory.create(Material.WATER_BUCKET, "§7Water Bubble", getInfo(ClassEffectType.WATER_BUBBLE));
		ItemStack cold = ItemFactory.create(Material.ICE, "§7Cold", getInfo(ClassEffectType.COLD));
		ItemStack love = ItemFactory.create(Material.RED_ROSE, "§7Love Burst", getInfo(ClassEffectType.LOVE_BURST));
		ItemStack blockBlummer = ItemFactory.create(Material.STAINED_CLAY, "§7Block Rumbler", getInfo(ClassEffectType.BLOCK_RUMBLER));
		ItemStack flameDust = ItemFactory.create(Material.FLINT_AND_STEEL, "§7Flame Dust", getInfo(ClassEffectType.FLAME_DUST));
		ItemStack emerald = ItemFactory.create(Material.EMERALD_BLOCK, "§7Emerald Breath", getInfo(ClassEffectType.EMERALD));
		ItemStack flameImpulse = ItemFactory.create(Material.LAVA_BUCKET, "§7Flame Impulse", getInfo(ClassEffectType.FLAME_IMPULSE));
		
		
		ItemStack none = soon("§c???", null);
		
		//uplines
		menu.setItem(10, has(p, ClassEffectType.DEMON_WINGS) ? demonWings : no("§cDemon Wings", getInfo(ClassEffectType.DEMON_WINGS)));
		menu.setItem(11, has(p, ClassEffectType.DIAMOND_PUNCH) ? diamondPunch : no("§cDiamond Punch", getInfo(ClassEffectType.DIAMOND_PUNCH)));
		menu.setItem(12, has(p, ClassEffectType.FLAME_STRIKE) ? flameStrike : no("§cFlame Strike", getInfo(ClassEffectType.FLAME_STRIKE)));
		menu.setItem(13, has(p, ClassEffectType.WATER_BUBBLE) ? waterBubble : no("§cWater Bubble", getInfo(ClassEffectType.WATER_BUBBLE)));
		menu.setItem(14, has(p, ClassEffectType.COLD) ? cold : no("§cCold", getInfo(ClassEffectType.COLD)));
		menu.setItem(15, has(p, ClassEffectType.LOVE_BURST) ? love : no("§cLove Burst", getInfo(ClassEffectType.LOVE_BURST)));
		menu.setItem(16, has(p, ClassEffectType.FLAME_DUST) ? flameDust : no("§cFlame Dust", getInfo(ClassEffectType.FLAME_DUST)));
		menu.setItem(28, has(p, ClassEffectType.BLOCK_RUMBLER) ? blockBlummer : no("§cBlack Rumbler", getInfo(ClassEffectType.BLOCK_RUMBLER)));
		menu.setItem(29, has(p, ClassEffectType.EMERALD) ? emerald : no("§cEmerald Breath", getInfo(ClassEffectType.EMERALD)));
		menu.setItem(30, has(p, ClassEffectType.FLAME_IMPULSE) ? flameImpulse : no("§cFlame Impulse", getInfo(ClassEffectType.FLAME_IMPULSE)));
		menu.setItem(31, none);
		menu.setItem(32, none);
		menu.setItem(33, none);
		menu.setItem(34, none);
		
		//downlines
		menu.setItem(19, sel(p, ClassEffectType.DEMON_WINGS) ? green("§7Demon Wings") : cyan("§cDemon Wings"));
		menu.setItem(20, sel(p, ClassEffectType.DIAMOND_PUNCH) ? green("§7Diamond Punch") : cyan("§cDiamond Punch"));
		menu.setItem(21, sel(p, ClassEffectType.FLAME_STRIKE) ? green("§7Flame Strike") : cyan("§cFlame Strike"));
		menu.setItem(22, sel(p, ClassEffectType.WATER_BUBBLE) ? green("§7Water Bubble") : cyan("§cWater Bubble"));
		menu.setItem(23, sel(p, ClassEffectType.COLD) ? green("§7Cold") : cyan("§cCold"));
		menu.setItem(24, sel(p, ClassEffectType.LOVE_BURST) ? green("§7Love Burst") : cyan("§cLove Burst"));
		menu.setItem(25, sel(p, ClassEffectType.FLAME_DUST) ? green("§7Flame Dust") : cyan("§cFlame Dust"));
		menu.setItem(37, sel(p, ClassEffectType.BLOCK_RUMBLER) ? green("§7Block Rumbler") : cyan("§cBlock Rumbler"));
		menu.setItem(38, sel(p, ClassEffectType.EMERALD) ? green("§7Emerald Breath") : cyan("§cEmerald Breath"));
		menu.setItem(39, sel(p, ClassEffectType.FLAME_IMPULSE) ? green("§7Flame Impulse") : cyan("§cFlame Impulse"));
		menu.setItem(40, soonDye());
		menu.setItem(41, soonDye());
		menu.setItem(42, soonDye());
		menu.setItem(43, soonDye());
		
		menu.setItem(45, ItemFactory.EasyFactory.getXArrow());
		menu.build();
		menu.showMenu(p);
	}
	
	@EventHandler
	public void onInventory(InventoryClickEvent e) {
		if(e.getInventory().getName().equalsIgnoreCase("§e§lEfeitos")) {
			Player p = (Player)e.getWhoClicked();
			GamePlayer gp = GamePlayer.get(p);
			e.setCancelled(true);
			e.setResult(Result.DENY);
			if(e.getCurrentItem() != null && e.getCurrentItem().getType() != Material.AIR) {
				String name = ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName());
				if(name.equalsIgnoreCase("Demon Wings")) {
					name = ClassEffectType.DEMON_WINGS.toString();
				}else if(name.equalsIgnoreCase("Diamond Punch")) {
					name = ClassEffectType.DIAMOND_PUNCH.toString();
				}else if(name.equalsIgnoreCase("Flame Strike")) {
					name = ClassEffectType.FLAME_STRIKE.toString();
				}else if(name.equalsIgnoreCase("Water Bubble")) {
					name = ClassEffectType.WATER_BUBBLE.toString();
				}else if(name.equalsIgnoreCase("Love Burst")) {
					name = ClassEffectType.LOVE_BURST.toString();
				}else if(name.equalsIgnoreCase("Block Rumbler")) {
					name = ClassEffectType.BLOCK_RUMBLER.toString();
				}else if(name.equalsIgnoreCase("Flame Dust")) {
					name = ClassEffectType.FLAME_DUST.toString();
				}else if(name.equalsIgnoreCase("Cold")) {
					name = ClassEffectType.COLD.toString();
				}else if(name.equalsIgnoreCase("Emerald Breath")) {
					name = ClassEffectType.EMERALD.toString();
				}else if(name.equalsIgnoreCase("Flame Impulse")) {
					name = ClassEffectType.FLAME_IMPULSE.toString();
				}else {
					return;
				}
				ClassEffectType effect = ClassEffectType.fromString(name.toUpperCase());
				if(!has(p, effect)) {
					if(!Rank.hasPermission(p, Rank.YOUTUBER)) {
						p.sendMessage(MessageUtil.color("&9Você não tem esse efeito."));
						p.closeInventory();
						return;
					}else{
						gp.getEffects().add(effect.toString().toUpperCase());
						p.sendMessage(MessageUtil.color("&9Efeito desbloqueado por permissão."));
					}
				}
				if(Rank.isVIP(p)) {
					if(sel(p, effect)) {
						gp.getSelectedEffects().remove(effect.toString().toUpperCase());
					}else{
						gp.getSelectedEffects().add(effect.toString().toUpperCase());
					}
				}else {
					p.sendMessage(MessageUtil.color("&cVocê precisa de &aVIP &cpara usar esse efeito."));
				}
				
				show(p);
			}
		}
	}
}
